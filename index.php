<?php
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
/////////                 Sistema de gestion online PIE>A                    /////////
/////////                                                                    /////////
/////////   El sistema de gestion online PIE>A ha sido desarrollado en el    /////////
///////// lenguaje php utilizando el framework Yii en su version 1.1.13. A   /////////
///////// la fecha se han desarrollado los siguientes modulos:               /////////
/////////  * Usuarios: contiene controladores y modelos relaciones con el    /////////
/////////              manejo de usuarios.                                   /////////
/////////  * Proyectos: contiene controladres y modelos relacionados con el  /////////
/////////               manejo de los proyectos tanto postulantes a fondos   /////////
/////////               I+D+i como tambien creados directamente en el        /////////
/////////               sistema.                                             /////////
/////////  * Iniciativas: contiene controladores y modelos relacionados      /////////
/////////                 con el manejo de las iniciativas PIE>A.            /////////
/////////  * Tareas: contiene controladres y modelos relacionados con el     /////////
/////////            manejo de las actividades de las entidades de PIEA.     /////////
/////////  * Formularios: contiene controladores y modelos relaciones con    /////////
/////////                 el manejo de los formularios I+D+i, de viajes,     /////////
/////////                 FAE, etc.                                          /////////
/////////  * Finanzas: contiene controladores y modelos relacionados con     /////////
/////////              el manejo financiero de PIEA, iniciativas y           /////////
/////////              proyectos.                                            /////////
/////////  Cada modulo aun continua en desarrollo. Mas detalle sobre el      /////////
/////////  funcionamiento y otras hierbas puede encontrarse en la clase      /////////
/////////  inicial de cada modulo.                                           /////////
/////////                                                                    /////////
/////////  Desarrollador principal: dacuna <diego.acuna@usm.cl>              /////////
/////////  Fecha: 07 de julio, 2013                                          /////////
/////////                                                                    /////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


// $yii debe apuntar al archivo yii.php del framework
$yii=dirname(__FILE__).'/../framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
$globals = dirname(__FILE__).'/protected/globals.php';

// Las siguientes lineas son para debuggear el sistema. En produccion deben comentarse.
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once($globals);
Yii::createWebApplication($config)->run();