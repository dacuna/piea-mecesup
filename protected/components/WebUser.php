<?php
/**
 * Representa a un usuario logeado dentro de la aplicacion. Este objeto es utilizable mediante
 * el atajo Yii de user()
 *
 * Provee de variados metodos utiles que representan informacion sobre un usuario que esta logeado
 * en la aplicacion. Permite por ejemplo, crear el menu tipo arbol con los proyectos e iniciativas
 * de un usuario, obtener un listado con los proyectos e iniciativas del usuario, etc.
 *
 * @package   components
 * @example <br />
 *    $id_logeado = user()->id;<br />
 *  $proyectos_usuario_logeado = user()->proyectos();<br />
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class WebUser extends CWebUser
{
    /**
     * Overrides el metodo que chequea persmisos de un usuario agregandole que si el usuario no esta
     * logeado se le redirige a la ventana de login.
     *
     * @param string $operation Nombre del item a chequear (role, task, operation).
     * @param mixed $params (opt) Parametros para la operacion.
     * @return bool true si el usuario tiene persmisos para ejecutar la operacion, false en caso contrario.
     */
    public function checkAccess($operation, $params = array())
    {
        $val = parent::checkAccess($operation, $params);
        if (!$val)
            if ($this->getIsGuest())
                $this->loginRequired();
        return $val;
    }

    /**
     * Crea el menu de acciones de usuario para el usuario logeado.
     *
     * @return array menu de usuario para ser entregado a un widget CTreeView
     * @see CTreeView
     */
    public function crearMenu()
    {
        $data = array();
        $css = 'border:0;padding:3px 0;font-size:100%;';
        //si el usuario es administrador le muestro todas las opciones
        if (Yii::app()->user->checkAccess('administrador')) {
            $options = array('href' => Yii::app()->createUrl('iniciativas/crudIniciativas/crear'), 'style' => $css);
            array_push($data, array('text' => CHtml::openTag('a', $options) . 'Crear iniciativa' . CHtml::closeTag('a')));
            $options = array('href' => Yii::app()->createUrl('proyectos/crud/crear'), 'style' => $css);
            array_push($data, array('text' => CHtml::openTag('a', $options) . 'Crear proyecto' . CHtml::closeTag('a')));

            //INICIO MANEJO DE USUARIOS
            $extra = array('text' => 'Usuarios');
            $data_cuentas = array();
            $options = array('href' => Yii::app()->createUrl('usuarios/userActions/verUsuarios'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Ver usuarios del sistema' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/usuarios/userActions/excelUsuariosSistema'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Excel con todos los usuarios del sistema' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/formularios/administracionFondoIDI/generarExcelPostulantes'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Excel usuarios postulantes fondo I+D+i' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/formularios/revisionFondosIDI/generarExcelRevisores'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Excel usuarios revisores fondo I+D+i' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $extra['children'] = $data_cuentas;
            $extra['expanded'] = true;
            array_push($data, $extra);
            //FIN MANEJO DE USUARIOS

            //se agrega el manejo de cuentas
            $extra = array('text' => 'Cuentas');
            $data_cuentas = array();
            $options = array('href' => Yii::app()->createUrl('/finanzas/crud/index'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Ver cuentas' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/finanzas/crud/crear'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Agregar cuenta' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/finanzas/crud/entregaFondos'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Entrega de fondos' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/finanzas/crud/transferenciaFondos'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Transferencia de fondos' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/finanzas/historico/entregas'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Hist&oacute;rico de entregas' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $options = array('href' => Yii::app()->createUrl('/finanzas/historico/transferencias'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Hist&oacute;rico de transferencias' . CHtml::closeTag('a'));
            array_push($data_cuentas, $contenido);
            $extra['children'] = $data_cuentas;
            $extra['expanded'] = false;
            array_push($data, $extra);
            //se agrega manejo de fondos
            $extra = array('text' => 'Fondos');
            $data_fondos = array();
            $options = array('href' => Yii::app()->createUrl('/formularios/administracionFondos/verFormulario', array('id' => 1)), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Fondo I+D+i' . CHtml::closeTag('a'));
            array_push($data_fondos, $contenido);
            $extra['children'] = $data_fondos;
            $extra['expanded'] = false;
            array_push($data, $extra);

            //se agrega manejo de FAE
            $extra = array('text' => 'FAE');
            $data_fondos = array();
            $options = array('href' => Yii::app()->createUrl('/formularios/administracionFondos/verFAE'), 'style' => $css);
            $contenido = array('text' => CHtml::openTag('a', $options) . 'Ver proceso FAE' . CHtml::closeTag('a'));
            array_push($data_fondos, $contenido);
            $extra['children'] = $data_fondos;
            $extra['expanded'] = false;
            array_push($data, $extra);

            //el administrador puede ver todo
            //obtengo todas las iniciativas
            $extra = array('text' => 'Iniciativas');
            $iniciativas = Iniciativa::model()->findAll();
            $data_iniciativas = array();
            foreach ($iniciativas as $iniciativa) {
                $options = array('href' => Yii::app()->createUrl('iniciativas/crudIniciativas/ver', array('id' => $iniciativa->id)), 'style' => $css);
                $contenido = array('text' => CHtml::openTag('a', $options) . $iniciativa->nombre_abreviado . CHtml::closeTag('a'));
                $proyectos = array();
                foreach ($iniciativa->proyectos as $proyecto) {
                    $o = array('href' => Yii::app()->createUrl('proyectos/crud/ver', array('id' => $proyecto->id)), 'style' => $css);
                    $c = array('text' => CHtml::openTag('a', $o) . $proyecto->nombre . CHtml::closeTag('a'));
                    array_push($proyectos, $c);
                }
                $contenido['children'] = $proyectos;
                $contenido['expanded'] = true;
                array_push($data_iniciativas, $contenido);
            }
            $extra['children'] = $data_iniciativas;
            array_push($data, $extra);
        } else {
            //manejo de fondos:
            if (user()->checkAccess("ver_menu_idi")) {
                //se agrega manejo de fondos
                $extra = array('text' => 'Fondos');
                $data_fondos = array();
                $options = array('href' => Yii::app()->createUrl('/formularios/administracionFondos/verFormulario', array('id' => 1)), 'style' => $css);
                $contenido = array('text' => CHtml::openTag('a', $options) . 'Fondo I+D+i' . CHtml::closeTag('a'));
                array_push($data_fondos, $contenido);
                $extra['children'] = $data_fondos;
                $extra['expanded'] = true;
                array_push($data, $extra);
            }
            //obtengo todos los roles del usuario actual
            $usuario = Persona::model()->findByPk(Yii::app()->user->id);
            $iniciativas = $usuario->iniciativas;
            $proyectos_marked = array();
            if ($iniciativas != null) {
                //se construye un elemento para cada iniciativa
                $extra = array('text' => 'Iniciativas');
                $data_iniciativas = array();
                foreach ($iniciativas as $iniciativa) {
                    $options = array('href' => Yii::app()->createUrl('iniciativas/crudIniciativas/ver', array('id' => $iniciativa->id)), 'style' => $css);
                    $contenido = array('text' => CHtml::openTag('a', $options) . $iniciativa->nombre_abreviado . CHtml::closeTag('a'));
                    $contenido['expanded'] = true;
                    //agrego los proyectos
                    //si es coordinador, se agregan todos los proyectos
                    if (Yii::app()->user->checkAccess('coordinador_iniciativa', array('iniciativa' => $iniciativa->id))) {
                        $proyectos = array();
                        foreach ($iniciativa->proyectos as $proyecto) {
                            array_push($proyectos_marked, $proyecto->id);
                            $o = array('href' => Yii::app()->createUrl('proyectos/crud/ver', array('id' => $proyecto->id)), 'style' => $css);
                            $c = array('text' => CHtml::openTag('a', $o) . $proyecto->nombre . CHtml::closeTag('a'));
                            array_push($proyectos, $c);
                        }
                        $contenido['children'] = $proyectos;
                    } //sino agrego solo los del usuario
                    else {
                        $proyectos = array();
                        foreach ($iniciativa->getProyectosDeUsuario($usuario->id) as $proyecto) {
                            array_push($proyectos_marked, $proyecto->id);
                            $o = array('href' => Yii::app()->createUrl('proyectos/crud/ver', array('id' => $proyecto->id)), 'style' => $css);
                            $c = array('text' => CHtml::openTag('a', $o) . $proyecto->nombre . CHtml::closeTag('a'));
                            array_push($proyectos, $c);
                        }
                        $contenido['children'] = $proyectos;
                    }
                    array_push($data_iniciativas, $contenido);
                }
                $extra['children'] = $data_iniciativas;
                array_push($data, $extra);
            }
            //busco los proyectos
            $proyectos = $usuario->proyectos;
            $total_p = count($proyectos);
            if ($total_p != 0) {
                $extra = array('text' => 'Proyectos');
                $data_proyectos = array();
                foreach ($proyectos as $proyecto) {
                    if (!in_array($proyecto->id, $proyectos_marked)) {
                        $options = array('href' => Yii::app()->createUrl('/proyectos/crud/ver', array('id' => $proyecto->id)), 'style' => $css);
                        $contenido = array('text' => CHtml::openTag('a', $options) . $proyecto->nombre . CHtml::closeTag('a'));
                        array_push($data_proyectos, $contenido);
                    }
                }
                $extra['children'] = $data_proyectos;
                if (count($data_proyectos) > 0)
                    array_push($data, $extra);
            }
        }

        //para los ayudantes de hitos
        Yii::import('application.modules.formularios.models.ProyectoSeleccionado');
        Yii::import('application.modules.formularios.models.ProcesoFormulario');
        $extra = array('text' => 'Ayudante de hitos');
        $data_proyectos = array();
        $proceso = FormularioPiea::procesoFormularioFondosIDI();
        $proyectos = ProyectoSeleccionado::model()->findAll('id_proceso_formulario=:ipf', array(
            ':ipf' => $proceso->id));
        //ver que proyectos ya tiene asignado
        foreach ($proyectos as $proyecto) {
            $permiso = "ayudante_hitos_proyecto_{$proyecto->id_proyecto}";
            if (am()->checkAccess($permiso, user()->id)) {
                $options = array('href' => Yii::app()->createUrl('/proyectos/crud/ver', array('id' => $proyecto->id_proyecto)), 'style' => $css);
                $contenido = array('text' => CHtml::openTag('a', $options) . $proyecto->proyecto->nombre . CHtml::closeTag('a'));
                array_push($data_proyectos, $contenido);
            }
        }
        $extra['children'] = $data_proyectos;
        $extra['expanded'] = true;
        if (count($data_proyectos) > 0)
            array_push($data, $extra);

        return $data;
    }

    /**
     * BizRule para RBAC que verifica los permisos que tiene un usuario contra
     * la tabla de LookupIniciativa o LookupProyecto segun corresponda. Para el caso de un
     * proyecto, se debe pasar tambien como parametro la iniciativa a la que pertenece, para
     * que el sistema RBAC pueda recorrer el arbol de permisos y verificar correctamente la
     * jerarquia de estos. En el caso de las iniciativas, el parametro proyecto no es necesario.
     *
     * @param string $nombre_rol    El nombre del rol, operacion o task que se desea comprobar.
     * @param int $iniciativa    El id de la iniciativa donde se desean verificar los permisos.
     * @param int $proyecto      El id del proyecto donde se desean verificar los permisos.
     *
     * @return  boolean true si el usuario posee los permisos, false en caso contrario.
     * @throws  \CHttpException
     *
     * @since   2012-07-02
     * @author  Diego Acuna <diego.acuna@usm.cl>
     *
     * @edit    2012-09-10<br />
     *          Diego Acuna <diego.acuna@usm.cl><br />
     *          Solucionado bug que entregaba los permisos de manera incorrecta, debido a que se estaban
     *          utilizando mal los $param['algo'] del sistema RBAC.<br/>
     *          #edit3392
     */
    public function verificar_rol($nombre_rol, $iniciativa, $proyecto = null)
    {
        if (user()->checkAccess('administrador'))
            return true;
        if ($iniciativa == null) return false; //no pueden haber proyectos sin iniciativas
        if ($nombre_rol != 'coordinador_iniciativa') {
            if (user()->checkAccess('coordinador_iniciativa', array('iniciativa' => $iniciativa)))
                return true;
        } else {
            $connection = Yii::app()->db;
            $table = 'tbl_lookup_iniciativa';
            $sql = "SELECT user_id FROM " . $table . " WHERE activo=1 and nombre_item='coordinador_iniciativa' and iniciativa_id=" . $iniciativa;
            $command = $connection->createCommand($sql);
            $dataReader = $command->query();
            //se debe verificar que el usuario obtenido es el mismo que esta logeado!
            foreach ($dataReader as $row) {
                if ($row['user_id'] == Yii::app()->user->id)
                    return true;
            }
            return false;
        }
        //en este punto debo distinguir si estoy validando un rol de proyecto o de iniciativa
        //si proyecto no es null, entonces estoy evaluando un proyecto
        if ($proyecto != null) {
            $tipo_entidad = 'proyecto';
            $entidad_id = $proyecto;

            if ($nombre_rol != 'jefe_proyecto') {
                if (user()->checkAccess('jefe_proyecto', array('iniciativa' => $iniciativa, 'proyecto' => $proyecto)))
                    return true;
            } else {
                $connection = Yii::app()->db;
                $table = 'tbl_lookup_proyecto';
                $sql = "SELECT user_id FROM " . $table . " WHERE activo=1 and nombre_item='jefe_proyecto' and proyecto_id=" . $proyecto;
                $command = $connection->createCommand($sql);
                $dataReader = $command->query();
                //se debe verificar que el usuario obtenido es el mismo que esta logeado!
                foreach ($dataReader as $row) {
                    if ($row['user_id'] == Yii::app()->user->id)
                        return true;
                }
            }
        } else {
            $tipo_entidad = 'iniciativa';
            $entidad_id = $iniciativa;
        }
        //ejecuta una consulta raw a la db para ver si tiene el rol
        $connection = Yii::app()->db;
        $table = 'tbl_lookup_' . $tipo_entidad;
        if ($table != "tbl_lookup_iniciativa" && $table != "tbl_lookup_proyecto")
            throw new CHttpException(403, 'Solicitud incorrecta. Por favor, no vuelva a realizar la misma petici&oacute;n.');
        $sql = "SELECT user_id FROM " . $table . " WHERE activo=1 and nombre_item='" . $nombre_rol . "' and ";
        $sql .= $tipo_entidad . "_id=" . $entidad_id;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        //se debe verificar que el usuario obtenido es el mismo que esta logeado!
        foreach ($dataReader as $row) {
            if ($row['user_id'] == Yii::app()->user->id)
                return true;
        }
        return false;
    }

    /**
     * Retorna las iniciativas a las que pertenece el usuario logeado.
     *
     * @return  array Array de CActiveRecords de iniciativas
     * @todo se debe chequear los permisos en lookup_iniciativas, puesto que pueden haber usuarios que no sean
     *  integrantes de una iniciativa pero que si puedan ver determinadas iniciativas en las que tengan permisos
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function iniciativas()
    {
        if (user()->checkAccess('administrador'))
            return Iniciativa::model()->findAll();
        $usuario = Persona::model()->findByPk(user()->id);
        return $usuario->iniciativas;
    }

    /**
     * Retorna los proyects a los que pertenece el usuario logeado.
     *
     * @return  array Array de CActiveRecords de proyectos
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function proyectos()
    {
        if (user()->checkAccess('administrador'))
            return Proyecto::model()->findAll();
        $usuario = Persona::model()->findByPk(user()->id);
        return $usuario->proyectos;
    }

    /**
     * Retorna un boolean indicando si el usuario logeado es postulante al proceso actual de
     * fondos I+D+i.
     *
     * @return  bool true si es logeado false en caso contrario.
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function esPostulanteFondosIDI()
    {
        $persona = Persona::model()->findByPk(user()->id);
        return $persona->getEsPostulanteFondoIDI();
    }

    /**
     * Retorna, si es que el usuario logeado es postulante a los fondos I+D+i, el proyecto con el
     * que esta postulando.
     *
     * @return  mixed Un {@link Proyecto} si es que es postulante, null en caso contrario.
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function proyectoPostulacion()
    {
        $persona = Persona::model()->findByPk(user()->id);
        return $persona->getProyectoEnPostulacion();
    }

}