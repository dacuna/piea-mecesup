<?php
/**
 * Mailman contiene la implementacion de un sistema de notificaciones para el portal PIEA que implementa
 * el patron publish-suscribe dentro de su core y hace un uso fuerte de la clase \CEvent de Yii.
 *
 * Una notificacion corresponde a un mensaje entre dos entidades del sistema (normalmente ambas entidades
 * seran usuarios pero el mailman no se limita unicamente a este tipo de entidades). El mensaje esta
 * representado por la clase \Mensaje la cual contiene variados atributos los cuales no siempre seran en
 * su totalidad utilizados. Un mensaje puede ser reconocido ya sea por su id (clave primaria autoincremental) o
 * mediante 3 valores conocidos:
 *  - e: id del emisor de la notificacion
 *  - d: destinatario de la notificacion
 *  - uid: identificador unico de la operacion, es creado mediante la funcion uniqid('',true) de php agregando
 *         entropia al mersenne twister.
 * Basicamente el MailMan se puede utilizar de dos maneras distintas (esto debido a cambios en las implementaciones
 * que se han realizado en la aplicacion y para mantener el codigo legacy). El primer metodo es sin utilizar los
 * eventos del framework yii. Para esto, se debe construir el mensaje a enviar y luego utilizar las funciones
 * basicas que provee el mailman directamente. Por ejemplo:
 *
 * @example <br/>
 *  $mailman=Yii::app()->mailman;
 *  $asunto='asunto del mensaje';
 *  $mailman->crearMensaje($asunto,'temp');
 *  $mailman->setPublisher(Yii::app()->user->id);
 *  $mailman->setSuscriber($this->usuario_id);
 *  $mailman->setRelation($this->entidad_id,$this->tipo_entidad);
 *  $id_mensaje=$mailman->enviarMensaje();
 *  $mailman->actualizarMensaje($id_mensaje,$mensaje);
 *
 * Del ejemplo anterior se deben rescatar varias caracteristicas del mailman:
 *  - Es un componente del framework (es configurado en config/main.php como un componente), es por esto que
 *    puede ser llamado como Yii::app()->mailman sin la necesidad de ser instanciado.
 *  - Si se desea referenciar el mensaje por su id clave primaria entonces esta no puede ser obtenida en el
 *    contenido del mensaje. Esto es debido a que la id se genera despues de guardar el mensaje en la db. La
 *    implicancia de esto es que si en el contenido del mensaje se desea autoreferenciar al mismo mensaje entonces
 *    se debera actualizar el contenido una vez ya guardado en la db (se debe guardar el mensaje y luego volver
 *    a actualizarlo). Si no se desea realizar esto (debido a que involucra 2 operaciones en la db) entonces se
 *    debe utilizar los campos e,d y uid (los cuales estan indexados a nivel de db por lo que no se pierde performance.
 *
 * Esta forma de uso del mailman esta obsoleta y no debe utilizarse (sin embargo se puede ver en ciertas partes de
 * codigo legacy el cual debiera a futuro ser actualizado). La version recomendada de uso es mediante eventos de yii.
 * Para esto se debe definir que evento gatillara el envio de una notificacion. Por ejemplo, suponga que luego de que
 * un usuario realice la solicitud de un presupuesto se desea notificar al dueno de la cuenta presupuestaria utilizada
 * en el presupuesto, para esto se define en la clase correspondiente (en este caso el controlador) el evento a ejecutar:
 *
 * @example <br />
 *  //definicion del evento que se ejecutara luego de que un usuario solicite un presupuesto
 *  public function onSolicitarPresupuesto($event){
 *    $this->raiseEvent('onSolicitarPresupuesto',$event);
 *  }
 *
 * Ademas, se debe definir el handler de este evento, se debe poner atencion en esta parte pues es la parte central de
 * funcionamiento del mailman. El handler se puede definir por ejemplo en el init() (ya que se esta trabajando con un
 * controlador /CController de yii el cual posee un metodo init similar al __construct tipico de otra clase en php)
 * de la clase (en realidad puede estar definido en cualquier parte del codigo pero cuidando que la aplicacion realmente
 * ejecute dicha fraccion de codigo). En dicho handler se debe especificar la funcion helper de Mailman llamada
 * 'notificacionHandler', para esto, se realiza de la siguiente manera
 *
 * @example <br />
 *  public function init(){
 *    //definicion del handler para el evento definido anteriormente
 *    $this->onSolicitarPresupuesto=array(Yii::app()->mailman,'notificacionHandler');
 *  }
 *
 * Por ultimo, se debe definir el contenido de mensaje, esto se realiza en la funcion notificacionHandler del propio
 * Mailman, el codigo es bastante autoexplicativo:
 *
 * @example <br />
 * case 'solicitar_presupuesto':
 *    $this->mensaje->asunto='Solicitud de aprobaci&oacute;n de presupuesto';
 *    $this->setPublisher(user()->id);
 *    $this->setAccion('presupuesto');
 *    $this->mensaje->id_tarea=$event->params['presupuesto']->id;
 *    $this->setSuscriber($event->params['destinatario']);
 *    $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
 *    $this->contenido=array('location'=>'_solicitar_presupuesto',
 *       'params'=>array('mensaje'=>$this->mensaje,'entidad'=>$event->params['entidad'],
 *       'presupuesto'=>$event->params['presupuesto'],'encargado'=>$event->params['encargado']));
 * break;
 *
 * Luego, para llamar al evento:
 *
 * @example <br />
 *   $this->onSolicitarPresupuesto(new CEvent($this,array('tipo'=>'solicitar_presupuesto',
 *      'presupuesto'=>$presupuesto)));
 *
 * Como se puede observar, al utilizar el Mailman de esta manera no se debe lidiar con las funciones de bajo nivel
 * implementadas en esta clase ya que el mailman se encarga automaticamente del envio de las notificaciones y ademas
 * si el usuario tiene activada la opcion de recibir mails, el mailman se encarga tambien de enviar el mail.
 *
 * Para mayor referencia sobre que campos se pueden utilizar en los mensajes y cuales son los significados de cada uno
 * debe dirigirse a la clase \Mensaje.
 *
 * @category  CApplicacionComponent
 * @package   Components
 * @version   1.1
 * @since     2012-05-25
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Mailman extends CApplicationComponent
{
    /**
     * El mensaje a enviar por el mailman
     * @var \Mensaje
     */
    private $mensaje;

    /**
     * Array de suscriptores que recibiran el mensaje. Los suscriptores pueden ser de dos tipos, ya sea cualquier clase
     * que implemente un getter magico para un atributo id o que posea un atributo publico id, o, una variable numerica
     * que represente la id de alguna entidad (tipicamente un usuario).
     * @var array
     */
    private $suscribers = array();

    /**
     * Array que mantiene la informacion sobre a que entidades (la id de estas) se les ha enviado el mensaje.
     * @var array
     */
    private $id_enviados = array();

    /**
     * Asunto del mensaje a enviar
     * @var string
     */
    private $asunto;

    /**
     * Contenido de mensaje a enviar. Puede contener un documento html.
     * @var string
     */
    private $contenido;

    /**
     * Permite identificar en el handler de eventos notificacionHandler que mensaje se esta enviando. Se pueden agregar
     * multiples tipos de notificaciones, para esto solo se debe agregar el caso particular en el handler
     * notificacionHandler.
     * @var string
     */
    private $tipo_notificacion;

    /**
     * Metodo de inicializacion del mailman.
     *
     * @return  void
     *
     * @since   2012-05-25
     */
    public function init()
    {

    }

    /**
     * Permite crear un mensaje asignandole automaticamente un uid unico (en la practica) y asignando los parametros
     * basicos del mensaje. Esta funcion no deberia ser utilizada directamente en la aplicacion.
     *
     * @param string $asunto Asunto del mensaje
     * @param string $contenido Contenido del mensaje
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     *
     * @edit    2013-06-08<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Agregado aviso en la documentacion de que esta funcion esta obsoleta y no debe ser utilizada
     *          directamente en la aplicacion.
     */
    public function crearMensaje($asunto, $contenido)
    {
        $this->mensaje = new Mensaje;
        $this->id_enviados = array();
        $this->mensaje->asunto = $asunto;
        $this->asunto = $asunto;
        $this->mensaje->contenido = $contenido;
        $this->mensaje->uid=uniqid('',true);
    }

    /**
     * Funcion para asignar el emisor del mensaje. Puede ser ya sea cualquier clase que posea un atributo llamado
     * 'id' y que sea accesible ya sea por ser publico o mediante un metodo magico, o, una variable numerica que
     * represente el id de una entidad cualquiera.
     *
     * @param mixed $publisher Entidad que actuara como emisor del mensaje
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     */
    public function setPublisher($publisher)
    {
        if (isset($publisher->id))
            $this->mensaje->emisor = $publisher->id;
        else
            $this->mensaje->emisor = $publisher;
    }

    /**
     * Funcion para asignar los receptores del mensaje a enviar. La variable suscribers puede contener la informacion
     * que se detalla en la definicion de esta variable en esta clase (ver documentacion de las variables de esta
     * clase).
     *
     * @param mixed $suscribers Suscriptores del mensaje.
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     */
    public function setSuscriber($suscribers)
    {
        if (is_array($suscribers)) {
            foreach ($suscribers as $elem) {
                if (isset($elem->id))
                    array_push($this->suscribers, $elem->id);
                else
                    array_push($this->suscribers, $elem);
            }
        } else
            if (isset($suscribers->id))
                array_push($this->suscribers, $suscribers->id);
            else
                array_push($this->suscribers, $suscribers);
    }

    /**
     * Funcion para asignar la relacion de la notificacion con alguna entidad del sistema (iniciativas o proyectos).
     * Para mayor informacion sobre la relacion de un mensaje ver \Mensaje.
     *
     * @param int $relation ID de la entidad relacionada
     * @param int $relation_type Tipo de la entidad relacionada
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     */
    public function setRelation($relation, $relation_type)
    {
        $this->mensaje->id_relacion = $relation;
        $this->mensaje->tipo_relacion = $relation_type;
    }

    /**
     * Funcion que permite enviar un mensaje ya armado en el mailman. ESTA FUNCION ES OBSOLETA Y NO SE RECOMIENDA SU
     * USO.
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     *
     * @edit    2013-06-08<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Agregado comentario a la documentacion de que la funcion esta obsoleta.
     */
    public function enviarMensaje()
    {
        $id_mensaje = null;
        foreach ($this->suscribers as $elem) {
            $this->mensaje->destinatario = $elem;
            $this->mensaje->save();
            //lanzo el evento de que el mensaje fue enviado
            $this->onMensajeEnviado(new CEvent($this));
            //fin lanzamiento eventos
            $id_mensaje = $this->mensaje->primaryKey;
            array_push($this->id_enviados, $id_mensaje);
            $this->mensaje->id = null;
            $this->mensaje->isNewRecord = true;
        }
        //lo destruyo para evitar ambiguedades
        $this->mensaje = null;

        if (count($this->id_enviados) == 1)
            return $id_mensaje;
        return $this->id_enviados;
    }

    /**
     * Funcion que permite actualizar el contenido de un mensaje mediante su id (clave primaria).
     *
     * @param int $id ID del mensaje a actualizar
     * @param string $mensaje Nuevo contenido del mensaje
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     */
    public function actualizarMensaje($id, $mensaje)
    {
        Mensaje::model()->updateByPk($id, array('contenido' => $mensaje));
        $this->enviarMail($this->asunto, $mensaje);
    }

    /**
     * Funcionar que permite actualizar los mensajes enviados a todos los suscriptores a los que
     * previamente se les ha enviado un mensaje.
     *
     * @param string $mensaje Contenido nuevo a insertar en los mensajes a actualizar
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     */
    public function actualizarEnviados($mensaje)
    {
        foreach ($this->id_enviados as $id_enviado) {
            Mensaje::model()->updateByPk($id_enviado, array('contenido' => $mensaje));
        }
        $this->enviarMail($this->asunto, $mensaje);
    }

    /**
     * Permite asignar el id de la tarea del mensaje a enviar. Para mas informacion ver \Mensaje.
     *
     * @param int $tarea_id ID de la tarea a asignar
     *
     * @return  void
     *
     * @since   2012-08-12
     */
    public function setTarea($tarea_id)
    {
        $this->mensaje->id_tarea = $tarea_id;
    }

    /**
     * Permite asignar la accion del mensaje a enviar. Para mas informacion ver \Mensaje.
     *
     * @param string $accion Accion del mensaje
     *
     * @return  void
     *
     * @since   2012-08-12
     */
    public function setAccion($accion)
    {
        $this->mensaje->accion = $accion;
    }

    /**
     * Funcion que permite enviar un mensaje por email. ESTA FUNCION ESTA OBSOLETA Y NO DEBE SER UTILIZADA.
     *
     * @param string $asunto Asunto del mensaje
     * @param string $contenido Contenido de mensaje a enviar
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-05-25
     *
     * @edit    2013-06-08<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Agregado comentario de que esta funcion se encuentra obsoleta y no se debe utilizar.
     */
    public function enviarMail($asunto, $contenido)
    {
        $flag = false;
        $message = new YiiMailMessage;
        $message->setBody($contenido, 'text/html');
        $message->subject = $asunto;
        foreach ($this->suscribers as $elem) {
            $mail = Persona::model()->findByPk($elem);
            $data = Yii::app()->db->createCommand('SELECT email AS email FROM {{user}} WHERE id=' . $elem)->queryColumn();
            $data2 = Yii::app()->db->createCommand('SELECT recibir_email AS recibir_email FROM {{perfil_usuario}} WHERE user_id=' . $elem)->queryColumn();
            if ($data2[0] == 1) {
                $message->addTo($data[0]);
                $flag = true;
            }
        }
        $message->from = array('piea@usm.cl' => 'PIE>A::Gestion I+D+i');
        if ($flag)
            Yii::app()->mail->send($message);
    }

    /**
     * Getter para el atributo mensaje.
     *
     * @return  \Mensaje
     *
     * @since   2012-05-25
     */
    public function getMensaje(){
        return $this->mensaje;
    }

    /**
     * Handler para manejar el funcionamiento del mailman mediante \CEvent de yii. Para agregar un nuevo tipo de
     * notificacion se debe agregar el case particular dentro de este handler. Luego cuando se genere el evento
     * se especifica el tipo como param del mismo evento (vease ejemplos al comienzo de la clase).
     *
     * @param \CEvent $event Evento que ejecuto este handler
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-08-12
     */
    public function notificacionHandler($event){
        //en este handler se maneja el envio de las distintas notificaciones posibles del sistema
        $this->tipo_notificacion=$event->params['tipo'];
        $this->mensaje=new Mensaje;
        switch($this->tipo_notificacion){
            case 'invitacion_jefe_proyecto':
                $this->mensaje->asunto="Solicitud de jefe de proyecto {$event->params['proyecto']->nombre}";
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['usuario']);
                $this->setRelation($event->params['proyecto']->id,'proyecto');
                $this->contenido=array('location'=>'_invitacion_jefe_proyecto',
                    'params'=>array('mensaje'=>$this->mensaje,'usuario'=>$event->params['usuario'],
                    'proyecto'=>$event->params['proyecto']));
                break;
            case 'cambiar_coordinador':
                $iniciativa = Iniciativa::model()->findByPk($event->sender->id_iniciativa);
                $usuario=Persona::model()->findByPk($event->sender->id_usuario);
                $this->mensaje->asunto=$this->asunto='Solicitud de coordinaci&oacute;n iniciativa '.$iniciativa->nombre_abreviado;
                $this->setPublisher(app()->user);
                $this->setSuscriber($usuario);
                $this->setRelation($iniciativa->id,'iniciativa');
                $this->contenido=array('location'=>'_cambiar_coordinador_solicitud',
                    'params'=>array('mensaje'=>$this->mensaje,'usuario'=>$usuario,'iniciativa'=>$iniciativa));
                break;
            case 'aceptar_cambio_coordinacion':
                $this->mensaje->asunto=$this->asunto='Invitaci&oacute;n coordinaci&oacute;n iniciativa '.$event->params['iniciativa']->nombre_abreviado.' aceptada';
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['iniciativa']->id,'iniciativa');
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'aceptar_cambio_coordinacion',
                    'params'=>array('usuario'=>$event->params['usuario'],'iniciativa'=>$event->params['iniciativa']));
                break;
            case 'rechazar_cambio_coordinacion':
                $this->mensaje->asunto=$this->asunto='Invitaci&oacute;n coordinaci&oacute;n iniciativa '.$event->params['iniciativa']->nombre_abreviado.' rechazada';
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['iniciativa']->id,'iniciativa');
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'rechazar_cambio_coordinacion',
                    'params'=>array('usuario'=>$event->params['usuario'],'iniciativa'=>$event->params['iniciativa']));
                break;
            case 'jefe_proyecto':
                $proyecto=Proyecto::model()->findByPk($event->sender->id_proyecto);
                $usuario=Persona::model()->findByPk($event->sender->id_usuario);
                $this->mensaje->asunto=$this->asunto='Solicitud de jefe de proyecto en '.$proyecto->nombre;
                $this->setPublisher(app()->user);
                $this->setSuscriber($usuario);
                $this->setRelation($proyecto->id,'proyecto');
                $this->contenido=array('location'=>'_jefe_proyecto_solicitud',
                    'params'=>array('mensaje'=>$this->mensaje,'usuario'=>$usuario,'proyecto'=>$proyecto));
                break;
            case 'aceptar_cambio_jefe_proyecto':
                $this->mensaje->asunto=$this->asunto='Invitaci&oacute;n jefe de proyecto '.$event->params['proyecto']->nombre.' aceptada';
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['proyecto']->id,'proyecto');
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'aceptar_cambio_jefe_proyecto',
                    'params'=>array('usuario'=>$event->params['usuario'],'proyecto'=>$event->params['proyecto']));
                break;
            case 'rechazar_cambio_jefe_proyecto':
                $this->mensaje->asunto=$this->asunto='Invitaci&oacute;n jefe de proyecto '.$event->params['proyecto']->nombre.' rechazada';
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['proyecto']->id,'proyecto');
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'rechazar_cambio_jefe_proyecto',
                    'params'=>array('usuario'=>$event->params['usuario'],'proyecto'=>$event->params['proyecto']));
                break;
            case 'cargo_eliminado':
                $this->mensaje->asunto=$this->asunto='Eliminaci&oacute;n de cargo en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $usuario=Persona::model()->findByPk(user()->id);
                $this->contenido=array('location'=>'_cargo_eliminado',
                    'params'=>array('usuario'=>$usuario,'entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo']));
                break;
            case 'notificar_encargado_cargo_eliminado':
                $this->mensaje->asunto=$this->asunto='Eliminaci&oacute;n de cargo en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(app()->user);
                $this->setSuscriber($event->params['superior']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_notificar_encargado_cargo_eliminado',
                    'params'=>array('entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo']));
                break;
            case 'solicitar_transferencia':
                $this->mensaje->asunto='Solicitud de aceptaci&oacute;n de cargo en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['usuario']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion($event->params['cargo']->nombre_item);
                $this->contenido=array('location'=>'_solicitar_transferencia',
                    'params'=>array('mensaje'=>$this->mensaje,'entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo']));
                break;
            case 'transferencia_cargo_aceptada':
                $this->mensaje->asunto='Cargo aceptado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_transferencia_cargo_aceptada',
                    'params'=>array('entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo'],'usuario'=>$event->params['usuario']));
                break;
            case 'cargo_revocado':
                $this->mensaje->asunto='Cargo revocado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher($event->params['emisor']);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_cargo_revocado',
                    'params'=>array('entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo']));
                break;
            case 'transferencia_cargo_rechazada':
                $this->mensaje->asunto='Cargo rechazado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_transferencia_cargo_rechazada',
                    'params'=>array('entidad'=>$event->params['entidad'],'cargo'=>$event->params['cargo'],'usuario'=>$event->params['usuario']));
                break;
            case 'notificar_entrega_entidad':
                $this->mensaje->asunto='Entrega de fondos realizada en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber(1);
                $this->setRelation($event->params['entidad']->id,strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_notificar_entrega_entidad',
                    'params'=>array('entidad'=>$event->params['entidad'],'origen'=>$event->params['origen'],'usuario'=>$event->params['usuario']));
                break;
            case 'solicitar_aprobacion_transferencia':
                $this->mensaje->asunto='Solicitud de transferencia de fondos en '.$event->params['solicitud']->getEntidadDestino()->getTipoEntidad().' '.$event->params['solicitud']->getEntidadDestino()->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber(1);
                $this->setRelation($event->params['solicitud']->entidad_id_destino,strtolower($event->params['solicitud']->getEntidadDestino()->getTipoEntidad()));
                $this->contenido=array('location'=>'_solicitar_aprobacion_transferencia',
                    'params'=>array('mensaje'=>$this->mensaje,'origen'=>$event->params['solicitud']->getEntidadOrigen(),'destino'=>$event->params['solicitud']->getEntidadDestino(),'solicitud'=>$event->params['solicitud'],'usuario'=>$event->params['usuario']));
                break;
            //TODO: terminar esta notificacion
            case 'contestar_solicitud_transferencia':
                $this->mensaje->asunto='Solicitud de transferencia de fondos '.$event->params['accion'].' en '.$event->params['solicitud']->getEntidadDestino()->getTipoEntidad().' '.$event->params['solicitud']->getEntidadDestino()->getNombre();
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['solicitud']->entidad_id_destino,strtolower($event->params['solicitud']->getEntidadDestino()->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_contestar_solicitud_transferencia',
                    'params'=>array('accion'=>$event->params['accion'],'mensaje'=>$this->mensaje,'origen'=>$event->params['solicitud']->getEntidadOrigen(),'destino'=>$event->params['solicitud']->getEntidadDestino(),'solicitud'=>$event->params['solicitud']));
                break;
            case 'solicitar_presupuesto':
                $this->mensaje->asunto='Solicitud de aprobaci&oacute;n de presupuesto en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion('presupuesto');
                $this->mensaje->id_tarea=$event->params['presupuesto']->id;
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_solicitar_presupuesto',
                    'params'=>array('mensaje'=>$this->mensaje,'entidad'=>$event->params['entidad'],
                    'presupuesto'=>$event->params['presupuesto'],'encargado'=>$event->params['encargado']));
                break;
            case 'aceptar_presupuesto':
                $this->mensaje->asunto='Presupuesto aceptado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['presupuesto']->id_usuario_que_envia);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_aceptar_presupuesto',
                    'params'=>array('entidad'=>$event->params['entidad'],'presupuesto'=>$event->params['presupuesto']));
                break;
            case 'responsable_presupuesto':
                $this->mensaje->asunto="Presupuesto aceptado en cuenta {$event->params['presupuesto']->cuenta->numero}";
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['responsable']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_responsable_presupuesto',
                    'params'=>array('entidad'=>$event->params['entidad'],'presupuesto'=>$event->params['presupuesto']));
                break;
            case 'rechazar_presupuesto':
                $this->mensaje->asunto='Presupuesto rechazado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['presupuesto']->id_usuario_que_envia);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_rechazar_presupuesto',
                    'params'=>array('entidad'=>$event->params['entidad'],'presupuesto'=>$event->params['presupuesto']));
                break;
            case 'solicitar_recibo':
                $this->mensaje->asunto='Solicitud de aprobaci&oacute;n de '.$event->params['tipoRecibo'].' en '.
                    $event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion($event->params['tipoRecibo']);
                $this->mensaje->id_tarea=$event->params['recibo']->id;
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_solicitar_recibo',
                    'params'=>array('mensaje'=>$this->mensaje,'entidad'=>$event->params['entidad'],
                        'recibo'=>$event->params['recibo'],'tipo'=>$event->params['tipoRecibo']));
                break;
            case 'aceptar_recibo':
                $this->mensaje->asunto="Formulario {$event->params['recibo']->tipoRecibo} aceptado en
                    {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['recibo']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_aceptar_recibo',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['recibo']));
                break;
            case 'rechazar_recibo':
                $this->mensaje->asunto="Formulario {$event->params['recibo']->tipoRecibo} rechazado en
                    {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['recibo']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_rechazar_recibo',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['recibo']));
                break;
            case 'notificar_entrega_retiro':
                $this->mensaje->asunto="Solicitud de aceptaci&oacute;n de retiro de fondos en {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setSuscriber(1);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_notificar_entrega_retiro',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['retiro']));
                break;
            case 'notificar_recibo_retiro':
                $this->mensaje->asunto="Retiro de fondos aceptado en {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['retiro']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->setAccion('answered_as_read');
                $this->contenido=array('location'=>'_notificar_recibo_retiro',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['retiro']));
                break;
            case 'notificar_entrega_reintegracion':
                $this->mensaje->asunto="Notificar recibo de comprobante reintegraci&oacute;n de gastos en {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setSuscriber(1);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_notificar_entrega_reintegracion',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['recibo']));
                break;
            case 'notificar_recibo_reintegracion':
                $this->mensaje->asunto="{$event->params['recibo']->tipoRecibo} aceptado en {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['recibo']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_notificar_recibo_reintegracion',
                    'params'=>array('entidad'=>$event->params['entidad'],'recibo'=>$event->params['recibo']));
                break;
            case 'solicitar_pago_proveedor':
                $this->mensaje->asunto="Solicitud de {$event->params['pago']->tipoRecibo} en {$event->params['entidad']->getTipoEntidad()} {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setSuscriber(1);
                $this->setAccion('pago_proveedor');
                $this->mensaje->id_tarea=$event->params['pago']->id;
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_solicitar_pago_proveedor',
                    'params'=>array('entidad'=>$event->params['entidad'],'pago'=>$event->params['pago']));
                break;
            case 'aceptar_pago_proveedor':
                $this->mensaje->asunto='Formulario de '.$event->params['pago']->tipoRecibo.' aceptado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['pago']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_aceptar_pago_proveedor',
                    'params'=>array('entidad'=>$event->params['entidad'],'pago'=>$event->params['pago']));
                break;
            case 'rechazar_pago_proveedor':
                $this->mensaje->asunto='Formulario de '.$event->params['pago']->tipoRecibo.' rechazado en '.$event->params['entidad']->getTipoEntidad().' '.$event->params['entidad']->getNombre();
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['pago']->id_usuario_que_solicita);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_rechazar_pago_proveedor',
                    'params'=>array('entidad'=>$event->params['entidad'],'pago'=>$event->params['pago']));
                break;
            case 'cotizacion_subida':
                $this->mensaje->asunto="Cotizaci&oacute;n de gastos de env&iacute;o subida en {$event->params['entidad']->getNombre()}";
                $this->setPublisher(1);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['presupuesto']->id_usuario_que_envia);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_cotizacion_subida',
                    'params'=>array('entidad'=>$event->params['entidad'],'presupuesto'=>$event->params['presupuesto']));
                break;
            case 'subir_factura':
                $this->mensaje->asunto="Subir factura para unidad de abastecimiento en {$event->params['entidad']->getNombre()}";
                $this->setPublisher(user()->id);
                $this->setAccion('answered_as_read');
                $this->setSuscriber(1);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_subir_factura',
                    'params'=>array('entidad'=>$event->params['entidad'],'presupuesto'=>$event->params['presupuesto']));
                break;
            case 'unidad_abastecimiento_aprobada':
                $this->mensaje->asunto="Formulario de unidad de abastecimiento aprobado en {$event->params['entidad']->getNombre()}";
                $this->setPublisher(1);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_unidad_abastecimiento_aprobada',
                    'params'=>array('entidad'=>$event->params['entidad'],'pago'=>$event->params['pago']));
                break;
            case 'solicitud_equipo_trabajo':
                $nombre=truncate("Solicitud de integrante de proyecto {$event->params['proyecto']->nombre}",100);
                $this->mensaje->asunto=$nombre;
                $this->setPublisher(user()->id);
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_solicitud_equipo_trabajo',
                    'params'=>array('proyecto'=>$event->params['proyecto'],'integrante'=>$event->params['integrante'],'mensaje'=>$this->mensaje));
                break;
            case 'envio_proyecto_postulacion':
                $nombre=truncate("Se ha enviado a revision para fondos IDI tu proyecto {$event->params['proyecto']->nombre}",100);
                $this->mensaje->asunto=$nombre;
                $this->setPublisher(1);
                $this->setAccion('answered_as_read');
                $this->setSuscriber($event->params['destinatario']);
                $this->setRelation($event->params['entidad']->getId(),strtolower($event->params['entidad']->getTipoEntidad()));
                $this->contenido=array('location'=>'_envio_proyecto_postulacion',
                    'params'=>array('proyecto'=>$event->params['proyecto'],'mensaje'=>$this->mensaje));
                break;
        }

        $this->enviarNotificacion();
    }

    /**
     * Funcion que envia la notificacion creada en el handler notificationHandler. Este metodo es utilizado internamente
     * por el mailman.
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-08-12
     */
    private function enviarNotificacion()
    {
        //lo primero, se debe truncar el asunto del mensaje a 100 caracteres
        $this->mensaje->asunto=truncate($this->mensaje->asunto,100);
        foreach ($this->suscribers as $elem) {
            $this->mensaje->destinatario = $elem;
            $this->mensaje->uid=uniqid('',true);
            //agrego el contenido para cada suscriptor, en este punto se pueden agregar mas parametros al view
            $this->contenido['params']['destinatario']=$elem;
            $this->contenido['params']['uid']=$this->mensaje->uid;
            $html=app()->controller->renderPartial('application.views.notificaciones_mailman.'.$this->contenido['location'],
                $this->contenido['params'],true);
            $this->mensaje->contenido=$html;
            $this->mensaje->save();
            $this->enviarNotificacionPorMail($this->mensaje->destinatario,$this->mensaje->asunto,$html);
            //lanzo el evento de que el mensaje fue enviado
            $this->onMensajeEnviado(new CEvent($this,array('mensaje'=>$this->mensaje,'tipo'=>$this->tipo_notificacion)));
            //fin lanzamiento eventos
        }
        //lo destruyo para evitar ambiguedades
        $this->mensaje = null;
    }

    /**
     * Funcion que envia una notificacion por email. Es utilizada internamente por el mailman.
     *
     * @param int $id_usuario ID del usuario al que se desea enviar el email
     * @param string $asunto Asunto del mensaje
     * @param string $contenido Contenido del mensaje
     *
     * @return  void
     * @throws  \Exception
     *
     * @since   2012-08-12
     */
    public function enviarNotificacionPorMail($id_usuario,$asunto, $contenido)
    {
        $flag = false;
        $message = new YiiMailMessage;
        $message->setBody($contenido, 'text/html');
        $message->subject = $asunto;
        $mail = Persona::model()->findByPk($id_usuario);
        $data = Yii::app()->db->createCommand('SELECT email AS email FROM {{user}} WHERE id=' . $id_usuario)->queryColumn();
        $data2 = Yii::app()->db->createCommand('SELECT recibir_email AS recibir_email FROM {{perfil_usuario}} WHERE user_id=' . $id_usuario)->queryColumn();
        if ($data2[0] == 1) {
            $message->addTo($data[0]);
            $flag = true;
        }
        $message->from = array('piea@usm.cl' => 'PIE>A::Gestion I+D+i');
        if ($flag)
            Yii::app()->mail->send($message);
    }

    /**
     * Evento ejecutado luego de enviar una notificacion a un mensaje.
     *
     * @param \CEvent $event Evento raised
     *
     * @return  void
     *
     * @since   2012-08-12
     */
    public function onMensajeEnviado($event){
        $this->raiseEvent('onMensajeEnviado', $event);
    }

}