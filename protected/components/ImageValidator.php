<?php
/**
 * ImageValidator permite validar variados parametros y propiedades de un archivo de imagen.
 *
 * Este validador permite a partir de una imagen subida mediante CUploadedFile realizar distintas
 * validaciones de propiedades que debe potencialmente cumplir la imagen.
 *
 * @author dacuna <diego.acuna@usm.cl>
 * @version 1.0
 * @package components
 * @since 1.0
 * @todo Finalizar validador permitiendo setear los mensajes de error y validar tambien si el archivo es una imagen o no.
 */
class ImageValidator extends CValidator
{
    /**
     * Si es que el atributo a ser validado requiere poseer un valor o no. Por defecto es falso, es decir, si se
     * requiere que el atributo sea llenado con un valor.
     * @var boolean
     */
    public $allowEmpty = false;

    /**
     * @var string the error message used when the uploaded file has an extension name
     * that is not listed among {@link extensions}.
     */
    public $tooMany;

    public $minHeight;
    public $minWidth;
    public $maxHeight;
    public $maxWidth;
    public $square = false;

    /**
     * Set the attribute and then validates using {@link validateFile}.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object, $attribute)
    {
        $file = $object->$attribute;
        if (!$file instanceof CUploadedFile) {
            $file = CUploadedFile::getInstance($object, $attribute);
            if (null === $file)
                return $this->emptyAttribute($object, $attribute);
        }

        $image = Yii::app()->image->load($file->tempName);

        /** verify if image is on range minWidth and minHeight **/
        if ($this->minWidth > $image->width || $this->minHeight > $image->height) {
            $message = 'La imagen a subir debe tener como m&iacute;nimo las dimensiones de ' . $this->minWidth . 'x' . $this->minHeight . 'px';
            $this->addError($object, $attribute, $message);
        }
        /** verify if image is on range maxWidth and maxHeight **/
        if ($this->maxWidth < $image->width || $this->maxHeight < $image->height) {
            $message = 'La imagen a subir debe tener como m&aacute;ximo las dimensiones de ' . $this->maxWidth . 'x' . $this->maxHeight . 'px';
            $this->addError($object, $attribute, $message);
        }
        /** verify geometric proportion of image (if square is set to true) **/
        if ($this->square && ($image->width != $image->height)) {
            $message = 'La imagen a subir debe ser cuadrada';
            $this->addError($object, $attribute, $message);
        }
    }

    /**
     * Raises an error to inform end user about blank attribute.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function emptyAttribute($object, $attribute)
    {
        if (!$this->allowEmpty) {
            $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} cannot be blank.');
            $this->addError($object, $attribute, $message);
        }
    }

}