<?php
/**
 * Clase que define un controlador personalizado para el sistema.
 *
 * Los controladores creados para el sistema, deben heredar de la clase {@link CController}. de Yii, pero
 * para una mayor personalizacion se ha creado esta clase que hereda de {@link CController} configurando
 * algunos parametros de uso comun en los controladores del sistema. Cada controlador creado debe
 * heredar de esta clase.
 *
 * @example <br />
 * class MyController extends Controller
 * {
 *  ...
 * }
 * @version   1.0
 * @package components
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Controller extends CController
{
    /**
     * Layout a utilizar por defecto en las vistas de los controladores. Los layouts disponibles
     * se encuentran en la carpeta protected/views/layouts. Por defecto se utiliza el layout a
     * dos columnas.
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Menu contextual. Puede ser cambiado en cada accion del controlador segun se necesite (en general
     * no es necesario utilizar esta propiedad). El valor sera linkeado a un elemento de tipo {@link CMenu::items}.
     * @var array
     */
    public $menu = array();

    /**
     * El contenido del widget breadcrumbs de cada vista. El valor que sea asignado a esta variable sera
     * posteriormente asignado a {@link CBreadcrumbs::links}. Ver {@link CBreadcrumbs::links} para mas
     * detalles sobre como se debe utilizar esta propiedad.
     * @var array
     */
    public $breadcrumbs = array();

    /**
     * Permite crear un modal en la vista que lo estime necesario. Solo se necesita asignar los siguientes pares
     * llave-valor en la vista que necesite el modal: id=>'myId' ID del elemento div que sera el container para el
     * contenido del modal, 'size'=>'xlarge' tamano del modal, 'content'=>'...' contenido del modal, puede ser por
     * ejemplo un partial renderizado con renderPartial(). El modal es el widget del framework css Foundation 3.0.
     * En este array puede agregarse cualquier propiedad que sea admisible en el widget modal de foundation.
     * @var string
     */
    public $modal = array();

    /**
     * Si se desea inicializar un modulo o controlador para AngularJS en el decorator de la vista (es decir, en el
     * codigo html del layout superior a la vista que se esta renderizando) entonces se debe setear esta variable
     * con la informacion necesaria. Por ejemplo: $this->angular=' ng-app="milestoneModule"'; hara que se ejecute el
     * contexto del modulo de angular en el arbol html del layout del controlador (que esta por sobre la vista ejecutada).
     * @var string
     */
    public $angular = null;

    /**
     * Retorna la data pasado como parametro como una respuesta JSON.
     *
     * @param array $data
     */
    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }
}