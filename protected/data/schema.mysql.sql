SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `sistemagestion` DEFAULT CHARACTER SET latin1 ;
USE `sistemagestion` ;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_AuthItem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_AuthItem` (
  `name` VARCHAR(64) NOT NULL ,
  `type` INT(11) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_AuthAssignment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_AuthAssignment` (
  `itemname` VARCHAR(64) NOT NULL ,
  `userid` VARCHAR(64) NOT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`itemname`, `userid`) ,
  CONSTRAINT `tbl_authassignment_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `sistemagestion`.`tbl_AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_AuthItemChild`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_AuthItemChild` (
  `parent` VARCHAR(64) NOT NULL ,
  `child` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`parent`, `child`) ,
  INDEX `child` (`child` ASC) ,
  CONSTRAINT `tbl_authitemchild_ibfk_1`
    FOREIGN KEY (`parent` )
    REFERENCES `sistemagestion`.`tbl_AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `tbl_authitemchild_ibfk_2`
    FOREIGN KEY (`child` )
    REFERENCES `sistemagestion`.`tbl_AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_iniciativa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_iniciativa` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `rol_piea` VARCHAR(10) NOT NULL ,
  `nombre_completo` VARCHAR(255) NOT NULL ,
  `nombre_abreviado` VARCHAR(45) NOT NULL ,
  `sigla` VARCHAR(10) NOT NULL ,
  `estatutos` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `descripcion` TEXT NULL DEFAULT NULL ,
  `objetivos_generales` TEXT NULL DEFAULT NULL ,
  `objetivos_especificos` TEXT NULL DEFAULT NULL ,
  `mision` TEXT NULL DEFAULT NULL ,
  `vision` TEXT NULL DEFAULT NULL ,
  `campus` VARCHAR(40) NULL DEFAULT NULL ,
  `telefono` VARCHAR(15) NULL DEFAULT NULL ,
  `logo` VARCHAR(40) NULL DEFAULT NULL ,
  `foto_equipo` VARCHAR(40) NULL DEFAULT NULL ,
  `fecha_creacion` DATE NOT NULL ,
  `fecha_ingreso` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(50) NOT NULL ,
  `nombres` VARCHAR(45) NOT NULL ,
  `apellido_paterno` VARCHAR(20) NOT NULL ,
  `apellido_materno` VARCHAR(20) NOT NULL ,
  `rol_piea` VARCHAR(10) NOT NULL ,
  `fecha_registro` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  `token` VARCHAR(36) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_integrante_iniciativa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_integrante_iniciativa` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `iniciativa_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `visibilidad` INT(11) NOT NULL DEFAULT '1' COMMENT 'El campo cargo hace referencia a si es administrador o no. 0 si es admin, otro valor no es admin. Se puede utilizar para alguna otra cosa si es que el valor es 0.' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_integrante_iniciativa_tbl_iniciativa1` (`iniciativa_id` ASC) ,
  INDEX `fk_tbl_integrante_iniciativa_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_integrante_iniciativa_tbl_iniciativa1`
    FOREIGN KEY (`iniciativa_id` )
    REFERENCES `sistemagestion`.`tbl_iniciativa` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_integrante_iniciativa_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `iniciativa_id` INT(11) NULL ,
  `nombre` VARCHAR(255) NOT NULL ,
  `rol_piea` VARCHAR(10) NOT NULL ,
  `sigla` VARCHAR(10) NULL ,
  `descripcion` TEXT NULL ,
  `campus` VARCHAR(40) NULL ,
  `objetivos_generales` TEXT NULL DEFAULT NULL ,
  `objetivos_especificos` TEXT NULL DEFAULT NULL ,
  `logo` VARCHAR(40) NULL DEFAULT NULL ,
  `foto_equipo` VARCHAR(40) NULL DEFAULT NULL ,
  `fecha_inicio` DATE NULL ,
  `fecha_ingreso` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  `es_postulacion` INT NOT NULL DEFAULT 0 ,
  `estado_postulacion` INT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_proyecto_tbl_iniciativa1` (`iniciativa_id` ASC) ,
  CONSTRAINT `fk_tbl_proyecto_tbl_iniciativa1`
    FOREIGN KEY (`iniciativa_id` )
    REFERENCES `sistemagestion`.`tbl_iniciativa` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_integrante_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_integrante_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `proyecto_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `visibilidad` INT(11) NOT NULL DEFAULT 1 ,
  `estado` INT NOT NULL DEFAULT 1 ,
  `lider` INT(1) NOT NULL DEFAULT 0 ,
  `finanzas` INT(1) NOT NULL DEFAULT 0 ,
  `responsabilidades` VARCHAR(100) NULL DEFAULT NULL ,
  `id_mensaje` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_integrante_proyecto_tbl_proyecto1` (`proyecto_id` ASC) ,
  INDEX `fk_tbl_integrante_proyecto_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_integrante_proyecto_tbl_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_integrante_proyecto_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_mensaje`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_mensaje` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `uid` VARCHAR(23) NOT NULL ,
  `asunto` VARCHAR(100) NOT NULL ,
  `contenido` TEXT NOT NULL ,
  `emisor` INT(11) NOT NULL ,
  `destinatario` INT(11) NOT NULL ,
  `leido` TINYINT(4) NOT NULL DEFAULT '0' ,
  `contestado` TINYINT(4) NOT NULL DEFAULT '0' ,
  `fecha_envio` DATETIME NOT NULL ,
  `id_relacion` INT(11) NULL DEFAULT NULL ,
  `tipo_relacion` VARCHAR(45) NULL DEFAULT NULL ,
  `id_tarea` INT(11) NULL DEFAULT NULL ,
  `accion` VARCHAR(45) NULL DEFAULT NULL ,
  `accion_realizada` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Corresponde a la accion que se realizo en el mensaje, por ejemplo \"aceptar\" o \"rechazar\".' ,
  PRIMARY KEY (`id`) ,
  INDEX `get_message` (`uid` ASC, `emisor` ASC, `destinatario` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_perfil_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_perfil_usuario` (
  `user_id` INT(11) NOT NULL ,
  `rut` VARCHAR(10) NULL DEFAULT NULL ,
  `rol` VARCHAR(10) NULL DEFAULT NULL ,
  `pasaporte` VARCHAR(45) NULL DEFAULT NULL ,
  `fecha_nacimiento` DATE NOT NULL ,
  `telefono_contacto` VARCHAR(12) NOT NULL ,
  `direccion_laboral` VARCHAR(45) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `contacto_emergencia` VARCHAR(45) NULL DEFAULT NULL ,
  `avatar` VARCHAR(45) NULL DEFAULT NULL ,
  `recibir_email` INT NOT NULL DEFAULT 0 ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`user_id`) ,
  INDEX `fk_tbl_perfil_usuario_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_perfil_usuario_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_rol_piea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_rol_piea` (
  `ano_actual` INT(10) NOT NULL ,
  `siguiente_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ano_actual`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_status_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_status_usuario` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(25) NOT NULL ,
  `descripcion` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_usuario_has_status`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_usuario_has_status` (
  `user_id` INT(11) NOT NULL ,
  `status_usuario_id` INT(10) UNSIGNED NOT NULL ,
  `es_interno` INT(11) NULL DEFAULT NULL ,
  `empresa` VARCHAR(45) NULL DEFAULT NULL ,
  `ano_egreso` INT(11) NULL DEFAULT NULL ,
  `cargo` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`user_id`, `status_usuario_id`) ,
  INDEX `fk_tbl_user_has_tbl_status_usuario_tbl_status_usuario1` (`status_usuario_id` ASC) ,
  INDEX `fk_tbl_user_has_tbl_status_usuario_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_user_has_tbl_status_usuario_tbl_status_usuario1`
    FOREIGN KEY (`status_usuario_id` )
    REFERENCES `sistemagestion`.`tbl_status_usuario` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_user_has_tbl_status_usuario_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_lookup_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_lookup_proyecto` (
  `nombre_item` VARCHAR(64) NOT NULL ,
  `proyecto_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `activo` INT(1) NOT NULL DEFAULT 1 ,
  `es_cargo` INT NOT NULL DEFAULT 0 ,
  `asignacion_nula` INT NOT NULL DEFAULT 0 COMMENT 'Corresponde cuando se asigna el cargo a un usuario nulo (cargo queda pendiente).' ,
  `cargo_que_asigno` VARCHAR(64) NULL DEFAULT NULL ,
  PRIMARY KEY (`nombre_item`, `proyecto_id`, `user_id`) ,
  INDEX `fk_tbl_lookup_proyecto_tbl_proyecto1` (`proyecto_id` ASC) ,
  INDEX `fk_tbl_lookup_proyecto_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_lookup_proyecto_tbl_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_lookup_proyecto_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_lookup_iniciativa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_lookup_iniciativa` (
  `nombre_item` VARCHAR(64) NOT NULL ,
  `iniciativa_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `activo` INT(1) NOT NULL DEFAULT 1 ,
  `es_cargo` INT NOT NULL DEFAULT 0 ,
  `asignacion_nula` INT NOT NULL DEFAULT 0 COMMENT 'Corresponde cuando se asigna el cargo a un usuario nulo (cargo queda pendiente).' ,
  `cargo_que_asigno` VARCHAR(64) NULL DEFAULT NULL ,
  PRIMARY KEY (`nombre_item`, `iniciativa_id`, `user_id`) ,
  INDEX `fk_tbl_lookup_iniciativa_tbl_iniciativa1` (`iniciativa_id` ASC) ,
  INDEX `fk_tbl_lookup_iniciativa_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_lookup_iniciativa_tbl_iniciativa1`
    FOREIGN KEY (`iniciativa_id` )
    REFERENCES `sistemagestion`.`tbl_iniciativa` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_lookup_iniciativa_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_invitacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_invitacion` (
  `mensaje_id` INT(11) NOT NULL ,
  `usuario_id` INT(11) NULL ,
  `usuario_invito_id` INT(11) NOT NULL ,
  `entidad_id` INT(11) NOT NULL ,
  `tipo_entidad` INT NOT NULL ,
  `tipo_invitacion` VARCHAR(35) NOT NULL ,
  `aceptada` INT NOT NULL ,
  `fecha_invitacion` DATETIME NOT NULL ,
  `fecha_aceptada` DATETIME NULL ,
  PRIMARY KEY (`mensaje_id`) ,
  INDEX `fk_tbl_invitacion_tbl_user1` (`usuario_id` ASC) ,
  INDEX `fk_tbl_invitacion_tbl_mensaje1` (`mensaje_id` ASC) ,
  INDEX `tipo_invitacion_index` (`tipo_invitacion` ASC, `entidad_id` ASC, `tipo_entidad` ASC) ,
  CONSTRAINT `fk_tbl_invitacion_tbl_user1`
    FOREIGN KEY (`usuario_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_invitacion_tbl_mensaje1`
    FOREIGN KEY (`mensaje_id` )
    REFERENCES `sistemagestion`.`tbl_mensaje` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_invitacion_externa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_invitacion_externa` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `usuario_invita_id` INT(11) NULL ,
  `email_invitado` VARCHAR(45) NOT NULL ,
  `nombre_invitado` VARCHAR(45) NOT NULL ,
  `entidad_id` INT(11) NOT NULL ,
  `tipo_entidad` VARCHAR(12) NOT NULL ,
  `aceptada` INT NOT NULL ,
  `salt` VARCHAR(45) NOT NULL ,
  `fecha_invitacion` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_invitacion_externa_tbl_user1` (`usuario_invita_id` ASC) ,
  CONSTRAINT `fk_tbl_invitacion_externa_tbl_user1`
    FOREIGN KEY (`usuario_invita_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_tarea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_tarea` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `entidad_id` INT(11) NOT NULL ,
  `tipo_entidad` VARCHAR(10) NOT NULL ,
  `categoria_id` INT(11) NOT NULL ,
  `clasificacion` VARCHAR(45) NOT NULL ,
  `titulo` VARCHAR(45) NOT NULL ,
  `fecha_inicio` DATE NOT NULL ,
  `fecha_termino` DATE NOT NULL ,
  `duracion` VARCHAR(5) NOT NULL ,
  `aceptada` INT(11) NOT NULL DEFAULT '0' ,
  `aprobada` INT(11) NOT NULL DEFAULT '0' ,
  `publica` INT(2) NOT NULL DEFAULT '0' ,
  `descripcion` TEXT NULL DEFAULT NULL ,
  `fecha_ingreso` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_participante_tarea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_participante_tarea` (
  `tarea_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `aprobada` INT NOT NULL DEFAULT 0 ,
  INDEX `fk_tbl_participante_tarea_tbl_tarea1` (`tarea_id` ASC) ,
  INDEX `fk_tbl_participante_tarea_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_participante_tarea_tbl_tarea1`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_participante_tarea_tbl_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_categoria_actividad`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_categoria_actividad` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `id_padre` INT NOT NULL ,
  `nombre` VARCHAR(45) NOT NULL ,
  `nivel` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `padre` (`id_padre` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_categoria_actividad_has_tarea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_categoria_actividad_has_tarea` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `categoria_actividad_id` INT(11) NOT NULL ,
  `tarea_id` INT(11) NOT NULL ,
  `titulo` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_tarea1` (`tarea_id` ASC) ,
  INDEX `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_categoria_activi1` (`categoria_actividad_id` ASC) ,
  CONSTRAINT `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_categoria_activi1`
    FOREIGN KEY (`categoria_actividad_id` )
    REFERENCES `sistemagestion`.`tbl_categoria_actividad` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_tarea1`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_tarea_tmp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_tarea_tmp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `editor` INT(11) NOT NULL ,
  `entidad_id` INT(11) NULL DEFAULT NULL ,
  `tipo_entidad` VARCHAR(10) NULL DEFAULT NULL ,
  `categoria_id` INT(11) NULL DEFAULT NULL ,
  `titulo` VARCHAR(45) NULL DEFAULT NULL ,
  `fecha_inicio` DATE NULL DEFAULT NULL ,
  `fecha_termino` DATE NULL DEFAULT NULL ,
  `duracion` VARCHAR(4) NULL DEFAULT NULL ,
  `aceptada` INT(11) NULL DEFAULT '0' ,
  `descripcion` TEXT NULL DEFAULT NULL ,
  `fecha_ingreso` DATETIME NULL DEFAULT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_tarea_tmp_tbl_user1` (`editor` ASC) ,
  CONSTRAINT `fk_tbl_tarea_tmp_tbl_user1`
    FOREIGN KEY (`editor` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_participante_tarea_tmp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_participante_tarea_tmp` (
  `tarea_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `aprobada` INT NOT NULL DEFAULT 0 ,
  INDEX `fk_tbl_participante_tarea_tbl_tarea1` (`tarea_id` ASC) ,
  INDEX `fk_tbl_participante_tarea_tbl_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_tbl_participante_tarea_tbl_tarea_tmp`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea_tmp` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_participante_tarea_tmp_tbl_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_categoria_actividad_has_tarea_tmp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_categoria_actividad_has_tarea_tmp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `categoria_actividad_id` INT(11) NOT NULL ,
  `tarea_id` INT(11) NOT NULL ,
  `titulo` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_tarea1` (`tarea_id` ASC) ,
  INDEX `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_categoria_activi1` (`categoria_actividad_id` ASC) ,
  CONSTRAINT `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_categoria_activi`
    FOREIGN KEY (`categoria_actividad_id` )
    REFERENCES `sistemagestion`.`tbl_categoria_actividad` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_categoria_actividad_has_tbl_tarea_tbl_tarea10`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea_tmp` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_tarea_archivo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_tarea_archivo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `tarea_id` INT(11) NOT NULL ,
  `archivo` VARCHAR(100) NOT NULL ,
  `descripcion` VARCHAR(150) NULL DEFAULT NULL ,
  INDEX `fk_tbl_tarea_archivo_tbl_tarea1` (`tarea_id` ASC) ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_tbl_tarea_archivo_tbl_tarea1`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_tarea_archivo_tmp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_tarea_archivo_tmp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `tarea_id` INT(11) NOT NULL ,
  `archivo` VARCHAR(100) NOT NULL ,
  `descripcion` VARCHAR(150) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_tarea_archivo_tmp_tbl_tarea_tmp1` (`tarea_id` ASC) ,
  CONSTRAINT `fk_tbl_tarea_archivo_tmp_tbl_tarea_tmp1`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `sistemagestion`.`tbl_tarea_tmp` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_delegacion_tarea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_delegacion_tarea` (
  `id_iniciativa` INT(11) NOT NULL ,
  `id_tarea` INT(11) NOT NULL ,
  PRIMARY KEY (`id_iniciativa`, `id_tarea`) ,
  INDEX `iniciativa_delegada` (`id_iniciativa` ASC) ,
  INDEX `tarea_delegada` (`id_tarea` ASC) ,
  CONSTRAINT `iniciativa_delegada`
    FOREIGN KEY (`id_iniciativa` )
    REFERENCES `sistemagestion`.`tbl_iniciativa` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `tarea_delegada`
    FOREIGN KEY (`id_tarea` )
    REFERENCES `sistemagestion`.`tbl_tarea` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_cuenta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_cuenta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_entidad` INT(11) NOT NULL ,
  `tipo_entidad` INT(1) NOT NULL ,
  `nombre` VARCHAR(45) NOT NULL ,
  `numero` VARCHAR(20) NOT NULL ,
  `propietario` VARCHAR(45) NOT NULL ,
  `cargo_propietario` VARCHAR(45) NOT NULL ,
  `saldo` INT(11) NOT NULL DEFAULT 0 ,
  `fecha_creacion` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NOT NULL ,
  `encargado` INT(11) NULL DEFAULT NULL ,
  `descripcion` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_entrega`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_entrega` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_cuenta` INT(11) NOT NULL ,
  `id_entidad` INT(11) NOT NULL COMMENT 'Corresponde a la entidad de destino. No se llama id_entidad_destino solo por motivos de compatibilidad con versiones anteriores.' ,
  `tipo_entidad` INT(1) NOT NULL ,
  `id_entidad_origen` INT(11) NOT NULL ,
  `tipo_entidad_origen` INT NOT NULL ,
  `id_operador` INT(11) NOT NULL ,
  `monto` INT(11) NOT NULL ,
  `saldo` INT(11) NOT NULL ,
  `fecha_entrega` DATETIME NOT NULL ,
  `saldo_cuenta_origen` VARCHAR(45) NOT NULL COMMENT 'Se debe mantener un registro del saldo historico en el momento de la entrega (transferencia para fines de sistema). Calcular este valor deduciendolo de las entregas y la cuenta es demasiado trabajo.' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_entrega_1_tbl_cuenta` (`id_cuenta` ASC) ,
  CONSTRAINT `fk_tbl_entrega_1_tbl_cuenta`
    FOREIGN KEY (`id_cuenta` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_transferencia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_transferencia` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_cuenta_origen` INT(11) NOT NULL ,
  `id_cuenta_destino` INT(11) NOT NULL ,
  `monto` INT(11) NOT NULL ,
  `id_entidad_origen` INT(11) NOT NULL ,
  `tipo_entidad_origen` INT NOT NULL ,
  `id_entidad_destino` INT(11) NOT NULL ,
  `tipo_entidad_destino` INT NOT NULL ,
  `id_operador` INT(11) NOT NULL ,
  `fecha_transferencia` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_transferencia_1` (`id_cuenta_origen` ASC) ,
  INDEX `fk_tbl_transferencia_2` (`id_cuenta_destino` ASC) ,
  CONSTRAINT `fk_tbl_transferencia_1`
    FOREIGN KEY (`id_cuenta_origen` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_transferencia_2`
    FOREIGN KEY (`id_cuenta_destino` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_solicitud_transferencia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_solicitud_transferencia` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `entidad_id_origen` INT(11) NOT NULL ,
  `entidad_id_destino` INT(11) NOT NULL ,
  `tipo_entidad_origen` INT NOT NULL ,
  `tipo_entidad_destino` INT NOT NULL ,
  `partida_id_origen` INT(11) NOT NULL ,
  `tipo_partida` INT NOT NULL ,
  `cuenta_id_destino` INT(11) NOT NULL ,
  `tipo_cuenta_destino` INT NOT NULL ,
  `estado` INT NOT NULL DEFAULT 0 ,
  `monto` INT(11) NOT NULL ,
  `fecha_solicitud` DATETIME NOT NULL ,
  `fecha_accion_realizada` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_presupuesto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_usuario_que_envia` INT(11) NOT NULL ,
  `id_entidad` INT(11) NOT NULL ,
  `tipo_entidad` INT NOT NULL ,
  `detalle` TEXT NOT NULL ,
  `estado` INT NOT NULL DEFAULT 0 ,
  `fecha_solicitud` DATETIME NOT NULL ,
  `fecha_accion_realizada` DATETIME NULL DEFAULT NULL ,
  `id_cuenta` INT(11) NULL DEFAULT NULL ,
  `unidad_de_abastecimiento` INT(11) NULL DEFAULT NULL COMMENT 'Si es que el presupuesto es para un formulario de unidad de abastecimiento.' ,
  `comentario_revisor` TEXT NULL DEFAULT NULL ,
  `cotizacion_gastos_envio` INT NULL ,
  `cotizacion_solicitada` INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_presupuesto_1` (`id_cuenta` ASC) ,
  INDEX `fk_tbl_presupuesto_2` (`unidad_de_abastecimiento` ASC) ,
  CONSTRAINT `fk_tbl_presupuesto_1`
    FOREIGN KEY (`id_cuenta` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_presupuesto_2`
    FOREIGN KEY (`unidad_de_abastecimiento` )
    REFERENCES `sistemagestion`.`tbl_pago_proveedores` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_item_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_item_presupuesto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_presupuesto` INT(11) NOT NULL ,
  `nombre` VARCHAR(45) NOT NULL ,
  `precio` INT NOT NULL ,
  `unidades` INT NOT NULL ,
  `prefix_documentos` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_item_presupuesto_1` (`id_presupuesto` ASC) ,
  CONSTRAINT `fk_tbl_item_presupuesto_1`
    FOREIGN KEY (`id_presupuesto` )
    REFERENCES `sistemagestion`.`tbl_presupuesto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_recibo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_recibo` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `codigo_presupuestario` INT(11) NOT NULL ,
  `unidad_presupuestaria` VARCHAR(45) NOT NULL DEFAULT 'DIRECCION GENERAL DE DOCENCIA' ,
  `id_deudor` INT(11) NOT NULL ,
  `id_usuario_que_solicita` INT(11) NOT NULL ,
  `id_entidad` INT(11) NOT NULL ,
  `tipo_entidad` INT NOT NULL ,
  `monto_vale` INT NOT NULL DEFAULT 0 ,
  `fecha_vale` DATETIME NOT NULL ,
  `nota` TEXT NOT NULL ,
  `total_gastos` INT NOT NULL DEFAULT 0 ,
  `saldo_favor_deudor` INT NOT NULL DEFAULT 0 ,
  `saldo_reintegrar_caja` INT NOT NULL DEFAULT 0 ,
  `total_igual` INT NOT NULL DEFAULT 0 ,
  `tipo_recibo` INT NOT NULL DEFAULT 0 ,
  `estado` INT NOT NULL DEFAULT 0 ,
  `comentario_revisor` TEXT NULL DEFAULT NULL ,
  `fecha_emision_documento` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_recibo_1` (`codigo_presupuestario` ASC) ,
  INDEX `fk_tbl_recibo_2` (`id_deudor` ASC) ,
  INDEX `fk_tbl_recibo_3` (`id_usuario_que_solicita` ASC) ,
  CONSTRAINT `fk_tbl_recibo_1`
    FOREIGN KEY (`codigo_presupuestario` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_recibo_2`
    FOREIGN KEY (`id_deudor` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_recibo_3`
    FOREIGN KEY (`id_usuario_que_solicita` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_item_recibo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_item_recibo` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `id_recibo` INT(11) NOT NULL ,
  `fecha` DATE NOT NULL ,
  `numero_boleta` VARCHAR(45) NOT NULL ,
  `valor` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_item_recibo_1` (`id_recibo` ASC) ,
  CONSTRAINT `fk_tbl_item_recibo_1`
    FOREIGN KEY (`id_recibo` )
    REFERENCES `sistemagestion`.`tbl_recibo` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_item_boleta_recibo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_item_boleta_recibo` (
  `id_item_recibo` INT(11) NOT NULL ,
  `id_item_presupuesto` INT(11) NOT NULL ,
  `item_presupuestario` INT NOT NULL ,
  `cantidad` INT NOT NULL ,
  `costo_unitario` INT NOT NULL ,
  `detalles_adicionales` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id_item_recibo`, `id_item_presupuesto`) ,
  INDEX `fk_tbl_item_boleta_recibo_1` (`id_item_recibo` ASC) ,
  INDEX `fk_tbl_item_boleta_recibo_2` (`id_item_presupuesto` ASC) ,
  CONSTRAINT `fk_tbl_item_boleta_recibo_1`
    FOREIGN KEY (`id_item_recibo` )
    REFERENCES `sistemagestion`.`tbl_item_recibo` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_item_boleta_recibo_2`
    FOREIGN KEY (`id_item_presupuesto` )
    REFERENCES `sistemagestion`.`tbl_item_presupuesto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_recibo_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_recibo_presupuesto` (
  `id_recibo` INT(11) NOT NULL ,
  `id_presupuesto` INT(11) NOT NULL ,
  PRIMARY KEY (`id_recibo`, `id_presupuesto`) ,
  INDEX `fk_tbl_recibo_presupuesto_1` (`id_recibo` ASC) ,
  INDEX `fk_tbl_recibo_presupuesto_2` (`id_presupuesto` ASC) ,
  CONSTRAINT `fk_tbl_recibo_presupuesto_1`
    FOREIGN KEY (`id_recibo` )
    REFERENCES `sistemagestion`.`tbl_recibo` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_recibo_presupuesto_2`
    FOREIGN KEY (`id_presupuesto` )
    REFERENCES `sistemagestion`.`tbl_presupuesto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_pago_proveedores`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_pago_proveedores` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_entidad` INT(11) NOT NULL ,
  `tipo_entidad` INT(11) NOT NULL ,
  `tipo_recibo` INT NOT NULL DEFAULT 1 ,
  `id_usuario_que_solicita` INT(11) NULL DEFAULT NULL ,
  `numero_factura` VARCHAR(45) NOT NULL ,
  `monto_factura` INT(11) NOT NULL,
  `estado` INT NOT NULL DEFAULT 0 ,
  `gastos_envio` INT NOT NULL DEFAULT 0 ,
  `fecha_solicitud` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NOT NULL ,
  `comentario_revisor` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_pago_proveedores_1` (`id_usuario_que_solicita` ASC) ,
  CONSTRAINT `fk_tbl_pago_proveedores_1`
    FOREIGN KEY (`id_usuario_que_solicita` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Almacena registros de pago a proveedores y de unidad de abas';


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_pago_proveedor_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_pago_proveedor_presupuesto` (
  `id_pago_proveedor` INT(11) NOT NULL ,
  `id_presupuesto` INT(11) NOT NULL ,
  PRIMARY KEY (`id_pago_proveedor`, `id_presupuesto`) ,
  INDEX `fk_tbl_pago_proveedor_presupuesto_1` (`id_pago_proveedor` ASC) ,
  INDEX `fk_tbl_pago_proveedor_presupuesto_2` (`id_presupuesto` ASC) ,
  CONSTRAINT `fk_tbl_pago_proveedor_presupuesto_1`
    FOREIGN KEY (`id_pago_proveedor` )
    REFERENCES `sistemagestion`.`tbl_pago_proveedores` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_pago_proveedor_presupuesto_2`
    FOREIGN KEY (`id_presupuesto` )
    REFERENCES `sistemagestion`.`tbl_presupuesto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_movimiento_cuenta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_movimiento_cuenta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `fecha_transferencia` DATETIME NOT NULL ,
  `id_operador` INT(11) NOT NULL ,
  `id_cuenta_origen` INT(11) NOT NULL ,
  `id_entidad_origen` INT(11) NOT NULL ,
  `tipo_entidad_origen` INT(2) NOT NULL ,
  `id_cuenta_destino` INT(11) NULL ,
  `operacion` VARCHAR(45) NULL ,
  `id_entidad_destino` INT(11) NULL ,
  `tipo_entidad_destino` INT(2) NULL ,
  `detalle` TEXT NULL ,
  `monto` INT(11) NOT NULL ,
  `saldo` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_movimiento_cuenta_1` (`id_operador` ASC) ,
  INDEX `fk_tbl_movimiento_cuenta_2` (`id_cuenta_origen` ASC) ,
  INDEX `fk_tbl_movimiento_cuenta_3` (`id_cuenta_destino` ASC) ,
  CONSTRAINT `fk_tbl_movimiento_cuenta_1`
    FOREIGN KEY (`id_operador` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_movimiento_cuenta_2`
    FOREIGN KEY (`id_cuenta_origen` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_movimiento_cuenta_3`
    FOREIGN KEY (`id_cuenta_destino` )
    REFERENCES `sistemagestion`.`tbl_cuenta` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_revisor_tarea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_revisor_tarea` (
  `id_tarea` INT(11) NOT NULL ,
  `id_usuario` INT(11) NULL ,
  PRIMARY KEY (`id_tarea`) ,
  INDEX `fk_tbl_revisor_tarea_1` (`id_tarea` ASC) ,
  INDEX `fk_tbl_revisor_tarea_2` (`id_usuario` ASC) ,
  CONSTRAINT `fk_tbl_revisor_tarea_1`
    FOREIGN KEY (`id_tarea` )
    REFERENCES `sistemagestion`.`tbl_tarea` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_revisor_tarea_2`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_formulario_piea`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_formulario_piea` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` TEXT NOT NULL,
  `estado` VARCHAR(45) NOT NULL ,
  `fecha_creacion` VARCHAR(45) NOT NULL ,
  `fecha_actualizacion` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_proceso_formulario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_proceso_formulario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_formulario` INT(11) NOT NULL ,
  `fecha_inicio` DATETIME NOT NULL ,
  `fecha_cierre` DATETIME NOT NULL ,
  `activo` INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_proceso_formulario_1` (`id_formulario` ASC) ,
  CONSTRAINT `fk_tbl_proceso_formulario_1`
    FOREIGN KEY (`id_formulario` )
    REFERENCES `sistemagestion`.`tbl_formulario_piea` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_perfil_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_perfil_proyecto` (
  `id_proyecto` INT(11) NOT NULL ,
  `id_proceso_formulario` INT(11) NULL DEFAULT NULL ,
  `id_postulante` INT(11) NULL DEFAULT NULL ,
  `extracto` VARCHAR(300) NULL DEFAULT NULL ,
  `campus_sede` VARCHAR(50) NULL DEFAULT NULL ,
  `linea_postulacion` VARCHAR(45) NULL DEFAULT NULL ,
  `resumen` VARCHAR(500) NULL DEFAULT NULL ,
  `objetivos_generales` VARCHAR(500) NULL DEFAULT NULL ,
  `estado_del_arte` VARCHAR(500) NULL DEFAULT NULL ,
  `experiencia_previa` VARCHAR(500) NULL DEFAULT NULL ,
  `investigacion` VARCHAR(500) NULL DEFAULT NULL ,
  `desarrollo` VARCHAR(500) NULL DEFAULT NULL ,
  `innovacion` VARCHAR(500) NULL DEFAULT NULL ,
  `lugar_investigacion` INT NULL DEFAULT NULL ,
  `lugar_desarrollo` INT NULL DEFAULT NULL ,
  `lugar_innovacion` INT NULL DEFAULT NULL ,
  `impacto_academico` VARCHAR(500) NULL DEFAULT NULL ,
  `impacto_universitario_social` VARCHAR(500) NULL DEFAULT NULL ,
  `proyecciones` VARCHAR(500) NULL DEFAULT NULL ,
  `estado_postulacion` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id_proyecto`) ,
  INDEX `fk_tbl_perfil_proyecto_1` (`id_proyecto` ASC) ,
  INDEX `fk_tbl_perfil_proyecto_2` (`id_postulante` ASC) ,
  INDEX `fk_tbl_perfil_proyecto_3` (`id_proceso_formulario` ASC) ,
  CONSTRAINT `fk_tbl_perfil_proyecto_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_perfil_proyecto_2`
    FOREIGN KEY (`id_postulante` )
    REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_perfil_proyecto_3`
    FOREIGN KEY (`id_proceso_formulario` )
    REFERENCES `sistemagestion`.`tbl_proceso_formulario` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_objetivos_especificos_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_objetivos_especificos_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_proyecto` INT(11) NOT NULL ,
  `descripcion` VARCHAR(50) NOT NULL ,
  INDEX `fk_tbl_objetivos_especificos_proyecto_1` (`id_proyecto` ASC) ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_tbl_objetivos_especificos_proyecto_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_hito_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_hito_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_proyecto` INT(11) NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` VARCHAR(500) NULL ,
  `fecha` DATE NULL ,
  `es_hito` INT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_hito_proyecto_1` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_hito_proyecto_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_mes_hito`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_mes_hito` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_hito` INT(11) NOT NULL ,
  `mes` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_mes_hito_1` (`id_hito` ASC) ,
  CONSTRAINT `fk_tbl_mes_hito_1`
    FOREIGN KEY (`id_hito` )
    REFERENCES `sistemagestion`.`tbl_hito_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_item_presupuesto_postulacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_item_presupuesto_postulacion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_proyecto` INT(11) NOT NULL ,
  `id_categoria` INT NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `precio` INT(7) NOT NULL ,
  `unidades` INT NOT NULL ,
  `idi` INT(7) NOT NULL DEFAULT 0 ,
  `propio` INT(7) NOT NULL DEFAULT 0 ,
  `otros` INT(7) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_item_presupuesto_postulacion_1` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_item_presupuesto_postulacion_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_referentes_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_referentes_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_proyecto` INT(11) NOT NULL ,
  `nombre` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NOT NULL ,
  `telefono` VARCHAR(12) NOT NULL ,
  `relacion_proyecto` VARCHAR(100) NOT NULL ,
  `institucion` VARCHAR(45) NOT NULL ,
  `cargo` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_referentes_proyecto_1` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_referentes_proyecto_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_anexo_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_anexo_proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_proyecto` INT(11) NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `filename` VARCHAR(100) NOT NULL ,
  `original_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_anexo_proyecto_1` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_anexo_proyecto_1`
    FOREIGN KEY (`id_proyecto` )
    REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_revision_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_revision_proyecto` (
  `id_proyecto` INT(11) NOT NULL ,
  `id_revisor` INT(11) NOT NULL ,
  `extracto_ortografia` INT(3) NULL DEFAULT NULL ,
  `extracto_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `extracto_redaccion` INT(3) NULL DEFAULT NULL ,
  `resumen_ortografia` INT(3) NULL DEFAULT NULL ,
  `resumen_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `resumen_redaccion` INT(3) NULL DEFAULT NULL ,
  `objetivos_ortografia` INT(3) NULL DEFAULT NULL ,
  `objetivos_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `objetivos_redaccion` INT(3) NULL DEFAULT NULL ,
  `objetivos_factibilidad` INT(3) NULL DEFAULT NULL ,
  `objetivos_alcanzabilidad` INT(3) NULL DEFAULT NULL ,
  `estado_del_arte_pertinencia` INT(3) NULL DEFAULT NULL ,
  `estado_del_arte_acceso` INT(3) NULL DEFAULT NULL ,
  `estado_del_arte_ortografia` INT(3) NULL DEFAULT NULL ,
  `estado_del_arte_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `estado_del_arte_redaccion` INT(3) NULL DEFAULT NULL ,
  `experiencia_previa_ortografia` INT(3) NULL DEFAULT NULL ,
  `experiencia_previa_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `experiencia_previa_redaccion` INT(3) NULL DEFAULT NULL ,
  `investigacion_presencia` INT(3) NULL DEFAULT NULL ,
  `investigacion_ortografia` INT(3) NULL DEFAULT NULL ,
  `investigacion_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `investigacion_redaccion` INT(3) NULL DEFAULT NULL ,
  `desarrollo_presencia` INT(3) NULL DEFAULT NULL ,
  `desarrollo_ortografia` INT(3) NULL DEFAULT NULL ,
  `desarrollo_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `desarrollo_redaccion` INT(3) NULL DEFAULT NULL ,
  `innovacion_presencia` INT(3) NULL DEFAULT NULL ,
  `innovacion_ortografia` INT(3) NULL DEFAULT NULL ,
  `innovacion_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `innovacion_redaccion` INT(3) NULL DEFAULT NULL ,
  `impacto_academico_nota` INT(3) NULL DEFAULT NULL ,
  `impacto_academico_ortografia` INT(3) NULL DEFAULT NULL ,
  `impacto_academico_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `impacto_academico_redaccion` INT(3) NULL DEFAULT NULL ,
  `impacto_universitario_social_nota` INT(3) NULL DEFAULT NULL ,
  `impacto_universitario_social_ortografia` INT(3) NULL DEFAULT NULL ,
  `impacto_universitario_social_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `impacto_universitario_social_redaccion` INT(3) NULL DEFAULT NULL ,
  `proyecciones_nota` INT(3) NULL DEFAULT NULL ,
  `proyecciones_ortografia` INT(3) NULL DEFAULT NULL ,
  `proyecciones_capacidad_sintesis` INT(3) NULL DEFAULT NULL ,
  `proyecciones_redaccion` INT(3) NULL DEFAULT NULL ,
  `equipo_trabajo_presencia_especialidades` INT(3) NULL DEFAULT NULL ,
  `equipo_trabajo_asignacion_tareas` INT(3) NULL DEFAULT NULL ,
  `referentes_nota` INT(3) NULL DEFAULT NULL ,
  `hitos_alcanzables` INT(3) NULL DEFAULT NULL ,
  `hitos_realizables` INT(3) NULL DEFAULT NULL ,
  `hitos_ortografia` INT(3) NULL ,
  `hitos_capacidad_sintesis` INT(3) NULL ,
  `hitos_redaccion` INT(3) NULL ,
  `carta_gantt_tareas_apropiadas` INT(3) NULL DEFAULT NULL ,
  `carta_gantt_tiempos_apropiados` INT(3) NULL DEFAULT NULL ,
  `carta_gantt_ortografia` INT(3) NULL ,
  `carta_gantt_capacidad_sintesis` INT(3) NULL ,
  `carta_gantt_redaccion` INT(3) NULL ,
  `anexos_pertinencia` INT(3) NULL DEFAULT NULL ,
  `fecha_creacion` DATETIME NULL DEFAULT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  `extracto_comentario` TEXT NULL DEFAULT NULL ,
  `resumen_comentario` TEXT NULL DEFAULT NULL ,
  `objetivos_comentario` TEXT NULL DEFAULT NULL ,
  `estado_del_arte_comentario` TEXT NULL DEFAULT NULL ,
  `experiencia_previa_comentario` TEXT NULL DEFAULT NULL ,
  `investigacion_comentario` TEXT NULL DEFAULT NULL ,
  `desarrollo_comentario` TEXT NULL DEFAULT NULL ,
  `innovacion_comentario` TEXT NULL DEFAULT NULL ,
  `impacto_academico_comentario` TEXT NULL DEFAULT NULL ,
  `impacto_universitario_social_comentario` TEXT NULL DEFAULT NULL ,
  `proyecciones_comentario` TEXT NULL DEFAULT NULL ,
  `equipo_trabajo_comentario` TEXT NULL DEFAULT NULL ,
  `referentes_comentario` TEXT NULL DEFAULT NULL ,
  `hitos_comentario` TEXT NULL DEFAULT NULL ,
  `carta_gantt_comentario` TEXT NULL DEFAULT NULL ,
  `presupuesto_comentario` TEXT NULL DEFAULT NULL ,
  `anexos_comentario` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id_proyecto`, `id_revisor`) ,
  INDEX `fk_tbl_revision_proyecto_1` (`id_proyecto` ASC) ,
  INDEX `fk_tbl_revision_proyecto_2` (`id_revisor` ASC) ,
  CONSTRAINT `fk_tbl_revision_proyecto_1`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_revision_proyecto_2`
  FOREIGN KEY (`id_revisor` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_observacion_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_observacion_proyecto` (
  `id_proyecto` INT(11) NOT NULL ,
  `id_revisor` INT(11) NOT NULL ,
  `extracto_comentario` TEXT NULL DEFAULT NULL ,
  `resumen_comentario` TEXT NULL DEFAULT NULL ,
  `objetivos_comentario` TEXT NULL DEFAULT NULL ,
  `estado_del_arte_comentario` TEXT NULL DEFAULT NULL ,
  `experiencia_previa_comentario` TEXT NULL DEFAULT NULL ,
  `investigacion_comentario` TEXT NULL DEFAULT NULL ,
  `desarrollo_comentario` TEXT NULL DEFAULT NULL ,
  `innovacion_comentario` TEXT NULL DEFAULT NULL ,
  `impacto_academico_comentario` TEXT NULL DEFAULT NULL ,
  `impacto_universitario_social_comentario` TEXT NULL DEFAULT NULL ,
  `proyecciones_comentario` TEXT NULL DEFAULT NULL ,
  `equipo_trabajo_comentario` TEXT NULL DEFAULT NULL ,
  `referentes_comentario` TEXT NULL DEFAULT NULL ,
  `hitos_comentario` TEXT NULL DEFAULT NULL ,
  `carta_gantt_comentario` TEXT NULL DEFAULT NULL ,
  `presupuesto_comentario` TEXT NULL DEFAULT NULL ,
  `anexos_comentario` TEXT NULL DEFAULT NULL ,
  `fecha_creacion` DATETIME NULL DEFAULT NULL ,
  `fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id_proyecto`, `id_revisor`) ,
  INDEX `fk_tbl_observacion_proyecto_1` (`id_proyecto` ASC) ,
  INDEX `fk_tbl_observacion_proyecto_2` (`id_revisor` ASC) ,
  CONSTRAINT `fk_tbl_observacion_proyecto_1`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_observacion_proyecto_2`
  FOREIGN KEY (`id_revisor` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_revisor_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_revisor_proyecto` (
  `id_usuario` INT(11) NOT NULL ,
  `id_proyecto` INT(11) NOT NULL ,
  `id_proceso_formulario` INT(11) NOT NULL ,
  `estado_revision` INT(1) NOT NULL DEFAULT 0 ,
  `nota` VARCHAR(5) NULL ,
  PRIMARY KEY (`id_usuario`, `id_proyecto`) ,
  INDEX `fk_tbl_revisor_proyecto_1` (`id_proyecto` ASC) ,
  INDEX `fk_tbl_revisor_proyecto_2` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_revisor_proyecto_1`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_revisor_proyecto_2`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_revision_item_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_revision_item_presupuesto` (
  `id_item_presupuesto_postulacion` INT(11) NOT NULL ,
  `id_revisor` INT(11) NOT NULL ,
  `fue_editado` INT(1) NOT NULL DEFAULT 0 ,
  `item_de_costo_apropiado_nota` INT(3) NULL ,
  `montos_adecuados_a_la_realizacion_item` INT(3) NULL ,
  `monto_idi_editado` INT NULL ,
  `motivo_edicion` VARCHAR(255) NULL ,
  PRIMARY KEY (`id_item_presupuesto_postulacion`, `id_revisor`) ,
  INDEX `fk_tbl_revision_item_presupuesto_1` (`id_item_presupuesto_postulacion` ASC) ,
  INDEX `fk_tbl_revision_item_presupuesto_2` (`id_revisor` ASC) ,
  CONSTRAINT `fk_tbl_revision_item_presupuesto_1`
  FOREIGN KEY (`id_item_presupuesto_postulacion` )
  REFERENCES `sistemagestion`.`tbl_item_presupuesto_postulacion` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_revision_item_presupuesto_2`
  FOREIGN KEY (`id_revisor` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_proyecto_seleccionado`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_proyecto_seleccionado` (
  `id_proyecto` INT(11) NOT NULL ,
  `id_proceso_formulario` INT(11) NOT NULL ,
  `monto_idi` INT(15) NOT NULL ,
  `fecha_seleccion` DATETIME NOT NULL ,
  PRIMARY KEY (`id_proyecto`) ,
  INDEX `fk_tbl_proyecto_seleccionado_1` (`id_proyecto` ASC) ,
  INDEX `fk_tbl_proyecto_seleccionado_2` (`id_proceso_formulario` ASC) ,
  CONSTRAINT `fk_tbl_proyecto_seleccionado_1`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_proyecto_seleccionado_2`
  FOREIGN KEY (`id_proceso_formulario` )
  REFERENCES `sistemagestion`.`tbl_proceso_formulario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_evaluacion_fae`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_evaluacion_fae` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_evaluado` INT(11) NOT NULL ,
  `id_evaluador` INT(11) NULL ,
  `id_entidad` INT(11) NOT NULL ,
  `tipo_entidad` INT(1) NOT NULL ,
  `competencia_uno` INT(1) NULL ,
  `competencia_dos` INT(1) NULL ,
  `estado` INT(1) NOT NULL DEFAULT 0 ,
  `fecha_evaluacion` DATETIME NULL ,
  `fecha_actualizacion` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_evaluacion_fae_1` (`id_evaluado` ASC) ,
  INDEX `fk_tbl_evaluacion_fae_2` (`id_evaluador` ASC) ,
  CONSTRAINT `fk_tbl_evaluacion_fae_1`
  FOREIGN KEY (`id_evaluado` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_evaluacion_fae_2`
  FOREIGN KEY (`id_evaluador` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_estadistica_idi`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_estadistica_idi` (
  `id_formulario` INT(11) NOT NULL ,
  `id_proceso_formulario` INT(11) NOT NULL ,
  `nota_promedio` VARCHAR(5) NULL ,
  `nota_sd` VARCHAR(5) NULL ,
  `nota_minima` VARCHAR(5) NULL ,
  `nota_maxima` VARCHAR(5) NULL ,
  `forma_promedio` VARCHAR(5) NULL ,
  `forma_sd` VARCHAR(5) NULL ,
  `fondo_promedio` VARCHAR(5) NULL ,
  `fondo_sd` VARCHAR(5) NULL ,
  `planificacion_promedio` VARCHAR(5) NULL ,
  `planificacion_sd` VARCHAR(5) NULL ,
  `impacto_promedio` VARCHAR(5) NULL ,
  `impacto_sd` VARCHAR(5) NULL ,
  `fecha_creacion` DATETIME NULL ,
  `fecha_actualizacion` DATETIME NULL ,
  PRIMARY KEY (`id_formulario`, `id_proceso_formulario`) ,
  INDEX `fk_tbl_estadistica_idi_1` (`id_formulario` ASC) ,
  INDEX `fk_tbl_estadistica_idi_2` (`id_proceso_formulario` ASC) ,
  CONSTRAINT `fk_tbl_estadistica_idi_1`
  FOREIGN KEY (`id_formulario` )
  REFERENCES `sistemagestion`.`tbl_formulario_piea` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_estadistica_idi_2`
  FOREIGN KEY (`id_proceso_formulario` )
  REFERENCES `sistemagestion`.`tbl_proceso_formulario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_observacion_item_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_observacion_item_presupuesto` (
  `id_item_presupuesto_postulacion` INT(11) NOT NULL ,
  `id_observador` INT(11) NOT NULL ,
  `monto_idi_editado` INT NULL ,
  PRIMARY KEY (`id_item_presupuesto_postulacion`, `id_observador`) ,
  INDEX `fk_tbl_observacion_item_presupuesto_1` (`id_item_presupuesto_postulacion` ASC) ,
  INDEX `fk_tbl_observacion_item_presupuesto_2` (`id_observador` ASC) ,
  CONSTRAINT `fk_tbl_observacion_item_presupuesto_1`
  FOREIGN KEY (`id_item_presupuesto_postulacion` )
  REFERENCES `sistemagestion`.`tbl_item_presupuesto_postulacion` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_observacion_item_presupuesto_2`
  FOREIGN KEY (`id_observador` )
  REFERENCES `sistemagestion`.`tbl_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_historico_hito_proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_historico_hito_proyecto` (
  `id` INT(11) NOT NULL,
  `id_proyecto` INT(11) NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` VARCHAR(500) NULL DEFAULT NULL ,
  `fecha` DATE NULL DEFAULT NULL ,
  `es_hito` INT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_historico_hito_proyecto_1` (`id_proyecto` ASC) ,
  CONSTRAINT `fk_tbl_historico_hito_proyecto_1`
  FOREIGN KEY (`id_proyecto` )
  REFERENCES `sistemagestion`.`tbl_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sistemagestion`.`tbl_historico_mes_hito`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemagestion`.`tbl_historico_mes_hito` (
  `id` INT(11) NOT NULL,
  `id_hito` INT(11) NOT NULL ,
  `mes` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_historico_mes_hito_1` (`id_hito` ASC) ,
  CONSTRAINT `fk_tbl_historico_mes_hito_1`
  FOREIGN KEY (`id_hito` )
  REFERENCES `sistemagestion`.`tbl_historico_hito_proyecto` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Data for table `sistemagestion`.`tbl_rol_piea`
-- -----------------------------------------------------
START TRANSACTION;
USE `sistemagestion`;
INSERT INTO `sistemagestion`.`tbl_rol_piea` (`ano_actual`, `siguiente_id`) VALUES (2013, 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `sistemagestion`.`tbl_status_usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `sistemagestion`;
INSERT INTO `sistemagestion`.`tbl_status_usuario` (`id`, `nombre`, `descripcion`) VALUES (NULL, 'Alumno pregrado', 'Alumno de pregrado');
INSERT INTO `sistemagestion`.`tbl_status_usuario` (`id`, `nombre`, `descripcion`) VALUES (NULL, 'Alumno posgrado', 'Alumno de posgrado');
INSERT INTO `sistemagestion`.`tbl_status_usuario` (`id`, `nombre`, `descripcion`) VALUES (NULL, 'Profesor', 'Profesor');
INSERT INTO `sistemagestion`.`tbl_status_usuario` (`id`, `nombre`, `descripcion`) VALUES (NULL, 'ExAlumno UTFSM', 'Ex alumno UTFSM');
INSERT INTO `sistemagestion`.`tbl_status_usuario` (`id`, `nombre`, `descripcion`) VALUES (NULL, 'Usuario externo', 'Usuario externo');

COMMIT;

-- -----------------------------------------------------
-- Data for table `sistemagestion`.`tbl_categoria_actividad`
-- -----------------------------------------------------
START TRANSACTION;
USE `sistemagestion`;
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 0, 'Toma de Decisiones', 1);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 0, 'Relevancia', 1);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 0, 'Personas a Cargo', 1);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 1, 'Grupal', 2);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 1, 'Individual', 2);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 2, 'Hito Crítico', 2);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 2, 'Impacto', 2);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 2, 'Continuidad', 2);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 3, 'Número de Personas', 2);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 3, 'Diversidad de Disciplinas Involucradas', 2);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 3, 'Diversidad de Experiencia', 2);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 7, 'Número de Personas', 3);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 7, 'Diversidad', 3);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 7, 'Orientación', 3);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 8, 'Nueva', 3);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 8, 'Tradicional', 3);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 13, 'Social', 4);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 13, 'Cultural', 4);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 13, 'Otro', 4);

INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 14, 'Proyecto', 4);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 14, 'Iniciativa', 4);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 14, 'Universidad', 4);
INSERT INTO `sistemagestion`.`tbl_categoria_actividad` (`id`, `id_padre`, `nombre`, `nivel`) VALUES (NULL, 14, 'Externo', 4);

COMMIT;

START TRANSACTION;
USE `sistemagestion`;
INSERT INTO `tbl_cuenta` (`id`, `id_entidad`, `tipo_entidad`, `nombre`, `numero`, `propietario`, `cargo_propietario`, `saldo`, `fecha_creacion`, `fecha_actualizacion`, `encargado`, `descripcion`) VALUES
  ('1', '0', '2', 'PIEA Central', '00.00.00', 'PIEA', 'Administrador', '0', '2013-01-03 12:44:32', '2013-01-03 13:13:01', '1', NULL);
INSERT INTO `tbl_formulario_piea` (`id`, `nombre`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
  ('1', 'Fondos concursables I+D+i', 'Fondos concursables para proyectos I+D+i PIEA', '1', '1359565791', '1359573815');
INSERT INTO `tbl_proceso_formulario` (`id`, `id_formulario`, `fecha_inicio`, `fecha_cierre`, `activo`) VALUES
  ('2', '1', '2013-03-21 00:00:00', '2013-03-21 00:00:00', '1');
COMMIT;
