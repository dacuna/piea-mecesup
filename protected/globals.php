<?php
/**
 * El archivo globals.php permite utilizar atajos en el codigo simplificando las llamadas funciones que
 * se utilizan comunmente en Yii.
 *
 * Este archivo se importa en index.php y las funciones existentes se pueden utilizar en cualquier parte
 * del codigo del sistema (controladres, modelos, vistas, componentes, etc.). La mayoria de estas funciones
 * son atajos a llamadas de funciones en Yii. Por ejemplo, para crear una url normalmente se deberia escribir
 * Yii::app()->createUrl($route,$params,$ampersand);, pero el archivo globals.php agrega el atajo para que solo
 * se deba escribir: url($route, $params,...). Ademas, se agregan funciones de utilidad en otros contextos.
 * Bajo ese punto de vista, el archivo globals.php funciona como un 'helper' (los helper no existen como tal
 * en Yii.
 */

/**
 * Dump a variable to output buffer
 * @param mixed $var a variable to dump
 * @return string HTML dump of parameter
 */
function dump($var) {
  CVarDumper::dump($var, 10, true);
  return $var;
}

/**
 * Shortcut to Yii::trace()
 * @param mixed $x the message to trace
 * @return mixed the argument passed in
 * @param bool $export to var_export the value of $x
 */
function trace($x, $export = false) {
  Yii::trace($export ? var_export($x, true) : $x);
  return $x;
}

/**
 * DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS',DIRECTORY_SEPARATOR);

/**
 * @return CApplication Yii::app()
 */
function app() {
    return Yii::app();
}

/**
 * @return CClientScript Yii::app()->clientScript
 */
function cs() {
    return Yii::app()->getClientScript();
}

/**
 * @return CAuthManager Yii::app()->authManager
 */
function am() {
    return Yii::app()->getAuthManager();
}

/**
 * @return CWebUser Yii::app()->user
 */
function user() {
    return Yii::app()->getUser();
}

/**
 * Sets or gets user state. getter if $val is null. setter otherwise
 * @param string $key state store key
 * @param null $val key for the stored data
 * @return mixed the stored data
 */
function state($key, $val = null) {
  if ($val === null)
    return Yii::app()->getUser()->getState($key);
  else
    return Yii::app()->getUser()->getState($key, $val);
}

/**
 * Shortcut to Yii::app()->createUrl()
 * @param string $route controller/action-type route
 * @param array $params
 * @param string $ampersand
 * @return string
 */
function url($route, $params=array(), $ampersand='&') {
    return Yii::app()->createUrl($route,$params,$ampersand);
}

/**
 * Shortcut to CHtml::encode
 * @param string $text raw text to encode
 * @return string
 */
function h($text) {
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

/**
 * Shortcut to CHtml::link()
 * @param string $text raw link text
 * @param string $url link URL or route
 * @param array $htmlOptions
 * @return string HTML link tag
 */
function l($text, $url = '#', $htmlOptions = array()) {
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * Shortcut to Yii::t() with default category = 'stay'
 * @param string $message soure language text
 * @param string $category translation library
 * @param array $params string params
 * @param string $source source language
 * @param string $language target language
 * @return string translated text
 */
function t($message, $category = 'dca', $params = array(), $source = null, $language = null) {
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * Quotes a string value for use in a query.
 * @param string $s string to be quoted
 * @return string the properly quoted string
 * @see http://www.php.net/manual/en/function.PDO-quote.php
 */
function q($s) {
  return Yii::app()->db->quoteValue($s);
}

/**
 * Shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 * @param string $url a relative url to prefix with baseUrl
 * @return string
 */
function baseUrl($url=null) {
    static $baseUrl;
    if ($baseUrl===null)
        $baseUrl=Yii::app()->getRequest()->getBaseUrl();
    return $url===null ? $baseUrl : $baseUrl.'/'.ltrim($url,'/');
}

/**
 * Shortcut to Yii::app()->params[$name].
 * @param $name
 * @return mixed the named application parameter
 */
function param($name) {
    return Yii::app()->params[$name];
}

/**
 * @param string $str subject of test for integerness
 * @return bool true if argument is an integer string
 */
function intStr($str) {
  return !!preg_match('/^\d+$/', $str);
}

/**
 * Convert string to slug 
 * @param string $title
 * @return mixed $slug
 */
function makeSlug($title){
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $title);
    $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
    $clean = strtolower(trim($clean));
    $clean = preg_replace("/[\/_| -]+/", '-', $clean);

    return $clean;
}

/**
 * truncate() Simple function to shorten a string and add an ellipsis
 * 
 * @param string $string Origonal string
 * @param integer $max Maximum length
 * @param string $rep Replace with ...
 * @return string
 * @author David Duong - dacuna
 **/
function truncate ($string, $max = 50, $rep = '...') {
  if(strlen($string) <= $max)
    return $string;
  $leave = $max - strlen ($rep);
  return substr_replace($string, $rep, $leave);
}

/**
 * Sluggify un string.
 *
 * @param $string
 * @return string
 */
function string_to_underscore_name($string)
{
    $string = preg_replace('/[\'"]/', '', $string);
    $string = preg_replace('/[^a-zA-Z0-9]+/', '_', $string);
    $string = trim($string, '_');
    $string = strtolower($string);

    return $string;
}

/**
 * Transforma desde un numero (digitos) a la version en palabras del mismo numero. Por ejemplo:
 * 1234 => Mil doscientos treinta y cuatro
 * @param $num
 * @param bool $fem
 * @param bool $dec
 * @return string
 */
function num2letras($num, $fem = false, $dec = true) {
    $matuni[2]  = "dos";
    $matuni[3]  = "tres";
    $matuni[4]  = "cuatro";
    $matuni[5]  = "cinco";
    $matuni[6]  = "seis";
    $matuni[7]  = "siete";
    $matuni[8]  = "ocho";
    $matuni[9]  = "nueve";
    $matuni[10] = "diez";
    $matuni[11] = "once";
    $matuni[12] = "doce";
    $matuni[13] = "trece";
    $matuni[14] = "catorce";
    $matuni[15] = "quince";
    $matuni[16] = "dieciseis";
    $matuni[17] = "diecisiete";
    $matuni[18] = "dieciocho";
    $matuni[19] = "diecinueve";
    $matuni[20] = "veinte";
    $matunisub[2] = "dos";
    $matunisub[3] = "tres";
    $matunisub[4] = "cuatro";
    $matunisub[5] = "quin";
    $matunisub[6] = "seis";
    $matunisub[7] = "sete";
    $matunisub[8] = "ocho";
    $matunisub[9] = "nove";

    $matdec[2] = "veint";
    $matdec[3] = "treinta";
    $matdec[4] = "cuarenta";
    $matdec[5] = "cincuenta";
    $matdec[6] = "sesenta";
    $matdec[7] = "setenta";
    $matdec[8] = "ochenta";
    $matdec[9] = "noventa";
    $matsub[3]  = 'mill';
    $matsub[5]  = 'bill';
    $matsub[7]  = 'mill';
    $matsub[9]  = 'trill';
    $matsub[11] = 'mill';
    $matsub[13] = 'bill';
    $matsub[15] = 'mill';
    $matmil[4]  = 'millones';
    $matmil[6]  = 'billones';
    $matmil[7]  = 'de billones';
    $matmil[8]  = 'millones de billones';
    $matmil[10] = 'trillones';
    $matmil[11] = 'de trillones';
    $matmil[12] = 'millones de trillones';
    $matmil[13] = 'de trillones';
    $matmil[14] = 'billones de trillones';
    $matmil[15] = 'de billones de trillones';
    $matmil[16] = 'millones de billones de trillones';

    //Zi hack
    $float=explode('.',$num);
    $num=$float[0];

    $num = trim((string)@$num);
    if ($num[0] == '-') {
        $neg = 'menos ';
        $num = substr($num, 1);
    }else
        $neg = '';
    while ($num[0] == '0') $num = substr($num, 1);
    if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num;
    $zeros = true;
    $punt = false;
    $ent = '';
    $fra = '';
    for ($c = 0; $c < strlen($num); $c++) {
        $n = $num[$c];
        if (! (strpos(".,'''", $n) === false)) {
            if ($punt) break;
            else{
                $punt = true;
                continue;
            }

        }elseif (! (strpos('0123456789', $n) === false)) {
            if ($punt) {
                if ($n != '0') $zeros = false;
                $fra .= $n;
            }else

                $ent .= $n;
        }else

            break;

    }
    $ent = '     ' . $ent;
    if ($dec and $fra and ! $zeros) {
        $fin = ' coma';
        for ($n = 0; $n < strlen($fra); $n++) {
            if (($s = $fra[$n]) == '0')
                $fin .= ' cero';
            elseif ($s == '1')
                $fin .= $fem ? ' una' : ' un';
            else
                $fin .= ' ' . $matuni[$s];
        }
    }else
        $fin = '';
    if ((int)$ent === 0) return 'Cero ' . $fin;
    $tex = '';
    $sub = 0;
    $mils = 0;
    $neutro = false;
    while ( ($num = substr($ent, -3)) != '   ') {
        $ent = substr($ent, 0, -3);
        if (++$sub < 3 and $fem) {
            $matuni[1] = 'una';
            $subcent = 'as';
        }else{
            $matuni[1] = $neutro ? 'un' : 'uno';
            $subcent = 'os';
        }
        $t = '';
        $n2 = substr($num, 1);
        if ($n2 == '00') {
        }elseif ($n2 < 21)
            $t = ' ' . $matuni[(int)$n2];
        elseif ($n2 < 30) {
            $n3 = $num[2];
            if ($n3 != 0) $t = 'i' . $matuni[$n3];
            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        }else{
            $n3 = $num[2];
            if ($n3 != 0) $t = ' y ' . $matuni[$n3];
            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        }
        $n = $num[0];
        if ($n == 1) {
            $t = ' ciento' . $t;
        }elseif ($n == 5){
            $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
        }elseif ($n != 0){
            $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
        }
        if ($sub == 1) {
        }elseif (! isset($matsub[$sub])) {
            if ($num == 1) {
                $t = ' mil';
            }elseif ($num > 1){
                $t .= ' mil';
            }
        }elseif ($num == 1) {
            $t .= ' ' . $matsub[$sub] . '?n';
        }elseif ($num > 1){
            $t .= ' ' . $matsub[$sub] . 'ones';
        }
        if ($num == '000') $mils ++;
        elseif ($mils != 0) {
            if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub];
            $mils = 0;
        }
        $neutro = true;
        $tex = $t . $tex;
    }
    $tex = $neg . substr($tex, 1) . $fin;
    return strtoupper($tex).' PESOS';
    //$end_num=ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
    //return $end_num;
}

/**
 * Zerofill to the left a number.
 *
 * @param $num
 * @param int $zerofill
 * @return string
 */
function zerofill ($num, $zerofill = 5)
{
    return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
}

/**
 * Creates a compressed zip file
 *
 * @param array $files
 * @param string $destination
 * @param bool $overwrite
 * @return bool
 */
function create_zip($files = array(),$destination = '',$overwrite = false) {
    //if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }
    //vars
    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file[0])) {
                $valid_files[] = array($file[0],$file[1]);
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $zip->addFile($file[0],$file[1]);
        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        //close the zip -- done!
        $zip->close();

        //check to make sure the file exists
        return file_exists($destination);
    }
    else
    {
        return false;
    }
}

/**
 * Function to calculate square of value - mean
 *
 * @param $x
 * @param $mean
 * @return number
 */
function sd_square($x, $mean) { return pow($x - $mean,2); }

/**
 * @param $a
 * @param $b
 * @return int
 */
function compareGrade($a,$b){
    if ($a[3] == $b[3]) {
        return 0;
    }
    return ($a[3] < $b[3]) ? 1 : -1;
}