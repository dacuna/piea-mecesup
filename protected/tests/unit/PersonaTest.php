<?php

/**
 * Brief description of class
 *
 * Long description of class
 *
 * @example   ../index.php
 * @example <br />
 *     $oUser = new MyLibrary\User(new Mappers\UserMapper());<br />
 *  $oUser->setUsername('swader');<br />
 *  $aAllEmails = $oUser->getEmails();<br />
 *  $oUser->addEmail('test@test.com');<br />
 * @version   0.01
 * @since     10/20/12
 * @author    dacuna <dacuna@dacuna.me>
 */
class PersonaTest extends CDbTestCase
{
    public $fixtures=array(
        'personas'=>'Persona'
    );

    public function testCreatePersona(){
        Yii::app()->getModule('usuarios');
        $persona=new Usuario;
        $persona->nombres="Carlos Juan";
        $persona->apellido_materno="Gutierrez";
        $persona->apellido_paterno="Alvarez";
        $persona->password=sha1('hola');

        $this->assertTrue($persona->save(),'La persona se ha guardado correctamente');
    }

}
