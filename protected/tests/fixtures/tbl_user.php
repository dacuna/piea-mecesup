<?php
/**
 * Fixture file for table user. Contains example data for doing automated unit and functional test.
 * User: dacuna
 * Date: 10/20/12
 * Time: 3:27 PM
 */
return array(
    'admin'=>array(
        'email'=>'olga.godoy@usm.cl',
        'password'=>sha1('admin'),
        'nombres'=>'Olga',
        'apellido_paterno'=>'Godoy',
        'apellido_materno'=>'Funez',
    ),
    'user1'=>array(
        'email'=>'diego.acuna@usm.cl',
        'password'=>sha1('hola'),
        'nombres'=>'Diego Ignacio',
        'apellido_paterno'=>'Acuna',
        'apellido_materno'=>'Rozas',
    ),
);