<?php

// Yii::setPathOfAlias('local','path/to/local-folder');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'PIE>A',
    'preload' => array('log', 'foundation'),
    'language' => 'es',
    'sourceLanguage' => 'en_us',

    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'ext.yii-mail.YiiMailMessage',
    ),
    'aliases' => array(
        'xupload' => 'ext.xupload'
    ),

    'modules' => array(
        'usuarios',
        'iniciativas',
        'proyectos',
        'buzon',
        'tareas',
        'finanzas',
        'formularios',
        'gii' => array('class' => 'system.gii.GiiModule', 'password' => 'hola', 'ipFilters' => array('127.0.0.1', '::1')),
    ),

    'components' => array(
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                    'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format'      => 'letter', // format A4, A5, ...
                        'language'    => 'en', // language: fr, en, it ...
                        'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges'      => array(10, 10, 10, 13), // margins by default, in order (left, top, right, bottom)
                    )
                )
            ),
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('usuarios/authenticate/login'),
        ),
        'file' => array(
            'class' => 'application.extensions.cfile.CFile',
        ),
        'mailman' => array(
            'class' => 'Mailman'
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,
            'transportOptions' => array('host' => 'smtp.gmail.com', 'username' => 'web@piea.usm.cl',
                'password' => 'w3bp1342oi3', 'port' => 465, 'encryption' => 'ssl')
        ),
        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            'driver' => 'GD'
        ),
        'input' => array(
            'class' => 'CmsInput',
            'cleanPost' => false,
            'cleanGet' => false,
        ),
        /*
        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        */
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=sistemagestion',
            'emulatePrepare' => true,
            'username' => 'piea',
            'password' => 'p80..1.usm',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_'
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'tbl_AuthItem',
            'itemChildTable' => 'tbl_AuthItemChild',
            'assignmentTable' => 'tbl_AuthAssignment',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, trace',
                ),
                /**
                array(
                'class'=>'CWebLogRoute',
                ),
                 **/
            ),
        ),
        'foundation' => array("class" => "ext.foundation.components.Foundation"),
    ),

    'params' => array(
        'adminEmail' => 'webmaster@example.com',
    ),
);
