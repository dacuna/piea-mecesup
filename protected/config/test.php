<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
        'import' => array(
            'application.modules.usuarios.models.*',
            'application.modules.usuarios.components.*',
            // any other includings
        ),
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
            'db'=>array(
                'connectionString' => 'mysql:host=localhost;dbname=sistemagestion',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => 'sroot',
                'charset' => 'utf8',
                'tablePrefix'=>'tbl_'
            ),
            'assetManager'=>array(
                'basePath'=>'/var/www/piea-mecesup/assets',
            )
		),
	)
);
