<?php
Yii::import('application.modules.proyectos.models.*');
/**
 * Controlador que contiene las acciones basicas del sistema, es decir, las paginas de inicio, de contacto
 * etc.
 *
 * @package   controllers
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class SiteController extends Controller
{

    /**
     * Index del sitio.
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actionIndex()
    {
        $model = new LoginForm;
        $this->render('index', array(
            'model' => $model
        ));
    }

    /**
     * Accion que permite manenar cualquier error que se ejecute en la aplicacion. Cuando se produce
     * una {@link CHttpException} entonces se redirige el flujo de la aplicacion hacia esta accion.
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Accion que despliega un formulario de contacto con el que los usuarios se pueden comunicar con el
     * administrador del sitio.
     * @throws  \CHttpException
     * @todo    IMPLEMENTAR ESTA FUNCIONALIDAD
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Accion que despliega la totalidad de las iniciativas y proyectos mostrando informacion resumida sobre
     * estas entidades para un usuario logeado. De esta forma los usuarios pueden solicitar su participacion
     * en las entidades y iniciativas que estimen conveniente.
     *
     * @since   2012-08-28
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actionExplorarPiea()
    {
        $id_proyecto = Yii::app()->input->get('id');
        $proyecto = null;

        if (isset($id_proyecto) && !empty($id_proyecto))
            $proyecto = ProyectoModel::model()->findByPk($id_proyecto);

        $dataProvider = new CActiveDataProvider('Iniciativa', array(
            'criteria' => array(
                'order' => 'nombre_abreviado DESC'
            )
        ));
        $this->render('explorar', array(
            'dataProvider' => $dataProvider,
            'proyecto' => $proyecto
        ));
    }

    /**
     * Acciones del controlador.
     *
     * @return  array array con las acciones utilizadas en el controlador.
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

}