<?php $this->beginContent('//layouts/main');
Yii::import('application.modules.formularios.models.FormularioPiea');?>

<?php if(!Yii::app()->user->isGuest):?>

<div class="row">
  <div class="three columns">
    
    <p>Bienvenid@, <?php echo Yii::app()->user->nombre;?>

      <?php if(FormularioPiea::formularioIDI()->estado!=5):?>
      <dl class="nice vertical tabs">
          <dd><a href="#vertical1" class="active">Fondos I+D+i</a></dd>
          <dd class="clearfix" style="border:1px #eee solid;border-top:0;border-bottom:0;">
              <?php if(user()->esPostulanteFondosIDI()):?>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/verProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Perfil Proyecto</a>
                <?php if(user()->proyectoPostulacion()->estado_postulacion!=1):?>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/crearProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Editar proyecto</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/hitosProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Hitos del proyecto</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/cartaGantt',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Carta Gantt</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/equipoTrabajoProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Equipo de trabajo</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/presupuestoProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Presupuesto</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/referentesProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Referentes</a>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/anexosProyecto',array('id'=>user()->proyectoPostulacion()->id_proyecto));?>" style="font-size:13px;border:0;padding:6px 8px;">- Anexos</a>
                <?php endif;?>
              <?php else:?>
                  <a href="<?php echo url('/formularios/postulacionFondosIDI/crearProyecto');?>" style="font-size:13px;">
                      - Crear Proyecto</a>
              <?php endif;?>
          </dd>
      </dl>
      <?php endif;?>


    <?php if(user()->checkAccess('administrador')):?>
    <dl class="nice vertical tabs">
      <dd><a href="#vertical1" class="active">Men&uacute; principal</a></dd>
      <dd class="clearfix" style="border:1px #eee solid;border-top:0;border-bottom:0;">    
      <?php $dataTree = array(
        array('text'=>CHtml::openTag('a',array('href'=>Yii::app()->createUrl('site/explorarPiea'),'style'=>'border:0;padding:3px 0;font-size:100%;')).'Explorar PIEA'.CHtml::closeTag('a')),
        array('text'=>CHtml::openTag('a',array('href'=>Yii::app()->createUrl('/tareas/crud/crear'),'style'=>'border:0;padding:3px 0;font-size:100%;')).'Ingresar Actividad'.CHtml::closeTag('a')),
        array('text'=>CHtml::openTag('a',array('href'=>Yii::app()->createUrl('/tareas/gestionActividades'),'style'=>'border:0;padding:3px 0;font-size:100%;')).'Ver historial de actividades'.CHtml::closeTag('a')),
        array('text'=>'Solicitar evaluaci&oacute;n')
        );
           
        $this->widget('CTreeView',array(
          'data'=>$dataTree,
          'animated'=>'fast', 
          'collapsed'=>'false',
          'htmlOptions'=>array(
            'class'=>'treeview-famfamfam',
          ),
        ));?>
      </dd>
    </dl>

    <?php endif;?>
    
    <dl class="nice vertical tabs">
      <dd><a href="#vertical1" class="active">Acciones de usuario</a></dd>
      <dd class="clearfix" style="border:1px #eee solid;border-top:0;border-bottom:0;">
        <?php
        $this->widget('CTreeView',array(
          'data'=>Yii::app()->user->crearMenu(),
          'animated'=>'fast', //quick animation
          'collapsed'=>false,
          'htmlOptions'=>array(
            'class'=>'treeview-famfamfam',
          ),
        ));?>
      </dd>
    </dl>
    
  </div>
  <div class="nine columns">
    <?php $this->widget("foundation.widgets.FounAlert"); ?>
    <?php echo $content; ?>			
  </div>
</div>

<?php else:?>

  <?php echo $content; ?>		

<?php endif;?>
<?php $this->endContent(); ?>