<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
    <?php $this->widget("foundation.widgets.FounAlert"); ?>
    <?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>