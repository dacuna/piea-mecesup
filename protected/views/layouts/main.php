<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

  <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/svgweb/svg.js" data-path="<?php echo Yii::app()->baseUrl;?>/js/svgweb/"></script>
	<title>PIE&gt;A::Sistema de gesti&oacute;n de iniciativas y proyectos - <?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body <?php if(isset($this->angular) && !empty($this->angular)){echo $this->angular;}?>>

<div class="container" id="page">

  <div id="header" class="clearfix">
    <div id="logo">
      <img src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png">
    </div>
    <div id="quick-links">
      <?php
        $this->widget("foundation.widgets.FounNavBar", array('items' => array(
            array('label' => 'Documentos de inter&eacute;s','url'=>array('/site/page','view'=>'documentos'),'visible'=>!Yii::app()->user->isGuest),
            array('label' => 
              'Notificaciones ('.Mensaje::model()->notificaciones.')',
              'url'=>array('/buzon/inbox/index'),'visible'=>!Yii::app()->user->isGuest),
            array('label' => 'Mi perfil','url'=>array('/usuarios/perfil/miPerfil'),'visible'=>!Yii::app()->user->isGuest,
            'flyout' => array('small', 
            '<ul style="font-size:14px;">
              <li><a href="'.Yii::app()->createUrl('/usuarios/perfil/miPerfil').'">Ver perfil</a></li>
              <li><a href="'.Yii::app()->createUrl('/usuarios/perfil/modificarPerfil').'">Modificar perfil</a></li>
              <li><a href="'.Yii::app()->createUrl('/usuarios/privacidad').'">Privacidad</a></li>
              <li><a href="'.Yii::app()->createUrl('/usuarios/perfil/seguridad').'">Seguridad</a></li>
            </ul>')),
            array('label' => 'Registrarme','url'=>array('/usuarios/userActions/create'),'visible'=>Yii::app()->user->isGuest),
            array('label' => 'Iniciar Sesi&oacute;n','url'=>array('/usuarios/authenticate/login'),'visible'=>Yii::app()->user->isGuest),
            array('label' => 'Cerrar Sesi&oacute;n','url'=>array('/usuarios/authenticate/logout'),'visible'=>!Yii::app()->user->isGuest),
        )));
      ?>      
    </div>
  </div>

	<?php if(isset($this->breadcrumbs)):?>
    <?php $this->widget("foundation.widgets.FounBreadcrumbs", array(
      'links'=>$this->breadcrumbs,
      'encodeLabel'=>false
    )); ?>
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> - Programa de iniciativas estudiantiles acad&eacute;micas PIEA.<br/>
	Todos los derechos reservados.<br/>
	Contacto web@piea.usm.cl.<br/>
	</div><!-- footer -->

</div><!-- page -->

<?php if(isset($this->modal) && !empty($this->modal)):?>
<div id="<?php echo $this->modal['id'];?>" class="reveal-modal <?php if(isset($this->modal['size'])) echo $this->modal['size'];?>">
  <?php echo $this->modal['content'];?>
  <?php if($this->modal['close']):?>
    <a class="close-reveal-modal">&#215;</a>
  <?php endif;?>
</div>
<?php endif;?>

</body>
</html>
