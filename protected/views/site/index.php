<?php $this->pageTitle=Yii::app()->name; 
$this->breadcrumbs=array('');
?>
<?php if(Yii::app()->user->isGuest):?>
<div class="row">
  <div class="three columns">
    <div class="panel">
      <h5>Iniciar Sesi&oacute;n</h5>
      <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'login-form',
        'type'=>'nice',
        'action'=>Yii::app()->createUrl('/usuarios/authenticate/login')
      )); ?>
        <br>
        <?php echo $form->textFieldRow($model, 'username',array('style'=>'width:180px;')); ?> 
        <?php echo $form->passwordFieldRow($model, 'password',array('style'=>'width:180px;')); ?>
        <?php echo $form->checkboxRow($model, 'rememberMe'); ?>    
        
        <p>
          <a href="<?php echo Yii::app()->createUrl('/usuarios/userActions/create');?>">&iquest;No tienes cuenta? puedes registrarte aqu&iacute;.</a>
          <br>
          <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/recuperarPassword');?>">Recuperar contrase&ntilde;a</a>
        </p>

        <div class="row buttons">
          <?php echo CHtml::submitButton('Iniciar Sesi&oacute;n',array('class'=>'nice medium radius blue button','encode'=>false)); ?>
        </div>
        
        <?php $this->endWidget(); ?>
    </div>
  </div>
  <div class="nine columns">
    <?php $this->widget("foundation.widgets.FounAlert"); ?>
    <h1>Bienvenido</i></h1>

    <p>Bienvenido al Sistema de Gesti&oacute;n Online de PIE&gt;A, esperamos con esta herramienta facilitar el desarrollo de tus proyectos innovadores.
        <br><br>
        Si tienes algun problema contactanos en 
        <a href="mailto:web@piea.usm.cl">web@piea.usm.cl</a><br>
        Mas informaci&oacute;n sobre PIE&gt;A en  
        <a href="http://www.piea.usm.cl">http://www.piea.usm.cl</a>
    </p>

    <p>
        Se ha dado inicio a la VI convocatoria a fondos de investigaci&oacute;n, desarrollo en innovaci&oacute;n para
        estudiantes de pregrado. La postulaci&oacute;n comienza el dia viernes 22 de marzo y finaliza el 18 de abril
        del 2013.
        <br><br>
        Las bases del concurso las puedes encontrar en: <a href="http://www.piea.usm.cl/bases">http://www.piea.usm.cl/bases</a>
    </p>
  </div>
</div>
<?php else: ?>
  <h1>Bienvenido</i></h1>

  <p>Bienvenido al Sistema de Gesti&oacute;n Online de PIE>A, a tu izquierda encontraras un men&uacute;
      desde el cual podr&aacute;s acceder a las distintas &aacute;reas del sistema.
      <br><br>
      Si tienes algun problema contactanos en
      <a href="mailto:web@piea.usm.cl">web@piea.usm.cl</a><br>
      Mas informaci&oacute;n sobre PIE&gt;A en
      <a href="http://www.piea.usm.cl">http://www.piea.usm.cl</a>
  </p>

    <p>
        Se ha dado inicio a la VI convocatoria a fondos de investigaci&oacute;n, desarrollo en innovaci&oacute;n para
        estudiantes de pregrado. La postulaci&oacute;n comienza el dia viernes 22 de marzo y finaliza el 18 de abril
        del 2013.
        <br><br>
        Las bases del concurso las puedes encontrar en: <a href="http://www.piea.usm.cl/bases">http://www.piea.usm.cl/bases</a>
    </p>
<?php endif;?>
