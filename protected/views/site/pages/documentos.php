<?php
$this->pageTitle=Yii::app()->name . ' - Documentos de inter&eacute;s';
$this->breadcrumbs=array(
	'Documentos de inter&eacute;s',
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Documentos de inter&eacute;s</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <h5>Documentos de inter&eacute;s</h5>
            <p>
                A continuaci&oacute;n se presenta un listado con los documentos de inter&eacute;s para los 
                proyectos PIEA.
            </p>
            <ul>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formato_carta_de_compromiso.pdf">
                    Formato de carta de compromiso</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/manual_de_procedimiento_proyectos_idi2013.pdf">
                    Manual de procedimientos proyectos I+D+i</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/PIEA_bases_idi_v1.pdf">
                    Bases convocatoria I+D+i 2013</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/idi_informe_de_revision_de_hitos.docx">
                        Informe de revisi&oacute;n de hitos</a>
                </li>
            </ul>
            <p>Formatos de rendici&oacute;n:</p>
            <ul>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_rendicion_final/idiposter_formato.pptx">
                    I+D+i poster formato</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_rendicion_final/idireporte_informativo_estilo_paper.doc">
                    I+D+i reporte informativo estilo paper</a>
                </li>
            </ul>
            <p>Formatos de solicitud de fondos:</p>
            <ul>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_solicitud_de_fondos/idi_formato_documento_codeudor.docx">
                    I+D+i formato documento co-deudor</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_solicitud_de_fondos/idi_formato_recibo_de_dinero.docx">
                    I+D+i formato recibo de dinero</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_solicitud_de_fondos/idi_formato_rendicion_de_cuentas.xlsx">
                    I+D+i formato rendici&oacute;n de cuentas</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_solicitud_de_fondos/idi_formulario_solicitud_de_reitemizacion.docx">
                    I+D+i formulario de reitemizaci&oacute;n</a>
                </li>
                <li>- <a href="http://sistemagestionpiea.utfsm.cl/files/formatos_solicitud_de_fondos/idi_instructivo_para_solicitud_de_fondos.pdf">
                    I+D+i instructivo para solicitud de fondos</a>
                </li>
            </ul>
        </div>

    </li>

</ul>
