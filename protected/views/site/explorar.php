<?php
$this->pageTitle = 'Explorar PIEA';
$this->breadcrumbs=array('Explorar PIEA');
?>

<dl class="nice contained tabs">
  <?php if($proyecto==null):?>
    <dd><a href="#explorar-piea" id="explorar-piea" class="active">Explorar PIEA</a></dd>
    <dd><a href="#ver-iniciativa" id="ver-iniciativa">Iniciativa</a></dd>
    <dd><a href="#ver-proyecto" id="ver-proyecto">Proyecto</a></dd>
  <?php else:?>
    <dd><a href="#explorar-piea" id="explorar-piea">Explorar PIEA</a></dd>
    <dd><a href="#ver-iniciativa" id="ver-iniciativa">Iniciativa</a></dd>
    <dd><a href="#ver-proyecto" id="ver-proyecto" class="active">Proyecto</a></dd>
  <?php endif;?>
</dl>

<ul class="nice tabs-content contained">
  <?php if($proyecto==null):?>
  <li class="active" id="explorar-pieaTab">
  <?php else:?>
  <li id="explorar-pieaTab">
  <?php endif;?>
    <h3>Listado de iniciativas</h3>
    
    <?php
      $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'selectableRows'=>0,
        'columns'=>array(
            'nombre_abreviado',
            'descripcion',
            'objetivos_generales',
            array(
              'class'=>'CButtonColumn',
              'template'=>'{my_button}',
              'buttons'=>array(
                  'my_button'=>array(
                    'label'=>'Ver',
                    'url'=>'url("/iniciativas/crudIniciativas/explorar",array("id"=>$data->id))',
                  ),
              ),
          ),
        ),
    ));
    ?>
  </li>
  
  <li id="ver-iniciativaTab">
    <p>Selecciona alguna iniciativa en la pesta&ntilde;a anterior.</p>
  </li>
  
  <?php if($proyecto!==null):?>
    <li id="ver-proyectoTab" class="active">
    <h1><?php echo $proyecto->nombre;?></h1>

    <p>
    <strong>Nombre:</strong> <?php echo $proyecto->nombre;?><br>
    <strong>Iniciativa:</strong> <?php echo $proyecto->iniciativa->nombre_abreviado;?><br>
    <strong>Descripci&oacute;n:</strong> <?php echo $proyecto->descripcion;?><br>
    <strong>Objetivos generales:</strong> <?php echo $proyecto->objetivos_generales;?>
    </p>
    
    <p>
      <?php if($proyecto->jefe_proyecto!=null):?>
        <strong>Jefe de proyecto: </strong><?php echo $proyecto->jefe_proyecto->nombreCompleto;?>
        <br><br>
        <?php if(!Yii::app()->user->checkAccess('integrante_proyecto',array('iniciativa'=>$proyecto->iniciativa_id,'proyecto'=>$proyecto->id))):?>
          <a href="<?php echo Yii::app()->createUrl('/proyectos/gestionIntegrantes/solicitarMembresia',array('id'=>$proyecto->id));?>" class="small nice button">Solicitar participaci&oacute;n</a>
          <br><br>
        <?php endif;?>
        
      <?php endif;?>
    </p>
    
  <?php else:?>
    <li id="ver-proyectoTab">
    <p>Selecciona algun proyecto en la pesta&ntilde;a anterior.</p>
  <?php endif;?>
  </li>

</ul>
