Estimado usuario:<br><br>
Se ha aceptado el formulario de <?php echo $recibo->tipoRecibo;?> que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>. Puedes ver dicho formulario
presionando en el enlace "ver <?php echo $recibo->tipoRecibo;?>".
<?php if($recibo->tipo_recibo==1):?>
 Para continuar con el proceso, debe llevar el formulario impreso
y firmado por la persona que solicito la recuperaci&oacute;n de gastos y las boletas originales.
<?php endif;?>
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRecibo',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver <?php echo $recibo->tipoRecibo;?>
</a>

<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>