Estimado usuario:<br><br>
Se ha subido la cotizaci&oacute;n de los gastos de env&iacute;o para el presupuesto que solicitaste en
 <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>.
Para continuar debes enviar el presupuesto para revisi&oacute;n.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/enviarPresupuestoUnidadAbastecimiento',
    array('id'=>$presupuesto->id));?>" class="nice radius small button">
    Ver presupuesto
</a>

<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>