Estimado usuario:<br><br>
La transferencia de fondos solicitada en
<?php echo $destino->articuloEntidad();?> <?php echo $destino->getTipoEntidad();?> <?php echo $destino->getNombre();?>
 desde <?php echo $origen->articuloEntidad();?> <?php echo $origen->getTipoEntidad();?> <?php echo $origen->getNombre();?>
 por un monto de <?php echo $solicitud->monto;?> hacia la cuenta <b></b><?php echo $solicitud->cuentaDestino->identificador;?></b>
ha sido <?php echo $accion;?> por el administrador de la plataforma.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
