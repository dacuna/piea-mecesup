Estimado usuario:<br><br>
Se ha aceptado el formulario de <?php echo $pago->tipoRecibo;?> que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>. Puedes ver dicho formulario de <?php echo $pago->tipoRecibo;?>
presionando en el enlace "Ver <?php echo $pago->tipoRecibo;?>".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPagoProveedor',
    array('id'=>$pago->id));?>" class="nice radius small button">
    Ver <?php echo $pago->tipoRecibo;?>
</a>

<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>