Estimado usuario:<br><br>
El proyecto <i><?php echo $proyecto->nombre;?></i> que has llenado para postular a los fondos I+D+i ha sido enviado
correctamente para su revisi&oacute;n. Cualquier decisi&oacute;n tomada sobre tu proyecto te ser&aacute; notificada
debidamente.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A
