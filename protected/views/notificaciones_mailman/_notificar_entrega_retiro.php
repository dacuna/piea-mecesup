Estimado administrador:<br><br>
Se te ha notificado de la entrega (o pronta entrega) de un formulario de retiro de fondos para un <?php echo $recibo->tipoRecibo;?>
 en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>.
Puedes ver dicho formulario presionando en el enlace "Ver retiro de fondos". Para notificar que has
recibido el formulario correctamente, puedes ingresar en "Ver retiro de fondos" y luego presionar en
el bot&oacute;n "Notificar recepci&oacute;n de retiro".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRetiroFondos',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver retiro de fondos
</a>