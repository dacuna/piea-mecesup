Estimado usuario:<br><br>
Se ha rechazado el formulario de <?php echo $recibo->tipoRecibo;?> que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>
<?php if(isset($presupuesto->comentario_revisor)):?>
 por el siguiente motivo:
<code>
    <?php echo $recibo->comentario_revisor;?>
</code>
<?php endif;?>
. Puedes ver dicho formulario
presionando en el enlace "ver <?php echo $recibo->tipoRecibo;?>".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRecibo',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver <?php echo $recibo->tipoRecibo;?>
</a>