Estimado usuario:<br><br>
El usuario <?php echo user()->nombre;?>, te ha agregado como integrante en el proyecto <?php echo $proyecto->nombre;?>,
el cual se encuentra en proceso de postulaci&oacute;n para los fondos concursables I+D+i. Tambi&eacute;n ha indicado
que posees los siguientes cargos y responsabilidades dentro del proyecto:
<br>
<ul>
    <li><b>Nombre integrante</b>: <?php echo $integrante->usuario->nombreCompleto;?> (T&uacute;)</li>
    <li><b>&iquest;Es l&iacute;der?</b>: <?php if($integrante->lider==1){echo "Si";}else{echo "no";};?></li>
    <li><b>&iquest;Cargo en finanzas?</b>: <?php if($integrante->finanzas==1){echo "Si";}else{echo "no";};?></li>
    <li><b>Responsabilidades</b>: <?php echo $integrante->responsabilidades;?></li>
</ul>
<br>
    Para aceptar ser parte del proyecto con los cargos y responsabilidades mencionadas, debes presionar el bot&oacute;n
"Aceptar membres&iacute;a", en caso contrario puedes presionar en "Rechazar membres&iacute;a".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A
<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/formularios/postulacionFondosIDI/aceptarMembresia',
    array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid,'i'=>$integrante->id));?>" class="nice radius small button">
    Aceptar membres&iacute;a
</a>
<a href="<?php echo app()->createAbsoluteUrl('/formularios/postulacionFondosIDI/rechazarMembresia',
    array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid,'i'=>$integrante->id));?>" class="nice radius small red button">
    Rechazar membres&iacute;a
</a>
