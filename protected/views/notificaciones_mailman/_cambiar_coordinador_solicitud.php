Estimado <?php echo $usuario->nombres;?>:<br><br>Has sido invitado a formar parte
de la coordinaci&oacute;n de la iniciativa <?php echo $iniciativa->nombre_abreviado;?>.
Para aceptar la invitaci&oacute;n presiona en el bot&oacute;n "Aceptar invitaci&oacute;n,
para rechazarla presiona en "Rechazar Invitaci&oacute;n" <br><br>

Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/iniciativas/administracion/aceptarSolicitudCambioCoordinacion',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small button">
    Aceptar Invitaci&oacute;n
</a>
<a href="<?php echo app()->createAbsoluteUrl('/iniciativas/administracion/rechazarSolicitudCambioCoordinacion',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small red button">
    Rechazar Invitaci&oacute;n
s</a>
