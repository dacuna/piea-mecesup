Estimado usuario:<br><br>
Como encargado de la cuenta <?php echo $presupuesto->cuenta->identificador;?>, se te notifica que se ha aceptado un
 presupuesto solicitado en <?php echo $entidad->articuloEntidad();?>
 <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?> por un monto de
 <?php echo $presupuesto->totalMontoItems;?>.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>