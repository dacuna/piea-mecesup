Estimado usuario:<br><br>
Se ha aceptado el presupuesto que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>. Puedes ver dicho presupuesto
presionando en el enlace "ver presupuesto".
<?php if($presupuesto->unidad_de_abastecimiento!=null):?>
Si el presupuesto est&aacute; destinado a un formulario de unidad de abastecimiento, entonces se le ha enviado una
notificaci&oacute;n al administrador del sistema para que suba la factura correspondiente y apruebe el formulario.
Ante cualquier acci&oacute;n, ser&aacute;s notificado del estado del formulario de unidad de abastecimiento solicitado.
<?php endif;?>
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPresupuesto',
    array('id'=>$presupuesto->id));?>" class="nice radius small button">
    Ver presupuesto
</a>

<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>