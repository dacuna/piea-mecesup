Estimado usuario:<br><br>
Se ha rechazado el presupuesto que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>
<?php if(isset($presupuesto->comentario_revisor)):?>
por el siguiente motivo:
<code>
    <?php echo $presupuesto->comentario_revisor;?>
</code>
<?php endif;?>
. Puedes ver dicho presupuesto
presionando en el enlace "ver presupuesto".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPresupuesto',
    array('id'=>$presupuesto->id));?>" class="nice radius small button">
    Ver presupuesto
</a>