Estimado usuario:<br><br>
Se te ha aceptado el formulario de retiro de fondos para un <?php echo $recibo->tipoRecibo;?>
 en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?> que
ha sido entregado en las oficinas de PIEA. Puedes ver dicho formulario presionando en el enlace "Ver retiro de fondos". Ahora,
puedes crear un vale de rendici&oacute;n desde el retiro aceptado. Para esto dirigete a "Ver retiro de fondos".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRetiroFondos',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver retiro de fondos
</a>