Estimado usuario:<br><br>
Se solicita que subas la factura de la solicitud realizada en
 <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>, para
un formulario de unidad de abastecmiento. Puedes subir dicha factura presionando en "Subir Factura".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/crearPagoProveedores',
    array('id'=>$entidad->getId(),'tipo'=>$entidad->getTipoEntidadNumerico(),'tf'=>2,'u'=>$presupuesto->unidad_de_abastecimiento));?>"
   class="nice radius small button">
    Subir Factura
</a>
