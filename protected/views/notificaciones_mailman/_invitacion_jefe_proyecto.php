Estimado(a) <?php echo $usuario->nombrePresentacion;?>:<br><br>Has sido invitado a tomar el cargo de
jefe de proyecto en el proyecto <?php echo $proyecto->nombre;?>.
 Para aceptar la invitaci&oacute;n presiona en el bot&oacute;n "Aceptar invitaci&oacute;n,
 para rechazarla presiona en "Rechazar Invitaci&oacute;n" <br><br>

Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/proyectos/gestionIntegrantes/aceptarInvitacion',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small button">
    Aceptar Invitaci&oacute;n
</a>
<a href="<?php echo app()->createAbsoluteUrl('/proyectos/gestionIntegrantes/rechazarSolicitud',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small red button">
    Rechazar Invitaci&oacute;n
</a>
