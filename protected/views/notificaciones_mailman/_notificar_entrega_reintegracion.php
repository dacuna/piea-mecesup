Estimado administrador:<br><br>
Se te ha notificado de la entrega (o pronta entrega) de un documento de reintegraci&oacute;n de fondos para un
 <?php echo $recibo->tipoRecibo;?> en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?>
 <?php echo $entidad->getNombre();?>.
Puedes ver dicho documento presionando en el enlace "Ver <?php echo $recibo->tipoRecibo;?>". Para notificar que has
recibido el formulario correctamente, puedes ingresar en "Ver <?php echo $recibo->tipoRecibo;?>" y luego presionar en
el bot&oacute;n "Notificar recepci&oacute;n de documento de reintegraci&oacute;n".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRecibo',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver <?php echo $recibo->tipoRecibo;?>
</a>