Estimado usuario:<br><br>
Como encargado de <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>
 se te ha solicitado aprobar un formulario de <?php echo $tipo;?>. Puedes ver dicho formulario presionando en el enlace
"ver <?php echo $tipo;?>".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRecibo',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver <?php echo $tipo;?>
</a>