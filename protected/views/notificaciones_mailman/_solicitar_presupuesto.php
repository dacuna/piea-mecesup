Estimado usuario:<br><br>
<?php if($encargado=='entidad'):?>
Como encargado (o superior) de <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?>
 <?php echo $entidad->getNombre();?>
<?php else:?>
Como responsable de la cuenta <?php echo $presupuesto->cuenta->identificador;?>,
<?php endif;?>
 se te ha solicitado aprobar un presupuesto de art&iacute;culos. Puedes ver dicho
 presupuesto presionando en el enlace "ver presupuesto".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPresupuesto',
    array('id'=>$presupuesto->id));?>" class="nice radius small button">
    Ver presupuesto
</a>