Estimado usuario:<br><br>
El usuario <?php echo $usuario->nombrePresentacion;?> ha solicitado la realizaci&oacute;n de una transferencia de fondos a
<?php echo $destino->articuloEntidad();?> <?php echo $destino->getTipoEntidad();?> <?php echo $destino->getNombre();?>
 desde <?php echo $origen->articuloEntidad();?> <?php echo $origen->getTipoEntidad();?> <?php echo $origen->getNombre();?>
 por un monto de <?php echo $solicitud->monto;?> hacia la cuenta <b></b><?php echo $solicitud->cuentaDestino->identificador;?></b>.
Para que la transferencia se realice con &eacute;xito debes aprobarla.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>

<a href="<?php echo app()->createAbsoluteUrl('/finanzas/crud/contestarSolicitudTransferencia',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid,'s'=>$solicitud->id,'a'=>1));?>" class="nice radius small button">
    Aceptar transferencia
</a>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/crud/contestarSolicitudTransferencia',array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid,'s'=>$solicitud->id,'a'=>0));?>" class="nice radius small red button">
    Rechazar transferencia
</a>