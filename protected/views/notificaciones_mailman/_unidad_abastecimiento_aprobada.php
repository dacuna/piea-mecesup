Estimado usuario:<br><br>
Se ha aprobado el formulario de unidad de abastecimiento solicitado en
 <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>.
 Puedes ver dicho formulario presionando en el enlace "Ver unidad de abastecimiento".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPagoProveedor',
    array('id'=>$pago->id));?>" class="nice radius small button">
    Ver unidad de abastecimiento
</a>
<a href="<?php echo app()->createAbsoluteUrl('/buzon/inbox/index');?>" class="nice radius small button">
    Ok
</a>