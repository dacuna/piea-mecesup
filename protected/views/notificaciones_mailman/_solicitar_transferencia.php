Estimado usuario:<br><br>Se te solicita tomar el cargo de <?php echo am()->getAuthItem($cargo->nombre_item)->getDescription();?>
en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>.
Para aceptar la invitaci&oacute;n presiona en el bot&oacute;n "Aceptar solicitud,
para rechazarla presiona en "Rechazar solicitud". <br><br>

Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/usuarios/cargos/aceptarTransferenciaCargo',
    array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small button">
    Aceptar solicitud
</a>
<a href="<?php echo app()->createAbsoluteUrl('/usuarios/cargos/rechazarTransferenciaCargo',
    array('e'=>$mensaje->emisor,'d'=>$destinatario,'uid'=>$uid));?>" class="nice radius small red button">
    Rechazar solicitud
</a>
