Estimado usuario:<br><br>
El documento de reintegraci&oacute;n de gastos entregado en oficinas de PIEA, ha sido recibido correctamente, por lo que
se te ha aceptado el <?php echo $recibo->tipoRecibo;?>
 en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?> que
ha sido entregado en las oficinas de PIEA. Puedes ver dicho <?php echo $recibo->tipoRecibo;?> presionando en el enlace
 "Ver <?php echo $recibo->tipoRecibo;?>"
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verRecibo',
    array('id'=>$recibo->id));?>" class="nice radius small button">
    Ver <?php echo $recibo->tipoRecibo;?>
</a>