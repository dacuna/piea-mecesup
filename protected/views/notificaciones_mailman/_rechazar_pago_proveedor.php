Estimado usuario:<br><br>
Se ha rechazado el formulario de <?php echo $pago->tipoRecibo;?> que solicitaste en <?php echo $entidad->articuloEntidad();?>
<?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?>
<?php if(isset($pago->comentario_revisor)):?>
por el siguiente motivo:
<code>
    <?php echo $pago->comentario_revisor;?>
</code>
<?php endif;?>
. Puedes ver dicho formulario de <?php echo $pago->tipoRecibo;?>
presionando en el enlace "Ver <?php echo $pago->tipoRecibo;?>".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPagoProveedor',
    array('id'=>$pago->id));?>" class="nice radius small button">
    Ver <?php echo $pago->tipoRecibo;?>
</a>