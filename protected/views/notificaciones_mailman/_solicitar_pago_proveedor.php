Estimado administrador:<br><br>
Se te ha notificado de la entrega de una factura por <?php echo $pago->tipoRecibo;?>
 en <?php echo $entidad->articuloEntidad();?> <?php echo $entidad->getTipoEntidad();?>
 <?php echo $entidad->getNombre();?>.
Puedes ver dicho documento y ya sea aceptarlo o rechazarlo, presionando en el enlace "Ver <?php echo $pago->tipoRecibo;?>".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A<br><br>
<a href="<?php echo app()->createAbsoluteUrl('/finanzas/operaciones/verPagoProveedor',
    array('id'=>$pago->id));?>" class="nice radius small button">
    Ver <?php echo $pago->tipoRecibo;?>
</a>