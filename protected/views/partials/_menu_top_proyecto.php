<!--
0 => proyecto
1 => perfil
2 => actividades
3 => equipo
4 => hitos
5 => carta gantt
6 => presupuesto
7 => fae
-->
<dl class="nice contained tabs">
    <?php if($entidad->getTipoEntidadNumerico()==1 && $entidad->getEntidad()->es_postulacion==1 &&
        $entidad->getEntidad()->estado_postulacion==0):?>

        <dd>
            <a href="<?php echo app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" <?php if($selected==0){echo 'class="active"';}?>>
                Proyecto
            </a>
        </dd>

    <?php else:?>

        <dd>
            <a href="<?php echo app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" <?php if($selected==0){echo 'class="active"';}?>>
                Proyecto
            </a>
        </dd>

        <?php if($entidad->checkPermisosBoolean('integrante',user()->id) || user()->checkAccess("ayudante_hitos_proyecto_{$entidad->id}")):?>
            <dd>
                <a href="<?php echo app()->createUrl('/proyectos/perfil/index', array('id' => $entidad->id));?>" <?php if($selected==1){echo 'class="active"';}?>>
                    Perfil
                </a>
            </dd>

            <dd>
                <a <?php if($selected==2){echo 'class="active"';}?> href="<?php echo app()->createUrl('/tareas/actividadesEntidad/verActividades', array('id' => $entidad->id, 'tipo' => $entidad->getTipoEntidadNumerico()));?>">
                    Actividades
                </a>
            </dd>

            <dd>
                <a <?php if($selected==3){echo 'class="active"';}?> href="<?php echo app()->createUrl('/proyectos/perfil/integrantes', array('id' => $entidad->id));?>">
                    Equipo
                </a>
            </dd>

        <?php endif;?>

        <?php if($entidad->checkPermisosBoolean('integrante',user()->id) || user()->checkAccess("ayudante_hitos_proyecto_{$entidad->id}")):?>
            <dd>
                <a <?php if($selected==4){echo 'class="active"';}?> href="<?php echo app()->createUrl('/proyectos/perfil/verHitos', array('id' => $entidad->id));?>">
                    Hitos
                </a>
            </dd>
            <dd>
                <a <?php if($selected==5){echo 'class="active"';}?> href="<?php echo app()->createUrl('/proyectos/perfil/verCartaGantt', array('id' => $entidad->id));?>">
                    Carta Gantt
                </a>
            </dd>
            <dd>
                <a <?php if($selected==6){echo 'class="active"';}?> href="<?php echo app()->createUrl('/proyectos/perfil/verPresupuesto', array('id' => $entidad->id));?>">
                    Presupuesto
                </a>
            </dd>
        <?php endif;?>

        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <dd>
                <a <?php if($selected==7){echo 'class="active"';}?> href="<?php echo app()->createUrl('/usuarios/fae/resumenFae', array('id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));?>">
                    FAE
                </a>
            </dd>
        <?php endif;?>

    <?php endif;?>

</dl>