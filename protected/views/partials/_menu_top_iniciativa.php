<!--
0 => proyecto
1 => perfil
2 => actividades
3 => equipo
4 => hitos
5 => carta gantt
6 => presupuesto
7 => fae
-->
<dl class="nice contained tabs">
        <dd>
            <a href="<?php echo app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" <?php if($selected==0){echo 'class="active"';}?>>
                Ver Iniciativa
            </a>
        </dd>

        <?php if($entidad->checkPermisosBoolean('integrante',user()->id)):?>

            <dd>
                <a <?php if($selected==2){echo 'class="active"';}?> href="<?php echo app()->createUrl('/tareas/actividadesEntidad/verActividades', array('id' => $entidad->id, 'tipo' => $entidad->getTipoEntidadNumerico()));?>">
                    Actividades
                </a>
            </dd>

            <dd>
                <a <?php if($selected==3){echo 'class="active"';}?> href="<?php echo app()->createUrl('/iniciativas/perfil/integrantes', array('id' => $entidad->id));?>">
                    Equipo
                </a>
            </dd>

        <?php endif;?>


        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <dd>
                <a <?php if($selected==7){echo 'class="active"';}?> href="<?php echo app()->createUrl('/usuarios/fae/resumenFae', array('id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));?>">
                    FAE
                </a>
            </dd>
        <?php endif;?>

</dl>