<?php
$this->pageTitle = 'Cuentas';
$this->breadcrumbs=array(
    'Cuentas'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Cuentas</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Cuentas</h5>
            <p>A continuaci&oacute;n se presenta el listado de cuentas:</p>

            <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider'=>$dataProvider,
                    'columns'=>array(
                    'id',          // display the 'title' attribute
                    'identificador',  // display the 'name' attribute of the 'category' relation
                    'saldo',   // display the 'content' attribute as purified HTML
                    array('name'=>'Iniciativa/Proyecto','value'=>'$data->entidad->tipoEntidad.": ".$data->entidad->nombre'),
                    array(            // display 'author.username' using an expression
                        'name'=>'Fecha de creac&oacute;n',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_creacion)'),
                    ),
                 ));
            ?>
        </div>

    </li>
</ul>