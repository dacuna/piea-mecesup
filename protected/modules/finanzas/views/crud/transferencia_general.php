<?php
$this->pageTitle = 'Transferencia';
$this->breadcrumbs = array(
    $entidad->tipoEntidad => array($entidad->urlEntidad(), 'id' => $entidad->id),
    $entidad->getNombre() => array($entidad->urlEntidad(), 'id' => $entidad->id),
    'Transferencia'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Transferencia</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Transferencia</h5>

            <p>Para realizar una transferencia de dinero, selecciona la cuenta de origen y la iniciativa o proyecto
                hacia donde
                quieres realizar la transferencia.</p>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm', array('type' => 'nice')); ?>
            <?php echo $form->errorSummary($model);?>

            <label for="TransferenciaGeneral_cuenta_origen">Cuenta origen:</label>
            <select name="TransferenciaGeneral[cuenta_origen]" id="TransferenciaGeneral_cuenta_origen">
                <?php foreach ($cuentas_acceso as $cuenta): ?>
                <option value="<?php echo $cuenta[0]->id;?>" data-tipo="<?php echo $cuenta[1];?>">
                    <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                </option>
                <?php endforeach;?>
            </select>
            <?php echo $form->hiddenField($model, 'tipo_cuenta_origen');?>

            <div class="row">
                <div class="six columns">
                    Iniciativa:
                    <ul id="iniciativas-choose">
                    </ul>
                </div>

                <div class="six columns">
                    Proyecto:
                    <ul id="proyectos-choose">
                    </ul>
                </div>
            </div>
            <p id="mensaje-entidad">
                Debes seleccionar una iniciativa o proyecto como destino de la entrega.
            </p>
            <?php echo $form->hiddenField($model, 'entidad_id_destino');?>
            <?php echo $form->hiddenField($model, 'tipo_entidad_destino');?>

            <label for="TransferenciaGeneral_cuenta_destino">Cuenta destino:</label>
            <select name="TransferenciaGeneral[cuenta_destino]" id="TransferenciaGeneral_cuenta_destino">
            </select>

            <?php echo $form->textFieldRow($model,'monto');?>

            <br>
            <?php echo CHtml::submitButton('Realizar transferencia', array('class' => 'nice radius medium button', 'encode' => false)); ?>
            <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(), array('id' => $entidad->id));?>"
               class="nice radius medium red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>

<div style="display: none;">
    <ul id="iniciativas-list">
        <?php foreach (user()->iniciativas() as $iniciativa): ?>
        <li data-id="<?php echo $iniciativa->id;?>"><?php echo $iniciativa->nombre_abreviado;?></li>
        <?php endforeach;?>
        <?php foreach (user()->proyectos() as $proyecto): ?>
        <li data-id="<?php echo $proyecto->iniciativa->id;?>"><?php echo $proyecto->iniciativa->nombre_abreviado;?></li>
        <?php endforeach;?>
    </ul>

    <ul id="proyectos-list">
        <?php foreach (user()->proyectos() as $proyecto): ?>
        <li data-id="<?php echo $proyecto->id;?>">
            <span class="padre"><?php echo $proyecto->iniciativa->id;?></span>
            <span class="nombre"><?php echo $proyecto->nombre;?></span>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">
    $(document).ready(function () {
        //codigo final para filtros de entidades
        var mensajes = ["Debes seleccionar una iniciativa o proyecto como destino de la entrega", "Has seleccionado el proyecto ", "Has seleccionado la iniciativa "];
        var iniciativas = [];
        var proyectos = [];
        $('#iniciativas-list li').each(function () {
            iniciativas.push({'id':$(this).attr('data-id'), 'name':$(this).html()});
        });
        //en iniciativas_final estan las iniciativas filtradas (sin repeticion)
        iniciativas = _.groupBy(iniciativas, function (iniciativa) {
            return iniciativa.name
        });
        _.each(iniciativas, function (elem, key) {
            iniciativas[key] = _.uniq(elem, false, function (value) {
                return value.name
            });
        });
        //para los proyectos
        $('#proyectos-list li').each(function () {
            var name = $(this).children('.nombre').first().html();
            var padre = $(this).children('.padre').first().html();
            proyectos.push({'id':$(this).attr('data-id'), 'name':name, 'padre':padre});
        });
        //se llenan las listas con la informacion correspondiente
        _.each(iniciativas, function (iniciativa, key) {
            elem = '<a href="#" class="filter-proyecto iniciativa-select" data-id="' + iniciativa[0].id + '">' + iniciativa[0].name + '</a>';
            $('#iniciativas-choose').append('<li>' + elem + '</li>');
        });
        //handler para filtrar los proyectos
        $('.filter-proyecto').click(function (e) {
            e.preventDefault();
            $('#proyectos-choose').empty(); //se eliminan todos los proyectos en la lista
            var id = $(this).attr('data-id');
            _.each(proyectos, function (proyecto, key) {
                if (proyecto.padre == id) {
                    elem = '<a href="#" class="proyecto-select" data-id="' + proyecto.id + '">' + proyecto.name + '</a>';
                    $('#proyectos-choose').append('<li>' + elem + '</li>');
                    $('.proyecto-select').bind('click', proyectoSelectClickHandler);
                }
            });
        });
        //dos handlers mas, uno para cuando se selecciona una iniciativa, y el otro para cuando se selecciona un proyecto
        function changeDataStyle(elem) {
            //se remueven los estilos de seleccion de los hermanos
            $(elem).parent().siblings().removeAttr('style');
            $(elem).parent().siblings().children().removeAttr('style');
            //se agregan los estilos de seleccion
            $(elem).parent().css('background-color', '#00A6FC');
            $(elem).css('color', 'white');
        }

        $('.iniciativa-select').click(function (e) {
            e.preventDefault();
            changeDataStyle(this);
            //se debe actualizar el mensaje y los inputs correspondientes
            $('#mensaje-entidad').html(mensajes[2] + '<b>' + $(this).html() + '</b>');
            $('#TransferenciaGeneral_entidad_id_destino').val($(this).attr('data-id'));
            $('#TransferenciaGeneral_tipo_entidad_destino').val('0');

            //cargo las cuentas de esta entidad
            var text="<?php echo url('/finanzas/crud/cargarCuentasAcceso',array('id'=>'REPLACE1','tipo'=>'REPLACE2'));?>";
            var final=text.replace('REPLACE1',$('#TransferenciaGeneral_entidad_id_destino').val());
            final=final.replace('REPLACE2',$('#TransferenciaGeneral_tipo_entidad_destino').val());
            $.ajax({
                url: final,
                success: function(msg){
                    $("#TransferenciaGeneral_cuenta_destino").html(msg);
                    $('#TransferenciaGeneral_tipo_cuenta_destino').val($('#TransferenciaGeneral_cuenta_destino').find(":selected").attr('data-tipo'));
                }
            });
        });
        function proyectoSelectClickHandler(e) {
            e.preventDefault();
            changeDataStyle(this);
            //se debe actualizar el mensaje y los inputs correspondientes
            $('#mensaje-entidad').html(mensajes[1] + '<b>' + $(this).html() + '</b>');
            $('#TransferenciaGeneral_entidad_id_destino').val($(this).attr('data-id'));
            $('#TransferenciaGeneral_tipo_entidad_destino').val('1');

            //cargo las cuentas de esta entidad
            var text="<?php echo url('/finanzas/crud/cargarCuentasAcceso',array('id'=>'REPLACE1','tipo'=>'REPLACE2'));?>";
            var final=text.replace('REPLACE1',$('#TransferenciaGeneral_entidad_id_destino').val());
            final=final.replace('REPLACE2',$('#TransferenciaGeneral_tipo_entidad_destino').val());
            $.ajax({
                url: final,
                success: function(msg){
                    $("#TransferenciaGeneral_cuenta_destino").html(msg);
                    $('#TransferenciaGeneral_tipo_cuenta_destino').val($('#TransferenciaGeneral_cuenta_destino').find(":selected").attr('data-tipo'));
                }
            });
        }

        //por ultimo, si hay errores de validacion, mantener seleccionada la entidad correspondiente
        var selected_in = $('#TransferenciaGeneral_entidad_id_destino').val();
        var tipo_selected = $('#TransferenciaGeneral_tipo_entidad_destino').val();
        if (tipo_selected == '0')
            $('.iniciativa-select[data-id=' + selected_in + ']').trigger('click');
        if (tipo_selected == '1') {
            //se deben lanzar dos handlers, el de la iniciativa y el del proyecto
            var proyecto = _.find(proyectos, function (proyecto) {
                return proyecto.id == selected_in;
            });
            $('.iniciativa-select[data-id=' + proyecto.padre + ']').trigger('click');
            $('.proyecto-select[data-id=' + proyecto.id + ']').trigger('click');
        }

        $('#TransferenciaGeneral_tipo_cuenta_origen').val($('#TransferenciaGeneral_cuenta_origen').find(":selected").attr('data-tipo'));
        $("#TransferenciaGeneral_cuenta_origen").change(function () {
            $('#TransferenciaGeneral_tipo_cuenta_origen').val($(this).find(":selected").attr('data-tipo'));
        });

        $("#TransferenciaGeneral_cuenta_destino").change(function () {
            $('#TransferenciaGeneral_tipo_cuenta_destino').val($(this).find(":selected").attr('data-tipo'));
        });
    });
</script>