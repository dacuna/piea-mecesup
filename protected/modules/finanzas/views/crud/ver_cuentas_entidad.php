<?php
$this->pageTitle = 'Cuentas disponibles';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Cuentas disponibles'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Cuentas</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Cuentas disponibles</h5>
            <p>A continuaci&oacute;n se presenta un listado con las cuentas con saldo disponible para
            <?php echo $entidad->articuloEntidad().' '.$entidad->getTipoEntidad().' '.$entidad->getNombre();?>.
            </p>

            <table>
                <thead>
                    <tr>
                        <th>Cuenta</th>
                        <th>Saldo ($)</th>
                        <th>Movimientos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($cuentas as $cuenta):?>
                        <tr>
                            <td><?php echo ($cuenta[1]=='cuenta')? $cuenta[0]->identificador :$cuenta[0]->cuenta->identificador;?></td>
                            <td style="text-align: right;">$<?php echo app()->NumberFormatter->formatDecimal($cuenta[0]->saldo);?></td>
                            <td><a href="<?php echo url('/finanzas/historico/movimientosCuenta',array(
                                'c'=>($cuenta[1]=='cuenta')? $cuenta[0]->id :$cuenta[0]->id_cuenta,
                                'id'=>$entidad->getId(),'tipo'=>$entidad->getTipoEntidadNumerico()
                            ));?>">Ver movimientos</a></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>

    </li>
</ul>