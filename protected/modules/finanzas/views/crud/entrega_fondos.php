<?php
$this->pageTitle = 'Entrega de fondos';
$this->breadcrumbs=array(
    'Cuentas'=>array('/finanzas/crud/index'),
    'Entrega de fondos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Entrega de fondos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Entrega de fondos</h5>
            <p>Al realizar una entrega de fondos, parte del saldo de la cuenta especificada pasa a ser manejado por
               ya sea una iniciativa o proyecto elegido. El saldo entregado se descuenta de la cuenta seleccionada.
            </p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
            'id'=>'cambiarPassForm-form',
            'type'=>'nice',
        )); ?>

            <?php echo $form->errorSummary($model);?>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->dropDownListRow($model,'id_cuenta',CHtml::listData(Cuenta::model()->findAll(),'id','identificador')); ?>
                </div>

                <div class="six columns" id="info-cuenta">

                </div>
            </div>


            <div class="row">
                <div class="six columns">
                    Iniciativa:
                    <ul id="iniciativas-choose">
                    </ul>
                </div>

                <div class="six columns">
                    Proyecto:
                    <ul id="proyectos-choose">
                    </ul>
                </div>
            </div>

            <p id="mensaje-entidad">
                Debes seleccionar una iniciativa o proyecto como destino de la entrega.
            </p>
            <?php echo $form->hiddenField($model,'id_entidad');?>
            <?php echo $form->hiddenField($model,'tipo_entidad');?>


            <?php echo $form->textFieldRow($model,'monto'); ?>
            <br>
            <?php echo CHtml::submitButton('Entregar fondos',array('class'=>'nice radius medium button','encode'=>false)); ?>
            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>

<div style="display: none;">
    <ul id="iniciativas-list">
        <?php foreach(user()->iniciativas() as $iniciativa):?>
        <li data-id="<?php echo $iniciativa->id;?>"><?php echo $iniciativa->nombre_abreviado;?></li>
        <?php endforeach;?>
        <?php foreach(user()->proyectos() as $proyecto):?>
        <li data-id="<?php echo $proyecto->iniciativa->id;?>"><?php echo $proyecto->iniciativa->nombre_abreviado;?></li>
        <?php endforeach;?>
    </ul>

    <ul id="proyectos-list">
        <?php foreach(user()->proyectos() as $proyecto):?>
        <li data-id="<?php echo $proyecto->id;?>">
            <span class="padre"><?php echo $proyecto->iniciativa->id;?></span>
            <span class="nombre"><?php echo $proyecto->nombre;?></span>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/underscore-min.js', CClientScript::POS_HEAD);?>
<script type="text/javascript">
    $(document).ready(function(){
        //codigo final para filtros de entidades
        var mensajes=["Debes seleccionar una iniciativa o proyecto como destino de la entrega","Has seleccionado el proyecto ","Has seleccionado la iniciativa "];
        var iniciativas=[]; var proyectos=[];
        $('#iniciativas-list li').each(function(){
            iniciativas.push({'id':$(this).attr('data-id'),'name':$(this).html()});
        });
        //en iniciativas_final estan las iniciativas filtradas (sin repeticion)
        iniciativas=_.groupBy(iniciativas,function(iniciativa){return iniciativa.name});
        _.each(iniciativas,function(elem,key){
            iniciativas[key]=_.uniq(elem,false,function(value){return value.name});
        });
        //para los proyectos
        $('#proyectos-list li').each(function(){
            var name=$(this).children('.nombre').first().html();
            var padre=$(this).children('.padre').first().html();
            proyectos.push({'id':$(this).attr('data-id'),'name':name,'padre':padre});
        });
        //se llenan las listas con la informacion correspondiente
        _.each(iniciativas,function(iniciativa,key){
            elem='<a href="#" class="filter-proyecto iniciativa-select" data-id="'+iniciativa[0].id+'">'+iniciativa[0].name+'</a>';
            $('#iniciativas-choose').append('<li>'+elem+'</li>');
        });
        //handler para filtrar los proyectos
        $('.filter-proyecto').click(function(e){
            e.preventDefault();
            $('#proyectos-choose').empty(); //se eliminan todos los proyectos en la lista
            var id=$(this).attr('data-id');
            _.each(proyectos,function(proyecto,key){
                if(proyecto.padre==id)
                {
                    elem='<a href="#" class="proyecto-select" data-id="'+proyecto.id+'">'+proyecto.name+'</a>';
                    $('#proyectos-choose').append('<li>'+elem+'</li>');
                    $('.proyecto-select').bind('click',proyectoSelectClickHandler);
                }
            });
        });
        //dos handlers mas, uno para cuando se selecciona una iniciativa, y el otro para cuando se selecciona un proyecto
        function changeDataStyle(elem){
            //se remueven los estilos de seleccion de los hermanos
            $(elem).parent().siblings().removeAttr('style');
            $(elem).parent().siblings().children().removeAttr('style');
            //se agregan los estilos de seleccion
            $(elem).parent().css('background-color','#00A6FC');
            $(elem).css('color','white');
        }
        $('.iniciativa-select').click(function(e){
            e.preventDefault();
            changeDataStyle(this);
            //se debe actualizar el mensaje y los inputs correspondientes
            $('#mensaje-entidad').html(mensajes[2]+'<b>'+$(this).html()+'</b>');
            $('#Entrega_id_entidad').val($(this).attr('data-id'));
            $('#Entrega_tipo_entidad').val('0');
        });
        function proyectoSelectClickHandler(e)
        {
            e.preventDefault();
            changeDataStyle(this);
            //se debe actualizar el mensaje y los inputs correspondientes
            $('#mensaje-entidad').html(mensajes[1]+'<b>'+$(this).html()+'</b>');
            $('#Entrega_id_entidad').val($(this).attr('data-id'));
            $('#Entrega_tipo_entidad').val('1');
        }
        //por ultimo, si hay errores de validacion, mantener seleccionada la entidad correspondiente
        var selected_in=$('#Entrega_id_entidad').val();
        var tipo_selected=$('#Entrega_tipo_entidad').val();
        if(tipo_selected=='0')
            $('.iniciativa-select[data-id='+selected_in+']').trigger('click');
        if(tipo_selected=='1')
        {
            //se deben lanzar dos handlers, el de la iniciativa y el del proyecto
            var proyecto=_.find(proyectos,function(proyecto){ return proyecto.id==selected_in;});
            $('.iniciativa-select[data-id='+proyecto.padre+']').trigger('click');
            $('.proyecto-select[data-id='+proyecto.id+']').trigger('click');
        }

        function loadInfoCuenta(id){
            var text="<?php echo url('/finanzas/crud/verCuenta',array('id'=>'REPLACE'));?>";
            $.ajax({
                url: text.replace('REPLACE',id),
                success: function(msg){
                    $("#info-cuenta").html(msg);
                }
            });
        }
        loadInfoCuenta($("#Entrega_id_cuenta").val());

        $("#Entrega_id_cuenta").change(function() {
            var id=$(this).val();
            loadInfoCuenta(id);
        });
    });
</script>