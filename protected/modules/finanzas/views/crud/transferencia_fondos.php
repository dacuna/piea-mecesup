<?php
$this->pageTitle = 'Transferencia de fondos';
$this->breadcrumbs=array(
    'Cuentas'=>array('/finanzas/crud/index'),
    'Transferencia de fondos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Transferencia de fondos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Transferencia de fondos</h5>
            <p>Mediante el siguiente formulario se puede realizar una transferencia de dinero desde una cuenta <i>A</i>
                a una cuenta <i>B</i>.
            </p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('type'=>'nice',)); ?>

            <?php echo $form->errorSummary($model);?>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->dropDownListRow($model,'id_cuenta_origen',CHtml::listData(Cuenta::model()->findAll(),'id','identificador')); ?>
                    <div id="info-cuenta-origen"></div>
                </div>

                <div class="six columns" id="info-cuenta">
                    <?php echo $form->dropDownListRow($model,'id_cuenta_destino',CHtml::listData(Cuenta::model()->findAll(),'id','identificador')); ?>
                    <div id="info-cuenta-destino"></div>
                </div>
            </div>

            <?php echo $form->textFieldRow($model,'monto'); ?>
            <br>
            <?php echo CHtml::submitButton('Transferir fondos',array('class'=>'nice radius medium button','encode'=>false)); ?>
            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>
<script type="text/javascript">
$(function(){
    function loadInfoCuenta(id,update){
        var text="<?php echo url('/finanzas/crud/verCuenta',array('id'=>'REPLACE'));?>";
        $.ajax({
            url: text.replace('REPLACE',id),
            success: function(msg){
                $(update).html(msg);
            }
        });
    }
    loadInfoCuenta($("#Transferencia_id_cuenta_origen").val(),'#info-cuenta-origen');
    loadInfoCuenta($("#Transferencia_id_cuenta_destino").val(),'#info-cuenta-destino');

    $("#Transferencia_id_cuenta_origen").change(function() {
        var id=$(this).val();
        loadInfoCuenta(id,'#info-cuenta-origen');
    });

    $("#Transferencia_id_cuenta_destino").change(function() {
        var id=$(this).val();
        loadInfoCuenta(id,'#info-cuenta-destino');
    });
});
</script>