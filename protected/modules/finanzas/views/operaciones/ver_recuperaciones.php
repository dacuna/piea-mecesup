<?php
$this->pageTitle = 'Ver recuperaciones de gastos';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Ver recuperaciones de gastos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ver recuperaciones de gastos</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ver recuperaciones de gastos</h5>
            <p>A continuaci&oacute;n, se listan todas las recuperaciones de gastos valid&aacute;dadas en esta entidad.
            </p>
            <div class="grid-view">
            <table class="items">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>C&oacute;digo presupuestario</th>
                        <th>Deudor</th>
                        <th>Solicitante</th>
                        <th>Total gastos</th>
                        <th>Estado</th>
                        <th>Descargar</th>
                        <th>Ver</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($recuperaciones as $recuperacion):?>
                    <tr>
                        <td><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",
                            $recuperacion->fecha_vale);?></td>
                        <td><?php echo $recuperacion->codigoPresupuestario->identificador;?></td>
                        <td><?php echo $recuperacion->deudor->nombrePresentacion;?></td>
                        <td><?php echo $recuperacion->solicitante->nombrePresentacion;?></td>
                        <td style="text-align: right;">
                            $<?php echo app()->numberFormatter->formatDecimal($recuperacion->total_gastos);?></td>
                        <td style="text-align: center;"><?php echo $recuperacion->estadoRecibo;?></td>
                        <td style="text-align: center;"><a href="<?php echo url('/finanzas/operaciones/generarRecuperacion',array(
                            'id'=>$recuperacion->id
                        ));?>">PDF</a></td>
                        <td><a href="<?php echo url('/finanzas/operaciones/verRecibo',array('id'=>$recuperacion->id));?>">Ver detalle</a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>

        <div class="panel clearfix">
            <h5>Ver recuperaciones de gastos pendientes de edici&oacute;n</h5>
            <p>A continuaci&oacute;n, se listan todas las recuperaciones de gastos en edici&oacute;n.
            </p>
            <div class="grid-view">
                <table class="items">
                    <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>C&oacute;digo presupuestario</th>
                        <th>Deudor</th>
                        <th>Solicitante</th>
                        <th>Total gastos</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($pendientes as $recuperacion):?>
                    <tr>
                        <td><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",
                            $recuperacion->fecha_vale);?></td>
                        <td><?php echo $recuperacion->codigoPresupuestario->identificador;?></td>
                        <td><?php if(isset($recuperacion->deudor)){echo $recuperacion->deudor->nombrePresentacion;}?></td>
                        <td><?php echo $recuperacion->solicitante->nombrePresentacion;?></td>
                        <td style="text-align: right;">
                            $<?php echo app()->numberFormatter->formatDecimal($recuperacion->total_gastos);?></td>
                        <td><a href="<?php echo url('/finanzas/operaciones/agregarItemsRecuperacion',array('id'=>$recuperacion->id));?>">
                            Editar</a></td>
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>

    </li>
</ul>
<?php
$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
$cssFile=$baseScriptUrl.'/styles.css';
Yii::app()->getClientScript()->registerCssFile($cssFile);
?>
