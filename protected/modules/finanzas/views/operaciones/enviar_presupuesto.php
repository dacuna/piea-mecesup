<?php
$this->pageTitle = 'Enviar presupuesto';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Enviar presupuesto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Enviar presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Enviar presupuesto</h5>

            <p>
                <b>Fecha solicitud presupuesto: </b><?php app()->dateFormatter->format("dd'/'MM'/'y",
                    $presupuesto->fecha_solicitud);?><br>
                <b>Detalle: </b><?php echo $presupuesto->detalle;?><br>
                <b>Usuario que solicita presupuesto: </b><?php echo $presupuesto->solicitante->nombreCompleto;?><br>
                <b>Total presupuesto: </b>$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?>
                <?php if($presupuesto->unidad_de_abastecimiento!=null):?>
                    <?php if($presupuesto->unidadDeAbastecimiento->gastos_envio==0):?>
                        <br><span style="color:red;">Este presupuesto no considera gastos de env&iacute;o.</span>
                    <?php else:?>
                        <br><span style="color:green;">Este presupuesto s&iacute; considera gastos de env&iacute;o.</span>
                    <?php endif;?>
                <?php endif;?>
            </p>

            <h6>Items agregados</h6>

            <?php if(count($presupuesto->items)>0):?>
                <?php $this->widget('ItemsPresupuestoWidget',array('items'=>$presupuesto->items,
                    'presupuesto'=>$presupuesto)); ?>
                <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',array('type' => 'nice')); ?>
                <?php echo CHtml::submitButton('Enviar Presupuesto', array('name'=>'enviarPresupuesto',
                    'class' => 'nice radius medium button', 'encode' => false)); ?>
                <?php $this->endWidget(); ?>
            <?php else:?>
                <p>No se han agregado items al presupuesto actual.</p>
            <?php endif;?>

            <p>Para enviar un presupuesto, debes completar el siguiente formulario:</p>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
                array('type' => 'nice','id' => 'somemodel-form',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

            <h6>Agregar item:</h6><br>

            <?php echo $form->textFieldRow($item,'nombre'); ?>
            <?php echo $form->textFieldRow($item,'precio'); ?>
            <?php echo $form->textFieldRow($item,'unidades'); ?>

            <h6>Presupuestos para el item:</h6>
            <br>
            <?php
            $this->widget( 'xupload.XUpload', array(
                    'url' => Yii::app( )->createUrl( "/finanzas/operaciones/upload"),
                    //our XUploadForm
                    'model' => $boletas,
                    //We set this for the widget to be able to target our own form
                    'htmlOptions' => array('id'=>'somemodel-form'),
                    'attribute' => 'file',
                    'multiple' => true,
                    'autoUpload'=>true,
                    'options'=>array('acceptFileTypes' => "js:/(\.|\/)(pdf)$/i",
                    'maxFileSize'=>'10000000')
                )
            );
            ?>

            <?php echo CHtml::submitButton('Agregar Item', array('class' => 'nice radius medium button', 'encode' => false)); ?>
            <?php $this->endWidget(); ?>

            </div>

        </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">

    function ItemPresupuestoController($scope, $http) {

        $scope.addedItems=[];
        $scope.countItems=1;

        $scope.emptyItem=function(){
            $scope.itemName='';
            $scope.itemPrice='';
            $scope.itemUnits='';
            $scope.itemFile='';
        }

        $scope.addItem=function(){
            var itemExists=_.find($scope.addedItems,function(itemList){return itemList.id==$scope.itemID;});
            if(itemExists==undefined){
                $scope.addedItems.push({id:$scope.countItems,name:$scope.itemName,price:$scope.itemPrice,
                    units:$scope.itemUnits});
                $scope.countItems+=1;
            } else{
                itemExists.name=$scope.itemName;
                itemExists.price=$scope.itemPrice;
                itemExists.units=$scope.itemUnits;
            }
            $scope.emptyItem();
        }

        $scope.deleteItem=function(item){
            var itemAction=_.find($scope.addedItems,function(itemList){return itemList.name==item.name;});
            var items=$scope.addedItems;
            $scope.addedItems=[];
            _.each(items,function(item){if(item.name!=itemAction.name) $scope.addedItems.push(item)});
        }

        $scope.editItem=function(item){
            $scope.itemName=item.name;
            $scope.itemPrice=item.price;
            $scope.itemUnits=item.units;
            $scope.itemID=item.id;
        }

        $scope.addFileToItem=function(){
            console.log($scope.itemFile);
            $scope.filesPerItem.push($scope.itemFile);
            $scope.itemFile='';
        }

    }

</script>
