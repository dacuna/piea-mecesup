<?php
$this->pageTitle = "Ver {$pago->tipoRecibo}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Ver {$pago->tipoRecibo}"
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active"><?php echo "Ver {$pago->tipoRecibo}";?></a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5><?php echo "Ver {$pago->tipoRecibo}";?></h5>
            <p><b>Fecha</b>: <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",
                $pago->fecha_solicitud);?><br>
               <b>Estado</b>: <?php echo $pago->estadoRecibo;?>
                <?php if($pago->estado==-1 && $mostrar_edit):?><br>
                    <a href="<?php echo url('/finanzas/operaciones/editarPagoProveedor',array('id'=>$pago->id));?>">
                    <b>Editar <?php echo $pago->tipoRecibo;?></b></a>
                <?php endif;?>

                <br><b>Presupuestos asociados</b>:
                <?php foreach($pago->presupuestos as $key=>$presupuesto):?>
                    <span title="<?php echo $presupuesto->detalle;?>">
                    <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                        '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)
                    <?php if($key!=count($pago->presupuestos)-1){ echo ',';};?>
                <?php endforeach;?>
                <br><b>N&uacute;mero factura:</b> <?php echo $pago->numero_factura;?>
                <br><b>Monto factura:</b> $<?php echo app()->NumberFormatter->formatDecimal($pago->monto_factura);?>
                <br>
                <b><a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$pago->id,
                    'i'=>null,'f'=>$pago->factura,'folder'=>'proveedores'));?>">Ver factura</a></b>
            </p>


            <?php if($pago->estado==0 && user()->checkAccess('administrador')):?>
            <br>
                <a href="<?php echo url('/finanzas/operaciones/aprobarPagoProveedor',array('id'=>$pago->id));?>"
                   class="nice button">Aprobar <?php echo $pago->tipoRecibo;?></a>
                <a href="#" data-reveal-id="myModal" class="nice red button">Rechazar <?php echo $pago->tipoRecibo;?></a>
                <br><br>
                <?php
                $this->modal['id']='myModal';
                $this->modal['close']=true;

                $this->modal['content']=$this->renderPartial('_rechazar_pago_proveedor',array('pago'=>$pago),true)
                ?>
            <?php endif;?>

        </div>

    </li>
</ul>
