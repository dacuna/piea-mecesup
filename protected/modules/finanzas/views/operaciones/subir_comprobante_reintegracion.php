<?php
$this->pageTitle = "Subir comprobante de reintegraci&oacute;n";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Subir comprobante de reintegraci&oacute;n"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Subir comprobante de reintegraci&oacute;n</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Subir comprobante de reintegraci&oacute;n</h5>

            <p>Al enviar el documento de comprobante de reintegraci&oacute;n de gastos, el administrador ser&aacute;
                notificado y revisar&aacuta; dicho documento. Una vez aceptado &eacute;ste, el vale a rendir tambi&eacute;n
                pasar&aacute; a ser aceptado.

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
                array('type' => 'nice','id' => 'somemodel-form',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

            Documento de comprobante de reintegraci&oacute;n:
                <?php
                    $this->widget('xupload.XUpload', array(
                            'url' => Yii::app()->createUrl( "/finanzas/operaciones/upload",array('folder'=>'recibos')),
                            //our XUploadForm
                            'model' => $boleta,
                            //We set this for the widget to be able to target our own form
                            'htmlOptions' => array('id'=>'somemodel-form'),
                            'attribute' => 'file',
                            'autoUpload'=>true
                        )
                    );
                    ?>

            <?php echo CHtml::submitButton('Enviar documento', array('name'=>'enviar','class' => 'nice radius medium button', 'encode' => false)); ?>

            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>