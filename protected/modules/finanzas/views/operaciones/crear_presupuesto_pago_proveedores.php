<?php
$this->pageTitle = 'Ingresar presupuesto';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Crear presupuesto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ingresar presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ingresar presupuesto</h5>
            <p>Para ingresar un presupuesto debes completar la informaci&oacute;n que se solicita a continuaci&oacute;.
                Luego, se te solicitar&aacute; completar los items que deseas agregar a tu presupuesto.</p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',array('type'=>'nice')); ?>

            <?php echo $form->errorSummary($presupuesto);?>

            <?php echo $form->textFieldRow($presupuesto,'detalle',array('style'=>'width:600px;')); ?>

            <p>El presupuesto a ingresar &iquest;considera gastos de env&iacute;o?
                <br>S&iacute; <input type="radio" name="gastos_envio" value="1"><br>
                No <input type="radio" name="gastos_envio" value="0">
            </p>

            <?php echo CHtml::submitButton('Continuar',array('class'=>'nice radius medium button','encode'=>false)); ?>

            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>