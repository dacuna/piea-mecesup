<?php
$this->pageTitle = 'Ver recibo';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Ver recibo'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Ver recibo</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ver <?php echo $recibo->tipoRecibo;?></h5>
            <p><b>Fecha</b>: <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",
                $recibo->fecha_vale);?><br>
               <b>Estado</b>: <?php echo $recibo->estadoRecibo;?>
                <?php if($recibo->estado==-1 && $mostrar_edit):?><br>
                    <a href="<?php echo url('/finanzas/operaciones/llenarRecibo',array('id'=>$recibo->id));?>">
                    <b>Editar recibo</b></a>
                <?php endif;?>

                <?php if($recibo->estado==6 && user()->checkAccess('administrador')):?>
                    <br>
                    <a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$recibo->id,
                        'i'=>'reintegracion','f'=>$recibo->documentoReintegracion,'folder'=>'recibos'));?>">Ver documento reintegraci&oacute;n</a>
                    <br>
                    <a href="<?php echo url('/finanzas/operaciones/notificarReciboReintegracion',array('id'=>$recibo->id));?>">
                        Notificar recibo correcto de documento de reintegraci&oacute;n de gastos</a>
                <?php endif;?>

                <?php if($recibo->estado==1 || $recibo->estado==5):?>
                    <br>
                    <!--<a href="<?php echo url('/finanzas/operaciones/generarComprobanteRecibo',array('id'=>$recibo->id));?>">
                        Generar Comprobante Interno de Recibo</a>
                    <br>-->
                    <a href="<?php echo url('/finanzas/operaciones/generarRecuperacion',array('id'=>$recibo->id));?>">
                        Generar Recibo de <?php echo $recibo->tipoRecibo;?></a><br>
                    <?php if($recibo->tipo_recibo==1):?>
                    <a href="<?php echo url('/finanzas/operaciones/generarTodos',array('id'=>$recibo->id));?>">
                        Generar todos los documentos</a><br>
                    <?php endif;?>
                    <?php if($recibo->tipo_recibo==2):?>
                        <a href="<?php echo url('/finanzas/operaciones/generarRetiroFondos',array('id'=>$recibo->id));?>">
                            Generar Formulario de Retiro de Fondos</a>
                    <?php endif;?>
                <?php endif;?>
                <br><b>Presupuestos asociados</b>:
                <?php foreach($recibo->presupuestos as $key=>$presupuesto):?>
                    <span title="<?php echo $presupuesto->detalle;?>">
                    <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                        '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)
                    <?php if($key!=count($recibo->presupuestos)-1){ echo ',';};?>
                <?php endforeach;?>
            </p>

            <h5>Items agregados</h5>

            <?php $this->widget('ItemsReciboWidget',array('items'=>$recibo->items,
            'recibo'=>$recibo)); ?>

            <?php if($mostrar_aprobacion && ($recibo->estado==0 || $recibo->estado==4)):?>
                <br>
                <a href="<?php echo url('/finanzas/operaciones/aprobarRecibo',array('id'=>$recibo->id));?>"
                   class="nice button">Aprobar <?php echo $recibo->tipoRecibo;?></a>
                <a href="#" data-reveal-id="myModal" class="nice red button">Rechazar <?php echo $recibo->tipoRecibo;?></a>
                <br><br>
                <?php
                $this->modal['id']='myModal';
                $this->modal['close']=true;

                $this->modal['content']=$this->renderPartial('_rechazar_recibo',array('recibo'=>$recibo),true)
                ?>
            <?php endif;?>

        </div>

    </li>
</ul>
