<style xmlns="http://www.w3.org/1999/html">
    .row:before, .row:after, .clearfix:before, .clearfix:after { content:""; display:table; }
    .row:after, .clearfix:after { clear: both; }
    .row, .clearfix { zoom: 1; }
    .p{margin-top:5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;}
    .top{
        border:1px #000000 solid;width:100%;height: 180px;
    }
</style>
    <div style="text-align: left;margin-top:20px;margin-left:40px;">
        <img src="http://www.exalumnos.utfsm.cl/assets/Logos/MARCA-Color.jpg" width="120" height="65">
    </div>


     <div style="text-align: center;">
         <h2 style="text-decoration:underline;margin-bottom:0px;">RECIBO N° <?php echo $recibo->id;?>/<?php echo date("Y");?></h2>
         <h3 style="margin-top:0px;">COMPROBANTE INTERNO</h3>
     </div>

     <div style="text-align:right;font-size: 16px;margin-right:80px;margin-top:50px;">
         <b>TOTAL</b> <span style="text-decoration: underline;">&nbsp;&nbsp;<b>$<?php echo
            app()->NumberFormatter->formatDecimal($recibo->monto_vale);?>.-</b>&nbsp;&nbsp;</span>
     </div>

<table style="border:0px;margin-top: 30px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="30" style="border:0px;">
            </td>

        <td width="570" height="30" style="border:0px;">
            <p style="font-size: 16px;text-align: justify;line-height: 1.5;">
                He recibido de la Universidad T&eacute;cnica Federico Santa Mar&iacute;a la suma de:
                <span style="text-decoration: underline;"><?php echo num2letras($recibo->monto_vale);?></span>.
                <br><br>
                <b>Por lo siguiente:</b> <?php echo $recibo->nota;?>.
            </p>
        </td>

        <td width="50" height="30" style="border:0px;"></td>
    </tr>
</table>

<table style="border:0px;margin-top: 50px;margin-left:60px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="250" height="40" style="border:1px #000000 solid;text-align: center;font-size:14px;">
            <b>Contabilizaci&oacute;n</b>
        </td>

        <td width="50" height="40" style="border:0px;"></td>

        <td width="380" height="40" style="border:0px;"></td>

        <td width="50" height="40" style="border:0px;"></td>
    </tr>
    <tr>
        <td width="250" height="20" style="border:0px;border-left:1px #000000 solid;border-right:1px #000000 solid;">
            &nbsp;&nbsp;&nbsp;&nbsp;<b>D&eacute;bito</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>Cr&eacute;dito</b>
        </td>

        <td width="50" height="20" style="border:0px;"></td>

        <td width="380" height="20" style="border:0px;"></td>

        <td width="50" height="20" style="border:0px;"></td>
    </tr>
    <tr>
        <td width="250" height="20" style="border:1px #000000 solid;">
        </td>

        <td width="50" height="60" style="border:0px;"></td>

        <td width="380" height="60" style="border:0px;"></td>

        <td width="50" height="60" style="border:0px;"></td>
    </tr>
</table>

<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="5" style="border:0px;">
        </td>

        <td width="380" height="5" style="border:0px;">
        </td>

        <td width="250" height="5" style="border:0px;text-align:center;font-size:16px;">
            <b><?php echo $usuario->perfil->rut;?></b><br>
            <b><?php echo $usuario->nombreCompleto;?></b>
        </td>
        <td width="50" height="5" style="border:0px;"></td>
    </tr>
</table>


<table style="margin-left:60px;border:1px #000000 solid;margin-top: 60px;font-size:16px;text-align:center;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="210" height="60" style="border-right:1px #000000 solid;">
            <b>CUENTA <?php echo $recibo->codigoPresupuestario->numero;?></b>
        </td>

        <td width="210" height="60" style="border-right:1px #000000 solid;">
        </td>

        <td width="210" height="60">
        </td>
    </tr>
    <tr>
        <td width="210" height="40" style="border-right:1px #000000 solid;border-top:1px #000000 solid;">
            Unidad Presupuestaria
        </td>

        <td width="210" height="40" style="border-right:1px #000000 solid;border-top:1px #000000 solid;">
            Jefe de Presupuesto
        </td>

        <td width="210" height="40" style="border-top:1px #000000 solid;">
            Direcci&oacute;n de Finanzas
        </td>
    </tr>
</table>

<p style="margin-top:50px;margin-left:60px;font-size: 16px;">VALPARAISO, <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$recibo->fecha_emision_documento);?></p>
