<?php
$this->pageTitle = "Editar {$recibo->tipoRecibo}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $recibo->tipoRecibo =>array('/finanzas/operaciones/agregarItemsRecuperacion','id'=>$recibo->id),
    "Editar {$recibo->tipoRecibo}"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active"><?php echo "Editar {$recibo->tipoRecibo}";?></a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5><?php echo "Editar {$recibo->tipoRecibo}";?></h5>
            <p>Puedes editar la informaci&oacute;n general del recibo de <?php echo $recibo->tipoRecibo;?> mediante
            el siguiente formulario:</p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',array('type'=>'nice')); ?>

            <?php echo $form->errorSummary($recibo);?>

            <label for="Recibo_codigo_presupuestario">C&oacute;digo presupuestario:</label>
            <select name="Recibo[codigo_presupuestario]" id="Recibo_codigo_presupuestario">
                <?php foreach ($cuentas as $cuenta): ?>
                <?php if(($cuenta[1]=='cuenta' && $recibo->codigo_presupuestario==$cuenta[0]->id) ||
                         ($cuenta[1]=='entrega' && $recibo->codigo_presupuestario==$cuenta[0]->cuenta->id)):?>
                    <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id : $cuenta[0]->cuenta->id;?>"
                            selected="selected">
                        <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                    </option>
                <?php else:?>
                    <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id : $cuenta[0]->cuenta->id;?>">
                        <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                    </option>
                <?php endif;?>
                <?php endforeach;?>
            </select>

            <?php echo $form->dropDownListRow($recibo,'id_deudor',
                CHtml::listData($entidad->integrantes,'user_id','usuario.nombrePresentacion'),
                array('options'=>array(user()->id=>array('selected'=>'selected')))); ?>

            <?php echo $form->textFieldRow($recibo,'nota',array('style'=>'width:100%;')); ?>

            <?php echo CHtml::submitButton('Continuar',array('class'=>'nice radius medium button','encode'=>false)); ?>

            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>