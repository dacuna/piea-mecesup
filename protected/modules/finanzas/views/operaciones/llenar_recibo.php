<?php
$this->pageTitle = "Llenar {$recibo->tipoRecibo}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Llenar {$recibo->tipoRecibo}"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active"><?php echo "Llenar {$recibo->tipoRecibo}";?></a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5><?php echo "Llenar {$recibo->tipoRecibo}";?></h5>

            <div class="row">
                <div class="four columns">
                    C&oacute;digo presupuestario: <br>
                    Nombre del deudor:     <br>
                    Monto del vale:        <br>
                    Nota:                  <br>
                    Presupuestos asociados: <br>
                </div>
                <div class="six columns">
                    <?php echo $recibo->codigoPresupuestario->identificador;?><br>
                    <?php echo $recibo->deudor->nombrePresentacion;?><br>
                    $<?php echo app()->NumberFormatter->formatDecimal($recibo->monto_vale);?><br>
                    <?php echo $recibo->nota;?><br>
                    <?php foreach($recibo->presupuestos as $presupuesto):?>
                    <span title="<?php echo $presupuesto->detalle;?>">
                              <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                        '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)<br>
                    <?php endforeach;?>
                </div>
                <div class="two columns" style="text-align:right;">
                </div>
            </div>
        </div>

        <?php if(count($recibo->items) >0):?>
        <div class="panel">
            <h5>Art&iacute;culos agregados</h5>
            <table>
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>N° Bol.-Factura</th>
                        <th>Item</th>
                        <th>Detalle</th>
                        <th>Valor</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($recibo->items as $item):?>

                    <?php foreach($item->articulos as $key=>$articulo):?>
                    <tr>
                        <?php if($key==0):?>
                        <td><?php echo $item->fecha;?></td>
                        <td>
                            <a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$recibo->id,
                                'i'=>$item->id,'f'=>$item->boleta,'folder'=>'recibos'));?>"><?php echo $item->numero_boleta;?></a>
                        </td>
                        <?php else:?>
                        <td></td>
                        <td></td>
                        <?php endif;?>

                        <td><?php echo $articulo->item_presupuestario;?></td>
                        <td><?php echo $articulo->itemPresupuesto->nombre;?></td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal(
                            $articulo->cantidad*$articulo->costo_unitario);?></td>
                        <td></td>
                    </tr>
                        <?php endforeach;?>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Total boleta</td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($item->valor);?></td>
                        <td><a href="<?php echo url('/finanzas/operaciones/eliminarItemRecibo',array('id'=>$item->id));?>">
                            Eliminar boleta</a></td>
                    </tr>

                    <?php endforeach;?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Total de gastos:</td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->total_gastos);?></td>
                        <td></td>
                    </tr>
                <?php if($recibo->tipo_recibo!=1):?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Saldo a favor del deudor:</td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->saldo_favor_deudor);?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Saldo a reintegrar en caja:</td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->saldo_reintegrar_caja);?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Total igual:</td>
                        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->total_igual);?></td>
                        <td></td>
                    </tr>
                <?php endif;?>
                </tbody>
            </table>
            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',array('type' => 'nice')); ?>
            <?php echo CHtml::submitButton("Rendir {$recibo->tipoRecibo}", array('class' => 'nice radius small button',
            'encode' => false,'name'=>'enviar_recibo')); ?>

            <?php if($recibo->tipo_recibo==2 && $recibo->estado==3):?>
                <a href="<?php echo url('/finanzas/operaciones/reintegrarTotalidadVale',array('id'=>$recibo->id));?>" class="nice radius small button">Reintegrar totalidad del vale</a>
            <?php endif;?>

            <?php $this->endWidget(); ?>
        </div>
        <?php endif;?>

        <div class="panel">
            <h6>Agregar art&iacute;culo o servicio</h6>

            <p>
            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
            array('type' => 'nice','id' => 'somemodel-form',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

            <?php echo $form->labelEx($model,'fecha'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model'=>$model,
                'attribute'=>'fecha',
                'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
                'language'=>'es',
                'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
                ?>
            <div class="form-field error">
                <?php echo $form->error($model,'fecha'); ?>
            </div>
            <?php echo $form->textFieldRow($model,'numero_boleta'); ?>
            <?php echo $form->textFieldRow($model,'valor'); ?>


        <h6>Seleccione el o los &iacute;tem que correspondan con el documento contable que est&aacute; registrando:</h6>
            <div class="grid-view">
            <table class="items">
                <thead>
                <tr>
                    <th>Art&iacute;culo</th>
                    <th>Item presupuestario</th>
                    <th>Cantidad</th>
                    <th>Costo unitario</th>
                    <th>Costo total</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($itemsBoleta as $i => $item): ?>
                        <tr>
                        <td><input type="checkbox" name="<?php echo "ItemBoletaRecibo[{$i}][id_item_presupuesto]";?>"
                           id="<?php echo "ItemBoletaRecibo_{$i}_id_item_presupuesto";?>"
                           value="<?php echo $itemsPresupuestos[$i]->id;?>" class="item_presupuestario">
                            <?php echo $itemsPresupuestos[$i]->nombre;?>
                        </td>
                        <td><?php echo CHtml::activeDropDownList($item,"[$i]item_presupuestario",
                        CHtml::listData($item->items,'id','mostrar'),array('encode'=>false,'class'=>'item_pres',
                                'disabled'=>'disabled')); ?></td>
                        <td><?php echo CHtml::activeTextField($item,"[$i]cantidad",array('style'=>'width:35px;',
                            'class'=>'cantidad','disabled'=>'disabled')); ?></td>
                        <td><?php echo CHtml::activeTextField($item,"[$i]costo_unitario",array('style'=>'width:50px;',
                            'class'=>'costo_unitario','disabled'=>'disabled')); ?></td>
                        <td class="costo_total" style="text-align: right;">0</td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td></td>
                        <td style="text-align: right;">Total</td>
                        <td style="text-align: right;" id="total_total">123</td>
                        <td style="text-align: right;">Total esperado</td>
                        <td style="text-align: right;" id="total_esperado">0</td>
                    </tr>
                </tbody>
            </table>
        </div>

            Documento contable que acredita la compra:
            <?php
            $this->widget('xupload.XUpload', array(
                    'url' => Yii::app()->createUrl( "/finanzas/operaciones/upload",array('folder'=>'recibos')),
                    //our XUploadForm
                    'model' => $boleta,
                    //We set this for the widget to be able to target our own form
                    'htmlOptions' => array('id'=>'somemodel-form'),
                    'attribute' => 'file',
                    'autoUpload'=>true
                )
            );
            ?>

            <?php echo CHtml::submitButton('Agregar Item', array('class' => 'nice radius medium button',
                'encode' => false,'name'=>'submitDatas')); ?>
            <?php $this->endWidget(); ?>
            </p>

        </div>

    </li>
</ul>
<?php
$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
$cssFile=$baseScriptUrl.'/styles.css';
Yii::app()->getClientScript()->registerCssFile($cssFile);
?>
<script>
function recalculateCost(){
    var suma=0;
    $('.costo_total').each(function(){
        $total_cost=$(this).prev().children("input[type=text]");
        $quantity=$(this).prev().prev().children("input[type=text]");
        valor_total=($total_cost.val()==undefined) ? 0 : $total_cost.val();
        valor_quant=($quantity.val()==undefined) ? 0 : $quantity.val();
        $(this).html(valor_total*valor_quant);
        if($(this).prev().prev().prev().prev().children(".item_presupuestario").is(':checked'))
            suma+=valor_total*valor_quant;
    });
    $('#total_total').html(suma);
}
function calculateTotalEsperado(){
    $('#total_esperado').html($('#ItemRecibo_valor').val());
}
$(function(){
    recalculateCost();
    calculateTotalEsperado();
    $('.cantidad').change(function(){
       recalculateCost();
    });
    $('.costo_unitario').change(function(){
        recalculateCost();
    });

    $('#ItemRecibo_valor').change(function(){
        calculateTotalEsperado();
    });

    $('.item_presupuestario').click(function(){
        $(this).parent().siblings().each(function(){
            var elem=$(this).children();
            $(elem).prop("disabled",!$(elem).prop("disabled"))
        });
        recalculateCost();
        //$(this).siblings().children('input').attr('disabled', !$(this).attr('checked'));
    });
});
</script>