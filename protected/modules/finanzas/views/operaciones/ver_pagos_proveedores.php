<?php
$this->pageTitle = "Ver {$tipo_vale}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Ver {$tipo_vale}"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active"><?php echo "Ver {$tipo_vale}";?></a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5><?php echo "Ver {$tipo_vale}";?> aceptados</h5>
            <p>A continuaci&oacute;n se listan los <?php echo $tipo_vale;?> aceptados:</p>
            <div class="grid-view">
            <table class="items">
                <thead>
                    <tr>
                        <th>Fecha solicitud</th>
                        <th>Monto Factura</th>
                        <th>N&uacute;mero Factura</th>
                        <th>Ver Factura</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($pagos_aprobados as $pago):?>
                    <tr>
                        <td style="text-align: center;"><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",
                            $pago->fecha_solicitud);?></td>
                        <td style="text-align: right;">$<?php echo app()->NumberFormatter->formatDecimal($pago->monto_factura);?></td>
                        <td style="text-align: right;"><?php echo $pago->numero_factura;?></td>
                        <td style="text-align: center;"><a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$pago->id,
                            'i'=>null,'f'=>$pago->factura,'folder'=>'proveedores'));?>">Ver factura</a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>

        <div class="panel clearfix">
            <h5><?php echo "Ver {$tipo_vale}";?> pendientes de evaluaci&oacute;n</h5>
            <p>A continuaci&oacute;n se listan los <?php echo $tipo_vale;?> pendientes de evaluaci&oacute;n:</p>
            </p>
            <div class="grid-view">
                <table class="items">
                    <thead>
                    <tr>
                        <th>Fecha solicitud</th>
                        <th>Monto Factura</th>
                        <th>N&uacute;mero Factura</th>
                        <th>Ver Factura</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($pagos_pendientes as $pago):?>
                    <tr>
                        <td style="text-align: center;"><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",
                            $pago->fecha_solicitud);?></td>
                        <td style="text-align: right;">$<?php echo app()->NumberFormatter->formatDecimal($pago->monto_factura);?></td>
                        <td style="text-align: right;"><?php echo $pago->numero_factura;?></td>
                        <td style="text-align: center;"><a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$pago->id,
                            'i'=>null,'f'=>$pago->factura,'folder'=>'proveedores'));?>">Ver factura</a></td>
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>

    </li>
</ul>
<?php
$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
$cssFile=$baseScriptUrl.'/styles.css';
Yii::app()->getClientScript()->registerCssFile($cssFile);
?>
