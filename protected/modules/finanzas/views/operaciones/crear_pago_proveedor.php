<?php
$this->pageTitle = "Crear {$model->tipoRecibo}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Crear {$model->tipoRecibo}"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Crear <?php echo $model->tipoRecibo;?></a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>
                <?php if($model->tipo_recibo==2):?>
                    Subir factura para unidad de abastecimiento
                <?php else:?>
                    Crear <?php echo $model->tipoRecibo;?>
                <?php endif;?>
            </h5>

            <?php if($model->tipo_recibo==1):?>
            <p>Para editar un formulario de pago a proveedores debes completar los siguientes datos y subir la
                factura correspondiente. Luego, el administrador de PIEA validar&aacute; el formulario. El sistema
                te notificar&aacute; de cualquier acci&oacute;n tomada en tu formulario.
                <b>La factura debe estar a nombre de Universidad T&eacute;cnica Federico Santa Mar&iacute;a</b>
            </p>
            <?php endif;?>

            <?php if($model->tipo_recibo==2):?>
            <p>Mediante el siguiente formulario se debe subir la factura para el formulario de unidad de abastecimiento.</p>
            <?php endif;?>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',array('type'=>'nice',
                'id' => 'somemodel-form',)); ?>

            <?php echo $form->errorSummary($model);?>

            <?php if($model->tipo_recibo==1):?>
            <label>Presupuestos asociados:</label>
            <?php foreach($presupuestos as $presupuesto):?>
            <input type="checkbox" name="PagoProveedores[presupuestosIds][]" value="<?php echo $presupuesto->id;?>"
                <?php echo (in_array($presupuesto->id,$model->presupuestosIds))?'selected="selected"':'';?>>
            <span title="<?php echo $presupuesto->detalle;?>">
                <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)<br>
            <?php endforeach;?>

            <?php echo $form->error($model, 'presupuestos'); ?>
            <?php endif;?>

            <?php echo $form->textFieldRow($model,'numero_factura'); ?>
            <?php echo $form->textFieldRow($model,'monto_factura'); ?>

            <br>
            Factura:
            <?php
            $this->widget('xupload.XUpload', array(
                    'url' => Yii::app()->createUrl( "/finanzas/operaciones/upload",array('folder'=>'proveedores')),
                    //our XUploadForm
                    'model' => $factura,
                    //We set this for the widget to be able to target our own form
                    'htmlOptions' => array('id'=>'somemodel-form'),
                    'attribute' => 'file',
                    'autoUpload'=>true,
                    'options'=>array('acceptFileTypes' => "js:/(\.|\/)(pdf)$/i",
                        'maxFileSize'=>'10000000')
                )
            );
            ?>

            <?php echo CHtml::submitButton('Continuar',array('class'=>'nice radius medium button','encode'=>false)); ?>
            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>
<script>
    $(function(){
        $('#show-pre-utilizados').click(function(e){
            e.preventDefault();
            $('#pre-utilizados').toggleClass('hide');
        });
    });
</script>