<?php
$this->pageTitle = 'Recuperaci&oacute;n de gastos';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Recuperaci&oacute;n de gastos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Recuperaci&oacute;n de gastos</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app ng-controller="RecuperacionController">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Crear recuperaci&oacute;n de gastos</h5>

            <div class="row">
                <div class="four columns">
                    UNIDAD PRESUPUESTARIA: <br>
                    CODIGO PRESUPUESTARIO: <br>
                    NOMBRE DEL DEUDOR:     <br>
                    MONTO DEL VALE:        <br>
                    NOTA:
                    <?php if($recibo->estado!=-2):?>
                    <br><br><b>ESTADO:</b>
                    <?php endif;?>
                </div>
                <div class="five columns">
                    <?php echo $recibo->unidad_presupuestaria;?><br>
                    <?php echo $recibo->codigoPresupuestario->identificador;?><br>
                    <?php echo $recibo->deudor->nombrePresentacion;?><br>
                    $<?php echo app()->NumberFormatter->formatDecimal($recibo->totalMontoItems);?><br>
                    <?php echo $recibo->nota;?>
                    <?php if($recibo->estado!=-2):?>
                    <br><br><?php echo $recibo->estadoRecibo;?>
                    <?php endif;?>
                </div>
                <div class="three columns" style="text-align:right;">
                    <a href="<?php echo url('/finanzas/operaciones/editarRecibo',array('id'=>$recibo->id));?>"
                        class="nice small radius button">Editar</a>
                    <br>
                    <a style="margin-top:5px;" href="<?php echo url($entidad->urlEntidad(),array('id'=>$entidad->id));?>"
                       class="nice small radius button">Guardar para m&aacute;s adelante</a>
                </div>
            </div>
        </div>

        <?php if(count($recibo->items) >0):?>
        <div class="panel">
            <h5>Art&iacute;culos agregados</h5>
            <table>
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Item</th>
                        <th>N° Bol.-Factura</th>
                        <th>Detalles</th>
                        <th>Valor</th>
                        <th>Boleta</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($recibo->items as $item):?>
                    <tr>
                        <td><?php echo $item->fecha;?></td>
                        <td><?php echo $item->item;?></td>
                        <td><?php echo $item->numero_boleta;?></td>
                        <td><?php echo $item->detalle;?></td>
                        <td><?php echo app()->NumberFormatter->formatDecimal($item->valor);?></td>
                        <td id="valor_item"><?php echo $item->valor;?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',array('type' => 'nice')); ?>
            <?php echo CHtml::submitButton('Solicitar recuperaci&oacute;n', array('class' => 'nice radius small button',
            'encode' => false,'name'=>'enviar_recuperacion')); ?>
            <?php $this->endWidget(); ?>
        </div>
        <?php endif;?>

        <div class="panel">
            <h6>Agregar art&iacute;culo o servicio</h6>
            <p>
            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
            array('type' => 'nice','id' => 'somemodel-form',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

            <?php echo $form->labelEx($model,'fecha'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model'=>$model,
                'attribute'=>'fecha',
                'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
                'language'=>'es',
                'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
                ?>
            <div class="form-field error">
                <?php echo $form->error($model,'fecha'); ?>
            </div>
            <?php echo $form->dropDownListRow($model,'item',
                CHtml::listData($model->items,'id','mostrar'),array('encode'=>false)); ?>
            <?php echo $form->dropDownListRow($model,'id_item_presupuesto',
            CHtml::listData($recibo->itemsPresupuestos,'id','detalle')); ?>
            <?php echo $form->textFieldRow($model,'numero_boleta'); ?>
            <?php echo $form->textFieldRow($model,'detalle'); ?>
            <?php echo $form->textFieldRow($model,'valor'); ?>

            Documento contable que acredita la compra:
                <?php
            $this->widget('xupload.XUpload', array(
                    'url' => Yii::app()->createUrl( "/finanzas/operaciones/upload",array('folder'=>'recibos')),
                    //our XUploadForm
                    'model' => $boleta,
                    //We set this for the widget to be able to target our own form
                    'htmlOptions' => array('id'=>'somemodel-form'),
                    'attribute' => 'file',
                    'autoUpload'=>true
                )
            );
            ?>

            <?php echo CHtml::submitButton('Agregar Item', array('class' => 'nice radius medium button', 'encode' => false)); ?>
            <?php $this->endWidget(); ?>
            </p>

        </div>

    </li>
</ul>