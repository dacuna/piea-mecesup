<div class="form">
  <div class="row">
    <div class="twelve columns">
      <form class="nice" method="post" action="<?php echo Yii::app()->createUrl('/finanzas/operaciones/rechazarPagoProveedor',
        array('id'=>$pago->id));?>">
      <div class="panel">
        <h5>Rechazar <?php echo $pago->tipoRecibo;?></h5>
        <p>Puedes incluir un comentario indicando los motivos de porque has decidido rechazar el formulario de
            <?php echo $pago->tipoRecibo;?>
        actual.</p>
        <textarea name="comentario_rechazo" rows="5">Comentario...</textarea>        
        <p>
          <br>
          <?php echo CHtml::submitButton('Rechazar '.$pago->tipoRecibo,array('class'=>'nice red radius small button',
        'encode'=>false)); ?>
        </p>        
      </div>   
      </form>
    </div>
  </div>
</div>