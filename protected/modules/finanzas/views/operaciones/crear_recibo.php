<?php
$this->pageTitle = "Crear {$model->tipoRecibo}";
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    "Crear {$model->tipoRecibo}"
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Crear <?php echo $model->tipoRecibo;?></a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Crear <?php echo $model->tipoRecibo;?></h5>

            <?php if($model->tipo_recibo==1):?>
                <p>Para crear una recuperaci&oacute;n de gastos, se deben completar los siguientes datos. Luego, en la
                siguiente pantalla se agregar&aacute;n los art&iacute;los de la recuperaci&oacute;n. Para continuar,
                completa los siguientes campos y luego presiona en "Continuar".</p>
            <?php endif;?>

            <?php if($model->tipo_recibo==2):?>
                <p>Para crear un vale a rendir, se deben completar los siguientes datos. Luego, podr&aacute;s obtener un
                formulario de retiro de fondos el cual deber&aacute;s imprimir y entregar en las oficinas de PIEA. Una vez
                recibido dicho formulario, podr&aacute;s solicitar el vale a rendir mediante un formulario de rendici&oacute;n.
                Ser&aacute;s
                notificado en cada acci&oacute;n que se realice respecto a tu vale.</p>
            <?php endif;?>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',array('type'=>'nice')); ?>

            <?php echo $form->errorSummary($model);?>

            <label>Presupuestos asociados:</label>
            <?php foreach($presupuestos as $presupuesto):?>
            <input type="checkbox" name="Recibo[presupuestosIds][]" value="<?php echo $presupuesto->id;?>"
                <?php echo (in_array($presupuesto->id,$model->presupuestosIds))?'selected="selected"':'';?>>
            <span title="<?php echo $presupuesto->detalle;?>">
                <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)<br>
            <?php endforeach;?>

            <?php echo $form->error($model, 'presupuestos'); ?>

            <br>
            <?php echo $form->dropDownListRow($model,'id_deudor',
                CHtml::listData($entidad->integrantes,'user_id','usuario.nombrePresentacion'),
                array('options'=>array(user()->id=>array('selected'=>'selected')))); ?>
            <?php echo $form->textFieldRow($model,'nota',array('style'=>'width:100%;')); ?>
            <?php echo CHtml::submitButton('Continuar',array('class'=>'nice radius medium button','encode'=>false)); ?>
            <?php $this->endWidget(); ?>
        </div>

    </li>
</ul>