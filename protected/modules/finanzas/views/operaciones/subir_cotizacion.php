<?php
$this->pageTitle = 'Subir cotizaci&oacute;n gastos de env&iacute;o de presupuesto';
$this->breadcrumbs=array(
    'Subir cotizaci&oacute;n gastos de env&iacute;o de presupuesto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Subir cotizaci&oacute;n gastos de env&iacute;o de presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">

            <?php if($error!=null):?>
                <p style="color: red;">
                    <?php echo $error;?>
                </p>
            <?php endif;?>

            <h5>Subir cotizaci&oacute;n gastos de env&iacute;o de presupuesto</h5>

            <?php if(!$showFinal):?>
            <p>Para continuar debe ingresar la cotizaci&oacute;n de los gastos de env&iacute;o para el
            presupuesto solicitado.</p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',array('type'=>'nice')); ?>

            Cotizaci&oacute;n gastos de env&iacute;o: <input type="text" name="gastos_envio">
            <br>
            <?php echo CHtml::submitButton('Enviar',array('class'=>'nice radius medium button','encode'=>false)); ?>

            <?php $this->endWidget(); ?>

            <?php else:?>
                <p>La cotizaci&oacute;n ingresada se ha guardado correctamente.</p>
            <?php endif;?>
        </div>

    </li>
</ul>