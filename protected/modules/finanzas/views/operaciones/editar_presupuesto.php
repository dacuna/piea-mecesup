<?php
$this->pageTitle = 'Editar presupuesto';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Editar presupuesto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Editar presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Editar presupuesto</h5>

            <p><b>Fecha presupuesto</b>: <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",
                $presupuesto->fecha_solicitud);?><br>
                <b>Estado</b>: <?php echo $presupuesto->estadoPresupuesto;?><br>
                <?php if($presupuesto->estado==-1 && $presupuesto->comentario_revisor!=null):?>
                    <b>Motivo rechazo: </b><?php echo $presupuesto->comentario_revisor;?><br>
                    <?php endif;?>
                <b>Usuario solicitante</b>: <?php echo $presupuesto->solicitante->nombrePresentacion;?><br>
                <b>Total</b>: $<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?>
            </p>

            <p>Puedes modificar un presupuesto mediante el siguiente formulario. Una vez que presiones en el boto&oacute;n
                "Editar presupuesto", &eacute;ste ser&aacute; enviado a revisi&oacute;n.</p>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
                array('type' => 'nice','id' => 'somemodel-form',
                'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

            <h6>Agregar item:</h6><br>

            <?php echo $form->textFieldRow($item,'nombre'); ?>
            <?php echo $form->textFieldRow($item,'precio'); ?>
            <?php echo $form->textFieldRow($item,'unidades'); ?>

            <h6>Presupuestos:</h6>
            <br>
            <?php
            $this->widget( 'xupload.XUpload', array(
                    'url' => Yii::app( )->createUrl( "/finanzas/operaciones/upload"),
                    //our XUploadForm
                    'model' => $boletas,
                    //We set this for the widget to be able to target our own form
                    'htmlOptions' => array('id'=>'somemodel-form'),
                    'attribute' => 'file',
                    'multiple' => true,
                )
            );
            ?>

            <?php echo CHtml::submitButton('Agregar Item', array('class' => 'nice radius small button', 'encode' => false)); ?>
            <?php $this->endWidget(); ?>
            <br><br>

            <h5>Items agregados</h5>

            <?php if(count($presupuesto->items)>0):?>
                <?php $this->widget('ItemsPresupuestoWidget',array('items'=>$presupuesto->items,
                    'presupuesto'=>$presupuesto)); ?>
            <?php else:?>
                <p>No se han agregado items al presupuesto actual.</p>
            <?php endif;?>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',array('type' => 'nice')); ?>
            <?php echo CHtml::submitButton('Enviar Presupuesto', array('name'=>'editarPresupuesto',
                'class' => 'nice radius medium button', 'encode' => false)); ?>
            <?php $this->endWidget(); ?>

            </div>

        </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">

    function ItemPresupuestoController($scope, $http) {

        $scope.addedItems=[];
        $scope.countItems=1;

        $scope.emptyItem=function(){
            $scope.itemName='';
            $scope.itemPrice='';
            $scope.itemUnits='';
            $scope.itemFile='';
        }

        $scope.addItem=function(){
            var itemExists=_.find($scope.addedItems,function(itemList){return itemList.id==$scope.itemID;});
            if(itemExists==undefined){
                $scope.addedItems.push({id:$scope.countItems,name:$scope.itemName,price:$scope.itemPrice,
                    units:$scope.itemUnits});
                $scope.countItems+=1;
            } else{
                itemExists.name=$scope.itemName;
                itemExists.price=$scope.itemPrice;
                itemExists.units=$scope.itemUnits;
            }
            $scope.emptyItem();
        }

        $scope.deleteItem=function(item){
            var itemAction=_.find($scope.addedItems,function(itemList){return itemList.name==item.name;});
            var items=$scope.addedItems;
            $scope.addedItems=[];
            _.each(items,function(item){if(item.name!=itemAction.name) $scope.addedItems.push(item)});
        }

        $scope.editItem=function(item){
            $scope.itemName=item.name;
            $scope.itemPrice=item.price;
            $scope.itemUnits=item.units;
            $scope.itemID=item.id;
        }

        $scope.addFileToItem=function(){
            console.log($scope.itemFile);
            $scope.filesPerItem.push($scope.itemFile);
            $scope.itemFile='';
        }

    }

</script>
