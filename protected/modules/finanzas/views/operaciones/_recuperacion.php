<style>
    .row:before, .row:after, .clearfix:before, .clearfix:after { content:""; display:table; }
    .row:after, .clearfix:after { clear: both; }
    .row, .clearfix { zoom: 1; }
    .p{margin-top:5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;}
    .top{
        border:1px #000000 solid;width:100%;height: 180px;
    }
    <?php if($recuperacion->estado!=1):?>
    body {
        background-image:url('http://pieaserver.dti.utfsm.cl/images/pendiente-aprobacion.png');
        background-repeat: repeat;
    }
        <?php endif;?>
</style>
    <div class="top">
        <u>DIRECCION GENERAL DE FINANZAS</u>
        <p style="margin: 50px;margin-left:280px;">
            RECIBO N° <?php echo $recuperacion->id;?>/2012<br><br>
            <?php if($recuperacion->tipo_recibo==1):?>
            <b>X</b> &nbsp;&nbsp; RECUPERACION DE GASTOS<br>
            <?php endif;?>
            <?php if($recuperacion->tipo_recibo==2):?>
            <b>X</b> &nbsp;&nbsp;VALE A RENDIR CUENTA<br>
            <?php endif;?>
            <?php if($recuperacion->tipo_recibo==3):?>
            <b>X</b> &nbsp;&nbsp;RENDICION DE CAJA CHICA<br>
            <?php endif;?>
        </p>
    </div>
    <div style="width: 100%;border-bottom:1px #000000 solid;border-left:1px #000000 solid;border-right:1px #000000 solid;text-align: right;">
        <p class="p">Valpara&iacute;so, <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",$recuperacion->fecha_vale);?></p>
    </div>
<div style="width: 100%;border-left:1px #000000 solid;border-right:1px #000000 solid;text-align: right;">
    <p>&nbsp;</p>
</div>
<!-- INFORMACION DEL RECIBO -->
<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>UNIDAD PRESUPUESTARIA</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo $recuperacion->unidad_presupuestaria;?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>CODIGO PRESUPUESTARIO</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo $recuperacion->codigoPresupuestario->numero;?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>NOMBRE DEL DEUDOR</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo $recuperacion->deudor->nombrePresentacion;?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>MONTO DEL VALE</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;">$<?php echo app()->NumberFormatter->formatDecimal($recuperacion->monto_vale);?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>FECHA DEL VALE</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",$recuperacion->fecha_vale);?></td>
    </tr>

    <tr>
        <td width="200" height="18" style=""><b>NOTA</b></td>
        <td width="530" height="18" style="text-align:left;"><?php echo $recuperacion->nota;?></td>
    </tr>
</table>

<!-- FIN INFORMACION DEL RECIBO -->
<!-- INICIO ITEMS -->
    <table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
        <tr>
            <td width="84" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"><b>FECHA</b></td>
            <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>N° Bol.-Factura</b></td>
            <td width="50" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>ITEM</b></td>
            <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>DETALLES</b></td>
            <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:center;"><b>VALOR$</b></td>
        </tr>
<?php foreach($recuperacion->items as $key=>$item):?>




        <?php foreach($item->articulos as $key=>$articulo):?>
            <tr>
                <?php if($key==0):?>
                <td width="84" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"><?php echo $item->fecha;?></td>
                <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><?php echo $item->numero_boleta;?></td>
                <?php else:?>
                <td width="84" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"></td>
                <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"></td>
                <?php endif;?>

                <td width="50" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><?php echo $articulo->item_presupuestario;?></td>
                <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:left;"><?php echo $articulo->itemPresupuesto->nombre;?></td>
                <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
                    <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal(
                    $articulo->cantidad*$articulo->costo_unitario);?></p></td>
            </tr>
            <?php endforeach;?>

        <tr>
            <td width="84" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"></td>
            <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"></td>
            <td width="50" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"></td>
            <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
                Total boleta
            </td>
            <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
                <p style="margin: 0px;margin-right: 5px;"> $<?php echo app()->NumberFormatter->formatDecimal($item->valor);?></p>
            </td>
        </tr>
        <?php endforeach;?>
    </table>
<!-- FIN ITEMS -->

<!-- PARTE FINAL -->
<table style="border:1px #000000 solid;border-top: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="85" height="30" style=""></td>
        <td width="51" height="30" style=""></td>
        <td width="104" height="30" style=""></td>
        <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;"><b>TOTAL DE GASTOS</b></td>
        <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
            <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($recuperacion->total_gastos);?></p></td>
    </tr>
    <tr>
        <td width="85" height="30" style=""></td>
        <td width="51" height="30" style=""></td>
        <td width="104" height="30" style=""></td>
        <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
            <b>SALDO A FAVOR DEL DEUDOR</b></td>
        <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
            <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($recuperacion->saldo_favor_deudor);?></p></td>
    </tr>
    <tr>
        <td width="85" height="30" style=""></td>
        <td width="51" height="30" style=""></td>
        <td width="104" height="30" style=""></td>
        <td width="376" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
            <b>SALDO A REINTEGRAR CAJA</b></td>
        <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
            <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($recuperacion->saldo_reintegrar_caja);?></p></td>
    </tr>
    <tr>
        <td width="85" height="30" style=""></td>
        <td width="51" height="30" style=""></td>
        <td width="104" height="30" style=""></td>
        <td width="376" height="30" style="border-right:1px #000000 solid;">
            <b>TOTAL IGUAL</b></td>
        <td width="100" height="30" style="text-align:right;">
            <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($recuperacion->total_igual);?></p></td>
    </tr>
</table>
<!-- FIN PARTE FINAL-->

<!-- FIRMAS -->
<table style="border:1px #000000 solid;border-top: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="365" height="130" style="text-align: center;">
            <p style="margin-top: 70px;"><b><?php echo $recuperacion->deudor->nombrePresentacion;?><br>
            FIRMA DEL DEUDOR</b></p>
        </td>
        <td width="365" height="130" style="text-align: center;">
            <p style="margin-top: 70px;">
                <b><?php echo $recuperacion->codigoPresupuestario->propietario;?></b><br>
                <b><?php echo $recuperacion->codigoPresupuestario->cargo_propietario;?></b>
            </p>
        </td>
    </tr>
</table>
<!-- FIN FIRMAS -->

