<style xmlns="http://www.w3.org/1999/html">
    .row:before, .row:after, .clearfix:before, .clearfix:after { content:""; display:table; }
    .row:after, .clearfix:after { clear: both; }
    .row, .clearfix { zoom: 1; }
    .p{margin-top:5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;}
    .top{
        border:1px #000000 solid;width:100%;height: 180px;
    }
</style>
    <div style="text-align: center;margin-top:20px;">
        <img src="http://www.exalumnos.utfsm.cl/assets/Logos/MARCA-Color.jpg" width="194" height="104">
    </div>


<table style="border:0px;margin-top: 50px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="30" style="border:0px;">
            </td>

        <td width="630" height="30" style="border:0px;">
            <p style="font-size: 16px;text-align: justify;line-height: 1.5;">
                POR EL PRESENTE INSTRUMENTO, YO <b><?php echo strtoupper($usuario->nombreCompleto);?></b>, RUT
                <?php echo $usuario->perfil->rut;?>
                ME CONSTITUYO EN DEUDOR DE LA UNIVERSIDAD T&Eacute;CNICA FEDERICO SANTA MAR&Iacute;A, POR LA SUMA DE
                <b>$<?php echo app()->NumberFormatter->formatDecimal($recibo->totalMontoItemsPresupuestos);?>.-</b>
                (<?php echo num2letras($recibo->totalMontoItemsPresupuestos);?>),
                EN LA FORMA Y BAJO
                LAS CONDICIONES SE&Ntilde;ALADAS EN EL
                PUNTO N°7 DE LA CIRCULAR N°1/85 DE LA DIRECCI&Oacute;N
                DE ASUNTOS ECON&Oacute;MICOS DE FECHA 7 DE OCTUBRE DE 1985 QUE DECLARO CONOCER.
            </p>
        </td>

        <td width="50" height="30" style="border:0px;"></td>
    </tr>
</table>

<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="80" style="border:0px;">
        </td>

        <td width="380" height="80" style="border:0px;">
        </td>

        <td width="250" height="80" style="border-bottom:1px #000000 solid;">
        </td>
        <td width="50" height="80" style="border:0px;"></td>
    </tr>
</table>
<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="5" style="border:0px;">
        </td>

        <td width="380" height="5" style="border:0px;">
        </td>

        <td width="250" height="5" style="border:0px;text-align:center;">
            <b>FIRMA</b>
        </td>
        <td width="50" height="5" style="border:0px;"></td>
    </tr>
</table>

<p style="margin-top:20px;margin-left:50px;font-size: 16px;">FECHA: <?php echo strtoupper(app()->dateFormatter->format("dd' DE 'MMMM' DE 'y",$recibo->fecha_emision_documento));?></p>

<table style="border:0px;margin-top: 30px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="30" style="border:0px;">
        </td>

        <td width="630" height="30" style="border:0px;">
            <p style="font-size: 16px;text-align: justify;line-height: 1.5;margin-bottom: 0px;">
                EL <?php echo strtoupper($recibo->codigoPresupuestario->cargo_propietario);?>, QUIEN SE SUSCRIBE, AUTORIZA A
            <b><?php echo strtoupper($usuario->nombreCompleto);?></b>
                PARA RETIRAR LA SUMA DE <b>$<?php echo app()->NumberFormatter->formatDecimal($recibo->totalMontoItemsPresupuestos);?>.-</b>
               (<?php echo num2letras($recibo->totalMontoItemsPresupuestos);?>), CONSTITUY&Eacute;NDOSE EN CO-DEUDOR SOLIDARIO,
                EN LAS FORMAS Y BAJO LAS CONDICIONES PRESCRITAS EN EL PUNTO N°7 DE LA CIRCULAR N°1/85 ANTES SE&Ntilde;ALADA.
            </p>
        </td>

        <td width="50" height="30" style="border:0px;"></td>
    </tr>
</table>

<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="80" style="border:0px;">
        </td>

        <td width="380" height="80" style="border:0px;">
        </td>

        <td width="250" height="80" style="border-bottom:1px #000000 solid;">
        </td>
        <td width="50" height="80" style="border:0px;"></td>
    </tr>
</table>
<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="5" style="border:0px;">
        </td>

        <td width="380" height="5" style="border:0px;">
        </td>

        <td width="250" height="5" style="border:0px;text-align:center;">
            <b><?php echo strtoupper($recibo->codigoPresupuestario->propietario);?></b><br>
            <b><?php echo strtoupper($recibo->codigoPresupuestario->cargo_propietario);?></b>
        </td>
        <td width="50" height="5" style="border:0px;"></td>
    </tr>
</table>


<p style="margin-top:40px;margin-left:50px;font-size: 16px;">VALPARAISO, <?php echo strtoupper(app()->dateFormatter->format("dd' DE 'MMMM' DE 'y",$recibo->fecha_emision_documento));?></p>
