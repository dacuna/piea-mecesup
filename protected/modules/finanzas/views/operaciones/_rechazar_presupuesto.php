<div class="form">
  <div class="row">
    <div class="twelve columns">
      <form class="nice" method="post" action="<?php echo Yii::app()->createUrl('/finanzas/operaciones/rechazarPresupuesto',
        array('id'=>$presupuesto->id));?>">
      <div class="panel">
        <h5>Rechazar presupuesto</h5>
        <p>Puedes incluir un comentario indicando los motivos de porque has decidido rechazar el presupuesto
        actual.</p>
        <textarea name="comentario_rechazo" rows="5">Comentario...</textarea>        
        <p>
          <br>
          <?php echo CHtml::submitButton('Rechazar presupuesto',array('class'=>'nice red radius small button')); ?>
        </p>        
      </div>   
      </form>
    </div>
  </div>
</div>