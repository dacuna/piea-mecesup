<style xmlns="http://www.w3.org/1999/html">
    .row:before, .row:after, .clearfix:before, .clearfix:after { content:""; display:table; }
    .row:after, .clearfix:after { clear: both; }
    .row, .clearfix { zoom: 1; }
    .p{margin-top:5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;}
    .top{
        border:1px #000000 solid;width:100%;height: 180px;
    }
</style>
<p style="text-align: center;font-size: 16px;">
    MEMO N°<?php echo $unidad->id;?>
</p>
<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="30" style="border:0px;"></td>
        <td width="50" height="30" style="border:0px;">A: </td>
        <td width="580" height="30" style="border:0px;">Secci&oacute;n compras</td>
    </tr>
    <tr>
        <td width="50" height="30" style="border:0px;"></td>
        <td width="50" height="30" style="border:0px;">DE: </td>
        <td width="580" height="30" style="border:0px;"><?php echo $unidad->solicitante->nombrePresentacion;?></td>
    </tr>
    <tr>
        <td width="50" height="30" style="border:0px;"></td>
        <td width="50" height="30" style="border:0px;">REF: </td>
        <td width="580" height="30" style="border:0px;">Solicitud de cotizaci&oacute;n de presupuesto para
        <?php echo $entidad->getTipoEntidad();?> <?php echo $entidad->getNombre();?> de Programa de Iniciativas
            Estudiantiles Acad&eacute;micas.
        </td>
    </tr>
    <tr>
        <td width="50" height="30" style="border:0px;"></td>
        <td width="50" height="30" style="border:0px;">FECHA: </td>
        <td width="580" height="30" style="border:0px;"><?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",date('Y-m-d H:i:s'));?></td>
    </tr>
</table>

<table>
    <tr>
        <td width="50" height="10" style="border:0px;"></td>
        <td width="630" height="10" style="border:0px;border-bottom: 1px #000 solid;"></td>
    </tr>
    <tr>
        <td width="50" height="30" style="border:0px;"></td>
        <td width="630" height="30" style="border:0px;padding-top:10px;">
            Se solicita realizar una cotizaci&oacute;n de gastos de env&iacute;o para los productos presentes en el
            presupuesto adjuntado junto a este documento. Puede ingresar dicha cotizaci&oacute;n en la direcci&oacute;n:
            <br>
            <?php echo wordwrap(app()->createAbsoluteUrl('/finanzas/operaciones/subirCotizacion',array('id'=>$presupuesto->id
                ,'t'=>md5($presupuesto->fecha_solicitud))),100,"<br>\n",true);?>
        </td>
    </tr>
</table>


<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="80" style="border:0px;">
        </td>

        <td width="380" height="80" style="border:0px;">
        </td>

        <td width="250" height="80">
        </td>
        <td width="50" height="80" style="border:0px;"></td>
    </tr>
</table>
<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="5" style="border:0px;">
        </td>

        <td width="380" height="5" style="border:0px;">
        </td>
    </tr>
</table>


<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="80" style="border:0px;">
        </td>

        <td width="630" height="80" style="border:0px;">
        </td>
    </tr>
</table>

<table style="border:0px;margin-top: 20px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50" height="200" style="border:0px;">
        </td>

        <td width="380" height="200" style="border:0px;">
        </td>

        <td width="250" height="200" style="border:0px;border-bottom:1px #000000 solid;">
        </td>
        <td width="50" height="200" style="border:0px;"></td>
    </tr>
    <tr>
        <td width="50" height="100" style="border:0px;">
        </td>

        <td width="380" height="100" style="border:0px;">
        </td>

        <td width="250" height="100" style="border:0px;text-align:center;padding-top:10px;">
            <b><?php echo $unidad->solicitante->nombrePresentacion;?></b>
        </td>
        <td width="50" height="100" style="border:0px;"></td>
    </tr>
</table>

<!-- PRESUPUESTO -->
<div class="top">
    <u>Programa de iniciativas estudiantiles acad&eacute;micas</u>
</div>
<div style="width: 100%;border-bottom:1px #000000 solid;border-left:1px #000000 solid;border-right:1px #000000 solid;text-align: right;">
    <p class="p">Valpara&iacute;so, <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",$presupuesto->fecha_solicitud);?></p>
</div>
<div style="width: 100%;border-left:1px #000000 solid;border-right:1px #000000 solid;text-align: right;">
    <p>&nbsp;</p>
</div>
<!-- INFORMACION DEL RECIBO -->
<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b><?php echo $entidad->getTipoEntidad();?></b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo $entidad->getNombre();?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>Nombre solicitante</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo $unidad->solicitante->nombrePresentacion;?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>Monto del presupuesto</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;">$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?></td>
    </tr>

    <tr>
        <td width="200" height="18" style="border-bottom:1px #000000 solid;"><b>Fecha solicitud</b></td>
        <td width="530" height="18" style="border-bottom:1px #000000 solid;text-align:left;"><?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y",$presupuesto->fecha_solicitud);?></td>
    </tr>

    <tr>
        <td width="200" height="18" style=""><b>Detalle</b></td>
        <td width="530" height="18" style="text-align:left;"><?php echo $presupuesto->detalle;?></td>
    </tr>
</table>

<!-- FIN INFORMACION DEL RECIBO -->
<!-- INICIO ITEMS -->
<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="74" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align: center;"><b>ID</b></td>
        <td width="371" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>Nombre</b></td>
        <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>Precio</b></td>
        <td width="65" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><b>Unidades</b></td>
        <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:center;"><b>Total</b></td>
    </tr>
    <?php foreach($presupuesto->items as $key=>$item):?>
        <tr>
            <td width="74" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align: right;"><p style="margin: 0px;margin-right: 5px;"><?php echo $key;?></p></td>
            <td width="371" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:center;"><?php echo $item->nombre;?></td>
            <td width="100" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:right;"><p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($item->precio);?></p></td>
            <td width="65" height="30" style="border-right:1px #000000 solid;border-bottom:1px #000000 solid;text-align:right;"><p style="margin: 0px;margin-right: 5px;"><?php echo app()->NumberFormatter->formatDecimal($item->unidades);?></p></td>
            <td width="100" height="30" style="border-bottom:1px #000000 solid;text-align:right;">
                <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($item->unidades*$item->precio);?></p></td>
        </tr>
    <?php endforeach;?>
</table>
<!-- FIN ITEMS -->

<!-- PARTE FINAL -->
<table style="border:1px #000000 solid;border-top: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="85" height="30" style=""></td>
        <td width="51" height="30" style=""></td>
        <td width="104" height="30" style=""></td>
        <td width="376" height="30" style="border-right:1px #000000 solid;text-align: right;"><p style="margin: 0px;margin-right: 5px;"><b>TOTAL</b></p></td>
        <td width="100" height="30" style="text-align:right;">
            <p style="margin: 0px;margin-right: 5px;">$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?></p></td>
    </tr>
</table>
<!-- FIN PARTE FINAL-->


