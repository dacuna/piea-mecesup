<?php
$this->pageTitle = 'Ver retiro de fondos';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Ver retiro de fondos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ver retiro de fondos</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ver retiro de fondos</h5>
            <p>A continuaci&oacute;n, se presenta el retiro de fondo COD <?php echo $retiro->id;?></p>

            <table>
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Usuario que solicit&oacute;</th>
                        <th>C&oacute;digo presupuestario</th>
                        <th>Presupuestos asociados</th>
                        <th>Descargar (pdf)</th>
                        <th>Acci&oacute;n</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo app()->dateFormatter->format("dd'/'MM'/'y",$retiro->fecha_vale);?></td>
                        <td><?php echo $retiro->deudor->nombrePresentacion;?></td>
                        <td><?php echo $retiro->codigoPresupuestario->identificador;?></td>
                        <td>
                            <?php foreach($retiro->presupuestos as $key=>$presupuesto):?>
                            <span title="<?php echo $presupuesto->detalle;?>">
                    <?php echo truncate($presupuesto->detalle,78);?></span> (<a href="<?php echo url(
                                '/finanzas/operaciones/verPresupuesto',array('id'=>$presupuesto->id));?>">Ver presupuesto</a>)
                            <?php if($key!=count($retiro->presupuestos)-1){ echo ',';};?>
                            <?php endforeach;?>
                        </td>
                        <td><a href="<?php echo url('/finanzas/operaciones/generarRetiroFondos',array(
                            'id'=>$retiro->id));?>">Descargar PDF</a></td>
                        <td>
                            <?php if(user()->checkAccess('administrador')):?>
                                <a href="<?php echo url('/finanzas/operaciones/notificarReciboRetiro',array('id'=>$retiro->id));?>">
                                    Notificar Recibo Correcto</a>
                            <?php else:?>
                                <?php if($retiro->estado==0):?>
                                    <a href="<?php echo url('/finanzas/operaciones/notificarEntregaRetiro',array('id'=>$retiro->id));?>">
                                    Notificar entrega</a>
                                <?php endif;?>
                                <?php if($retiro->estado==3):?>
                                    <a href="<?php echo url('/finanzas/operaciones/llenarRecibo',array('id'=>$retiro->id));?>">
                                        Llenar formulario de rendici&oacute;n de cuentas</a>
                                <?php endif;?>
                                <?php if($retiro->estado==2):?>
                                    ---
                                <?php endif;?>
                                 <?php if($retiro->estado==5):?>
                                <a href="<?php echo url('/finanzas/operaciones/subirComprobanteReintegracion',
                                    array('id'=>$retiro->id));?>">
                                    Subir documento de comprobante de reintegraci&oacute;n</a>
                                <?php endif;?>
                            <?php endif;?>
                        </td>
                        <td><?php echo $retiro->estadoRecibo;?></td>
                    </tr>
                </tbody>
            </table>

        </div>

    </li>
</ul>
