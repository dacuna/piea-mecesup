<?php
$this->pageTitle = 'Ver vales a rendir';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Ver vales a rendir'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ver vales a rendir</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ver vales a rendir</h5>
            <p>A continuaci&oacute;n, se listan todos los vales a rendir en esta entidad.
            </p>

            <table>
                <thead>
                    <tr>
                        <th>C&oacute;digo presupuestario</th>
                        <th>Deudor</th>
                        <th>Usuario que solicit&oacute;</th>
                        <th>Fecha</th>
                        <th>Monto Vale</th>
                        <th>Total gastos</th>
                        <th>Ver</th>
                        <th>PDF</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($vales as $vale):?>
                    <tr>
                        <td><?php echo $vale->codigoPresupuestario->identificador;?></td>
                        <td><?php echo $vale->deudor->nombrePresentacion;?></td>
                        <td><?php echo $vale->solicitante->nombrePresentacion;?></td>
                        <td><?php echo $vale->fecha_vale;?></td>
                        <td>$<?php echo app()->NumberFormatter->formatDecimal($vale->monto_vale);?></td>
                        <td>$<?php echo app()->NumberFormatter->formatDecimal($vale->total_gastos);?></td>
                        <td><a href="<?php echo url('/finanzas/operaciones/verRecibo',array(
                            'id'=>$vale->id
                        ));?>">Ver</a></td>
                        <td><a href="<?php echo url('/finanzas/operaciones/generarRecuperacion',array(
                            'id'=>$vale->id
                        ));?>">Descargar</a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

        </div>

    </li>
</ul>
