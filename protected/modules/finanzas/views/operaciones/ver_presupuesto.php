<?php
$this->pageTitle = 'Ver presupuesto';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Ver presupuesto'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Ver presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Ver presupuesto</h5>
            <p><b>Fecha presupuesto</b>: <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",
                $presupuesto->fecha_solicitud);?><br>
               <b>Estado</b>: <?php echo $presupuesto->estadoPresupuesto;?><br>
               <?php if($presupuesto->estado==-1 && $presupuesto->comentario_revisor!=null):?>
                <b>Motivo rechazo: </b><?php echo $presupuesto->comentario_revisor;?><br>
               <?php endif;?>
                <b>Usuario solicitante</b>: <?php echo $presupuesto->solicitante->nombrePresentacion;?><br>
                <?php if($presupuesto->unidad_de_abastecimiento!=null && $presupuesto->cotizacion_gastos_envio!=null):?>
                    <b>Total presupuesto: </b>$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems-$presupuesto->cotizacion_gastos_envio);?>
                    <?php else:?>
                    <b>Total presupuesto: </b>$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?>
                    <?php endif;?>
                <?php if($presupuesto->id_cuenta!=null):?>
                    <br><b>Cuenta</b>: <?php echo $presupuesto->cuenta->identificador;?>
                <?php endif;?>
                <?php if($presupuesto->estado==-1):?><br>
                <a href="<?php echo url('/finanzas/operaciones/editarPresupuesto',array('id'=>$presupuesto->id));?>">
                    <b>Editar presupuesto</b></a>
                <?php endif;?>
                <br><b>Detalle</b>: <?php echo $presupuesto->detalle;?>
                <?php if($presupuesto->unidad_de_abastecimiento!=null):?>
                <?php if($presupuesto->unidadDeAbastecimiento->gastos_envio==0):?>
                   <br> <span style="color:red;">Este presupuesto no considera gastos de env&iacute;o.</span>
                    <?php else:?>
                   <br> <span style="color:green;">Este presupuesto s&iacute; considera gastos de env&iacute;o.</span>
                    <?php endif;?>
                <?php endif;?>
                <?php if($presupuesto->unidad_de_abastecimiento!=null && $presupuesto->cotizacion_gastos_envio!=null):?>
                    <br><b>Cotizaci&oacute;n de gastos de env&iacute;o: </b>$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->cotizacion_gastos_envio);?>
                    <br><b>TOTAL FINAL PRESUPUESTO: </b>$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?>
                    <?php endif;?>
            </p>

            <h5>Items agregados</h5>

            <?php $this->widget('ItemsPresupuestoWidget',array('items'=>$presupuesto->items,
                'presupuesto'=>$presupuesto)); ?>

            <?php if(($mostrar_aprobacion && ($presupuesto->estado==0 || $presupuesto->estado==3)) ||
                ($mostrar_aprobacion_final && $presupuesto->estado==4)):?>
                <br>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',
                    array('type'=>'nice','action'=>array('/finanzas/operaciones/aprobarPresupuesto','id'=>$presupuesto->id))); ?>

                    <?php if($presupuesto->estado==0):?>
                    <p>Para aceptar el presupuesto, debes seleccionar la cuenta de origen de los fondos para &eacute;ste.
                    <select name="Presupuesto[id_cuenta]" id="Presupuesto_id_cuenta">
                            <?php foreach ($cuentas as $cuenta): ?>
                            <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id : $cuenta[0]->cuenta->id;?>"
                                    data-tipo="<?php echo $cuenta[1];?>">
                                <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                                <?php echo ' | Saldo: $'.app()->NumberFormatter->formatDecimal($cuenta[0]->saldo);?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>

                    <?php echo CHtml::submitButton('Validar Presupuesto',
                        array('class'=>'nice medium button','encode'=>false,'name'=>'Presupuesto[validar]')); ?>
                <a href="#" data-reveal-id="myModal" class="nice red button">Rechazar presupuesto</a>
                <?php $this->endWidget(); ?>

            </p>


                <br><br>
                <?php
                $this->modal['id']='myModal';
                $this->modal['close']=true;

                $this->modal['content']=$this->renderPartial('_rechazar_presupuesto',array('presupuesto'=>$presupuesto),true)
                ?>
            <?php endif;?>

            <a href="javascript:history.back()" class="button nice small">Volver</a><br><br>

        </div>

    </li>
</ul>
