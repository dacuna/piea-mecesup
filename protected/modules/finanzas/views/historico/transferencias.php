<?php
$this->pageTitle = 'Hist&oacute;rico de transferencias';
$this->breadcrumbs=array(
    'Hist&oacute;rico de transferencias'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Hist&oacute;rico de transferencias</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Hist&oacute;rico de transferencias</h5>
            <p>A continuaci&oacute;n se presentan las &uacute;ltimas transferencias de fondos realizadas:</p>

            <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider'=>$dataProvider,
                    'columns'=>array(
                    'id',
                    array('name'=>'Cuenta origen','value'=>'$data->cuenta_origen->nombre'),
                    array('name'=>'Cuenta destino','value'=>'$data->cuenta_destino->nombre'),
                    'monto',
                    array(
                        'name'=>'Fecha transferencia',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_transferencia)'),
                    ),
                 ));
            ?>
        </div>

    </li>
</ul>