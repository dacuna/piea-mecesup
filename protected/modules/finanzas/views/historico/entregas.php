<?php
$this->pageTitle = 'Hist&oacute;rico de entregas';
$this->breadcrumbs=array(
    'Hist&oacute;rico de entregas'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Hist&oacute;rico de entregas</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Hist&oacute;rico de entregas</h5>
            <p>A continuaci&oacute;n se presentan las &uacute;ltimas entregas de fondos realizadas:</p>

            <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider'=>$dataProvider,
                    'columns'=>array(
                    'id',          // display the 'title' attribute
                    array('name'=>'Cuenta de origen','value'=>'$data->cuenta->identificador'),  // display the 'name' attribute of the 'category' relation
                    'monto',   // display the 'content' attribute as purified HTML
                    array('name'=>'Iniciativa/Proyecto destino','value'=>'$data->entidad->tipoEntidad.": ".$data->entidad->nombre'),
                    array(            // display 'author.username' using an expression
                        'name'=>'Fecha de entrega',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_entrega)'),
                    ),
                 ));
            ?>
        </div>

    </li>
</ul>