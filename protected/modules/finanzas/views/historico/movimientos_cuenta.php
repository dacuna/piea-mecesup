<?php
$this->pageTitle = 'Movimientos';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Cuentas'=>array('/finanzas/crud/verCuentasEntidad','id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()),
    'Movimientos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Movimientos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Movimientos cuenta <?php echo $cuenta->identificador;?></h5>

            <div class="grid-view">
            <table class="items">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha de transferencia</th>
                    <th>Operador</th>
                    <th>Cuenta origen</th>
                    <th>Origen</th>
                    <th>Cuenta destino/Operaci&oacute;n</th>
                    <th>Destino/Detalle</th>
                    <th>Monto</th>
                    <th>Saldo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($movimientos as $movimiento):?>
                <tr>
                    <td style="text-align:right;"><?php echo $movimiento->id;?></td>
                    <td style="text-align:center;"><?php echo app()->dateFormatter->format("dd'/'MM'/'y",$movimiento->fechaTransferencia);?></td>
                    <td style="text-align:center;"><?php echo $movimiento->operador->nombrePresentacion;?></td>
                    <td>
                        <?php if($movimiento->operacion!=3):?>
                        <?php echo $movimiento->cuentaOrigen->identificador;?>
                        <?php else:?>
                        <?php echo $movimiento->cuentaOrigen;?>
                        <?php endif;?>
                    </td>
                    <td style="text-align:center;"><?php echo $movimiento->origen->getNombre();?></td>
                    <td style="text-align:center;">
                        <?php if($movimiento->operacion==0 ||$movimiento->operacion==3):?>
                        <?php echo $movimiento->cuentaDestino->identificador;?>
                        <?php else:?>
                        <?php echo $movimiento->cuentaDestino;?>
                        <?php endif;?>
                    </td>
                    <td>
                        <?php if($movimiento->operacion==0 || $movimiento->operacion==3):?>
                            <?php echo $movimiento->destino->getNombre();?>
                        <?php else:?>
                            <?php echo $movimiento->destino;?>
                        <?php endif;?>
                    </td>
                    <td style="text-align: right;">$<?php echo app()->NumberFormatter->formatDecimal($movimiento->monto);?></td>
                    <td style="text-align: right;">$<?php echo app()->NumberFormatter->formatDecimal($movimiento->saldo);?></td>
                </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

        </div>

    </li>
</ul>
<?php
$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
$cssFile=$baseScriptUrl.'/styles.css';
Yii::app()->getClientScript()->registerCssFile($cssFile);
?>