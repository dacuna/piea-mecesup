<?php
$this->pageTitle = 'Hist&oacute;rico de transferencias';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Hist&oacute;rico de transferencias'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Hist&oacute;rico de transferencias</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <div class="panel clearfix">
            <h5>Transferencias realizadas</h5>
            <p>A continuaci&oacute;n se presentan las &uacute;ltimas transferencias de fondos realizadas:</p>

            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider'=>$enviadas,
                'columns'=>array(
                    'id',
                    array(
                        'name'=>'Fecha transferencia',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_entrega)'
                    ),
                    array('name'=>'Usuario operador','value'=>'$data->operador->nombrePresentacion'),
                    array('name'=>'Destino','value'=>'$data->entidad->getTipoEntidad().": ".$data->entidad->getNombre()'),
                    array('name'=>'Cuenta origen','value'=>'$data->cuenta->identificador'),
                    array('name'=>'Monto','value'=>'app()->NumberFormatter->formatDecimal($data->monto)',
                          'htmlOptions'=>array('style' => 'text-align: right;')),
                )));
            ?>
        </div>

        <div class="panel clearfix">
            <h5>Transferencias recibidas</h5>
            <p>A continuaci&oacute;n se presentan las &uacute;ltimas transferencias de fondos recibidas:</p>

            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider'=>$recibidas,
                'columns'=>array(
                    'id',
                    array(
                        'name'=>'Fecha transferencia',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_entrega)'
                    ),
                    array('name'=>'Usuario operador','value'=>'$data->operador->nombrePresentacion'),
                    array('name'=>'Origen','value'=>'$data->entidadOrigen->getNombre()'),
                    array('name'=>'Cuenta origen','value'=>'$data->cuenta->identificador'),
                    array('name'=>'Monto','value'=>'app()->NumberFormatter->formatDecimal($data->monto)',
                          'htmlOptions'=>array('style' => 'text-align: right;')),
                )));
            ?>
        </div>

        <div class="panel clearfix">
            <h5>Transferencias internas</h5>
            <p>A continuaci&oacute;n se presentan las &uacute;ltimas transferencias internas de fondos:</p>

            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider'=>$internas,
                'columns'=>array(
                    'id',
                    array(
                        'name'=>'Fecha transferencia',
                        'value'=>'app()->dateFormatter->format("dd\'/\'MM\'/\'y",$data->fecha_transferencia)'
                    ),
                    array('name'=>'Usuario operador','value'=>'$data->operador->nombrePresentacion'),
                    array('name'=>'Cuenta origen','value'=>'$data->cuenta_origen->identificador'),
                    array('name'=>'Cuenta destino','value'=>'$data->cuenta_destino->identificador'),
                    array('name'=>'Monto','value'=>'app()->NumberFormatter->formatDecimal($data->monto)',
                        'htmlOptions'=>array('style' => 'text-align: right;')),
                )));
            ?>
        </div>

    </li>
</ul>