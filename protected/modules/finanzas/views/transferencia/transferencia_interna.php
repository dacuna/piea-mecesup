<?php
$this->pageTitle = 'Transferencia interna';
$this->breadcrumbs = array(
    $entidad->tipoEntidad => array($entidad->urlEntidad(), 'id' => $entidad->id),
    $entidad->getNombre() => array($entidad->urlEntidad(), 'id' => $entidad->id),
    'Transferencia interna'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Transferencia interna</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Transferencia interna</h5>

            <p>Mediante el siguiente formulario puedes traspasar dinero desde cuentas a las que tengas acceso. Este tipo
            de transferencias debe ser aceptada por el administrador de la plataforma.</p>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm', array('type' => 'nice')); ?>

            <?php echo $form->errorSummary($model);?>

            <label for="Transferencia_id_cuenta_origen">Cuenta origen:</label>
            <select name="Transferencia[id_cuenta_origen]" id="Transferencia_id_cuenta_origen">
                <?php foreach ($cuentas_acceso as $cuenta): ?>
                <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id: $cuenta[0]->id_cuenta; ?>"
                        data-tipo="<?php echo $cuenta[1];?>">
                    <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                    <?php echo ' | Saldo: $'.app()->NumberFormatter->formatDecimal($cuenta[0]->saldo);?>
                </option>
                <?php endforeach;?>
            </select>

            <?php echo $form->hiddenField($model, 'tipo_cuenta_origen');?>

            <label for="Transferencia_id_cuenta_destino">Cuenta destino:</label>
            <select name="Transferencia[id_cuenta_destino]" id="Transferencia_id_cuenta_destino">
                <?php foreach ($cuentas_acceso as $cuenta): ?>
                <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id: $cuenta[0]->id_cuenta; ?>"
                        data-tipo="<?php echo $cuenta[1];?>">
                    <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                    <?php echo ' | Saldo: $'.app()->NumberFormatter->formatDecimal($cuenta[0]->saldo);?>
                </option>
                <?php endforeach;?>
            </select>

            <?php echo $form->hiddenField($model, 'tipo_cuenta_destino');?>

            <?php echo $form->textFieldRow($model,'monto');?>

            <br>
            <?php echo CHtml::submitButton('Realizar transferencia', array('class' => 'nice radius medium button', 'encode' => false)); ?>
            <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(), array('id' => $entidad->id));?>"
               class="nice radius medium red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>
<script>
    $(function(){
        $('#Transferencia_tipo_cuenta_origen').val($('#Transferencia_id_cuenta_origen').find(":selected").attr('data-tipo'));
        $("#Transferencia_id_cuenta_origen").change(function () {
            $('#Transferencia_tipo_cuenta_origen').val($(this).find(":selected").attr('data-tipo'));
        });
        $('#Transferencia_tipo_cuenta_destino').val($('#Transferencia_id_cuenta_destino').find(":selected").attr('data-tipo'));
        $("#Transferencia_id_cuenta_destino").change(function () {
            $('#Transferencia_tipo_cuenta_destino').val($(this).find(":selected").attr('data-tipo'));
        });
    });
</script>