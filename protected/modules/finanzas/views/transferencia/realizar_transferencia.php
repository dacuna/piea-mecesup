<?php
$this->pageTitle = 'Transferencia';
$this->breadcrumbs = array(
    $entidad->tipoEntidad => array($entidad->urlEntidad(), 'id' => $entidad->id),
    $entidad->getNombre() => array($entidad->urlEntidad(), 'id' => $entidad->id),
    'Transferencia'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Transferencia</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app>
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Transferencia</h5>

            <p>Para realizar una transferencia de fondos, selecciona la cuenta de origen y la iniciativa o proyecto
                hacia donde
                quieres realizar la transferencia.</p>

            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm', array('type' => 'nice')); ?>
            <?php echo $form->errorSummary($model);?>

            <label for="TransferenciaGeneral_id_cuenta_origen">Cuenta origen:</label>
            <select name="TransferenciaGeneral[id_cuenta_origen]" id="TransferenciaGeneral_id_cuenta_origen">
                <?php foreach ($cuentas_acceso as $cuenta): ?>
                <option value="<?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->id: $cuenta[0]->id_cuenta; ?>"
                        data-tipo="<?php echo $cuenta[1];?>">
                    <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador: $cuenta[0]->cuenta->identificador; ?>
                    <?php echo ' | Saldo: $'.app()->NumberFormatter->formatDecimal($cuenta[0]->saldo);?>
                </option>
                <?php endforeach;?>
            </select>
            <?php echo $form->hiddenField($model, 'tipo_cuenta_origen');?>

            <div class="row" ng-controller="EntidadCtrl"
                 ng-init="selectedEntity=<?php echo ($model->id_entidad_destino!=null)?$model->id_entidad_destino:-1;?>;
                          selectedTypeEntity=<?php echo ($model->tipo_entidad_destino!=null)?$model->tipo_entidad_destino:-1;?>;
                          init()">

                <div class="six columns">
                    Iniciativa:
                    <ul id="iniciativas-choose">
                        <li ng-repeat="iniciativa in iniList"
                            ng-class="{selectedlist:(iniciativa.id==selectedEntity && selectedTypeEntity==0) || (iniciativa.id==selectedParent && selectedTypeEntity==1)}">
                            <a href="" ng-click="loadProject(iniciativa)">{{iniciativa.nombre}}</a>
                        </li>
                    </ul>
                </div>

                <div class="six columns">
                    Proyecto:
                    <ul id="proyectos-choose">
                        <li ng-repeat="project in proList|filter:{padre:selectedParent}" ng-class="{selectedlist:project.id==selectedEntity && selectedTypeEntity==1}">
                            <a href="" ng-click="selectProject(project)">{{project.nombre}}</a>
                        </li>
                    </ul>
                </div>

                <?php echo $form->hiddenField($model,'id_entidad_destino',array('value'=>'{{selectedEntity}}'));?>
                <?php echo $form->hiddenField($model,'tipo_entidad_destino',array('value'=>'{{selectedTypeEntity}}'));?>

                <p>{{messageSelected}}</p>
            </div>


            <?php echo $form->textFieldRow($model,'monto');?>

            <br>
            <?php echo CHtml::submitButton('Realizar transferencia', array('class' => 'nice radius medium button', 'encode' => false)); ?>
            <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(), array('id' => $entidad->id));?>"
               class="nice radius medium red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">

    function EntidadCtrl($scope, $http) {

        $scope.selectedParent=-1;//en el caso de que se seleccione un proyecto, aqui se almacena el padre
        $scope.messageSelected='Debes seleccionar una iniciativa o proyecto como destino de la entrega.';

        $scope.loadProject = function(entity){
            $scope.selectedEntity=entity.id;
            $scope.selectedParent=entity.id;
            $scope.selectedTypeEntity=0;
            $scope.messageSelected='Has seleccionado la iniciativa '+entity.nombre;

        }

        $scope.selectProject = function(project){
            if($scope.selectedEntity==project.id && $scope.selectedTypeEntity==1){
                var iniciative=_.find($scope.iniList,function(entity){return entity.id==project.padre;});
                $scope.selectedEntity=iniciative.id;
                $scope.selectedTypeEntity=0;
                $scope.messageSelected='Has seleccionado la iniciativa '+iniciative.nombre;
            }else{
                $scope.selectedEntity=project.id;
                $scope.selectedTypeEntity=1;
                $scope.selectedParent=project.padre;
                $scope.messageSelected='Has seleccionado el proyecto '+project.nombre;
            }
        }

        $scope.init = function(){
            $http.get('<?php echo url('/iniciativas/crudIniciativas/iniciativasUsuario');?>').success(function(data) {
                $scope.iniList=data;
                $http.get('<?php echo url('/proyectos/crud/proyectosUsuario');?>').success(function(dataProject) {
                    $scope.proList=dataProject;
                    if($scope.selectedEntity!=-1 && $scope.selectedTypeEntity!=-1){
                        if($scope.selectedTypeEntity==0){
                            var validEntity = _.find($scope.iniList,function(entity){return entity.id == $scope.selectedEntity;});
                            $scope.loadProject(validEntity);
                        }
                        else if($scope.selectedTypeEntity==1){
                            var validEntity = _.find($scope.proList,function(entity){return entity.id == $scope.selectedEntity;});
                            $scope.loadProject(validEntity.padre);
                            $scope.selectProject(validEntity);
                        }
                    }
                });
            });
        }

    }

    $(function(){
        $('#TransferenciaGeneral_tipo_cuenta_origen').val($('#TransferenciaGeneral_id_cuenta_origen').find(":selected").attr('data-tipo'));
        $("#TransferenciaGeneral_id_cuenta_origen").change(function () {
            $('#TransferenciaGeneral_tipo_cuenta_origen').val($(this).find(":selected").attr('data-tipo'));
        });
    });

</script>