<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Archivos</th>
            <th>Unidades</th>
            <th>Precio</th>
            <th>Total</th>
            <th>% del Total</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($items as $key_item=>$item):?>
        <tr>
            <td><?php echo $item->nombre;?></td>
            <td>
                <?php foreach($item->archivos as $key=>$archivo):?>
                <a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$presupuesto->id,
                    'i'=>$item->id,'f'=>$archivo));?>">PRES<?php echo ($key_item+1).zerofill($key+1,2);?></a>
                <?php if($key!=count($item->archivos)-1){echo '<br>';}?>
                <?php endforeach;?>
            </td>
            <td style="text-align: right;"><?php echo $item->unidades;?></td>
            <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($item->precio);?></td>
            <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($item->precio*$item->unidades);?></td>
            <td style="text-align:right;"><?php echo number_format(
                (($item->precio*$item->unidades)/$presupuesto->totalMontoItems)*100,2);?>%</td>
        </tr>
            <?php endforeach;?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align:right;"><b>TOTAL</b></td>
            <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($presupuesto->totalMontoItems);?></td>
            <td style="text-align:right;">100.00%</td>
        </tr>
    </tbody>
</table>