<table>
    <thead>
        <tr>
            <th>Fecha</th>
            <th>N° Boleta</th>
            <th>Item</th>
            <th>Detalle</th>
            <th>Valor</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($recibo->items as $item):?>

        <?php foreach($item->articulos as $key=>$articulo):?>
        <tr>
            <?php if($key==0):?>
            <td><?php echo $item->fecha;?></td>
            <td><a href="<?php echo url('/finanzas/operaciones/sendFile',array('p'=>$recibo->id,
                'i'=>$item->id,'f'=>$item->boleta,'folder'=>'recibos'));?>"><?php echo $item->numero_boleta;?></a></td>
            <?php else:?>
            <td></td>
            <td></td>
            <?php endif;?>

            <td><?php echo $articulo->item_presupuestario;?></td>
            <td><?php echo $articulo->itemPresupuesto->nombre;?></td>
            <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal(
                $articulo->cantidad*$articulo->costo_unitario);?></td>
        </tr>
            <?php endforeach;?>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;">Total boleta</td>
        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($item->valor);?></td>
    </tr>

        <?php endforeach;?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;">Total de gastos:</td>
        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->totalMontoItems);?></td>
    </tr>
    <?php if($recibo->tipo_recibo!=1):?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;">Saldo a favor del deudor:</td>
        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->saldo_favor_deudor);?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;">Saldo a reintegrar en caja:</td>
        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->calcularSaldoReintegrarCaja());?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;">Total igual:</td>
        <td style="text-align:right;">$<?php echo app()->NumberFormatter->formatDecimal($recibo->totalMontoItems);?></td>
    </tr>
        <?php endif;?>
    </tbody>
</table>