<?php
Yii::import('application.modules.usuarios.components.Entidad');
Yii::import('xupload.models.XUploadForm');

/**
 * Controlador que implementa todas las acciones que proveen de las operaciones financieras que pueden
 * realizar las entidades en el sistema.
 *
 * Las operaciones financieras soportadas por el sistema son: solicitud de un vale a rendir, crear una
 * recuperacion de gastos, pagos a proveedores y solicitar compras a la unidad de abastecimiento. Para
 * ejecutar cualquiera de las operaciones se debe tener un presupuesto de items aprobado en una entidad.
 * Los presupuestos los aprueban los superiores de las cuentas utilizadas (en el ultimo nivel se encuentra
 * el administrador piea).
 *
 * @version   1.0
 * @since     2012-11-14
 * @author    dacuna <dacuna@dacuna.me>
 */
class OperacionesController extends Controller
{
    /**
     * Inicializacion del controlador. Se cargan los handlers de los eventos utilizados en el controlador.
     *
     * @return  void
     */
    public function init()
    {
        $this->onSolicitarPresupuesto = array(app()->mailman, 'notificacionHandler');
        $this->onSolicitarRecibo = array(app()->mailman, 'notificacionHandler');
        $this->onAceptarPresupuesto = array(app()->mailman, 'notificacionHandler');
        $this->onRechazarPresupuesto = array(app()->mailman, 'notificacionHandler');
        $this->onAceptarRecibo = array(app()->mailman, 'notificacionHandler');
        $this->onRechazarRecibo = array(app()->mailman, 'notificacionHandler');
        $this->onNotificarEntregaRetiro = array(app()->mailman, 'notificacionHandler');
        $this->onNotificarReciboRetiro = array(app()->mailman, 'notificacionHandler');
    }

    /**
     * Accion para que el integrante de una entidad con los permisos correspondientes pueda crear
     * un presupuesto en dicha entidad. Un presupuesto puede estar asociado a mas de una cuenta y
     * puede contener multiples items.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad
     */
    public function actionCrearPresupuesto($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('ingresar_presupuesto', user()->id);
        $presupuesto = new Presupuesto;
        if (isset($_POST['Presupuesto'])) {
            $presupuesto->attributes = $_POST['Presupuesto'];
            $presupuesto->setEntidad($entidad);
            if ($presupuesto->save()) {
                user()->setFlash('warning', 'Para continuar, debes ingresar los items a tu presupuesto.');
                $this->redirect(array('/finanzas/operaciones/enviarPresupuesto', 'id' => $presupuesto->primaryKey));
            }
        }
        $this->render('crear_presupuesto', array(
            'presupuesto' => $presupuesto,
            'entidad' => $entidad
        ));
    }

    /**
     * Accion que permite enviar un presupuesto para revision. Los presupuestos son revisados por
     * los superiores de la entidad donde son creados y tambien por el superior de la cuenta
     * utilizada para crearlo. En esta accion tambien se permiten agregar items a los presupuestos,
     * por lo tanto cumple una doble funcion.
     *
     * @param int $id ID del presupuesto.
     *
     * @throws  \CException
     */
    public function actionEnviarPresupuesto($id)
    {
        $presupuesto = Presupuesto::managePresupuesto($id);
        $boletas = new XUploadForm;
        $item = new ItemPresupuesto;
        $entidad = $presupuesto->getEntidad();

        if (isset($_POST['ItemPresupuesto'])) {
            $item->attributes = $_POST['ItemPresupuesto'];
            $item->id_presupuesto = $presupuesto->id;
            if ($item->validate()) {
                $transaction = app()->db->beginTransaction();
                try {
                    $item->save(false);
                    $this->addFiles($presupuesto->id, $item->primaryKey);
                    $transaction->commit();
                    user()->setFlash('success', 'El item se ha agregado correctamente al presupuesto.');
                    $this->redirect(array('/finanzas/operaciones/enviarPresupuesto', 'id' => $presupuesto->id,
                        'tipo' => $entidad->getTipoEntidadNumerico()));
                } catch (CException $e) {
                    $transaction->rollback();
                    user()->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
        } else if (isset($_POST['enviarPresupuesto'])) {
            if (count($presupuesto->items) == 0) {
                user()->setFlash('error', 'Debes ingresar al menos un item antes de enviar el presupuesto.');
            } else {
                $presupuesto->estado = 0;
                if ($presupuesto->save(false)) {
                    $this->onSolicitarPresupuesto(new CEvent($this, array('tipo' => 'solicitar_presupuesto',
                        'presupuesto' => $presupuesto, 'entidad' => $entidad, 'destinatario' => $entidad->getEncargado()->id,
                        'encargado' => 'entidad')));
                    user()->setFlash('success', 'El presupuesto ha sido enviado correctamente para revisi&oacute;n.');
                    $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
                } else {
                    user()->setFlash('error', 'Ha ocurrido un error al enviar el presupuesto, por favor int&eacute;ntelo
                        nuevamente.');
                }
            }
        }

        $this->render('enviar_presupuesto', array(
            'entidad' => $entidad,
            'boletas' => $boletas,
            'item' => $item,
            'presupuesto' => $presupuesto
        ));
    }

    /**
     * Accion que permite al usuario que solicito un presupuesto el editarlo. Por editar se entiende
     * que se pueden agregar nuevos items al presupuesto como tambien que se pueden editar datos
     * simples de este (cambiar la cuenta no esta permitido).
     *
     * @param int $id ID del presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEditarPresupuesto($id)
    {
        $presupuesto = Presupuesto::model()->findByPk($id);
        if ($presupuesto == null || $presupuesto->estado == 1 || $presupuesto->id_usuario_que_envia != user()->id)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        $boletas = new XUploadForm;
        $item = new ItemPresupuesto;
        $entidad = $presupuesto->getEntidad();

        if (isset($_POST['ItemPresupuesto'])) {
            $item->attributes = $_POST['ItemPresupuesto'];
            $item->id_presupuesto = $presupuesto->id;
            if ($item->validate()) {
                $transaction = app()->db->beginTransaction();
                try {
                    $item->save(false);
                    $this->addFiles($presupuesto->id, $item->primaryKey);
                    $transaction->commit();
                    user()->setFlash('success', 'El item se ha agregado correctamente al presupuesto.');
                    $this->redirect(array('/finanzas/operaciones/editarPresupuesto', 'id' => $presupuesto->id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    app()->handleException($e);
                }
            }
        } else if (isset($_POST['editarPresupuesto'])) {
            $presupuesto->estado = 0;
            if ($presupuesto->save(false)) {
                $this->onSolicitarPresupuesto(new CEvent($this, array('tipo' => 'solicitar_presupuesto',
                    'presupuesto' => $presupuesto, 'entidad' => $entidad, 'destinatario' => $entidad->getEncargado()->id)));
                user()->setFlash('success', 'El presupuesto ha sido enviado correctamente para revisi&oacute;n.');
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error al enviar el presupuesto, por favor int&eacute;ntelo
                        nuevamente.');
            }
        }

        $this->render('editar_presupuesto', array(
            'entidad' => $entidad,
            'boletas' => $boletas,
            'item' => $item,
            'presupuesto' => $presupuesto
        ));
    }

    /**
     * Accion que permite visualizar un presupuesto junto con todos los items que estan agregados a este.
     * Ademas, se muestra un resumen con los totales en forma de una tabla.
     *
     * @param int $id ID del presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerPresupuesto($id)
    {
        $presupuesto = Presupuesto::managePresupuesto($id, false);
        $entidad = $presupuesto->getEntidad();
        $entidad->checkPermisos('integrante', user()->id);

        $mostrar_apr = ($presupuesto->getEntidad()->getEncargado()->id == user()->id) ? true : false;
        if ($presupuesto->estado == 3)
            $mostrar_apr = ($presupuesto->getEntidad()->getSUperior()->id == user()->id) ? true : false;
        if ($presupuesto->estado == 4)
            $mostrar_apr_final = ($presupuesto->cuenta->encargado == user()->id) ? true : false;
        else
            $mostrar_apr_final = false;

        $this->render('ver_presupuesto', array(
            'presupuesto' => $presupuesto,
            'entidad' => $entidad,
            'mostrar_aprobacion' => $mostrar_apr,
            'mostrar_aprobacion_final' => $mostrar_apr_final,
            'cuentas' => Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico())
        ));
    }

    /**
     * Accion que permite al usuario correspondiente el aprobar un presupuesto. El flujo de aprobacion consiste
     * en primero debe aprobar el presupuestp el superior de la entidad, para esto tambien se verifica si es
     * que la cuenta tiene los fondos necesarios, si es que si los tiene entonces se reservan estos fondos (se
     * realiza el descuento de la cuenta). Luego, el presupuesto debe ser aprobado por el encargado de la
     * cuenta.
     *
     * @param int $id ID del presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAprobarPresupuesto($id)
    {
        $presupuesto = Presupuesto::managePresupuesto($id, false);
        $entidad = $presupuesto->getEntidad();
        $entidad->checkPermisos('aprobar_presupuesto', user()->id);
        $presupuesto->setScenario('aceptarPresupuesto');
        if ($presupuesto->estado == 3) $presupuesto->setScenario('aceptarPresupuestoDeProyectoPorIniciativa');
        //se verifica que si es un proyecto entonces el jefe de proyecto no puede volver a aprobarlo
        if ($entidad->getTipoEntidad == 'Proyecto' && $presupuesto->estado == 3) {
            $encargado = $entidad->getEncargado();
            if ($encargado->id == user()->id && $encargado->id != $entidad->getSuperior()->id)
                throw new CHttpException(500, "No estas autorizado a ejecutar esta acci&oacute;n.");
        }

        if (isset($_POST['Presupuesto'])) {
            $presupuesto->attributes = $_POST['Presupuesto'];
            if ($presupuesto->validate()) {
                //se valida que la cuenta seleccionada no supere el total del presupuesto
                if (Cuenta::getSaldoCuentaConAcceso($entidad->getId(), $entidad->getTipoEntidadNumerico(), $presupuesto->id_cuenta)
                    < $presupuesto->totalMontoItems
                ) {
                    user()->setFlash("error", "La cuenta seleccionada no tiene saldo suficiente para cubrir el total del presupuesto.");
                    $this->redirect(array('/finanzas/operaciones/verPresupuesto', 'id' => $presupuesto->id));
                } else {
                    if ($presupuesto->estado == 4) $presupuesto->estado = 1;
                    else {
                        if ($presupuesto->estado == 3) $presupuesto->estado = 4;
                        else {
                            $presupuesto->estado = ($presupuesto->estado == 0 && $entidad->getTipoEntidad() == 'Proyecto') ? 3 : 4;
                        }
                    }
                    if ($presupuesto->save(false)) {
                        Mensaje::contestarMensajePresupuesto($presupuesto->id);
                        //se descuenta el dinero desde la cuenta del presupuesto
                        if ($presupuesto->estado == 1) {
                            $presupuesto->reservarFondosPresupuesto();
                            user()->setFlash('success', 'El presupuesto ha sido aceptado correctamente.');
                            $this->onAceptarPresupuesto(new CEvent($this, array('tipo' => 'aceptar_presupuesto',
                                'presupuesto' => $presupuesto, 'entidad' => $entidad)));
                            //se le envia el mensaje al responsable de la cuenta
                            $this->onAceptarPresupuesto(new CEvent($this, array('tipo' => 'responsable_presupuesto',
                                'presupuesto' => $presupuesto, 'entidad' => $entidad, 'responsable' => $presupuesto->cuenta->encargadoCuenta->id)));
                            if ($presupuesto->unidad_de_abastecimiento != null)
                                $this->onAceptarPresupuesto(new CEvent($this, array('tipo' => 'subir_factura',
                                    'presupuesto' => $presupuesto, 'entidad' => $entidad)));
                        } else {
                            if ($presupuesto->estado == 3) {
                                //se notifica al coordinador de la iniciativa de que tiene que aprobar un presupuesto
                                user()->setFlash('success', "El presupuesto ha sido pre-aceptado. Ahora el coordinador de la
                                 iniciativa a la que pertenece este proyecto deber&aacute; aprobar el presupuesto.");
                                $this->onSolicitarPresupuesto(new CEvent($this, array('tipo' => 'solicitar_presupuesto',
                                    'presupuesto' => $presupuesto, 'entidad' => $entidad, 'destinatario' => $entidad->getSuperior()->id,
                                    'encargado' => 'entidad')));
                            } else {
                                //se notifica al responsable de la cuenta de que tiene que aprobar el presupuesto
                                user()->setFlash('success', "El presupuesto ha sido pre-aceptado. Ahora el responsable de la
                                 cuenta seleccionada para el presupuesto deber&aacute; aprobarlo.");
                                $this->onSolicitarPresupuesto(new CEvent($this, array('tipo' => 'solicitar_presupuesto',
                                    'presupuesto' => $presupuesto, 'entidad' => $entidad, 'destinatario' => $entidad->getSuperior()->id,
                                    'encargado' => 'cuenta')));
                            }
                        }
                        $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
                    } else {
                        user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente. Recuerda ingresar
                        la cuenta de fondos para el presupuesto.');
                        $this->redirect(array('/finanzas/operaciones/verPresupuesto', 'id' => $presupuesto->id));
                    }
                }
            }
        }
    }

    /**
     * Accion que permite al usuario correspondiente el rechazar un presupuesto. Sigue la misma logica que
     * la accion aceptarPresupuesto. Si es que el usuario encargado de la cuenta rechaza el presupuesto entonces
     * se devuelve el fondo a la cuenta.
     *
     * @param int $id ID del presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRechazarPresupuesto($id)
    {
        $presupuesto = Presupuesto::managePresupuesto($id, false);
        $entidad = $presupuesto->getEntidad();
        $entidad->checkPermisos('aprobar_presupuesto', user()->id);
        $presupuesto->estado = -1;
        if (isset($_POST['comentario_rechazo']))
            $presupuesto->comentario_revisor = app()->input->post('comentario_rechazo');
        if ($presupuesto->save(false)) {
            Mensaje::contestarMensajePresupuesto($presupuesto->id);
            user()->setFlash('success', 'El presupuesto ha sido rechazado correctamente.');
            $this->onRechazarPresupuesto(new CEvent($this, array('tipo' => 'rechazar_presupuesto',
                'presupuesto' => $presupuesto, 'entidad' => $entidad)));
            $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
        } else {
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
            $this->redirect(array('/finanzas/operaciones/verPresupuesto', 'id' => $presupuesto->id));
        }
    }

    /**
     * actionCrearRecibo: permite crear un recibo financiero en una entidad especifica.
     * Los recibos pueden ser: recuperacion de gastos(1) y vale a rendir(2).
     *
     * @param   int $id   ID de la entidad donde se desea crear el recibo
     * @param   int $tipo Tipo de la entidad donde se desea crear el recibo
     * @param   int $tf   Tipo del recibo, 1=recuperaciond de gastos, 2=vale a rendir
     *
     * @throws  \CHttpException
     *
     * @since   31-12-2012
     *
     */
    public function actionCrearRecibo($id, $tipo, $tf)
    {
        $entidad = new Entidad($id, $tipo);
        $model = new Recibo;
        $model->setTipoRecibo($tf);
        if ($model->tipo_recibo == 1)
            $entidad->checkPermisos('ingresar_recuperacion', user()->id);
        else if ($model->tipo_recibo == 2)
            $entidad->checkPermisos('ingresar_vale', user()->id);
        $model->setEntidad($entidad);
        $model->estado = 0;

        $presupuestos = Presupuesto::getPresupuestosEntidadValidos($entidad);

        if (isset($_POST['Recibo'])) {
            $model->attributes = $_POST['Recibo'];
            $model->presupuestos = $model->presupuestosIds;

            if ($model->tipo_recibo == 2) {
                $model->monto_vale = $model->totalMontoItemsPresupuestos;
            }
            if ($model->save()) {
                $pre = new Presupuesto;
                $pre->marcarPresupuestosComoUtilizados($model->presupuestosIds);
                if ($model->tipo_recibo == 1) {
                    user()->setFlash('warning', 'Para completar la recuperaci&oacute;n debes agregar los art&iacute;culos
                                                correspondientes.');
                    $this->redirect(array('/finanzas/operaciones/llenarRecibo', 'id' => $model->primaryKey));
                } else if ($model->tipo_recibo == 2) {
                    user()->setFlash('success', 'Para continuar debes llevar el formulario de retiro de fondos a la oficina
                     de PIEA. Puedes descargar dicho formulario desde esta p&aacute;gina.');
                    $this->redirect(array('/finanzas/operaciones/verRetirosFondoDisponiblePorUsuario', 'id' => $entidad->getId(),
                        'tipo' => $entidad->getTipoEntidadNumerico()));
                }
            }
        }

        $this->render('crear_recibo', array(
            'presupuestos' => $presupuestos,
            'model' => $model,
            'entidad' => $entidad,
            'cuentas' => Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico())
        ));
    }

    /**
     * Accion que permite editar un recibo por el usuario que solicito dicho recibo.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEditarRecibo($id)
    {
        $recibo = Recibo::model()->findByPk($id);
        if ($recibo == null || $recibo->id_usuario_que_solicita != user()->id || ($recibo->estado != -2 && $recibo->estado != -1))
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        if (isset($_POST['Recibo'])) {
            $recibo->attributes = $_POST['Recibo'];
            $recibo->presupuestos = $recibo->presupuestosIds;
            if ($recibo->save()) {
                user()->setFlash('success', 'La informaci&oacute;n b&aacute;sica de la recuperaci&oacute;n de gastos
                    ha sido editado correctamente.');
                $this->redirect(array('/finanzas/operaciones/agregarItemsRecuperacion', 'id' => $recibo->id));
            }
        }
        $entidad = $recibo->getEntidad();
        $this->render('editar_recibo', array(
            'recibo' => $recibo,
            'entidad' => $entidad,
            'cuentas' => Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico())
        ));
    }

    /**
     * Accion que permite visualizar un recibo por el usuario que tenga los permisos de ver recuperaciones
     * de gastos en una entidad.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerRecibo($id)
    {
        $recibo = Recibo::load($id);
        $entidad = $recibo->getEntidad();
        if ($recibo->id_usuario_que_solicita != user()->id) {
            if ($recibo->tipo_recibo == 1)
                $entidad->checkPermisos('ver_recuperacion', user()->id);
            else if ($recibo->tipo_recibo == 2)
                $entidad->checkPermisos('ver_vale', user()->id);
        }

        $mostrar_apr = ($entidad->getEncargado()->id == user()->id) ? true : false;
        $mostrar_edit = ($recibo->id_usuario_que_solicita == user()->id) ? true : false;

        $this->render('ver_recibo', array(
            'recibo' => $recibo,
            'entidad' => $entidad,
            'mostrar_aprobacion' => $mostrar_apr,
            'mostrar_edit' => $mostrar_edit
        ));
    }

    /**
     * Accion que permite aprobar un recibo en una entidad. Los recibos deben ser aprobados
     * por la persona que tenga los permisos de ya sea aprobar una recuperacion de gastos
     * y aprobar un vale a rendir. Por defecto el encargado de la entidad tiene dichos
     * permisos pero pueden ser heredados.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAprobarRecibo($id)
    {
        $recibo = Recibo::load($id);
        $entidad = $recibo->getEntidad();
        if ($recibo->tipo_recibo == 1)
            $entidad->checkPermisos('aprobar_recuperacion', user()->id);
        else if ($recibo->tipo_recibo == 2)
            $entidad->checkPermisos('aprobar_vale', user()->id);
        if ($recibo->tipo_recibo == 2 && $recibo->saldo_reintegrar_caja > 0)
            $recibo->cambiarEstado(5);
        else
            $recibo->cambiarEstado(1);
        $recibo->presupuestos = $recibo->presupuestosIds;
        if ($recibo->save(false)) {
            Mensaje::contestarMensajePresupuesto($recibo->id, $recibo->tipoRecibo);
            user()->setFlash('success', "El formulario de {$recibo->tipoRecibo} ha sido aceptado correctamente.");
            $this->onAceptarRecibo(new CEvent($this, array('tipo' => 'aceptar_recibo', 'recibo' => $recibo,
                'entidad' => $entidad)));
            $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
        } else {
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
            $this->redirect(array('/finanzas/operaciones/verRecibo', 'id' => $recibo->id));
        }
    }

    /**
     * Accion que permite rechazar un recibo en una entidad. Los recibos deben ser rechazados
     * por la persona que tenga los permisos de ya sea aprobar una recuperacion de gastos
     * y aprobar un vale a rendir. Por defecto el encargado de la entidad tiene dichos
     * permisos pero pueden ser heredados.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRechazarRecibo($id)
    {
        $recibo = Recibo::load($id);
        $entidad = $recibo->getEntidad();
        if ($recibo->tipo_recibo == 1)
            $entidad->checkPermisos('aprobar_recuperacion', user()->id);
        else if ($recibo->tipo_recibo == 2)
            $entidad->checkPermisos('aprobar_vale', user()->id);
        $recibo->cambiarEstado(-1);
        if (isset($_POST['comentario_rechazo']))
            $recibo->comentario_revisor = app()->input->post('comentario_rechazo');
        $recibo->presupuestos = $recibo->presupuestosIds;
        if ($recibo->save(false)) {
            Mensaje::contestarMensajePresupuesto($recibo->id, $recibo->tipoRecibo);
            user()->setFlash('success', "El formulario de {$recibo->tipoRecibo} ha sido rechazado correctamente.");
            $this->onRechazarRecibo(new CEvent($this, array('tipo' => 'rechazar_recibo',
                'recibo' => $recibo, 'entidad' => $entidad)));
            $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
        } else {
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
            $this->redirect(array('/finanzas/operaciones/verRecibo', 'id' => $recibo->id));
        }
    }

    /**
     * Accion que carga todos los vales de recuperacion de gastos que se han solicitado en una
     * entidad. Solo los usuarios con los permisos correspondientes pueden ver dicho listado.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad
     *
     * @return  void
     */
    public function actionVerRecuperacionGastos($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('ver_recuperacion', user()->id);
        //se cargan los recibos de recuperacion que esten aceptados
        $recuperaciones = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado in (0,1,4) and tipo_recibo=1 order by fecha_vale DESC',
            array(':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico()));

        $pendientes = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado in (-2) and tipo_recibo=1 order by fecha_vale DESC',
            array(':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico()));

        $this->render('ver_recuperaciones', array(
            'recuperaciones' => $recuperaciones,
            'pendientes' => $pendientes,
            'entidad' => $entidad
        ));
    }

    /**
     * Accion que genera una recuperacion de gastos (o vale a rendir) siguiendo el formato original de los
     * documentos oficiales de PIEA. El documento generado es un PDF.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionGenerarRecuperacion($id)
    {
        $recuperacion = $this->checkReciboPermisos($id);
        $entidad = $recuperacion->getEntidad();
        /*$this->render('_recuperacion',
            array('entidad'=>$entidad,'recuperacion'=>$recuperacion));*/
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('_recuperacion',
            array('entidad' => $entidad, 'recuperacion' => $recuperacion), true));
        $html2pdf->Output();
    }

    /**
     * Accion que carga todos los retiros de fondos que se han creado en una
     * entidad. Solo los usuarios con los permisos correspondientes pueden ver dicho listado.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad
     *
     * @return  void
     */
    public function actionVerRetirosFondoDisponible($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('gestionar_vales', user()->id);
        $retiros = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado in(0,2,3,5) and tipo_recibo=2
            order by fecha_vale DESC', array(
            ':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico()
        ));

        $retiros_historico = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=1 and tipo_recibo=2
            order by fecha_vale DESC', array(
            ':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico()
        ));

        $this->render('retiros_disponibles', array(
            'entidad' => $entidad,
            'retiros' => $retiros,
            'retiros_historico' => $retiros_historico
        ));
    }

    /**
     * Accion que carga todos los retiros de fondo que el usuario logeado a solicitado en una entidad en
     * particular.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad
     *
     * @return  void
     */
    public function actionVerRetirosFondoDisponiblePorUsuario($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('integrante', user()->id);
        $retiros = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado in(0,2,3,5) and tipo_recibo=2
            and id_usuario_que_solicita=:iu order by fecha_vale DESC', array(
            ':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico(), ':iu' => user()->id
        ));

        $retiros_historico = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=1 and tipo_recibo=2
             and id_usuario_que_solicita=:iu order by fecha_vale DESC', array(
            ':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico(), ':iu' => user()->id
        ));

        $this->render('retiros_disponibles', array(
            'entidad' => $entidad,
            'retiros' => $retiros,
            'retiros_historico' => $retiros_historico
        ));
    }

    /**
     * Accion que genera el documento PDF oficial solicitado para un retiro de fondos..
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionGenerarRetiroFondos($id)
    {
        $recibo = $this->checkReciboPermisos($id);
        $persona = Persona::model()->findByPk($recibo->id_usuario_que_solicita);
        if ($recibo->fecha_emision_documento == null) {
            $recibo->fecha_emision_documento = date("Y-m-d");
            $recibo->presupuestos = $recibo->presupuestosIds;
            $recibo->save(false);
        }
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('_retiro_fondos',
            array('usuario' => $persona, 'recibo' => $recibo), true));
        $html2pdf->Output();
    }

    /**
     * Accion que genera un pdf con el documento oficial necesario para un comprobante de recibo.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionGenerarComprobanteRecibo($id)
    {
        $recibo = $this->checkReciboPermisos($id);
        $persona = Persona::model()->findByPk($recibo->id_usuario_que_solicita);
        if ($recibo->fecha_emision_documento == null) {
            $recibo->fecha_emision_documento = date("Y-m-d");
            $recibo->presupuestos = $recibo->presupuestosIds;
            $recibo->save(false);
        }
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('_comprobante_recibo',
            array('usuario' => $persona, 'recibo' => $recibo), true));
        $html2pdf->Output();
    }

    /**
     * Accion que genera un documento pdf con todos los documentos anexados como uno solo necesarios para
     * una recuperacion de gastos..
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionGenerarTodos($id)
    {
        $recibo = $this->checkReciboPermisos($id);
        $persona = Persona::model()->findByPk($recibo->id_usuario_que_solicita);
        if ($recibo->fecha_emision_documento == null) {
            $recibo->fecha_emision_documento = date("Y-m-d");
            $recibo->presupuestos = $recibo->presupuestosIds;
            $recibo->save(false);
        }
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html = $this->renderPartial('_comprobante_recibo',
            array('usuario' => $persona, 'recibo' => $recibo), true);
        $entidad = $recibo->getEntidad();
        $html .= $this->renderPartial('_recuperacion',
            array('entidad' => $entidad, 'recuperacion' => $recibo), true);
        $html2pdf->WriteHTML($html);
        $html2pdf->Output();
    }

    /**
     * Accion que genera una notificacion de entrega del formulario de retiro de fondos en las oficias de PIEA al
     * administrador del sistema. Esta notificacion indica que se ha entregado (o se entregara) dicho formulario.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     */
    public function actionNotificarEntregaRetiro($id)
    {
        $recibo = $this->checkReciboPermisos($id);
        $recibo->estado = 2;
        $recibo->presupuestos = $recibo->presupuestosIds;
        if ($recibo->save(false)) {
            //notificar la entrega
            $this->onNotificarEntregaRetiro(new CEvent($this, array('tipo' => 'notificar_entrega_retiro', 'retiro' => $recibo
            , 'entidad' => $recibo->getEntidad())));
            user()->setFlash('success', 'Se ha notificado correctamente la entrega del retiro de fondos al administrador de PIEA.');
        } else
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/finanzas/operaciones/verRetirosFondoDisponiblePorUsuario', 'id' => $recibo->getEntidad()->getId(),
            'tipo' => $recibo->getEntidad()->getTipoEntidadNumerico()));
    }

    /**
     * Accion que permite visualizar por los usuarios con los permisos correspondientes un retiro de
     * fondos en una entidad.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerRetiroFondos($id)
    {
        $recibo = $this->checkReciboPermisos($id);

        $this->render('ver_retiro_fondos', array(
            'retiro' => $recibo,
            'entidad' => $recibo->getEntidad()
        ));
    }

    /**
     * Accion que notifica a un usuario que su recibo de retiro de fondos ha sido recibido en las oficinas de
     * PIEA.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionNotificarReciboRetiro($id)
    {
        $recibo = $this->checkReciboPermisos($id);
        $recibo->estado = 3;
        $recibo->presupuestos = $recibo->presupuestosIds;
        if ($recibo->save(false)) {
            //notificar la entrega
            $this->onNotificarReciboRetiro(new CEvent($this, array('tipo' => 'notificar_recibo_retiro', 'retiro' => $recibo
            , 'entidad' => $recibo->getEntidad())));
            user()->setFlash('success', 'Se ha notificado correctamente el recibo del retiro de fondos.');
        } else
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/finanzas/operaciones/verRetirosFondoDisponible', 'id' => $recibo->getEntidad()->getId(),
            'tipo' => $recibo->getEntidad()->getTipoEntidadNumerico()));
    }

    /**
     * Accion que permite completar un recibo (ya sea para vale a rendir o recuperacion de gastos) agregandole
     * items a este. Solo se pueden agregar items si es que el recibo no ha sido enviado para revision o utilizado
     * en una operacion.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionLlenarRecibo($id)
    {
        $recibo = $this->checkLlenarReciboPermisos($id, 'ingresar');
        if ($recibo->estado != -2 && $recibo->estado != -1 && $recibo->estado != 3 && $recibo->estado != 0)
            throw new CHttpException(404, "La p&aacute;gina solicitada no existe.");
        $model = new ItemRecibo;

        $itemsBoleta = array();
        $itemsPresupuestos = $recibo->itemsPresupuestos;
        foreach ($itemsPresupuestos as $item) {
            $itemsBoleta[] = new ItemBoletaRecibo;
        }

        $boleta = new XUploadForm;
        $valid_second = true;

        if (isset($_POST['submitDatas']) && isset($_POST['ItemRecibo'])) {
            $model->attributes = $_POST['ItemRecibo'];
            if (isset($_POST['ItemBoletaRecibo'])) {
                $model->id_recibo = $recibo->id;
                $valid = $model->validate();
                $itemsBoletaFinal = array();

                //almacenar todos los itemsBoletaRecibo
                foreach ($_POST['ItemBoletaRecibo'] as $i => $item) {
                    if (isset($_POST['ItemBoletaRecibo'][$i])) {
                        $itemsBoleta[$i] = new ItemBoletaRecibo;
                        $itemsBoleta[$i]->attributes = $_POST['ItemBoletaRecibo'][$i];
                        $itemsBoletaFinal[] = $itemsBoleta[$i];
                        $valid = $valid && $itemsBoleta[$i]->validate();
                        $valid_second = $valid_second && $itemsBoleta[$i]->validate();
                    }
                }

                if ($valid) {
                    $suma = 0;
                    foreach ($itemsBoletaFinal as $item)
                        $suma += $item->cantidad * $item->costo_unitario;
                    if ($suma != $model->valor) {
                        user()->setFlash("error", "El total de la boleta debe ser igual al total esperado en los art&iacute;culos
                        seleccionados.");
                        user()->setState('images', null);
                    } else {
                        $transaction = app()->db->beginTransaction();
                        try {
                            $model->save(false);
                            $this->addFiles($recibo->id, $model->primaryKey, 'recibos');
                            foreach ($itemsBoletaFinal as $i => $item) {
                                $item->id_item_recibo = $model->primaryKey;
                                $item->save(false);
                            }
                            if ($recibo->tipo_recibo == 1) {
                                $recibo->monto_vale += $model->valor;
                            }
                            $recibo->actualizarTotales();
                            $recibo->presupuestos = $recibo->presupuestosIds;
                            $recibo->save(false);
                            $transaction->commit();
                            user()->setFlash('success', "El item se ha agregado correctamente al formulario de {$recibo->tipoRecibo}");
                            $this->redirect(array('/finanzas/operaciones/llenarRecibo', 'id' => $recibo->id));
                        } catch (Exception $e) {
                            $transaction->rollback();
                            user()->setFlash('error', 'Ha ocurrido un error, por favor verifica que todos los campos han sido
                            completados correctamente (subir una boleta es obligatorio).');
                        }
                    }
                } else {
                    if (!$valid_second) {
                        user()->setState('images', null);
                        user()->setFlash("error", "Debes completar correctamente los items de la boleta.
                    Verifica:<br>- Cantidad y costo unitario debe ser entero.");
                    }
                }
            }
        } else if (isset($_POST['enviar_recibo'])) {
            $recibo->total_gastos = $recibo->totalMontoItems;
            $recibo->estado = ($recibo->tipo_recibo == 1) ? 0 : 4;
            $recibo->presupuestos = $recibo->presupuestosIds;
            if ($recibo->save(false)) {
                $entidad = $recibo->getEntidad();
                $this->onSolicitarRecibo(new CEvent($this, array('tipo' => 'solicitar_recibo', 'recibo' => $recibo,
                    'entidad' => $entidad, 'destinatario' => $entidad->getEncargado()->id, 'tipoRecibo' => $recibo->tipoRecibo)));
                user()->setFlash('success', "El formulario de {$recibo->tipoRecibo} ha sido enviado correctamente para
                    revisi&oacute;n.");
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }
            user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntalo nuevamente.');
        }

        if (isset($_POST['submitDatas']) && !isset($_POST['ItemBoletaRecibo']))
            user()->setFlash("error", "Debes seleccionar al menos un art&iacute;culo del presupuesto.");

        $this->render('llenar_recibo', array(
            'model' => $model,
            'entidad' => $recibo->getEntidad(),
            'recibo' => $recibo,
            'boleta' => $boleta,
            'itemsBoleta' => $itemsBoleta,
            'itemsPresupuestos' => $itemsPresupuestos,
            'valid_second' => $valid_second
        ));
    }

    /**
     * Accion que es un atajo para que en un recibo utilizado para un vale a rendir se pueda reintegrar
     * la totalidad del vale (esto es notificar que se utilizo completamente el presupuesto en el vale
     * a rendir sin tener que llenar todos los campos).
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionReintegrarTotalidadVale($id)
    {
        $recibo = $this->checkReciboPermisos($id, 'ingresar');
        $entidad = $recibo->getEntidad();
        if ($recibo->estado != 3)
            $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
        $recibo->estado = 4;
        $recibo->total_gastos = $recibo->totalMontoItems;
        $recibo->saldo_favor_deudor = 0;
        $recibo->saldo_reintegrar_caja = $recibo->total_gastos;
        $recibo->total_igual = $recibo->total_gastos;
        $recibo->presupuestos = $recibo->presupuestosIds;
        if ($recibo->save(false)) {
            $recibo->eliminarItemsRecibo();
            $this->onSolicitarRecibo(new CEvent($this, array('tipo' => 'solicitar_recibo', 'recibo' => $recibo,
                'entidad' => $entidad, 'destinatario' => $entidad->getEncargado()->id, 'tipoRecibo' => $recibo->tipoRecibo)));
            user()->setFlash('success', "El formulario de {$recibo->tipoRecibo} ha sido enviado correctamente para
                    revisi&oacute;n.");
        } else
            user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntalo nuevamente.');
        $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
    }

    /**
     * Accion que permite eliminar un item de un presupuesto (solo por el usuario que solicito el presupuesto)
     * solamente cuando el presupuesto no ha sido utilizado en una operacion financiera ni sido aprobado.
     *
     * @param int $id ID del presupuesto.
     * @param string $ref referenccia de hacia que accion debo volver al concluir esta accion.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEliminarItemPresupuesto($id, $ref = 'editar')
    {
        $item = ItemPresupuesto::model()->findByPk($id);
        if ($item == null || $item->presupuesto->id_usuario_que_envia != user()->id || $item->presupuesto->estado == 1)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $presupuesto = $item->id_presupuesto;
        $entidad = $item->presupuesto->getEntidad();
        if ($item->delete())
            user()->setFlash('success', 'El item del presupuesto ha sido eliminado con &eacute;xito.');
        else
            user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntalo nuevamente.');
        if ($ref == 'editar')
            $this->redirect(array('/finanzas/operaciones/editarPresupuesto', 'id' => $presupuesto));
        else
            $this->redirect(array('/finanzas/operaciones/enviarPresupuesto', 'id' => $entidad->getId(),
                'tipo' => $entidad->getTipoEntidadNumerico()));
    }

    /**
     * Accion que permite eliminar un item de un recibo solamente si es que el recibo no ha sido utilizado en
     * una operacion ni ha sido aprobado. Solamente el usuario que lo solicita puede eliminar items.
     *
     * @param int $id ID del recibo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEliminarItemRecibo($id)
    {
        $item = ItemRecibo::model()->findByPk($id);
        if ($item == null || $item->recibo->id_usuario_que_solicita != user()->id || $item->recibo->estado == 1)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $recibo = $item->id_recibo;
        $this->checkReciboPermisos($recibo);
        if ($item->delete())
            user()->setFlash('success', 'El item del recibo ha sido eliminado con &eacute;xito.');
        else
            user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntalo nuevamente.');
        $this->redirect(array('/finanzas/operaciones/llenarRecibo', 'id' => $recibo));
    }

    /**
     * Despliega los vales a rendir de una entidad en particular.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     */
    public function actionVerValesRendir($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('ver_vales_historicos', user()->id);
        $vales = Recibo::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=1 and tipo_recibo=2 order by
            fecha_vale DESC',
            array(':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico()));

        $this->render('ver_vales_rendir', array(
            'vales' => $vales,
            'entidad' => $entidad
        ));
    }

    /**
     * Permite a un usuario que solicito un recibo el subir un comprobante de reintegracion de gastos.
     * Dicho comprobante posteriormente debera ser aprobado desde la direccion de PIEA para poder hacer
     * valido el recibo.
     *
     * @param int $id ID del recibo
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionSubirComprobanteReintegracion($id)
    {
        $recibo = $this->checkReciboPermisos($id, 'ingresar');
        $boleta = new XUploadForm;

        if (isset($_POST['enviar'])) {
            $recibo->cambiarEstado(6);
            $recibo->presupuestos = $recibo->presupuestosIds;

            $transaction = app()->db->beginTransaction();
            try {
                $recibo->save(false);
                $this->addFiles($recibo->id, 'reintegracion', 'recibos');
                $this->onNotificarEntregaRetiro(new CEvent($this, array('tipo' => 'notificar_entrega_reintegracion', 'recibo' => $recibo
                , 'entidad' => $recibo->getEntidad())));
                $transaction->commit();
                user()->setFlash('success', "El documento de reintegraci&oacute;n se ha enviado correctamente.
                    El administrador deber&aacute; notificar el recibo correcto.");
                $this->redirect(array('/finanzas/operaciones/verRecibo', 'id' => $recibo->id));
            } catch (Exception $e) {
                $transaction->rollback();
                user()->setFlash('error', 'Ha ocurrido un error, recuerda que debes agregar un archivo.');
            }
        }

        $this->render('subir_comprobante_reintegracion', array(
            'recibo' => $recibo,
            'entidad' => $recibo->getEntidad(),
            'boleta' => $boleta
        ));
    }

    /**
     * Permite al administrador de PIEA el notificar que el comprobante de reintegracion fue recibido
     * correctamente.
     *
     * @param int $id ID del recibo
     *
     * @return  User
     * @throws  \CHttpException
     */
    public function actionNotificarReciboReintegracion($id)
    {
        if (user()->checkAccess('administrador')) {
            $recibo = Recibo::load($id);
            $recibo->cambiarEstado(1);
            $recibo->presupuestos = $recibo->presupuestosIds;
            if ($recibo->save(false)) {
                $this->onNotificarEntregaRetiro(new CEvent($this, array('tipo' => 'notificar_recibo_reintegracion', 'recibo' => $recibo
                , 'entidad' => $recibo->getEntidad())));
                user()->setFlash('success', "Se ha notificado correctamente el recibo del documento de reintegraci&oacute;n");
                $this->redirect(array('/finanzas/operaciones/verRecibo', 'id' => $recibo->id));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntalo nuevamente.');
                $this->redirect(array('/finanzas/operaciones/verRecibo', 'id' => $recibo->id));
            }
        }
        $this->redirect(array('/'));
    }

    /**
     * Accion que permite crear un pago a proveedores o tambien un formulario para envio a la unidad
     * de abastecimiento de la universidad. ¿¿Por ahora solo el administrador puede crear un formulario
     * para la unidad de abastecimiento ??.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad
     * @param int $tf Tipo del recibo a crear
     * @param int $u ID del formulario de unidad de abastecimiento (si es que aplica)
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCrearPagoProveedores($id, $tipo, $tf, $u = null)
    {
        $entidad = new Entidad($id, $tipo);
        $model = new PagoProveedores;
        $model->setTipoRecibo($tf);
        if ($model->tipo_recibo == 1) {
            $entidad->checkPermisos('ingresar_pago_proveedor', user()->id);
            $model->setScenario('pagoProveedores');
        } else if ($model->tipo_recibo == 2) {
            if (!user()->checkAccess('administrador'))
                throw new CHttpException(403, "No tienes los permisos suficientes para ingresar a esta p&aacute;gina");
            $model->setScenario('unidadAbastecimiento');
        }
        $model->setEntidad($entidad);
        $model->estado = 0;
        $factura = new XUploadForm;
        $presupuestos = Presupuesto::getPresupuestosEntidadValidos($entidad);

        if ($model->tipo_recibo == 2)
            $model = PagoProveedores::load($u);
        if (isset($_POST['PagoProveedores'])) {
            $model->attributes = $_POST['PagoProveedores'];
            $model->presupuestos = $model->presupuestosIds;

            $transaction = app()->db->beginTransaction();
            if ($model->validate()) {
                try {
                    if ($model->tipo_recibo == 1) {
                        if ($model->monto_factura != $model->totalMontoItemsPresupuestos) {
                            throw new Exception;
                        }
                    } else {
                        $model->estado = 1;
                    }
                    $model->save(false);
                    $this->addFiles($model->primaryKey, null, 'proveedores');
                    if ($model->tipo_recibo == 1) {
                        $pre = new Presupuesto;
                        $pre->marcarPresupuestosComoUtilizados($model->presupuestosIds);
                        //se envia la notificacion al administrador
                        $this->onSolicitarRecibo(new CEvent($this, array('tipo' => 'solicitar_pago_proveedor', 'pago' => $model,
                            'entidad' => $entidad, 'tipoRecibo' => $model->tipoRecibo)));
                        user()->setFlash('warning', 'La factura ha sido correctamente enviada para revisi&oacute;n por el
                        administrador de PIEA.');
                    } else if ($model->tipo_recibo == 2) {
                        //se envia la notificacion al usuario que solicito el formulario, de que este ha sido aprobado
                        $this->onSolicitarRecibo(new CEvent($this, array('tipo' => 'unidad_abastecimiento_aprobada', 'pago' => $model,
                            'entidad' => $entidad, 'destinatario' => $model->id_usuario_que_solicita)));
                        user()->setFlash('success', 'Se ha subido correctamente la factura.');
                    }
                    $transaction->commit();
                    $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
                } catch (Exception $e) {
                    $transaction->rollback();
                    user()->setState('images', null);
                    $extra = ($model->tipo_recibo == 1) ? " y el monto de la factura debe ser igual que la suma de los presupuestos" : "";
                    user()->setFlash('error', "Ha ocurrido un error, por favor verifica que todos los campos han sido
                                completados correctamente (subir una factura es obligatorio{$extra}).");
                }
            }
        }

        $this->render('crear_pago_proveedor', array(
            'presupuestos' => $presupuestos,
            'model' => $model,
            'entidad' => $entidad,
            'factura' => $factura
        ));
    }

    /**
     * Permite ver ya sea un formulario de pago a proveedores o uno de unidad de abastecimiento
     * en una entidad en particular. Solo los usuarios con los permisos correspondientes pueden
     * visualizar dicha informacion. Los permisos son: ver_pago_proveedor y ver_unidad_abastecimiento.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad
     * @param int $tf Tipo del formulario
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerPagoProveedores($id, $tipo, $tf)
    {
        $entidad = new Entidad($id, $tipo);
        $model = new PagoProveedores;
        $model->setTipoRecibo($tf);
        if ($model->tipo_recibo == 1)
            $entidad->checkPermisos('ver_pago_proveedor', user()->id);
        else if ($model->tipo_recibo == 2)
            $entidad->checkPermisos('ver_unidad_abastecimiento', user()->id);
        //se cargan los pago a proveedores de la entidad respectiva
        $pagos_aprobados = PagoProveedores::model()->findAll('id_entidad=:ie and tipo_entidad=:te and tipo_recibo=:tr and
            estado=1', array(':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico(), ':tr' => $tf));

        $pagos_pendientes = PagoProveedores::model()->findAll('id_entidad=:ie and tipo_entidad=:te and tipo_recibo=:tr and
            estado=0', array(':ie' => $entidad->getId(), ':te' => $entidad->getTipoEntidadNumerico(), ':tr' => $tf));

        $tipo_vale = ($tf == 1) ? 'Pagos a proveedores' : 'Unidades de abastecimientos';
        $this->render('ver_pagos_proveedores', array(
            'pagos_aprobados' => $pagos_aprobados,
            'pagos_pendientes' => $pagos_pendientes,
            'entidad' => $entidad,
            'tipo_vale' => $tipo_vale
        ));
    }

    /**
     * Accion para visualizar un formulario de pago a proveedor mediante su ID. Si el usuario no
     * es quien solicito el formulario entonces se chequean sus permisos en contra de la entidad
     * a la que pertenece el formulario.
     *
     * @param int $id ID del formulario.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerPagoProveedor($id)
    {
        $pago = PagoProveedores::load($id);
        $entidad = $pago->getEntidad();
        if ($pago->id_usuario_que_solicita != user()->id) {
            if ($pago->tipo_recibo == 1)
                $entidad->checkPermisos('ver_recuperacion', user()->id);
            else if ($pago->tipo_recibo == 2)
                $entidad->checkPermisos('ver_vale', user()->id);
        }

        $mostrar_edit = ($pago->id_usuario_que_solicita == user()->id) ? true : false;

        $this->render('ver_pago_proveedor', array(
            'pago' => $pago,
            'entidad' => $entidad,
            'mostrar_edit' => $mostrar_edit
        ));
    }

    /**
     * Accion que permite al administrador del sistema el aprobar un formulario de pago a proveedores. Una
     * vez aprobado entonces se emite una notificacion informando al usuario que lo solicito de que su
     * solicitud ha sido aprobada.
     *
     * @param int $id ID del formulario.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAprobarPagoProveedor($id)
    {
        if (user()->checkAccess('administrador')) {
            $pago = PagoProveedores::load($id);
            $entidad = $pago->getEntidad();
            $pago->estado = 1;
            $pago->presupuestos = $pago->presupuestosIds;
            if ($pago->save(false)) {
                Mensaje::contestarMensajePresupuesto($pago->id, 'pago_proveedor');
                user()->setFlash('success', "El formulario de {$pago->tipoRecibo} ha sido aceptado correctamente.");
                $this->onAceptarRecibo(new CEvent($this, array('tipo' => 'aceptar_pago_proveedor', 'pago' => $pago,
                    'entidad' => $entidad)));
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
                $this->redirect(array('/finanzas/operaciones/verPagoProveedor', 'id' => $pago->id));
            }
        } else
            throw new CHttpException(500, 'No tienes los permisos suficientes para ver esta p&aacute;gina');
    }

    /**
     * Accion que permite al administrador del sistema el rechazar un formulario de pago a proveedores. Una
     * vez aprobado entonces se emite una notificacion informando al usuario que lo solicito de que su
     * solicitud ha sido rechaza.
     *
     * @param int $id ID del formulario.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo Este metodo se puede unir con el anterior
     */
    public function actionRechazarPagoProveedor($id)
    {
        if (user()->checkAccess('administrador')) {
            $pago = PagoProveedores::load($id);
            $entidad = $pago->getEntidad();
            $pago->estado = -1;
            if (isset($_POST['comentario_rechazo']))
                $pago->comentario_revisor = app()->input->post('comentario_rechazo');
            $pago->presupuestos = $pago->presupuestosIds;
            if ($pago->save(false)) {
                Mensaje::contestarMensajePresupuesto($pago->id, 'pago_proveedor');
                user()->setFlash('success', "El formulario de {$pago->tipoRecibo} ha sido rechazado correctamente.");
                $this->onRechazarRecibo(new CEvent($this, array('tipo' => 'rechazar_pago_proveedor',
                    'pago' => $pago, 'entidad' => $entidad)));
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
                $this->redirect(array('/finanzas/operaciones/verPagoPRoveedor', 'id' => $pago->id));
            }
        } else
            throw new CHttpException(500, 'No tienes los permisos suficientes para ver esta p&aacute;gina');
    }

    /**
     * Accion que permite al usuario que solicito un formulario de pago a proveedores el editar la informacion
     * basica de este. Esto solo se permite si es que el formulario ya existe y ademas una vez editado el
     * formulario debe nuevamente pasar por el proceso de aprobacion en la administracion de PIEA.
     *
     * @param int $id ID del formulario.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEditarPagoProveedor($id)
    {
        $pago = PagoProveedores::load($id);
        if ($pago == null || $pago->id_usuario_que_solicita != user()->id || $pago->estado != -1)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $entidad = $pago->getEntidad();

        $factura = new XUploadForm;
        $presupuestos = Presupuesto::getPresupuestosEntidadValidos($entidad);
        $presupuestos_utilizados = Presupuesto::getPresupuestosEntidadUtilizados($entidad);

        if (isset($_POST['PagoProveedor'])) {
            $pago->attributes = $_POST['PagoProveedor'];
            $pago->presupuestos = $pago->presupuestosIds;
            if ($pago->save()) {
                //se envia la notificacion al administrador
                $this->onSolicitarRecibo(new CEvent($this, array('tipo' => 'solicitar_pago_proveedor', 'pago' => $pago,
                    'entidad' => $entidad, 'tipoRecibo' => $pago->tipoRecibo)));
                user()->setFlash('warning', "El formulario de {$pago->tipoRecibo} ha sido correctamente enviada para
                    revisi&oacute;n por el administrador de PIEA.");
                $this->redirect(array('/finanzas/operaciones/verPagoProveedor', 'id' => $pago->id));
            }
        }

        $this->render('editar_pago_proveedor', array(
            'presupuestos' => $presupuestos,
            'presupuestos_utilizados' => $presupuestos_utilizados,
            'model' => $pago,
            'entidad' => $entidad,
            'factura' => $factura
        ));
    }

    /**
     * Accion para que el integrante de una entidad con los permisos correspondientes pueda crear
     * un presupuesto en dicha entidad. Un presupuesto puede estar asociado a mas de una cuenta y
     * puede contener multiples items. Esta funcion es similar a la accion crearPresupuesto pero
     * la diferencia es que esta esta explicitamente disenada para crear un presupuesto para la unidad
     * de abastecimiento de la universidad.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad
     */
    public function actionCrearPresupuestoUnidadAbastecimiento($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('ingresar_unidad_abastecimiento', user()->id);
        $presupuesto = new Presupuesto;
        if (isset($_POST['Presupuesto'])) {
            $presupuesto->attributes = $_POST['Presupuesto'];
            $presupuesto->setEntidad($entidad);
            if ($presupuesto->validate()) {
                $model = new PagoProveedores;
                $model->setEntidad($entidad);
                $model->tipo_recibo = 2;
                $model->monto_factura = $model->numero_factura = 0;
                $model->estado = -2; //estado representa, unidad de abastecimiento aun no creada
                if (isset($_POST['gastos_envio']) && $_POST['gastos_envio'] == "1")
                    $model->gastos_envio = app()->input->post('gastos_envio');
                $model->save(false);
                $presupuesto->unidad_de_abastecimiento = $model->primaryKey;
                $presupuesto->save(false);
                //se guardan los presupuestos del pago
                $model->presupuestos = array($presupuesto->primaryKey);
                $model->save(false);
                user()->setFlash('warning', 'Para continuar, debes ingresar los items a tu presupuesto.');
                $this->redirect(array('/finanzas/operaciones/enviarPresupuestoUnidadAbastecimiento', 'id' => $presupuesto->primaryKey));
            }
        }
        $this->render('crear_presupuesto_pago_proveedores', array(
            'presupuesto' => $presupuesto,
            'entidad' => $entidad
        ));
    }

    /**
     * Accion que permite enviar un presupuesto para revision. Los presupuestos son revisados por
     * los superiores de la entidad donde son creados y tambien por el superior de la cuenta
     * utilizada para crearlo. En esta accion tambien se permiten agregar items a los presupuestos,
     * por lo tanto cumple una doble funcion. Esta funcion es similar a la accion enviarPresupuesto pero
     * la diferencia es que esta esta explicitamente disenada para solicitar presupuesto para la unidad
     * de abastecimiento de la universidad.
     *
     * @param int $id ID del presupuesto.
     *
     * @throws  \CException
     */
    public function actionEnviarPresupuestoUnidadAbastecimiento($id)
    {
        $presupuesto = Presupuesto::managePresupuesto($id);
        $boletas = new XUploadForm;
        $item = new ItemPresupuesto;
        $entidad = $presupuesto->getEntidad();

        if (isset($_POST['ItemPresupuesto'])) {
            $item->attributes = $_POST['ItemPresupuesto'];
            $item->id_presupuesto = $presupuesto->id;
            if ($item->validate()) {
                $transaction = app()->db->beginTransaction();
                try {
                    $item->save(false);
                    $this->addFiles($presupuesto->id, $item->primaryKey);
                    $transaction->commit();
                    user()->setFlash('success', 'El item se ha agregado correctamente al presupuesto.');
                    $this->redirect(array('/finanzas/operaciones/enviarPresupuestoUnidadAbastecimiento', 'id' => $presupuesto->id,
                        'tipo' => $entidad->getTipoEntidadNumerico()));
                } catch (CException $e) {
                    $transaction->rollback();
                    user()->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
        } else if (isset($_POST['enviarPresupuesto'])) {
            if (count($presupuesto->items) == 0) {
                user()->setFlash('error', 'Debes ingresar al menos un item antes de enviar el presupuesto.');
            } else {
                $presupuesto->estado = 0;
                if ($presupuesto->save(false)) {
                    $this->onSolicitarPresupuesto(new CEvent($this, array('tipo' => 'solicitar_presupuesto',
                        'presupuesto' => $presupuesto, 'entidad' => $entidad, 'destinatario' => $entidad->getEncargado()->id,
                        'encargado' => 'entidad')));
                    user()->setFlash('success', 'El presupuesto ha sido enviado correctamente para revisi&oacute;n.');
                    $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
                } else {
                    user()->setFlash('error', 'Ha ocurrido un error al enviar el presupuesto, por favor int&eacute;ntelo
                        nuevamente.');
                }
            }
        } else if (isset($_POST['editarPresupuesto'])) {
            //se elimina la cotizacion de gastos de envio, se deja en 0 el flag de que se ha generado el memo de la
            //cotizacion
            $presupuesto->cotizacion_solicitada = 0;
            $presupuesto->cotizacion_gastos_envio = null;
            $presupuesto->save(false);
            $this->refresh();
        }

        $this->render('enviar_presupuesto_unidad_abastecimiento', array(
            'entidad' => $entidad,
            'boletas' => $boletas,
            'item' => $item,
            'presupuesto' => $presupuesto,
            'unidad' => $presupuesto->unidadDeAbastecimiento
        ));
    }

    /**
     * Genera el PDF con el memo que sirve como solicitud de cotizacion para envio a la unidad de
     * abastecimiento de la universidad. En dicha unidad cotizaran los gastos de envio de la compra
     * de los items de un presupuesto.
     *
     * @param int $id ID del presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionGenerarMemoCotizacion($id)
    {
        $pago = PagoProveedores::load($id);
        $entidad = $pago->getEntidad();
        $entidad->checkPermisos('ingresar_unidad_abastecimiento', user()->id);
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $presupuesto = $pago->presupuestos[0];
        $presupuesto->cotizacion_solicitada = 1;
        $presupuesto->save(false);
        $html2pdf->WriteHTML($this->renderPartial('_memo_cotizacion',
            array('entidad' => $entidad, 'unidad' => $pago, 'presupuesto' => $presupuesto), true));
        $html2pdf->Output();
    }

    /**
     * Permite a la unidad de finanzas el subir una cotizacion de los gastos de envios para una solicitud
     * de compra a la unidad de abastecimiento.
     *
     * @param int $id ID del presupuesto
     * @param string $t Hash de la fecha de solicitud que sirve para evitar que cualquier persona pueda subir
     *                  una cotizacion (es medianamente seguro)
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionSubirCotizacion($id, $t)
    {
        $presupuesto = Presupuesto::managePresupuesto($id, false);
        if ($t != md5($presupuesto->fecha_solicitud) || $presupuesto->cotizacion_gastos_envio != null)
            throw new CHttpException(500, 'Acci&oacute;n no permitida: no tienes los permisos suficientes para ingresar
            a esta p&aacute;gina');

        $show_final = false;
        $error = null;
        if (isset($_POST['gastos_envio'])) {
            $gastos = app()->input->post('gastos_envio');
            if (!is_numeric($gastos)) {
                $error = "La cotizaci&oacute;n debe ser num&eacute;rico.";
            } else {
                if ($gastos < 0) {
                    $error = "La cotizaci&oacute;n debe ser un n&uacute;mero v&aacute;lido.";
                } else {
                    $presupuesto->cotizacion_gastos_envio = $gastos;
                    $show_final = true;
                    $presupuesto->save(false);
                    $this->onAceptarPresupuesto(new CEvent($this, array('tipo' => 'cotizacion_subida',
                        'presupuesto' => $presupuesto, 'entidad' => $presupuesto->getEntidad())));
                }
            }
        }

        $this->render('subir_cotizacion', array(
            'entidad' => $presupuesto->getEntidad(),
            'unidad' => $presupuesto,
            'showFinal' => $show_final,
            'error' => $error
        ));
    }

    /**
     * Sube un archivo al servidor. Ver documentacion de la extension XUpload.
     *
     * @param string $folder carpeta hacia donde subir el archivo
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionUpload($folder = 'presupuestos')
    {
        //Here we define the paths where the files will be stored temporarily
        $path = app()->getBasePath() . "/../../piea-files/" . $folder . "/tmp/";
        //die($path);

        //This is for IE which doens't handle 'Content-type: application/json' correctly
        header('Vary: Accept');
        if (isset($_SERVER['HTTP_ACCEPT'])
            && (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)
        ) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }

        //Here we check if we are deleting and uploaded file
        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');
            //We check that the file was successfully uploaded
            if ($model->file !== null) {
                //Grab some data
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                $model->name = $model->file->getName();
                //(optional) Generate a random name for our file
                $filename = md5(microtime() . $model->name);
                $filename .= "." . $model->file->getExtensionName();
                if ($model->validate()) {
                    //Move our file to our temporary dir
                    $model->file->saveAs($path . $filename);
                    chmod($path . $filename, 0777);
                    //here you can also generate the image versions you need
                    //using something like PHPThumb


                    //Now we need to save this path to the user's session
                    if (Yii::app()->user->hasState('images')) {
                        $userImages = Yii::app()->user->getState('images');
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $path . $filename,
                        //the same file or a thumb version that you generated
                        "thumb" => $path . $filename,
                        "filename" => $filename,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('images', $userImages);

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode(array(array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "delete_url" => $this->createUrl("upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        )),
                        "delete_type" => "POST"
                    )));
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),
                        )));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()),
                        CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    /**
     * Accion para enviar un archivo almacenado en el servidor (presupuestos y
     * recibos).
     *
     * @param int $p ID del presupuesto o recibo
     * @param int $i ID del item del recibo
     * @param int $f archivo de presupuesto
     * @param string $folder carpeta donde revisar el archivo a enviar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionSendFile($p, $i, $f, $folder = "presupuestos")
    {
        if ($i == null)
            $path = app()->getBasePath() . "/../../piea-files/" . $folder . "/" . $p . "/" . $f;
        else
            $path = app()->getBasePath() . "/../../piea-files/" . $folder . "/" . $p . "/" . $i . "/" . $f;
        $myfile = Yii::app()->file->set($path);
        if ($folder == 'recibos') {
            $recibo = Recibo::load($p);
            $itemRecibo = ItemRecibo::load($i);
            $fakeName = "boleta_{$itemRecibo->numero_boleta}_recibo{$p}_item{$i}";
            $myfile->download($fakeName);
        } else if ($folder == 'proveedores') {
            $pago = PagoProveedores::load($p);
            $fakeName = "factura_{$pago->tipoRecibo}_{$p}";
            $myfile->download($fakeName);
        } else
            $myfile->download();
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    /**
     * Funcion interna del controlador para agregar archivos a un recibo.
     *
     * @param int $presupuesto_id ID del presupueso (o recibo) donde se agrega un archivo
     * @param int $item_id item (en caso de recibo) donde se agrega un archivo
     * @param string $folder carpeta donde se almacenan los archivos
     *
     * @return  bool true si se agrego con exito, false en caso contrario
     * @throws  \CException
     */
    private function addFiles($presupuesto_id, $item_id = null, $folder = "presupuestos")
    {
        //If we have pending images
        if (user()->hasState('images')) {
            $userImages = user()->getState('images');
            //Resolve the final path for our images
            if ($item_id == null)
                $path = app()->getBasePath() . "/../../piea-files/" . $folder . "/" . $presupuesto_id . "/";
            else
                $path = app()->getBasePath() . "/../../piea-files/" . $folder . "/" . $presupuesto_id . "/" . $item_id . "/";
            //Create the folder and give permissions if it doesnt exists
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }

            //Now lets create the corresponding models and move the files
            foreach ($userImages as $image) {
                if (is_file($image["path"])) {
                    if (rename($image["path"], $path . $image["filename"])) {
                        Yii::log($image["path"] . " agregado correctamente", CLogger::LEVEL_INFO);
                    }
                } else {
                    //You can also throw an execption here to rollback the transaction
                    Yii::log($image["path"] . " is not a file", CLogger::LEVEL_WARNING);
                    user()->setState('images', null);
                    throw new CException("Uno o m&aacute;s archivos, no pudieron ser subidos. Por favor, int&eacute;ntelo
                    nuevamente");
                }
            }
            //Clear the user's session
            user()->setState('images', null);
            return true;
        } else
            throw new CException("Se ha intentando agregar registro sin archivo adjunto.");
    }

    /**
     * Verifica si es que el usuario logeado tiene permisos sobre un recibo. Si los tiene
     * entonces se retorna el recibo, sino entonces se lanza una exception. Ademas se chequea
     * si es que el recibo esta enviado o no.
     *
     * @param int $id ID del recibo
     * @param string $permiso permiso a validar, por defecto es 'ver'
     *
     * @return  Recibo
     * @throws  \CHttpException
     */
    private function checkReciboPermisos($id, $permiso = 'ver')
    {
        $recibo = Recibo::model()->findByPk($id);
        if ($recibo == null || $recibo->estado == -1)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $entidad = $recibo->getEntidad();
        if ($recibo->id_usuario_que_solicita != user()->id) {
            if ($recibo->tipo_recibo == 1)
                $entidad->checkPermisos($permiso . '_recuperacion', user()->id);
            else if ($recibo->tipo_recibo == 2)
                $entidad->checkPermisos($permiso . '_vale', user()->id);
        }
        return $recibo;
    }

    /**
     * Verifica si es que el usuario logeado tiene permisos sobre un recibo. Si los tiene
     * entonces se retorna el recibo, sino entonces se lanza una exception.
     *
     * @param int $id ID del recibo
     * @param string $permiso permiso a validar, por defecto es 'ver'
     *
     * @return  Recibo
     * @throws  \CHttpException
     */
    private function checkLlenarReciboPermisos($id, $permiso = 'ver')
    {
        $recibo = Recibo::model()->findByPk($id);
        if ($recibo == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $entidad = $recibo->getEntidad();
        if ($recibo->id_usuario_que_solicita != user()->id) {
            if ($recibo->tipo_recibo == 1)
                $entidad->checkPermisos($permiso . '_recuperacion', user()->id);
            else if ($recibo->tipo_recibo == 2)
                $entidad->checkPermisos($permiso . '_vale', user()->id);
        }
        return $recibo;
    }

    /**
     * Filtros del controlador.
     *
     * @return  array filtros del controlador.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controladr.
     *
     * @return  array reglas de acceso del controlador.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('subirCotizacion'),
                'users' => array('*'),
            ),
            array('allow',
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onSolicitarPresupuesto($event)
    {
        $this->raiseEvent('onSolicitarPresupuesto', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onSolicitarRecibo($event)
    {
        $this->raiseEvent('onSolicitarRecibo', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onAceptarPresupuesto($event)
    {
        $this->raiseEvent('onAceptarPresupuesto', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onRechazarPresupuesto($event)
    {
        $this->raiseEvent('onRechazarPresupuesto', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onAceptarRecibo($event)
    {
        $this->raiseEvent('onAceptarRecibo', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onRechazarRecibo($event)
    {
        $this->raiseEvent('onRechazarRecibo', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onNotificarEntregaRetiro($event)
    {
        $this->raiseEvent('onNotificarEntregaRetiro', $event);
    }

    /**
     * Evento...
     *
     * @param CEvent $event evento a lanzas
     */
    public function onNotificarReciboRetiro($event)
    {
        $this->raiseEvent('onNotificarReciboRetiro', $event);
    }

}
