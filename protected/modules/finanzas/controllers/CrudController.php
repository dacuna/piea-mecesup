<?php
Yii::import('application.modules.usuarios.components.Entidad');
/**
 * Controlador que implementa la funcionalidad basica del modulo. Se permite crear cuentas y
 * realizar transferencias entre cuentas.
 *
 * Se debe distinguir entre las transferencias que realiza el administrador de PIEA con las
 * transferencias que realizan los usuarios. Para las transferencias de los usuarios entonces
 * se debe ver @link{TransferenciaController}. En este controlador solo se presentan las
 * acciones para realizar transferencias por el administrador. Los conflictos de nombre se
 * deben a que la funcionalidad de transferencias estaba construida tal como se presenta en
 * este controlador (para todos los usuarios) pero posteriormente desde PIEA se solicito que
 * se crearan distintos tipos de transferencias por lo que se tuvo que adaptar el presente
 * codigo y crear el codigo de la clase @link{TransferenciaController}.
 *
 * @package   modules.finanzas.controllers
 * @version   1.0
 * @since     2012-11-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class CrudController extends Controller
{
    /**
     * Funcion llamada cada vez que se inicializa el controlador. Se carga el handler para el
     * evento de contestar solicitud de transferencia.
     *
     * @return void
     */
    public function init()
    {
        $this->onContestarSolicitudTransferencia = array(app()->mailman, 'notificacionHandler');
    }

    /**
     * Despliega un listado con todas las cuentas existentes en el sistema. Solo el administrador
     * puede visualizar esta accion.
     *
     * @return void
     */
    public function actionIndex()
    {
        //se muestran todas las cuentas que existen en el sistema (solo al admin)
        $dataProvider = new CActiveDataProvider('Cuenta', array(
            'criteria' => array(
                //'condition'=>'valida=1',
                'order' => 'fecha_creacion DESC'
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));

        $this->render('index', array(
            'dataProvider' => $dataProvider
        ));
    }

    /**
     * Muestra las cuentas que estan asociadas a una entidad en particular.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad (0=iniciativa, 1=proyecto)
     *
     * @return void
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE REALMENTE EJECUTARLA!!!
     */
    public function actionVerCuentasEntidad($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $cuentas = Cuenta::getCuentasConAccesoYSaldo($id, $tipo);

        $this->render('ver_cuentas_entidad', array(
            'entidad' => $entidad,
            'cuentas' => $cuentas
        ));
    }

    /**
     * Permite al administrador crear una cuenta en el sistema.
     *
     * @return void
     */
    public function actionCrear()
    {
        $model = new Cuenta;

        if (isset($_POST['Cuenta'])) {
            $model->attributes = $_POST['Cuenta'];
            if ($model->save()) {
                user()->setFlash("success", "Se ha creado la cuenta con &eacute;xito.");
                $this->redirect(array('/finanzas/crud/index'));
            }
        }

        $this->render('crear', array(
            'model' => $model
        ));
    }

    /**
     * Permite al administrador realizar una entrega de fondos en el sistema. Una entrega corresponde
     * a "entregar" una parte de una cuenta a otra entidad. Por ejemplo si poseo 1000 en la cuenta 1
     * puedo entregar 500 a la entidad 2, asi la entidad 2 ahora tendra asociada la cuenta 1 con un
     * saldo de 500.
     *
     * @return void
     */
    public function actionEntregaFondos()
    {
        $model = new Entrega;

        if (isset($_POST['Entrega'])) {
            $model->attributes = $_POST['Entrega'];
            $model->id_entidad_origen = $model->cuenta->id_entidad;
            $model->tipo_entidad_origen = $model->cuenta->tipo_entidad;
            if ($model->save()) {
                //se debe descontar el monto desde la cuenta de origen
                $model->cuenta->saldo -= $model->monto;
                $model->cuenta->save(false);
                user()->setFlash('success', 'Se ha realizado correctamente la entrega de fondos');
                $this->redirect(array('/finanzas/historico/entregas', 'id' => $model->id));
            }
        }

        $this->render('entrega_fondos', array(
            'model' => $model
        ));
    }

    /**
     * Permite al administrador realizar una transferencia de fondos desde una cuenta asociada a una
     * entidad hacia otra cuenta en otra entidad.
     *
     * @return void
     * @todo hacer la transferencia como una transaccion sql
     */
    public function actionTransferenciaFondos()
    {
        $model = new Transferencia;

        if (isset($_POST['Transferencia'])) {
            $model->attributes = $_POST['Transferencia'];
            $model->id_entidad_origen = $model->cuenta_origen->id_entidad;
            $model->tipo_entidad_origen = $model->cuenta_origen->tipo_entidad;
            $model->id_entidad_destino = $model->cuenta_destino->id_entidad;
            $model->tipo_entidad_destino = $model->cuenta_destino->tipo_entidad;
            $model->tipo_cuenta_origen = 'cuenta';
            $model->tipo_cuenta_destino = 'cuenta';
            if ($model->save()) {
                //se le resta el monto a la cuenta de origen y se suma al de la de destino
                $model->cuenta_origen->saldo -= $model->monto;
                $model->cuenta_destino->saldo += $model->monto;
                $model->cuenta_origen->save(false);
                $model->cuenta_destino->save(false);
                user()->setFlash('success', 'La transferencia se ha realizado correctamente');
                $this->redirect(array('/finanzas/historico/transferencias', 'id' => $model->id));
            }
        }

        $this->render('transferencia_fondos', array(
            'model' => $model
        ));
    }

    /**
     * Permite al administrador ver los datos asociados a una cuenta en el sistema.
     *
     * @param int $id ID de la cuenta
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerCuenta($id)
    {
        $model = Cuenta::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException('404', 'La p&aacute;gina solicitada no existe.');

        if (app()->request->isAjaxRequest)
            $this->renderPartial('_ver_cuenta', array('model' => $model));
        else
            $this->render('ver_cuenta', array(
                'model' => $model
            ));
    }

    /**
     * Permite realizar una transferencia entre entidades. Esta funcion esta obsoleta.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE REALMENTE EJECUTARLA!!!
     */
    public function actionTransferencia($id, $tipo)
    {
        $model = new TransferenciaGeneral;
        $entidad = new Entidad($id, $tipo);
        $model->setEntidadOrigen($entidad);
        //obtengo las cuentas a las que la entidad seleccionada tiene acceso
        $cuentas_acceso = Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico());

        if (isset($_POST['TransferenciaGeneral'])) {
            $model->attributes = $_POST['TransferenciaGeneral'];
            if ($model->validate()) {
                //se realiza la transferencia
                if ($model->realizarTransferencia()) {
                    $extra = ($model->cuenta_destino == -1) ? 'El monto ya ha sido traspasado a su destino' : 'El administrador debe aprobar la transferencia realizada. Una vez aprobada se efectuar&aacute; el traspaso.';
                    user()->setFlash('success', 'La transferencia se ha realizado correctamente. ' . $extra);
                } else {
                    user()->setFlash('error', 'Al parecer ha ocurrido un error. Por favor, intent&eacute;lo nuevamente.');
                }
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }
        }

        $this->render('transferencia_general', array(
            'model' => $model,
            'entidad' => $entidad,
            'cuentas_acceso' => $cuentas_acceso
        ));
    }

    /**
     * Permite contestar aceptando como tambien rechazando una solicitud de transferencia en
     * el sistema. Las solicitudes de transferencia se envian por ejemplo cuando una entidad
     * realizar una transferencia interna, esta transferencia debe ser aprobada por un superior
     * (en este caso en particular el superior es el administrador) y con esta funcion se
     * puede realizar la aprobacion o rechazo de la notificacion de transferencia.
     *
     * @param int $e id del emisor del mensaje
     * @param int $d id del destinatario del mensaje
     * @param string $uid identificador unico del mensaje
     * @param int $s id de la solicitud
     * @param int $a accion (1=aceptar, 0=rechazar)
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionContestarSolicitudTransferencia($e, $d, $uid, $s, $a)
    {
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        $solicitud = SolicitudTransferencia::model()->findByPk($s);
        if ($solicitud == null || $mensaje->contestado == 1)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($a != 0 && $a != 1)
            throw new CHttpException(500, 'Solicitud incorrecta');
        //rechazar solicitud
        if ($a == 0) {
            $solicitud->estado = -1; //rechazada
            //se debe devolver el monto a la cuenta original
            if ($solicitud->tipo_partida == 0) { //tipo_partida=0 equivale a cuenta
                $cuenta = Cuenta::model()->findByPk($solicitud->partida_id_origen);
                $cuenta->saldo += $solicitud->monto;
                $cuenta->save(false);
                $solicitud->save(false);
            } else {
                $entrega = new EntregaUnica($solicitud->getEntidadOrigen(), $solicitud->partida_id_origen);
                $entrega->aumentarSaldo($solicitud->monto);
                $solicitud->save(false);
            }
            $mensaje->contestado = 1;
            $mensaje->accion_realizada = 'Rechazar transferencia';
            $mensaje->save(false);
            $this->onContestarSolicitudTransferencia(new CEvent($this, array('tipo' => 'contestar_solicitud_transferencia',
                'solicitud' => $solicitud, 'accion' => 'rechazada', 'destinatario' => $mensaje->emisor)));
            user()->setFlash('success', 'Se ha rechazado correctamente la solicitud.');
            $this->redirect(array('/buzon/inbox/index'));
        } else {
            //aceptar solicitud
            $solicitud->estado = 1;
            //se debe transferir el monto a la cuenta de destino
            if ($solicitud->tipo_cuenta_destino == 0) {
                $cuenta = Cuenta::model()->findByPk($solicitud->cuenta_id_destino);
                $cuenta->saldo += $solicitud->monto;
                $cuenta->save(false);
            } else {
                $entrega = new EntregaUnica($solicitud->getEntidadDestino(), $solicitud->cuenta_id_destino);
                $entrega->aumentarSaldo($solicitud->monto);
            }
            $transferencia = new Transferencia;
            $transferencia->id_entidad_origen = $solicitud->entidad_id_origen;
            $transferencia->tipo_entidad_origen = $solicitud->tipo_entidad_origen;
            $transferencia->id_entidad_destino = $solicitud->entidad_id_origen;
            $transferencia->tipo_entidad_destino = $solicitud->tipo_entidad_origen;
            $transferencia->monto = $solicitud->monto;
            $transferencia->id_cuenta_origen = $solicitud->partida_id_origen;
            $transferencia->id_cuenta_destino = $solicitud->cuenta_id_destino;
            $transferencia->id_operador = $mensaje->emisor;
            if ($transferencia->save(false)) {
                //se notifica al usuario que solicito la transferenci            $transferencia->tipo_cuenta_origen = $solicitud->tipo_partida;a que esta fue efectuada correctamente
                $this->onContestarSolicitudTransferencia(new CEvent($this, array('tipo' => 'contestar_solicitud_transferencia', 'solicitud' => $solicitud, 'accion' => 'aceptada', 'destinatario' => $mensaje->emisor)));
                user()->setFlash('success', 'La transferencia se ha efectuado correctamente.');
                $mensaje->contestado = 1;
                $mensaje->accion_realizada = 'Aceptar transferencia';
                $mensaje->save(false);
                $solicitud->save(false);
            } else
                user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
            $this->redirect(array('/buzon/inbox/index'));
        }
    }

    /**
     * Carga las entidades del sistema como <options> para un selectBox.
     */
    public function actionCargarEntidades()
    {
        if (isset($_POST['Cuenta']))
            $id = app()->input->stripClean($_POST['Cuenta']['tipo_entidad']);
        else if (isset($_POST['Entrega']))
            $id = app()->input->stripClean($_POST['Entrega']['tipo_entidad']);
        else
            return null;
        if ($id == 0) {
            $data = Iniciativa::model()->findAll();
            $data = CHtml::listData($data, 'id', 'nombre_abreviado');
        } else if ($id == 1) {
            $data = Proyecto::model()->findAll();
            $data = CHtml::listData($data, 'id', 'nombre');
        } else
            echo CHtml::tag('option', array('value' => ' '), CHtml::encode(' '), true);

        foreach ($data as $value => $name) {
            echo CHtml::tag('option',
                array('value' => $value), CHtml::encode($name), true);
        }
    }

    /**
     * Carga las cuentas en las que una entidad tiene fondos. Las carga como options para un selectBox.
     *
     * @param int $id id de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE REALMENTE EJECUTARLA!!!
     */
    public function actionCargarCuentasAcceso($id, $tipo)
    {
        if (isset($_GET['id']) && isset($_GET['tipo'])) {
            $id = app()->input->stripClean($_GET['id']);
            $tipo = app()->input->stripClean($_GET['tipo']);

            $data = Cuenta::getCuentasConAcceso($id, $tipo);
            echo "<option value=\"-1\" data-tipo=\"entrega\">Entrega de fondos</option>";
            foreach ($data as $cuenta) {
                ?>
                <option value="<?php echo $cuenta[0]->id; ?>" data-tipo="<?php echo $cuenta[1]; ?>">
                    <?php echo ($cuenta[1] == 'cuenta') ? $cuenta[0]->identificador : $cuenta[0]->cuenta->identificador; ?>
                </option>
            <?php
            }
        }
    }

    /**
     * Filtros del controlador.
     *
     * @return  array filtros del controlador.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controladr.
     *
     * @return  array reglas de acceso del controlador.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'crear', 'entregaFondos', 'verCuenta', 'transferenciaFondos', 'contestarSolicitudTransferencia'),
                'roles' => array('administrador'),
            ),
            array('allow',
                'actions' => array('cargarEntidades', 'transferencia', 'cargarCuentasAcceso', 'verCuentasEntidad'),
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Evento que se genera al contestar una solicitud de transferencia.
     *
     * @param CEvent $event evento generado
     */
    public function onContestarSolicitudTransferencia($event)
    {
        $this->raiseEvent('onContestarSolicitudTransferencia', $event);
    }
}