<?php
Yii::import('application.modules.usuarios.components.Entidad');

/**
 * TransferenciaController: contiene las acciones destinadas a realizar transferencias entre entidades.
 *
 * Se entiende como transferencia para el usuario lo que para el sistema internamente se conoce como una entrega de
 * fondos. El procedimiento de transferencia corresponde a realizar una entrega desde una entidad A con la cuenta x
 * a una entidad B. Si la entidad B ya posee una entrega desde la cuenta x entonces a dicha entrega se le debe sumar
 * el valor de la nueva entrega (transferencia). En otro caso, solamente se realiza la entrega de la manera tradicional.
 * Como implementacion se ha decidido guardar cada entrega como un registro aparte. Para obtener el saldo completo
 * que una entidad tiene sobre una cuenta entonces se deben sumar todos los saldos que se tienen para esa entidad en
 * dicha cuenta.
 *
 * @version   0.1
 * @since     10/31/12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class TransferenciaController extends Controller{

    /**
     * Permite realizar una transferencia desde una entidad hacia otra. La entidad de origen debe seleccionar
     * una entidad de destino, ademas de una cuenta de origen. Con esta informacion se realiza la transferencia
     * a la entidad de destino (para fines del sistema dicha accion se denomina entrega de fondos). La accion
     * de transferir fondos debe ser solamente notificada al administrador de la plataforma.
     *
     * @param  int $id    ID de la entidad de origen
     * @param  int $tipo  Tipo de la entidad de origen. 0=iniciativa, 1=proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-10-2012
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE REALMENTE EJECUTARLA!!!
     */
    public function actionRealizarTransferencia($id,$tipo){
        $entidad = new Entidad($id, $tipo);
        $model = new TransferenciaGeneral;
        $model->setEntidadOrigen($entidad);
        //obtengo las cuentas a las que la entidad seleccionada tiene acceso
        $cuentas_acceso = Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico());

        if (isset($_POST['TransferenciaGeneral'])) {
            $model->attributes = $_POST['TransferenciaGeneral'];
            if ($model->validate()) {
                //se realiza la transferencia
                if ($model->realizarTransferencia())
                    user()->setFlash('success', 'La transferencia se ha realizado correctamente. El monto ya ha sido traspasado a su destino');
                else
                    user()->setFlash('error', 'Al parecer ha ocurrido un error. Por favor, intent&eacute;lo nuevamente.');
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }
        }

        $this->render('realizar_transferencia', array(
            'model' => $model,
            'entidad' => $entidad,
            'cuentas_acceso' => $cuentas_acceso
        ));
    }

    /**
     * Permite realizar una transferencia interna entre cuentas de una misma entidad. El operador de la entidad debe
     * seleccionar una cuenta de origen y una cuenta de destino.. Con esta informacion se realiza la transferencia
     * desde la cuenta de origen a la cuenta de destino por el monto especificado. La accion de transferir fondos de
     * manera interna debe ser aprobada por el administrador de la plataforma.
     *
     * @param  int $id    ID de la entidad de origen
     * @param  int $tipo  Tipo de la entidad de origen. 0=iniciativa, 1=proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-10-2012
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE REALMENTE EJECUTARLA!!!
     */
    public function actionTransferenciaInterna($id,$tipo){
        $entidad = new Entidad($id, $tipo);
        $model= new Transferencia;
        $model->setScenario('transferencia_interna');
        $model->setEntidadOrigen($entidad);
        $model->setEntidadDestino($entidad);

        if(isset($_POST['Transferencia'])){
            $model->attributes=$_POST['Transferencia'];
            if($model->enviarSolicitudTransferencia()){
                user()->setFlash('success','Se ha enviado la solicitud de transferencia al administrador correctamente.');
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }
        }

        $cuentas_acceso = Cuenta::getCuentasConAccesoYSaldo($entidad->getId(), $entidad->getTipoEntidadNumerico());

        $this->render('transferencia_interna',array(
           'entidad'=>$entidad,
            'model'=>$model,
            'cuentas_acceso'=>$cuentas_acceso
        ));
    }

    /**
     * Filtros del controlador.
     *
     * @return  array filtros del controlador.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controladr.
     *
     * @return  array reglas de acceso del controlador.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array(),
                'roles' => array('administrador'),
            ),
            array('allow',
                'actions' => array('realizarTransferencia','transferenciaInterna'),
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
}