<?php
Yii::import('application.modules.usuarios.components.Entidad');
/**
 * Controlador que contiene las acciones para desplegar los datos historicos de cuentas y movimientos
 * de entidades en el sistema.
 *
 * @version   1.0
 * @since     10/11/12
 * @author    dacuna <dacuna@dacuna.me>
 */
class HistoricoController extends Controller
{
    /**
     * Muestra un resumen de todas las entregas realizadas recientemente. Es solo util para
     * fines administrativos, ya que las entidades solo ven un detalle de todos los movimientos
     * de sus cuentas (entregas + transferencias).
     *
     * @return  void
     */
    public function actionEntregas(){
        $dataProvider=new CActiveDataProvider('Entrega', array(
            'criteria'=>array(
                //'condition'=>'valida=1',
                'order'=>'fecha_entrega DESC'
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $this->render('entregas',array(
            'dataProvider'=>$dataProvider
        ));
    }

    /**
     * Muestra un resumen de todas las transferencias realizadas recientemente. Es solo util para
     * fines administrativos, ya que las entidades solo ven un detalle de todos los movimientos
     * de sus cuentas (entregas + transferencias).
     *
     * @return  void
     */
    public function actionTransferencias(){
        $dataProvider=new CActiveDataProvider('Transferencia', array(
            'criteria'=>array(
                //'condition'=>'valida=1',
                'order'=>'fecha_transferencia DESC'
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $this->render('transferencias',array(
            'dataProvider'=>$dataProvider
        ));
    }

    /**
     * Accion que despliega todos las transferencias realizados en una cuenta en una determinada entidad.
     * Esta accion es la que realmente permite a los usuarios del sistema ver que esta pasando con sus
     * cuentas. Para esto se deben unificar los movimientos de transferencias y transferencias internas
     * dentro del sistema.
     *
     * @param int $id id de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE VER LA CUENTA DE LA ENTIDAD!!!
     */
    public function actionVerTransferenciasEntidad($id,$tipo){
        $entidad = new Entidad($id, $tipo);

        //transferencias recibidas: entregas que ha recibido la entidad
        $recibidas=new CActiveDataProvider('Entrega', array(
            'criteria'=>array(
                'condition'=>'id_entidad=:id and tipo_entidad=:tipo',
                'params'=>array(':id'=>$entidad->getId(),':tipo'=>$entidad->getTipoEntidadNumerico()),
                'order'=>'fecha_entrega DESC'
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $enviadas=new CActiveDataProvider('Entrega', array(
            'criteria'=>array(
                'condition'=>'id_entidad_origen=:id and tipo_entidad_origen=:tipo',
                'params'=>array(':id'=>$entidad->getId(),':tipo'=>$entidad->getTipoEntidadNumerico()),
                'order'=>'fecha_entrega DESC'
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $internas=new CActiveDataProvider('Transferencia', array(
            'criteria'=>array(
                'condition'=>'(id_entidad_origen=:id and tipo_entidad_origen=:tipo) OR
                              (id_entidad_destino=:id and tipo_entidad_destino=:tipo)',
                'params'=>array(':id'=>$entidad->getId(),':tipo'=>$entidad->getTipoEntidadNumerico()),
                'order'=>'fecha_transferencia DESC'
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $this->render('transferencias_entidad',array(
            'enviadas'=>$enviadas,
            'recibidas'=>$recibidas,
            'internas'=>$internas,
            'entidad'=>$entidad
        ));

    }

    /**
     * Accion que despliega todos los movimientos realizados en una cuenta en una determinada entidad.
     * Esta accion es la que realmente permite a los usuarios del sistema ver que esta pasando con sus
     * cuentas. Para esto se deben unificar los movimientos de transferencias y transferencias internas
     * dentro del sistema. Para esto se utiliza la clase @link{MovimientosCuenta} que encapsula la logica
     * para obtener todos los movimientos.
     *
     * @param int $c id de la cuenta
     * @param int $id id de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     * @todo VERIFICAR PERMISOS DE QUE LA PERSONA QUE VE ESTA ACCION PUEDE VER LA CUENTA DE LA ENTIDAD!!!
     */
    public function actionMovimientosCuenta($c,$id,$tipo){
        $entidad = new Entidad($id, $tipo);
        $cuenta=Cuenta::model()->findByPk($c);
        $movimientos=new MovimientosCuenta($cuenta->id,$entidad);
        $movimientos_all=$movimientos->cargarMovimientos();

        $this->render('movimientos_cuenta',array(
            'entidad'=>$entidad,
            'movimientos'=>$movimientos_all,
            'cuenta'=>$cuenta
        ));
    }

    /**
     * Filtros del controlador.
     *
     * @return  array filtros del controlador.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controladr.
     *
     * @return  array reglas de acceso del controlador.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('entregas','transferencias'),
                'roles'=>array('administrador'),
            ),
            array('allow',
                'actions'=>array('verTransferenciasEntidad','movimientosCuenta'),
                'roles'=>array('usuario'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
}
