<?php
/**
 * Renderiza el widget que contiene los items de un presupuesto en particular.
 *
 * @package   modules.finanzas.components
 * @example <br />
 *  //en una vista
 *  $this->widget('ItemsPresupuestoWidget',array('items'=>$presupuesto->items, 'presupuesto'=>$presupuesto));
 * @version   1.0
 * @since     2012-11-22
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ItemsPresupuestoWidget extends CWidget
{
    /**
     * Los items del presupuesto a renderizar.
     * @var array
     */
    public $items = array();

    /**
     * Referencia directa al presupuesto utilizado
     * @var Presupuesto
     */
    public $presupuesto;

    /**
     * Despliega el widget actual.
     */
    public function run()
    {
        $this->render('finanzas.views.widget.itemPresupuestoWidget', array('items' => $this->items,
            'presupuesto' => $this->presupuesto));
    }
}