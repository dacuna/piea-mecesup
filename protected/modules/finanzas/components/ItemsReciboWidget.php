<?php
/**
 * Renderiza el widget que contiene los items de un recibo en particular.
 *
 * @package   modules.finanzas.components
 * @example <br />
 *  //en una vista
 *  $this->widget('ItemsReciboWidget',array('items'=>$recibo->items, 'recibo'=>$recibo));
 * @version   1.0
 * @since     2012-11-22
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ItemsReciboWidget extends CWidget
{
    /**
     * Los items del recibo a renderizar.
     * @var array
     */
    public $items = array();

    /**
     * Referencia directa al recibo utilizado
     * @var Presupuesto
     */
    public $recibo;

    /**
     * Despliega el widget actual.
     */
    public function run()
    {
        $this->render('finanzas.views.widget.itemReciboWidget', array('items' => $this->items,
            'recibo' => $this->recibo));
    }
}