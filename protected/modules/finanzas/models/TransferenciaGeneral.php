<?php
/**
 * TransferenciaGeneral: modelo que permite realizar transferencias entre entidades. Corresponde
 * a un modelo de formulario (no ActiveRecord).
 *
 * Las transferencias posibles solamente puede corresponder a:
 * <ul>
 *  <li>Desde una cuenta en que se tenga saldo a:
 *      <ul>
 *          <li>Una entrega de fondos</li>/
 *      </ul>
 *  </li>
 * </ul>
 * Cada transferencia va desde una entidad origen, a la que el usuario que realiza la transferencia
 * tiene acceso, hacia una entidad destino que puede ser <b>cualquier</b> entidad presente en PIE&gt;A.
 * Antes de realizar la transferencia, se debe asignar una entidad de origen de tipo \Entidad al presente
 * modelo y para efectuar la transferencia, se debe utilizar el metodo realizarTransferencia().
 *
 * @example <br />
 *     $transferencia = new TransferenciaGeneral;<br />
 *     $transferencia->setEntidadOrigen(new Entidad($id,$tipo));<br />
 *     if($transferencia->validate()){<br />
 *        return $transferencia->realizarTransferencia();
 * @version   0.5
 * @since     10/18/12
 * @author    dacuna <diego.acuna@usm.cl>
 * @see       \Entidad
 * @edit    31-10-2012<br />
 *          dacuna <diego.acuna@usm.cl><br />
 *          Se refactorizo la clase debido a los cambios en el procedimiento de transferencias. Se ha actualizado
 *          tambien la documentacion de la clase.
 */
class TransferenciaGeneral extends CFormModel
{
    /**
     * El ID de la cuenta origen desde donde salen los fondos a transferir
     * @var int
     */
    private $id_cuenta_origen;
    /**
     * El tipo de la cuenta origen desde donde salen los fondos a transferir
     * @var int
     */
    private $tipo_cuenta_origen;
    /**
     * El ID de la entidad hacia donde se van a transferir los fondos
     * @var int
     */
    public $id_entidad_destino;
    /**
     * El tipo de la entidad hacia donde se van a transferir los fondos
     * @var int
     */
    public $tipo_entidad_destino;
    /**
     * El monto de la transferencia (en pesos)
     * @var int
     */
    public $monto;
    /**
     * La entidad de origen (desde donde se realiza la transferencia)
     * @var Entidad
     */
    private $entidad_origen;
    /**
     * La entidad de destino (hacia donde se realiza la transferencia)
     * @var type
     */
    private $entidad_destino;

    /**
     * Inicializa el modelo, en particular se cargan los handlers para los eventos de notificacion de transferencias
     * al administrador de la plataforma.
     *
     * @return  void
     *
     * @since   24-10-2012
     * @see     \MailMan
     */
    public function init()
    {
        parent::init();
        $this->onNotificarEntrega = array(app()->mailman, 'notificacionHandler');
    }

    /**
     * Reglas de validacion para el formulario de transferencia de fondos.
     *
     * @return  array arreglo con las reglas de validacion del modelo.
     *
     * @since   31-10-2012
     */
    public function rules()
    {
        return array(
            array('id_cuenta_origen,tipo_cuenta_origen,id_entidad_destino,tipo_entidad_destino,monto', 'required'),
            array('id_cuenta_origen', 'cuentaAccesoValidator'),
            array('monto,id_entidad_destino,tipo_entidad_destino', 'numerical','min'=>0,'integerOnly' => true,
                'tooSmall'=>'Debe seleccionar una iniciativa o proyecto v&aacute;lido.'),
            array('monto', 'numerical','min'=>0,'integerOnly' => true),
            array('tipo_cuenta_origen', 'in', 'range' => array('cuenta', 'entrega')),
            array('monto', 'noMayorValidator','tipoCuentaOrigen'=>'tipo_cuenta_origen','idCuentaOrigen'=>'id_cuenta_origen'),
            array('tipo_cuenta_origen,tipo_cuenta_destino', 'safe')
        );
    }

    /**
     * realizarTransferencia: Permite realizar una transferencia entre dos entidades, para efectos
     * de entidades, las tranferencias se pueden realizar desde:
     * <ul>
     *      <li>Una cuenta propia de la entidad</li>
     *      <li>Una cuenta a la que la entidad tenga acceso mediante una entrega</li>
     * </ul>
     * y pueden llegar hacia:
     * <ul>
     *      <li>Tener caracteristica de entrega (similar al caso comun de una entrega de fondos). Para efectos
     *          de los usuarios del sistema, esto se denomina <b>siempre</b> transferencia de fondos.
     *      </li>
     * </ul>
     * Como transferencia tambien se debe almacenar la entidad y el tipo de entidad que realizo la
     * transferencia, esto debido a que en una transferencia comun (de cuenta a cuenta) no se guarda
     * esta informacion dado que no es necesaria (se puede deducir desde la id_cuenta), pero en este tipo
     * de transferencias puede ser que se transfiera desde una entrega a una cuenta y en es caso hay que
     * almacenar dicha informacion.
     *
     * @return  bool true si se realizo la transferencia con exito, false en caso contrario.
     *
     * @since   24-10-2012
     * @edit    31-10-2012<br />
     *          dacuna <dacuna@usm.cl><br />
     *          Refactorizacion por cambios en la forma en que se efectuan las transferencias.
     *
     */
    public function realizarTransferencia()
    {
        $nueva_entrega = new Entrega;
        $nueva_entrega->monto = $this->monto;
        $nueva_entrega->id_entidad = $this->id_entidad_destino;
        $nueva_entrega->tipo_entidad = $this->tipo_entidad_destino;
        $nueva_entrega->id_entidad_origen=$this->getEntidadOrigen()->getId();
        $nueva_entrega->tipo_entidad_origen=$this->getEntidadOrigen()->getTipoEntidadNumerico();
        if ($this->tipo_cuenta_origen == 'cuenta') {
            $cuenta = Cuenta::model()->findByPk($this->id_cuenta_origen);
            $cuenta->saldo-=$this->monto;
            $nueva_entrega->id_cuenta = $cuenta->id;
            $realizo=$cuenta;

        } else if ($this->tipo_cuenta_origen == 'entrega') {
            $entrega= new EntregaUnica($this->getEntidadOrigen(),$this->id_cuenta_origen);
            $entrega->saldo -= $this->monto;
            $nueva_entrega->id_cuenta = $entrega->cuenta->id;
            $realizo=$entrega;
        }

        $transaction=app()->db->beginTransaction();
        try{
            $nueva_entrega->save(false);
            $realizo->save(false);
            //notifico al administrador
            $usuario = Persona::model()->findByPk(user()->id);
            $this->onNotificarEntrega(new CEvent($this,array('tipo'=>'notificar_entrega_entidad', 'entidad' => $this->getEntidadDestino(), 'origen' => $this->getEntidadOrigen(), 'usuario' => $usuario)));
            //se graba el movimiento
            MovimientoCuenta::grabarMovimiento($this->getEntidadOrigen(),$this->id_cuenta_origen,$nueva_entrega->fecha_entrega,
                $nueva_entrega->id_operador,$nueva_entrega->id_cuenta,$nueva_entrega->getEntidadOrigen(),$nueva_entrega->monto,
                $nueva_entrega->id_cuenta,$nueva_entrega->getEntidad());

            $transaction->commit();
            return true;
        }catch (Exception $e){
            $transaction->rollback();
        }

        return false;
    }

    public function noMayorValidator($attribute, $params)
    {
        //se verifica que el monto de la entrega no sea mayor que el saldo de la cuenta
        if (isset($this->$attribute) && !empty($this->$attribute)) {
            if ($this->$params['tipoCuentaOrigen'] == 'cuenta') {
                $cuenta = Cuenta::model()->findByPk($this->$params['idCuentaOrigen']);
                if ($cuenta->id_entidad == 0) //es la cuenta maestra
                    return;
                if ($cuenta->saldo < $this->monto)
                    $this->addError($attribute, 'El monto de la transferencia no puede sobrepasar al saldo de la cuenta seleccionada');
            } else if ($this->$params['tipoCuentaOrigen'] == 'entrega') {
                $entrega=new EntregaUnica($this->getEntidadOrigen(),$this->$params['idCuentaOrigen']);
                if ($entrega->getSaldoTotal() < $this->monto)
                    $this->addError($attribute, 'El monto de la transferencia no puede sobrepasar al saldo de la cuenta seleccionada');
            }
        }
    }

    public function cuentaAccesoValidator($attribute, $params)
    {
        if (isset($this->$attribute) && !empty($this->$attribute)) {
            //se debe verificar que la cuenta de origen sea seleccionable por la entidad de origen
            $cuentas = Cuenta::getCuentasConAccesoYSaldo($this->getEntidadOrigen()->getId(),
                $this->getEntidadOrigen()->getTipoEntidadNumerico());
            foreach ($cuentas as $cuenta) {
                if ($cuenta[1] == 'entrega' && $cuenta[0]->id_cuenta == $this->$attribute)
                    return;
                else if ($cuenta[1] == 'cuenta' && $cuenta[0]->id == $this->$attribute)
                    return;
            }
            $this->addError($attribute, 'Debes seleccionar una cuenta de origen v&aacute;lida.');
        }
        return;
    }

    public function setEntidadDestino($entidad_destino)
    {
        $this->entidad_destino = $entidad_destino;
    }

    public function getEntidadDestino()
    {
        if (!isset($this->entidad_destino)) {
            $entidad = new Entidad($this->id_entidad_destino, $this->tipo_entidad_destino);
            $this->entidad_destino = $entidad;
        }
        return $this->entidad_destino;
    }

    public function setEntidadOrigen($entidad_origen)
    {
        $this->entidad_origen = $entidad_origen;
    }

    public function getEntidadOrigen()
    {
        return $this->entidad_origen;
    }

    /**
     * onNotificarEntrega: lanza el evento de notificacion de una entrega
     * desde una entidad hacia otra. Dicha notificacion va dirigida al
     * administrador de la plataforma.
     *
     * @param   CEvent $event El evento a lanzar
     *
     * @return  void
     *
     * @since   21-10-2012
     *
     */
    public function onNotificarEntrega($event)
    {
        $this->raiseEvent('onNotificarEntrega', $event);
    }

    /**
     * @param int $id_cuenta_origen
     */
    public function setId_cuenta_origen($id_cuenta_origen)
    {
        $this->id_cuenta_origen = $id_cuenta_origen;
    }

    /**
     * @return int
     */
    public function getId_cuenta_origen()
    {
        return $this->id_cuenta_origen;
    }

    /**
     * @param int $tipo_cuenta_origen
     */
    public function setTipo_cuenta_origen($tipo_cuenta_origen)
    {
        $this->tipo_cuenta_origen = $tipo_cuenta_origen;
    }

    /**
     * @return int
     */
    public function getTipo_cuenta_origen()
    {
        return $this->tipo_cuenta_origen;
    }

}
