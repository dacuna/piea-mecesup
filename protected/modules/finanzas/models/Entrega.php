<?php

/**
 * Modelo para la tabla "{{entrega}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{entrega}}':
 * @property integer $id
 * @property integer $id_cuenta
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property integer $id_entidad_origen
 * @property integer $tipo_entidad_origen
 * @property integer $id_operador
 * @property integer $monto
 * @property integer $saldo
 * @property integer $saldo_cuenta_origen
 * @property string $fecha_entrega
 *
 * Las siguientes son las relaciones disponibles:
 * @property Cuenta $idCuenta
 */
class Entrega extends CActiveRecord
{
    private $entidad=null;
    private $entidad_origen;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Entrega the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{entrega}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_cuenta, id_entidad, tipo_entidad, monto', 'required'),
			array('id_cuenta, id_entidad, tipo_entidad, monto', 'numerical', 'integerOnly'=>true),
			array('id_cuenta, id_entidad, tipo_entidad, monto, fecha_entrega', 'safe', 'on'=>'search'),
            array('monto','noMayorValidator')
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'cuenta' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta'),
			'operador' => array(self::BELONGS_TO, 'Persona', 'id_operador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_cuenta' => 'Cuenta',
			'id_entidad' => 'Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'monto' => 'Monto',
			'saldo' => 'Saldo',
			'fecha_entrega' => 'Fecha Entrega',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_cuenta',$this->id_cuenta);
		$criteria->compare('id_entidad',$this->id_entidad);
		$criteria->compare('tipo_entidad',$this->tipo_entidad);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('fecha_entrega',$this->fecha_entrega,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Behaviors del modelo. Se cuenta con los siguientes behaviors:
     *   - CTimeStampBehavior: agrega y actualiza automaticamente el campo
     *                         fecha_entrega.
     * @return array Listado de behaviors del modelo.
     */
    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_entrega',
                'updateAttribute' => null
            ),
        );
    }

    public function beforeSave(){
        if(parent::beforeSave()){
            if($this->isNewRecord){
                $this->saldo=$this->monto;
                $this->id_operador=user()->id;
            }
            return true;
        }else{
            return false;
        }
    }

    public function noMayorValidator($attribute,$params){
        //se verifica que el monto de la entrega no sea mayor que el saldo de la cuenta
        if(isset($this->id_cuenta) && !empty($this->id_cuenta)){
            $cuenta=Cuenta::model()->findByPk($this->id_cuenta);
            if($cuenta->id_entidad==0) //es la cuenta maestra
                return;
            if($cuenta->saldo<$this->monto)
                $this->addError($attribute,'El monto de la entrega no puede sobrepasar al saldo de la cuenta seleccionada');
        }
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function getEntidad()
    {
        if($this->entidad==null)
            $this->entidad=new Entidad($this->id_entidad,$this->tipo_entidad);
        return $this->entidad;
    }

    public function setEntidadOrigen($entidad_origen)
    {
        $this->entidad_origen = $entidad_origen;
    }

    public function getEntidadOrigen()
    {
        if(!isset($this->entidad_origen) || $this->entidad_origen==null)
            $this->entidad_origen=new Entidad($this->id_entidad_origen,$this->tipo_entidad_origen);
        return $this->entidad_origen;
    }
}