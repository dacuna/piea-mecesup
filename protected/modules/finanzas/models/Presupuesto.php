<?php
Yii::import('application.modules.usuarios.components.Entidad');
/**
 * Modelo para la tabla "{{presupuesto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{presupuesto}}':
 * @property integer $id
 * @property integer $id_usuario_que_envia
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property integer $estado
 * @property string  $detalle
 * @property integer $id_cuenta
 * @property string  $fecha_solicitud
 * @property string  $fecha_accion_realizada
 * @property string  $comentario_revisor
 *
 * Las siguientes son las relaciones disponibles:
 * @property ItemPresupuesto[] $itemPresupuestos
 */
class Presupuesto extends CActiveRecord
{
	private $entidad;
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Presupuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{presupuesto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('detalle', 'required','on'=>'insert'),
            array('id_cuenta','exist','allowEmpty'=>false,'className'=>'Cuenta','attributeName'=>'id',
                'on'=>'aceptarPresupuesto')
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'items' => array(self::HAS_MANY, 'ItemPresupuesto', 'id_presupuesto'),
            'solicitante' => array(self::BELONGS_TO, 'Persona', 'id_usuario_que_envia'),
            'recibos' => array(self::MANY_MANY, 'Recibo', 'tbl_recibo_presupuesto(id_presupuesto,id_recibo)'),
            'pagoProveedores' => array(self::MANY_MANY, 'PagoProveedores', 'tbl_pago_proveedor_presupuesto(id_presupuesto,id_pago_proveedor)'),
            'cuenta' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta'),
            'unidadDeAbastecimiento' => array(self::BELONGS_TO, 'PagoProveedores', 'unidad_de_abastecimiento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_usuario_que_envia' => 'Id Usuario Que Envia',
			'id_entidad' => 'Id Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'estado' => 'Estado',
			'detalle' => 'Objetivo del presupuesto (destino de los articulos y/o servicios que se presupuestan en este documento)',
			'fecha_solicitud' => 'Fecha Solicitud',
			'fecha_accion_realizada' => 'Fecha Accion Realizada',
			'comentario_revisor' => 'Comentario Revisor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_usuario_que_envia',$this->id_usuario_que_envia);
		$criteria->compare('id_entidad',$this->id_entidad);
		$criteria->compare('tipo_entidad',$this->tipo_entidad);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('fecha_solicitud',$this->fecha_solicitud,true);
		$criteria->compare('fecha_accion_realizada',$this->fecha_accion_realizada,true);
		$criteria->compare('comentario_revisor',$this->comentario_revisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_solicitud',
                'updateAttribute' => 'fecha_accion_realizada',
            )
        );
    }

    public function beforeSave(){
        if($this->isNewRecord){
            $this->estado=-2;
            $this->id_usuario_que_envia=user()->id;
            $this->id_entidad=$this->entidad->getId();
            $this->tipo_entidad=$this->entidad->getTipoEntidadNumerico();
        }
        return parent::beforeSave();
    }

    public function getEntidad(){
        if(!isset($this->entidad))
            $this->entidad=new Entidad($this->id_entidad,$this->tipo_entidad);
        return $this->entidad;
    }

    public function setEntidad($entidad){
        $this->entidad=$entidad;
    }

    public function getEstadoPresupuesto(){
        if($this->estado==0)
            return "Pendiente de aprobaci&oacute;n";
        else if($this->estado==-1)
            return "Rechazado";
        else if($this->estado==1)
            return "Aceptado";
        else if($this->estado==2)
            return "Utilizado en acci&oacute;n comercial";
        else if($this->estado==3)
            return "Pendiente de aprobaci&oacute;n en iniciativa";
        else if($this->estado==4)
            return "Pendiente de aprobaci&oacute;n por responsable de la cuenta";
        else
            return "Sin informaci&oacute;n";
    }

    public function getTotalMontoItems(){
        $sql="SELECT SUM(precio*unidades) AS total FROM {{item_presupuesto}} WHERE id_presupuesto=".$this->id;
        $conn=app()->db;
        $command=$conn->createCommand($sql);
        $row=$command->queryRow();
        $total=$row['total'];
        if($this->cotizacion_gastos_envio!=null)
            $total+=$this->cotizacion_gastos_envio;
        return $total;
    }

    public static function getPresupuestosEntidadValidos($entidad,$unidad_de_abastecimiento=null){
        if($unidad_de_abastecimiento!=null)
            $p=Presupuesto::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=1 and unidad_de_abastecimiento is not null',array(
                ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
            ));
        else
            $p=Presupuesto::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=1 and unidad_de_abastecimiento is null',array(
                ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
            ));
        return $p;
    }

    public static function getPresupuestosEntidadUtilizados($entidad){
        $p=Presupuesto::model()->findAll('id_entidad=:ie and tipo_entidad=:te and estado=2',array(
            ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        return $p;
    }

    public function marcarPresupuestosComoUtilizados(array $presupuestos){
        foreach ($presupuestos as $presupuesto) {
            Presupuesto::updateByPk($presupuesto,array('estado'=>2));
        }
    }

    //verifica si es que la cuenta asociada a este presupuesto es una entrega o una cuenta propia en la entidad
    //que hizo el presupuesto
    private function cuentaEsEntregaOTransferencia(){
        if($this->id_entidad==$this->cuenta->id_entidad && $this->tipo_entidad==$this->cuenta->tipo_entidad)
            return 'propia';
        return 'entrega';
    }

    //reserva los fondos desde la cuenta especificada en el presupuesto
    public function reservarFondosPresupuesto(){
        $tipo_cuenta=$this->cuentaEsEntregaOTransferencia();
        if($tipo_cuenta=='entrega'){
            $cuenta=new EntregaUnica($this->getEntidad(),$this->id_cuenta);
            $cuenta->saldo-=$this->totalMontoItems;
            $cuenta->save();
        }else{
            $cuenta=$this->cuenta;
            $cuenta->saldo-=$this->totalMontoItems;
            $cuenta->save(false);
        }
        //se graba el movimiento en la cuenta
        MovimientoCuenta::grabarMovimiento($this->getEntidad(),$this->id_cuenta,$this->fecha_accion_realizada,
            $this->id_usuario_que_envia,$this->id_cuenta,$this->getEntidad(),$this->getTotalMontoItems(),null,null,
            'Solicitud de presupuesto',$this->detalle);
    }

    //permite obtener un presupuesto y verificar si es que el usuario que lo envio es quien desea manejarlo, sino es
    //dicho usuario entonces se tira una exception
    public static function managePresupuesto($id,$check_manager=true){
        $presupuesto=Presupuesto::model()->findByPk($id);
        if($presupuesto==null || ($check_manager && $presupuesto->id_usuario_que_envia!=user()->id))
            throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
        return $presupuesto;
    }

}