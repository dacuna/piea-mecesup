<?php

Yii::import('application.modules.usuarios.components.Entidad');

/**
 * Modelo para la tabla "{{cuenta}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{cuenta}}':
 * @property integer $id
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property string $nombre
 * @property string $numero
 * @property string $propietario
 * @property string $cargo_propietario
 * @property integer $saldo
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 * @property integer $encargado
 *
 * Las siguientes son las relaciones disponibles:
 * @property Entrega[] $entregas
 * @property Transferencia[] $transferencias
 * @property Transferencia[] $transferencias1
 * @property Persona $encargadoCuenta
 */
class Cuenta extends CActiveRecord
{
    private $entidad = null;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cuenta the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{cuenta}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id_entidad, tipo_entidad, nombre,numero,propietario,cargo_propietario,encargado', 'required'),
            array('id_entidad, tipo_entidad', 'numerical', 'integerOnly' => true),
            array('numero', 'match', 'pattern'=>'/^\d{2}\.\d{2}\.\d{2}$/','message'=>'{attribute} debe poseer el
                siguiente formato: 12.34.56'),
            array('nombre,encargado', 'length', 'max' => 45),
            array('numero','unique','allowEmpty'=>false),
            array('id_entidad, tipo_entidad, nombre, saldo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'entregas' => array(self::HAS_MANY, 'Entrega', 'id_cuenta'),
            'transferencias_salientes' => array(self::HAS_MANY, 'Transferencia', 'id_cuenta_origen'),
            'transferencias_entrantes' => array(self::HAS_MANY, 'Transferencia', 'id_cuenta_destino'),
            'encargadoCuenta' => array(self::BELONGS_TO, 'Persona', 'encargado'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_entidad' => 'Entidad',
            'tipo_entidad' => 'Tipo Entidad',
            'nombre' => 'Nombre',
            'propietario' => 'Propietario',
            'cargo_propietario' => 'Cargo Propietario',
            'saldo' => 'Saldo',
            'descripcion' => 'Descripci&oacute;n',
            'fecha_creacion' => 'Fecha Creaci&oacute;n',
            'fecha_actualizacion' => 'Fecha Actualizaci&oacute;n',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id_entidad', $this->id_entidad);
        $criteria->compare('tipo_entidad', $this->tipo_entidad);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('saldo', $this->saldo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Behaviors del modelo. Se cuenta con los siguientes behaviors:
     *   - CTimeStampBehavior: agrega y actualiza automaticamente los campos de
     *                         fecha_creacion y fecha_actualizacion respectivamente.
     * @return array Listado de behaviors del modelo.
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_creacion',
                'updateAttribute' => 'fecha_actualizacion',
            ),
        );
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function getEntidad()
    {
        if ($this->entidad == null)
            $this->entidad = new Entidad($this->id_entidad, $this->tipo_entidad);
        return $this->entidad;
    }

    public static function getCuentasConAcceso($id_entidad,$tipo_entidad){
        $entidad=new Entidad($id_entidad,$tipo_entidad);
        //obtengo las cuentas propias, estas son las que por definicion se tiene acceso
        $cuentas_propias=Cuenta::model()->findAll('id_entidad=:ie and tipo_entidad=:te',array(
            ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        $cuentas=array();
        foreach ($cuentas_propias as $cuenta)
            array_push($cuentas,array($cuenta,'cuenta'));
        return $cuentas;
    }

    public static function getCuentasConAccesoYSaldo($id_entidad,$tipo_entidad){
        $entidad=new Entidad($id_entidad,$tipo_entidad);
        //obtengo las cuentas propias, estas son las que por definicion se tiene acceso
        $cuentas_propias=Cuenta::model()->findAll('id_entidad=:ie and tipo_entidad=:te and saldo>0',array(
           ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        //ahora se obtienen las cuentas donde se tiene acceso por entregas
        $entregas=Entrega::model()->findAll('id_entidad=:ie and tipo_entidad=:te and saldo>0',array(
            ':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        //las junto
        $cuentas=array();
        //se deben unificar estas entregas ya que pueden haber mas de 1 entrega desde la misma cuenta hacia la misma
        //entidad de destino
        $entregas_agregar=array();
        foreach ($entregas as $entrega){
            $use_as_key = $entrega->id_cuenta.$entrega->id_entidad.$entrega->tipo_entidad;
            if(!array_key_exists($use_as_key,$entregas_agregar))
                $entregas_agregar[$use_as_key]=$entrega;
            else{
                //se busca el elemento correspondiente
                $entregas_agregar[$use_as_key]->saldo+=$entrega->saldo;
            }
        }
        //se agregan las entregas correspondientes
        foreach ($entregas_agregar as $entrega) {
            array_push($cuentas,array($entrega,'entrega'));
        }

        foreach ($cuentas_propias as $cuenta)
            array_push($cuentas,array($cuenta,'cuenta'));
        return $cuentas;
    }

    public static function getSaldoCuentaConAcceso($id_entidad,$tipo_entidad,$id_cuenta){
        $cuentas=Cuenta::getCuentasConAccesoYSaldo($id_entidad,$tipo_entidad);
        foreach ($cuentas as $cuenta) {
            if($cuenta[1] == 'cuenta'){
                if($cuenta[0]->id==$id_cuenta)
                    return $cuenta[0]->saldo;
            }else{
                if($cuenta[0]->cuenta->id==$id_cuenta)
                    return $cuenta[0]->saldo;
            }
        }
    }

    public function getIdentificador(){
        return $this->numero.': '.$this->nombre;
    }

}