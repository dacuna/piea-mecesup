<?php
/**
 * Permite unificar todas las entregas desde una cuenta a una entidad en una sola.
 *
 * Esta clase representa todas las entregas que se han realizado desde una cuenta en particular hacia una entidad
 * en particular. Permite tener el saldo unificado de la totalidad de dichas entregas y ademas descontar montos a las
 * entregas.
 *
 * @example <br />
 *     $entrega = new EntregaUnica($entidad,$id_cuenta);<br />
 *     //para descontar un $monto a las entregas
 *     $entrega->saldo-=$monto;
 *     $entrega->save();<br />
 * @version   0.1
 * @since     04/11/2012
 * @author    dacuna <diego.acuna@usm.cl>
 */
class EntregaUnica extends CComponent
{
    /**
     * La entidad que posee la entrega unificada.
     * @var Entidad
     */
    private $entidad;
    /**
     * El ID de la cuenta de origen de las entregas que se desean unificar.
     * @var int
     */
    private $id_cuenta;
    /**
     * El saldo total de la entrega unificada.
     * @var int
     */
    private $saldo_total;
    /**
     * El saldo de la entrega unificada. A diferencia del saldo total este valor puede ser cambiado mediante su setter.
     * Asi al modificar la entrega unificada se puede comparar el saldo con el saldo_total para saber en cuanto se debe
     * modificar el saldo de la entrega.
     * @var int
     */
    private $saldo;
    /**
     * Array de entregas tipo Entrega que contiene todas las entregas de la cuenta especificada por id_cuenta hacia
     * la entidad $entidad.
     * @var array
     */
    private $entregas;

    /**
     * Constructor de la clase. Asigna valores a la entidad de la entrega unificada y el ID de la cuenta de las entregas
     * que se desean unificar. Ademas, asigna el saldo total de la entrega unificada y el arreglo de entregas totales.
     *
     * @param  Entidad $entidad    Entidad que posee la entrega unificada
     * @param  int     $id_cuenta  ID de la cuenta de las entregas a unificar
     *
     * @since   04-11-2012
     *
     */
    public function __construct($entidad, $id_cuenta)
    {
        $this->entidad = $entidad;
        $this->id_cuenta = $id_cuenta;

        //se debe obtener el saldo de la entrega unificada
        $this->entregas=Entrega::model()->findAll('id_cuenta=:ic and id_entidad=:ie and tipo_entidad=:te and saldo>0',array(
            ':ic'=>$this->id_cuenta,':ie'=>$this->entidad->getId(),':te'=>$this->entidad->getTipoEntidadNumerico()
        ));
        $this->saldo=0;
        foreach ($this->entregas as $entrega){
            $this->saldo+=$entrega->saldo;
        }
        $this->saldo_total=$this->saldo;
    }

    /**
     * Actualiza el saldo de una entrega unificada. Para esto, se va descontando desde la entrega mas nueva hacia la
     * mas antigua hasta igualar el saldo que se desea descontar desde la entrega. El nombre del metodo es solo para
     * mantener compatibilidad con el codigo antiguo de TransferenciaGeneral.
     *
     * @since   04-11-2012
     *
     */
    public function save($validate=false){
        //se debe modificar en $variacion las entregas de la entrega unificada.
        $variacion=$this->saldo_total-$this->saldo;
        while($variacion!=0){
            $entrega=array_pop($this->entregas);
            //se hace solo un camino
            if($entrega->saldo>=$variacion){
                $entrega->saldo-=$variacion;
                $entrega->save(false);
                $variacion=0;
            }else{
                //es menor, descuento lo posible de la variacion y se repite el proceso
                $variacion-=$entrega->saldo;
                $entrega->saldo=0;
                $entrega->save(false);
            }
        }
        //reajusto los saldos
        $this->saldo_total=$this->saldo_total-$this->saldo;
        //cargo nuevamente las entregas al array de entregas
        $this->entregas=Entrega::model()->findAll('id_cuenta=:ic and id_entidad=:ie and tipo_entidad=:te and saldo>0',array(
            ':ic'=>$this->id_cuenta,':ie'=>$this->entidad->getId(),':te'=>$this->entidad->getTipoEntidadNumerico()
        ));

        return true;
    }

    public function aumentarSaldo($monto){
        //se le suma el monto a la ultima entrega existente
        $entrega=array_pop($this->entregas);
        $entrega->saldo+=$monto;
        if($entrega->save(false)){
            $this->saldo_total+=$monto;
            $this->saldo=$this->saldo_total;
            //cargo nuevamente las entregas al array de entregas
            $this->entregas=Entrega::model()->findAll('id_cuenta=:ic and id_entidad=:ie and tipo_entidad=:te and saldo>0',array(
                ':ic'=>$this->id_cuenta,':ie'=>$this->entidad->getId(),':te'=>$this->entidad->getTipoEntidadNumerico()
            ));
            return true;
        }
        return false;
    }

    public function getCuenta(){
        return Cuenta::model()->findByPk($this->id_cuenta);
    }

    /**
     * @param \Entidad $entidad
     */
    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    /**
     * @return \Entidad
     */
    public function getEntidad()
    {
        return $this->entidad;
    }

    /**
     * @param int $id_cuenta
     */
    public function setId_cuenta($id_cuenta)
    {
        $this->id_cuenta = $id_cuenta;
    }

    /**
     * @return int
     */
    public function getId_cuenta()
    {
        return $this->id_cuenta;
    }

    /**
     * @param int $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @return int
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param int $saldo_total
     */
    public function setSaldoTotal($saldo_total)
    {
        $this->saldo_total = $saldo_total;
    }

    /**
     * @return int
     */
    public function getSaldoTotal()
    {
        return $this->saldo_total;
    }

}
