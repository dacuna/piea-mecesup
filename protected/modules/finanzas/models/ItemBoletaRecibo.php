<?php

/**
 * Modelo para la tabla "{{item_boleta_recibo}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{item_boleta_recibo}}':
 * @property integer $id_item_recibo
 * @property integer $id_item_presupuesto
 * @property integer $item_presupuestario
 * @property integer $cantidad
 * @property integer $costo_unitario
 * @property string $detalles_adicionales
 */
class ItemBoletaRecibo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemBoletaRecibo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{item_boleta_recibo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('cantidad, costo_unitario', 'required'),
            array('id_item_presupuesto','exist','allowEmpty'=>false,'attributeName'=>'id','className'=>'ItemPresupuesto'),
            array('item_presupuestario, cantidad, costo_unitario', 'numerical', 'integerOnly'=>true),
			array('detalles_adicionales', 'length', 'max'=>45),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
            'itemPresupuesto' => array(self::BELONGS_TO, 'ItemPresupuesto', 'id_item_presupuesto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_recibo' => 'Id Item Recibo',
			'id_item_presupuesto' => 'Id Item Presupuesto',
			'item_presupuestario' => 'Item Presupuestario',
			'cantidad' => 'Cantidad',
			'costo_unitario' => 'Costo Unitario',
			'detalles_adicionales' => 'Detalles Adicionales',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item_recibo',$this->id_item_recibo);
		$criteria->compare('id_item_presupuesto',$this->id_item_presupuesto);
		$criteria->compare('item_presupuestario',$this->item_presupuestario);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('costo_unitario',$this->costo_unitario);
		$criteria->compare('detalles_adicionales',$this->detalles_adicionales,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getItems(){
        return array(
            array('id'=>21,'nombre'=>'&Iacute;tem 21','detalle'=>'Est&aacute; constituido por todos los costos de remuneraciones del personal de empleados contratados en alguna planta de la Universidad.','desc'=>'Empleados','mostrar'=>'&Iacute;tem 21 - Empleados'),
            array('id'=>22,'nombre'=>'&Iacute;tem 22','detalle'=>'Est&aacute; constituido por todos los costos de remuneraciones de los operarios contratados en alguna unidad de la Universidad.','desc'=>'Operarios','mostrar'=>'&Iacute;tem 22 - Operarios'),
            array('id'=>23,'nombre'=>'&Iacute;tem 23','detalle'=>'Est&aacute; constituido por todos los costos de remuneraciones canceladas bajo esta forma contractual.','desc'=>'Honorarios','mostrar'=>'&Iacute;tem 23 - Honorarios'),
            array('id'=>24,'nombre'=>'&Iacute;tem 24','detalle'=>'Est&aacute; constituido por todos los costos de becas de alumnos ayudantes.','desc'=>'Ayudant&iacute;as','mostrar'=>'&Iacute;tem 24 - Ayudant&iacute;as'),
            array('id'=>25,'nombre'=>'&Iacute;tem 25','detalle'=>'Son los egresos por concepto de premios por a&ntilde;os de servicio que se entregan anualmente de acuerdo con las normas de la instituci&oacute;n.','desc'=>'Premios por a&ntilde;os de servicios','mostrar'=>'&Iacute;tem 25 - Premios por a&ntilde;os de servicios'),
            array('id'=>26,'nombre'=>'&Iacute;tem 26','detalle'=>'Son los egresos por concepto de pago de horas extraordinarias, las cuales deber&aacute;n ser autorizadas por el jefe de la unidad respectiva, de acuerdo a la legislaci&oacute;n laboral vigente. El financiamiento ser&aacute; con cargo a la cuenta de operaciones de cada unidad.','desc'=>'Sobretiempos','mostrar'=>'&Iacute;tem 26 - Sobretiempos'),
            array('id'=>27,'nombre'=>'&Iacute;tem 27','detalle'=>'Son los egresos por concepto de pago de indemnizaciones y pago de vacaciones proporcionales al personal que se retira de la Instituci&oacute;n.','desc'=>'Indemnizaciones','mostrar'=>'&Iacute;tem 27 - Indemnizaciones'),
            array('id'=>28,'nombre'=>'&Iacute;tem 28','detalle'=>'Se refiere exclusivamente a los pr&eacute;stamos al personal, tales como pr&eacute;stamos bancarios, pr&eacute;stamos de salud,   contingentes y otros.','desc'=>'Pr&eacute;stamos al Personal','mostrar'=>'&Iacute;tem 28 - Pr&eacute;stamos al Personal'),
            array('id'=>29,'nombre'=>'&Iacute;tem 29','detalle'=>'Comprende todo tipo de pagos al personal que no est&aacute; contemplado en los &iacute;tems anteriores, como aguinaldos, movilizaci&oacute;n complementaria, bonificaciones especiales, etc.','desc'=>'Otras remuneraciones','mostrar'=>'&Iacute;tem 29 - Otras remuneraciones'),
            array('id'=>31,'nombre'=>'&Iacute;tem 31','detalle'=>'Son los gastos que est&aacute;n relacionados con la ense&ntilde;anza, como por ejemplo la dictaci&oacute;n de clases, actividades de laboratorio de las diferentes asignaturas, investigaci&oacute;n dentro de la universidad y elaboraci&oacute;n de proyectos que tengan que ver con la  docencia, lo  que provoca la  adquisici&oacute;n de insumos tales como: cuadernos, papeles de dibujo, impresi&oacute;n de material para clases, punteros l&aacute;ser, telones, borradores, resmas de hojas distintos tama&ntilde;os,  minerales, hojalatas, planchas, planchones de acero, ca&ntilde;er&iacute;as, productos de cobre, zinc, bronce, alambres. Adem&aacute;s, se incluyen herramientas menores, tiza, plumones, reglas, transportadores, compases, l&aacute;pices, gomas, etc. y cualquier otro material de naturaleza similar. En este &iacute;tem, se incluir&aacute; tambi&eacute;n,  el material deportivo como: balones, cuerdas, mancuernas, pesas, colchoneta, bastones y otros utilizados para la ense&ntilde;anza y actividad deportiva.','desc'=>'Materiales de ense&ntilde;anza','mostrar'=>'&Iacute;tem 31 - Materiales de ense&ntilde;anza'),
            array('id'=>32,'nombre'=>'&Iacute;tem 32','detalle'=>'Son los gastos que est&aacute;n relacionados con  el uso de materiales en las oficinas tales  como: formularios, impresos, archivadores de palanca, carpetas, formularios cont&iacute;nuos, calendarios, libretas, libretas de correspondencia, libros de contabilidad, block de apuntes, post-it note, tacos, papel  mantequilla, milimetrado, original, rayado, resmas de papel de distintos tama&ntilde;os, correctores, plumones, cartones, car&aacute;tulas, cartulinas,  sobres de diversos tipos, tarjetas, cinta adhesiva, pegamento, cartuchos de tinta para impresoras, corcheteras, perforadoras, porta scotch, scotch, porta clips, tampones  alfileres, apretadores, broches, chinches, magic clip,  clips, corchetes, cordel, el&aacute;stico, tijeras, corta cart&oacute;n, escuadras y reglas,  gomas de borrar, hilo, l&aacute;pices, timbres,  etc. Se incluye adem&aacute;s el material necesario para el funcionamiento de m&aacute;quinas de calcular electr&oacute;nicas, Fax,  computadores e impresoras, tales como, rollos de papel, CD y DVD, pendrive, papeles, formularios impresos y cualquier otro insumo de naturaleza similar.','desc'=>'Materiales y &uacute;tiles de oficina','mostrar'=>'&Iacute;tem 32 - Materiales y &uacute;tiles de oficina'),
            array('id'=>33,'nombre'=>'&Iacute;tem 33','detalle'=>'Son los gastos por concepto de adquisiciones de productos qu&iacute;micos org&aacute;nicos e inorg&aacute;nicos, fertilizantes, insecticidas  y  fungicidas, material de curaciones, productos farmac&eacute;uticos y en general lo que tiene que ver con la actividad m&eacute;dico y dental.','desc'=>'Productos qu&iacute;micos y farmac&eacute;uticos','mostrar'=>'&Iacute;tem 33 - Productos qu&iacute;micos y farmac&eacute;uticos'),
            array('id'=>34,'nombre'=>'&Iacute;tem 34','detalle'=>'Comprende los gastos por concepto de adquisiciones de materias primas y semi-elaboradas de origen agropecuario, forestal, minero e industrial, que requieren las diferentes unidades.','desc'=>'Materias primas','mostrar'=>'&Iacute;tem 34 - Materias primas'),
            array('id'=>35,'nombre'=>'&Iacute;tem 35','detalle'=>'Son los gastos por concepto de adquisiciones de materiales que tiene que ver con el desarrollo de limpieza y aseo de la universidad tales como: baldes, bombas insecticidas, ceras, diluyente, detergente, escobas, escobillas, escobillones, esponjas, hisopos, huaipes, insecticida l&iacute;quido, jabones, limpia metales, palas de basura, pa&ntilde;os de limpieza, papel higi&eacute;nico, servilletas de papel, plumeros, sopapos, toallas de papel, virutillas y todo producto de naturaleza similar, ocupados en esta labor.','desc'=>'&Uacute;tiles de aseo','mostrar'=>'&Iacute;tem 35 - &Uacute;tiles de aseo'),
            array('id'=>36,'nombre'=>'&Iacute;tem 36','detalle'=>'Corresponde a aquellos gastos no considerados en los restantes &iacute;tems del clasificador de gastos.','desc'=>'Otros gastos','mostrar'=>'&Iacute;tem 36 - Otros gastos'),
            array('id'=>39,'nombre'=>'&Iacute;tem 39','detalle'=>'Corresponde exclusivamente a gastos efectuados por la adquisici&oacute;n de los siguientes bienes: galvanos, medallas, diplomas, estatuillas, bandejas y otros similares.','desc'=>'Premios','mostrar'=>'&Iacute;tem 39 - Premios'),
            array('id'=>41,'nombre'=>'&Iacute;tem 41','detalle'=>'Son los gastos por mantenci&oacute;n y reparaci&oacute;n menores de los edificios o construcciones de la instituci&oacute;n. Que por su menor monto, no califican para el concurso de Infraestructura.','desc'=>'Mantenci&oacute;n y reparaci&oacute;n de edificios','mostrar'=>'&Iacute;tem 41 - Mantenci&oacute;n y reparaci&oacute;n de edificios'),
            array('id'=>42,'nombre'=>'&Iacute;tem 42','detalle'=>'Son los gastos por mantenci&oacute;n y reparaci&oacute;n de equipos y m&aacute;quinas de laboratorios, talleres,  equipos de aire acondicionado, calderas, muebles, impresoreas, fax, incluyendo equipos usados con fines administrativos.','desc'=>'Mantenci&oacute;n y reparaci&oacute;n de m&aacute;quinas, equipos y muebles','mostrar'=>'&Iacute;tem 42 - Mantenci&oacute;n y reparaci&oacute;n de m&aacute;quinas, equipos y muebles'),
            array('id'=>50,'nombre'=>'&iacute;tem 50','detalle'=>'Son los gastos por concepto de publicidad, tales como avisos, promoci&oacute;n en peri&oacute;dicos, confecci&oacute;n de afiches, compra de diarios, art&iacute;culos publicitarios, etc.','desc'=>'Publicidad y merchandising','mostrar'=>'&iacute;tem 50 - Publicidad y merchandising'),
            array('id'=>51,'nombre'=>'&Iacute;tem 51','detalle'=>'Son los gastos por concepto de correspondencia tales como: env&iacute;o de cartas y todo elemento que se despache por intermedio de la Oficina de Partes u otro medio externo. ','desc'=>'Correspondencia','mostrar'=>'&Iacute;tem 51 - Correspondencia'),
            array('id'=>52,'nombre'=>'&iacute;tem 52','detalle'=>'Son los gastos por concepto de servicios de impresi&oacute;n de folletos, revistas, apuntes, circulares y en general, todo gasto similar que se destine a estos efectos, realizados interna o externamente.','desc'=>'Servicio de impresi&oacute;n','mostrar'=>'&iacute;tem 52 - Servicio de impresi&oacute;n'),
            array('id'=>53,'nombre'=>'&Iacute;tem 53','detalle'=>'Son los gastos por concepto de arriendo de maquinarias, equipos electr&oacute;nicos, bienes ra&iacute;ces, licencias de software y otros.','desc'=>'Arriendo','mostrar'=>'&Iacute;tem 53 - Arriendo'),
            array('id'=>54,'nombre'=>'&Iacute;tem 54','detalle'=>'Son los gastos por concepto de pasajes (ejemplos como: a&eacute;reos, terrestres, etc.) y gastos por concepto de vi&aacute;ticos asociados a comisiones de servicios. Adem&aacute;s de gastos por fletes, mudanzas, efectuadas en veh&iacute;culos o medios no pertenecientes a la Universidad.','desc'=>'Pasajes, fletes y vi&aacute;ticos','mostrar'=>'&Iacute;tem 54 - Pasajes, fletes y vi&aacute;ticos'),
            array('id'=>55,'nombre'=>'&Iacute;tem 55','detalle'=>'Son los gastos por concepto de movilizaci&oacute;n, fletes, transporte y otros similares, efectuados en veh&iacute;culos de la Universidad.','desc'=>'Servicios de movilizaci&oacute;n','mostrar'=>'&Iacute;tem 55 - Servicios de movilizaci&oacute;n'),
            array('id'=>56,'nombre'=>'&Iacute;tem 56','detalle'=>'Son los gastos de contrataci&oacute;n de servicios tales como: auditorias externas, asesor&iacute;as legales, estrat&eacute;gicas y otras asesor&iacute;as institucionales y similares. Se deber&aacute;n incluir los pagos por inscripci&oacute;n y matriculas en cursos, seminarios y otros.','desc'=>'Contrataci&oacute;n de servicios','mostrar'=>'&Iacute;tem 56 - Contrataci&oacute;n de servicios'),
            array('id'=>57,'nombre'=>'&Iacute;tem 57','detalle'=>'Comprende los gastos de adquisici&oacute;n de combustibles y lubricantes para el consumo de maquinaria, caldera, central el&eacute;ctrica, calefacci&oacute;n, equipos de laboratorio y otros usos. Tambi&eacute;n se incluyen los gastos por concepto de gas de ca&ntilde;er&iacute;a y licuado.','desc'=>'Combustibles, lubricantes y gas','mostrar'=>'&Iacute;tem 57 - Combustibles, lubricantes y gas'),
            array('id'=>58,'nombre'=>'&Iacute;tem 58','detalle'=>'Son los gastos por concepto de inauguraciones, recepciones, atenci&oacute;n de autoridades y otros an&aacute;logos, en representaci&oacute;n de la Universidad.','desc'=>'Gastos de representaci&oacute;n','mostrar'=>'&Iacute;tem 58 - Gastos de representaci&oacute;n'),
            array('id'=>59,'nombre'=>'&Iacute;tem 59','detalle'=>'Comprende los pagos que efect&uacute;en las unidades a los departamentos inform&aacute;ticos  por concepto de   servicios de computaci&oacute;n prestados.','desc'=>'Servicio de computaci&oacute;n','mostrar'=>'&Iacute;tem 59 - Servicio de computaci&oacute;n'),
            array('id'=>61,'nombre'=>'&Iacute;tem 61','detalle'=>'Son los gastos que por este concepto se realizan para la alimentaci&oacute;n de funcionarios y estudiantes, a trav&eacute;s de los Servicios de Alimentaci&oacute;n. Se incluir&aacute;n en este &iacute;tem, los gastos que efect&uacute;en las unidades para colaciones o refrigerios.','desc'=>'Gastos de alimentaci&oacute;n','mostrar'=>'&Iacute;tem 61 - Gastos de alimentaci&oacute;n'),
            array('id'=>62,'nombre'=>'&Iacute;tem 62','detalle'=>'Incluye los gastos por concepto de adquisiciones de uniformes, delantales, zapatos, camisas y otros art&iacute;culos de naturaleza similar, destinados a complementar el vestuario del personal, de acuerdo con la reglamentaci&oacute;n vigente. En este &iacute;tem, se deber&aacute; incluir adem&aacute;s los elementos de seguridad. Y adem&aacute;s se deber&aacute; incluir camisetas, pantalones, medias, buzos, etc., adquiridos para la representaci&oacute;n institucional.','desc'=>'Vestuario y calzado','mostrar'=>'&Iacute;tem 62 - Vestuario y calzado'),
            array('id'=>63,'nombre'=>'&Iacute;tem 63','detalle'=>'En este &iacute;tem se incluye todo gasto por adquisici&oacute;n de loza, cuchiller&iacute;a, vajilla y otros.','desc'=>'Menaje','mostrar'=>'&Iacute;tem 63 - Menaje'),
            array('id'=>64,'nombre'=>'&Iacute;tem 64','detalle'=>'Son los pagos por concepto de consumo de energ&iacute;a el&eacute;ctrica. Se presupuesta de forma centralizada, por Servicios del Campus.','desc'=>'Electricidad','mostrar'=>'&Iacute;tem 64 - Electricidad'),
            array('id'=>65,'nombre'=>'&Iacute;tem 65','detalle'=>'Son los pagos por concepto de consumo de agua potable y desag&uuml;es. Se presupuesta de forma centralizada, por Servicios del Campus.','desc'=>'Agua, desag&uuml;es','mostrar'=>'&Iacute;tem 65 - Agua, desag&uuml;es'),
            array('id'=>66,'nombre'=>'&Iacute;tem 66','detalle'=>'Corresponde a los gastos por arriendo de central telef&oacute;nica, tel&eacute;fonos individuales, arriendo de l&iacute;nea de fax, contrataci&oacute;n de Internet y telefon&iacute;a m&oacute;vil, adquisici&oacute;n de tel&eacute;fonos, cit&oacute;fonos, celulares, etc., adem&aacute;s de la adquisici&oacute;n de tarjetas telef&oacute;nicas de prepago.','desc'=>'Tel&eacute;fono, fax e Internet','mostrar'=>'&Iacute;tem 66 - Tel&eacute;fono, fax e Internet'),
            array('id'=>67,'nombre'=>'&Iacute;tem 67','detalle'=>'Son los gastos correspondientes al uso de lavander&iacute;a, lavado de ropa y otros similares.','desc'=>'Servicio de lavander&iacute;a','mostrar'=>'&Iacute;tem 67 - Servicio de lavander&iacute;a'),
            array('id'=>68,'nombre'=>'&Iacute;tem 68','detalle'=>'Son los pagos por primas de seguros contratados para los bienes muebles e inmuebles institucionales, seguros para alumnos y personal de la Universidad, cuando corresponda.','desc'=>'Seguros','mostrar'=>'&Iacute;tem 68 - Seguros'),
            array('id'=>69,'nombre'=>'&Iacute;tem 69','detalle'=>'En este &iacute;tem, se incluyen los pagos por contribuciones, pago de impuesto, patentes y otros gastos fiscales y/o municipales.','desc'=>'Impuestos y contribuciones fiscales','mostrar'=>'&Iacute;tem 69 - Impuestos y contribuciones fiscales'),
            array('id'=>71,'nombre'=>'&Iacute;tem 71','detalle'=>'Se entiende por transferencias al sector privado, todos aquellos pagos que constituyan aportes de parte de la Universidad, tales como: donaciones, erogaciones y aportes al Consejo de Rectores, Consejo Coordinador Universitario de Valpara&iacute;so y otros.','desc'=>'Transferencias al sector privado','mostrar'=>'&Iacute;tem 71 - Transferencias al sector privado'),
            array('id'=>72,'nombre'=>'&Iacute;tem 72','detalle'=>'Son aquellos aportes que realiza la Universidad a instituciones del sector p&uacute;blico.','desc'=>'Transferencias al sector p&uacute;blico','mostrar'=>'&Iacute;tem 72 - Transferencias al sector p&uacute;blico'),
            array('id'=>73,'nombre'=>'&Iacute;tem 73','detalle'=>'Se incluir&aacute;n las Becas Internas de Arancel, Matr&iacute;cula y Mantenci&oacute;n. Adem&aacute;s de los pr&eacute;stamos otorgados a los alumnos en el corto y largo plazo.','desc'=>'Transferencias alumnos','mostrar'=>'&Iacute;tem 73 - Transferencias alumnos'),
            array('id'=>81,'nombre'=>'&Iacute;tem 81','detalle'=>'Compra de m&aacute;quinas y herramientas, destinadas para laboratorios, talleres, oficina, instrucci&oacute;n u otro fin.','desc'=>'Adquisici&oacute;n de m&aacute;quinas herramientas','mostrar'=>'&Iacute;tem 81 - Adquisici&oacute;n de m&aacute;quinas herramientas'),
            array('id'=>82,'nombre'=>'&Iacute;tem 82','detalle'=>'Compra de equipos, muebles y &uacute;tiles inventariables para oficinas, talleres, laboratorios, salas de clases, recintos deportivos y otros.','desc'=>'Adquisici&oacute;n de muebles, &uacute;tiles y alhajamiento','mostrar'=>'&Iacute;tem 82 - Adquisici&oacute;n de muebles, &uacute;tiles y alhajamiento'),
            array('id'=>83,'nombre'=>'&Iacute;tem 83','detalle'=>'Comprende los gastos por compra de libros, suscripciones de revistas y otro material destinado a las bibliotecas, cuyas compras deber&aacute;n encauzarse a trav&eacute;s del sistema de adquisiciones en la Biblioteca Central. No obstante, siendo estos libros adquiridos con el presupuesto de la unidad, deber&aacute;n ser resguardados por las bibliotecas respectivas.','desc'=>'Adquisici&oacute;n de material bibliogr&aacute;fico','mostrar'=>'&Iacute;tem 83 - Adquisici&oacute;n de material bibliogr&aacute;fico'),
            array('id'=>84,'nombre'=>'&Iacute;tem 84','detalle'=>'Se refiere a los gastos de construcciones y remodelaciones de salas de clases, laboratorios, oficinas u otros espacios f&iacute;sicos.','desc'=>'Construcciones y remodelaciones','mostrar'=>'&Iacute;tem 84 - Construcciones y remodelaciones'),
            array('id'=>85,'nombre'=>'&Iacute;tem 85','detalle'=>'Comprende aquellos gastos originados por la adquisici&oacute;n de bienes ra&iacute;ces, tales como casas, edificios, terrenos, etc.','desc'=>'Adquisici&oacute;n bienes ra&iacute;ces','mostrar'=>'&Iacute;tem 85 - Adquisici&oacute;n bienes ra&iacute;ces'),
            array('id'=>86,'nombre'=>'&Iacute;tem 86','detalle'=>'Comprende aquellos gastos por concepto de adquisici&oacute;n de acciones, t&iacute;tulos, bonos, pagar&eacute;s, deventures y otras especies de valor.','desc'=>'Adquisici&oacute;n de valores mobiliarios y otros','mostrar'=>'&Iacute;tem 86 - Adquisici&oacute;n de valores mobiliarios y otros'),
            array('id'=>87,'nombre'=>'&Iacute;tem 87','detalle'=>'Comprende aquellos gastos originados por la adquisici&oacute;n de veh&iacute;culos.','desc'=>'Adquisici&oacute;n material rodante','mostrar'=>'&Iacute;tem 87 - Adquisici&oacute;n material rodante'),
            array('id'=>91,'nombre'=>'&Iacute;tem 91','detalle'=>'Corresponde a pagos parciales o totales de deudas contra&iacute;das a largo plazo (m&aacute;s de 12 meses), por equipamiento, d&eacute;ficit de caja u otro motivo. ','desc'=>'Amortizaci&oacute;n cr&eacute;ditos a largo plazo','mostrar'=>'&Iacute;tem 91 - Amortizaci&oacute;n cr&eacute;ditos a largo plazo'),
            array('id'=>92,'nombre'=>'&Iacute;tem 92','detalle'=>'Corresponde a los gastos financieros directos generados por el endeudamiento a largo plazo.','desc'=>'Intereses cr&eacute;ditos a largo plazo','mostrar'=>'&Iacute;tem 92 - Intereses cr&eacute;ditos a largo plazo'),
            array('id'=>93,'nombre'=>'&Iacute;tem 93','detalle'=>'Corresponde a pagos parciales o totales de deudas contra&iacute;das a corto plazo (menos de 12 meses), por equipamiento, d&eacute;ficit de caja u otro motivo.','desc'=>'Amortizaci&oacute;n cr&eacute;ditos a corto plazo','mostrar'=>'&Iacute;tem 93 - Amortizaci&oacute;n cr&eacute;ditos a corto plazo'),
            array('id'=>94,'nombre'=>'&Iacute;tem 94','detalle'=>'Comprende los gastos financieros directos, generados por el endeudamiento.','desc'=>'Intereses cr&eacute;ditos a corto plazo','mostrar'=>'&Iacute;tem 94 - Intereses cr&eacute;ditos a corto plazo'),
            array('id'=>95,'nombre'=>'&Iacute;tem 95','detalle'=>'Comprende los gastos bancarios, comisiones y otros, generados por pr&eacute;stamos a corto y largo plazo y por otros motivos. No se considerar&aacute;n los gastos bancarios por importaciones, los cuales se incorporar&aacute;n a los bienes.','desc'=>'Gastos Bancarios','mostrar'=>'&Iacute;tem 95 - Gastos Bancarios'),
        );
    }
}