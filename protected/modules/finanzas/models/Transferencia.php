<?php

/**
 * Modelo para la tabla "{{transferencia}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{transferencia}}':
 * @property integer $id
 * @property integer $id_cuenta_origen
 * @property integer $id_cuenta_destino
 * @property integer $monto
 * @property integer $id_entidad_origen
 * @property integer $tipo_entidad_origen
 * @property integer $id_entidad_destino
 * @property integer $tipo_entidad_destino
 * @property integer $id_operador
 * @property string $fecha_transferencia
 *
 * Las siguientes son las relaciones disponibles:
 * @property Cuenta $idCuentaOrigen
 * @property Cuenta $idCuentaDestino
 */
class Transferencia extends CActiveRecord
{
    public  $tipo_cuenta_origen;
    public  $tipo_cuenta_destino;
    private $entidad_que_transfirio;
    private $actualizar_cuenta_origen=true;

    private $entidad_origen;
    private $entidad_destino;

    public function init(){
        parent::init();
        $this->onSolicitarAprobacionTransferencia=array(app()->mailman,'notificacionHandler');
    }

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Transferencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{transferencia}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_cuenta_origen, id_cuenta_destino, monto,tipo_cuenta_origen,tipo_cuenta_destino', 'required'),
			array('tipo_cuenta_origen,tipo_cuenta_destino', 'required','on'=>'transferencia_interna'),
			array('id_cuenta_origen, id_cuenta_destino, monto', 'numerical', 'integerOnly'=>true),
            array('id_cuenta_origen','noMayorValidator'),
			array('id_cuenta_origen, id_cuenta_destino, monto, fecha_transferencia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'cuenta_origen' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta_origen'),
			'cuenta_destino' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta_destino'),
			'operador' => array(self::BELONGS_TO, 'Persona', 'id_operador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_cuenta_origen' => 'Cuenta Origen',
			'id_cuenta_destino' => 'Cuenta Destino',
			'monto' => 'Monto',
			'fecha_transferencia' => 'Fecha Transferencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_cuenta_origen',$this->id_cuenta_origen);
		$criteria->compare('id_cuenta_destino',$this->id_cuenta_destino);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('fecha_transferencia',$this->fecha_transferencia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Behaviors del modelo. Se cuenta con los siguientes behaviors:
     *   - CTimeStampBehavior: agrega y actualiza automaticamente el campo
     *                         fecha_transferencia.
     * @return array Listado de behaviors del modelo.
     */
    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_transferencia',
            ),
        );
    }

    public function beforeSave(){
        if(parent::beforeSave()){
            if($this->isNewRecord){
                if(empty($this->id_operador))
                    $this->id_operador=user()->id;
            }
            return true;
        }else{
            return false;
        }
    }

    public function getEntidadQueTransfirio(){
        if(!isset($this->entidad_que_transfirio)){
            $this->entidad_que_transfirio=new Entidad($this->id_entidad_que_transfirio,$this->tipo_entidad_que_transfirio);
        }
        return $this->entidad_que_transfirio;
    }

    public function noMayorValidator($attribute,$params){
        //se verifica que el monto de la entrega no sea mayor que el saldo de la cuenta
        if(isset($this->id_cuenta_origen) && !empty($this->id_cuenta_origen)){
            $cuenta_origen=Cuenta::model()->findByPk($this->id_cuenta_origen);
            if($cuenta_origen->id_entidad==0) //es la cuenta maestra
                return;
            if($cuenta_origen->saldo<$this->monto)
                $this->addError($attribute,'El monto de la entrega no puede sobrepasar al saldo de la cuenta de origen');
        }
    }

    public function setActualizarCuentaOrigen($actualizar_cuenta_origen)
    {
        $this->actualizar_cuenta_origen = $actualizar_cuenta_origen;
    }

    public function getActualizarCuentaOrigen()
    {
        return $this->actualizar_cuenta_origen;
    }

    public function enviarSolicitudTransferencia(){
        $usuario=Persona::model()->findByPk(user()->id);
        $tipo_origen=($this->tipo_cuenta_origen=='cuenta')?1:0;
        $tipo_destino=($this->tipo_cuenta_destino=='cuenta')?1:0;
        $solicitud=SolicitudTransferencia::crearSolicitudTransferencia($this->getEntidadOrigen(),
            $this->getEntidadDestino(),array($this->id_cuenta_origen,$tipo_origen),
            array($this->id_cuenta_destino,$tipo_destino),$this->monto);
        if($solicitud!=null){
            //se debe reservar el monto en la cuenta de origen
            if ($this->tipo_cuenta_origen == 'cuenta') {
                $cuenta = Cuenta::model()->findByPk($this->id_cuenta_origen);
                $cuenta->saldo-=$this->monto;
                $realizo=$cuenta;
            } else if ($this->tipo_cuenta_origen == 'entrega') {
                $entrega= new EntregaUnica($this->getEntidadOrigen(),$this->id_cuenta_origen);
                $entrega->saldo -= $this->monto;
                $realizo=$entrega;
            }
            if($realizo->save(false)){
                $this->onSolicitarAprobacionTransferencia(new CEvent($this,
                    array('tipo'=>'solicitar_aprobacion_transferencia','solicitud'=>$solicitud,'usuario'=>$usuario)));
                return true;
            }
        }
        return false;
    }

    public function setEntidadDestino($entidad_destino)
    {
        $this->entidad_destino = $entidad_destino;
    }

    public function getEntidadDestino()
    {
        if($this->entidad_destino==null)
            $this->entidad_destino=new Entidad($this->id_entidad_destino,$this->tipo_entidad_destino);
        return $this->entidad_destino;
    }

    public function setEntidadOrigen($entidad_origen)
    {
        $this->entidad_origen = $entidad_origen;
    }

    public function getEntidadOrigen()
    {
        if($this->entidad_origen==null)
            $this->entidad_origen=new Entidad($this->id_entidad_origen,$this->tipo_entidad_origen);
        return $this->entidad_origen;
    }

    /**
     * onSolicitarAprobacionTransferencia: lanza el evento de notificacion de solicitud de una
     * transferencia desde una entidad hacia otra. Dicha notificacion va dirigida hacia el
     * administrador de la plataforma.
     *
     * @param   CEvent $event El evento a lanzar
     *
     * @return  void
     *
     * @since   21-10-2012
     *
     */
    public function onSolicitarAprobacionTransferencia($event){
        $this->raiseEvent('onSolicitarAprobacionTransferencia',$event);
    }

}