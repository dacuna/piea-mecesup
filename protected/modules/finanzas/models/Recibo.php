<?php

/**
 * Modelo para la tabla "{{recibo}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{recibo}}':
 * @property integer $id
 * @property integer $codigo_presupuestario
 * @property string $unidad_presupuestaria
 * @property integer $id_deudor
 * @property integer $id_usuario_que_solicita
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property string $fecha_vale
 * @property integer $monto_vale
 * @property string $nota
 * @property integer $total_gastos
 * @property integer $saldo_favor_deudor
 * @property integer $saldo_reintegrar_caja
 * @property integer $total_igual
 * @property integer $tipo_recibo
 * @property integer $estado
 * @property Date $fecha_emison_documento
 *
 * Las siguientes son las relaciones disponibles:
 * @property ItemRecibo[] $items
 * @property Cuenta $codigoPresupuestario
 * @property User $idDeudor
 */
class Recibo extends CActiveRecord
{
    public $presupuestosIds=array();
    private $entidad;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Recibo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{recibo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_deudor, nota', 'required','on'=>'insert'),
			array(' id_deudor, nota', 'required','on'=>'update'),
            array('presupuestosIds','ext.ArrayValidator','validator' => 'exist',
                'params' => array(
                    'attributeName' => 'id','className' => 'Presupuesto'
                ),
                'separateParams' => false,
                'allowEmpty'=>false
            ),
            array('presupuestosIds','validarMismaCuenta'),
		);
	}

    public function validarMismaCuenta($attribute,$params)
    {
        if(isset($this->$attribute) || is_array($this->$attribute)){
            $cuentas=array();
            foreach ($this->$attribute as $key=>$presupuesto) {
                $pre=Presupuesto::model()->findByPk($presupuesto);
                array_push($cuentas,$pre->id_cuenta);
            }
            $cuentas=array_unique($cuentas);
            if(count($cuentas)>1){
                $this->addError($attribute,'Los presupuestos deben tener la misma cuenta de origen de fondos.');
            }

        }
    }

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'items' => array(self::HAS_MANY, 'ItemRecibo', 'id_recibo'),
			'codigoPresupuestario' => array(self::BELONGS_TO, 'Cuenta', 'codigo_presupuestario'),
			'deudor' => array(self::BELONGS_TO, 'Persona', 'id_deudor'),
			'solicitante' => array(self::BELONGS_TO, 'Persona', 'id_usuario_que_solicita'),
            'presupuestos' => array(self::MANY_MANY, 'Presupuesto', 'tbl_recibo_presupuesto(id_recibo,id_presupuesto)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo_presupuestario' => 'Codigo Presupuestario',
			'unidad_presupuestaria' => 'Unidad Presupuestaria',
			'id_deudor' => 'Nombre del deudor',
            'presupuestosIds'=>'presupuesto',
			'fecha_vale' => 'Fecha Vale',
			'nota' => 'Descripci&oacute;n del formulario de '.$this->tipoRecibo,
			'total_gastos' => 'Total Gastos',
			'saldo_favor_deudor' => 'Saldo Favor Deudor',
			'saldo_reintegrar_caja' => 'Saldo Reintegrar Caja',
			'total_igual' => 'Total Igual',
			'tipo_recibo' => 'Tipo Recibo',
			'estado' => 'Estado',
		);
	}

    public function beforeSave(){
        if($this->isNewRecord){
            $pre=Presupuesto::model()->findByPk($this->presupuestosIds[0]);
            $this->codigo_presupuestario=$pre->id_cuenta;
            $this->id_usuario_que_solicita=user()->id;
            $this->id_entidad=$this->entidad->getId();
            $this->tipo_entidad=$this->entidad->getTipoEntidadNumerico();
        }

        return parent::beforeSave();
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_vale',
                'updateAttribute' => null,
            ),
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
        );
    }

    public function afterFind()
    {
        parent::afterFind();
        if (!empty($this->presupuestos))
        {
            foreach ($this->presupuestos as $n => $presupuesto)
                $this->presupuestosIds[] = $presupuesto->id;
        }
    }

    public function getTipoRecibo(){
        if($this->tipo_recibo==1)
            return "Recuperaci&oacute;n de gastos";
        else if($this->tipo_recibo==2)
            return "Vale a rendir cuenta";
        else
            return "Rendici&oacute;n de caja chica";
    }

    public function getEntidad(){
        if(!isset($this->entidad))
            $this->entidad=new Entidad($this->id_entidad,$this->tipo_entidad);
        return $this->entidad;
    }

    public function setEntidad($entidad){
        $this->entidad=$entidad;
    }

    public function setTipoRecibo($tipo_recibo){
        if($tipo_recibo!=1 && $tipo_recibo!=2)
            throw new CHttpException(500,"Tipo de recibo financiero no v&aacute;lido.");
        $this->tipo_recibo=$tipo_recibo;
    }

    public function getTotalMontoItems(){
        $sql="SELECT SUM(valor) AS total FROM {{item_recibo}} WHERE id_recibo=".$this->id;
        $conn=app()->db;
        $command=$conn->createCommand($sql);
        $row=$command->queryRow();
        return $row['total'];
    }

    public function getTotalMontoItemsPresupuestos()
    {
        $total = 0;
        foreach ($this->presupuestosIds as $presupuesto_id) {
            $presupuesto = Presupuesto::model()->findByPk($presupuesto_id);
            if($presupuesto!=null){
                foreach ($presupuesto->items as $item)
                   $total += $item->precio * $item->unidades;
            }
        }
        return $total;
    }

    public function calcularSaldoReintegrarCaja(){
        $total_items=$this->totalMontoItems;
        $total_presupuesto=$this->totalMontoItemsPresupuestos;
        if($total_items<$total_presupuesto)
            return $total_presupuesto-$total_items;
        return 0;
    }


    public function getItemsPresupuestos(){
        $items=array();
        foreach ($this->presupuestos as $presupuesto)
            foreach ($presupuesto->items as $item)
                $items[]=$item;
        return $items;
    }

    public function getEstadoRecibo(){
        if($this->estado==0 && $this->tipo_recibo!=2)
            return "Pendiente de aprobaci&oacute;n";
        else if($this->estado==0 && $this->tipo_recibo==2)
            return "Sin notificaci&oacute;n de entrega";
        else if($this->estado==-1)
            return "Rechazado";
        else if($this->estado==1)
            return "Aceptado";
        else if($this->estado==2 && $this->tipo_recibo==2)
            return "Notificado como entrega en oficina PIEA";
        else if($this->estado==2 && $this->tipo_recibo!=2)
            return "Utilizado en acci&oacute;n comercial";
        else if($this->estado==3 && $this->tipo_recibo==2)
            return "Recibido en oficina de PIEA";
        else if($this->estado==4 && $this->tipo_recibo==2)
            return "Pendiente de aprobaci&oacute;n";
        else if($this->estado==5)
            return "En espera de comprobante de reintegraci&oacute;n";
        else if($this->estado==6)
            return "En espera de notificaci&oacute;n de recibo de comprobante de reintegraci&oacute;n";
        else
            return "Sin informaci&oacute;n";
    }

    public function getDocumentoReintegracion(){
        $path = app( )->getBasePath( )."/../../piea-files/recibos/".$this->id."/reintegracion/";
        $dir=array_slice(scandir($path),2);
        return $dir[0];
    }

    public function eliminarItemsRecibo(){
        foreach ($this->items as $item) {
            $item->delete();
        }
    }

    public function cambiarEstado($estado){
        $this->estado=$estado;
        $ret=true;
        //si el recibo esta completamente aprobado entonces se debe realizar el proceso de traspaso de fondos en
        //la cuenta utilizada
        if($this->estado==1){
            $cuenta=$this->codigoPresupuestario;
            $cuenta->saldo+=$this->totalMontoItemsPresupuestos;
            //ahora cargo el total del vale
            $cuenta->saldo-=$this->totalMontoItems;
            $ret=$cuenta->save(false);
        }
        return $ret;
    }

    public function actualizarTotales(){
        $this->total_gastos=$this->totalMontoItems;
        if($this->tipo_recibo==1){
            $this->saldo_favor_deudor=0;
            $this->saldo_reintegrar_caja=0;
            $this->total_igual=$this->total_gastos;
        }else{
            //se calcula el saldo a reintegrar caja
            $total_items=$this->totalMontoItems;
            $total_presupuesto=$this->totalMontoItemsPresupuestos;
            if($total_items<$total_presupuesto){
                $this->saldo_reintegrar_caja=$total_presupuesto-$total_items;
                $this->saldo_favor_deudor=0;
            }
            else if($total_items>$total_presupuesto){
                $this->saldo_reintegrar_caja=0;
                $this->saldo_favor_deudor=$total_items-$total_presupuesto;
            }else
                $this->saldo_reintegrar_caja=0;
            $this->total_igual=$this->monto_vale;
        }
    }

    public static function load($id){
        $recibo=Recibo::model()->findByPk($id);
        if($recibo==null)
            throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
        return $recibo;
    }

}