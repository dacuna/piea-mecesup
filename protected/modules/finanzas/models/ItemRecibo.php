<?php

/**
 * Modelo para la tabla "{{item_recibo}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{item_recibo}}':
 * @property integer $id
 * @property integer $id_recibo
 * @property string $fecha
 * @property string $numero_boleta
 * @property integer $valor
 *
 * Las siguientes son las relaciones disponibles:
 * @property Recibo $idRecibo
 */
class ItemRecibo extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ItemRecibo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{item_recibo}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('fecha, numero_boleta, valor', 'required'),
            array('valor', 'numerical', 'integerOnly' => true),
            array('numero_boleta', 'length', 'max' => 45),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'recibo' => array(self::BELONGS_TO, 'Recibo', 'id_recibo'),
            'itemPresupuesto' => array(self::HAS_ONE, 'ItemPresupuesto', 'id_item_presupuesto'),
            'articulos' => array(self::HAS_MANY, 'ItemBoletaRecibo', 'id_item_recibo'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_recibo' => 'Recibo',
            'fecha' => 'Fecha Compra',
            'numero_boleta' => 'Numero Boleta',
            'valor' => 'Monto'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_recibo', $this->id_recibo);
        $criteria->compare('numero_boleta', $this->numero_boleta, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->fecha = date('Y-m-d', strtotime($this->fecha));
            }
            return true;
        } else
            return false;
    }

    public function getBoleta(){
        $path = app( )->getBasePath( )."/../../piea-files/recibos/".$this->id_recibo."/".$this->id."/";
	    $dir=array_slice(scandir($path),2);
	    return $dir[0];
    }

    public static function load($id){
        $itemRecibo=ItemRecibo::model()->findByPk($id);
        if($itemRecibo==null)
            throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
        return $itemRecibo;
    }

}
