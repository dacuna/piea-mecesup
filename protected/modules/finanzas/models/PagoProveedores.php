<?php

/**
 * Modelo para la tabla "{{pago_proveedores}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{pago_proveedores}}':
 * @property integer $id
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property integer $id_usuario_que_solicita
 * @property integer $monto_factura
 * @property integer $estado
 * @property integer $gastos_envio
 * @property string $fecha_solicitud
 * @property string $fecha_actualizacion
 * @property string $comentario_revisor
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $idUsuarioQueSolicita
 */
class PagoProveedores extends CActiveRecord
{
    public $presupuestosIds=array();
    private $entidad;

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PagoProveedores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{pago_proveedores}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('monto_factura,numero_factura', 'required'),
			array('monto_factura, gastos_envio', 'numerical', 'integerOnly'=>true),
            array('presupuestosIds','ext.ArrayValidator','validator' => 'exist',
                'params' => array(
                    'attributeName' => 'id','className' => 'Presupuesto'
                ),
                'separateParams' => false,
                'allowEmpty'=>false,
                'on'=>'pagoProveedores',
            ),
            array('presupuestosIds','validarMismaCuenta','on'=>'pagoProveedores'),
		);
	}

    public function validarMismaCuenta($attribute,$params)
    {
        if(isset($this->$attribute) || is_array($this->$attribute)){
            $cuentas=array();
            foreach ($this->$attribute as $key=>$presupuesto) {
                $pre=Presupuesto::model()->findByPk($presupuesto);
                array_push($cuentas,$pre->id_cuenta);
            }
            $cuentas=array_unique($cuentas);
            if(count($cuentas)>1){
                $this->addError($attribute,'Los presupuestos deben tener la misma cuenta de origen de fondos.');
            }

        }
    }

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'solicitante' => array(self::BELONGS_TO, 'Persona', 'id_usuario_que_solicita'),
            'presupuestos' => array(self::MANY_MANY, 'Presupuesto', 'tbl_pago_proveedor_presupuesto(id_pago_proveedor,id_presupuesto)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_entidad' => 'Id Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'id_usuario_que_solicita' => 'Id Usuario Que Solicita',
			'numero_factura' => 'N&uacute;mero Factura',
			'monto_factura' => 'Monto Factura',
			'estado' => 'Estado',
			'gastos_envio' => 'Gastos Envio',
			'fecha_solicitud' => 'Fecha Solicitud',
			'fecha_actualizacion' => 'Fecha Actualizacion',
			'comentario_revisor' => 'Comentario Revisor',
		);
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_solicitud',
                'updateAttribute' => 'fecha_actualizacion',
            ),
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
        );
    }

    public function beforeSave(){
        if($this->isNewRecord){
            $this->id_usuario_que_solicita=user()->id;
            $this->id_entidad=$this->entidad->getId();
            $this->tipo_entidad=$this->entidad->getTipoEntidadNumerico();
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        parent::afterFind();
        if (!empty($this->presupuestos))
        {
            foreach ($this->presupuestos as $n => $presupuesto)
                $this->presupuestosIds[] = $presupuesto->id;
        }
    }

    public function getEntidad(){
        if(!isset($this->entidad))
            $this->entidad=new Entidad($this->id_entidad,$this->tipo_entidad);
        return $this->entidad;
    }

    public function setEntidad($entidad){
        $this->entidad=$entidad;
    }

    public function setTipoRecibo($tipo_recibo){
        if($tipo_recibo!=1 && $tipo_recibo!=2)
            throw new CHttpException(500,"Tipo de recibo financiero no v&aacute;lido.");
        $this->tipo_recibo=$tipo_recibo;
    }

    public function getTipoRecibo(){
        if($this->tipo_recibo==1)
            return "Pago a proveedores";
        else
            return "Unidad de abastecimiento";
    }

    public function getTotalMontoItemsPresupuestos()
    {
        $total = 0;
        foreach ($this->presupuestosIds as $presupuesto_id) {
            $presupuesto = Presupuesto::model()->findByPk($presupuesto_id);
            if($presupuesto!=null){
                foreach ($presupuesto->items as $item)
                    $total += $item->precio * $item->unidades;
            }
        }
        return $total;
    }

    public function getItemsPresupuestos(){
        $items=array();
        foreach ($this->presupuestos as $presupuesto)
            foreach ($presupuesto->items as $item)
                $items[]=$item;
        return $items;
    }

    public function getEstadoRecibo(){
        if($this->estado==0 && $this->tipo_recibo!=2)
            return "Pendiente de aprobaci&oacute;n";
        else if($this->estado==0 && $this->tipo_recibo==2)
            return "Sin notificaci&oacute;n de entrega";
        else if($this->estado==-1)
            return "Rechazado";
        else if($this->estado==1)
            return "Aceptado";
        else if($this->estado==2 && $this->tipo_recibo==2)
            return "Notificado como entrega en oficina PIEA";
        else if($this->estado==2 && $this->tipo_recibo!=2)
            return "Utilizado en acci&oacute;n comercial";
        else if($this->estado==3 && $this->tipo_recibo==2)
            return "Recibido en oficina de PIEA";
        else if($this->estado==4 && $this->tipo_recibo==2)
            return "Pendiente de aprobaci&oacute;n";
        else if($this->estado==5)
            return "En espera de comprobante de reintegraci&oacute;n";
        else if($this->estado==6)
            return "En espera de notificaci&oacute;n de recibo de comprobante de reintegraci&oacute;n";
        else
            return "Sin informaci&oacute;n";
    }

    public function getFactura(){
        $path = app( )->getBasePath( )."/../../piea-files/proveedores/".$this->id."/";
        $dir=array_slice(scandir($path),2);
        return $dir[0];
    }

    public static function load($id){
        $recibo=PagoProveedores::model()->findByPk($id);
        if($recibo==null)
            throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
        return $recibo;
    }

}