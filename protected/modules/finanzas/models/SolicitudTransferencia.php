<?php

/**
 * Modelo para la tabla "{{solicitud_transferencia}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{solicitud_transferencia}}':
 * @property integer $id
 * @property integer $entidad_id_origen
 * @property integer $entidad_id_destino
 * @property integer $tipo_entidad_origen
 * @property integer $tipo_entidad_destino
 * @property integer $partida_id_origen
 * @property integer $tipo_partida
 * @property integer $cuenta_id_destino
 * @property integer $tipo_cuenta_destino
 * @property integer $estado
 * @property integer $monto
 * @property string $fecha_solicitud
 * @property string $fecha_accion_realizada
 */
class SolicitudTransferencia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SolicitudTransferencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{solicitud_transferencia}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('entidad_id_origen, entidad_id_destino, tipo_entidad_origen, tipo_entidad_destino, partida_id_origen,
			    tipo_partida, cuenta_id_destino, fecha_solicitud', 'required'),
			array('entidad_id_origen, entidad_id_destino, tipo_entidad_origen, tipo_entidad_destino, partida_id_origen,
			    tipo_partida, cuenta_id_destino, estado', 'numerical', 'integerOnly'=>true),
			array('fecha_accion_realizada', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entidad_id_origen, entidad_id_destino, tipo_entidad_origen, tipo_entidad_destino, partida_id_origen, tipo_partida, cuenta_id_destino, estado, fecha_solicitud, fecha_accion_realizada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entidad_id_origen' => 'Entidad Id Origen',
			'entidad_id_destino' => 'Entidad Id Destino',
			'tipo_entidad_origen' => 'Tipo Entidad Origen',
			'tipo_entidad_destino' => 'Tipo Entidad Destino',
			'partida_id_origen' => 'Partida Id Origen',
			'tipo_partida' => 'Tipo Partida',
			'cuenta_id_destino' => 'Cuenta Id Destino',
			'estado' => 'Estado',
			'fecha_solicitud' => 'Fecha Solicitud',
			'fecha_accion_realizada' => 'Fecha Accion Realizada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entidad_id_origen',$this->entidad_id_origen);
		$criteria->compare('entidad_id_destino',$this->entidad_id_destino);
		$criteria->compare('tipo_entidad_origen',$this->tipo_entidad_origen);
		$criteria->compare('tipo_entidad_destino',$this->tipo_entidad_destino);
		$criteria->compare('partida_id_origen',$this->partida_id_origen);
		$criteria->compare('tipo_partida',$this->tipo_partida);
		$criteria->compare('cuenta_id_destino',$this->cuenta_id_destino);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('fecha_solicitud',$this->fecha_solicitud,true);
		$criteria->compare('fecha_accion_realizada',$this->fecha_accion_realizada,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_solicitud',
                'updateAttribute' => 'fecha_accion_realizada'
            ),
        );
    }

    public static function crearSolicitudTransferencia($entidad_origen,$entidad_destino,$partida_origen,$cuenta_destino,$monto){
        $solicitud=new SolicitudTransferencia;
        $solicitud->entidad_id_origen=$entidad_origen->getId();
        $solicitud->tipo_entidad_origen=$entidad_origen->getTipoEntidadNumerico();
        $solicitud->entidad_id_destino=$entidad_destino->getId();
        $solicitud->tipo_entidad_destino=$entidad_destino->getTipoEntidadNumerico();
        $solicitud->partida_id_origen=$partida_origen[0];
        $solicitud->tipo_partida=$partida_origen[1];
        $solicitud->cuenta_id_destino=$cuenta_destino[0];
        $solicitud->tipo_cuenta_destino=$cuenta_destino[1];
        $solicitud->monto=$monto;
        if($solicitud->save(false)){
            return $solicitud;
        }
        return null;
    }

    public function getEntidadOrigen(){
        $entidad=new Entidad($this->entidad_id_origen,$this->tipo_entidad_origen);
        return $entidad;
    }

    public function getEntidadDestino(){
        $entidad=new Entidad($this->entidad_id_destino,$this->tipo_entidad_destino);
        return $entidad;
    }

    public function getCuentaDestino(){
        return Cuenta::model()->findByPk($this->cuenta_id_destino);
    }
}