<?php
/**
 * Contiene todos los movimientos financieros ejecutados en una cuenta en particular para una entidad en particular.
 *
 * Permite obtener una lista con las entregas, transferencias, solicitudes de presupuestos aceptadas y recibos
 * (recuperaciones de gastos y vales a rendir) realizadas en una cuenta en particular para una entidad en particular.
 * La totalidad de los movimientos son ordenados por la fecha de la transaccion y poseen el siguiente formato:
 * <ul>
 *  <li>ID movimiento</li>
 *  <li>Fecha de la transferencia</li>
 *  <li>Operador a cargo</li>
 *  <li>Entidad de origen</li>
 *  <li>Cuenta de origen</li>
 *  <li>Entidad de destino</li>
 *  <li>Cuenta de destino</li>
 *  <li>Monto</li>
 *  <li>Saldo</li>
 * </ul>
 * Para obtener los movimientos de una cuenta es necesario especificar tanto el id de la cuenta como la entidad a buscar,
 * @example <br />
 *  $movimientos = new MovimientosCuenta($id_cuenta,$entidad);<br />
 *  $movimientos->cargarMovimientos();<br />
 * @version   0.01
 * @since     31/12/2012
 * @author    dacuna <diego.acuna@usm.cl>
 */
class MovimientosCuenta extends CComponent
{
    private $cuenta;
    private $id;
    private $fecha_transferencia;
    private $operador;
    private $origen;
    private $cuenta_origen;
    private $destino;
    private $cuenta_destino;
    private $monto;
    private $saldo;
    private $entidad;
    private $operacion;

    public function __construct($cuenta,$entidad){
        $this->entidad=$entidad;
        $this->cuenta=$cuenta;
    }

    public function cargarMovimientos(){
        $cuenta=$this->cuenta;
        $entidad=$this->entidad;
        $movimientos=array();
        //se deben cargar las entregas, luego las transferencias y luego los cargos en presupuestos y recibos
        $entregas=Entrega::model()->findAll('id_cuenta=:ic AND (id_entidad=:ie and tipo_entidad=:te)
            OR (id_entidad_origen=:ie and tipo_entidad_origen=:te) order by fecha_entrega DESC',array(
            ':ic'=>$cuenta,':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        foreach ($entregas as $entrega) {
            $movimiento=new MovimientosCuenta($cuenta,$entidad);
            $movimiento->setId($entrega->id);
            $movimiento->setFechaTransferencia($entrega->fecha_entrega);
            $movimiento->setOperador($entrega->operador);
            $movimiento->setOrigen($entrega->getEntidadOrigen());
            $movimiento->setCuentaOrigen($entrega->cuenta);
            $movimiento->setDestino($entrega->getEntidad());
            $movimiento->setCuentaDestino($entrega->cuenta);
            $movimiento->setMonto($entrega->monto);
            $movimiento->setOperacion(0);
            $movimientos[]=$movimiento;
        }


        $transferencias=Transferencia::model()->findAll('(id_cuenta_origen=:ic OR id_cuenta_destino=:ic) AND
            ((id_entidad_origen=:ie and tipo_entidad_origen=:te) OR (id_entidad_destino=:ie and tipo_entidad_destino=:te))
              order by fecha_transferencia DESC',array(
            ':ic'=>$cuenta,':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));

        foreach ($transferencias as $transferencia) {
            $movimiento=new MovimientosCuenta($cuenta,$entidad);
            $movimiento->setId($transferencia->id);
            $movimiento->setFechaTransferencia($transferencia->fecha_transferencia);
            $movimiento->setOperador($transferencia->operador);
            $movimiento->setOrigen($transferencia->getEntidadOrigen());
            $movimiento->setCuentaOrigen($transferencia->cuenta_origen);
            $movimiento->setDestino($transferencia->getEntidadDestino());
            $movimiento->setCuentaDestino($transferencia->cuenta_destino);
            $movimiento->setMonto($transferencia->monto);
            $movimiento->setOperacion(0);
            $movimientos[]=$movimiento;
        }

        $presupuestos=Presupuesto::model()->findAll('id_cuenta=:ic AND id_entidad=:ie AND tipo_entidad=:te AND (estado=1 or estado=2)
            order by fecha_accion_realizada DESC',array(
            ':ic'=>$cuenta,':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));

        foreach ($presupuestos as $presupuesto) {
            $movimiento=new MovimientosCuenta($cuenta,$entidad);
            $movimiento->setId($presupuesto->id);
            $movimiento->setFechaTransferencia($presupuesto->fecha_accion_realizada);
            $movimiento->setOperador($presupuesto->solicitante);
            $movimiento->setOrigen($presupuesto->getEntidad());
            $movimiento->setCuentaDestino("Solicitud de Presupuesto");
            $movimiento->setDestino($presupuesto->detalle);
            $movimiento->setCuentaOrigen($presupuesto->cuenta);
            $movimiento->setMonto($presupuesto->totalMontoItems);
            $movimiento->setOperacion(1);
            $movimientos[]=$movimiento;
        }

        $recibos=Recibo::model()->findAll('codigo_presupuestario=:ic AND id_entidad=:ie AND tipo_entidad=:te AND estado=1
            order by fecha_emision_documento DESC',array(
            ':ic'=>$cuenta,':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));

        foreach ($recibos as $recibo) {
            //por cada recibo hay un abono en la cuenta igual al total del presupuesto de dicho recibo
            $movimiento=new MovimientosCuenta($cuenta,$entidad);
            $movimiento->setId('P'.$recibo->id);
            $movimiento->setFechaTransferencia($recibo->fecha_emision_documento);
            $movimiento->setOperador($recibo->solicitante);
            $movimiento->setOrigen($recibo->getEntidad());
            $movimiento->setCuentaDestino($recibo->codigoPresupuestario);
            $movimiento->setDestino($recibo->getEntidad());
            $movimiento->setCuentaOrigen("Presupuesto recibo {$recibo->id}");
            $movimiento->setMonto($recibo->totalMontoItemsPresupuestos);
            $movimiento->setOperacion(3);
            $movimientos[]=$movimiento;

            $movimiento=new MovimientosCuenta($cuenta,$entidad);
            $movimiento->setId($recibo->id);
            $movimiento->setFechaTransferencia($recibo->fecha_emision_documento);
            $movimiento->setOperador($recibo->solicitante);
            $movimiento->setOrigen($recibo->getEntidad());
            $movimiento->setCuentaDestino($recibo->tipoRecibo);
            $movimiento->setDestino($recibo->nota);
            $movimiento->setCuentaOrigen($recibo->codigoPresupuestario);
            $movimiento->setMonto($recibo->monto_vale);
            $movimiento->setOperacion(2);
            $movimientos[]=$movimiento;
        }

        usort($movimientos, "self::sortMovimientos");

        //se deben cargar los saldos
        $i=0;
        $saldo_anterior=0;
        foreach ($movimientos as $movimiento) {
            if($i==0){//el primer movimiento es el primer valor de la cuenta, siempre es el primer saldo
                $movimiento->setSaldo($movimiento->getMonto());
            }
            else{
                if($movimiento->operacion!=0){
                    if($movimiento->operacion==3) //es el cargo de un presupuesto
                        $movimiento->setSaldo($saldo_anterior+$movimiento->getMonto());
                    else //le saque para un presupuesto o un recibo
                        $movimiento->setSaldo($saldo_anterior-$movimiento->getMonto());
                }else{
                    if($movimiento->getCuentaDestino()->id==$cuenta){
                        //es un abono
                        $movimiento->setSaldo($saldo_anterior+$movimiento->getMonto());
                    }else{
                        //es un cargo
                        $movimiento->setSaldo($saldo_anterior-$movimiento->getMonto());
                    }
                }
            }
            $saldo_anterior=$movimiento->getSaldo();
            $i++;
        }

        return array_reverse($movimientos);
    }

    private function sortMovimientos($a, $b)
    {
        if ($a->fecha_transferencia == $b->fecha_transferencia) {
            return 0;
        }
        return ($a->fecha_transferencia < $b->fecha_transferencia) ? -1 : 1;
    }

    public function setCuentaDestino($cuenta_destino)
    {
        $this->cuenta_destino = $cuenta_destino;
    }

    public function getCuentaDestino()
    {
        return $this->cuenta_destino;
    }

    public function setCuentaOrigen($cuenta_origen)
    {
        $this->cuenta_origen = $cuenta_origen;
    }

    public function getCuentaOrigen()
    {
        return $this->cuenta_origen;
    }

    public function setDestino($destino)
    {
        $this->destino = $destino;
    }

    public function getDestino()
    {
        return $this->destino;
    }

    public function setFechaTransferencia($fecha_transferencia)
    {
        $this->fecha_transferencia = $fecha_transferencia;
    }

    public function getFechaTransferencia()
    {
        return $this->fecha_transferencia;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    public function getMonto()
    {
        return $this->monto;
    }

    public function setOperador($operador)
    {
        $this->operador = $operador;
    }

    public function getOperador()
    {
        return $this->operador;
    }

    public function setOrigen($origen)
    {
        $this->origen = $origen;
    }

    public function getOrigen()
    {
        return $this->origen;
    }

    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    public function getSaldo()
    {
        return $this->saldo;
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function getEntidad()
    {
        return $this->entidad;
    }

    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;
    }

    public function getCuenta()
    {
        return $this->cuenta;
    }

    public function setOperacion($operacion)
    {
        $this->operacion = $operacion;
    }

    public function getOperacion()
    {
        return $this->operacion;
    }

}
