<?php

/**
 * Modelo para la tabla "{{item_presupuesto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{item_presupuesto}}':
 * @property integer $id
 * @property integer $id_presupuesto
 * @property string $nombre
 * @property integer $precio
 * @property integer $unidades
 * @property string $prefix_documentos
 *
 * Las siguientes son las relaciones disponibles:
 * @property Presupuesto $idPresupuesto
 */
class ItemPresupuesto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPresupuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{item_presupuesto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('precio, unidades,nombre', 'required'),
			array('precio, unidades', 'numerical','allowEmpty'=>'false','integerOnly'=>true,'max'=>2147483647),
			// Please remove those attributes that should not be searched.
			array('id, id_presupuesto, cantidad, unidades, prefix_documentos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'presupuesto' => array(self::BELONGS_TO, 'Presupuesto', 'id_presupuesto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_presupuesto' => 'Id Presupuesto',
			'nombre' => 'Nombre Item',
			'precio' => 'Precio para el producto seleccionado',
			'unidades' => 'Unidades',
			'prefix_documentos' => 'Prefix Documentos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_presupuesto',$this->id_presupuesto);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('unidades',$this->unidades,true);
		$criteria->compare('prefix_documentos',$this->prefix_documentos,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getArchivos(){
        $path = app( )->getBasePath( )."/../../piea-files/presupuestos/".$this->id_presupuesto."/".$this->id."/";
        return array_slice(scandir($path), 2);
    }

    public function getDetalle(){
        return $this->nombre.' ($'.app()->numberFormatter->formatDecimal($this->precio*$this->unidades).')';
    }
}