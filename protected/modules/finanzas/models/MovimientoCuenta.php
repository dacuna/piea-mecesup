<?php

/**
 * Modelo para la tabla "{{movimiento_cuenta}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{movimiento_cuenta}}':
 * @property integer $id
 * @property string $fecha_transferencia
 * @property integer $id_operador
 * @property integer $id_cuenta_origen
 * @property integer $id_entidad_origen
 * @property integer $tipo_entidad_origen
 * @property integer $id_cuenta_destino
 * @property string $operacion
 * @property integer $id_entidad_destino
 * @property integer $tipo_entidad_destino
 * @property string $detalle
 * @property integer $monto
 * @property integer $saldo
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $idOperador
 * @property Cuenta $idCuentaOrigen
 * @property Cuenta $idCuentaDestino
 */
class MovimientoCuenta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MovimientoCuenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{movimiento_cuenta}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'idOperador' => array(self::BELONGS_TO, 'User', 'id_operador'),
			'idCuentaOrigen' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta_origen'),
			'idCuentaDestino' => array(self::BELONGS_TO, 'Cuenta', 'id_cuenta_destino'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha_transferencia' => 'Fecha Transferencia',
			'id_operador' => 'Id Operador',
			'id_cuenta_origen' => 'Id Cuenta Origen',
			'id_entidad_origen' => 'Id Entidad Origen',
			'tipo_entidad_origen' => 'Tipo Entidad Origen',
			'id_cuenta_destino' => 'Id Cuenta Destino',
			'operacion' => 'Operacion',
			'id_entidad_destino' => 'Id Entidad Destino',
			'tipo_entidad_destino' => 'Tipo Entidad Destino',
			'detalle' => 'Detalle',
			'monto' => 'Monto',
			'saldo' => 'Saldo',
		);
	}

    //$ent_fija es la entidad en donde se esta grabando el movimiento
    public static function grabarMovimiento($ent_fija,$cuenta_fija,$fecha,$operador,$cuenta_origen,$entidad_origen,$monto,
                                            $cuenta_destino=null,$entidad_destino=null,$operacion=null,$detalle=null){
        $nueva=new MovimientoCuenta;
        $nueva->fecha_transferencia=$fecha;
        $nueva->id_operador=$operador;
        $nueva->id_cuenta_origen=$cuenta_origen;
        $nueva->id_entidad_origen=$entidad_origen->getId();
        $nueva->tipo_entidad_origen=$entidad_origen->getTipoEntidadNumerico();
        $nueva->monto=$monto;
        $last_saldo=MovimientoCuenta::obtenerUltimoMovimientoCuenta($cuenta_fija,$ent_fija);
        if($last_saldo)
            $nueva->saldo=$last_saldo->saldo;
        else
            $nueva->saldo=$nueva->monto;
        if($cuenta_destino){
            $nueva->id_cuenta_destino=$cuenta_destino;
        }else{
            $nueva->operacion=$operacion;
        }
        if($entidad_destino){
            $nueva->id_entidad_destino=$entidad_destino->getId();
            $nueva->tipo_entidad_destino=$entidad_destino->getTipoEntidadNumerico();
        }else{
            $nueva->detalle=$detalle;
        }
        return $nueva->save();
    }

    public static function obtenerUltimoMovimientoCuenta($cuenta,$entidad){
        $mov=MovimientoCuenta::model()->find('(id_cuenta_origen=:ic or id_cuenta_destino=:ic) and
            ((id_entidad_origen=:ie and tipo_entidad_origen=:te) or (id_entidad_destino=:ie and tipo_entidad_destino=:te))
             order by fecha_transferencia DESC',array(
            ':ic'=>$cuenta,':ie'=>$entidad->getId(),':te'=>$entidad->getTipoEntidadNumerico()
        ));
        return $mov;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('fecha_transferencia',$this->fecha_transferencia,true);
		$criteria->compare('id_cuenta_origen',$this->id_cuenta_origen);
		$criteria->compare('id_entidad_origen',$this->id_entidad_origen);
		$criteria->compare('tipo_entidad_origen',$this->tipo_entidad_origen);
		$criteria->compare('id_cuenta_destino',$this->id_cuenta_destino);
		$criteria->compare('id_entidad_destino',$this->id_entidad_destino);
		$criteria->compare('tipo_entidad_destino',$this->tipo_entidad_destino);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}