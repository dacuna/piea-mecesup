<?php
/**
 * TransferenciaGeneral: modelo que permite realizar transferencias entre entidades. Corresponde
 * a un modelo de formulario (no ActiveRecord).
 *
 * Las transferencias posibles pueden ser las siguientes:
 * <ul>
 *  <li>Desde una cuenta propia a:
 *      <ul>
 *          <li>Otra cuenta propia</li>
 *          <li>Entrega de fondos</li>/
 *      </ul>
 *  </li>
 *  <li>Desde una entrega propia a:
 *      <ul>
 *          <li>Otra cuenta propia</li>
 *      </ul>
 *  </li>
 * </ul>
 * Cada transferencia va desde una entidad origen a la que el usuario que realiza la transferencia
 * tiene acceso hacia una entidad destino que puede ser <b>cualquier</b> entidad presente en PIE&gt;A.
 * Antes de realizar la transferencia, se debe asignar una entidad de origen de tipo \Entidad y para
 * efectuar la transferencia, se debe utilizar el metodo realizarTransferencia().
 *
 * @example <br />
 *     $transferencia = new TransferenciaGeneral;<br />
 *     $transferencia->setEntidadOrigen(new Entidad($id,$tipo));<br />
 *     if($transferencia->validate()){<br />
 *        return $transferencia->realizarTransferencia();
 * @version   0.1
 * @since     10/18/12
 * @author    dacuna <diego.acuna@usm.cl>
 * @see       \Entidad
 */
class TransferenciaGeneral_legacy extends CFormModel
{
    public $cuenta_origen;
    public $tipo_cuenta_origen;
    public $cuenta_destino;
    public $entidad_id_destino;
    public $tipo_entidad_destino;
    public $monto;

    private $entidad_origen;
    private $entidad_destino;

    /**
     * Inicializa el modelo, en particular se cargan los handlers para los eventos de notificacion
     * y solicitud de aprobacion de entregas y transferencias en el MailMan.
     *
     * @return  void
     *
     * @since   24-10-2012
     * @see     \MailMan
     *
     */
    public function init(){
        parent::init();
        $this->onNotificarEntrega=array(app()->mailman,'notificacionHandler');
        $this->onSolicitarAprobacionTransferencia=array(app()->mailman,'notificacionHandler');
    }

    public function rules()
    {
        return array(
            array('cuenta_origen,cuenta_destino,entidad_id_destino,tipo_entidad_destino,monto,tipo_cuenta_origen', 'required'),
            array('cuenta_origen','cuentaAccesoValidator'),
            array('cuenta_destino','cuentaExistenteValidator'),
            array('monto,entidad_id_destino,tipo_entidad_destino', 'numerical', 'integerOnly' => true),
            array('tipo_cuenta_origen', 'in', 'range' => array('cuenta', 'entrega')),
            array('monto', 'noMayorValidator'),
            array('tipo_cuenta_origen,tipo_cuenta_destino', 'safe')
        );
    }

    /**
     * realizarTransferencia: Permite realizar una transferencia entre dos entidades, para efectos
     * de entidades, las tranferencias se pueden realizar desde:
     * <ul>
     *      <li>Una cuenta propia de la entidad</li>
     *      <li>Una cuenta a la que la entidad tenga acceso mediante una entrega</li>
     * </ul>
     * y pueden llegar hacia:
     * <ul>
     *      <li>Una cuenta propia de la entidad</li>
     *      <li>Tener caracteristica de entrega (similar al caso comun de una entrega de fondos)</li>
     * </ul>
     * Como transferencia tambien se debe almacenar la entidad y el tipo de entidad que realizo la
     * transferencia, esto debido a que en una transferencia comun (de cuenta a cuenta) no se guarda
     * esta informacion dado que no es necesaria (se puede deducir desde la id_cuenta), pero en este tipo
     * de transferencias puede ser que se transfiera desde una entrega a una cuenta y en es caso hay que
     * almacenar dicha informacion.
     *
     * @return  bool true si se realizo la transferencia con exito, false en caso contrario.
     *
     * @since   24-10-2012
     *
     */
    public function realizarTransferencia()
    {
        $usuario=Persona::model()->findByPk(user()->id);
        if ($this->tipo_cuenta_origen == 'cuenta') {
            $cuenta = Cuenta::model()->findByPk($this->cuenta_origen);
            //se esta haciendo una entrega
            if ($this->cuenta_destino == -1) {
                //se descuenta el monto de la entrega original y se crea una nueva entrega
                $nueva_entrega = new Entrega;
                $nueva_entrega->id_cuenta = $cuenta->id;
                $nueva_entrega->monto = $this->monto;
                $nueva_entrega->id_entidad = $this->entidad_id_destino;
                $nueva_entrega->tipo_entidad = $this->tipo_entidad_destino;
                if ($nueva_entrega->save(false)) {
                    //notifico al administrador
                    $usuario=Persona::model()->findByPk(user()->id);
                    $this->onNotificarEntrega(new CEvent($this,array('tipo'=>'notificar_entrega_entidad','entidad'=>$this->getEntidadDestino(),'origen'=>$this->getEntidadOrigen(),'usuario'=>$usuario)));
                    return true;
                } else {
                    return false;
                }
            }
            //se esta haciendo una transferencia
            else{
                //en este caso se descuenta el dinero de la cuenta original pero el administrador debe aceptar la
                //transferencia, en ese momento se le suma el monto a la cuenta transferida. Se debe crear una solicitud de transferencia
                $solicitud=SolicitudTransferencia::crearSolicitudTransferencia($this->getEntidadOrigen(),$this->getEntidadDestino(),
                    array($cuenta->id,'0'),$this->cuenta_destino,$this->monto);
                if($solicitud!=null){
                    $cuenta->saldo-=$this->monto;
                    if($cuenta->save(false)){
                        $this->onSolicitarAprobacionTransferencia(new CEvent($this,array('tipo'=>'solicitar_aprobacion_transferencia','solicitud'=>$solicitud,'usuario'=>$usuario)));
                        return true;
                    }
                }
                return false;
            }
        }else if($this->tipo_cuenta_origen=='entrega'){
            $entrega=Entrega::model()->findByPk($this->cuenta_origen);
            //se esta haciendo una entrega
            if ($this->cuenta_destino == -1) {
                //se descuenta el monto de la entrega original y se crea una nueva entrega
                $entrega->saldo-=$this->monto;
                $nueva_entrega = new Entrega;
                $nueva_entrega->id_cuenta = $entrega->cuenta->id;
                $nueva_entrega->monto = $this->monto;
                $nueva_entrega->id_entidad = $this->entidad_id_destino;
                $nueva_entrega->tipo_entidad = $this->tipo_entidad_destino;
                //todo: aqui se puede utilizar una transaccion, es buena oportunidad para aprender como funcionan
                if ($nueva_entrega->save(false) && $entrega->save(false)) {
                    //notifico al administrador
                    $this->onNotificarEntrega(new CEvent($this,array('tipo'=>'notificar_entrega_entidad','entidad'=>$this->getEntidadDestino(),'origen'=>$this->getEntidadOrigen(),'usuario'=>$usuario)));
                    return true;
                } else {
                    return false;
                }
            }
            //se esta haciendo una transferencia
            else{
                //en este caso se descuenta el dinero de la entrega original pero el administrador debe aceptar la
                //transferencia, en ese momento se le suma el monto a la cuenta transferida.
                $solicitud=SolicitudTransferencia::crearSolicitudTransferencia($this->getEntidadOrigen(),$this->getEntidadDestino(),
                    array($entrega->id,'1'),$this->cuenta_destino,$this->monto);
                if($solicitud!=null){
                    $entrega->saldo-=$this->monto;
                    if($entrega->save(false)){
                        $this->onSolicitarAprobacionTransferencia(new CEvent($this,array('tipo'=>'solicitar_aprobacion_transferencia','solicitud'=>$solicitud,'usuario'=>$usuario)));
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function noMayorValidator($attribute, $params)
    {
        //se verifica que el monto de la entrega no sea mayor que el saldo de la cuenta
        if (isset($this->cuenta_origen) && !empty($this->cuenta_origen)) {
            if ($this->tipo_cuenta_origen == 'cuenta') {
                $cuenta = Cuenta::model()->findByPk($this->cuenta_origen);
                if ($cuenta->id_entidad == 0) //es la cuenta maestra
                    return;
                if ($cuenta->saldo < $this->monto)
                    $this->addError($attribute, 'El monto de la transferencia no puede sobrepasar al saldo de la cuenta seleccionada');
            } else if ($this->tipo_cuenta_origen == 'entrega') {
                $entrega = Entrega::model()->findByPk($this->cuenta_origen);
                if ($entrega->saldo < $this->monto)
                    $this->addError($attribute, 'El monto de la transferencia no puede sobrepasar al saldo de la cuenta seleccionada');
            }
        }
    }

    public function cuentaAccesoValidator($attribute,$params){
        if (isset($this->cuenta_origen) && !empty($this->cuenta_origen)) {
            //se debe verificar que la cuenta de origen sea seleccionable por la entidad de origen
            $cuentas=Cuenta::getCuentasConAccesoYSaldo($this->getEntidadOrigen()->getId(),$this->getEntidadOrigen()->getTipoEntidadNumerico());
            foreach ($cuentas as $cuenta) {
                if($cuenta[1]=='entrega' && $cuenta[0]->id==$this->$attribute)
                    return;
                else if($cuenta[1]=='cuenta' && $cuenta[0]->id==$this->$attribute)
                    return;
            }
            $this->addError($attribute, 'Debes seleccionar una cuenta de origen v&aacute;lida.');
        }
        return;
    }

    public function cuentaExistenteValidator($attribute,$params){
        if (isset($this->$attribute) && !empty($this->$attribute)) {
            if($this->$attribute==-1)
                return;
            $cuenta=Cuenta::model()->findByPk($this->$attribute);
            if($cuenta==null)
                $this->addError($attribute, 'Debes seleccionar una cuenta de destino v&aacute;lida.');
            return;
        }
    }

    public function setEntidadDestino($entidad_destino)
    {
        $this->entidad_destino = $entidad_destino;
    }

    public function getEntidadDestino()
    {
        if(!isset($this->entidad)){
            $entidad=new Entidad($this->entidad_id_destino,$this->tipo_entidad_destino);
            $this->entidad_destino=$entidad;
        }
        return $this->entidad_destino;
    }

    public function setEntidadOrigen($entidad_origen)
    {
        $this->entidad_origen = $entidad_origen;
    }

    public function getEntidadOrigen()
    {
        return $this->entidad_origen;
    }

    /**
     * onNotificarEntrega: lanza el evento de notificacion de una entrega
     * desde una entidad hacia otra. Dicha notificacion va dirigida al
     * administrador de la plataforma.
     *
     * @param   CEvent $event El evento a lanzar
     *
     * @return  void
     *
     * @since   21-10-2012
     *
     */
    public function onNotificarEntrega($event)
    {
        $this->raiseEvent('onNotificarEntrega', $event);
    }

    /**
     * onSolicitarAprobacionTransferencia: lanza el evento de notificacion de solicitud de una
     * transferencia desde una entidad hacia otra. Dicha notificacion va dirigida hacia el
     * administrador de la plataforma.
     *
     * @param   CEvent $event El evento a lanzar
     *
     * @return  void
     *
     * @since   21-10-2012
     *
     */
    public function onSolicitarAprobacionTransferencia($event){
        $this->raiseEvent('onSolicitarAprobacionTransferencia',$event);
    }

}
