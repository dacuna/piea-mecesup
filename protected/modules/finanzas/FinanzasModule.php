<?php
/**
 * El modulo de finanzas contiene la funcionalidad que permite el manejo de cuentas en las iniciativas
 * y proyectos del sistema.
 *
 * Este modulo permite que cada responsable de una entidad pueda manejar los dineros que les son asignados
 * a sus entidades a traves de fondos o asignaciones desde PIEA. Para esto, existen cuentas dentro del
 * sistema que representan ya sea cuentas financieras reales o cuentas simuladas solo dentro del sistema.
 * El manejo de los fondos en las entidades provee a estas de comodidades como: autogeneracion de documentos
 * de vale a rendir y rendicion de cuentas, autogeneracion de documentacion para solicitar cotizaciones de
 * gastos a la unidad de finanzas, etc.
 *
 * @package   modules.finanzas
 * @version   1.0
 * @since     2013-11-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class FinanzasModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
	{
		$this->setImport(array(
			'finanzas.models.*',
			'finanzas.components.*',
		));
	}

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
