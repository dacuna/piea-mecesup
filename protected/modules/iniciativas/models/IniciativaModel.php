<?php
/**
 * Clase que implementa metodos para el modelo @link{Iniciativa} que solo son necesarios en
 * el modulo de iniciativas.
 *
 * Se decidio extender la clase Iniciativa con el objetivo de no contaminar la clase general de
 * iniciativas con metodos que no son necesarios de ser compartidos en otros modulos.
 *
 * @category  Controllers
 * @package   modules.iniciativas.models
 * @version   1.0
 * @since     2012-07-12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class IniciativaModel extends Iniciativa
{
    /**
     * Variable que almacena el archivo de estatutos que se subio al sistema.
     * @var string
     */
    public $estatutos_subido;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('nombre_completo, nombre_abreviado, sigla, fecha_creacion', 'required'),
            array('nombre_completo', 'length', 'max' => 255),
            array('nombre_abreviado, email, direccion', 'length', 'max' => 45),
            array('sigla', 'length', 'max' => 10),
            //hay dos validadores de sigla ya que las siglas son unicas tanto para iniciativas como para proyectos
            array('sigla', 'unique', 'className' => 'Proyecto', 'attributeName' => 'sigla'),
            array('sigla', 'unique'),
            array('campus', 'length', 'max' => 40),
            array('telefono', 'length', 'max' => 15),
            array('descripcion, objetivos_generales, objetivos_especificos, mision, vision', 'safe'),
            //archivos
            array('estatutos_subido', 'file', 'types' => 'pdf', 'allowEmpty' => true, 'maxSize' => 2 * 1024 * 1024),
            array('logo', 'file', 'types' => 'svg', 'allowEmpty' => true),
            array('foto_equipo', 'ImageValidator', 'allowEmpty' => true),
            //search
            array('nombre_completo,nombre_abreviado,sigla,email', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['estatutos_subido'] = 'Estatutos';
        return $labels;

    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('nombre_completo', $this->nombre_completo, true);
        $criteria->compare('nombre_abreviado', $this->nombre_abreviado, true);
        $criteria->compare('sigla', $this->sigla, true);
        $criteria->compare('email', $this->email, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Behaviors del modelo.
     *
     * @return  array behaviors del modelo
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_ingreso',
                'updateAttribute' => 'fecha_actualizacion',
            ),
            'dataProvider' => array(
                'class' => 'application.components.Behaviors.DynamicDataProviderBehavior',
            ),
        );
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente setea la fecha de creacion
     * de la iniciativa y asigna un rol piea.
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (isset($this->fecha_creacion) && !empty($this->fecha_creacion))
                $this->fecha_creacion = date('Y-m-d', strtotime($this->fecha_creacion));
            if ($this->isNewRecord)
                $this->rol_piea = RolPiea::model()->generarRol();
            return true;
        } else
            return false;
    }

    /**
     * Handler para el evento afterSave del modelo. Basicamente actualiza el generador de roles PIEA.
     */
    public function afterSave()
    {
        parent::afterSave();
        if ($this->isNewRecord)
            RolPiea::model()->actualizarGenerador();
    }

    /**
     * Retorna el usuario que es el coordinador actual.
     *
     * @return  mixed @link{Persona} si existe jefe de proyecto, null en caso contrario
     */
    public function getCoordinador()
    {
        //se busca el coordinador de esta iniciativa en la tabla tbl_lookup_iniciativa
        $lookup = LookupIniciativa::model()->find('nombre_item=:n_item and iniciativa_id=:iid and activo=1', array(
            ':n_item' => 'coordinador_iniciativa', ':iid' => $this->id
        ));

        if ($lookup != null)
            return $lookup->usuario;

        return null;
    }

    /**
     * getCargoDeUsuario: Obtiene el cargo que posee el usuario de id $user_id dentro de la iniciativa
     * especificado por $iniciativa_id.
     *
     * @param   int  $iniciativa_id ID de la iniciativa donde se busca el cargo del usuario
     * @param   int  $user_id     ID del usuario al que se le busca el cargo dentro de la iniciativa especificada
     *
     * @return  Mixed  String con el nombre del cargo en caso de que el usuario posea un cargo, null en caso contrario.
     *
     * @since   16-10-2012
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public static function getCargoDeUsuario($iniciativa_id, $user_id)
    {
        $lookup = LookupIniciativa::model()->find('iniciativa_id=:ii and user_id=:ui and activo=1 and es_cargo=1',
            array(':ii' => $iniciativa_id, ':ui' => $user_id));
        if ($lookup != null)
            return $lookup;
        return null;
    }

    /**
     * Retorna un array con los cargos de todos los usuarios que son integrantes de la iniciativa
     *
     * @return  array array con los cargos de los usuarios
     */
    public function getCargos()
    {
        $cargos = array();
        foreach ($this->usuarios as $integrante) {
            $cargo = IniciativaModel::getCargoDeUsuario($this->id, $integrante->id);
            if ($cargo != null) {
                $item = am()->getAuthItem($cargo->nombre_item);
                array_push($cargos, array($item, $cargo));
            }
        }
        //obtengo tambien los cargos asignados a personas nulas
        $nulos = LookupIniciativa::model()->findAll('iniciativa_id=:ii and asignacion_nula=1', array(':ii' => $this->id));
        foreach ($nulos as $nulo) {
            $item = am()->getAuthItem($nulo->nombre_item);
            array_push($cargos, array($item, $nulo));
        }

        return $cargos;
    }

}
