<?php
/**
 * LoginForm class, representa el modelo de ingreso al sistema.
 *
 * Provee el formulario y metodos para autenticar a los
 * usuarios en la plataforma. Es utilizado por la accion
 * login del controlador AutenticateController.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package iniciativas.models
 * @since 1.0
 */
class CambiarLogoForm extends CFormModel
{
    /**
     * Archivo que contiene el logo
     * @var string
     */
    public $logo;

    /**
     * Logo antiguo de la iniciativa
     * @var string
     */
    public $logo_antiguo;

    /**
     * Iniciativa donde se cambiara el logo.
     * @var IniciativaModel
     */
    public $iniciativa;

    /**
     * Reglas de validacion para el modelo.
     * Las reglas corresponden a que el nombre de usuario y
     * password deben existir y ser una combinacion valida de
     * un usuario en la db.
     */
    public function rules()
    {
        return array(
            array('logo', 'file', 'types' => 'svg', 'allowEmpty' => false),
            array('iniciativa', 'exist', 'attributeName' => 'id', 'className' => 'IniciativaModel', 'allowEmpty' => false)
        );
    }

    /**
     * Labels de cada atributo del modelo.
     */
    public function attributeLabels()
    {
        return array(
            'logo' => 'Logo iniciativa'
        );
    }

    /**
     * Permite guardar el nuevo logo en el sistema.
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function save()
    {
        $this->logo = CUploadedFile::getInstance($this, 'logo');
        $logo_enviar = time() . '_' . $this->logo;
        $dir = YiiBase::getPathOfAlias('webroot') . '/logos/' . $this->iniciativa;
        if (!is_dir($dir))
            mkdir($dir);
        $valid = $this->logo->saveAs($dir . '/' . $logo_enviar);
        //se actualiza el campo logo del perfil del usuario
        $total = 0;
        if ($valid)
            $total = IniciativaModel::model()->updateByPk($this->iniciativa, array('logo' => $logo_enviar));
        if ($valid && $total > 0)
            return true;
        return false;
    }

}
