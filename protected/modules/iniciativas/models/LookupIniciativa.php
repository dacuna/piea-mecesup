<?php

/**
 * Modelo para la tabla "{{lookup_iniciativa}. Este modelo representa un permiso que tiene un
 * usuario dentro de una iniciativa. Es un add-on para manejar de manera mas sencilla el sistema
 * de permisos del framework Yii. Vease la documentacion tecnica en seccion "Autorizacion y
 * autenticacion".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{lookup_iniciativa}}':
 * @property integer $nombre_item
 * @property integer $iniciativa_id
 * @property integer $user_id
 * @property integer $activo
 * @property string  $cargo_que_asigno
 * @property integer $es_cargo
 * @property integer $asignacion_nula
 *
 * Las siguientes son las relaciones disponibles:
 * @property Iniciativa $iniciativa
 * @property User $user
 */
class LookupIniciativa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LookupIniciativa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{lookup_iniciativa}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'iniciativa_id'),
			'usuario' => array(self::BELONGS_TO, 'Persona', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre_item' => 'Nombre Item',
			'iniciativa_id' => 'Iniciativa',
			'user_id' => 'Usuario',
		);
	}

    /**
     * Permite buscar segun nombre de item, id de iniciativa y id de usuario dentro del modelo.
     *
     * @return  CActiveDataProvider data provider con los datos encontrados
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('nombre_item',$this->user_id);
        $criteria->compare('iniciativa_id',$this->iniciativa_id);
        $criteria->compare('user_id',$this->user_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Agregar un lookup de un permiso a un usuario en una iniciativa en particular.
     *
     * @param string $nombre_item permiso a asignar
     * @param int $user_id ID del usuario
     * @param int $iniciativa_id ID de la iniciativa
     *
     * @return  mixed LookupIniciativa si fue creado con exito, null en caso contrario
     */
    public static function crearLookupIniciativa($nombre_item,$user_id,$iniciativa_id){
        $lookup=LookupIniciativa::model()->find('nombre_item=:ni and iniciativa_id=:ii and user_id=:ui',array(
            ':ni'=>$nombre_item,
            ':ii'=>$iniciativa_id,
            ':ui'=>$user_id
        ));
        //ya existe, con 99% de probabilidad este cargo esta inactivo, se debe activar
        if($lookup!=null){
            $lookup->activo=1;
            if($lookup->save())
                return $lookup;
        }
        //sino, debo crearlo
        $integrante=new LookupIniciativa();
        $integrante->nombre_item=$nombre_item;
        $integrante->iniciativa_id=$iniciativa_id;
        $integrante->user_id=$user_id;
        if($integrante->save())
            return $integrante;
        return null;
    }

    /**
     * Retorna un listado con los cargos de un usuario en todas sus iniciativas..
     *
     * @param int $user_id ID del usuario
     *
     * @return  array array de LookupIniciativa
     */
    public static function getCargosDeUsuario($user_id){
        return LookupIniciativa::model()->findAll('user_id=:ui and activo=1 and es_cargo=1',array(
            ':ui'=>$user_id
        ));

    }

}
