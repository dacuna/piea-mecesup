<?php
/**
 * InvitarCoordinadorForm class, formulario para invitar a un usuario
 * a coordinar una iniciativa.
 *
 * Provee el formulario y metodos para invitar a un usuario a ser
 * el coordinador de una iniciativa. Realiza todas las validaciones
 * correspondientes sobre los modelos referenciados.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package iniciativas.models
 * @since 1.0
 */
class InvitarCoordinadorForm extends CFormModel
{
    /**
     * @var int id_usuario id del usuario a invitar.
     */
    public $id_usuario;
    /**
     * @var int id_iniciativa id de la iniciativa a la que se invita un coordinador.
     */
    public $id_iniciativa;

    /**
     * Reglas de validacion para el modelo.
     * Las reglas corresponden a que el nombre de usuario y
     * password deben existir y ser una combinacion valida de
     * un usuario en la db.
     */
    public function rules()
    {
        return array(
            array('id_usuario,id_iniciativa', 'numerical', 'allowEmpty' => false, 'integerOnly' => true, 'message' => 'Debes ingresar un {attribute} v&aacute;lido'),
            //verifico la existencia del usuario y de la iniciativa enviadas
            array('id_usuario', 'exist', 'className' => 'Persona', 'attributeName' => 'id', 'allowEmpty' => false),
            array('id_iniciativa', 'exist', 'className' => 'Iniciativa', 'attributeName' => 'id', 'allowEmpty' => false),
        );
    }

    /**
     * Labels de cada atributo del modelo.
     */
    public function attributeLabels()
    {
        return array(
            'id_usuario' => 'Usuario',
            'id_iniciativa' => 'Iniciativa'
        );
    }

    /**
     * Enviar la invitacion para ser coordinador de una iniciativa.
     * @todo Refactorizar para que utilice el sistema de eventos del MailMan
     */
    public function enviarInvitacion()
    {
        $iniciativa = Iniciativa::model()->findByPk($this->id_iniciativa);
        $usuario = Persona::model()->findByPk($this->id_usuario);
        $mailman = Yii::app()->mailman;
        $asunto = 'Solicitud de coordinaci&oacute;n iniciativa ' . $iniciativa->nombre_abreviado;
        $mensaje = new MensajeInvitacionCoordinacion($usuario, $iniciativa);
        $mailman->crearMensaje($asunto, 'temp');
        $mailman->setPublisher(Yii::app()->user);
        $mailman->setSuscriber($usuario);
        $mailman->setRelation($iniciativa->id, 'iniciativa');
        $id_mensaje = $mailman->enviarMensaje();
        $mailman->actualizarMensaje($id_mensaje, $mensaje->mensaje($id_mensaje));

        //se crea la invitacion en la base de datos
        $invitacion = new Invitacion;
        $invitacion->mensaje_id = $id_mensaje;
        $invitacion->usuario_id = $this->id_usuario;
        $invitacion->entidad_id = $this->id_iniciativa;
        $invitacion->tipo_invitacion = 'coordinacion_iniciativa';
        $invitacion->usuario_invito_id=user()->id;
        $invitacion->tipo_entidad = 0; //0 quiere decir iniciativa
        $invitacion->save(false);
    }

}
