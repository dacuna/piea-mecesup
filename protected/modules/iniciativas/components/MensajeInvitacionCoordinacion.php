<?php
/**
 * @todo Refactorizar y utilizar el mailman con eventos!
 */
class MensajeInvitacionCoordinacion
{
  private $usuario;
  private $iniciativa;
  
  public function __construct($usuario,$iniciativa)
  {
    $this->usuario=$usuario;
    $this->iniciativa=$iniciativa;
  }
  
  public function  mensaje($id)
  {
    $mensaje='Estimado '.$this->usuario->nombres.':<br><br>Has sido invitado a formar parte';
    $mensaje.=' de la coordinaci&oacute;n de la iniciativa '.$this->iniciativa->nombre_abreviado;
    $mensaje.='. Para aceptar la invitaci&oacute;n presiona en el bot&oacute;n "Aceptar invitaci&oacute;n"';
    $mensaje.=', para rechazarla presiona en "Rechazar Invitaci&oacute;n".<br><br>';
    $mensaje.='Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/iniciativas/gestionIntegrantes/aceptarInvitacion',array('id'=>$id)).'" class="nice radius small button">Aceptar Invitaci&oacute;n</a> ';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/iniciativas/gestionIntegrantes/rechazarSolicitud',array('id'=>$id)).'" class="nice radius small red button">Rechazar Invitaci&oacute;n</a> ';
    
    return $mensaje;
  }
}