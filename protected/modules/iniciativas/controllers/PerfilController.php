<?php
/**
 * Controlador que contiene las acciones necesarias para desplegar toda la informacion
 * de una iniaciativa en particular.
 *
 * El presente controlador posee acciones para visualizar a los integrantes de una iniciativa,
 * entre otras cosas (por definir). Cada accion puede ser ejecutada por
 * los usuarios que posean los permisos necesarios para visualizar la accion (pueden ser
 * distintos por accion). Lo importante a notar es que no cualquier usuario puede observar
 * la informacion de una iniciativa.
 *
 * @category  Controller
 * @package   modules.iniciativas.controllers
 * @version   1.0
 * @since     2012-08-21
 * @author    dacuna <diego.acuna@usm.cl>
 */
class PerfilController extends Controller
{
    /**
     * Layout utilizado por el controlador (por defecto aplicado a todas las acciones)
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Permite para el usuario que es coordinador de la iniaciativa el cambiar el logo asociado a esta.
     * Por requerimientos tecnicos emanados desde PIEA se solicito que el logo debe estar si o si en
     * formato svg. Esto ocasiona problemas para la visualizacion del logo en navegadores antiguos, para
     * resolver esto se utiliza una libreria javascript como fallback pero por lo visto el funcionamiento
     * no es 100% bueno.
     */
    public function actionCambiarLogo()
    {
        $imagen = new CambiarLogoForm;
        if (isset($_POST['CambiarLogoForm'])) {

            $imagen->attributes = $_POST['CambiarLogoForm'];
            if ($imagen->validate()) {
                if (Yii::app()->user->checkAccess('coordinador_iniciativa', array('iniciativa' => $imagen->iniciativa))) {
                    $iniciativa = IniciativaModel::model()->findByPk($imagen->iniciativa);
                    $imagen->logo_antiguo = $iniciativa->logo;
                    if ($imagen->save())
                        Yii::app()->user->setFlash('success', 'Se ha actualizado correctamente el logo de la iniciativa.');
                    else
                        Yii::app()->user->setFlash('error', 'Ha ocurrido un error al actualizar el logo de la iniciativa. Asegurate que el formato del archivo sea svg.');
                }
            } else {
                Yii::app()->user->setFlash('error', 'Ha ocurrido un error al actualizar el logo de la iniciativa. Asegurate que el formato del archivo sea svg.');
            }

            $this->redirect(array('/iniciativas/crudIniciativas/ver', 'id' => $imagen->iniciativa));
        }
        $this->redirect(array('/'));
    }

    /**
     * Accion que permite visualizar los integrantes de una iniciativa. Si quien visualiza esta pagina
     * no es miembro de la iniciativa entonces solo podra ver a los integrantes que explicitamente
     * hayan marcado la opcion de aparecer en los listados publicos de la iniciativa. El permiso
     * minimo para ver el listado completo de integrantes es ser un integrante de la iniciativa.
     *
     * @param int $id ID de la iniciativa
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionIntegrantes($id)
    {
        $model = IniciativaModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if (!Yii::app()->user->checkAccess('integrante_iniciativa', array('iniciativa' => $model->id)))
            throw new CHttpException(403, 'No tienes los permisos necesarios para ver esta p&aacute;gina.');

        $this->render('integrantes', array(
            'model' => $model
        ));
    }

}