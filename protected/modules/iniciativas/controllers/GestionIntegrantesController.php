<?php
/**
 * Controlador que contiene la logica para invitar y aceptar/rechazar invitaciones de
 * membresia dentro de una iniciativa.
 *
 * @package   modules.iniaciativas.controllers
 * @version   1.0
 * @since     2012-07-14
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo al 07 de agosto de 2013 este controlador esta sujeto a grandes refactorizaciones
 */
class  GestionIntegrantesController extends Controller
{
    /**
     * Layout por defecto del controladr.
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Accion que permite que un usuario externo a una iniciativa solicite
     * una membresia dentro de esta. Para esto se envia una notificacion
     * al coordinador para que acepte o rechaze la membresia.
     *
     * @param int id id de la iniciativa a la que se solicita membresia
     * @throws CHttpException
     */
    public function actionSolicitarMembresia($id)
    {
        $iniciativa = IniciativaModel::model()->findByPk($id);
        if ($iniciativa == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $usuario_id = Yii::app()->user->id;
        $solicitud = new SolicitudMembresia;
        $solicitud->usuario_id = $usuario_id;
        $solicitud->iniciativa = $iniciativa;
        if ($solicitud->enviarSolicitud())
            Yii::app()->user->setFlash('success', 'Tu solicitud ha sido enviada con &eacute;xito. El coordinador de la iniciativa te responder&aacute; a la brevedad.');
        else
            Yii::app()->user->setFlash('error', 'Ha ocurrido un error al crear la solicitud. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/iniciativas/crudIniciativas/explorar', 'id' => $iniciativa->id));
    }

    /**
     * Accion que permite a un coordinador de iniciativa aceptar una solicitud
     * de un usuario para formar parte del equipo de la iniciativa.
     *
     * @param int id id del mensaje que contiene la solicitud
     * @throws CHttpException
     */
    public function actionAceptarSolicitud($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        $iniciativa = IniciativaModel::model()->findByPk($mensaje->id_relacion);
        if ($iniciativa == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que esta accion solo la pueda ejecutar el coordinador de la iniciativa
        if (!Yii::app()->user->checkAccess('coordinador_iniciativa', array('iniciativa' => $iniciativa->id)))
            throw new CHttpException(500, 'No tienes los permisos suficientes para acceder a esta p&aacute;gina.');

        //valido tambien al usuario
        $usuario = Persona::model()->findByPk($mensaje->emisor);
        if ($usuario == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        //ok, creo entonces al nuevo integrante
        $auth = Yii::app()->authManager;
        //verifico que no sea integrante de otra iniciativa, si lo es entonces ya tiene el permiso asignado
        if ($auth->getAuthAssignment("integrante_iniciativa", $usuario->id) == null) {
            $admin = $auth->assign('integrante_iniciativa', $usuario->id);
        }
        //seteo el lookup de la iniciativa correspondiente
        $lookup = new LookupIniciativa;
        $lookup->nombre_item = 'integrante_iniciativa';
        $lookup->iniciativa_id = $iniciativa->id;
        $lookup->user_id = $usuario->id;
        $lookup->save(false);

        //se debe tambien ingresar el integrante nuevo
        $integrante = new IntegranteIniciativa;
        $integrante->iniciativa_id = $iniciativa->id;
        $integrante->user_id = $usuario->id;
        $integrante->save(false);

        //tambien debo marcar el mensaje como contestado y leido
        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Aceptar solicitud';
        $mensaje->save(false);

        //en este punto all esta correcto, el usuario ya pertenece a la iniciativa
        Yii::app()->user->setFlash('success', 'Se ha aceptado correctamente al usuario ' . $usuario->nombreCompleto . ' como integrante en la iniciativa ' . $iniciativa->nombre_abreviado);
        $this->redirect(array('/buzon/inbox/index'));
    }

    /**
     * Accion que ejecuta la logica cuando un usuario acepta la invitacion
     * a coordinar una iniciativa.
     *
     * @param int id la id de la notificacion de invitacion a coordinacion de iniciativa
     * @throws CHttpException
     */
    public function actionAceptarInvitacion($id)
    {
        //se obtiene el mensaje correspondiente
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || $mensaje->destinatario != $usuario_id)
            throw new CHttpException(403, 'No tienes los permisos para aceptar esta invitaci&oacute;n. Tu acci&oacute;n ser&aacute; notificada al administrador');

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        //verifico que efectivamente corresponde a una invitacion a coordinar iniciativa, lo contrario
        //nunca deberia pasar pero siempre hay que estar paranoic_mode ON
        if ($mensaje->tipo_relacion != 'iniciativa')
            throw new CHttpException(500, 'Solicitud incorrecta. Por favor, no vuelva a realizar la misma petici&oacute;n.');

        $iniciativa = Iniciativa::model()->findByPk($mensaje->id_relacion);
        //nuevamente paranoic_mode ON
        if ($iniciativa == null)
            throw new CHttpException(500, 'Solicitud incorrecta. Por favor, no vuelva a realizar la misma petici&oacute;n.');

        //ok, se procesa el ingreso del nuevo coordinador al usuario logeado
        $auth = Yii::app()->authManager;
        //verifico que no sea coordinador de otra iniciativa, si lo es entonces ya tiene el permiso asignado
        if ($auth->getAuthAssignment("coordinador_iniciativa", $usuario_id) == null) {
            $admin = $auth->assign('coordinador_iniciativa', $usuario_id);
        }
        //seteo el lookup de la iniciativa correspondiente
        $lookup = new LookupIniciativa;
        $lookup->nombre_item = 'coordinador_iniciativa';
        $lookup->iniciativa_id = $iniciativa->id;
        $lookup->user_id = $usuario_id;
        $lookup->save(false);

        //se debe tambien ingresar el integrante nuevo
        $integrante = new IntegranteIniciativa;
        $integrante->iniciativa_id = $iniciativa->id;
        $integrante->user_id = $usuario_id;
        $integrante->save(false);

        //se debe obtener la invitacion correspondiente para marcarla como aceptada
        $invitacion = Invitacion::model()->findByPk($mensaje->id);
        $invitacion->aceptada = 1;
        $mensaje->accion_realizada = 'Aceptar invitacion';
        $invitacion->save(false);
        //tambien debo marcar el mensaje como contestado y leido
        $mensaje->contestado = 1;
        $mensaje->save(false);

        //en este punto all esta correcto, ya es el nuevo coordinador de la iniciativa
        //se setea un mensaje y se envia a la vista de la iniciativa
        Yii::app()->user->setFlash('success', 'Felicidades, ahora eres el nuevo coordinador de la iniciativa ' . $iniciativa->nombre_abreviado);
        $this->redirect(array('/iniciativas/crudIniciativas/ver', 'id' => $iniciativa->id));
    }

    /**
     * Accion que ejecuta la logica cuando un usuario decide rechazar ya sea una
     * invitacion o solicitud. En este caso se marca el mensaje como contestado y
     * si es una invitacion entonces se marca como rechazada.
     *
     * @param int id la id de la notificacion de invitacion a coordinacion de iniciativa
     * @throws CHttpException
     */
    public function actionRechazarSolicitud($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || $mensaje->destinatario != $usuario_id)
            throw new CHttpException(403, 'No tienes los permisos para rechazar esta solicitud. Tu acci&oacute;n ser&aacute; notificada al administrador');

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        //veo si es que es una invitacion
        $invitacion = Invitacion::model()->findByPk($mensaje->id);
        if ($invitacion != null) {
            $invitacion->aceptada = -1;
            $invitacion->save(false);
        }
        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar solicitud';
        $mensaje->save(false);

        Yii::app()->user->setFlash('success', 'Se ha rechazado correctamente la notificaci&oacute;n.');
        $this->redirect(array('/buzon/inbox/index'));
    }

    /**
     * Permite a un usuario rechazar la solicitud de membresia a una iniciativa en particular (invitacion a ser
     * parte como integrante de una iniciativa).
     *
     * @param int $id la id de la notificacion de solicitud de membresia
     * @throws CHttpException
     */
    public function actionRechazarSolicitudMembresia($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || $mensaje->destinatario != $usuario_id)
            throw new CHttpException(403, 'No tienes los permisos para rechazar esta solicitud. Tu acci&oacute;n ser&aacute; notificada al administrador');

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar invitacion';
        $mensaje->save(false);

        //notifico al emisor que la actividad ha sido rechazada
        $notificacion = new AvisoRechazoSolicitud;
        $notificacion->rechazo = Persona::model()->findByPk($usuario_id);
        $notificacion->invito = Persona::model()->findByPk($mensaje->emisor);
        $notificacion->iniciativa = Iniciativa::model()->findByPk($mensaje->id_relacion);
        $notificacion->enviarSolicitud();

        Yii::app()->user->setFlash('success', 'Se ha rechazado correctamente la solicitud de membres&iacute;a');
        $this->redirect(array('/buzon/inbox/index'));
    }

}