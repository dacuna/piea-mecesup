<?php
/**
 * Controlador con acciones para administrar una iniciativa.
 *
 * Principalmente contiene las acciones que permiten cambiar al coordinador actual de la iniciativa y ademas
 * invitar a otro usuario a ser coordinador.
 *
 * @version   1.0
 * @since     10/15/12
 * @author    dacuna <dacuna@dacuna.me>
 */
class AdministracionController extends Controller
{
    /**
     * Permite cambiar al actual coordinador de una iniciativa. El flujo de esta accion consiste en
     * enviar una invitacion al nuevo coordinador. Solo el administrador de PIEA puede ejecutar esta accion.
     *
     * @param int $id ID de la iniciativa
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCambiarCoordinador($id)
    {
        $model = IniciativaModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La p&aacute;gina seleccionada no existe.');
        if ($model->coordinador == null)
            $this->redirect(array('/iniciativas/crudIniciativas/ver', 'id' => $id));

        //si es que ya se ha enviado una solicitud de cambio, no se puede ejecutar esta accion, solo una solicitud
        //puede haber en el sistema por cada iniciativa
        $invitacion = Invitacion::model()->existeInvitacionEnCurso('coordinacion_iniciativa', $id, 0);

        //se debe invitar a un coordinador primero, el nuevo coordinador debe aceptar la invitacion
        $cambiar_coordinador = new CambiarCoordinadorForm;
        if (!$invitacion && isset($_POST['CambiarCoordinadorForm'])) {
            $cambiar_coordinador->attributes = $_POST['CambiarCoordinadorForm'];
            if ($cambiar_coordinador->validate()) {
                $cambiar_coordinador->enviarInvitacion();
                Yii::app()->user->setFlash('success', 'Se ha enviado correctamente la invitaci&oacute;n.');
                $this->redirect(array('/iniciativas/crudIniciativas/ver', 'id' => $cambiar_coordinador->id_iniciativa));
            }
        }

        $this->render('cambiar_coordinador', array(
            'model' => $model,
            'cambiar_coordinador' => $cambiar_coordinador,
            'invitacion' => $invitacion
        ));
    }

    /**
     * Permite que un usuario que fue invitado a ser coordinador de una iniciativa acepte dicha invitacion pasando asi a
     * ser el nuevo coordinador. Esta accion solo puede ser ejecutada desde la notificacion de invitacion
     * correspondiente de invitacion a ser coordinador de iniciativa en alguna iniciativa en particular.
     *
     * @param int $e emisor de la notificacion
     * @param int $d destinatario de la notificacion
     * @param int $uid identificador unico del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAceptarSolicitudCambioCoordinacion($e, $d, $uid)
    {
        $this->onContestarInvitacion=array(app()->mailman,'notificacionHandler');
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        //se deben revocar los permisos de coordinacion al usuario antiguo y darle nuevos permisos al usuario destinatario del mensaje
        $iniciativa = IniciativaModel::model()->findByPk($mensaje->id_relacion);
        $coordinador_antiguo = $iniciativa->coordinador;
        $lookup_antiguo = LookupIniciativa::model()->find('nombre_item=:ni and iniciativa_id=:ii and user_id=:ui', array(
            ':ni' => 'coordinador_iniciativa',
            ':ii' => $iniciativa->id,
            ':ui' => $coordinador_antiguo->id
        ));
        //a ese lookup se debe dejar como inactivo
        $lookup_antiguo->activo = 0;
        if ($lookup_antiguo->save()) {
            $auth = Yii::app()->authManager;
            //verifico que no sea coordinador de otra iniciativa, si lo es entonces ya tiene el permiso asignado
            if ($auth->getAuthAssignment("coordinador_iniciativa", $mensaje->destinatario) == null)
                $admin = $auth->assign('coordinador_iniciativa', $mensaje->destinatario);
            //seteo el lookup de la iniciativa correspondiente
            $lookup = LookupIniciativa::crearLookupIniciativa('coordinador_iniciativa', $mensaje->destinatario, $iniciativa->id);

            //se debe tambien ingresar el integrante nuevo
            if (IntegranteIniciativa::crearIntegrante($mensaje->destinatario, $iniciativa->id) != null) {
                //por ultimo marco la invitacion como contestada y el mensaje como leido
                $invitacion = Invitacion::model()->updateByPk($mensaje->id, array('aceptada' => 1));
                $mensaje->contestado = 1;
                $mensaje->accion_realizada = 'Aceptar invitaci&oacute;n';
                $mensaje->save(false);

                //se notifica al usuario que la invitacion ha sido aceptada
                $iniciativa = IniciativaModel::model()->findByPk($mensaje->id_relacion);
                $persona=Persona::model()->findByPk($mensaje->destinatario);
                $this->onContestarInvitacion(new CEvent($this,array('tipo'=>'aceptar_cambio_coordinacion','iniciativa'=>$iniciativa,'usuario'=>$persona,'destinatario'=>$mensaje->emisor)));

                user()->setFlash('success', 'Felicidades, ahora eres el coordinador de la iniciativa ' . $iniciativa->nombre_abreviado);
                $this->redirect(array('/iniciativas/crudIniciativa/ver', 'id' => $iniciativa->id));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
                $this->redirect(array('/'));
            }
        }
    }

    /**
     * Permite que un usuario que fue invitado a ser coordinador de una iniciativa rechace dicha invitacion.
     * Esta accion solo puede ser ejecutada desde la notificacion de invitacion correspondiente de invitacion
     * a ser coordinador de iniciativa en alguna iniciativa en particular.
     *
     * @param int $e emisor de la notificacion
     * @param int $d destinatario de la notificacion
     * @param int $uid identificador unico del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRechazarSolicitudCambioCoordinacion($e, $d, $uid)
    {
        $this->onContestarInvitacion=array(app()->mailman,'notificacionHandler');
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        //por ultimo marco la invitacion como contestada y el mensaje como leido
        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar invitaci&oacute;n';
        $mensaje->save(false);

        $iniciativa = IniciativaModel::model()->findByPk($mensaje->id_relacion);
        $persona=Persona::model()->findByPk($mensaje->destinatario);
        $this->onContestarInvitacion(new CEvent($this,array('tipo'=>'rechazar_cambio_coordinacion','iniciativa'=>$iniciativa,'usuario'=>$persona,'destinatario'=>$mensaje->emisor)));
        $invitacion = Invitacion::model()->updateByPk($mensaje->id, array('aceptada' => -1));
        if($invitacion>0)
            user()->setFlash('success','Se ha rechazado correctamente la invitacion.');
        else
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/buzon/inbox'));
    }

    /**
     * Filtros del controlador.
     *
     * @return  array array de filtros que el controlador utiliza.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controlador.
     *
     * @return  array array de reglas de acceso que el controlador utiliza.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('cambiarCoordinador'),
                'roles' => array('administrador'),
            ),
            array('allow',
                'actions' => array('aceptarSolicitudCambioCoordinacion', 'rechazarSolicitudCambioCoordinacion'),
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Evento enviado al contestar la invitacion a ser coordinador de iniciativa.
     *
     * @param CEvent $event evento
     *
     * @return  void
     */
    public function onContestarInvitacion($event){
        $this->raiseEvent('onContestarInvitacion', $event);
    }
}
