<?php
Yii::import('application.modules.usuarios.components.Entidad');
/**
 * Controlador que contiene las acciones principales para manejar el modelo de Iniciativas del sistema.
 * Dichas acciones son aquellas conocidas por su denominacion CRUD.
 *
 * Las acciones son accesibles desde la url /iniciativas/crud/nombreMetodo.
 *
 * @category  Controller
 * @package   modules.iniciativas.controllers
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class CrudIniciativasController extends Controller
{
    /**
     * Layout por defecto del controlador
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Accion que lista para los usuarios administradores la totalidad de iniciativas y para los usuarios comunes
     * lista sus iniciativas.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo    Implementar esta funcion
     *
     * @since   -
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Crea/registra un nuevo usuario.
     * Si la creacion/registro es realizado por un administrador entonces
     * se redirige a la vista de perfil del usuario creado. Si es un invitado
     * entonces se logea y se redirige al inicio de la plataforma.
     *
     * @throws CHttpException
     */
    public function actionCrear()
    {
        if (!Yii::app()->user->checkAccess('crear_iniciativa'))
            throw new CHttpException(403, 'No tienes los permisos necesarios para ingresar a esta p&aacute;gina.');

        $model = new IniciativaModel('create');

        $this->performAjaxValidation($model);

        if (isset($_POST['IniciativaModel'])) {
            $model->attributes = $_POST['IniciativaModel'];
            $model->estatutos_subido = CUploadedFile::getInstance($model, 'estatutos_subido');
            if ($model->estatutos_subido != null) $model->estatutos = time() . '_' . $model->estatutos_subido;
            if ($model->save()) {
                $dir = YiiBase::getPathOfAlias('webroot') . '/docs/estatutos/' . $model->primaryKey;
                if (!is_dir($dir))
                    mkdir($dir);
                if ($model->estatutos_subido != null) $model->estatutos_subido->saveAs($dir . '/' . $model->estatutos);
                $this->redirect(array('ver', 'id' => $model->id));
            }
        }

        $this->render('crear', array(
            'model' => $model,
        ));
    }

    /**
     * Crea/registra un nuevo usuario.
     * Si la creacion/registro es realizado por un administrador entonces
     * se redirige a la vista de perfil del usuario creado. Si es un invitado
     * entonces se logea y se redirige al inicio de la plataforma.
     *
     * @param int $id ID de la iniciativa
     * @throws CHttpException
     */
    public function actionVer($id)
    {
        $model = IniciativaModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La iniciativa buscada no existe en nuestros registros.');

        //formulario para invitar a coordinador
        $invitar_coordinador = new InvitarCoordinadorForm;
        if (isset($_POST['InvitarCoordinadorForm'])) {
            $invitar_coordinador->attributes = $_POST['InvitarCoordinadorForm'];
            if ($invitar_coordinador->validate()) {
                $invitar_coordinador->enviarInvitacion();
                Yii::app()->user->setFlash('success', 'Se ha enviado correctamente la invitaci&oacute;n.');
                $this->refresh();
            }
        }

        if (isset($_GET['ajax']))
            $this->RenderPartial('_ver', array('model' => $model), false, false);
        else
            $this->render('ver', array(
                'model' => $model,
                'invitar_coordinador' => $invitar_coordinador,
                'cambiar_logo' => new CambiarLogoForm,
                'entidad' => new Entidad($model->id, 0)
            ));
    }

    /**
     * Accion disenada para el menu "Explorar PIEA". Basicamente se muestra una pantalla con informacion
     * reducida de la iniciativa y que permite a usuarios que no pertenecen a la iniciativa conocer sobre que
     * trata dicha iniciativa.
     *
     * @param int $id ID de la iniciativa a visualizar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   2012-05-30
     */
    public function actionExplorar($id)
    {
        $model = IniciativaModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La iniciativa buscada no existe en nuestros registros.');

        $dataProvider = new CActiveDataProvider('Proyecto', array(
            'criteria' => array(
                'condition' => 'iniciativa_id=' . $model->id,
                'order' => 'nombre DESC'
            )
        ));

        $integrantes = new CActiveDataProvider('IntegranteIniciativa', array(
            'criteria' => array(
                'condition' => 'iniciativa_id=' . $model->id . ' and visibilidad=1',
                'order' => 'id ASC'
            )
        ));

        $this->render('explorar', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'integrantes' => $integrantes
        ));
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el editar la informacion general
     * de una iniciativa.
     *
     * @param int $id ID de la iniciativa a editar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEditar($id)
    {
        $model = IniciativaModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La iniciativa buscada no existe en nuestros registros.');
        if (Yii::app()->user->checkAccess('editar_iniciativa', array('iniciativa' => $id))) {
            if (isset($_POST['IniciativaModel'])) {
                $model->attributes = $_POST['IniciativaModel'];
                $model->scenario = 'update';
                $model->estatutos_subido = CUploadedFile::getInstance($model, 'estatutos_subido');
                if ($model->estatutos_subido != null) $model->estatutos = time() . '_' . $model->estatutos_subido;
                if ($model->save()) {
                    $dir = YiiBase::getPathOfAlias('webroot') . '/docs/estatutos/' . $model->primaryKey;
                    if (!is_dir($dir))
                        mkdir($dir);
                    if ($model->estatutos_subido != null) $model->estatutos_subido->saveAs($dir . '/' . $model->estatutos);
                    Yii::app()->user->setFlash('success', 'Se ha actualizado correctamente la iniciativa.');
                    $this->redirect(array('/iniciativas/crudIniciativas/ver', 'id' => $model->id));
                }
            }

            $this->render('editar', array(
                'model' => $model,
                'cambiar_logo' => new CambiarLogoForm
            ));
        } else
            throw new CHttpException(500, 'No tienes los permisos suficientes para ver est&aacute; p&aacute;gina.');
    }

    /**
     * Validacion ajax para un modelo.
     *
     * @param CModel el modelo a validar
     * @param String id_form el id del formulario a validar. Por defecto es 'iniciativa-form'
     */
    protected function performAjaxValidation($model, $id_form = 'iniciativa-form')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $id_form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Accion que permite obtener las iniciativas donde un usuario es integrante mediante una peticion ajax. La
     * respuesta es un mensaje JSON con el listado de iniaciativas con el id y nombre.
     *
     * @return  string Representacion JSON de las iniciativas del usuario
     * @throws  \CHttpException
     */
    public function actionIniciativasUsuario()
    {
        $preini = user()->iniciativas();
        $prepro = user()->proyectos();
        $ret = array();
        foreach ($preini as $iniciativa)
            array_push($ret, array('id' => $iniciativa->id, 'nombre' => $iniciativa->nombre_abreviado));
        foreach ($prepro as $proyecto)
            if (!in_array(array('id' => $proyecto->iniciativa->id, 'nombre' => $proyecto->iniciativa->nombre_abreviado), $ret))
                array_push($ret, array('id' => $proyecto->iniciativa->id, 'nombre' => $proyecto->iniciativa->nombre_abreviado));

        echo CJSON::encode($ret);
        app()->end();
    }

    /**
     * Acciones del controlador.
     *
     * @return  array acciones del controlador
     */
    public function actions()
    {
        return array(
            'buscarIntegrante' => array(
                'class' => 'application.extensions.AutoCompleteAction',
                'model' => 'Persona',
                'attribute' => 'nombres',
            ),
        );
    }
}
