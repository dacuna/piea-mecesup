<?php
/**
 * El modulo de iniciativas  provee de toda la funcionalidad para manejar las iniciativas de PIEA.
 *
 * Se poseen acciones para gestionar por ejemplo las invitaciones a ser parte de una iniciativa o las
 * invitaciones a ser coordinador, ademas existe un controlador CRUD con las acciones basicas sobre el
 * modelo de iniciativas, etc.
 *
 * @edit 07-08-2013: Si bien, existe una clase generalizada denominada @link{Entidad} que encapsula tanto las
 * caracteristicas propias de los proyectos y de las iniciativas, dicha clase fue agregada con posterioridad a la
 * creacion de este modulo, es por esto que se hizo la separacion entre modulos de iniciativa y proyecto (porque la
 * idea de crear la clase entidad fue posterior y se debia mantener compatibilidad con este codigo). A futuro seria
 * una buena idea el mezclar los modulos iniciativa y proyecto en uno solo denominado "entidades" y que haga full uso
 * de las bondades de la clase @link{Entidad}.
 *
 * @package   modules.iniciativas
 * @version   1.0
 * @since     2012-07-12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class IniciativasModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
	{
		$this->setImport(array(
			'iniciativas.models.*',
			'iniciativas.components.*',
		));
	}

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
