<?php
$this->pageTitle = 'Ver Iniciativa '.$model->nombre_abreviado;
$this->breadcrumbs=array(
  'Iniciativas'=>array('index'),
  $model->nombre_abreviado=>array('ver','id'=>$model->id),
  'Integrantes'
);
?>

<dl class="nice contained tabs">
  <dd><a href="<?php echo Yii::app()->createUrl('/iniciativas/crudIniciativas/ver',array('id'=>$model->id));?>">Ver iniciativa</a></dd>
  <dd><a href="<?php echo Yii::app()->createUrl('/tareas/actividadesEntidad/verActividades',array('id'=>$model->id,'tipo'=>0));?>">Actividades</a></dd>
  <dd><a href="<?php echo Yii::app()->createUrl('/iniciativas/perfil/integrantes',array('id'=>$model->id));?>" class="active">Integrantes</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-integrantes">

    <div class="panel">
      <h5>Integrantes de la iniciativa</h5>
      
      <p>Los siguientes son los participantes de esta iniciativa:</p>

      <ul class="clearfix">
        <?php foreach($model->integrantes as $integrante):?>
          <li style="width:47%;float:left;">
            <?php if(!empty($integrante->usuario->perfil->avatar)):?>
              <img style="float:left;" src="<?php echo Yii::app()->request->baseUrl;?>/avatars/<?php echo $integrante->usuario->perfil->avatar;?>" width="64" height="64">
            <?php else:?>
              <img style="float:left;" src="<?php echo Yii::app()->request->baseUrl;?>/images/no-picture.jpg" width="64" height="64">
            <?php endif;?>

            <p style="float:left;padding-top:10px;margin-left:10px;">
              <?php echo $integrante->usuario->nombreCompleto;?><br>
              &lt;<?php echo $integrante->usuario->email;?>&gt;<br>
              <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/index',array('id'=>$integrante->usuario->id));?>">Ver Perfil</a>
            </p>
          </li>
        <?php endforeach;?>
      </ul>

    </div>

  </li>
</ul>