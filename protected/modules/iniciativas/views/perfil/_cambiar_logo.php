<div class="form">
  <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
    'id'=>'logo-form',
    'type'=>'nice',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'action'=>Yii::app()->createUrl('/iniciativas/perfil/cambiarLogo')
  )); ?>

  <div class="row">
    <div class="twelve columns">
      <fieldset>
        <h5>
          <?php if(empty($iniciativa->logo)):?>
            Agregar logo a iniciativa
          <?php else:?>
            Cambiar logo de iniciativa
          <?php endif;?>
        </h5>
        <p>Mediante el siguiente formulario puedes 
          <?php if(empty($iniciativa->logo)):?>
            agregar un logo a
          <?php else:?>
            cambiar el logo de 
          <?php endif;?>
          la iniciativa. Esta debe
        tener como formato obligatorio <i>.svg</i></p>
        <?php echo $form->fileFieldRow($model, "logo",array('accept'=>".svg")); ?>  
        <?php echo $form->hiddenField($model, "iniciativa",array('value'=>$iniciativa->id)); ?>  
        <p>
          <br>
          <?php echo CHtml::submitButton(empty($model->logo)?'Agregar Logo':'Cambiar Logo',array('class'=>'nice radius small button')); ?>
        </p>        
      </fieldset>   
    </div>
  </div>
  
  <?php $this->endWidget(); ?>
</div>