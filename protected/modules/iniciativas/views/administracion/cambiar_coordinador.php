<?php
$this->pageTitle = 'Ver Iniciativa '.$model->nombre_abreviado;
$this->breadcrumbs=array(
    'Iniciativas'=>array('index'),
    'Ver Iniciativa'=>array('/iniciativas/crudIniciativas/ver','id'=>$model->id),
    'Cambiar coordinador'
);
?>

<dl class="nice contained tabs">
    <dd><a href="<?php echo url('/iniciativas/crudIniciativas/ver',array('id'=>$model->id));?>">Ver iniciativa</a></dd>
    <dd><a href="<?php echo url('/tareas/actividadesEntidad/verActividades',array('id'=>$model->id,'tipo'=>0));?>">Actividades</a></dd>
    <dd><a href="<?php echo url('/iniciativas/perfil/integrantes',array('id'=>$model->id));?>">Integrantes</a></dd>
    <dd><a href="<?php echo url('/iniciativas/administracion/cambiarCoordinador',array('id'=>$model->id));?>" class="active">Cambiar coordinador</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-iniciativa">

        <h1><?php echo $model->nombre_abreviado;?></h1>

        <?php if($model->coordinador!=null):?>
            <h3>Cambiar coordinador:</h3>
                <p>Mediante el siguiente formulario se puede enviar una invitaci&oacute;n para que otro usuario coordine la
                presente iniciativa. Una vez que el otro usuario acepte la invitaci&oacute;n, se har&aacute; efectivo el
                cambio.</p>
             <p>

                 <?php if($invitacion):?>
                    Ya existe una solicitud de cambio de coordinaci&oacute;n pendiente.
                <?php else:?>

                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm');?>
                <?php echo $form->errorSummary($cambiar_coordinador); ?>
                <label style="display:inline;">Ingrese nombre de usuario:</label>
                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'coordinador-livesearch',
                    'sourceUrl'=>array('/iniciativas/crudIniciativas/buscarIntegrante'),
                    'name'=>'coordinador-livesearch',
                    'options'=>array(
                        'minLength'=>'3',
                        'showAnim'=>'fold',
                        'select'=>"js: function(event, ui) {
                          $('#CambiarCoordinadorForm_id_usuario').val(ui.item['id']);
                        }"
                    ),
                    'htmlOptions'=>array(
                        'size'=>45,
                        'maxlength'=>45,
                        'style'=>'height:24px;'
                    ),
                )); ?>
                <?php echo $form->hiddenField($cambiar_coordinador, "id_usuario"); ?>
                <?php echo $form->hiddenField($cambiar_coordinador, "id_iniciativa",array('value'=>$model->id)); ?>
                <?php echo CHtml::submitButton('Invitar',array('class'=>'nice radius small button')); ?>

                <?php $this->endWidget(); ?>

                <?php endif;?>
            </p>
        <?php endif;?>
    </li>
</ul>
