<?php
$this->pageTitle = 'Ver Iniciativa '.$model->nombre_abreviado;
$this->breadcrumbs=array(
	'Iniciativas'=>array('index'),
	'Ver Iniciativa',
);
?>

<dl class="nice contained tabs">
    <?php if($entidad->getTipoEntidadNumerico()==1 && $entidad->getEntidad()->es_postulacion==1 && $entidad->getEntidad()->perfilPostulacion->estado_postulacion!=2):?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" class="active" >Ver Iniciativa</a></dd>
    <?php else:?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" class="active" >Ver Iniciativa</a></dd>
        <dd>
            <a href="<?php echo Yii::app()->createUrl('/tareas/actividadesEntidad/verActividades', array('id' => $entidad->id, 'tipo' => $entidad->getTipoEntidadNumerico()));?>">Actividades</a>
        </dd>
        <dd><a href="<?php echo Yii::app()->createUrl('/iniciativas/perfil/integrantes', array('id' => $entidad->id));?>">Integrantes</a>
        </dd>
        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <dd>
                <a href="<?php echo Yii::app()->createUrl('/usuarios/fae/resumenFae', array('id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));?>">FAE</a>
            </dd>
        <?php endif;?>
    <?php endif;?>
</dl>


<ul class="nice tabs-content contained">
  <li class="active" id="ver-iniciativa">

    <h1><?php echo $model->nombre_abreviado;?></h1>

    <div class="row">
      <div class="seven columns">
        <p>
        <strong>Nombre completo:</strong> <?php echo $model->nombre_completo;?><br>
        <strong>Sigla:</strong> <?php echo $model->sigla;?><br>
        <strong>Fecha creaci&oacute;n:</strong> <?php echo $model->fecha_creacion;?><br>
        <strong>Misi&oacute;n:</strong> <?php echo $model->mision;?><br>
        <strong>Visi&oacute;n:</strong> <?php echo $model->vision;?><br>
        <strong>Descripci&oacute;n:</strong> <?php echo $model->descripcion;?><br>
        <strong>Objetivos generales:</strong> <?php echo $model->objetivos_generales;?><br>
        <strong>Objetivos espec&iacute;ficos:</strong> <?php echo $model->objetivos_especificos;?>
        </p>
      </div>
      <div class="five columns">
        <?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
          <?php if(empty($model->logo)):?>
            <a href="#" class="thumbnail has-tip" title="Para agregar un logo presiona sobre la imagen" data-reveal-id="myModal">
          <?php else:?>
            <a href="#" class="thumbnail has-tip" title="Para cambiar el logo presiona sobre la imagen" data-reveal-id="myModal">
          <?php endif;?>
        <?php else:?>
          <a href="#" class="thumbnail">
        <?php endif;?>

        <?php if(empty($model->logo)):?>
          <img src="http://placehold.it/260x180&text=Logo">
        <?php else:?>
          
          <script type="image/svg+xml">
           <?php echo file_get_contents('logos/'.$model->id.'/'.$model->logo);?>
          </script>
          
          <?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
          <a href="#" data-reveal-id="myModal">Cambiar logo</a>
          <?php endif;?>

        <?php endif;?>
        </a>
      </div>
    </div>
    
    
    <p>
      <?php if($model->coordinador!=null):?>
        <strong>Coordinador: </strong><?php echo $model->coordinador->nombreCompleto;?>
      <?php endif;?>
    </p>
    
    <?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
      <a href="<?php echo url('/proyectos/crud/crearProyecto',array('id'=>$model->id));?>" class="small nice button">Crear proyecto</a>
      <a href="<?php echo url('/usuarios/userActions/invitar',array('tipo'=>'iniciativa','id'=>$model->id));?>" class="nice small button">Invitar persona</a>
      <a href="<?php echo url('/iniciativas/crudIniciativas/editar',array('id'=>$model->id));?>" class="small nice button">Editar iniciativa</a>
      <br><br>
    <?php endif;?>

    <?php if(false && $entidad->checkPermisosBoolean('integrante',user()->id)):?>
    <div class="row">
        <div class="six columns">
            <div class="panel">
                <h5>Cuentas y transferencias:</h5>
                <ul>
                    <li><a href="<?php echo url('/finanzas/crud/verCuentasEntidad',array('id'=>$model->id,'tipo'=>0));?>">
                        - Ver Cuentas</a></li>
                    <li><a href="<?php echo url('/finanzas/transferencia/realizarTransferencia',array('id'=>$model->id,'tipo'=>0));?>">
                        - Realizar transferencia</a>
                    </li>
                    <li><a href="<?php echo url('/finanzas/transferencia/transferenciaInterna',array('id'=>$model->id,'tipo'=>0));?>">
                        - Transferencia interna</a>
                    </li>
                    <li><a href="<?php echo url('/finanzas/historico/verTransferenciasEntidad',array('id'=>$model->id,'tipo'=>0));?>">
                        - Ver transferencias</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="six columns">
            <div class="panel">
                <h5>Operaciones de finanzas:</h5>
                <ul>
                    <?php if($entidad->checkPermisosBoolean('ingresar_presupuesto',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearPresupuesto',array('id'=>$model->id,'tipo'=>0));?>">- Ingresar presupuesto</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ingresar_recuperacion',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearRecibo',array('id'=>$model->id,'tipo'=>0,'tf'=>1));?>">- Solicitar recuperaci&oacute;n de gastos</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ver_recuperacion',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/verRecuperacionGastos',array('id'=>$model->id,'tipo'=>0));?>">- Ver recuperaci&oacute;n de gastos</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ingresar_vale',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearRecibo',array('id'=>$model->id,'tipo'=>0,'tf'=>2));?>">- Solicitar vale a rendir</a>
                    </li>
                    <li><a href="<?php echo url('/finanzas/operaciones/verRetirosFondoDisponiblePorUsuario',array('id'=>$model->id,'tipo'=>0));?>">- Gestionar vales que he solicitado</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('gestionar_vales',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/verRetirosFondoDisponible',array('id'=>$model->id,'tipo'=>0));?>">- Gestionar vales solicitados de la iniciativa</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ver_vales_historicos',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/verValesRendir',array('id'=>$model->id,'tipo'=>0));?>">- Ver vales a rendir hist&oacute;ricos</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ingresar_pago_proveedor',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearPagoProveedores',array('id'=>$model->id,'tipo'=>0,'tf'=>1));?>">- Crear pago a proveedores</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ver_pago_proveedor',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/verPagoProveedores',array('id'=>$model->id,'tipo'=>0,'tf'=>1));?>">- Ver pagos a proveedores</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ingresar_presupuesto',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearPresupuestoUnidadAbastecimiento',array('id'=>$model->id,'tipo'=>0));?>">- Ingresar presupuesto para unidad de abastecimiento</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ingresar_unidad_abastecimiento',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/crearPagoProveedores',array('id'=>$model->id,'tipo'=>0,'tf'=>2));?>">- Crear unidad de abastecimiento</a>
                    </li>
                    <?php endif;?>
                    <?php if($entidad->checkPermisosBoolean('ver_unidad_abastecimiento',user()->id)):?>
                    <li><a href="<?php echo url('/finanzas/operaciones/verPagoProveedores',array('id'=>$model->id,'tipo'=>0,'tf'=>2));?>">- Ver unidad de abastecimiento</a>
                    </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif;?>


    <?php if(Yii::app()->user->checkAccess('administrador') && $model->coordinador==null):?>
      <?php if(Invitacion::model()->find('tipo_invitacion=:ti and entidad_id=:ei and tipo_entidad=:te',
        array(':ti'=>'coordinacion_iniciativa',':ei'=>$model->id,':te'=>0))==null):?>
          <h3>Invitar coordinador:</h3>
          <p>
            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
              'id'=>'invitar-coordinador-form'
            )); ?>
              <?php echo $form->errorSummary($invitar_coordinador); ?>
              <label style="display:inline;">Ingrese nombre de usuario:</label>
              <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'coordinador-livesearch',
                      'sourceUrl'=>array('/iniciativas/crudIniciativas/buscarIntegrante'),
                      'name'=>'coordinador-livesearch',
                      'options'=>array(
                        'minLength'=>'3',
                        'showAnim'=>'fold',
                        'select'=>"js: function(event, ui) {
                          $('#InvitarCoordinadorForm_id_usuario').val(ui.item['id']);
                        }"
                      ),
                      'htmlOptions'=>array(
                        'size'=>45,
                        'maxlength'=>45,
                        'style'=>'height:24px;'
                      ),
              )); ?>
              <?php echo $form->hiddenField($invitar_coordinador, "id_usuario"); ?>
              <?php echo $form->hiddenField($invitar_coordinador, "id_iniciativa",array('value'=>$model->id)); ?>
              <?php echo CHtml::submitButton('Invitar',array('class'=>'nice radius small button')); ?>

            <?php $this->endWidget(); ?>
          </p>
       <?php else:?>
          <h3>Invitar coordinador:</h3>
              <p>Ya existe una invitaci&oacute;n de coordinaci&oacute;n en curso.</p>
        <?php endif;?>

    <?php endif;?>

      <?php if(user()->checkAccess('integrante_iniciativa',array('iniciativa'=>$model->id))):?>

          <div class="row">
              <div class="twelve columns">
                  <div class="panel">
                      <h5>Cargos</h5>

                      <?php if($entidad->verificarAsignacionDeCargo(user()->id)):?>
                      <p style="text-align: right;">
                      <a href="<?php echo Yii::app()->createUrl('/usuarios/cargos/crear',array('id'=>$model->id,'tipo'=>0));?>"
                         class="small nice button">Asignar Cargo</a>
                          <?php if(user()->checkAccess('administrador') && $model->coordinador!=null):?>
                          <a href="<?php echo url('/iniciativas/administracion/cambiarCoordinador',array('id'=>$model->id));?>" class="small nice button">Cambiar Coordinador</a>
                          <?php endif;?>
                      </p>
                      <?php endif;?>

                      <p>La siguiente tabla muestra los cargos existentes en la iniciativa junto con sus responsables.</p>
                      <table>
                          <thead>
                          <tr>
                          <th>Cargo</th>
                          <th>Responsable</th>
                          <th>Permisos</th>
                          <th>Transferir</th>
                          <?php if(user()->checkAccess('coordinador_iniciativa',array('iniciativa'=>$entidad->getId()))):?>
                            <th>Eliminar</th>
                          <?php endif;?>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach($model->cargos as $cargo):?>
                            <tr>
                              <td><?php echo $cargo[0]->getDescription();?></td>
                              <td>
                                  <?php if($cargo[1]->asignacion_nula==1):?>
                                    ---
                                   <?php else:?>
                                    <?php echo $cargo[1]->usuario->nombrePresentacion;?>
                                  <?php endif;?>
                              </td>
                              <td><?php array_walk($cargo[0]->getChildren(),
                                  create_function('$item,$key,$size','echo $item->getDescription();if($key<$size-1) echo ", ";'),count($cargo[0]->getChildren()));?></td>
                              <td>
                                <?php if($entidad->verificarJerarquiaCargo($cargo[1])):?>
                                  <a href="<?php echo url('/usuarios/cargos/transferirCargo',array(
                                      'id'=>$model->id,'tipo'=>0,'cargo'=>$cargo[1]->nombre_item));?>">Transferir</a>
                                <?php else:?>
                                    ---
                                <?php endif;?>
                              </td>

                                <?php if(user()->checkAccess('coordinador_iniciativa',array('iniciativa'=>$entidad->getId()))):?>
                                    <td>
                                        <a href="<?php echo url('/usuarios/cargos/eliminarCargo',
                                            array('id'=>$entidad->getId(),'tipo'=>$entidad->getTipoEntidadNumerico(),'cargo'=>$cargo[1]->nombre_item));?>">Eliminar</a>
                                    </td>
                                <?php endif;?>

                            </tr>
                          <?php endforeach;?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      <?php endif;?>
  </li>
</ul>

<?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
<?php
$this->modal['id']='myModal';
$this->modal['close']=true;

$this->modal['content']=$this->renderPartial('/perfil/_cambiar_logo',array('model'=>$cambiar_logo,'iniciativa'=>$model),true)
?>
<?php endif;?>
<script>
$(function(){
  $('svg').attr('style','width:250px;height:250px;')
});
</script>