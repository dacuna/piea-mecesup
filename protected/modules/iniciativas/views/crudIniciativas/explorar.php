<?php
$this->pageTitle = 'Ver Iniciativa '.$model->nombre_abreviado;
$this->breadcrumbs=array(
  'Iniciativas'=>array('index'),
  'Ver Iniciativa',
);
?>

<dl class="nice contained tabs">
    <dd><a href="<?php echo url('/site/explorarPiea');?>" id="explorar-piea">Explorar PIEA</a></dd>
    <dd><a href="<?php echo url('/iniciativas/crudIniciativas/explorar',array('id'=>$model->id));?>" id="ver-iniciativa" class="active">Iniciativa</a></dd>
    <dd><a href="#ver-proyecto" id="ver-proyecto">Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-iniciativa">

    <h1><?php echo $model->nombre_abreviado;?></h1>

    <div class="row">
      <div class="seven columns">
        <p>
        <strong>Nombre completo:</strong> <?php echo $model->nombre_completo;?><br>
        <strong>Sigla:</strong> <?php echo $model->sigla;?><br>
        <strong>Fecha creaci&oacute;n:</strong> <?php echo $model->fecha_creacion;?><br>
        <strong>Misi&oacute;n:</strong> <?php echo $model->mision;?><br>
        <strong>Visi&oacute;n:</strong> <?php echo $model->vision;?><br>
        <strong>Descripci&oacute;n:</strong> <?php echo $model->descripcion;?><br>
        <strong>Objetivos generales:</strong> <?php echo $model->objetivos_generales;?><br>
        <strong>Objetivos espec&iacute;ficos:</strong> <?php echo $model->objetivos_especificos;?>
        </p>
      </div>
      <div class="five columns">

        <?php if(empty($model->logo)):?>
          <img src="http://placehold.it/260x180&text=Logo">
        <?php else:?>
          
          <script type="image/svg+xml">
           <?php echo file_get_contents('logos/'.$model->id.'/'.$model->logo);?>
          </script>
          
          <?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
          <a href="#" data-reveal-id="myModal">Cambiar logo</a>
          <?php endif;?>

        <?php endif;?>
        </a>
      </div>
    </div>
    
    
    <p>
      <?php if($model->coordinador!=null):?>
        <strong>Coordinador: </strong><?php echo $model->coordinador->nombreCompleto;?>
      <?php endif;?>
    </p>
    
    <?php if(!Yii::app()->user->checkAccess('integrante_iniciativa',array('iniciativa'=>$model->id))):?>
      <?php if(Mensaje::model()->mensajeSolicitud(array('tipo'=>'iniciativa','id'=>$model->id),user()->id)==null):?>
        <a href="<?php echo Yii::app()->createUrl('/iniciativas/gestionIntegrantes/solicitarMembresia',array('id'=>$model->id));?>" class="small nice button">Solicitar participaci&oacute;n</a>
      <?php else:?>
          Ya has solicitado participaci&oacute;n. Tu solicitud est&aacute; siendo procesada.
      <?php endif;?>
    <?php endif;?>
    <br><br>
    <div class="panel">
      <h5>Proyectos</h5>
      <?php
        $this->widget('zii.widgets.grid.CGridView', array(
          'dataProvider'=>$dataProvider,
          'selectableRows'=>0,
          'itemsCssClass'=>'table',
          'columns'=>array(
              'nombre',
              'descripcion',
              'objetivos_generales',
              array(
                'class'=>'CButtonColumn',
                'template'=>'{my_button}',
                'buttons'=>array(
                    'my_button'=>array(
                      'label'=>'Ver',
                      'url'=>'url("/proyectos/crud/explorar",array("id"=>$data->id))',
                    ),
                ),
            ),
          ),
      ));
      ?>
    </div>

    <div class="panel">
      <h5>Integrantes de la iniciativa</h5>
      <?php
        $this->widget('zii.widgets.grid.CGridView', array(
          'dataProvider'=>$integrantes,
          'selectableRows'=>0,
          'itemsCssClass'=>'table',
          'columns'=>array(
              'usuario.nombreCompleto',
              'usuario.email',  
          ),
      ));
      ?>
    </div>
    
  </li>

  <li id="ver-proyectoTab">
    <p>Selecciona algun proyecto en la pesta&ntilde;a anterior.</p>
  </li>

</ul>

<script>
$(function(){
  $('svg').attr('style','width:250px;height:250px;')
});
</script>