<h1><?php echo $model->nombre_abreviado;?></h1>

<p>
<strong>Nombre completo:</strong> <?php echo $model->nombre_completo;?><br>
<strong>Nombre abreviado:</strong> <?php echo $model->nombre_abreviado;?><br>
<strong>Descripci&oacute;n:</strong> <?php echo $model->descripcion;?><br>
<strong>Objetivos generales:</strong> <?php echo $model->objetivos_generales;?>
</p>

<p>
  <?php if($model->coordinador!=null):?>
    <strong>Coordinador: </strong><?php echo $model->coordinador->nombreCompleto;?>
  <?php endif;?>
</p>

<?php if(!Yii::app()->user->checkAccess('integrante_iniciativa',array('iniciativa'=>$model->id))):?>
  <a href="<?php echo Yii::app()->createUrl('/iniciativas/gestionIntegrantes/solicitarMembresia',array('id'=>$model->id));?>" class="small nice button">Solicitar participaci&oacute;n</a>
  <br><br>
<?php endif;?>

<h3>Proyectos</h3>

<?php
$this->widget('zii.widgets.grid.CGridView', 
  array('dataProvider' => $model->getDataProvider('proyectos'),
  'id'=>'grid-proyectos',
  'actionPrefix'=>'grid-proyectos',
  'columns'=>array(
    'nombre',
    'descripcion',
    'objetivos_generales',
    array(
      'class'=>'CLinkColumn',
      'label'=>'Ver',
      'urlExpression'=>'Yii::app()->createUrl("site/explorarPiea",array("id"=>$data->id))'
    ),
),
));
?>
