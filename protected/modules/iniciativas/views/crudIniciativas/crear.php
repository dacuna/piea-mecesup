<?php
$this->pageTitle = 'Crear Iniciativa';
$this->breadcrumbs=array(
	'Iniciativas'=>array('index'),
	'Crear iniciativa',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Crear iniciativa</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

    <div class="form">

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
      'id'=>'iniciativa-form',
      'type'=>'nice',
      'enableAjaxValidation'=>true,
      'htmlOptions'=>array('enctype'=>'multipart/form-data')
    )); ?>

      <p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

      <?php //la validacion se esta haciendo en cada textFieldRow echo $form->errorSummary($model); ?>

      <?php echo $form->textFieldRow($model,"nombre_completo"); ?> 
      <?php echo $form->textFieldRow($model,"nombre_abreviado"); ?> 
      <?php echo $form->textFieldRow($model,"sigla"); ?> 
      <?php echo $form->labelEx($model,'fecha_creacion'); ?>
      <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'model'=>$model,
        'attribute'=>'fecha_creacion',
        'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy','changeYear'=>'true','yearRange'=> '1940:2012'),
        'language'=>'es',
        'htmlOptions' => array('class'=>'input-text'),
        ));
      ?>
      <div class="form-field error">
        <?php echo $form->error($model,'fecha_creacion'); ?>
      </div>
      <?php echo $form->fileFieldRow($model, "estatutos_subido"); ?>
      <?php echo $form->textAreaRow($model, "mision",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "vision",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "objetivos_generales",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "objetivos_especificos",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "descripcion",array('rows'=>6)); ?> 
      
      <?php echo $form->textFieldRow($model,"direccion"); ?> 
      <?php echo $form->dropDownListRow($model, 'campus',
        CHtml::listData($model->lista_campus, 'id', 'nombre'),array('encode'=>false,'style'=>'height: 28px;')); ?> 
      <?php echo $form->textFieldRow($model,"telefono"); ?>
    
      <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear iniciativa' : 'Guardar',array('class'=>'nice radius medium button')); ?>
      </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->
  </li>
</ul>