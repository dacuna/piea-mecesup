<?php
$this->pageTitle = 'Editar Iniciativa '.$model->nombre_abreviado;
$this->breadcrumbs=array(
  'Iniciativas'=>array('index'),
  $model->nombre_abreviado=>array('ver','id'=>$model->id),
  'Editar'
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Editar iniciativa</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-iniciativa">

    <h1><?php echo $model->nombre_abreviado;?></h1>

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'iniciativa-form',
        'type'=>'nice',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data')
      )); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
      <div class="seven columns">
        <p>
          <?php echo $form->textFieldRow($model, "nombre_completo"); ?>
          <?php echo $form->textFieldRow($model, "nombre_abreviado"); ?>
          <?php echo $form->textFieldRow($model, "sigla"); ?>
          <?php echo $form->textFieldRow($model, "direccion"); ?>
          <?php echo $form->textFieldRow($model, "telefono"); ?>
          <?php echo $form->dropDownListRow($model, 'campus',
            CHtml::listData($model->lista_campus, 'id', 'nombre'),array('encode'=>false,'style'=>'height: 28px;')); ?>
            <?php echo $form->labelEx($model,'fecha_creacion'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$model,
            'attribute'=>'fecha_creacion',
            'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy',
                        'changeYear'=>'true','yearRange'=> '1940:2012',),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text'),
            ));
          ?>
            <?php echo $form->fileFieldRow($model, "estatutos_subido"); ?>
            <br>
        <strong>Misi&oacute;n:</strong><br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array('lang'=>'en','toolbar'=>'mini','model'=>$model,'attribute'=>'mision','htmlOptions'=>array('style'=>'height:200px;z-index:0;')));?>
        <strong>Visi&oacute;n:</strong><br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array('lang'=>'en','toolbar'=>'mini','model'=>$model,'attribute'=>'vision','htmlOptions'=>array('style'=>'height:200px;z-index:0;')));?>
        <strong>Descripci&oacute;n:</strong><br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array('lang'=>'en','toolbar'=>'mini','model'=>$model,'attribute'=>'descripcion','htmlOptions'=>array('style'=>'height:200px;z-index:0;')));?>
        <strong>Objetivos generales:</strong><br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array('lang'=>'en','toolbar'=>'mini','model'=>$model,'attribute'=>'objetivos_generales','htmlOptions'=>array('style'=>'height:200px;z-index:0;')));?>
        <strong>Objetivos espec&iacute;ficos:</strong><br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array('lang'=>'en','toolbar'=>'mini','model'=>$model,'attribute'=>'objetivos_especificos','htmlOptions'=>array('style'=>'height:200px;z-index:0;')));?>
        </p>
      </div>
      <div class="five columns">
        <?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
         <a href="#" class="thumbnail has-tip" title="Para cambiar el logo presiona sobre la imagen" data-reveal-id="myModal">
        <?php else:?>
          <a href="#" class="thumbnail">
        <?php endif;?>
          <?php if(empty($model->logo)):?>
            <img src="http://placehold.it/260x180&text=Logo">
          <?php else:?>
            <object width="248" height="180" data="<?php echo Yii::app()->baseUrl;?>/logos/<?php echo $model->id.'/'.$model->logo;?>" type="image/svg+xml"></object>
          <?php endif;?>
        </a>
      </div>
    </div>
    <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius medium button')); ?>
    <a href="<?php echo Yii::app()->createUrl('/iniciativas/crudIniciativas/ver',array('id'=>$model->id));?>" class="nice radius medium red button">Cancelar</a>
    <?php $this->endWidget(); ?>
    
  </li>
</ul>

<?php if(Yii::app()->user->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->id))):?>
<?php
$this->modal['id']='myModal';
$this->modal['close']=true;

$this->modal['content']=$this->renderPartial('/perfil/_cambiar_logo',array('model'=>$cambiar_logo,'iniciativa'=>$model),true)
?>
<?php endif;?>

<script type="text/javascript">
$(function(){
  $('#IniciativaModel_fecha_creacion').click(function(){
    $('#ui-datepicker-div').css('z-index','2003');
  });
});
</script>