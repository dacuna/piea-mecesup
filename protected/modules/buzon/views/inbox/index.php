<?php
$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h2>Notificaciones</h2>

<h4>Pendientes</h4>

<div id="inbox-no-contestadas">
  <ul class="list">
    <li class="titulo-mensajes clearfix"><p class="name">Emisor</p><p class="description">Asunto</p><p class="fecha">Fecha</p><span class="subcategory">all</span></li>
    <?php foreach($dataProvider->data as $msg):?>
      <li class="mensaje-all clearfix">
          <p class="name">
            <a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>">
              <?php echo $msg->emisor_persona->nombrePresentacion;?>
            </a>
          </p>
          <span class="category"><?php echo $msg->tipo_relacion;?></span>
          <?php if($msg->tipo_relacion=='proyecto'):?>
            <span class="subcategory"><?php echo $msg->proyecto->nombre;?></span>
          <?php else:?>
            <span class="subcategory"><?php echo $msg->iniciativa->nombre_abreviado;?></span>
          <?php endif;?>
          <p class="description" style="text-align:left;">
            <a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>"><?php echo $msg->asunto;?></a>
          </p>
          <p class="fecha"><a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>">
            <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",$msg->fecha_envio);?>
            </a>
          </p>
      </li>
    <?php endforeach;?>
  </ul>
</div>

<h4>Todo</h4>

<div id="inbox-all">
  <ul class="list">
    <li class="titulo-mensajes clearfix"><p class="name">Emisor</p><p class="description">Asunto</p><p class="fecha">Fecha</p><span class="subcategory">all</span></li>
    <?php foreach($todos->data as $msg):?>
      <li class="mensaje-all clearfix">
          <p class="name">
            <a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>">
              <?php echo $msg->emisor_persona->nombrePresentacion;?>
            </a>
          </p>
          <span class="category"><?php echo $msg->tipo_relacion;?></span>
          <?php if($msg->tipo_relacion=='proyecto'):?>
            <span class="subcategory"><?php echo $msg->proyecto->nombre;?></span>
          <?php else:?>
            <span class="subcategory"><?php echo $msg->iniciativa->nombre_abreviado;?></span>
          <?php endif;?>
          <p class="description" style="text-align:left;">
            <a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>"><?php echo $msg->asunto;?></a>
          </p>
          <p class="fecha"><a href="<?php echo Yii::app()->createUrl('/buzon/inbox/verMensaje',array('id'=>$msg->id));?>">
            <?php echo Yii::app()->dateFormatter->format("dd'/'MM'/'y '-' HH:mm 'hrs.'",$msg->fecha_envio);?>
            </a>
          </p>
      </li>
    <?php endforeach;?>
  </ul>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/list.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/underscore-min.js', CClientScript::POS_HEAD);?>

<script type="text/javascript">
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
$(function(){
  //se debe crear el menu de filtro con categoria y subcategorias
  var items = [];
  $('.mensaje-all').each(function(){
    var cat=$(this).children('.category').first().html();
    var subcat=$(this).children('.subcategory').first().html();
    items.push({'categoria':cat,'subcat':subcat});
  });
  //en elementos esta el arbol con los filtros correspondientes, se deben quitar los repetidos
  elementos=_.groupBy(items,function(item){return item.categoria});
  _.each(elementos,function(elem,key){
    elementos[key]=_.uniq(elem,false,function(value){return value.subcat});
  });
  
  //agrego a la lista de filtros, cada elemento encontrado junto a sus hijos
  _.each(elementos,function(elem,key){
    var contenido = '<li class="head-filter"><strong>'+key.capitalize()+'</strong><ul>';
    _.each(elem,function(e,k){
      contenido+='<li class="filter-entidad"><a href="#">'+e.subcat+'</a></li>';
    });
    contenido+='</ul></li>';
    $('.filter').append(contenido);
  });

  //se crea la lista de filtros finales
  var options = {
    valueNames: [ 'name', 'description', 'subcategory' ]
  };

  var featureList = new List('inbox-all', options);
  var noLeidosList = new List('inbox-no-contestadas', options);

  $('.filter-entidad').click(function() {
      var data=$(this).children('a').first().html();
      featureList.filter(function(item) {
          if (item.values().subcategory == data || item.values().subcategory == 'all') {
              return true;
          } else {
              return false;
          }
      });
      noLeidosList.filter(function(item) {
          if (item.values().subcategory == data || item.values().subcategory == 'all') {
              return true;
          } else {
              return false;
          }
      });
      return false;
  });

  $('#filter-none').click(function() {
      featureList.filter();
      noLeidosList.filter();
      return false;
  });
});
</script>