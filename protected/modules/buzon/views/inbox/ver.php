<?php
$this->breadcrumbs=array(
	$this->module->id=>array('index'),
  'Ver mensaje'
);
?>
<h2><?php echo $mensaje->asunto;?></h2>
<hr>
<p>
<span>De: <?php echo $mensaje->emisor_persona->nombreCompleto;?></span>
<span style="float:right;">Fecha de env&iacute;o: 
  <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y 'a las' h':'m a",$mensaje->fecha_envio);?>
</span>
</p>

<p style="color:#222222;">
<?php echo $mensaje->contenido;?>
</p>

<?php if($mensaje->contestado!=1 && $mensaje->accion=='answered_as_read'):?>
  <p style="text-align:right;">
  <a href="<?php echo url('/buzon/inbox/marcarComoContestado',array('id'=>$mensaje->id));?>" class="small nice green button">Marcar como contestado</a>
  </p>
<?php endif;?>
