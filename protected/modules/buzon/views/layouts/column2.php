<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
  <div class="three columns">
    
    <!--<dl class="nice vertical tabs">
      <dd><a href="#vertical1" class="active">Notificaciones</a></dd>
      <dd class="clearfix" style="border:1px #eee solid;border-top:0;border-bottom:0;">
        <ul>
          <li>Bandeja de entrada</li>
          <li>Le&iacute;dos</li>
          <li>Eliminados</li>
        </ul>
      </dd>
    </dl>-->
    
    <dl style="border: 1px #EEE solid;">
      <dd style="background: #00A6FC;color: white;padding: 15px 20px;font-weight: bold;font-size: 1.5rem;">Filtros</dd>
      <dd style="border:1px #eee solid;border-top:0;border-bottom:0;">
        <ul class="filter">
          <li id="filter-none"><a href="#">Mostrar todos</a></li>
        </ul>
      </dd>
    </dl>
    
  </div>
  <div class="nine columns">
    <?php $this->widget("foundation.widgets.FounAlert"); ?>
    <?php echo $content; ?>			
  </div>
</div>

<?php $this->endContent(); ?>