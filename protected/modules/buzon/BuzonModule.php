<?php

/**
 * El modulo buzon provee de funcionalidades para manejar la bandeja de entrada de las
 * notificaciones de los usuarios.
 *
 * Este modulo puede entenderse como la parte visual con la que el usuario interactua para
 * leer, filtrar y contestar sus notificaciones. De por si no existe un sistema de envio
 * directo de mensajes entre usuarios pero puede ser a futuro implementado. Actualmente
 * solo se permite el visualizar las notificaciones, contestarlas y la utilizacion de
 * filtros para seleccionar notificaciones segun entidades (iniciativas o proyectos) en particular.
 *
 * @package   modules.buzon
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class BuzonModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
    {
        $this->setImport(array(
            'buzon.models.*',
            'buzon.components.*',
        ));
    }

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }
}
