<?php

/**
 * Controlador principal del modulo de buzon.
 *
 * Este controlador implementa las acciones principales que permiten a los usuarios tanto
 * ver su bandeja de entrada como tambien ver y contestar mensajes que les han sido
 * enviados.
 *
 * @package   modules.buzon
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class InboxController extends Controller
{
    /**
     * Layout por defecto que utiliza el controlador
     * @var string
     */
    public $layout = '/layouts/column2';

    /**
     * Filtros del controlador.
     *
     * @return  array array de filtros que el controlador utiliza.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controlador.
     *
     * @return  array array de reglas de acceso que el controlador utiliza.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'verMensaje', 'marcarComoContestado'),
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Accion principal del controlador. Despliega la bandeja de entrada de notificaciones
     * del usuario logeado.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Mensaje', array(
            'criteria' => array(
                'condition' => 'destinatario=:id and contestado=0',
                'order' => 'fecha_envio DESC',
                'params' => array(':id' => Yii::app()->user->id))
        ));

        $todos = new CActiveDataProvider('Mensaje', array(
            'criteria' => array(
                'condition' => 'destinatario=:id and contestado=1',
                'order' => 'fecha_envio DESC',
                'params' => array(':id' => Yii::app()->user->id))
        ));

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'todos' => $todos
        ));
    }

    /**
     * Accion que permite a un usuario ver una notificacion desde su bandeja de entrada.
     * Destacar que solamente el usuario receptor del mensaje puede ver cada mensaje
     * desde la bandeja de entrada.
     *
     * @param int $id ID de la notificacion
     *
     * @throws  \CHttpException
     */
    public function actionVerMensaje($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || ($mensaje->emisor != $usuario_id && $mensaje->destinatario != $usuario_id))
            throw new CHttpException(403, 'No tienes los permisos suficientes para ver este mensaje.');

        //se marca el mensaje como leido
        $mensaje->leido = 1;
        //si el mensaje se debe marcar como contestado al leerlo, se actualiza
        if ($mensaje->accion == 'answered_as_read')
            $mensaje->contestado = 1;
        $mensaje->save(false);

        $this->render('ver', array(
            'mensaje' => $mensaje
        ));
    }

    /**
     * Accion que permite que un usuario marque como contestado una notificacion desde la vista de
     * ver notificacion.
     *
     * @param int $id ID de la notificacion
     *
     * @throws  \CHttpException
     */
    public function actionMarcarComoContestado($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || ($mensaje->emisor != $usuario_id && $mensaje->destinatario != $usuario_id))
            throw new CHttpException(403, 'No tienes los permisos suficientes para ver este mensaje.');

        $mensaje->contestado = 1;
        $mensaje->save(false);

        $this->redirect(array('/buzon/inbox/index'));
    }
}