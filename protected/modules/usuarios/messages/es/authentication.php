<?php

return array(
  'username'=>'Nombre de usuario',
  'password'=>'Contrase&ntilde;a',
  'remember_me'=>'Recordarme',
  'incorrect_user_pass_combination' => 'Nombre de usuario o contrase&ntilde;a incorrectos'
);