<?php

return array(
  'user.id'=>'ID',
  'user.email'=>'Correo electr&oacute;nico',
  'user.password'=>'Contrase&ntilde;a',
  'user.nombres'=>'Nombres',
  'user.apellido_paterno'=>'Apellido Paterno',
  'user.apellido_materno'=>'Apellido Materno',
  'user.statusesId'=>'Status',
);