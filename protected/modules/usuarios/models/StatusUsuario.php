<?php

/**
 * Modelo para la tabla "{{status_usuario}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{status_usuario}}':
 * @property string $id
 * @property string $nombre
 * @property string $descripcion
 *
 * Las siguientes son las relaciones disponibles:
 * @property User[] $tblUsers
 */
class StatusUsuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatusUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{status_usuario}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'usuarios' => array(self::MANY_MANY, 'Usuario', '{{usuario_has_status}}(status_usuario_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripci&oacute;n',
		);
	}

}