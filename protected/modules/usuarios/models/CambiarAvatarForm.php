<?php
/**
 * LoginForm class, representa el modelo de ingreso al sistema.
 *
 * Provee el formulario y metodos para autenticar a los
 * usuarios en la plataforma. Es utilizado por la accion
 * login del controlador AutenticateController.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package usuarios.models
 * @since 1.0
 */
class CambiarAvatarForm extends CFormModel
{
	/**
	 * @var string nombre de usuario del usuario de la plataforma. Debe ser unico.
	 */
  public $avatar;
  public $avatar_antiguo;

	/**
	 * Reglas de validacion para el modelo.
	 * Las reglas corresponden a que el nombre de usuario y
   * password deben existir y ser una combinacion valida de
   * un usuario en la db.
	 */
	public function rules()
	{
		return array(
			array('avatar','ImageValidator',
        'minWidth'=>200,'minHeight'=>200,
        'maxWidth'=>800,'maxHeight'=>800,
        'allowEmpty'=>false,'square'=>true
      )
		);
	}

	/**
	 * Labels de cada atributo del modelo.
	 */
	public function attributeLabels()
	{
		return array(
      'avatar'=>'Imagen de perfil'
		);
	}
  
  public function save()
  {
    $usuario_id=Yii::app()->user->id;
    $this->avatar=CUploadedFile::getInstance($this,'avatar');
    $avatar_enviar=time().'_'.$this->avatar;
    $dir = YiiBase::getPathOfAlias('webroot').'/avatars';
    $valid=$this->avatar->saveAs($dir.'/'.$avatar_enviar);
    //se actualiza el campo avatar del perfil del usuario
    $total=0;
    if($valid)
      $total=PerfilUsuario::model()->updateByPk($usuario_id,array('avatar'=>$avatar_enviar));
    if($valid && $total>0)
    {
      //debo eliminar el avatar antiguo
      if(isset($this->avatar_antiguo) && !empty($this->avatar_antiguo))
        unlink($dir.'/'.$this->avatar_antiguo);
      return true;
    }
    return false;
  }

}
