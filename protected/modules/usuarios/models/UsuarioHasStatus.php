<?php

/**
 * Modelo para la tabla "{{usuario_has_status}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{usuario_has_status}}':
 * @property integer $user_id
 * @property string $status_usuario_id
 * @property integer $es_interno
 * @property string $empresa
 * @property integer $ano_egreso
 * @property string $cargo
 */
class UsuarioHasStatus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsuarioHasStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{usuario_has_status}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
      'usuario' => array(self::BELONGS_TO, 'Usuario', 'user_id'),
      'status' => array(self::BELONGS_TO, 'StatusUsuario', 'status_usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'status_usuario_id' => 'Status Usuario',
			'es_interno' => 'Es Interno',
			'empresa' => 'Empresa',
			'ano_egreso' => 'Ano Egreso',
			'cargo' => 'Cargo',
		);
	}
  
  /**
	 * Asigna los campos extras con informacio dependiente del tipo
   * de estado que se posee.
	 */
  public function asignarInfomacionExtra($usuario,$status)
  {
    if($status==1 || $status==2 || $status==3)
    {
      if(isset($usuario->es_interno) && !empty($usuario->es_interno))
        $this->es_interno=$usuario->es_interno;
    }
    if($status==4)
    {
      if(isset($usuario->empresa) && !empty($usuario->empresa))
        $this->empresa=$usuario->empresa;
      if(isset($usuario->ano_egreso) && !empty($usuario->ano_egreso))
        $this->ano_egreso=$usuario->ano_egreso;
      if(isset($usuario->cargo) && !empty($usuario->cargo))
        $this->cargo=$usuario->cargo;
    }
    if($status==5)
    {
      if(isset($usuario->empresa) && !empty($usuario->empresa))
        $this->empresa=$usuario->empresa;
      if(isset($usuario->cargo) && !empty($usuario->cargo))
        $this->cargo=$usuario->cargo;
    }
  }

}