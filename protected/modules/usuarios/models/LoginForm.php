<?php
/**
 * LoginForm class, representa el modelo de ingreso al sistema.
 *
 * Provee el formulario y metodos para autenticar a los
 * usuarios en la plataforma. Es utilizado por la accion
 * login del controlador AutenticateController.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package usuarios.models
 * @since 1.0
 */
class LoginForm extends CFormModel
{
	/**
	 * @var string nombre de usuario del usuario de la plataforma. Debe ser unico.
	 */
  public $username;
  /**
	 * @var string password de acceso del usuario.
	 */
	public $password;
  /**
	 * @var bool recordar ingreso del usuario en futuras sesiones.
	 */
	public $rememberMe;

	private $_identity;

	/**
	 * Reglas de validacion para el modelo.
	 * Las reglas corresponden a que el nombre de usuario y
   * password deben existir y ser una combinacion valida de
   * un usuario en la db.
	 */
	public function rules()
	{
		return array(
			array('username, password', 'required'),
			array('rememberMe', 'boolean'),
			array('password', 'authenticate'),
		);
	}

	/**
	 * Labels de cada atributo del modelo.
	 */
	public function attributeLabels()
	{
		return array(
      'username'=>Yii::t('usuariosModule.authentication','username'),
      'password'=>Yii::t('usuariosModule.authentication','password'),
			'rememberMe'=>Yii::t('usuariosModule.authentication','remember_me'),
		);
	}

	/**
	 * Verifica la identidad del usuario que se desea autenticar.
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password',Yii::t('usuariosModule.authentication','incorrect_user_pass_combination'));
		}
	}

	/**
	 * Autentica al usuario dado un nombre de usuario y password.
   * @return boolean si la autenticacion fue realizada con exito o no.
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0;
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
