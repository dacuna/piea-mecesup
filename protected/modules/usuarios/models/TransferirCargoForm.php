<?php

/**
 * Modelo para la tabla "usuarios".
 *
 * Los siguientes atributos estan disponibles desde la tabla 'usuarios':
 * @property integer $id_usuario
 * @property Persona $usuario
 */
class TransferirCargoForm extends CFormModel
{
    public $id_usuario;
    private $usuario=null;
    private $usuario_que_transfiere=null;
    private $cargo;
    private $entidad;

    public function init(){
        parent::init();
        $this->onAfterValidate=array(app()->mailman,'notificacionHandler');
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id_usuario','required'),
            array('id_usuario','integrantesValidator'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_usuario' => 'Nombre de usuario',
        );
    }

    public function transferirAPersonaNula(){
        $this->cargo->activo=0;
        $this->cargo->asignacion_nula=1;
        return $this->cargo->save();
    }

    //permite notificar al usuario de que se le va a transferir el cargo
    public function notificar_transferencia(){
        $this->onAfterValidate(new CEvent($this,array(
            'tipo'=>'solicitar_transferencia',
            'usuario'=>$this->getUsuario(),
            'cargo'=>$this->cargo,
            'usuario_que_transfiere'=>$this->getUsuarioQueTransfiere(),
            'entidad'=>$this->getEntidad()
        )));
        return true;
    }

    public function integrantesValidator($attribute,$params){
        //el usuario seleccionado debe pertenecer a los integrantes de la entidad
        foreach ($this->entidad->integrantes as $integrante) {
            if($integrante->user_id==$this->$attribute)
                return;
        }
        //verifico que no sea el integrante nulo (para transferencias nulas)
        if($this->$attribute==-1)
            return;
        $this->addError($attribute,'Debes seleccionar a un usuario correcto.');
    }

    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    public function getCargo()
    {
        return $this->cargo;
    }

    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    public function getUsuario()
    {
        if($this->usuario==null)
            $this->usuario=Persona::model()->findByPk($this->id_usuario);
        return $this->usuario;
    }

    public function setUsuarioQueTransfiere($usuario_que_transfiere)
    {
        $usuario=Persona::model()->findByPk($usuario_que_transfiere);
        $this->usuario_que_transfiere = $usuario;
    }

    public function getUsuarioQueTransfiere()
    {
        return $this->usuario_que_transfiere;
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function getEntidad()
    {
        return $this->entidad;
    }

}
