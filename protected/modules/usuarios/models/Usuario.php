<?php

/**
 * Model Class para Usuario.
 *
 * Esta clase extiende a la clase {@link Persona} de la aplicacion general, dicha
 * clase contiene la representacion de la tabla en la db "{{user}}".
 */
class Usuario extends Persona
{
  public $statusesId = array();
  public $es_interno;
  public $empresa;
  public $ano_egreso;
  public $cargo;
  public $contacto_emergencia;
  
  public $repetir_password;
	
  /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Persona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
  
  /**
	 * @return array reglas de validacion para el modelo.
	 */
	public function rules()
	{
		return array(
			array('email, password, nombres, apellido_paterno, apellido_materno', 'required'),
      array('statusesId','required','message'=>'Debes seleccionar al menos un status'),
			array('email, nombres', 'length', 'max'=>45),
			array('apellido_paterno, apellido_materno', 'length', 'max'=>20),
      array('email','email'),
      array('email','unique'),
      array('password', 'compare', 'compareAttribute'=>'repetir_password','on'=>'create'),
      //reglas para los atributos de los status y contacto_emergencia que pertenece a perfil
      //para mas informacion de porque contacto_emergencia aparece aqui ver UserActionsController/create
      array('ano_egreso','numerical','integerOnly'=>true),
      array('es_interno','boolean'),
      array('contacto_emergencia', 'length', 'max'=>45),
      array('statusesId,cargo,repetir_password', 'safe'),
      //puede ser cualquier campo en este validador pues se ejecuta en todos.
      array('empresa','statusValidator'),
		);
	}

	/**
	 * @return array relaciones del modelo.
	 */
	public function relations()
	{
		return array(
      'perfil' => array(self::HAS_ONE, 'PerfilUsuario', 'user_id'),
      'statuses' => array(self::MANY_MANY, 'StatusUsuario', '{{usuario_has_status}}(user_id, status_usuario_id)'),
      'estados' => array(self::HAS_MANY, 'UsuarioHasStatus', 'user_id'),
		);
	}

	/**
	 * @return array Labels de los atributos del modelo.
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('usuariosModule.models','user.id'),
      'email' => Yii::t('usuariosModule.models','user.email'),
			'password' => Yii::t('usuariosModule.models','user.password'),
			'nombres' => Yii::t('usuariosModule.models','user.nombres'),
      'apellido_paterno' => Yii::t('usuariosModule.models','user.apellido_paterno'),
      'apellido_materno' => Yii::t('usuariosModule.models','user.apellido_materno'),
      'statusesId' => Yii::t('usuariosModule.models','user.statusesId'),
      'es_interno' => 'Pertenece a la UTFSM',
      'repetir_password' => 'Repetir Contrase&ntilde;a',
      'ano_egreso'=>'A&ntilde;o egreso',
		);
	}

	/**
	 * Retorna una lista de usuarios basado en los filtro utilizados en la busqueda
	 * @return CActiveDataProvider data provider con los resultados de la busqueda
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  /**
	 * Behaviors del modelo. Se cuenta con los siguientes behaviors:
   *   - CTimeStampBehavior: agrega y actualiza automaticamente los campos de 
   *                         fecha_registro y fecha_actualizacion respectivamente.
   *   - CAdvancedArBehavior: permite manejar inserts de los status de un usuario.
	 * @return array Listado de behaviors del modelo.
	 */
  public function behaviors(){
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'createAttribute' => 'fecha_registro',
        'updateAttribute' => 'fecha_actualizacion',
      ),
      'CAdvancedArBehavior' => array(
        'class' => 'application.extensions.CAdvancedArBehavior')
    );
  }
  
  /**
	 * Encripta la password entregada por el usuario para su almacenamiento en la bd.
	 * @return boolean si se realiza la insercion del usuario en la bd
	 */
	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
        $this->password=sha1($this->password);
        //le debo generar un rol piea al nuevo usuario:
        $this->rol_piea=RolPiea::model()->generarRol();
			}
			return true;
		}
		else
			return false;
	}
  
  /**
	 * Actualiza el generador de roles piea. Probablemente este codigo
   * se pueda encapsular en un behavior.
	 */
	public function afterSave()
	{
		parent::afterSave();
    if($this->isNewRecord)
      RolPiea::model()->actualizarGenerador();
	}
  
  /**
	 * Workaround para mantener luego de una validacion fallida los statuses
   * que ha ingresado un usuario en el formulario de registro.
	 */
  public function afterFind()
  {
    if (!empty($this->statuses))
    {
      foreach($this->statuses as $n => $status)
      {
          $this->statusesId[] = $status->id;
          if($status->nombre=='Alumno pregrado' || $status->nombre=='Alumno postgrado' || $status->nombre=='Profesor')
          {
            $link=UsuarioHasStatus::model()->findByPk(array('user_id'=>$this->id,'status_usuario_id'=>$status->id));
            if($link!=null)
              $this->es_interno=$link->es_interno;
          }
          elseif($status->nombre=='Exalumno utfsm')
          {
            $link=UsuarioHasStatus::model()->findByPk(array('user_id'=>$this->id,'status_usuario_id'=>$status->id));
            if($link!=null)
            {
              $this->empresa=$link->empresa;
              $this->ano_egreso=$link->ano_egreso;
              $this->cargo=$link->cargo;
            }
          }
          else
          {
            $link=UsuarioHasStatus::model()->findByPk(array('user_id'=>$this->id,'status_usuario_id'=>$status->id));
            if($link!=null)
            {
              $this->empresa=$link->empresa;
              $this->cargo=$link->cargo;
            }
          }
      }
      
      if(!$this->isNewRecord)
      {
        $this->contacto_emergencia=$this->perfil->contacto_emergencia;
      }
    }
    
    if(!empty($this->es_interno))

    parent::afterFind();
  }
  
  /**
   * Valida las reglas del negocio segun el tipo de status
   * que un usuario ha seleccionado. Las reglas son (por status):
   *   - Alumno de pregrado: debe existir es_interno y contacto_emergencia
   *   - Alumno de postgrado: debe existir es_interno y contacto_emergencia
   *   - Profesor: debe existir campo es_interno
   *   - Exalumno UTFSM: debe existir empresa, cargo y ano_egreso
   *   - Externo: debe existir empresa y cargo
  */
  public function statusValidator($attribute,$params)
  {
    //validacion para alumno de pregrado y postgrado
    if(is_array($this->statusesId))
    {
      if(in_array('1',$this->statusesId) || in_array('2',$this->statusesId))
      {
        if($this->es_interno==null || empty($this->es_interno))
          $this->addError('es_interno', 'Debes ingresar si eres alumno interno o externo a la UTFSM');
        if($this->contacto_emergencia==null || empty($this->contacto_emergencia))
          $this->addError('contacto_emergencia', 'Debes ingresar un contacto de emergencia');
      }
      
      if(in_array('3',$this->statusesId))
      {
        if($this->es_interno==null || empty($this->es_interno))
          $this->addError('es_interno', 'Debe ingresar si es profesor interno o externo a la UTFSM');
      }
      
      if(in_array('4',$this->statusesId))
      {
        if($this->empresa==null || empty($this->empresa))
          $this->addError('empresa', 'Debes ingresar una empresa');
        if($this->cargo==null || empty($this->cargo))
          $this->addError('cargo', 'Debes ingresar un cargo');
        if($this->ano_egreso==null || empty($this->ano_egreso))
          $this->addError('ano_egreso', 'Debes ingresar un a&ntilde;o de egreso de la UTFSM');
      }
      
      if(in_array('5',$this->statusesId))
      {
        if($this->empresa==null || empty($this->empresa))
          $this->addError('empresa', 'Debes ingresar una empresa');
        if($this->cargo==null || empty($this->cargo))
          $this->addError('cargo', 'Debes ingresar un cargo');
      }
    }
  }
  
}