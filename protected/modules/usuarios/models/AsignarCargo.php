<?php
Yii::import('application.modules.iniciativas.models.*');
Yii::import('application.modules.proyectos.models.*');

/**
 * AsignarCargo: clase que permite realizar asignaciones de cargos en una entidad
 * determinada.
 *
 * Contiene los atributos y realiza las validaciones correspondientes
 * en estos para el formulario de asignacion de cargos. Al nombre de los cargos
 * creados se les anexa el identificador de la entidad, por ejemplo si un cargo
 * lleva por nombre "tesorero" en la iniciativa de id 1, el nombre quedaria
 * "tesorero_1", esto dado que posteriormente se deben obtener los permisos hijos
 * para cada cargo, y se debe diferencia cargos por entidades (en el caso en que hayan
 * dos cargos de tesorero en dos iniciativas distintas no se podria saber cuales son los
 * permisos para cada uno). Al guardar el registro en la db, se debe llamar al metodo save()
 * pasandole como parametro el tipo de entidad y previamente seteando la entidad del
 * modelo.
 *
 * @example   ../index.php
 * @example <br />
 *      $model = new AsignarCargo;;<br />
 *      $model->setEntidad($entidad_id);<br />
 *      if($model->validate())<br />
 *          $model->save(0); //o 1 si es proyecto
 * @version   0.1
 * @since     10/10/12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AsignarCargo extends CFormModel
{
    /**
     * @var string nombre de usuario del usuario de la plataforma. Debe ser unico.
     */
    public $permiso=array();
    public $cargo;
    public $usuario_a_asignar;

    private $lista_permisos;
    private $entidad_id;
    private $entidad;

    /**
     * Reglas de validacion para el modelo.
     * Las reglas corresponden a que el nombre de usuario y
     * password deben existir y ser una combinacion valida de
     * un usuario en la db.
     */
    public function rules()
    {
        return array(
            array('cargo,permiso,usuario_a_asignar','required'),
            array(
                'permiso',
                'ext.ArrayValidator',
                'validator' => 'in',
                'params' => array(
                    'range'=>$this->lista_permisos,
                    'allowEmpty'=>false,
                ),
                'separateParams' => false,
                'allowEmpty' => false,
            ),
            array('cargo','length','max'=>25,'allowEmpty'=>false),
            array('cargo','cargoEnUsoValidator'),
            array('usuario_a_asignar','exist','className'=>'Persona','attributeName'=>'id','allowEmpty'=>false)
        );
    }

    /**
     * Labels de cada atributo del modelo.
     */
    public function attributeLabels()
    {
        return array(
            'permiso'=>'Permiso',
            'cargo'=>'Nombre del cargo',
            'usuario_a_asignar'=>'Usuario a quien se le asigna el cargo'
        );
    }

    public function save($tipo){
        //se debe agregar al authmanager el permiso recien creado
        $auth=am();
        //recordar que se estan seteando varios permisos con solo una accion
        $hijos=array();
        foreach ($this->permiso as $p) {
            array_push($hijos,$auth->getAuthItem($p));
        }

        $tipo_numerico=($tipo=='Iniciativa') ? 0 : 1;
        $nombre_cargo=string_to_underscore_name($this->cargo).'_'.$this->getEntidadId().$tipo_numerico;
        if($tipo=='Iniciativa'){
            $bizRule = 'return Yii::app()->user->verificar_rol("'.$nombre_cargo.'",$params["iniciativa"]);';
            //se debe agregar el lookup de la iniciativa correspondiente
            $lookup=new LookupIniciativa;
            $lookup->nombre_item=$nombre_cargo;
            $lookup->iniciativa_id=$this->getEntidadId();
            $lookup->user_id=$this->usuario_a_asignar;
            $lookup->es_cargo=1;
            //obtengo el cargo del usuario que esta asignando este cargo
            $cargo=IniciativaModel::getCargoDeUsuario($lookup->iniciativa_id,user()->id);
            if($cargo!=null)
                $lookup->cargo_que_asigno=$cargo->nombre_item;
            $lookup->save(false);
        }
        else{
            $bizRule = 'return Yii::app()->user->verificar_rol('.$this->cargo.',$params["iniciativa"],$params["proyecto"]);';
            $lookup=new LookupProyecto;
            $lookup->nombre_item=$nombre_cargo;
            $lookup->proyecto_id=$this->getEntidadId();
            $lookup->user_id=$this->usuario_a_asignar;
            $lookup->es_cargo=1;
            $cargo=ProyectoModel::getCargoDeUsuario($lookup->proyecto_id,user()->id);
            if($cargo!=null)
                $lookup->cargo_que_asigno=$cargo->nombre_item;
            $lookup->save(false);
        }
        //se crea el cargo en el authManager
        $item=$auth->getAuthItem($nombre_cargo);
        if(empty($item)){
            $nuevo_cargo = $auth->createTask($nombre_cargo,$this->cargo,$bizRule);
            //asigno como hijo a cada permiso especificado
            foreach ($hijos as $hijo) {
                $nuevo_cargo->addChild($hijo->getName());
            }
        }
        //se debe asignar el nuevo cargo al usuario especificado
        if($auth->getAuthAssignment($nombre_cargo,$this->usuario_a_asignar)==null){
            $auth->assign($nombre_cargo,$this->usuario_a_asignar);
        }

        return true;
    }

    public function cargoEnUsoValidator($attribute,$params){
        $nombre_cargo=string_to_underscore_name($this->$attribute).'_'.$this->getEntidad()->getId().$this->getEntidad()->getTipoEntidadNumerico();
        if(am()->getAuthItem($nombre_cargo)!=null)
            $this->addError('cargo','El cargo '.$this->$attribute.' ya existe');
    }

    public function setListaPermisos($lista_permisos)
    {
        //solo se necesita la lista con los nombres de los permisos, no las descripciones
        $this->lista_permisos=array();
        foreach ($lista_permisos as $key=>$permiso)
            array_push($this->lista_permisos,$key);
    }

    public function getListaPermisos()
    {
        return $this->lista_permisos;
    }

    public function setEntidadId($entidad_id)
    {
        $this->entidad_id = $entidad_id;
    }

    public function getEntidadId()
    {
        return $this->entidad_id;
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function getEntidad()
    {
        return $this->entidad;
    }

}
