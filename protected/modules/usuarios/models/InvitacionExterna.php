<?php

/**
 * Modelo para la tabla "{{invitacion_externa}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{invitacion_externa}}':
 * @property integer $id
 * @property integer $usuario_invita_id
 * @property string $email_invitado
 * @property string $nombre_invitado
 * @property string $fecha_invitacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $usuarioInvita
 */
class InvitacionExterna extends CActiveRecord
{
	public $tipo_entidad;
  public $entidad;
  
  /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvitacionExterna the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{invitacion_externa}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('email_invitado, nombre_invitado', 'required'),
			array('email_invitado, nombre_invitado', 'length', 'max'=>45),
      array('email_invitado','email'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'usuarioInvita' => array(self::BELONGS_TO, 'Persona', 'usuario_invita_id'),
      'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'entidad_id'),
      'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'entidad_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usuario_invita_id' => 'Usuario Invita',
			'email_invitado' => 'Email Invitado',
			'nombre_invitado' => 'Nombre Invitado',
			'fecha_invitacion' => 'Fecha Invitaci&oacute;n',
		);
	}
  
  public function behaviors(){
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'createAttribute' => 'fecha_invitacion'
      )
    );
  }
  
  public function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
      {
        $this->entidad_id=$this->entidad->id;
        $this->aceptada=0;
      }
			return true;
		}
		else
			return false;
	}
  
  public function afterSave()
  {
    parent::afterSave();
    if($this->isNewRecord)
    {
      if($this->tipo_entidad=='iniciativa')
        $nombre=$this->entidad->nombre_abreviado;
      else
        $nombre=$this->entidad->nombre;
      $mensaje = '
      <h3><a href="http://www.piea.usm.cl">PIE&gt;A::Sistema de gesti&oacute;n y seguimiento
          I+D+i</a></h3>
      <p>
        Estimad@ <strong>'.$this->nombre_invitado
        .'</strong>, <br /> Has sido invitado por '.$this->usuarioInvita->nombreCompleto.' a formar parte del equipo de '.$nombre.' en el sistema de gesti&oacute;n
        de iniciativas y proyectos estudiantiles I+D+i de PIE&gt;A. Para aceptar la invitaci&oacute;n
        <a href="'.Yii::app()->createAbsoluteUrl('/usuarios/userActions/aceptarInvitacion',array('id'=>$this->primaryKey)).'">Presiona aqu&iacute;</a>.
      </p>
      <p>Atte. Equipo PIE&gt;A</p>';

        $message = new YiiMailMessage;
        $message->setBody($mensaje, 'text/html');
        $message->subject = 'Has recibido una invitacion para formar parte del sistema de gestion de PIE>A';
        $message->addTo($this->email_invitado);
        $message->from = array('piea@usm.cl'=>'PIE>A::Gestion I+D+i');
        Yii::app()->mail->send($message);
    }
  }

}