<?php

/**
 * Modelo para la tabla "{{invitacion_externa}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{invitacion_externa}}':
 * @property integer $id
 * @property integer $usuario_invita_id
 * @property string $email_invitado
 * @property string $nombre_invitado
 * @property string $fecha_invitacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $usuarioInvita
 */
class InvitacionInterna extends CFormModel
{
  public $tipo_entidad;
  public $id_entidad;
  public $id_usuario;
  

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
      array('tipo_entidad','in','range'=>array('proyecto','iniciativa'),'allowEmpty'=>false),
      array('id_usuario','exist','className'=>'Persona','attributeName'=>'id','allowEmpty'=>false),
      array('id_entidad','validarEntidad'),
      array('id_usuario','validarInvitacion')
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_usuario' => 'Usuario Invitado',
      'tipo_entidad' => 'Iniciativa/Proyecto',
      'id_entidad' => 'Iniciativa/Proyecto',
    );
  }

  public function validarEntidad($attribute,$params)
  {
    if(!isset($this->$attribute) || empty($this->$attribute))
    {
      $this->addError($attribute,'Debes ingresar una iniciativa o proyecto');
      return;
    }
      
    $entidad=false;
    if($this->tipo_entidad=='proyecto')
      $entidad=Proyecto::model()->findByPk($this->$attribute);
    else if($this->tipo_entidad=='iniciativa')
      $entidad=Iniciativa::model()->findByPk($this->$attribute);
    if($entidad==null || !$entidad)
      $this->addError($attribute,'Debes seleccionar un(a) '.$this->tipo_entidad.' existente.');
  }

  //se debe validar que el usuario que se esta invitando no pertenesca a la entidad
  public function validarInvitacion($attribute,$params)
  {
    $entidad=false;
    if($this->tipo_entidad=='proyecto')
      $entidad='proyecto';
    else if($this->tipo_entidad=='iniciativa')
      $entidad='iniciativa';
    if($entidad)
    {
      $usuario=Persona::model()->findByPk($this->$attribute);
      if($usuario!=null)
      {
        $val=$entidad.'s';
        $integrantes=$usuario->$val;
        foreach($integrantes as $integrante)
          if($integrante->id==$this->id_entidad)
          {
            if($entidad=='proyecto')
              $this->addError($attribute,'El usuario seleccionado ya pertenece al presente proyecto');
            else
              $this->addError($attribute,'El usuario seleccionado ya pertenece a la presente iniciativa');
            return;
          }
      }
    }
  }
}