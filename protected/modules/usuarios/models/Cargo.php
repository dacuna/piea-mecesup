<?php

/**
 * Modelo para la tabla "{{cargo}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{cargo}}':
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property integer $id_usuario
 * @property string $nombre
 * @property integer $lft
 * @property integer $rgt
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $idUsuario
 */
class Cargo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cargo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{cargo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_entidad, tipo_entidad, id_usuario, nombre', 'required'),
			array('id_entidad, tipo_entidad, id_usuario','numerical','integerOnly'=>true),
			array('tipo_entidad','in','range'=>array('0','1'),'allowEmpty'=>false),
			array('id_usuario','exist','className'=>'Persona','attributeName'=>'id','allowEmpty'=>false),
			array('nombre', 'length', 'max'=>45),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'usuario'    => array(self::BELONGS_TO, 'Persona', 'id_usuario'),
			'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'id_entidad'),
			'proyecto'   => array(self::BELONGS_TO, 'Proyecto', 'id_entidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_entidad' => 'Id Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'id_usuario' => 'Id Usuario',
			'nombre' => 'Nombre',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_entidad',$this->id_entidad);
		$criteria->compare('tipo_entidad',$this->tipo_entidad);
		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}