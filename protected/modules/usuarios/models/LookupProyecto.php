<?php

/**
 * Modelo para la tabla "{{lookup_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{lookup_proyecto}}':
 * @property integer $nombre_item
 * @property integer $proyecto_id
 * @property integer $user_id
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $proyecto
 * @property User $user
 */
class LookupProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LookupProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{lookup_proyecto}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'proyecto_id'),
			'usuario' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre_item' => 'Nombre Item',
			'proyecto_id' => 'Proyecto',
			'user_id' => 'Usuario',
		);
	}

}
