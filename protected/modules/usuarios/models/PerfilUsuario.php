<?php

/**
 * Modelo para la tabla "{{perfil_usuario}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{perfil_usuario}}':
 * @property integer $user_id
 * @property string $rut
 * @property string $rol
 * @property string $pasaporte
 * @property string $fecha_nacimiento
 * @property string $telefono_contacto
 * @property string $direccion_laboral
 * @property string $direccion
 * @property string $contacto_emergencia
 * @property string $fecha_actualizacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $user
 */
class PerfilUsuario extends CActiveRecord
{
	//permite definir la presencia de al menos un identificador en el modelo (rut, rol, pasaporte)
  //ademas permite mostrar un mensaje de error en las vistas correspondientes si es que no existe
  //tal presencia
  public $identificadores;
  
  /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerfilUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{perfil_usuario}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('fecha_nacimiento, telefono_contacto', 'required'),
			array('rut, rol', 'length', 'max'=>11),
      array('rut','unique'),
      array('rol','unique'),
			array('pasaporte, direccion_laboral, direccion', 'length', 'max'=>45),
			array('telefono_contacto', 'length', 'max'=>12),
      //validar rol y rut
      array('rol,rut','validateDigitoVerificador','allowEmpty'=>true),
      array('fecha_nacimiento','validatePresenciaIdentificador'),
			array('user_id, rut, rol, pasaporte, fecha_nacimiento, telefono_contacto, direccion_laboral, direccion, contacto_emergencia, fecha_actualizacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'usuario' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'rut' => 'Rut (sin puntos, con gui&oacute;n)',
			'rol' => 'Rol',
			'pasaporte' => 'Pasaporte',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'telefono_contacto' => 'Telefono Contacto',
			'direccion_laboral' => 'Direcci&oacute;n Laboral',
			'direccion' => 'Direcci&oacute;n',
			'contacto_emergencia' => 'Contacto Emergencia',
			'fecha_actualizacion' => 'Fecha Actualizaci&oacute;n',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('rut',$this->rut,true);
		$criteria->compare('rol',$this->rol,true);
		$criteria->compare('pasaporte',$this->pasaporte,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('telefono_contacto',$this->telefono_contacto,true);
		$criteria->compare('direccion_laboral',$this->direccion_laboral,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('contacto_emergencia',$this->contacto_emergencia,true);
		$criteria->compare('fecha_actualizacion',$this->fecha_actualizacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  public function behaviors(){
    return array(
      'CTimestampBehavior' => array(
'class' => 'zii.behaviors.CTimestampBehavior',
'createAttribute' => null,
'updateAttribute' => 'fecha_actualizacion',
      )
    );
  }
  
  public function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
$this->fecha_nacimiento=date('Y-m-d', strtotime($this->fecha_nacimiento));
      else
if(isset($this->fecha_nacimiento) && !empty($this->fecha_nacimiento))
$this->fecha_nacimiento=date('Y-m-d', strtotime($this->fecha_nacimiento));
			return true;
		}
		else
			return false;
	}
  
  /**
   * Valida los campos que poseen un digito verificador. En particular:
   *   - Valida campo rut
   *   - Valida campor rol
   */
  public function validateDigitoVerificador($attribute,$params)
	{
		if($params['allowEmpty'] && (!isset($this->$attribute) || empty($this->$attribute)))
      return;
      
    $r = explode('-',$this->$attribute);
    if(!is_numeric($r[0]))
      $this->addError($attribute,'Debes ingresar un '.$attribute.' v&aacute;lido');
    else
    {
      $s=1;
      for($m=0;$r[0]!=0;$r[0]/=10)
$s=($s+$r[0]%10*(9-$m++%6))%11;
      if(!isset($r[1]) || $r[1] != chr($s?$s+47:75))
$this->addError($attribute,'Debes ingresar un '.$attribute.' correcto');
    }
	}
  
  /**
   * Valida la presencia de al menos un identificador (rut, rol, pasaporte)
   */
  public function validatePresenciaIdentificador($attribute,$params)
	{
		$presence=0;
    if(isset($this->rut) && !empty($this->rut))
      $presence++;
    if(isset($this->rol) && !empty($this->rol))
      $presence++;  
    if(isset($this->pasaporte) && !empty($this->pasaporte))
      $presence++;
    if($presence==0)
      $this->addError('identificadores','Debes ingresar al menos uno de los siguientes campos: rut, rol o pasaporte');
	}

    public function getRutAsArray(){
        $rut=explode("-",$this->rut);
        if(count($rut)>1)
            return $rut;
        return $this->rut;
    }

    public function getCarreraNombre(){
        $carreras=$this->getCarreras();
        if(isset($carreras[$this->carrera_id]))
            return $carreras[$this->carrera_id];
        else
            return "No registra";
    }

    public static function getCarreras(){
return array(
array('id'=>1,'nombre'=>'Ingeniería Civil Matemática'),
array('id'=>2,'nombre'=>'Ingeniería Civil'),
array('id'=>3,'nombre'=>'Construcción Civil'),
array('id'=>4,'nombre'=>'Arquitectura      '),
array('id'=>5,'nombre'=>'Ingeniería Civil Electrónica  '),
array('id'=>6,'nombre'=>'Ingeniería de Ejecución Electrónica'),
array('id'=>7,'nombre'=>'Ingeniería Civil Eléctrica    '),
array('id'=>8,'nombre'=>'Ingeniería de Ejecución Eléctrica'),
array('id'=>9,'nombre'=>'Ingeniería Eléctrica'),
array('id'=>10,'nombre'=>'Ingeniería Electrónica'),
array('id'=>11,'nombre'=>'Ingeniería Civil Telemática   '),
array('id'=>12,'nombre'=>'Ingeniería Mecánica Industrial'),
array('id'=>13,'nombre'=>'Ingeniería Civil Mecánica     '),
array('id'=>14,'nombre'=>'Ingeniería de Ejecución Mecánica '),
array('id'=>15,'nombre'=>'Ingeniería Mecánica Industrial'),
array('id'=>16,'nombre'=>'Ingeniería en Diseño de Productos'),
array('id'=>17,'nombre'=>'Técnico Universitario en Computación e Informática'),
array('id'=>18,'nombre'=>'Técnico Universitario en Proyecto y Diseño Mecánico'),
array('id'=>19,'nombre'=>'Técnico Universitario en Mantenimiento Aeronáutico'),
array('id'=>20,'nombre'=>'Ingeniería de Ejecución Química'),
array('id'=>21,'nombre'=>'Ingeniería Civil Química      '),
array('id'=>22,'nombre'=>'Ingeniería Civil Metalúrgica  '),
array('id'=>23,'nombre'=>'Ingeniería de Ejecución Metalúrgica'),
array('id'=>24,'nombre'=>'Ingeniería Comercial'),
array('id'=>25,'nombre'=>'Piloto Comercial  '),
array('id'=>26,'nombre'=>'Ingeniería Civil Industrial   '),
array('id'=>27,'nombre'=>'Ingeniería Comercial'),
array('id'=>28,'nombre'=>'Ingeniería en Aviación Comercial '),
array('id'=>29,'nombre'=>'Ingeniería de Ejecución en Sistemas de Información'),
array('id'=>30,'nombre'=>'Ingeniería Informática'),
array('id'=>31,'nombre'=>'Ingeniería Civil en Informática'),
array('id'=>32,'nombre'=>'Ingeniería de Ejecución en Informática'),
array('id'=>33,'nombre'=>'Ingeniería Civil Minas y Metalurgia'),
array('id'=>34,'nombre'=>'Químico'),
array('id'=>35,'nombre'=>'Ingeniería en Alimentos'),
array('id'=>36,'nombre'=>'Ingeniería Ambiental'),
array('id'=>37,'nombre'=>'Ingeniería Civil Ambiental    '),
array('id'=>38,'nombre'=>'Magíster en Gestión Empresarial'),
array('id'=>39,'nombre'=>'Magíster en Ingeniería Aeronáutica'),
array('id'=>40,'nombre'=>'Magíster en Tecnologías de Información'),
array('id'=>41,'nombre'=>'Magíster en Gestión de Activos y Mantenimiento'),
array('id'=>42,'nombre'=>'Magíster en Ecnomía Energética'),
array('id'=>43,'nombre'=>'Magíster en Emprendimiento e innovación Tecnológica'),
array('id'=>44,'nombre'=>'Magister en Gestión y Tecnología Agronómica'),
array('id'=>45,'nombre'=>'Magíster en Ciencias de la Ingeniería Eléctrica'),
array('id'=>46,'nombre'=>'Magister en Ciencias de la Ingenieria Telemática'),
array('id'=>47,'nombre'=>'Magíster en Ciencias de la Ingeniería Electrónica'),
array('id'=>48,'nombre'=>'Magister en Electrónica'),
array('id'=>49,'nombre'=>'Magíster en Ciencias de la Ingeniería Mecánica'),
array('id'=>50,'nombre'=>'Doctorado en Ingeniería Mecánica '),
array('id'=>51,'nombre'=>'Magíster en Ciencias de la Ingeniería Química'),
array('id'=>52,'nombre'=>'Doctorado en Ingeniería Electrónica'),
array('id'=>53,'nombre'=>'Magíster en Ciencias de la Ingeniería Informática'),
array('id'=>54,'nombre'=>'Doctorado en Ingeniería Informática'),
array('id'=>55,'nombre'=>'Magíster en Ciencias de la Ingeniería Civil'),
array('id'=>56,'nombre'=>'Magister en Ciencias Mención Física'),
array('id'=>57,'nombre'=>'Magister en Ciencias Mención Matemática'),
array('id'=>58,'nombre'=>'Magister en Ciencias Mención Química'),
array('id'=>59,'nombre'=>'Doctorado en Ciencias mención Física'),
array('id'=>60,'nombre'=>'Doctorado en Ciencias mención Química'),
array('id'=>61,'nombre'=>'Doctorado en Biotecnología    '),
array('id'=>62,'nombre'=>'Doctorado en Ingeniería Química'),
array('id'=>63,'nombre'=>'Ingeniería de Ejecución en Gestión Industrial'),
array('id'=>64,'nombre'=>'Ingeniería de Ejecución en Prevención De Riesgos'),
array('id'=>65,'nombre'=>'Ingeniería de Ejecución en Sistemas Computacionales'),
array('id'=>66,'nombre'=>'Ingeniería de Ejecución en Manufactura'),
array('id'=>67,'nombre'=>'Ingeniería de Ejecución en Control e Instrumentación Industrial'),
array('id'=>68,'nombre'=>'Ingeniería de Ejecución en Proyectos de Ingeniería'),
array('id'=>69,'nombre'=>'Ingeniería de Ejecución en Software'),
array('id'=>70,'nombre'=>'Ingeniería de Ejecución en Mantenimiento Industrial'),
array('id'=>71,'nombre'=>'Ingeniería de Ejecución en Química Mención Control'),
array('id'=>72,'nombre'=>'Técnico Universitario en Electricidad'),
array('id'=>73,'nombre'=>'Técnico Universitario en Electrónica'),
array('id'=>74,'nombre'=>'Técnico Universitario en Telecomunicaciones y Redes'),
array('id'=>75,'nombre'=>'Técnico Universitario en Matricería para Plásticos y Metales'),
array('id'=>76,'nombre'=>'Técnico Universitario en Diseño y Construcción de Matrices'),
array('id'=>77,'nombre'=>'Técnico Universitario en Mecánica Industrial'),
array('id'=>78,'nombre'=>'Ingeniería de Ejecución en Mecánica de Procesos y Mantenimiento Industrial'),
array('id'=>79,'nombre'=>'Técnico Universitario en Mecánica de Procesos y Mantenimiento Industrial'),
array('id'=>80,'nombre'=>'Técnico Universitario en Minería y Metalurgia'),
array('id'=>81,'nombre'=>'Técnico Universitario Industrial Mención en Mantenimiento Industrial'),
array('id'=>82,'nombre'=>'Técnico Universitario Industrial Mención en Operaciones de Plantas Químicas'),
array('id'=>83,'nombre'=>'Técnico Universitario en Mecánica Automotriz'),
array('id'=>84,'nombre'=>'Técnico Universitario en Construmensura'),
array('id'=>85,'nombre'=>'Técnico Universitario en Administración y Control de Obras'),
array('id'=>86,'nombre'=>'Técnico Universitario en Construcción'),
array('id'=>87,'nombre'=>'Técnico Universitario en Edificación'),
array('id'=>88,'nombre'=>'Técnico Universitario en Prevención de Riesgos'),
array('id'=>89,'nombre'=>'Ingeniería de Ejecución en Prevención De Riesgos'),
array('id'=>90,'nombre'=>'Ingeniería en Prevención de Riesgos Laborales y Ambientales'),
array('id'=>91,'nombre'=>'Técnico Universitario en Proyecto y Diseño Mecánico'),
array('id'=>92,'nombre'=>'Técnico Universitario en Proyectos de Ingeniería'),
array('id'=>93,'nombre'=>'Ingeniería en Fabricación y Diseño Industrial'),
array('id'=>94,'nombre'=>'Técnico Universitario en Dibujo y Proyectos Mecánicos'),
array('id'=>95,'nombre'=>'Técnico Universitario en Química Analítica'),
array('id'=>96,'nombre'=>'Técnico Universitario en Control de Alimentos'),
array('id'=>97,'nombre'=>'Técnico Universitario en Control del Medio Ambiente'),
array('id'=>98,'nombre'=>'Técnico Universitario en Química '),
array('id'=>99,'nombre'=>'Técnico Universitario en Programación de Computadores'),
array('id'=>100,'nombre'=>'Técnico Universitario en Informática'),
array('id'=>101,'nombre'=>'Ingeniería de Ejecución en Control e Instrumentación Industrial'),
array('id'=>102,'nombre'=>'Ingeniería de Ejecución en Gestión Industrial'),
array('id'=>103,'nombre'=>'Técnico Universitario en Electrónica'),
array('id'=>104,'nombre'=>'Técnico Universitario en Sistemas Electrónicos'),
array('id'=>105,'nombre'=>'Técnico Universitario en Electrónica Industrial'),
array('id'=>106,'nombre'=>'Técnico Universitario en Programación de Computadores'),
array('id'=>107,'nombre'=>'Técnico Universitario en Computación e Informática'),
array('id'=>108,'nombre'=>'Técnico Universitario en Informática'),
array('id'=>109,'nombre'=>'Técnico Universitario en Electricidad'),
array('id'=>110,'nombre'=>'Técnico Universitario en Automatización y Control'),
array('id'=>1,'nombre'=>'Técnico Universitario en Electrotecnia'),
array('id'=>1,'nombre'=>'Técnico Universitario en Mecánica Estructural'),
array('id'=>1,'nombre'=>'Técnico Universitario en Estructuras'),
array('id'=>1,'nombre'=>'Técnico Universitario en Proyectos y Diseño Estructural'),
array('id'=>1,'nombre'=>'Técnico Universitario en Topografía'),
array('id'=>1,'nombre'=>'Técnico Universitario en Construcción'),
array('id'=>1,'nombre'=>'Técnico Universitario en Construcción Civil'),
array('id'=>1,'nombre'=>'Técnico Universitario Dibujante Proyectista'),
array('id'=>1,'nombre'=>'Técnico Universitario en Proyectos de Ingeniería'),
array('id'=>1,'nombre'=>'Técnico Universitario en Mecánica de Mantenimiento'),
array('id'=>1,'nombre'=>'Técnico Universitario en Mecánica Industrial'),
array('id'=>1,'nombre'=>'Técnico Universitario en Mecánica Automotriz Diesel'),
array('id'=>1,'nombre'=>'Técnico Universitario en Mecánica Automotriz'),
array('id'=>1,'nombre'=>'Técnico Universitario Sidero Metalurgia'),
array('id'=>1,'nombre'=>'Técnico Universitario en Química Analítica'),
array('id'=>1,'nombre'=>'Técnico Universitario en Química Industrial'),
array('id'=>1,'nombre'=>'Técnico Universitario en Control de Alimentos'),
array('id'=>1,'nombre'=>'Técnico Universitario en Química '),
array('id'=>1,'nombre'=>'Técnico Universitario en Prevención de Riesgos'),
array('id'=>1,'nombre'=>'Ingeniería en Prevención de Riesgos Laborales y Ambientales'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Prevención de Riesgos'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Mecánica de Procesos y Mantenimiento Industrial'),
array('id'=>1,'nombre'=>'Técnico Universitario en Industrias Madereras'),
array('id'=>1,'nombre'=>'Técnico Universitario Administrativo'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención en Sistemas Eléctricos De Control'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención Mecánica de Mantenimiento'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención Operación Plantas de Procesos Industriales '),
array('id'=>1,'nombre'=>'Ingeniería en Construcción    '),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Gestión de la Calidad'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Software'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Gestión y Control Ambiental '),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Proyectos Estructurales'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Mantenimiento Industrial'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución Química Mención Control'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención Control Industrial'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención en Mantenimiento Industrial'),
array('id'=>1,'nombre'=>'Técnico Universitario Industrial Mención Operación Plantas Plantas Químicas'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Gestión Industrial'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Control e Instrumentación Industrial'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Proyectos de Ingeniería'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Mantenimiento Industrial'),
array('id'=>1,'nombre'=>'Ingeniería de Ejecución en Prevención De Riesgos'),

);
    }
  
}