<?php

/**
 * Modelo para la tabla "usuarios".
 *
 * Los siguientes atributos estan disponibles desde la tabla 'usuarios':
 * @property integer $idUsuarios
 * @property string $Username
 * @property string $Password
 * @property string $Nombre
 * @property string $Email
 */
class NotificacionPorEmailForm extends CFormModel
{
  public $por_email;

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
      array('por_email','required'),
      array('por_email','in','range'=>array('0','1'),'allowEmpty'=>false),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'por_email'=>'&iquestDeseas recibir las notificaciones del sistema a tu correo electr&oacute;nico?'
    );
  }
  
  public function save()
  {
    return PerfilUsuario::model()->updateByPk(Yii::app()->user->id,array('recibir_email'=>$this->por_email));
  }

  public function afterConstruct() 
  {
    $perfil=PerfilUsuario::model()->findByPk(Yii::app()->user->id);
    $this->por_email=$perfil->recibir_email; 

    return parent::afterConstruct();
  }
  
}
