<?php

/**
 * Modelo para la tabla "usuarios".
 *
 * Los siguientes atributos estan disponibles desde la tabla 'usuarios':
 * @property integer $idUsuarios
 * @property string $Username
 * @property string $Password
 * @property string $Nombre
 * @property string $Email
 */
class RecuperarPasswordForm extends CFormModel
{
  public $mail;

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
      array('mail','exist','allowEmpty'=>false,'attributeName'=>'email','className'=>'Persona'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'mail' => 'Email'
    );
  }
  
}
