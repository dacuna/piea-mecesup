<?php

/**
 * Modelo para la tabla "usuarios".
 *
 * Los siguientes atributos estan disponibles desde la tabla 'usuarios':
 * @property integer $idUsuarios
 * @property string $Username
 * @property string $Password
 * @property string $Nombre
 * @property string $Email
 */
class CambiarPassForm extends CFormModel
{
  public $password_original;
  public $password_nueva;
  public $password_repeat;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('password_original,password_nueva,password_repeat', 'required'),
      array('password_original','passwordExists'),
			array('password_nueva', 'length', 'max'=>255),
      array('password_nueva', 'compare', 'compareAttribute'=>'password_repeat'),
      array('password_repeat', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'password_original' => 'Contrase&ntilde;a actual',
      'password_nueva' => 'Nueva contrase&ntilde;a',
      'password_repeat' => 'Repetir nueva contrase&ntilde;a',
		);
	}
  public function passwordExists($attribute,$params)
  {
    $user = Usuario::model()->findByPk(Yii::app()->user->id);
    if($user->password != sha1($this->$attribute))
      $this->addError($attribute, 'La contrase&ntilde;a actual es incorrecta.');
  }
  
}
