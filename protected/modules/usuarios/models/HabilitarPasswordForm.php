<?php

/**
 * Modelo para la tabla "usuarios".
 *
 * Los siguientes atributos estan disponibles desde la tabla 'usuarios':
 * @property integer $idUsuarios
 * @property string $Username
 * @property string $Password
 * @property string $Nombre
 * @property string $Email
 */
class HabilitarPasswordForm extends CFormModel
{
  public $password_nueva;
  public $password_repeat;

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
      array('password_nueva,password_repeat', 'required'),
      array('password_nueva', 'length', 'max'=>255),
      array('password_nueva', 'compare', 'compareAttribute'=>'password_repeat'),
      array('password_repeat', 'safe'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'password_nueva' => 'Nueva contrase&ntilde;a',
      'password_repeat' => 'Repetir nueva contrase&ntilde;a',
    );
  }
  
}
