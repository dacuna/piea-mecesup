<?php

/**
 * Modelo para la tabla "{{lookup_iniciativa}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{lookup_iniciativa}}':
 * @property integer $nombre_item
 * @property integer $iniciativa_id
 * @property integer $user_id
 *
 * Las siguientes son las relaciones disponibles:
 * @property Iniciativa $iniciativa
 * @property User $user
 */
class LookupIniciativa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LookupIniciativa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{lookup_iniciativa}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'iniciativa_id'),
			'usuario' => array(self::BELONGS_TO, 'Persona', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre_item' => 'Nombre Item',
			'iniciativa_id' => 'Iniciativa',
			'user_id' => 'Usuario',
		);
	}

}
