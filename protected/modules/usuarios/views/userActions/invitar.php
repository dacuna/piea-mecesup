<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Invitar a un usuario',
);

?>

<h1>Invitar a un usuario</h1>

<p>
  Puedes invitar a un usuario al sistema, para ello debes ingresar el nombre y el email del usuario
  a invitar. A este le llegar&aacute; una invitaci&oacute;n al sistema. Estas invitando al usuario
  a participar de 
  <?php if($tipo_entidad=='iniciativa'):?>
    la iniciativa <strong><?php echo $entidad->nombre_abreviado;?></strong>.
  <?php else:?>
    el proyecto <strong><?php echo $entidad->nombre;?></strong>.
  <?php endif;?>
</p>

<div class="row">
  <div class="six columns form">

  <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
    'id'=>'usuario-form',
    'type'=>'nice',
    'enableAjaxValidation'=>false,
  )); ?>

    <fieldset>
        <h5>Datos del usuario a invitar</h5>
        <p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>
        <?php echo $form->textFieldRow($model, "email_invitado"); ?>
        <?php echo $form->textFieldRow($model, "nombre_invitado"); ?>   
        
        <?php echo CHtml::submitButton('Invitar',array('class'=>'nice radius medium button')); ?><br><br>
    </fieldset>

  <?php $this->endWidget(); ?>

  </div><!-- form -->

  <div class="six columns">
    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'interna-form',
        'type'=>'nice',
      )); ?>
      <fieldset>
        <h5>Invitar usuario interno</h5>
        <p>Puedes invitar a un usuario registrado en la plataforma ingresando
        su nombre. Luego, a este, le llegar&aacute; una invitaci&oacute;n con los datos correspondientes.</p>

        <?php echo $form->errorSummary($interna);?>
        
        <label for="usuario_invitar">Nombre usuario:</label><?php 
          $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
              'attribute'=>'usuario_invitar',
                'sourceUrl'=>array('/usuarios/userActions/buscarIntegrante'),
                'name'=>'invitar-interno[nombre_usuario]',
                'options'=>array(
                  'minLength'=>'3',
                  'showAnim'=>'fold',
                  'select'=>"js: function(event, ui) {
                    $('#id_invitar').val(ui.item['id']);
                  }"
                ),
                'htmlOptions'=>array(
                  'size'=>45,
                  'maxlength'=>45,
                  'style'=>'height:24px;'
                ),
        )); ?>
        <?php if($tipo_entidad=='iniciativa'):?>
          <input type="hidden" name="invitar-interno[tipo_entidad]" value="iniciativa">
          <input type="hidden" name="invitar-interno[id_entidad]" value="<?php echo $entidad->id;?>">
        <?php else:?>
          <input type="hidden" name="invitar-interno[tipo_entidad]" value="proyecto">
          <input type="hidden" name="invitar-interno[id_entidad]" value="<?php echo $entidad->id;?>">
        <?php endif;?>
        <input type="hidden" name="invitar-interno[id_usuario]" id="id_invitar"><br><br>
        <?php echo CHtml::submitButton('Invitar',array('class'=>'nice radius medium button','name'=>'invitar-interno[send]')); ?><br><br>
      </fieldset>
    <?php $this->endWidget(); ?>
  </div>
</div>
<script>
    $(function(){
       $('#interna-form').submit(function(){
            $('input[type=submit]').attr('disabled','disabled');
       });
    });
</script>