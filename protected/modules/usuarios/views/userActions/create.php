<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Formulario de registro',
);

?>

<h1>Formulario de registro</h1>

<?php if(isset($invitacion) && !empty($invitacion)):?>
  Est&aacute;s siendo invitado a participar en
  <?php if($invitacion->tipo_entidad=='iniciativa'):?>
    la iniciativa <?php echo $invitacion->iniciativa->nombre_abreviado;?>.
  <?php else:?>
    el proyecto <?php echo $invitacion->proyecto->nombre;?>
  <?php endif;?>
<?php endif;?>

<div class="form">

<?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
	'id'=>'usuario-form',
  'type'=>'nice',
	'enableAjaxValidation'=>true,
  'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

	<?php if($perfil->hasErrors('identificadores')):?>
  <div class="row">
    <div class="seven columns">
      <div class="alert-box error">
        <?php echo $perfil->getError('identificadores'); ?>
        <a href="" class="close">&times;</a>
      </div>
    </div>
  </div>
  <?php endif;?>
  
  <div class="row">
    <div class="seven columns">
      <fieldset>
          <h5>Datos de acceso</h5>
          <p>El email ser&aacute; tu nombre de usuario en el sistema.</p>
          <?php echo $form->textFieldRow($model, "email"); ?>
          <?php echo $form->passwordFieldRow($model, "password"); ?> 
          <?php echo $form->passwordFieldRow($model, "repetir_password"); ?>    
      </fieldset>
    </div>
  </div>
  
  <div class="row">
    <div class="seven columns">
      <fieldset>
          <h5>Datos personales</h5>
          <?php echo $form->textFieldRow($model, "nombres"); ?> 
          <?php echo $form->textFieldRow($model, "apellido_paterno"); ?>
          <?php echo $form->textFieldRow($model, "apellido_materno"); ?>
          <?php echo $form->textFieldRow($perfil, "rut"); ?>
          <p style="margin-bottom: 10px;">*Si agregas tu rut entonces no es necesario agregar un pasaporte.</p>
          <?php echo $form->textFieldRow($perfil, "pasaporte"); ?>
          <?php echo $form->labelEx($perfil,'fecha_nacimiento'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$perfil,
            'attribute'=>'fecha_nacimiento',
            'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy',
                        'changeYear'=>'true','yearRange'=> '1940:2012'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($perfil,'fecha_nacimiento'); ?>
          </div>
          
          <?php echo $form->textFieldRow($perfil, "telefono_contacto"); ?>
          <?php echo $form->textFieldRow($perfil, "direccion"); ?> 
      </fieldset>
    </div>
  </div>
  
  <div class="row">
    <div class="seven columns">
      <fieldset>
          <h5>Datos acad&eacute;micos o laborales</h5>
          <?php echo $form->textFieldRow($perfil, "rol"); ?>
          <?php echo $form->textFieldRow($perfil, "direccion_laboral"); ?>
          <?php echo $form->checkBoxListRow($model, "statusesId",CHtml::listData(StatusUsuario::model()->findAll(),'id', 'nombre')); ?>
          
          <div class="hide pregrado profesor">
            <?php echo $form->radioButtonListRow($model,'es_interno',array(1=>'Si', 0=>'No'),array('separator'=>' '));?>
          </div>

          <div class="hide pregrado">
              <?php echo $form->dropDownListRow($perfil, 'carrera_id',
                  CHtml::listData(PerfilUsuario::getCarreras(), 'id', 'nombre')); ?>
          </div>
          
          <div class="hide externo exalumno">
            <?php echo $form->textFieldRow($model, "empresa"); ?>
            <?php echo $form->textFieldRow($model, "cargo"); ?>
          </div>
          
          <div class="hide exalumno">
            <?php echo $form->textFieldRow($model, "ano_egreso"); ?>
          </div>
          
          <div class="hide pregrado">
            <?php echo $form->textFieldRow($model, "contacto_emergencia"); ?>          
          </div>
      </fieldset>
    </div>
  </div>

	<div class="row">
    <div class="seven columns">
      <?php echo CHtml::submitButton($model->isNewRecord ? 'Registrarme' : 'Guardar',array('class'=>'nice radius medium button')); ?>
    </div>
  </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
$(function(){
  //para pregrado
  if($('#Usuario_statusesId_0').is(':checked'))
    $('.pregrado').removeClass('hide');
  
  $('#Usuario_statusesId_0').click(function(){
    if($(this).is(':checked'))
      $('.pregrado').removeClass('hide');
    else
      $('.pregrado').addClass('hide');
  });
  
  //para postgrado
  if($('#Usuario_statusesId_1').is(':checked'))
    $('.pregrado').removeClass('hide');
  
  $('#Usuario_statusesId_1').click(function(){
    if($(this).is(':checked'))
      $('.pregrado').removeClass('hide');
    else
      $('.pregrado').addClass('hide');
  });
  
  //para profesor
  if($('#Usuario_statusesId_2').is(':checked'))
    $('.profesor').removeClass('hide');
  
  $('#Usuario_statusesId_2').click(function(){
    if($(this).is(':checked'))
      $('.profesor').removeClass('hide');
    else
      $('.profesor').addClass('hide');
  });
  
  //para exalumno
  if($('#Usuario_statusesId_3').is(':checked'))
    $('.exalumno').removeClass('hide');
  
  $('#Usuario_statusesId_3').click(function(){
    if($(this).is(':checked'))
      $('.exalumno').removeClass('hide');
    else
      $('.exalumno').addClass('hide');
  });
  
  //para externo
  if($('#Usuario_statusesId_4').is(':checked'))
    $('.externo').removeClass('hide');
  
  $('#Usuario_statusesId_4').click(function(){
    if($(this).is(':checked'))
      $('.externo').removeClass('hide');
    else
      $('.externo').addClass('hide');
  });
});
</script>