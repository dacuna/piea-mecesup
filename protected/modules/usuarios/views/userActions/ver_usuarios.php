<?php
$this->breadcrumbs=array(
    'Usuarios del sistema'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Usuarios del sistema</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>El siguiente listado corresponde a todos los usuarios registrados en el sistema. Puedes ver el
            perfil de cada uno presionando en "ver".</p>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'persona-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    'id',
                    'email',
                    'nombres',
                    'apellido_paterno',
                    'apellido_materno',
                    'perfil.rol',
                    'perfil.rut',
                    array(
                        'class'=>'CLinkColumn',
                        'label'=>'ver',
                        'header'=>'Acci&oacute;n',
                        'urlExpression'=>'array("/usuarios/perfil/index", "id"=>$data->id)',
                    ),
                    array(
                        'class'=>'CLinkColumn',
                        'label'=>'Asignar permisos',
                        'header'=>'',
                        'urlExpression'=>'array("/usuarios/cargos/asignarPermisosEspeciales", "user_id"=>$data->id)',
                    ),
                ),
            )); ?>

        </div>



    </li>

</ul>