<p>Estimado usuario:</p>
Recientemente solicitaste recuperar tu contrase&ntilde;a en el sistema de control y seguimiento de iniciativas y proyectos del
PIE&gt;A, para continuar con dicha acci&oacute;n debes presionar en el enlace que aparece a continuaci&oacute;n. Luego, podr&aacute;s
ingresar una nueva contrase&ntilde;a de ingreso. 
<br><br>
Si no solicitaste una nueva contrase&ntilde; omite este email.
<br><br>
Para cambiar tu contrase&ntilde; presiona en el siguiente enlace: 
<a href="<?php echo Yii::app()->createAbsoluteUrl('/usuarios/perfil/nuevaPassword',array('id'=>$usuario->id,'token'=>$usuario->token));?>">
  Cambiar contrase&ntilde;a
</a>
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.