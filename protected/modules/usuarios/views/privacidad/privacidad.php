<?php
$this->pageTitle = 'Privacidad';
$this->breadcrumbs=array(
	'Perfil'=>array('/usuarios/miPerfil'),
  'Privacidad'
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Privacidad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    <div class="panel clearfix">
      <h5>Visibilidad</h5>
      <p>Puedes seleccionar la <strong>visibilidad</strong> de tu participaci&oacute;n en proyectos e
       iniciativas mediante los siguientes formularios.</p>
       
      <p>Cuando la casilla de <strong>visibilidad</strong> se encuentra marcada quiere decir que aparecer&aacute;s
      como integrante del proyecto/iniciativa en los listados del sistema.</p>
      
      <div class="row">
        <div class="six columns">
          <?php if(count($iniciativas)!=0):?>
            <?php echo CHtml::beginForm(); ?>
            <table>
            <thead><th>Iniciativa</th><th>Visibilidad</th></thead>
            <?php foreach($iniciativas as $i=>$iniciativa): ?>
            <tr>
            <td><?php echo $iniciativa->iniciativa->nombre_abreviado;?></td>
            <td><?php echo CHtml::activeCheckBox($iniciativa,"[$i]visibilidad"); ?></td>
            </tr>
            <?php endforeach; ?>
            </table>
            <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius medium button')); ?>
            <?php echo CHtml::endForm(); ?>
          <?php endif;?>
        </div>
        
        <div class="six columns">
          <?php if(count($proyectos)!=0):?>
            <?php echo CHtml::beginForm(); ?>
            <table>
            <thead><th>Proyecto</th><th>Visibilidad</th></thead>
            <?php foreach($proyectos as $i=>$proyecto): ?>
            <tr>
            <td><?php echo $proyecto->proyecto->nombre;?></td>
            <td><?php echo CHtml::activeCheckBox($proyecto,"[$i]visibilidad"); ?></td>
            </tr>
            <?php endforeach; ?>
            </table>
            <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius medium button')); ?>
            <?php echo CHtml::endForm(); ?>
          <?php endif;?>
        </div>
      </div>
      
    </div>

    <div class="panel clearfix">
      <h5>Notificaciones</h5>
      <p>
        <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
          'id'=>'usuario-form',
          'type'=>'nice'
        )); ?>

          <?php echo $form->errorSummary($notificaciones); ?>

          <?php echo $form->radioButtonListRow($notificaciones,'por_email',array(1=>'Si', 0=>'No'),array('separator'=>' '));?>
          <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius medium button')); ?>

        <?php $this->endWidget(); ?>
      </p>
    </div>
    
  </li>
</ul>