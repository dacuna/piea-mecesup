<?php
$this->pageTitle = 'Listar evaluaciones FAE';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'FAE'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Listar evaluaciones FAE</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <p>A continuaci&oacute;n, se listan todos los integrantes que poseen una evaluaci&oacute;n FAE
                registrada. Puedes tambi&eacute;n editar la evaluaci&oacute;n. Si deseas agregar a un nuevo
                usuario a la evaluaci&oacute;n FAE, debes dirigirte a la pesta&ntilde;a de "Integrantes".
            </p>

            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Integrante</th>
                        <th>Competencia 1</th>
                        <th>Competencia 2</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($integrantes as $integrante):?>
                        <?php $fae=$entidad->obtenerEvaluacionFaeIntegrante($integrante->usuario->id);?>
                        <?php if($fae!=null):?>
                        <tr>
                            <td><?php echo $integrante->id;?></td>
                            <td><?php echo $integrante->usuario->nombreCompleto;?></td>
                            <td><?php echo $fae->competenciaUnoTexto();?></td>
                            <td><?php echo $fae->competenciaDosTexto();?></td>
                            <td>Editar</td>
                        </tr>
                        <?php endif;?>
                    <?php endforeach;?>
                </tbody>
            </table>

        </div>

    </li>
</ul>
