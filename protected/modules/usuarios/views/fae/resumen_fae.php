<?php
$this->pageTitle = 'FAE';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'FAE'
);
?>

<dl class="nice contained tabs">
    <?php if($entidad->getTipoEntidadNumerico()==1 && $entidad->getEntidad()->es_postulacion==1 && $entidad->getEntidad()->perfilPostulacion->estado_postulacion!=2):?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>">Ver Proyecto</a></dd>
    <?php else:?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>">Ver Proyecto</a></dd>
        <dd>
            <a href="<?php echo Yii::app()->createUrl('/tareas/actividadesEntidad/verActividades', array('id' => $entidad->id, 'tipo' => $entidad->getTipoEntidadNumerico()));?>">Actividades</a>
        </dd>
        <dd><a href="<?php echo Yii::app()->createUrl('/proyectos/perfil/integrantes', array('id' => $entidad->id));?>">Integrantes</a>
        </dd>
        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <dd>
                <a class="active" href="<?php echo Yii::app()->createUrl('/usuarios/fae/resumenFae', array('id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));?>">FAE</a>
            </dd>
        <?php endif;?>
    <?php endif;?>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Enlaces r&aacute;pidos:</h5>
            <ul>
                <li><a href="<?php echo url('/usuarios/fae/listarEvaluaciones',array('id'=>$entidad->getId(),'tipo'=>
                    $entidad->getTipoEntidadNumerico()));?>">Ver detalle de evaluaciones</a></li>
            </ul>
            <p>A continuaci&oacute;n, se listan todos los integrantes junto con el estado de su evaluaci&oacute;n
                FAE si es que poseen una.
            </p>

            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Integrante</th>
                        <th>Email</th>
                        <th>Estado</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($integrantes as $integrante):?>
                    <?php $fae=$entidad->obtenerEvaluacionFaeIntegrante($integrante->usuario->id);?>
                    <tr>
                        <td><?php echo $integrante->usuario->id;?></td>
                        <td><?php echo $integrante->usuario->nombreCompleto;?></td>
                        <td><?php echo $integrante->usuario->email;?></td>
                        <td>
                            <?php if($fae==null):?>
                                <a href="<?php echo url('/usuarios/fae/evaluarIntegrante',array('id'=>$entidad->id,
                                'tipo'=>$entidad->getTipoEntidadNumerico(),'i'=>$integrante->usuario->id));?>">
                                    Sin evaluaci&oacute;n (Registrar una aqu&iacute;)</a>
                            <?php else: ?>
                                <a href="<?php echo url('/usuarios/fae/verEvaluacion',array('id'=>$fae->id));?>">
                                    Evaluaci&oacute;n en curso</a>
                            <?php endif;?>
                        </td>
                        <td>
                            <?php if($fae!=null):?>
                            <a href="<?php echo url('/usuarios/fae/eliminarEvaluacion',array('id'=>$fae->id));?>">Eliminar</a>
                            <?php endif;?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>

            <?php if($entidad->getTipoEntidadNumerico()==0):?>
                <p>Los siguientes son los usuarios junto con el estado de su evaluaci&oacute;n FAE de los proyectos
                    relacionados a esta iniciativa.</p>
                <table>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Integrante</th>
                        <th>Email</th>
                        <th>Proyecto</th>
                        <th>Estado</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($integrantes_proyecto as $integrante):?>
                        <?php $fae=$integrante->proyecto->obtenerEvaluacionFaeIntegrante($integrante->usuario->id);?>
                        <tr>
                            <td><?php echo $integrante->usuario->id;?></td>
                            <td><?php echo $integrante->usuario->nombreCompleto;?></td>
                            <td><?php echo $integrante->usuario->email;?></td>
                            <td><?php echo $integrante->proyecto->nombre;?></td>
                            <td>
                                <?php if($fae==null):?>
                                    <a href="<?php echo url('/usuarios/fae/evaluarIntegrante',array('id'=>$integrante->proyecto->id,
                                        'tipo'=>1,'i'=>$integrante->usuario->id));?>">
                                        Sin evaluaci&oacute;n (Registrar una aqu&iacute;)</a>
                                <?php else: ?>
                                    <a href="<?php echo url('/usuarios/fae/verEvaluacion',array('id'=>$fae->id));?>">
                                        Evaluaci&oacute;n en curso pero pendiente de revisi&oacute;n (click para revisar y aprobar)</a>
                                <?php endif;?>
                            </td>
                            <td>
                                <?php if($fae!=null):?>
                                    <a href="<?php echo url('/usuarios/fae/eliminarEvaluacion',array('id'=>$fae->id));?>">Eliminar</a>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>

        </div>

    </li>
</ul>
