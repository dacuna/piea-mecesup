<?php
$this->pageTitle = 'FAE';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'FAE'=>array('/usuarios/fae/resumenFae','id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()),
    'Ver evaluaci&oacute;n'
);
?>

<dl class="nice contained tabs">
    <?php if($entidad->getTipoEntidadNumerico()==1 && $entidad->getEntidad()->es_postulacion==1 && $entidad->getEntidad()->perfilPostulacion->estado_postulacion!=2):?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>">Ver Proyecto</a></dd>
    <?php else:?>
        <dd><a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>">Ver Proyecto</a></dd>
        <dd>
            <a href="<?php echo Yii::app()->createUrl('/tareas/actividadesEntidad/verActividades', array('id' => $entidad->id, 'tipo' => $entidad->getTipoEntidadNumerico()));?>">Actividades</a>
        </dd>
        <dd><a href="<?php echo Yii::app()->createUrl('/proyectos/perfil/integrantes', array('id' => $entidad->id));?>">Integrantes</a>
        </dd>
        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <dd>
                <a class="active" href="<?php echo Yii::app()->createUrl('/usuarios/fae/resumenFae', array('id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));?>">FAE</a>
            </dd>
        <?php endif;?>
    <?php endif;?>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Ver evaluaci&oacute;n FAE</h5>

            <p>
                Estas viendo la evaluaci&oacuten; FAE del usuario <?php echo $evaluacion->evaluado->nombreCompleto;?>.
            </p>

            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Integrante</th>
                    <th>Competencia 1</th>
                    <th>Competencia 2</th>
                    <th>Acci&oacute;n</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $evaluacion->evaluado->id;?></td>
                        <td><?php echo $evaluacion->evaluado->nombreCompleto;?></td>
                        <td><?php echo $evaluacion->competenciaUnoTexto();?></td>
                        <td><?php echo $evaluacion->competenciaDosTexto();?></td>
                        <?php if($evaluacion->estado==0 && $evaluacion->id_evaluado!=user()->id):?>
                            <td><a href="<?php echo url('/usuarios/fae/aprobarEvaluacion',array('id'=>$evaluacion->id));?>">
                                    Aprobar</a></td>
                        <?php endif;?>
                    </tr>
                </tbody>
            </table>
        </div>
    </li>
</ul>
