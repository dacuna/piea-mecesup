<?php
$this->pageTitle = 'Evaluar FAE integrante';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'FAE' => array('/usuarios/fae/listarEvaluaciones','id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()),
    'Evaluar FAE Integrante'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Evaluar FAE Integrante</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <p>Mediante el siguiente formulario puedes evaluar o asignar una evaluaci&oacute;n FAE a un integrante.
            </p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('type'=>'nice',)); ?>
                <?php echo $form->errorSummary($evaluacion);?>

                <strong>Estas evaluando al integrante: <?php echo $usuario->nombreCompleto;?></strong>

                <?php echo $form->dropDownListRow($evaluacion,'competencia_uno',CHtml::listData(
                    EvaluacionFae::getCompetencias(),'id','nombre'),array('encode'=>false,'style'=>'width:100%;')); ?>
                <?php echo $form->dropDownListRow($evaluacion,'competencia_dos',CHtml::listData(
                    EvaluacionFae::getCompetencias(),'id','nombre'),array('encode'=>false,'style'=>'width:100%;')); ?>
                <br>
                <?php echo CHtml::submitButton('Agregar Evaluaci&oacute;n FAE',array('class'=>'nice radius medium button','encode'=>false)); ?>
                <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" class="nice radius medium red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>