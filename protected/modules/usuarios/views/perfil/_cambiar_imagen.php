<div class="form">
  <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
    'id'=>'avatar-form',
    'type'=>'nice',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'action'=>Yii::app()->createUrl('/usuarios/perfil/cambiarImagen')
  )); ?>

  <div class="row">
    <div class="twelve columns">
      <fieldset>
        <h5>Cambiar imagen de perfil</h5>
        <p>Mediante el siguiente formulario puedes cambiar tu imagen de perfil. Esta debe
        tener como m&iacute;nimo un tama&ntilde;o de 200*200px.</p>
        <?php echo $form->fileFieldRow($model, "avatar"); ?>  
        <p>
          <br>
          <?php echo CHtml::submitButton('Cambiar Imagen',array('class'=>'nice radius small button')); ?>
        </p>        
      </fieldset>   
    </div>
  </div>
  
  <?php $this->endWidget(); ?>
</div>