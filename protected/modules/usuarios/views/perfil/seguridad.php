<?php
$this->pageTitle = 'Seguridad';
$this->breadcrumbs=array(
	'Perfil'=>array('/usuarios/miPerfil'),
  'Seguridad'
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Seguridad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    <div class="panel clearfix">
      <h5>Seguridad</h5>
      <p>Puedes cambiar tu contrase&ntilde;a mediante el siguiente formulario.</p>
      
      <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'cambiarPassForm-form',
        'type'=>'nice',
      )); ?>
      <?php echo $form->passwordFieldRow($cambiarPassForm, "password_original"); ?>
      <?php echo $form->passwordFieldRow($cambiarPassForm, "password_nueva"); ?>
      <?php echo $form->passwordFieldRow($cambiarPassForm, "password_repeat"); ?>
      <?php echo CHtml::submitButton('Cambiar Contrase&ntilde;a',array('class'=>'nice radius medium button','encode'=>false)); ?>
      <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/miPerfil');?>" class="nice radius medium red button">Cancelar</a>
      <?php $this->endWidget(); ?>
    </div>
    
  </li>
</ul>