<?php
$this->pageTitle = 'Cambiar Password';
$this->breadcrumbs=array('');
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Cambiar contrase&ntilde;a</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    
    <div class="panel">
      
      <h5>Formulario de recuperaci&oacute;n de contrase&ntilde;a</h5>

      <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
          'id'=>'pass-form',
          'type'=>'nice',
          'enableAjaxValidation'=>true,
        )); ?>
        
      <?php echo $form->passwordFieldRow($model, "password_nueva"); ?> 
      <?php echo $form->passwordFieldRow($model, "password_repeat"); ?> 
    
      <?php echo CHtml::submitButton('Cambiar Contrase&ntilde;a',array('encode'=>false,'class'=>'nice radius medium button')); ?>

    </div>
    
    <?php $this->endWidget(); ?>
    
  </li>
</ul>