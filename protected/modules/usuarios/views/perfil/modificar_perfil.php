<?php
$this->pageTitle = 'Ver perfil';
$this->breadcrumbs=array(
	'Usuarios'=>array('/usuarios/index'),
	'Modificar perfil',
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Modificar perfil</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    
    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'usuario-form',
        'type'=>'nice',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data')
      )); ?>
      
    <?php if($perfil->hasErrors('identificadores')):?>
    <div class="row">
      <div class="seven columns">
        <div class="alert-box error">
          <?php echo $perfil->getError('identificadores'); ?>
          <a href="" class="close">&times;</a>
        </div>
      </div>
    </div>
    <?php endif;?>
    
    <div class="panel clearfix">
      <h5>Datos de usuario</h5>
      <p style="float:left;margin-right:10px;">
        <a href="#" data-reveal-id="myModal">
        <?php if(!empty($perfil->avatar)):?>
          <img src="<?php echo Yii::app()->request->baseUrl;?>/avatars/<?php echo $perfil->avatar;?>" width="150" height="150" class="has-tip" title="Presiona aqu&iacute; para cambiar tu imagen" data-width="210">
        <?php else:?>
          <img src="<?php echo Yii::app()->request->baseUrl;?>/images/no-picture.jpg" width="150" height="150" class="has-tip" title="Presiona aqu&iacute; para cambiar tu imagen" data-width="210">
        <?php endif;?>
        </a>
      </p>
      <p style="float:left;">
        <?php echo $form->textFieldRow($usuario, "nombres"); ?> 
        <?php echo $form->textFieldRow($usuario, "apellido_paterno"); ?>
        <?php echo $form->textFieldRow($usuario, "apellido_materno"); ?>
      </p>
    </div>
    
    <div class="row">
      <div class="six columns">
        <div class="panel">
          <h5>Datos personales</h5>
          <?php echo $form->textFieldRow($perfil, "rut"); ?>
          <?php echo $form->textFieldRow($perfil, "pasaporte"); ?>
          <?php echo $form->labelEx($perfil,'fecha_nacimiento'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$perfil,
            'attribute'=>'fecha_nacimiento',
            'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy',
                        'changeYear'=>'true','yearRange'=> '1940:2012',),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($perfil,'fecha_nacimiento'); ?>
          </div>
          
          <?php echo $form->textFieldRow($perfil, "telefono_contacto"); ?>
          <?php echo $form->textFieldRow($perfil, "direccion"); ?> 
        </div>
      </div>
      
      <div class="six columns">
        <div class="panel">
          <h5>Datos acad&eacute;micos</h5>
          <?php echo $form->textFieldRow($perfil, "rol"); ?>
          <?php echo $form->textFieldRow($perfil, "direccion_laboral"); ?>
          <?php echo $form->checkBoxListRow($usuario, "statusesId",CHtml::listData(StatusUsuario::model()->findAll(),'id', 'nombre')); ?>
          <?php echo $form->radioButtonListRow($usuario,'es_interno',array(1=>'Si', 0=>'No'),array('separator'=>' '));?>
          <?php echo $form->textFieldRow($usuario, "empresa"); ?>
          <?php echo $form->textFieldRow($usuario, "ano_egreso"); ?>
          <?php echo $form->textFieldRow($usuario, "cargo"); ?>
          <?php echo $form->textFieldRow($usuario, "contacto_emergencia"); ?> 
        </div>
      </div>
    </div>
    
    <div class="panel">
      <?php echo CHtml::submitButton('Actualizar datos',array('class'=>'nice radius medium button')); ?>
      <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/miPerfil');?>" class="nice radius medium red button">Cancelar</a>
      <br><br>
    </div>
    
    <?php $this->endWidget(); ?>
    
  </li>
</ul>

<?php
$this->modal['id']='myModal';
$this->modal['close']=true;
$this->modal['content']=$this->renderPartial('_cambiar_imagen',array('model'=>$cambiar_imagen),true)
?>
