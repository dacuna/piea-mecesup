<?php
$this->pageTitle = 'Recuperar Password';
$this->breadcrumbs=array('');
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Recuperar contrase&ntilde;a</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    
    <div class="panel">
      
      <h5>Formulario de recuperaci&oacute;n de contrase&ntilde;a</h5>

      <p>Para recuperar tu contrase&ntilde;a debes ingresar tu email en el siguiente formulario. Luego, se te enviar&aacute; un
        correo electr&oacute;nico con todas las instrucciones para que puedas volver a ingresar a la plataforma.</p>
      <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
          'id'=>'pass-form',
          'type'=>'nice',
          'enableAjaxValidation'=>true,
        )); ?>
        
      <?php echo $form->textFieldRow($model, "mail"); ?> 
    
      <?php echo CHtml::submitButton('Recuperar Contrase&ntilde;a',array('encode'=>false,'class'=>'nice radius medium button')); ?>

    </div>
    
    <?php $this->endWidget(); ?>
    
  </li>
</ul>