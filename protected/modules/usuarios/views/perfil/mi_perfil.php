<?php
$this->pageTitle = 'Ver perfil';
$this->breadcrumbs=array(
	'Usuarios'=>array('/usuarios/index'),
	'Mi perfil',
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Mi perfil</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-perfil">
    <div class="panel clearfix">
      <h5>Datos de usuario</h5>
      <p style="float:left;margin-right:10px;">
        <a href="#" data-reveal-id="myModal">
        <?php if(!empty($perfil->avatar)):?>
          <img src="<?php echo Yii::app()->request->baseUrl;?>/avatars/<?php echo $perfil->avatar;?>" width="100" height="100" class="has-tip" title="Presiona aqu&iacute; para cambiar tu imagen" data-width="210">
        <?php else:?>
          <img src="<?php echo Yii::app()->request->baseUrl;?>/images/no-picture.jpg" width="100" height="100" class="has-tip" title="Presiona aqu&iacute; para cambiar tu imagen" data-width="210">
        <?php endif;?>
        </a>
      </p>
      <p style="float:left;">
        <strong>Nombre de usuario</strong>: <?php echo $usuario->email;?><br>
        <strong>Nombre</strong>: <?php echo $usuario->nombreCompleto;?><br>
        <strong>Rol PIE&gt;A</strong>: <?php echo $usuario->rol_piea;?><br>
        <strong>Usuario desde el</strong>: <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y",$usuario->fecha_registro);?><br>
        <strong>Carrera</strong>: <?php echo $perfil->getCarreraNombre();?>
      </p>
      <p style="float:right;text-align:center;">
        <a style="margin-bottom:5px;" href="<?php echo Yii::app()->createUrl('/usuarios/perfil/modificarPerfil');?>" class="small nice button">Modificar Datos</a><br>
        <a style="margin-bottom:5px;" href="<?php echo Yii::app()->createUrl('/usuarios/privacidad');?>" class="small nice button">Privacidad</a><br>
        <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/seguridad');?>" class="small nice button">Seguridad</a>
      </p>
    </div>

    <div class="row">
      <div class="six columns">
        <div class="panel">
          <h5>Datos personales</h5>
          <p>
            <?php if(isset($perfil->rut) && !empty($perfil->rut)):?>
              <strong>Rut</strong>: <?php echo $perfil->rut;?><br>
            <?php endif;?>

            <?php if(isset($perfil->rol) && !empty($perfil->rol)):?>
              <strong>Rol</strong>: <?php echo $perfil->rol;?><br>
            <?php endif;?>

            <?php if(isset($perfil->fecha_nacimiento)):?>
              <strong>Fecha de nacimiento</strong>: <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y",$perfil->fecha_nacimiento);?><br>
            <?php endif;?>
          </p>
        </div>
      </div>

      <div class="six columns">
        <div class="panel">
          <h5>Informaci&oacute;n de contacto</h5>
          <p>
            <strong>Tel&eacute;fono</strong>: <?php echo $perfil->telefono_contacto;?><br>
            <?php if(isset($perfil->direccion_laboral) && !empty($perfil->direccion_laboral)):?>
              <strong>Direcci&oacute;n laboral</strong>: <?php echo $perfil->direccion_laboral;?><br>
            <?php endif;?>
            <?php if(isset($perfil->direccion) && !empty($perfil->direccion)):?>
              <strong>Direcci&oacute;n</strong>: <?php echo $perfil->direccion;?><br>
            <?php endif;?>
           <?php if(isset($perfil->contacto_emergencia) && !empty($perfil->contacto_emergencia)):?>
              <strong>Contacto de emergencia</strong>: <?php echo $perfil->contacto_emergencia;?><br>
            <?php endif;?>
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="six columns">
        <div class="panel">
          <h5>Iniciativas</h5>
          <br>
          <ul>
            <?php foreach($usuario->iniciativas as $iniciativa):?>
              <li><a href="<?php echo Yii::app()->createUrl('/iniciativas/crudIniciativas/ver',array('id'=>$iniciativa->id));?>"><?php echo $iniciativa->nombre_abreviado;?></a></li>
            <?php endforeach;?>
          </ul>
        </div>
      </div>

      <div class="six columns">
        <div class="panel">
          <h5>Proyectos</h5>
          <br>
          <?php if(count($usuario->proyectos)!=0):?>
          <ul>
            <?php foreach($usuario->proyectos as $proyecto):?>
              <li><a href="<?php echo Yii::app()->createUrl('/proyectos/crud/ver',array('id'=>$proyecto->id));?>"><?php echo $proyecto->nombre;?></a></li>
            <?php endforeach;?>
          </ul>
          <?php else:?>
          <p>No registras actividad en ning&uacute;n proyecto.</p>
          <?php endif;?>
        </div>
      </div>
    </div>

      <div class="row">
          <div class="twelve columns">
              <div class="panel">
                  <h5>Cargos iniciativas</h5>
                  <br>
                  <table>
                      <thead>
                      <tr>
                      <th>Cargo</th>
                      <th>Iniciativa</th>
                      <th>Permisos</th>
                      <th>Eliminar</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach($finales_iniciativas as $cargo):?>
                      <tr>
                          <td><?php echo $cargo[0]->getDescription();?></td>
                          <td><?php echo $cargo[1]->iniciativa->nombre_abreviado;?></td>
                          <td><?php array_walk($cargo[0]->getChildren(),
                              create_function('$item,$key,$size','echo $item->getDescription();if($key<$size-1) echo ", ";'),count($cargo[0]->getChildren()));?></td>
                          <td><a href="<?php echo url('/usuarios/cargos/eliminarCargo',array('id'=>$cargo[1]->iniciativa_id,'tipo'=>0,'cargo'=>$cargo[1]->nombre_item,'from'=>1));?>">Eliminar</a></td>
                      </tr>
                          <?php endforeach;?>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

      <div class="row">
          <div class="twelve columns">
              <div class="panel">
                  <h5>Cargos proyectos</h5>
                  <br>
                  <table>
                      <thead>
                      <tr>
                          <th>Cargo</th>
                          <th>Iniciativa</th>
                          <th>Permisos</th>
                          <th>Eliminar</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach($finales_proyectos as $cargo):?>
                      <tr>
                          <td><?php echo $cargo[0]->getDescription();?></td>
                          <td><?php echo $cargo[1]->proyecto->nombre;?></td>
                          <td><?php array_walk($cargo[0]->getChildren(),
                              create_function('$item,$key,$size','echo $item->getDescription();if($key<$size-1) echo ", ";'),count($cargo[0]->getChildren()));?></td>
                          <td><a href="<?php echo url('/usuarios/cargos/eliminarCargo',array('id'=>$cargo[1]->proyecto_id,'tipo'=>1,'cargo'=>$cargo[1]->nombre_item,'from'=>1));?>">Eliminar</a></td>
                      </tr>
                          <?php endforeach;?>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </li>
</ul>

<?php
$this->modal['id']='myModal';
$this->modal['close']=true;

$this->modal['content']=$this->renderPartial('_cambiar_imagen',array('model'=>$cambiar_imagen),true)
?>