<?php
$this->pageTitle = 'Cargos';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Cargos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Cargos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Cargos</h5>
            <p>Puedes asignar cargos a otros integrantes mediante el siguiente formulario.</p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('type'=>'nice',)); ?>
            <?php echo $form->errorSummary($model);?>
            <?php echo $form->textFieldRow($model, 'cargo'); ?>
            <?php echo $form->checkBoxListRow($model,'permiso',$permisos); ?>
            <?php echo $form->dropDownListRow($model,'usuario_a_asignar',$integrantes); ?>
            <br>
            <?php echo CHtml::submitButton('Asignar Cargo',array('class'=>'nice radius medium button','encode'=>false)); ?>
            <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" class="nice radius medium red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>
<script>
$(function(){
   var first=$('input[value=permiso_nulo_iniciativa]');
   var second=$('input[value=permiso_nulo_proyecto]');
    $('<br><p><b>Otros permisos<b></p>').insertBefore(first);
    $('<br><p><b>Otros permisos<b></p>').insertBefore(second);

    $('input[type=checkbox]').change(function(){
        if($(first).is(':checked') || $(second).is(':checked')){
          $('input[type=checkbox]').each(function(index,elem){
            if($(elem).val()!='permiso_nulo_iniciativa' && $(elem).val()!='permiso_nulo_proyecto'){
              $(elem).attr('disabled',true);
              $(elem).attr('checked',false);
            }
          });
        }
        else{
          $('input[type=checkbox]').each(function(index,elem){
            $(elem).removeAttr('disabled');
          });
        }
    });
});
</script>