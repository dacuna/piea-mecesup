<?php
$this->pageTitle = 'Transferir cargo';
$this->breadcrumbs=array(
    $entidad->tipoEntidad=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    $entidad->getNombre()=>array($entidad->urlEntidad(),'id'=>$entidad->id),
    'Transferir cargo'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Transferir cargo</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-perfil">
        <div class="panel clearfix">
            <h5>Transferir cargo</h5>
            <p>Se est&aacute; transfiriendo el cargo <b><?php echo am()->getAuthItem($cargo->nombre_item)->getDescription();?></b> del integrante
                <?php echo $cargo->usuario->nombrePresentacion;?>. Para continuar debes seleccionar a alg&uacute;n otro integrante
            al que se le transferir&aacute; el cargo. Este integrante, deber&aacute; aceptar la transferencia antes de que
            esta se haga efectiva.</p>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('type'=>'nice'));?>
                <?php echo $form->errorSummary($model);?>
                <?php echo $form->dropDownListRow($model,'id_usuario',$integrantes); ?>
            <br><br>
                <?php echo CHtml::submitButton('Transferir Cargo',array('class'=>'nice radius small button','encode'=>false)); ?>
                <a href="<?php echo Yii::app()->createUrl($entidad->urlEntidad(),array('id'=>$entidad->id));?>" class="nice radius small red button">Cancelar</a>
            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>