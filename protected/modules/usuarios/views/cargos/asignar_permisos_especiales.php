<?php
$this->breadcrumbs=array(
    'Usuarios del sistema'=>array('/usuarios/userActions/verUsuarios'),
    'Asignar permisos especiales a usuario'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Asignar permisos especiales a usuario</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>
                Estas asign&aacute;ndole permisos al usuario <strong><?php echo $usuario->nombreCompleto;?></strong>.
                Para asignar un permiso, selecciona el permiso de la siguiente lista y luego presiona en "Asignar Permiso".

                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('type'=>'nice',)); ?>

                <input type="checkbox" name="permiso[0]" value="0" <?php if($has[0]!=false){echo 'checked="checked"';};?>> Ver proyectos postulantes Fondos I+D+i<br>
                <input type="checkbox" name="permiso[1]" value="1" <?php if($has[1]!=false){echo 'checked="checked"';};?>> Ver proyectos enviados Fondos I+D+i<br>
                <input type="checkbox" name="permiso[2]" value="2" <?php if($has[2]!=false){echo 'checked="checked"';};?>> Cerrar postulaci&oacute;n Fondos I+D+i<br>
                <input type="checkbox" name="permiso[3]" value="3" <?php if($has[3]!=false){echo 'checked="checked"';};?>> Abrir periodo de revisi&oacute;n Fondos I+D+i<br>
                <input type="checkbox" name="permiso[4]" value="4" <?php if($has[4]!=false){echo 'checked="checked"';};?>> Asignar permisos de revisi&oacute;n a usuarios
                para el fondo I+D+i<<br>
                <input type="checkbox" name="permiso[5]" value="5" <?php if($has[5]!=false){echo 'checked="checked"';};?>> Observador de proyectos postulantes
                a fondos I+D+i<br>
                <input type="checkbox" name="permiso[6]" value="6" <?php if($has[6]!=false){echo 'checked="checked"';};?>> Ayudante administrativo<br>
                <input type="checkbox" name="permiso[7]" value="7" <?php if($has[7]!=false){echo 'checked="checked"';};?>> Ver estado de revisiones por proyecto en los fondos I+D+i<br>
                <input type="checkbox" name="permiso[8]" value="8" <?php if($has[8]!=false){echo 'checked="checked"';};?>> Ver usuarios del sistema<br>
                <br>
                <input type="hidden" name="permiso[sent]" value="1">
                <?php echo CHtml::submitButton('Asignar Permiso',array('class'=>'nice radius medium button','encode'=>false)); ?>

                <?php $this->endWidget(); ?>
            </p>

        </div>

    </li>

</ul>