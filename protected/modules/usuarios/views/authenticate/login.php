<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Iniciar sesi&oacute;n',
);
?>

<h1>Iniciar Sesi&oacute;n</h1>

<p>Por favor, ingresa tus datos de acceso para poder entrar en el sistema.</p>

<div class="form">
<?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
	'id'=>'login-form',
  'type'=>'nice',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->textFieldRow($model, 'username'); ?> 
  <?php echo $form->passwordFieldRow($model, 'password'); ?>
  <?php echo $form->checkboxRow($model, 'rememberMe'); ?>    

	<div class="row buttons">
		<?php echo CHtml::submitButton('Iniciar Sesi&oacute;n',array('class'=>'nice medium radius blue button','encode'=>false)); ?>
	</div>

    <p>
        <a href="<?php echo Yii::app()->createUrl('/usuarios/userActions/create');?>">&iquest;No tienes cuenta? puedes registrarte aqu&iacute;.</a>
        <br>
        <a href="<?php echo Yii::app()->createUrl('/usuarios/perfil/recuperarPassword');?>">Recuperar contrase&ntilde;a</a>
    </p>

    <?php $this->endWidget(); ?>
</div><!-- form -->
