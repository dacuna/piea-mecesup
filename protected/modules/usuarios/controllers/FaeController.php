<?php
Yii::import('application.modules.formularios.models.FormularioPiea');
Yii::import('application.modules.formularios.models.ProcesoFormulario');
/**
 * Controlador que contiene las acciones y metodos que permiten realizar las tareas de asignacion, evaluacion y
 * generacion de informes respecto al FAE de los alumnos.
 *
 * En las entidades, los alumnos son factibles de evaluacion para obtener FAE, el procedimiento de evaluacion
 * comienza con la persona encargada de la entidad donde el usuario que quiere obtener FAE pertenece, por ejemplo
 * el coordinador de una iniciativa es el encargado de evaluar a los integrantes de esta (en relacion al FAE) y
 * en el caso de los proyectos el jefe de proyecto es el encargado. El sistema ademas es capaz de generar un conjunto
 * de reportes de apoyo a la gestion que permiten principalmente oficializar la evaluacion del FAE.
 *
 * @version   0.01
 * @since     5/5/13
 * @author    dacuna <diego.acuna@usm.cl>
 */
class FaeController extends Controller {

    /**
     * Accion que permite ver un resumen del estado de los integrantes de una entidad en relacion con su
     * evaluacion FAE (si es que poseen una). Se listan todos los usuarios dando la posibilidad de
     * asginar FAE a quienes no lo tengan y de editar los que si lo tengan. Esta accion es valida solo para
     * el superior a cargo de la entidad.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad (0=iniciativa, 1=proyecto)
     *
     * @return  void
     * @throws  \ChttpException
     *
     * @since   06-05-2013
     */
    public function actionResumenFae($id,$tipo){
        $fae=FormularioPiea::formularioFAE();
        if($fae->procesoFAE()!=null){
            $entidad = new Entidad($id, $tipo);
            if($entidad->checkPermisos('encargado',user()->id)){
                //si es el lider de una iniciativa entonces debe poder ver los fae de tambien todos los integrantes
                //de los proyectos inlcuidos
                $integrantes_proyecto=null;
                $entidad2=null;
                if($tipo==0){
                    $por_proyecto=$entidad->getEntidad()->proyectos;
                    $integrantes_proyecto=array();
                    foreach ($por_proyecto as $proyecto) {
                        foreach ($proyecto->integrantes as $integrante) {
                            $integrantes_proyecto[]=$integrante;
                        }
                    }
                }
                $this->render('resumen_fae',array(
                    'entidad'=>$entidad,
                    'integrantes'=>$entidad->getIntegrantes(),
                    'integrantes_proyecto'=>$integrantes_proyecto
                ));
            }
        }
    }

    /**
     * Accion que permite listar todas las evaluaciones FAE de todos los integrantes de una entidad en
     * particular. Se listan todos los integrantes y a su lado si es que han recibido una evaluacion o
     * no. Si no han recibido entonces se presenta un enlace para agregar una evaluacion. Si el usuario
     * si ha recibido evaluacion entonces se presenta un enlace para ver y modificar la evaluacion.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad (0=iniciativa, 1=proyecto)
     *
     * @return  void
     * @throws  \ChttpException
     *
     * @since   06-05-2013
     */
    public function actionListarEvaluaciones($id,$tipo){
        $fae=FormularioPiea::formularioFAE();
        if($fae->procesoFAE()!=null){
            $entidad = new Entidad($id, $tipo);
            if($entidad->checkPermisos('encargado',user()->id)){

                $this->render('listar_evaluaciones',array(
                   'entidad'=>$entidad,
                   'integrantes'=>$entidad->getIntegrantes()
                ));
            }
        }
    }

    /**
     * Accion que permite al encargado de una entidad el asignar una evaluacion (FAE) a un integrante en
     * particular de la entidad. Si el usuario ya esta siendo evaluado en el year actual entonces no se
     * permite la evaluacion.
     *
     * @param int $id ID de la entidad
     * @param int $tipo Tipo de la entidad (0=iniciativa, 1=proyecto)
     * @param int $i ID del usuario a evaluar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   05-05-2013
     */
    public function actionEvaluarIntegrante($id,$tipo,$i){
        $fae=FormularioPiea::formularioFAE();
        if($fae->procesoFAE()!=null && $fae->estado==1){
            $entidad = new Entidad($id, $tipo);
            $encargado = $entidad->getEncargado();
            if($encargado->id != user()->id && !user()->checkAccess('administrador'))
                throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos necesarios para ver la
                p&aacute;gina que has solicitado.");
            //ok, se realiza la evaluacion
            $usuario = Persona::load($i);
            $check=$entidad->obtenerEvaluacionFaeIntegrante($usuario->id);

            if($check!=null){
                user()->setFlash("error","El usuario seleccionado ya posee una evaluaci&oacute;n FAE en curso.");
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->id));
            }

            $evaluacion = new EvaluacionFae;
            if(isset($_POST['EvaluacionFae'])){
                $evaluacion->attributes=$_POST['EvaluacionFae'];
                if($evaluacion->validate()){
                    $evaluacion->id_entidad=$entidad->getId();
                    $evaluacion->tipo_entidad=$entidad->getTipoEntidadNumerico();
                    $evaluacion->id_evaluador=user()->id;
                    $evaluacion->id_evaluado=$usuario->id;
                    //las evaluaciones ingresadas por el coordinador de una iniciativa son aprobadas inmediatamente
                    $superior=$entidad->getSuperior();
                    if($tipo==0 || ($tipo==1 && $superior->id==user()->id)) $evaluacion->estado=1;
                    $evaluacion->save(false);
                    user()->setFlash("success","Se ha ingresado correctamente la evaluaci&oacute;n FAE de
                        {$usuario->nombrePresentacion}.");
                    $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->id));
                }
            }

            $this->render('evaluar_integrante',array(
                'entidad'=>$entidad,
                'evaluacion'=>$evaluacion,
                'usuario'=>$usuario
            ));
        }
    }

    /**
     * Accion que despliega la informacion relacionada con una evaluacion FAE de un integrante en particular de una
     * entidad en particular. Los usuarios que pueden visualizar esta evaluacion son los superiores de la entidad (ya
     * sea jefe de proyecto, coordinador iniciativa y administrador o coordinador de iniciativa y administrador). Ademas,
     * si la evaluacion es de un proyecto entonces debe ser aprobada por el coordinador de la iniciativa (si es que no
     * fue el quien la registro).
     *
     * @param int $id ID de la evaluacion FAE a observar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   30-05-2013
     */
    public function actionVerEvaluacion($id){
        $evaluacion=EvaluacionFae::model()->load($id);
        $entidad=new Entidad($evaluacion->id_entidad,$evaluacion->tipo_entidad);
        if($entidad->checkPermisos('encargado',user()->id) || $evaluacion->id_evaluado==user()->id){
            $this->render('ver_evaluacion',array(
               'entidad'=>$entidad,
               'evaluacion'=>$evaluacion
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ejecutar
            esta acci&oacute;n. Por favor, no vuelvas a realizar esta consulta.");
    }

    /**
     * Accion que permite al lider una entidad eliminar una evaluacion relacionada con un integrante de la entidad
     * donde es lider o tiene permisos (por ejemplo un lider de iniciativa sobre un proyecto). La evaluacion FAE es
     * eliminada por completo (borrado fisico de la DB).
     *
     * @param int $id ID evaluacion
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   30-05-2013
     */
    public function actionEliminarEvaluacion($id){
        $evaluacion=EvaluacionFae::model()->load($id);
        $entidad=new Entidad($evaluacion->id_entidad,$evaluacion->tipo_entidad);
        if($entidad->checkPermisos('encargado',user()->id)){
            if($evaluacion->delete())
                user()->setFlash("success","La evaluacion ha sido eliminada con &eacute;xito.");
            else
                user()->setFlash("error","Ha ocurrido un error al eliminar la evaluaci&oacute;n. Por favor, int&eacute;talo
                m&aacute;s tarde.");
            $this->redirect(array('/usuarios/fae/resumenFae','id'=>$entidad->id,'tipo'=>$entidad->getTipoEntidadNumerico()));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ejecutar
            esta acci&oacute;n. Por favor, no vuelvas a realizar esta consulta.");
    }

    public function actionAprobarEvaluacion($id){
        $evaluacion=EvaluacionFae::model()->load($id);
        $entidad=new Entidad($evaluacion->id_entidad,$evaluacion->tipo_entidad);
        if($entidad->checkPermisos('encargado',user()->id)){
            $evaluacion->estado=1;
            if($evaluacion->save())
                user()->setFlash("success","La evaluacion ha sido aprobada con &eacute;xito.");
            else
                user()->setFlash("error","Ha ocurrido un error al aprobar la evaluaci&oacute;n. Por favor, int&eacute;talo
                m&aacute;s tarde.");
            $this->redirect(array('/usuarios/fae/verEvaluacion','id'=>$evaluacion->id));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ejecutar
            esta acci&oacute;n. Por favor, no vuelvas a realizar esta consulta.");
    }

    /**
     * Accion que permite al administrador del sistema, generar el archivo excel con el FAE de todos los
     * usuarios del sistema (que poseen evaluacion FAE).
     *
     * @param int $todas Si se desean agregar las evaluaciones aun no aprobadas (1=si, 0=no, 0 default)
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   06-05-2013
     */
    public function actionGenerarExcelEvaluacion($todas=0){
        if(user()->checkAccess('administrador')){
            $actual_year=date("Y");
            if($todas==1)
                $data=EvaluacionFae::model()->findAll("fecha_evaluacion BETWEEN '{$actual_year}-01-01' AND '{$actual_year}-12-31'");
            else
                $data=EvaluacionFae::model()->findAll("estado=1 and fecha_evaluacion BETWEEN '{$actual_year}-01-01' AND '{$actual_year}-12-31'");

            $table=array();
            foreach ($data as $eval) {
                $table[]=array('year'=>$actual_year,'semestre'=>1,'nom_sede'=>$eval->entidad->campusSegunFae,
                    'cod_sede'=>$eval->entidad->campusCodigoSegunFae,'nom_depto'=>'PIEA','cod_depto'=>110,
                    'nombre_actividad'=>$eval->entidad->getNombre(),'cod_actividad'=>1,'competencia1'=>$eval->competencia_uno,
                    'competencia2'=>$eval->competencia_dos,'rut'=>$eval->evaluado->perfil->rutAsArray[0],
                    'dv_rut'=>$eval->evaluado->perfil->rutAsArray[1],'paterno'=>$eval->evaluado->apellido_paterno,
                    'materno'=>$eval->evaluado->apellido_materno,'nombres'=>$eval->evaluado->nombres,
                    'nom_evaluador'=>'Olga Godoy','rut_eval'=>123445,'dv_eval'=>1);
            }

            $data=new CArrayDataProvider($table, array(
                'id'=>'fae-provider',
                'keyField'=>false
            ));

            $this->toExcel($data,
                array(
                    'year::Año',
                    'semestre',
                    'nom_sede',
                    'cod_sede',
                    'nom_depto',
                    'cod_depto',
                    'nombre_actividad',
                    'cod_actividad',
                    'competencia1',
                    'competencia2',
                    'rut',
                    'dv_rut',
                    'paterno',
                    'materno',
                    'nombres',
                    'nom_evaluador',
                    'rut_eval',
                    'dv_eval',
                ),
                'Evaluacion FAE',
                array(
                    'creator' => 'Zen',
                ),
                'Excel2007' // This is the default value, so you can omit it. You can export to CSV, PDF or HTML too
            );
        }else
            throw new CHttpException(403,"El sistema ha detectado que no posees los permisos necesarios para ingresar a
            esta p&aacute;gina.");
    }

    /**
     * Filtros del controlador. Actualmente solo se utiliza el filtro que restringe el acceso a las
     * acciones del controlador para los roles de usuarios que cumplan con las reglas especificadas
     * en el metodo accessRules().
     *
     * @return  array arreglo con los filtros del controlador
     *
     * @since   05-05-2013
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso para las acciones de este controlador.
     *
     * @return  array arreglo con las reglas de acceso para cada accion y cada rol
     *
     * @since   05-05-2013
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'), //allow authenticated users
            ),
            array('deny',
                'users' => array('?'), //deny anonymous users
            ),
        );
    }

    /**
     * Behaviors del componente controlador.
     *
     * @return  array array de behaviors
     *
     * @since   09-04-2013
     */
    public function behaviors()
    {
        return array(
            'eexcelview'=>array(
                'class'=>'ext.eexcelview.EExcelBehavior',
            ),
        );
    }
}
