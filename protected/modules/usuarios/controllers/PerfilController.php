<?php
Yii::import('application.modules.iniciativas.models.*');
Yii::import('application.modules.proyectos.models.*');

/**
 * Contiene las acciones que manejan el perfil de un usuario dentro del sistema.
 *
 * Permite por ejemplo, ver el perfil de los usuarios del sistema, que estos puedan
 * cambiar sus password, que los usuarios que olvidaron su password tambien la
 * puedan recuperar, etc.
 *
 * @package   modules.usuarios.controllers
 * @version   1.0
 * @since     2012-07-14
 * @author    dacuna <diego.acuna@usm.cl>
 */
class PerfilController extends Controller
{
    /**
     * Layout por defecto del controlador
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Muestra el perfil publico de un usuario cualquiera del sistema.
     *
     * @param int $id ID del usuario.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionIndex($id)
    {
        $usuario = Persona::model()->findByPk($id);
        if ($usuario == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($usuario->id == user()->id)
            $this->redirect(array('miPerfil'));
        $perfil = $usuario->perfil;

        $this->render('index', array(
            'usuario' => $usuario,
            'perfil' => $perfil
        ));
    }

    /**
     * Muestra el perfil del usuario logeado (para el mismo).
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionMiPerfil()
    {
        $usuario_id = Yii::app()->user->id;
        $usuario = Persona::model()->findByPk($usuario_id);
        $perfil_usuario = PerfilUsuario::model()->findByPk($usuario_id);

        $imagen = new CambiarAvatarForm;

        //se cargan los cargos de los usuarios
        $cargos_proyectos=LookupProyecto::getCargosDeUsuario($usuario_id);
        $finales_proyectos=array();
        foreach ($cargos_proyectos as $cargo) {
            $item=am()->getAuthItem($cargo->nombre_item);
            array_push($finales_proyectos,array($item,$cargo));
        }

        $cargos_iniciativas=LookupIniciativa::getCargosDeUsuario($usuario_id);
        $finales_iniciativas=array();
        foreach ($cargos_iniciativas as $cargo) {
            $item=am()->getAuthItem($cargo->nombre_item);
            array_push($finales_iniciativas,array($item,$cargo));
        }

        $this->render('mi_perfil', array(
            'usuario' => $usuario,
            'perfil' => $perfil_usuario,
            'cambiar_imagen' => $imagen,
            'cargos_proyectos'=>$cargos_proyectos,
            'cargos_iniciativas'=>$cargos_iniciativas,
            'finales_proyectos'=>$finales_proyectos,
            'finales_iniciativas'=>$finales_iniciativas
        ));
    }

    /**
     * Permite al usuario logeado modificar los datos de su perfil.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionModificarPerfil()
    {
        $usuario_id = Yii::app()->user->id;
        $usuario = Usuario::model()->findByPk($usuario_id);
        $perfil_usuario = PerfilUsuario::model()->findByPk($usuario_id);
        $imagen = new CambiarAvatarForm;

        if (isset($_POST['Usuario'], $_POST['PerfilUsuario'])) {
            $usuario->attributes = $_POST['Usuario'];
            $usuario->statuses = $usuario->statusesId;
            $perfil_usuario->attributes = $_POST['PerfilUsuario'];
            $perfil_usuario->contacto_emergencia = $usuario->contacto_emergencia;

            $valid = $usuario->validate();
            $valid = $perfil_usuario->validate() && $valid;

            if ($valid) {
                $usuario->statuses = null; //se deshace el uso de many:many
                $usuario->save(false);
                $perfil_usuario->save(false);

                //guardo la informacion de cada status del usuario
                foreach ($usuario->statusesId as $status) {
                    //busco si es que el status ya existe
                    $status_link = UsuarioHasStatus::model()->findByPk(array('user_id' => $usuario->id, 'status_usuario_id' => $status));
                    if ($status_link != null) {
                        $status_link->asignarInfomacionExtra($usuario, $status);
                        $status_link->save(false);
                    } else {
                        $status_assign = new UsuarioHasStatus;
                        $status_assign->user_id = $usuario->id;
                        $status_assign->status_usuario_id = $status;
                        $status_assign->asignarInfomacionExtra($usuario, $status);
                        $status_assign->save(false);
                    }
                }

                Yii::app()->user->setFlash('success', 'Se ha actualizado correctamente tu perfil.');
                $this->redirect(array('/usuarios/perfil/miPerfil'));
            }
        }

        $this->render('modificar_perfil', array(
            'usuario' => $usuario,
            'perfil' => $perfil_usuario,
            'cambiar_imagen' => $imagen
        ));
    }

    /**
     * Permite al usuario logeado cambiar su avatar.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCambiarImagen()
    {
        $usuario_id = Yii::app()->user->id;
        $perfil_usuario = PerfilUsuario::model()->findByPk($usuario_id);

        $imagen = new CambiarAvatarForm;
        $imagen->avatar_antiguo = $perfil_usuario->avatar;
        if (isset($_POST['CambiarAvatarForm'])) {
            $imagen->attributes = $_POST['CambiarAvatarForm'];
            if ($imagen->validate() && $imagen->save())
                Yii::app()->user->setFlash('success', 'Se ha actualizado correctamente tu imagen de perfil.');
            else
                Yii::app()->user->setFlash('error', 'Ha ocurrido un error al actualizar tu imagen de perfil. Asegurate que la imagen tenga un tama&ntilde;o m&iacute;nimo de 200*200px y que sea cuadrada.');
            $this->redirect(array('/usuarios/perfil/miPerfil'));
        }
    }

    /**
     * Despliega al usuario logeado el formulario para poder cambiar su password.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionSeguridad()
    {
        $usuario = Persona::model()->findByPk(Yii::app()->user->id);
        $model = new CambiarPassForm;

        if (isset($_POST['CambiarPassForm'])) {
            $model->attributes = $_POST['CambiarPassForm'];
            if ($model->validate()) {
                //debo cambiar la password del usuario por la nueva
                $usuario->password = sha1($model->password_nueva);
                $usuario->save(false);
                Yii::app()->user->setFlash('success', "Se ha actualizado tu contrase&ntilde;a exit&oacute;samente.");
                $this->redirect(array('miPerfil'));
            }
        }

        $this->render('seguridad', array(
            'model' => $usuario,
            'cambiarPassForm' => $model
        ));
    }

    /**
     * Permite a los usuarios del sistema recuperar su password enviandoles un mail con un enlace
     * para realizar la recuperacion.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRecuperarPassword()
    {
        $model = new RecuperarPasswordForm;
        if (isset($_POST['RecuperarPasswordForm'])) {
            $model->attributes = $_POST['RecuperarPasswordForm'];
            if ($model->validate()) {
                $usuario = Persona::model()->find('email=:email', array('email' => $model->mail));
                $usuario->token = $this->randString(36);
                $usuario->save(false);

                //se envia el email correspondiente
                $html = Yii::app()->controller->renderPartial('/notificaciones/_recuperar_password_email',
                    array('usuario' => $usuario), true);
                $message = new YiiMailMessage;
                $message->setBody($html, 'text/html');
                $message->subject = '[PIE>A] Sistema de gesti&oacute;n I+D+i - Recuperaci&oacute;n de contrase&ntilde;a';
                $message->from = array('piea@usm.cl' => 'PIE>A::Gestion I+D+i');
                $message->addTo($usuario->email);
                Yii::app()->mail->send($message);
                Yii::app()->user->setFlash('success', "Se te ha enviado un email con las instrucciones correspondientes para recuperar tu contrase&ntilde;a.");
                $this->redirect(array('/'));
            }
        }

        $this->render('recuperar_password_inicial', array('model' => $model));
    }

    /**
     * Accion que permite cambiar la password a los usuario que solicitar recuperar su password.
     * Para verificar la autenticidad del usuario se utiliza un token enviado a su mail mediante
     * un mensaje que contiene una url con el token (generado aleatoriamente).
     *
     * @param int $id ID del usuario
     * @param string $token token del usuario
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionNuevaPassword($id, $token)
    {
        $usuario = Persona::model()->findByPk($id);
        if ($usuario == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //se verifica la autenticidad del token
        if ($usuario->token != $token)
            throw new CHttpException(500, 'El c&oacute;digo de verificaci&oacute;n no coincide. Por favor, no intentes ingresar nuevamente a esta direcci&oacute;n.');
        //al parecer todo esta ok, se permite el cambio de contrasena
        $model = new HabilitarPasswordForm;

        if (isset($_POST['HabilitarPasswordForm'])) {
            $model->attributes = $_POST['HabilitarPasswordForm'];
            if ($model->validate()) {
                //se realiza la actualizacion de los datos del usuario
                $usuario->password = sha1($model->password_nueva);
                $usuario->save(false);
                Yii::app()->user->setFlash('success', "Se ha actualizado tu contrase&ntilde;a exit&oacute;samente. Ahora puedes ingresar con tu nueva contrase&ntilde;a al sistema.");
                $this->redirect(array('/'));
            }
        }

        $this->render('recuperar_password_final', array('model' => $model));
    }

    /**
     * Genera un string aleatorio utilizado como token para el nueva password.
     *
     * @param int $length largo del string generado
     * @param string $charset caracteres a considerar
     *
     * @return  string string generado
     */
    private function randString($length, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count - 1)];
        }
        return $str;
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('miPerfil', 'modificarPerfil', 'cambiarImagen', 'seguridad', 'index'),
                'roles' => array('usuario'),
            ),
            array('allow',
                'actions' => array('recuperarPassword', 'nuevaPassword'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

}