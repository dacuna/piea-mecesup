<?php
/**
 * Controlador que permite manejar las acciones relativas a la privacidad y seguridad de
 * la cuenta de los usuarios.
 *
 * @package   modules.usuarios.controllers
 * @version   1.0
 * @since     2012-07-22
 * @author    dacuna <diego.acuna@usm.cl>
 */
class PrivacidadController extends Controller
{
    /**
     * Layout por defecto del controlador
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Permite definir en cuales iniciativas y/o proyectos el usuario quiere aparecer como
     * integrante en los listados publicos de PIEA.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionIndex()
    {
        $usuario = Persona::model()->findByPk(Yii::app()->user->id);
        //se obtienen los proyectos de este usuario (como integrante)
        $proyectos = $usuario->integranteProyectos;
        //y las iniciativas
        $iniciativas = $usuario->integranteIniciativas;

        //proceso los integrantes de iniciativa
        if (isset($_POST['IntegranteIniciativa'])) {
            $valid = true;
            foreach ($iniciativas as $i => $iniciativa) {
                if (isset($_POST['IntegranteIniciativa'][$i]))
                    $iniciativa->attributes = $_POST['IntegranteIniciativa'][$i];
                $valid = $iniciativa->validate() && $valid;
            }
            if ($valid) {
                //se actualiza cada elemento
                foreach ($iniciativas as $i => $iniciativa)
                    $iniciativa->save(false);
                Yii::app()->user->setFlash('success', "Se han actualizado correctamente tus opciones de privacidad.");
                $this->redirect(array('/usuarios/perfil/miPerfil'));
            }
        }

        //proceso los integrantes de los proyectos
        if (isset($_POST['IntegranteProyecto'])) {
            $valid = true;
            foreach ($proyectos as $i => $proyecto) {
                if (isset($_POST['IntegranteProyecto'][$i]))
                    $proyecto->attributes = $_POST['IntegranteProyecto'][$i];
                $valid = $proyecto->validate() && $valid;
            }
            if ($valid) {
                //se actualiza cada elemento
                foreach ($proyectos as $i => $proyecto)
                    $proyecto->save(false);
                Yii::app()->user->setFlash('success', "Se han actualizado correctamente tus opciones de privacidad.");
                $this->redirect(array('/usuarios/perfil/miPerfil'));
            }
        }

        $notificaciones = new NotificacionPorEmailForm;

        if (isset($_POST['NotificacionPorEmailForm'])) {
            $notificaciones->attributes = $_POST['NotificacionPorEmailForm'];
            if ($notificaciones->validate() && $notificaciones->save())
                Yii::app()->user->setFlash('success', "Se han actualizado correctamente tus opciones de privacidad.");
            else
                Yii::app()->user->setFlash('error', "Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.");
            $this->redirect(array('/usuarios/perfil/miPerfil'));
        }

        $this->render('privacidad', array(
            'proyectos' => $proyectos,
            'iniciativas' => $iniciativas,
            'notificaciones' => $notificaciones
        ));
    }
}