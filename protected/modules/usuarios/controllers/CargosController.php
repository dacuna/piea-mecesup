<?php
Yii::import('application.modules.iniciativas.models.*');
Yii::import('application.modules.proyectos.models.*');
Yii::import('application.modules.formularios.models.FormularioPiea');
Yii::import('application.modules.formularios.models.ProcesoFormulario');

/**
 * Controlador que contiene los metodos y acciones que permiten realizar asignaciones de cargos dentro de una
 * iniciativa o proyecto a un integrante por medio de otros integrantes.
 *
 * Se permiten la creacion y gestion de permisos y roles dentro de una entidad  como ademas que el administrador
 * del sistema pueda asignar permisos especiales a los demas usuarios.
 *
 * @version   0.01
 * @since     09/10/2012
 * @author    dacuna <diego.acuna@usm.cl>
 */
class CargosController extends Controller
{

    /**
     * Accion principal, permite a los usuarios de una entidad especifica
     * asignar cargos a otros integrantes de dicha entidad. Los usuarios
     * solo pueden asignar cargos que posean o que deriven de sus propios
     * cargos y solamente los usuarios con cargos asignados pueden asignar
     * otros cargos.
     *
     * @param   int  $id    Id de la entidad a la que pertenece el usuario
     * @param   int  $tipo  Tipo de la entidad, 0=iniciativa, 1=proyecto
     *
     * @throws  \CHttpException
     *
     * @since   09-10-2012
     *
     */
    public function actionCrear($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        //verifico que el usuario que quiere asignar un cargo tiene un cargo asignado
        if(!$entidad->verificarAsignacionDeCargo(user()->id))
            throw new CHttpException(500,'No tienes los permisos suficientes para ingresar a esta p&aacute;gina.');
         //se listan los cargos a los que el usuario logeado tiene acceso
        $items = am()->getAuthItems(0);

        $permisos = array();
        //por cada item se debe verificar si es que el usuario tiene los permisos, si es asi, entonces se agrega
        //a la lista de permisos
        foreach ($items as $item) {
            $check = (strpos($item->getName(), 'iniciativa') === false);
            if ($entidad->getTipoEntidad() == 'Iniciativa') {
                if (!$check && user()->checkAccess($item->getName(), array('iniciativa' => $entidad->id)) && $item->getName()!='permiso_nulo_iniciativa')
                    $permisos[$item->getName()] = $item->getDescription();
            } else if ($check) {
                if (user()->checkAccess($item->getName(), array('iniciativa' => $entidad->entidad->iniciativa->id, 'proyecto' => $entidad->id)) && $item->getName()!='permiso_nulo_proyecto')
                    $permisos[$item->getName()] = $item->getDescription();
            }
        }
        if($entidad->getTipoEntidadNumerico()==0)
            $permisos['permiso_nulo_iniciativa']='Sin permisos especiales';
        else
            $permisos['permiso_nulo_proyecto']='Sin permisos especiales';

        $model = new AsignarCargo;
        $model->setEntidad($entidad);
        $model->setListaPermisos($permisos);
        $model->setEntidadId($entidad->getId());

        if (isset($_POST['AsignarCargo'])) {
            $model->attributes = $_POST['AsignarCargo'];
            if ($model->validate()) {
                //se debe salvar el permiso
                $model->save($entidad->getTipoEntidad());
                //hasta este punto, la operacion ha salido bien, se redirige a la vista inicial de la entidad
                Yii::app()->user->setFlash('success', 'El nuevo cargo ha sido asignado con &eacute;xito.');
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->id));
            }
        }

        $this->render('crear', array(
            'entidad' => $entidad,
            'permisos' => $permisos,
            'model' => $model,
            'integrantes' => $this->integrantesEntidad($entidad)
        ));
    }

    /**
     * eliminarCargo: permite que un usuario con los permisos correspondientes
     * pueda eliminar un cargo dentro de una entidad especifica. El usuario
     * podra eliminar dicho cargo solo si es que lo posee explicitamente o
     * tiene un cargo superior al que se desea eliminar.<br><br> Si el cargo
     * tiene cargos que descienden de el, entonces no se debe poder
     * eliminar. Primero se deben eliminar todas las dependencias directas del
     * cargo.
     *
     * @param int    $id    id de la entidad a la que pertenece el cargo
     * @param int    $tipo  tipo de la entidad, 0=iniciativa, 1=proyecto
     * @param String $cargo nombre del cargo a eliminar
     * @param bool   $from  si se desea volver a la pagina del perfil del usuario o al
     *                      perfil de la entidad.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   17-10-2012
     *
     */
    public function actionEliminarCargo($id, $tipo, $cargo, $from = null)
    {
        $entidad = new Entidad($id, $tipo);
        if ($from != null)
            $ureturn = array('/usuarios/perfil/miPerfil');
        else
            $ureturn = array($entidad->urlEntidad(), 'id' => $entidad->getId());
        $cargo = $entidad->obtenerCargoActivo($cargo);
        //verifico que el usuario logeado tenga permisos de eliminacion
        if (!$entidad->verificarJerarquiaCargo($cargo))
            throw new CHttpException(500, 'No tienes los permisos suficientes para ejecutar esta acci&oacute;n.');
        //se verifica que el cargo actual no tenga cargos que dependan de el
        if ($entidad->existeDependenciaCargo($cargo)) {
            user()->setFlash('error', 'El cargo a eliminar posee otros cargos que descienden de el. Primero aseg&uacute;rese de eliminar las dependencias.');
            $this->redirect($ureturn);
        }
        //en este punto todo_ esta ok. el cargo se puede eliminar
        $cargo->activo = 0;
        if ($cargo->save()) {
            //si es que el usuario que elimino el cargo no era el dueno del cargo y el cargo no esta asignado a una persona nula
            //entonces se lanza el evento de eliminacion del cargo para notificarlo
            $this->onEliminarCargo = array(app()->mailman, 'notificacionHandler');
            if ($cargo->user_id != user()->id && $cargo->asignacion_nula==0)
                $this->onEliminarCargo(new CEvent($this, array('tipo' => 'cargo_eliminado', 'entidad' => $entidad, 'cargo' => $cargo, 'usuario' => user()->id, 'destinatario' => $cargo->user_id)));
            //notifico tambien a la persona que asigno el cargo
            $superior=$entidad->getEncargardoSuperiorCargo($cargo);
            $this->onEliminarCargo(new CEvent($this, array('tipo'=>'notificar_encargado_cargo_eliminado','entidad'=>$entidad,'cargo'=>$cargo,'superior'=>$superior)));
            user()->setFlash('success', 'Se ha eliminado el cargo exit&oacute;samente.');
        } else
            user()->setFlash('error', 'Ha ocurrido un error, por favor int&eacute;ntelo nuevamente.');
        $this->redirect($ureturn);
    }

    /**
     * transferirCargo: renderiza la vista que permite solicitar una transferencia de un cargo. Al
     * solicitar la transferencia, se le notifica la usuario al que se le quiere transferir el cargo
     * para que acepte la transferencia. Solo se pueden seleccionar usuarios que no tengan cargos
     * asignados (un usuario solo puede tener un cargo por entidad para saber atributo cargo_que_asigno).
     *
     * @param int    $id    id de la entidad a la que pertenece el cargo
     * @param int    $tipo  tipo de la entidad, 0=iniciativa, 1=proyecto
     * @param String $cargo nombre del cargo a eliminar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   18-10-2012
     *
     */
    public function actionTransferirCargo($id, $tipo, $cargo)
    {
        $entidad = new Entidad($id, $tipo);
        $cargo = $entidad->obtenerCargoActivo($cargo);
        //verifico que el usuario logeado tenga permisos de jerarquia
        if (!$entidad->verificarJerarquiaCargo($cargo))
            throw new CHttpException(500, 'No tienes los permisos suficientes para ejecutar esta acci&oacute;n.');

        $model = new TransferirCargoForm;
        $model->setCargo($cargo);
        $model->setUsuarioQueTransfiere(user()->id);
        $model->setEntidad($entidad);
        if (isset($_POST['TransferirCargoForm'])) {
            $model->attributes = $_POST['TransferirCargoForm'];
            //veo si es una transferencia a persona nula
            if($model->id_usuario==-1){
                if($model->transferirAPersonaNula())
                    user()->setFlash('success', 'Se ha dejado el cargo sin responsable de manera correcta.');
                else
                    user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntelo nuevamente.');
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }else if($model->notificar_transferencia() && $model->validate()) {
                user()->setFlash('success', 'Se ha enviado la notificaci&oacute;n de transferencia de cargo al usuario ' . $model->usuario->nombrePresentacion);
                $this->redirect(array($entidad->urlEntidad(), 'id' => $entidad->getId()));
            }
        }
        $integrantes=$this->integrantesEntidad($entidad,$cargo->user_id);
        $integrantes[-1]='Dejar cargo sin responsable temporalmente';

        $this->render('transferir_cargo', array(
            'model' => $model,
            'entidad' => $entidad,
            'cargo' => $cargo,
            'integrantes'=>$integrantes
        ));
    }

    /**
     * Accion que permite a un usuario el aceptar el ofrecimiento de transferencia de cargo en una entidad
     * que se le haya sido asignado por otro usuario de dicha entidad. El cargo solo puede ser aceptado en
     * el caso de que el usuario en cuestion no posea otro cargo en la entidad donde se le desea transferir
     * el cargo.
     *
     * @param int $e ID del emisor del mensaje
     * @param int $d ID del destinatario del mensaje
     * @param int $uid UID del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   19-10-2012
     */
    public function actionAceptarTransferenciaCargo($e,$d,$uid){
        $mensaje=Mensaje::obtenerMensaje($e,$d,$uid);
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        $tipo=($mensaje->tipo_relacion=='iniciativa') ? 0 : 1;
        $entidad=new Entidad($mensaje->id_relacion,$tipo);
        $cargo=$entidad->obtenerCargoActivo($mensaje->accion);
        //se debe realizar la transferencia del cargo, verifico primero que el usuario al que se transfiere no tenga un cargo asignado
        if($entidad->getCargoDeUsuario($mensaje->destinatario)!=null){
            user()->setFlash('error','Ya posees un cargo asignado en '.$entidad->articuloEntidad().' '.$entidad->getTipoEntidad().' '.$entidad->getNombre().' No puedes aceptar el cargo ofrecido.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        $old_user=$cargo->user_id;
        $notify=1;
        if($cargo->asignacion_nula==1){
            $cargo->asignacion_nula=0;
            $cargo->activo=1;
            $notify=0;
        }
        $cargo->user_id=$mensaje->destinatario;
        //si se cambia el cargo efectivamente, se notifica al usuario que perdio el cargo y al usuario que lo ofrecio
        if($cargo->save()){
            //debo asignarle el authitem correspondiente al nuevo usuario
            if(am()->getAuthAssignment($cargo->nombre_item,$cargo->user_id)==null)
                am()->assign($cargo->nombre_item,$cargo->user_id);
            $this->onAceptarTransferenciaCargo=array(app()->mailman, 'notificacionHandler');
            $usuario=Persona::model()->findByPk($mensaje->destinatario);
            $this->onAceptarTransferenciaCargo(new CEvent($this, array('tipo'=>'transferencia_cargo_aceptada','entidad'=>$entidad,'cargo'=>$cargo,'usuario'=>$usuario,'destinatario'=>$mensaje->emisor)));
            if($notify)
                $this->onAceptarTransferenciaCargo(new CEvent($this, array('tipo'=>'cargo_revocado','entidad'=>$entidad,'cargo'=>$cargo,'emisor'=>$mensaje->emisor,'destinatario'=>$old_user)));
            user()->setFlash('success','El cargo ha sido aceptado exit&oacute;samente.');
            $mensaje->contestado=1;
            $mensaje->accion_realizada='Aceptar solicitud';
            $mensaje->save(false);
        }else
            user()->setFlash('error','Ha ocurrido un error. Por favor, intentalo nuevamente.');
        $this->redirect(array('/buzon/inbox'));
    }

    /**
     * Accion que permite a un usuario rechazar un cargo que se le haya ofrecido en una entidad en particular.
     * La accion genera los eventos correspondientes para luego notificar al usuario que ofrecio el cargo de
     * que este ha sido rechazado.
     *
     * @param int $e ID del emisor del mensaje
     * @param int $d ID del destinatario del mensaje
     * @param int $uid UID del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   19-10-2012
     */
    public function actionRechazarTransferenciaCargo($e,$d,$uid){
        $mensaje=Mensaje::obtenerMensaje($e,$d,$uid);
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        $tipo=($mensaje->tipo_relacion=='iniciativa') ? 0 : 1;
        $entidad=new Entidad($mensaje->id_relacion,$tipo);
        $cargo=$entidad->obtenerCargoActivo($mensaje->accion);
        $mensaje->contestado=1;
        $mensaje->accion_realizada='Rechazar solicitud';

        if($mensaje->save(false)){
            $this->onRechazarTransferenciaCargo=array(app()->mailman, 'notificacionHandler');
            $usuario=Persona::model()->findByPk($mensaje->destinatario);
            $this->onRechazarTransferenciaCargo(new CEvent($this, array('tipo'=>'transferencia_cargo_rechazada','entidad'=>$entidad,'cargo'=>$cargo,'usuario'=>$usuario,'destinatario'=>$mensaje->emisor)));
            user()->setFlash('success','El cargo ha sido rechazado exit&oacute;samente.');
        }else
            user()->setFlash('error','Ha ocurrido un error. Por favor, intentalo nuevamente.');

        $this->redirect(array('/buzon/inbox'));
    }

    /**
     * integrantesEntidad: retorna una lista de usuarios con los integrantes a los que
     * se les puede asignar un cargo dentro de la entidad especificada por $entidad. Cuando
     * se especifica el parametro $extra se quita de la lista el usuario especificado por
     * $extra.
     *
     * @param Entidad  $entidad   entidad onde se buscan los integrantes
     * @param int      $extra     id de usuario a filtrarr
     *
     * @return  array
     *
     * @since   18-10-2012
     * @todo    cambiar parametro $extra por un array para filtrar mas de un usuario.
     *
     */
    private function integrantesEntidad($entidad, $extra = null)
    {
        $usuarios = array();
        //se deben quitar los integrantes que tienen un cargo
        $size=count($entidad->integrantes);
        foreach ($entidad->integrantes as $integrante) {
            if ($integrante->usuario->id != $extra && $entidad->getCargoDeUsuario($integrante->user_id)==null)
                $usuarios[$integrante->usuario->id]=$integrante->usuario->nombreCompleto;
        }
        return $usuarios;
    }

    /**
     * Accion que permite al administrador del sistema asignar permisos especiales tales como
     * el revisar proyectos del formulario piea entre otros. Se utiliza tambien el proceso de
     * un formulario piea para mostrar el permiso especial 'observador_proceso' que permite a
     * un usuario observar todos los proyectos postulantes a un proceso de fondos I+D+i y dar
     * comentarios a estos.
     *
     * @param int $user_id Id del usuario al que se le desean asignar los permisos
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   15-04-2013
     */
    public function actionAsignarPermisosEspeciales($user_id)
    {
        //regla de negocio: los ayudantes administradores no puede asignar permisos, el usuario 1 siempre sera olga.godoy
        //ella si puede asignar los permisos que quiera
        if(user()->checkAccess('administrador') && user()->id!=1)
            throw new CHttpException(403,"El sistema ha detectado que tu posees el cargo de ayudante administrador PIEA.
             Los ayudantes administrativos no pueden asignar permisos a otros usuarios.");
        $usuario=Persona::load($user_id);
        //para el permiso de observador de un proceso de formulario piea
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        //se crea el permiso actual si es que no existe
        $item=am()->getAuthItem("observador_proceso_{$proceso->id}");
        if($item==null)
            am()->createOperation("observador_proceso_{$proceso->id}", 'Observador de proyectos postulantes
                    a fondos I+D+i');
        $permisos=array('idi_ver_proyectos_postulantes','idi_ver_proyectos_enviados','idi_cerrar_postulacion',
            'idi_abrir_revision','dar_permiso_de_revisor_fondos_idi','observador_proceso_'.$proceso->id,'administrador',
            'idi_estado_proyectos','ver_usuarios_del_sistema');

        $has=array();
        foreach ($permisos as $permiso) {
            if($item!=null){
                if(am()->checkAccess($permiso,$usuario->id)){
                    $has[]=$permiso;
                }else
                    $has[]=false;
            }
        }

        if(isset($_POST['permiso'])){
            $asignados="";
            $ya_posee="";
            for($i=0;$i<count($permisos);$i++){
                if(isset($_POST['permiso'][$i])){
                    $permiso=$_POST['permiso'][$i];
                    if(isset($permiso) && is_numeric($permiso) && $permiso>=0 && $permiso <= count($permisos)){
                        //se procesa el permiso
                        $asignar=$permisos[$permiso];
                        $item=am()->getAuthItem($asignar);
                        if($item!=null){
                            if(!am()->checkAccess($item->name,$usuario->id)){
                                am()->assign($item->name,$usuario->id);
                                $asignados.=$asignar.", ";
                                $has[$permiso]=$item->name;
                            }else{
                                $ya_posee.=$asignar;
                            }
                        }
                    }
                }else{
                    //entonces si es que el usuario tiene este permiso, se le revoca
                    $permiso=$permisos[$i];
                    if(am()->checkAccess($permiso,$usuario->id)){
                        am()->revoke($permiso,$usuario->id);
                    }
                    $has[$i]=false;
                }
            }

            if($asignados!="")
                $mensaje="El usuario seleccionado se le han agregado los siguientes permisos: ".$asignados;
            else
                $mensaje="Se han actualizado correctamente los permisos del usuario.";
            user()->setFlash("success",$mensaje);
        }

        $this->render('asignar_permisos_especiales',array(
            'usuario'=>$usuario,
            'proceso'=>$proceso,
            'has'=>$has
        ));
    }

    /**
     * Filtros del controlador. Actualmente solo se utiliza el filtro que restringe el acceso a las
     * acciones del controlador para los roles de usuarios que cumplan con las reglas especificadas
     * en el metodo accessRules().
     *
     * @return  array arreglo con los filtros del controlador
     *
     * @since   09-10-2012
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso para las acciones de este controlador.
     *
     * @return  array arreglo con las reglas de acceso para cada accion y cada rol
     *
     * @since   09-10-2012
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('crear', 'eliminarCargo', 'transferirCargo','aceptarTransferenciaCargo','rechazarTransferenciaCargo'),
                'roles' => array('usuario'),
            ),
            array('allow',
                'actions' => array('asignarPermisosEspeciales'),
                'roles' => array('administrador'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Evento emitido cuando se ejecuta la accion de eliminar un cargo.
     *
     * @param CEvent $event Evento generado
     *
     * @return  void
     *
     * @since   19-10-2012
     */
    public function onEliminarCargo($event)
    {
        $this->raiseEvent('onEliminarCargo', $event);
    }

    /**
     * Evento emitido cuando se ejecuta la accion de aceptar la transferencia de un cargo.
     *
     * @param CEvent $event Evento generado
     *
     * @return  void
     *
     * @since   19-10-2012
     */
    public function onAceptarTransferenciaCargo($event)
    {
        $this->raiseEvent('onAceptarTransferenciaCargo', $event);
    }

    /**
     * Evento emitido cuando se ejecuta la accion de rechazar la transferencia de un cargo.
     *
     * @param CEvent $event Evento generado
     *
     * @return  void
     *
     * @since   19-10-2012
     */
    public function onRechazarTransferenciaCargo($event)
    {
        $this->raiseEvent('onRechazarTransferenciaCargo', $event);
    }

}
