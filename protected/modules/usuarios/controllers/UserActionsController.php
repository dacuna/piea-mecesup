<?php
/**
 * Contiene acciones utiles para el manejo de usuarios del sistema.
 *
 * Permite por ejemplo enviar invitaciones de participacion a usuarios. Administrar ciertas
 * funcionalidades del sistema, manejo de permisos globales del sistema etc.
 *
 * @package   modules.usuarios.controllers
 * @version   0.01
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo este es practicamente el primer codigo que se escribio del sistema, por lo que actualmente con todas las mejoras
 * @todo que se han realizado es un gran candidato a refactorizacion (29 julio 2013), principalmente se pueden refactorizar
 * @todo el tema de las invitaciones y el envio de notificaciones.
 */
class UserActionsController extends Controller
{
    /**
     * Layout por defecto del controlador
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Accion que permite al administrador del sistema listar y buscar entre todos los usuarios
     * registrados en este.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   28-03-2013
     */
    public function actionVerUsuarios(){
        if(user()->checkAccess('ver_usuarios_del_sistema')){
            $model=new Persona('search');
            $model->unsetAttributes();
            if(isset($_GET['Persona']))
                $model->attributes=$_GET['Persona'];
            $this->render('ver_usuarios',array(
                'model'=>$model
            ));
        }else
            throw new CHttpException(500,"No estas autorizado a ejecutar esta acci&oacute;n");
    }

    /**
     * Accion que permite enviar invitaciones a usuarios para que participen de proyectos o
     * iniciativas dentro de PIEA. Se pueden invitar tanto usuarios que ya tienen una cuenta
     * en el sistema o usuarios externos.
     *
     * @param string $tipo iniciativa o proyecto
     * @param int $id ID de la entidad
     *
     * @return  void
     * @throws  \CHttpException
     * @todo se puede refactorizar el tema de las entidades y como se envian las notificaciones, usar MailMan con eventos!
     */
    public function actionInvitar($tipo, $id)
    {
        if ($tipo != 'iniciativa' && $tipo != 'proyecto')
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($tipo == 'iniciativa') {
            $entidad = Iniciativa::model()->findByPk($id);
            $url = array('/iniciativas/crudIniciativas/ver', 'id' => $entidad->id);
        } else {
            $entidad = Proyecto::model()->findByPk($id);
            $url = array('/proyectos/crud/ver', 'id' => $entidad->id);
        }
        if ($entidad == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        $model = new InvitacionExterna;

        if (isset($_POST['InvitacionExterna'])) {
            $model->attributes = $_POST['InvitacionExterna'];
            $model->usuario_invita_id = Yii::app()->user->id;
            $model->tipo_entidad = $tipo;
            $model->entidad = $entidad;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Se ha invitado correctamente a ' . $model->email_invitado);
                $this->redirect($url);
            }
        }

        $interna = new InvitacionInterna;
        if (isset($_POST['invitar-interno'])) {
            $interna->attributes = $_POST['invitar-interno'];

            if ($interna->validate()) {
                if ($interna->tipo_entidad == 'iniciativa') {
                    $entidad = Iniciativa::model()->findByPk($interna->id_entidad);
                    $nombre = $entidad->nombre_abreviado;
                    $url = array('/iniciativas/crudIniciativas/ver', 'id' => $entidad->id);
                } else if ($interna->tipo_entidad == 'proyecto') {
                    $entidad = Proyecto::model()->findByPk($interna->id_entidad);
                    $nombre = $entidad->nombre;
                    $url = array('/proyectos/crud/ver', 'id' => $entidad->id);
                }

                //TODO: se debe verificar que el usuario puede realizar estas invitaciones
                $invitacion = new InvitacionMembresia;
                $invitacion->usuario_id = $interna->id_usuario;
                $invitacion->tipo_entidad = $interna->tipo_entidad;
                $invitacion->entidad_id = $interna->id_entidad;
                $invitacion->entidad_nombre = $nombre;
                $invitacion->enviarSolicitud();
                Yii::app()->user->setFlash('success', 'La invitaci&oacute;n ha sido enviada con &eacute;xito.');
                $this->redirect($url);
            }
        }

        $this->render('invitar', array(
            'model' => $model,
            'entidad' => $entidad,
            'tipo_entidad' => $tipo,
            'interna' => $interna
        ));
    }

    public function actionAceptarInvitacion($id)
    {
        //se carga la invitacion correspondiente
        $invitacion = InvitacionExterna::model()->findByPk($id);
        if ($invitacion == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        $model = new Usuario;
        $perfil = new PerfilUsuario;
        $model->email = $invitacion->email_invitado;

        $this->performAjaxValidation($model);
        if (isset($_POST['Usuario'], $_POST['PerfilUsuario'])) {
            $model->attributes = $_POST['Usuario'];
            $model->statuses = $model->statusesId;
            $perfil->attributes = $_POST['PerfilUsuario'];
            $perfil->contacto_emergencia = $model->contacto_emergencia;

            $valid = $model->validate();
            $valid = $perfil->validate() && $valid;

            $pass_original = $model->password;
            if ($valid) {
                $model->statuses = null; //se deshace el uso de many:many
                $model->save(false);
                $perfil->user_id = $model->primaryKey;
                $perfil->save(false);

                //guardo la informacion de cada status del usuario
                foreach ($model->statusesId as $status) {
                    $status_assign = new UsuarioHasStatus;
                    $status_assign->user_id = $model->primaryKey;
                    $status_assign->status_usuario_id = $status;
                    $status_assign->asignarInfomacionExtra($model, $status);
                    $status_assign->save(false);
                }

                //se le debe asignar un rol de usuario
                $auth = Yii::app()->authManager;
                $auth->assign('usuario', $model->primaryKey);

                //se agrega a la correspondiente iniciativa o proyecto
                if ($invitacion->tipo_entidad == 'iniciativa') {
                    //verifico que no sea integrante de otra iniciativa, si lo es entonces ya tiene el permiso asignado
                    if ($auth->getAuthAssignment("integrante_iniciativa", $model->primaryKey) == null) {
                        $admin = $auth->assign('integrante_iniciativa', $model->primaryKey);
                    }
                    //seteo el lookup de la iniciativa correspondiente
                    $lookup = new LookupIniciativa;
                    $lookup->nombre_item = 'integrante_iniciativa';
                    $lookup->iniciativa_id = $invitacion->entidad_id;
                    $lookup->user_id = $model->primaryKey;
                    $lookup->save(false);

                    //se debe tambien ingresar el integrante nuevo
                    $integrante = new IntegranteIniciativa;
                    $integrante->iniciativa_id = $invitacion->entidad_id;
                    $integrante->user_id = $model->primaryKey;
                    $integrante->save(false);

                    $url = array('/iniciativas/crudIniciativas/ver', 'id' => $invitacion->entidad_id);
                }

                if ($invitacion->tipo_entidad == 'proyecto') {
                    //verifico que no sea integrante de otra iniciativa, si lo es entonces ya tiene el permiso asignado
                    if ($auth->getAuthAssignment("integrante_proyecto", $model->primaryKey) == null) {
                        $admin = $auth->assign('integrante_proyecto', $model->primaryKey);
                    }
                    //seteo el lookup del proyecto correspondiente
                    $lookup = new LookupProyecto;
                    $lookup->nombre_item = 'integrante_proyecto';
                    $lookup->proyecto_id = $invitacion->entidad_id;
                    $lookup->user_id = $model->primaryKey;
                    $lookup->save(false);

                    //se debe tambien ingresar el integrante nuevo
                    $integrante = new IntegranteProyecto;
                    $integrante->proyecto_id = $invitacion->entidad_id;
                    $integrante->user_id = $model->primaryKey;
                    $integrante->save(false);

                    $url = array('/proyectos/crud/ver', 'id' => $invitacion->entidad_id);
                }

                //es un invitado, se debe autentificar el usuario recien creado
                $auth = new LoginForm;
                $auth->username = $model->email;
                $auth->password = $pass_original;
                if ($auth->login())
                    $this->redirect($url);
            }
        }

        $this->render('create', array(
            'model' => $model,
            'perfil' => $perfil,
            'invitacion' => $invitacion
        ));

    }

    /**
     * Crea/registra un nuevo usuario.
     * Si la creacion/registro es realizado por un administrador entonces
     * se redirige a la vista de perfil del usuario creado. Si es un invitado
     * entonces se logea y se redirige al inicio de la plataforma.
     */
    public function actionCreate()
    {
        if (!Yii::app()->user->isGuest && !Yii::app()->user->checkAccess('administrador'))
            $this->redirect(array('/'));
        $model = new Usuario;
        $perfil = new PerfilUsuario;

        $this->performAjaxValidation($model);
        if (isset($_POST['Usuario'], $_POST['PerfilUsuario'])) {
            $model->attributes = $_POST['Usuario'];
            $model->statuses = $model->statusesId;
            $perfil->attributes = $_POST['PerfilUsuario'];
            //Lo siguiente es un workaround para validacion, puesto que la
            //presencia del contacto de emergencia depende del status que
            //haya seleccionado un usuario, y los status son parte del modelo
            //de usuario no del perfil, por otro lado el contacto de emergencia
            //es parte del perfil de usuario por lo que se necesita un atributo
            //dummy en el modelo Usuario para llevar a cabo la validacion.
            $perfil->contacto_emergencia = $model->contacto_emergencia;

            $valid = $model->validate();
            $valid = $perfil->validate() && $valid;

            $pass_original = $model->password;
            if ($valid) {
                $model->statuses = null; //se deshace el uso de many:many
                $model->save(false);
                $perfil->user_id = $model->primaryKey;
                $perfil->save(false);

                //guardo la informacion de cada status del usuario
                foreach ($model->statusesId as $status) {
                    $status_assign = new UsuarioHasStatus;
                    $status_assign->user_id = $model->primaryKey;
                    $status_assign->status_usuario_id = $status;
                    $status_assign->asignarInfomacionExtra($model, $status);
                    $status_assign->save(false);
                }

                //se le debe asignar un rol de usuario
                $auth = Yii::app()->authManager;
                $auth->assign('usuario', $model->primaryKey);

                if (!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')) {
                    $this->redirect(array('view', array('id' => $model->primaryKey)));
                } else {
                    //es un invitado, se debe autentificar el usuario recien creado
                    $auth = new LoginForm;
                    $auth->username = $model->email;
                    $auth->password = $pass_original;
                    if ($auth->login())
                        $this->redirect(array('/'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'perfil' => $perfil
        ));
    }

    /**
     * Inicializa el sistema. Esta es la primera funcion que se debe llamar al iniciar el sistema. Crea
     * el usuario administrador y todos los permisos del sistema. Solo puede ser llamada 1 vez.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCreateAdminUser()
    {
        $auth = Yii::app()->authManager;
        //verifico que no exista un administrador, si existe entonces esta accion no se puede ejecutar
        if ($auth->getAuthItem('administrador') != null)
            throw new CHttpException(403, Yii::t('usuariosModule.exception', 'created_admin'));

        $model = new Usuario;
        $perfil = new PerfilUsuario;

        if (isset($_POST['Usuario'], $_POST['PerfilUsuario'])) {
            $model->attributes = $_POST['Usuario'];
            $model->statuses = $model->statusesId;
            $perfil->attributes = $_POST['PerfilUsuario'];

            $valid = $model->validate();
            $valid = $perfil->validate() && $valid;

            $pass_original = $model->password;
            if ($valid) {
                $model->save(false);
                $perfil->user_id = $model->primaryKey;
                $perfil->save(false);

                //creo el rol de administrador
                $administrador = $auth->createRole('administrador', 'Administrador del sistema');
                $admin = $auth->assign('administrador', $model->primaryKey);

                //se crean los demas roles
                $bizRule = 'return !Yii::app()->user->isGuest;';
                $usuario = $auth->createRole('usuario', 'Usuario registrado en la plataforma', $bizRule);
                $bizRule = 'return Yii::app()->user->isGuest;';
                $invitado = $auth->createRole('invitado', 'Invitado', $bizRule);

                $bizRule = 'return Yii::app()->user->verificar_rol("coordinador_iniciativa",$params["iniciativa"]);';
                $coordinador_iniciativa = $auth->createRole('coordinador_iniciativa', 'Coordinador de una iniciativa', $bizRule);

                $bizRule = 'return Yii::app()->user->verificar_rol("integrante_iniciativa",$params["iniciativa"]);';
                $integrante_iniciativa = $auth->createRole('integrante_iniciativa', 'Integrante de una iniciativa', $bizRule);

                $bizRule = 'return Yii::app()->user->verificar_rol("jefe_proyecto",$params["iniciativa"],$params["proyecto"]);';
                $jefe_proyecto = $auth->createRole('jefe_proyecto', 'Jefe de proyecto', $bizRule);

                $bizRule = 'return Yii::app()->user->verificar_rol("integrante_proyecto",$params["iniciativa"],$params["proyecto"]);';
                $integrante_proyecto = $auth->createRole('integrante_proyecto', 'Integrante de proyecto', $bizRule);

                //se crean algunas acciones
                $crear_iniciativa = $auth->createOperation('crear_iniciativa', 'Crear iniciativas');
                $crear_proyecto = $auth->createOperation('crear_proyecto', 'Crear proyectos');

                //asignacion de permisos a roles
                //al rol administrador se le asignan las tareas de crear_iniciativa y crear_proyecto
                $administrador->addChild('crear_iniciativa');
                $administrador->addChild('crear_proyecto');

                //debo asignar la jerarquia de roles
                $integrante_proyecto->addChild('usuario');
                $jefe_proyecto->addChild('integrante_proyecto');
                $integrante_iniciativa->addChild('usuario'); //VERIFICAR!, quizas deberia tener los permisos de los proyectos
                $coordinador_iniciativa->addChild('integrante_iniciativa');
                $coordinador_iniciativa->addChild('jefe_proyecto');
                $administrador->addChild('coordinador_iniciativa');

                //operaciones
                $bizRule = 'return Yii::app()->user->verificar_rol("ver_tarea_iniciativa",$params["iniciativa"]);';
                $ver_tarea_iniciativa = $auth->createOperation('ver_tarea_iniciativa', 'Ver tareas de una iniciativa');

                $crear_proyecto_iniciativa = $auth->createOperation('crear_proyecto_iniciativa', 'Crear un proyecto en la iniciativa');
                $editar_iniciativa = $auth->createOperation('editar_iniciativa', 'Editar perfil de la iniciativa');
                $ver_tarea_proyecto = $auth->createOperation('ver_tarea_proyecto', 'Ver tareas de un proyecto');
                $editar_proyecto = $auth->createOperation('editar_proyecto', 'Editar perfil de un proyecto');

                //PERMISOS PARA PRESUPUESTOS
                $auth->createOperation('ingresar_presupuesto_iniciativa','Ingresar presupuesto en una iniciativa');
                $auth->createOperation('ingresar_presupuesto_proyecto','Ingresar presupuesto en un proyecto');
                $integrante_iniciativa->addChild('ingresar_presupuesto_iniciativa');
                $integrante_proyecto->addChild('ingresar_presupuesto_proyecto');
                $auth->createOperation('aprobar_presupuesto_iniciativa','Aprobar presupuesto en una iniciativa');
                $coordinador_iniciativa->addChild('aprobar_presupuesto_iniciativa');
                $auth->createOperation('aprobar_presupuesto_proyecto','Aprobar presupuesto en un proyecto');
                $jefe_proyecto->addChild('aprobar_presupuesto_proyecto');
                $auth->createOperation('aprobar_presupuesto_cuenta','Aprobar presupuesto cuenta');


                //PERMISOS PARA RECIBOS => RECUPERACION DE GASTOS
                $auth->createOperation('ingresar_recuperacion_iniciativa','Ingresar recuperaci&oacute;n de gastos en una iniciativa');
                $auth->createOperation('ingresar_recuperacion_proyecto','Ingresar recuperaci&oacute;n de gastos en un proyecto');
                $integrante_iniciativa->addChild('ingresar_recuperacion_iniciativa');
                $integrante_proyecto->addChild('ingresar_recuperacion_proyecto');
                $auth->createOperation('aprobar_recuperacion_iniciativa','Aprobar recuperaci&oacute;n de gastos en una iniciativa');
                $coordinador_iniciativa->addChild('aprobar_recuperacion_iniciativa');
                $auth->createOperation('aprobar_recuperacion_proyecto','Aprobar recuperaci&oacute;n de gastos en un proyecto');
                $jefe_proyecto->addChild('aprobar_recuperacion_proyecto');
                //PERMISOS PARA RECIBOS => VALE A RENDIR
                $auth->createOperation('ingresar_vale_iniciativa','Ingresar vale a rendir en una iniciativa');
                $auth->createOperation('ingresar_vale_proyecto','Ingresar vale a rendir en un proyecto');
                $integrante_iniciativa->addChild('ingresar_vale_iniciativa');
                $integrante_proyecto->addChild('ingresar_vale_proyecto');
                $auth->createOperation('aprobar_vale_iniciativa','Aprobar vale a rendir en una iniciativa');
                $coordinador_iniciativa->addChild('aprobar_vale_iniciativa');
                $auth->createOperation('aprobar_vale_proyecto','Aprobar vale a rendir en un proyecto');
                $jefe_proyecto->addChild('aprobar_vale_proyecto');
                //VER RECIBOS => RECUPERACION DE GASTOS
                $auth->createOperation('ver_recuperacion_iniciativa','Ver recuperaci&oacute;n de gastos en una iniciativa');
                $auth->createOperation('ver_recuperacion_proyecto','Ver recuperaci&oacute;n de gastos en un proyecto');
                $coordinador_iniciativa->addChild('ver_recuperacion_iniciativa');
                $jefe_proyecto->addChild('ver_recuperacion_proyecto');
                //VER RECIBOS => VALE A RENDIR
                $auth->createOperation('ver_vale_iniciativa','Ver vale a rendir en una iniciativa');
                $auth->createOperation('ver_vale_proyecto','Ver vale a rendir en un proyecto');
                $coordinador_iniciativa->addChild('ver_vale_iniciativa');
                $jefe_proyecto->addChild('ver_vale_proyecto');
                //GESTIONAR VALES SOLICITADOS
                $auth->createOperation('gestionar_vales_iniciativa','Gestionar vales solicitados en una iniciativa');
                $auth->createOperation('gestionar_vales_proyecto','Gestionar vales solicitados en un proyecto');
                $coordinador_iniciativa->addChild('gestionar_vales_iniciativa');
                $jefe_proyecto->addChild('gestionar_vales_proyecto');
                //VER VALES A RENDIR HISTORICOS
                $auth->createOperation('ver_vales_historicos_iniciativa','Ver vales historicos en una iniciativa');
                $auth->createOperation('ver_vales_historicos_proyecto','Ver vales historicos en un proyecto');
                $coordinador_iniciativa->addChild('ver_vales_historicos_iniciativa');
                $jefe_proyecto->addChild('ver_vales_historicos_proyecto');

                //PERMISOS PARA PAGOS A PROVEEDORES
                $auth->createOperation('ingresar_pago_proveedor_iniciativa','Ingresar pago proveedores en una iniciativa');
                $auth->createOperation('ingresar_pago_proveedor_proyecto','Ingresar pago proveedores en un proyecto');
                $administrador->addChild('ingresar_pago_proveedor_iniciativa');
                $administrador->addChild('ingresar_pago_proveedor_proyecto');
                //PERMISOS PARA UNIDADES DE ABASTECIMIENTO
                $auth->createOperation('ingresar_unidad_abastecimiento_iniciativa','Ingresar unidad de abastecimiento en una iniciativa');
                $auth->createOperation('ingresar_unidad_abastecimiento_proyecto','Ingresar unidad de abastecimiento en un proyecto');
                $integrante_iniciativa->addChild('ingresar_unidad_abastecimiento_iniciativa');
                $integrante_proyecto->addChild('ingresar_unidad_abastecimiento_proyecto');

                $auth->createOperation('ver_pago_proveedor_iniciativa','Ver pagos a proveedor en una iniciativa');
                $auth->createOperation('ver_pago_proveedor_proyecto','Ver pagos a proveedor en un proyecto');
                $coordinador_iniciativa->addChild('ver_pago_proveedor_iniciativa');
                $jefe_proyecto->addChild('ver_pago_proveedor_proyecto');

                $auth->createOperation('ver_unidad_abastecimiento_iniciativa','Ver unidades de abastecimiento en una iniciativa');
                $auth->createOperation('ver_unidad_abastecimiento_proyecto','Ver unidades de abastecimiento en un proyecto');
                $coordinador_iniciativa->addChild('ver_unidad_abastecimiento_iniciativa');
                $jefe_proyecto->addChild('ver_unidad_abastecimiento_proyecto');

                $permiso_nulo_iniciativa = $auth->createOperation('permiso_nulo_iniciativa', 'Sin permisos especiales');
                $permiso_nulo_proyecto = $auth->createOperation('permiso_nulo_proyecto', 'Sin permisos especiales');

                $administrador->addChild('ver_tarea_proyecto');
                $administrador->addChild('ver_tarea_iniciativa');
                $coordinador_iniciativa->addChild('crear_proyecto_iniciativa');
                $coordinador_iniciativa->addChild('ver_tarea_iniciativa');
                $coordinador_iniciativa->addChild('editar_iniciativa');
                $coordinador_iniciativa->addChild('ver_tarea_proyecto');
                $integrante_iniciativa->addChild('permiso_nulo_iniciativa');
                $integrante_proyecto->addChild('permiso_nulo_proyecto');
                $jefe_proyecto->addChild('ver_tarea_proyecto');
                $jefe_proyecto->addChild('editar_proyecto');

                $auth->createOperation('idi_ver_proyectos_postulantes', 'Ver los proyectos postulantes del fondo IDI contando los no enviados');
                $administrador->addChild('idi_ver_proyectos_postulantes');

                $auth->createOperation('idi_ver_proyectos_enviados', 'Ver los proyectos IDI que enviaron postulacion');
                $administrador->addChild('idi_ver_proyectos_enviados');

                $auth->createOperation('idi_cerrar_postulacion', 'Cerrar el periodo de postulacion a los fondos IDI');
                $administrador->addChild('idi_cerrar_postulacion');

                $auth->createOperation('idi_abrir_revision', 'Abrir el periodo de revisi&oacute;n de los fondos IDI');
                $administrador->addChild('idi_abrir_revision');

                //se autentifica al administrador
                $auth = new LoginForm;
                $auth->username = $model->email;
                $auth->password = $pass_original;
                if ($auth->login())
                    $this->redirect(array('site/index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'perfil' => $perfil
        ));
    }

    //usado internamente, en piea a veces piden que se creen nuevos permisos, yo los creaba con esta
    //funcion.
    public function actionPermisos()
    {
        $auth = Yii::app()->authManager;

        $administrador = $auth->getAuthItem('administrador');
        $usuario = $auth->getAuthItem('usuario');
        $invitado = $auth->getAuthItem('invitado');
        $coordinador_iniciativa = $auth->getAuthItem('coordinador_iniciativa');
        $integrante_iniciativa = $auth->getAuthItem('integrante_iniciativa');
        $jefe_proyecto = $auth->getAuthItem('jefe_proyecto');
        $integrante_proyecto = $auth->getAuthItem('integrante_proyecto');

        /*$auth->createOperation('ver_menu_idi', 'Ver el menu de los fondos I+D+i');
        $ver_postulantes=$auth->getAuthItem('idi_ver_proyectos_postulantes');
        $ver_enviados=$auth->getAuthItem('idi_ver_proyectos_enviados');
        $cerrar_postulacion=$auth->getAuthItem('idi_cerrar_postulacion');
        $abrir_revision=$auth->getAuthItem('idi_abrir_revision');
        $ver_postulantes->addChild('ver_menu_idi');
        $ver_enviados->addChild('ver_menu_idi');
        $cerrar_postulacion->addChild('ver_menu_idi');
        $abrir_revision->addChild('ver_menu_idi');

        $auth->createOperation('dar_permiso_de_revisor_fondos_idi', 'Dar permisos de revision en el formulario de fondos IDI');
        $administrador->addChild('dar_permiso_de_revisor_fondos_idi');

        $auth->createOperation('idi_cerrar_revision', 'Cerrar el periodo de revisi&oacute;n de los fondos IDI');
        $administrador->addChild('idi_cerrar_revision');

        $auth->createOperation('idi_estado_proyectos', 'Estado de revisiones de proyectos en los fondos IDI');
        $administrador->addChild('idi_estado_proyectos');*/

        $auth->createOperation('ver_usuarios_del_sistema', 'Ver el listado de usuarios del sistema');
        $administrador->addChild('ver_usuarios_del_sistema');

        $this->redirect(array('site/index'));
    }

    //se uso para la primera iteracion del formulario de fondos IDI. Hubo un error en la asignacion de permisos a los
    //integrantes por lo que hubo que hacer un hotfix para solucionar el bug. Este es dicho hotfix. Quizas sirva
    //para mas adelante.
    public function actionNormalizarPermisosProyectos(){
        if(user()->checkAccess('administrador')){
            $auth = Yii::app()->authManager;
            $proyectos=Proyecto::model()->findAll();
            //se van a recorrer todos los proyectos y chequear que todos los integrantes tengan los permisos necesarios
            //para ver sus proyectos
            foreach ($proyectos as $proyecto) {
                $conn=app()->db;
                $sql="SELECT user_id FROM {{lookup_proyecto}} WHERE proyecto_id=".$proyecto->id." and activo=1 and nombre_item='jefe_proyecto'";
                $command=$conn->createCommand($sql);
                $row=$command->queryRow();
                $lider=$row['user_id'];
                foreach ($proyecto->integrantes as $integrante) {
                    if($integrante->user_id!=$lider){
                        if($auth->getAuthAssignment("integrante_proyecto", $integrante->user_id) == null){
                            $auth->assign('integrante_proyecto', $integrante->user_id);
                        }
                        $keyArray['nombre_item'] = 'integrante_proyecto';
                        $keyArray['proyecto_id'] = $proyecto->id;
                        $keyArray['user_id'] = $integrante->user_id;
                        $lok = LookupProyecto::model()->findByPk($keyArray);
                        if ($lok == null) {
                            $lookup = new LookupProyecto;
                            $lookup->nombre_item = 'integrante_proyecto';
                            $lookup->proyecto_id = $proyecto->id;
                            $lookup->user_id = $integrante->user_id;
                            $lookup->save(false);
                        }
                    }
                }
            }
        }
    }

    public function actionInvitacionInterna()
    {
        $invitacion = new InvitacionInterna;
        if (isset($_POST['invitar-interno'])) {
            $invitacion->attributes = $_POST['invitar-interno'];

            if ($invitacion->validate()) {
                if ($invitacion->tipo_entidad == 'iniciativa') {
                    $entidad = Iniciativa::model()->findByPk($invitacion->id_entidad);
                    $nombre = $entidad->nombre_abreviado;
                    $url = array('/iniciativas/crudIniciativas/ver', 'id' => $entidad->id);
                } else if ($invitacion->tipo_entidad == 'proyecto') {
                    $entidad = Proyecto::model()->findByPk($invitacion->id_entidad);
                    $nombre = $entidad->nombre;
                    $url = array('/proyectos/crud/ver', 'id' => $entidad->id);
                }

                //TODO: se debe verificar que el usuario puede realizar estas invitaciones
                $invitacion = new InvitacionMembresia;
                $invitacion->usuario_id = $invitacion->id_usuario;
                $invitacion->tipo_entidad = $invitacion->tipo_entidad;
                $invitacion->entidad_id = $invitacion->id_entidad;
                $invitacion->entidad_nombre = $nombre;
                $invitacion->enviarSolicitud();
                Yii::app()->user->setFlash('success', 'La invitaci&oacute;n ha sido enviada con &eacute;xito.');
                $this->redirect($url);
            } else {
                $this->redirect(array('/usuario/userActions/invitar'));
            }
        }
        $this->redirect(array('site/index'));
    }

    public function actionAceptarMembresia($id)
    {
        //se carga la invitacion correspondiente
        $mensaje = Mensaje::model()->findByPk($id);
        if ($mensaje == null || $mensaje->destinatario != Yii::app()->user->id)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        $model = Persona::model()->findByPk(Yii::app()->user->id);
        //se le debe asignar un rol de usuario
        $auth = Yii::app()->authManager;

        //se agrega a la correspondiente iniciativa o proyecto
        if ($mensaje->tipo_relacion == 'iniciativa') {
            //verifico que no sea integrante de otra iniciativa, si lo es entonces ya tiene el permiso asignado
            if ($auth->getAuthAssignment("integrante_iniciativa", $model->id) == null) {
                $admin = $auth->assign('integrante_iniciativa', $model->id);
            }
            //seteo el lookup de la iniciativa correspondiente
            $lookup = new LookupIniciativa;
            $lookup->nombre_item = 'integrante_iniciativa';
            $lookup->iniciativa_id = $mensaje->id_relacion;
            $lookup->user_id = $model->id;
            $lookup->save(false);

            //se debe tambien ingresar el integrante nuevo
            $integrante = new IntegranteIniciativa;
            $integrante->iniciativa_id = $mensaje->id_relacion;
            $integrante->user_id = $model->id;
            $integrante->save(false);
            Yii::app()->user->setFlash('success', 'Felicidades, ahora formas parte del equipo de la presente iniciativa.');
            $url = array('/iniciativas/crudIniciativas/ver', 'id' => $mensaje->id_relacion);
        }

        if ($mensaje->tipo_relacion == 'proyecto') {
            //verifico que no sea integrante de otra iniciativa, si lo es entonces ya tiene el permiso asignado
            if ($auth->getAuthAssignment("integrante_proyecto", $model->id) == null) {
                $admin = $auth->assign('integrante_proyecto', $model->id);
            }
            //seteo el lookup del proyecto correspondiente
            $lookup = new LookupProyecto;
            $lookup->nombre_item = 'integrante_proyecto';
            $lookup->proyecto_id = $mensaje->id_relacion;
            $lookup->user_id = $model->id;
            $lookup->save(false);

            //se debe tambien ingresar el integrante nuevo
            $integrante = new IntegranteProyecto;
            $integrante->proyecto_id = $mensaje->id_relacion;
            $integrante->user_id = $model->id;
            $integrante->save(false);
            Yii::app()->user->setFlash('success', 'Felicidades, ahora formas parte del equipo del presente proyecto.');
            $url = array('/proyectos/crud/ver', 'id' => $mensaje->id_relacion);
        }
        $mensaje->contestado = 1;
        $mensaje->save();
        $this->redirect($url);
    }

    /**
     * Permite buscar un usuario entre los usuarios registrados en la plataforma. Utiliza el metodo
     * search() que se encuentra en la clase Persona.
     *
     * @return  void
     *
     * @since   21-01-2013
     *
     */
    public function actionBuscar(){

        $this->render('buscar_usuario');
    }

    /**
     * Accion que permite al usuario administrador generar un excel con todos los usuarios del sistema junto a la
     * informacion mas importante para cada uno de estos.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-05-2013
     */
    public function actionExcelUsuariosSistema(){
        if(user()->checkAccess('administrador')){
            $data=new CActiveDataProvider("Persona", array(
                'pagination'=>false,
            ));

            $this->toExcel($data,
                array(
                    'id',
                    'nombres::Nombres',
                    'apellido_paterno::Apellido Paterno',
                    'apellido_materno::Apellido Materno',
                    'email',
                    'perfil.rut::Rut',
                    'perfil.rol::Rol',
                    'perfil.fecha_nacimiento::Fecha de nacimiento',
                    'perfil.telefono_contacto::Telefono de Contacto',
                    'perfil.direccion::Direccion',
                    'perfil.direccion_laboral::Direccion Laboral',
                    'perfil.contacto_emergencia::Contacto de emergencia'
                ),
                'Usuarios del sistema',
                array(
                    'creator' => 'Zen',
                ),
                'Excel2007' // This is the default value, so you can omit it. You can export to CSV, PDF or HTML too
            );
        }else
            throw new CHttpException(403,"El sistema ha detectado que no posees los permisos necesarios para ingresar a
            esta p&aacute;gina.");
    }

    /**
     * Validacion ajax para un modelo.
     * @param CModel el modelo a validar
     * @param String id_form el id del formulario a validar. Por defecto es 'usuario-form'
     */
    protected function performAjaxValidation($model, $id_form = 'usuario-form')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $id_form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Acciones automaticas del controlador. Ver @link{CAction}
     *
     * @return  array array de acciones
     */
    public function actions()
    {
        return array(
            'buscarIntegrante' => array(
                'class' => 'application.extensions.AutoCompleteAction',
                'model' => 'Persona',
                'attribute' => 'nombres',
            ),
        );
    }

    /**
     * Behaviors del componente controlador.
     *
     * @return  array array de behaviors
     *
     * @since   31-05-2013
     */
    public function behaviors()
    {
        return array(
            'eexcelview'=>array(
                'class'=>'ext.eexcelview.EExcelBehavior',
            ),
        );
    }

}