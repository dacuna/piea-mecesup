<?php
/**
 * Controlador para manejar el login y logout de los usuarios del sistema.
 *
 * @package   modules.usuarios.controllers
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AuthenticateController extends Controller
{
    /**
     * Muestra el formulario de inicio de sesion y permite que los usuarios realicen login
     * dentro del sistema. La logica del login queda a cargo del @link{LoginForm}.
     */
    public function actionLogin()
    {
        $model = new LoginForm;

        // ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // si el usuario se logea en el sistema...
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validar usuario y autenticarlo.
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }

        $this->render('login', array('model' => $model));
    }

    /**
     * Cierra la sesion del usuario autenticado y retorna al inicio del sistema.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}