<?php
/**
 * SolicitudMembresia: componente que maneja la logica de solicitar
 * una membresia en una iniciativa
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package iniciativas.components
 * @since 1.0
 * @todo Con la refactorizacion del MailMan se debe eliminar esta clase y utilizar el envio de notificaciones mediante eventos
 */
class InvitacionMembresia extends CComponent
{
  public $usuario_id;
  public $tipo_entidad;
  public $entidad_id;
  public $entidad_nombre;
  
  public function enviarSolicitud() {
    $mailman=Yii::app()->mailman;
    $asunto='Invitaci&oacute;n a '.$this->tipo_entidad.' '.$this->entidad_nombre;
    $mailman->crearMensaje($asunto,'temp');
    $mailman->setPublisher(Yii::app()->user->id);
    $mailman->setSuscriber($this->usuario_id);
    $mailman->setRelation($this->entidad_id,$this->tipo_entidad);
    $id_mensaje=$mailman->enviarMensaje();
    $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
  }
  
  public function mensaje($id)
  {
    $usuario=Persona::model()->findByPk($this->usuario_id);
    $mensaje='Estimado '.$usuario->nombreCompleto.':<br><br>';
    $mensaje.='Has sido invitado a formar parte del equipo de '.$this->entidad_nombre;
    $mensaje.='. Para aceptar la invitaci&oacute;n presiona en el bot&oacute;n "Aceptar invitaci&oacute;n"';
    $mensaje.=', para rechazarla presiona en "Rechazar invitaci&oacute;n".<br><br>';
    $mensaje.='Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/usuarios/userActions/aceptarMembresia',array('id'=>$id)).'" class="nice radius small button">Aceptar invitaci&oacute;n</a> ';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/proyectos/gestionIntegrantes/rechazarSolicitudMembresia',array('id'=>$id)).'" class="nice radius small red button">Rechazar invitaci&oacute;n</a> ';
    
    return $mensaje;
  }
  
}