<?php
/**
 * La clase entidad representa tanto a un proyecto como una iniaciativa del sistema.
 *
 * Esta clase fue creada para unificar las clases @link{Proyecto} y @link{Iniciativa} para tener una
 * clase que permita realizar todas las funciones sobre proyectos y iniciativas de manera unificada.
 * Utilizar esta clase provee de multiples ventajas sobre instanciar un proyecto o iniciativa, por
 * ejemplo permite que se puedan crear modulos basados en entidades y no en proyectos o iniciativas
 * reduciendo asi la duplicacion de codigo. Se agregaron funciones de mucha utilidad por ejemplo aquellas
 * que permiten chequear los permisos de los usuarios sobre los proyectos e iniciativas de manera sencilla.
 * Para mas detalle ver cada funcion. Si se desea simplificar el desarrollo en acciones que involucren
 * proyectos y iniciativas entonces se debe usar esta clase. Se utiliza extensivamente en los modulos de
 * finanzas, tareas y formularios.
 *
 * @package   modules.usuarios.components
 * @example <br />
 *    $entidad = new Entidad($id, $tipo);<br />
 *  $entidad->checkPermisos('integrante',user()->id);<br />
 * @version   1.0
 * @since     2012-10-20
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Entidad
{
    /**
     * ID de la entidad.
     * @var int
     */
    private $id;

    /**
     * Nombre de la entidad. En proyectos es el atributo nombre, en iniciativas es el atributo
     * nombre_abreviado.
     * @var string
     */
    private $nombre;

    /**
     * Entrega una instancia de la clase que representa la entidad. Para un proyecto retorna una
     * instancia valida de @link{Proyecto} y para iniciativas retorna una instancia de @link{Iniciativa}.
     * @var mixed
     */
    private $entidad;

    /**
     * Permite diferenciar si es que la entidad es una iniciativa y un proyecto. Si es 0 representa
     * una iniciativa, 1 representa un proyecto.
     * @var int
     */
    private $tipo_entidad;

    /**
     * Constructor de la clase. Instancia la clase correspondiente que representa la entidad y
     * asigna los atributos de la clase.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad. 0=iniciativa, 1=proyecto
     *
     * @throws  \CHttpException
     */
    public function __construct($id, $tipo)
    {
        if ($tipo == 0 && $id != 0)
            $entidad = Iniciativa::model()->findByPk($id);
        else if ($tipo == 1)
            $entidad = Proyecto::model()->findByPk($id);
        else
            $entidad = new PieaEntidad;
        if ($entidad == null)
            throw new CHttpException(500, 'Petici&oacute;n incorrecta. Por favor, no ejecute nuevamente esta petici&oacute;n.');
        $this->tipo_entidad = $tipo;
        $this->id = $id;
        $this->entidad = $entidad;
    }

    /**
     * Retorna el id de la entidad
     *
     * @return  int id de la entidad
     */
    public function getId()
    {
        return $this->entidad->id;
    }

    /**
     * Retorna el nombre de la entidad.
     *
     * @return  string nombre de la entidad
     */
    public function getNombre()
    {
        if ($this->tipo_entidad == 0 && $this->id != 0)
            return $this->entidad->nombre_abreviado;
        return $this->entidad->nombre;
    }

    /**
     * Retorna un string con la url a la vista de "ver" de la entidad correspondiente. Recordar que
     * para las iniciativas dicha vista esta en la accion crud/ver del modulo iniciativas y para los
     * proyectos en la accion crud/ver del modulo proyectos. Es por esto que se necesita un metodo que
     * unifique dicho comportamiento.
     *
     * @return  string url a la accion crud/ver de la entidad
     */
    public function urlEntidad()
    {
        if ($this->tipo_entidad == 0)
            $phrase = "crudIniciativas";
        else
            $phrase = "crud";
        return '/' . strtolower($this->getTipoEntidad()) . 's/' . $phrase . '/ver';
    }

    /**
     * Retorna un version string de cual es el tipo de entidad que es la entidad actual, es decir, si
     * es que es un iniciativa o proyecto.
     *
     * @return  string tipo de la entidad
     */
    public function getTipoEntidad()
    {
        if ($this->tipo_entidad == 0)
            return "Iniciativa";
        else if ($this->tipo_entidad == 1)
            return "Proyecto";
        else
            return "PIEA";
    }

    /**
     * Retorna un version int de cual es el tipo de entidad que es la entidad actual, es decir, si
     * es que es un iniciativa (0) o proyecto (1).
     *
     * @return  int tipo de la entidad
     */
    public function getTipoEntidadNumerico()
    {
        return $this->tipo_entidad;
    }

    /**
     * Retorna una instancia de la entidad actual. Por ejemplo, si la entidad es un proyecto entonces
     * retorna la instancias @link{Proyecto} que corresponde con la entidad actual. Similar para una
     * iniciativa.
     *
     * @return  mixed tipo de la entidad
     */
    public function getEntidad()
    {
        return $this->entidad;
    }

    /**
     * Setter del atributo entidad.
     */
    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    /**
     * Retorna el articulo gramatical asociado con la entidad actual. Esto es util al escribir texto
     * relacionado con la entidad. Por ejemplo se puede utilizar como $entidad->articuloEntidad
     * $entidad->getTipoEntidad() para obtener "la iniciativa" o "el proyecto".
     *
     * @return  string articulo gramatical de la entidad.
     */
    public function articuloEntidad()
    {
        if ($this->tipo_entidad == 0)
            return "la";
        if ($this->tipo_entidad == 1)
            return "el";
        return "";
    }

    /**
     * verificarJerarquiaCargo: verifica para el cargo $cargo si es que el usuario identificado
     * por $usuario_id tiene jerarquia sobre este. El procedimiento es, si es que el usuario
     * asigno directamente el cargo entonces tiene jerarquia, sino, se comienza a buscar recursivamente
     * sobre las asignaciones superiores hasta encontrar alguna que corresponda o retornar falso
     * si es que no se encontro ninguna.<br><br>Como los roles de coordinador iniciativa y jefe de proyecto
     * no son cargo, a priori no tienen jerarquia sobre ningun otro cargo, es por esto que el metodo agrega
     * dicha jerarquia explicitamente, asi por ejemplo un coordinador de iniciativa si tendra jerarquia sobre
     * cargos en dicha iniciativa.
     *
     * @param mixed $cargo       Puede ser LookupIniciativa o LookupProyecto dependiendo de la entidad
     * @param int $usuario_id  ID del usuario identificado
     *
     * @throws   \CException
     * @return  bool true si es que hay jerarquia, false en caso contrario
     *
     * @since   17-10-2012
     * @TODO refactorizar metodo para que busque en $usuario_id, actualemente solo verifica permisos
     * en user()->id
     */
    public function verificarJerarquiaCargo($cargo, $usuario_id = null)
    {
        if ($cargo == null) //condicion de termino
        return false;
        $nombre_clase = get_class($cargo);
        if ($nombre_clase == 'LookupIniciativa') {
            //si es coordinador, se tiene la jerarquia lista
            if (user()->checkAccess('coordinador_iniciativa', array('iniciativa' => $cargo->iniciativa_id)))
                return true;
            $check = array('iniciativa' => $cargo->iniciativa->id);
        } else if ($nombre_clase == 'LookupProyecto') {
            if (user()->checkAccess('jefe_proyecto', array('iniciativa' => $cargo->proyecto->iniciativa_id, 'proyecto' => $cargo->proyecto->id)))
                return true;
            $check = array('iniciativa' => $cargo->proyecto->iniciativa_id, 'proyecto' => $cargo->proyecto_id);
        } else
            throw new CException('$cargo debe ser del tipo LookupIniciativa o LookupProyecto');
        $mas = $cargo->nombre_item;
        if (user()->checkAccess($cargo->nombre_item, $check))
            return true;
        $rec_cargo = $this->obtenerCargoActivo($cargo->cargo_que_asigno);
        return $this->verificarJerarquiaCargo($rec_cargo);
    }

    /**
     * obtenerCargoActivo: obtiene el objeto de tipo \LookupIniciativa o
     * \LookupProyecto (segun sea el caso) para el cargo especificado por
     * $nombre_cargo. El objeto obtenido representa un cargo activo (es decir
     * un cargo que actualmente esta en uso) dentro de la presente entidad.
     *
     * @param string $nombre_cargo El nombre del cargo que se desea obtener
     *
     * @return  mixed  LookupIniciativa o LookupProyecto segun sea el tipo de la
     *                 presente entidad o null si no existe dicho cargo en la entidad.
     *
     * @since   17-10-2012
     *
     */
    public function obtenerCargoActivo($nombre_cargo)
    {
        if ($this->tipo_entidad == 0) {
            $lookup = 'LookupIniciativa';
            $query = 'iniciativa_id';
        } else {
            $lookup = 'LookupProyecto';
            $query = 'proyecto_id';
        }

        $previo = $lookup::model()->find('nombre_item=:ni and ' . $query . '=:ii and activo=1 and es_cargo=1', array(
            ':ni' => $nombre_cargo, ':ii' => $this->getId()
        ));

        //verifico la posibilidad de que sea un cargo que esta asignado a una persona nula
        if ($previo == null) {
            $previo = $lookup::model()->find('nombre_item=:ni and ' . $query . '=:ii and activo=0 and es_cargo=1 and asignacion_nula=1', array(
                ':ni' => $nombre_cargo, ':ii' => $this->getId()
            ));
        }

        return $previo;
    }

    /**
     * verificarAsignacionDeCargo: verifica si es que el usuario especificado por $usuario_id puede
     * asignar cargos dentro de la presente entidad. Solo los usuarios que tienen un cargo asignado
     * en la entidad pueden asignar cargos, o bien, si es que son coordinadores en el caso de las
     * iniciativas o jefes de proyecto en el caso de proyectos.
     *
     * @param   int $usuario_id  ID del usuario en el que se desea saber si es que puede asignar cargos.
     *
     * @return  bool  true si es que puede asignar cargos, false en caso contrario.
     *
     * @since   21-10-2012
     *
     */
    public function verificarAsignacionDeCargo($usuario_id)
    {
        $first_check = $this->getCargoDeUsuario($usuario_id);
        if ($this->getTipoEntidadNumerico() == 0) {
            if ($first_check != null || am()->checkAccess('coordinador_iniciativa', $usuario_id, array('iniciativa' => $this->getId())))
                return true;
        } else {
            if ($first_check != null || am()->checkAccess('jefe_proyecto', $usuario_id, array('iniciativa' => $this->entidad->iniciativa_id, 'proyecto' => $this->getId())))
                return true;
        }
        return false;
    }

    /**
     * getCargoDeUsuario: obtiene el cargo activo que tiene actualmente un usuario dentro de la
     * presente entidad. Obtiene solo los cargos, es decir, si un usuario es por ejemplo coordinador
     * de una iniciativa (que no es un cargo, es un rol) y dicho usuario no tiene un cargo, entonces
     * el metodo retornara null. Solo se devulven los cargos explicitos (los que tienen la propiedad
     * es_cargo=1).
     *
     * @param   int $user_id  ID del usuario al que se le busca el cargo en la presente entidad.
     *
     * @return  mixed  LookupIniciativa o LookupProyecto del cargo segun el tipo de la presente entidad o
     *                 null si no se encuentra ningun cargo.
     *
     * @since   17-10-2012
     *
     */
    public function getCargoDeUsuario($user_id)
    {
        $class = 'Lookup' . $this->getTipoEntidad();
        $lookup = $class::model()->find(strtolower($this->getTipoEntidad()) . '_id=:ii and user_id=:ui and activo=1 and es_cargo=1',
            array(':ii' => $this->getId(), ':ui' => $user_id));
        if ($lookup != null)
            return $lookup;
        return null;
    }

    /**
     * getEncargadoCargo: obtiene la persona que es el superior encargado de un cargo. Se entiende
     * como superior encargado a la persona que asigno el cargo especificado por $cargo, si es que
     * el cargo no tiene cargo_que_asigno, entonces se busca en la estructura de cargos una persona
     * encargada. En ultima instancia dicha persona sera el coordinador en el caso de una iniciativa
     * o el jefe de proyecto en el caso de proyectos.
     *
     * @param   mixed $cargo  El cargo al que se le busca el superior encargado. Debe ser del tipo
     *                         LookupIniciativa o LookupProyecto.
     *
     * @return  Persona El usuario superior encargado del cargo especificado por $cargo.
     * @throws  \CException
     *
     * @since   21-10-2012
     *
     */
    public function getEncargardoSuperiorCargo($cargo)
    {
        if ($cargo == null) {
            //para el caso de iniciativa chequeo el permiso de coordinador, en caso contrario el permiso de jefe de proyecto
            if ($this->getTipoEntidadNumerico() == 0) {
                $lookup = LookupIniciativa::model()->find('nombre_item=:ni and iniciativa_id=:ii and activo=1', array(
                    ':ni' => 'coordinador_iniciativa', ':ii' => $this->getId()));
                return $lookup->usuario;
            } else {
                $lookup = LookupProyecto::model()->find('nombre_item=:ni and proyecto_id=:ii and activo=1', array(
                    ':ni' => 'jefe_proyecto', ':ii' => $this->getId()));
                return $lookup->usuario;
            }
        }
        $rec_cargo = $this->obtenerCargo($cargo->cargo_que_asigno);
        if ($rec_cargo != null && $rec_cargo->activo = 1)
            return $rec_cargo->usuario;
        return $this->getEncargardoSuperiorCargo($rec_cargo);
    }

    /**
     * obtenerCargo: obtiene el objeto de tipo \LookupIniciativa o
     * \LookupProyecto (segun sea el caso) para el cargo especificado por
     * $nombre_cargo. El objeto obtenido representa ya sea un cargo activo
     * o un cargo inactivo (el metodo devolvera cualquiera de los dos) dentro
     * de la presente entidad. Se devuelve el primer cargo que cumpla con el
     * $nombre_item (dado que puede haber mas de un cargo asignado en una entidad
     * con el mismo nombre, OJO: solo uno de esos cargos estara activo). Si se
     * desea obtener solamente el cargo activo se debe utilizar el metodo
     * obtenerCargoActivo().
     *
     * @param string $nombre_cargo El nombre del cargo que se desea obtener
     *
     * @return  mixed  LookupIniciativa o LookupProyecto segun sea el tipo de la
     *                 presente entidad o null si no existe dicho cargo en la entidad.
     *
     * @since   17-10-2012
     *
     */
    public function obtenerCargo($nombre_cargo)
    {
        if ($this->tipo_entidad == 0) {
            $lookup = 'LookupIniciativa';
            $query = 'iniciativa_id';
        } else {
            $lookup = 'LookupProyecto';
            $query = 'proyecto_id';
        }

        return $lookup::model()->find('nombre_item=:ni and ' . $query . '=:ii and es_cargo=1', array(
            ':ni' => $nombre_cargo, ':ii' => $this->getId()
        ));
    }

    /**
     * existeDependenciaCargo: verifica si es que para el cargo especificado por $cargo existen
     * otros cargos que dependan de el. Es decir, si es que el cargo $cargo a asignado a otros
     * cargos en la presente entidad.
     *
     * @param   mixed $cargo  El cargo que se desea verificar, del tipo LookupIniciativa en el
     *                         caso de una iniciativa o LookupProyecto en el caso de un proyecto.
     *
     * @return  bool  true si es que el cargo tiene cargos dependientes, false si no tiene ninguno.
     *
     * @since   16-10-2012
     *
     */
    public function existeDependenciaCargo($cargo)
    {
        if ($this->tipo_entidad == 0) {
            $lookup = 'LookupIniciativa';
            $query = 'iniciativa_id';
        } else {
            $lookup = 'LookupProyecto';
            $query = 'proyecto_id';
        }

        $data = $lookup::model()->find($query . '=:ii and activo=1 and es_cargo=1 and cargo_que_asigno=:ca', array(
            ':ii' => $this->getId(), ':ca' => $cargo->nombre_item
        ));
        if ($data == null)
            return false;
        return true;
    }

    /**
     * Retorna una lista con lo integrantes de la entidad.
     *
     * @return  array integrantes de la entidad
     */
    public function getIntegrantes()
    {
        return $this->entidad->integrantes;
    }

    /**
     * Permite obtener el usuario encargado de la entidad. En el caso de las iniciativas se retorna el coordinador
     * de la iniciativa, en el caso de los proyectos es el jefe de proyecto.
     *
     * @return  \Persona
     * @throws  \CHttpException
     *
     * @since   16-10-2012
     */
    public function getEncargado()
    {
        $conn = app()->db;
        if ($this->getTipoEntidadNumerico() == 0) {
            $sql = "SELECT user_id FROM {{lookup_iniciativa}} WHERE iniciativa_id=" . $this->getId() . " and activo=1 and
                  nombre_item='coordinador_iniciativa'";
        } else {
            $sql = "SELECT user_id FROM {{lookup_proyecto}} WHERE proyecto_id=" . $this->getId() . " and activo=1 and
                  nombre_item='jefe_proyecto'";
        }
        $command = $conn->createCommand($sql);
        $row = $command->queryRow();
        $usuario = Persona::model()->findByPk($row['user_id']);
        return $usuario;
    }

    /**
     * Retorna la persona que es superior al encargado de la entidad. Por ejemplo, para un proyecto
     * el superior seria el jefe de la iniciativa a la que el proyecto pertenece, para una iniciativa
     * el superior seria el administrador PIEA.
     *
     * @return  Persona
     */
    public function getSuperior()
    {
        //se retorna al administrador
        if ($this->getTipoEntidadNumerico() == 0)
            return Persona::model()->findByPk(1);
        $conn = app()->db;
        $iniciativa_id = $this->getEntidad()->iniciativa_id;
        $sql = "SELECT user_id FROM {{lookup_iniciativa}} WHERE iniciativa_id={$iniciativa_id} and activo=1 and
                nombre_item='coordinador_iniciativa'";
        $command = $conn->createCommand($sql);
        $row = $command->queryRow();
        $usuario = Persona::model()->findByPk($row['user_id']);
        return $usuario;
    }

    /**
     * Chequea los permisos de un usuario dentro de la entidad. Si se especifica el parametro
     * with_suffix entonces el metodo no asumira cual es el tipo de entidad que se esta
     * manejando por lo que el permiso debera especificarse por completo. Por ejemplo, para
     * chequear si un usuario es integrante de la entidad basta con llamar a la funcion como
     * $entidad->checkPermsisos('integrante',user()->id), pero si se especifica $with_suffix
     * entonces se debera llamar $entidad->checkPermisos('integrante_iniciativa',user()->id,true),
     * esto sirve debido a que para iniciativas existen permisos que no existen para proyetos
     * y viceversa. En este metodo si el usuario no tiene los permisos entonces se lanza una
     * excepcion.
     *
     * @param string $permiso permiso a chequear
     * @param int $user_id ID del usuario
     * @param bool $with_suffix no considerar el tipo de entidad. Por defecto si se considera (false).
     *
     * @return  bool true si el usuario tiene los permisos, false en caso contrario
     * @throws  \CHttpException
     */
    public function checkPermisos($permiso, $user_id, $with_suffix = false)
    {
        if ($this->getTipoEntidadNumerico() == 0) {
            if ($permiso == 'encargado') $permiso = 'coordinador';
            if (!$with_suffix)
                $permiso .= '_iniciativa';
            if (!am()->checkAccess($permiso, $user_id, array('iniciativa' => $this->id)))
                throw new CHttpException(403, 'No tienes los permisos suficientes para ver esta p&aacute;gina.');
        } else {
            if ($permiso == 'encargado') $permiso = 'jefe';
            if (!$with_suffix)
                $permiso .= '_proyecto';
            if (!am()->checkAccess($permiso, $user_id, array('iniciativa' => $this->entidad->iniciativa_id, 'proyecto' => $this->id)))
                throw new CHttpException(403, 'No tienes los permisos suficientes para ver esta p&aacute;gina.');
        }
        return true;
    }

    /**
     * Chequea los permisos de un usuario dentro de la entidad. Si se especifica el parametro
     * with_suffix entonces el metodo no asumira cual es el tipo de entidad que se esta
     * manejando por lo que el permiso debera especificarse por completo. Por ejemplo, para
     * chequear si un usuario es integrante de la entidad basta con llamar a la funcion como
     * $entidad->checkPermsisos('integrante',user()->id), pero si se especifica $with_suffix
     * entonces se debera llamar $entidad->checkPermisos('integrante_iniciativa',user()->id,true),
     * esto sirve debido a que para iniciativas existen permisos que no existen para proyetos
     * y viceversa. En este metodo si el usuario no tiene los permisos entonces se retorna un
     * boolean.
     *
     * @param string $permiso permiso a chequear
     * @param int $user_id ID del usuario
     * @param bool $with_suffix no considerar el tipo de entidad. Por defecto si se considera (false).
     *
     * @return  bool true si el usuario tiene los permisos, false en caso contrario
     * @throws  \CHttpException
     */
    public function checkPermisosBoolean($permiso, $user_id, $with_suffix = false)
    {
        if ($this->getTipoEntidadNumerico() == 0) {
            if ($permiso == 'encargado') $permiso = 'coordinador';
            if (!$with_suffix)
                $permiso .= '_iniciativa';
            if (!am()->checkAccess($permiso, $user_id, array('iniciativa' => $this->id)))
                return false;
        } else {
            if ($permiso == 'encargado') $permiso = 'jefe';
            if (!$with_suffix)
                $permiso .= '_proyecto';
            if (!am()->checkAccess($permiso, $user_id, array('iniciativa' => $this->entidad->iniciativa_id, 'proyecto' => $this->id)))
                return false;
        }
        return true;
    }

    /**
     * Retorna la evaluacion FAE de un integrante de la entidad.
     *
     * @param int $integrante ID del integrante
     *
     * @return  \EvaluacionFae
     *
     * @since   05-05-2013
     */
    public function obtenerEvaluacionFaeIntegrante($integrante)
    {
        $evaluacion = EvaluacionFae::model()->find('id_evaluado=:ie and id_entidad=:in and tipo_entidad=:te', array(
            ':ie' => $integrante, ':in' => $this->getId(), ':te' => $this->getTipoEntidadNumerico()
        ));
        return $evaluacion;
    }

    /**
     * Get magic method implementation.
     *
     * @param string $oname nombre del metodo a llamar
     *
     * @return  mixed retorno de llamada al metodo o false si no se puede invocar un callable
     */
    public function __get($oname)
    {
        $name = 'get' . ucfirst($oname);
        if (method_exists($this, $name))
            return $this->$name();
        if (method_exists($this->entidad, $name))
            return $this->entidad->$oname;
        if (property_exists($this->entidad, $oname))
            return $this->entidad->$oname;
        return false;
    }
}