<?php
/**
 * UserIdentity representa la informacion y los metodos necesarios
 * para identificar a un usuario en la plataforma.
 *
 * Contiene el metodo authenticate que provee la funcionalidad para
 * verificar la identidad de un usuario contra la base de datos del
 * sistema.
 *
 * @version   1.0
 * @since     2012-07-09
 * @author    dacuna <diego.acuna@usm.cl>
 */
class UserIdentity extends CUserIdentity
{
    /**
     * id del usuario autenticado
     * @var int
     */
    private $_id;

    /**
     * Autentica a un usuario en el sistema. La autenticacion se realiza contra la informacion contenida en
     * la base de datos del sistema.
     *
     * @return boolean si la autenticacion fue exitosa (true) o no (false).
     */
    public function authenticate()
    {
        //se recomienda que el username en la db tenga un index unique
        $record = Usuario::model()->findByAttributes(array('email' => $this->username));
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($record->password !== sha1($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $record->id;
            $this->setState('nombre', $record->nombres . ' ' . $record->apellido_paterno . ' ' . $record->apellido_materno);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    /**
     * Retorna la clave primaria del usuario autenticado
     * Este metodo sobreescribe el comportamiento por defecto de la clase
     * {@link CUserIdentity} que retorna como primaryKey del usuario el username.
     * En esta implementacion se retorna la id (real PK en la db).
     *
     * @return int ID del usuario autenticado
     */
    public function getId()
    {
        return $this->_id;
    }
}