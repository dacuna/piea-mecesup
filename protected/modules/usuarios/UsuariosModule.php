<?php
/**
 * Modulo que contiene la logica para el manejo de usuarios del sistema.
 *
 * Contiene controladores que permiten gestionar el login, registro, acceso y permisos de los
 * usuarios. Tambien que los usuarios manejen su perfil, ciertas tareas administrativas, su
 * privacidad, recuperacion y cambio de passwords, etc. En resumen permite gestionar tareas
 * relacionadas con los usuarios del sistema.
 *
 * @package   modules.usuarios
 * @version   1.0
 * @since     2012-07-12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class UsuariosModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'usuarios.models.*',
			'usuarios.components.*',
		));
	}

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
