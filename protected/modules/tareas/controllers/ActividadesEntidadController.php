<?php
/**
 * Controlador para manejar las acciones relativas a las actividades agregadas en una entidad.
 *
 * Permite visualizar las actividades existentes en una entidad. A futuro se espera agregar mas
 * acciones que agreguen valor en lo respecto a las actividades existentes en una entidad.
 *
 * @package   modules.tareas.controllers
 * @version   1.0
 * @since     2012-09-04
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ActividadesEntidadController extends Controller
{
    /**
     * Muestra las actividades publicas y aprobadas en una entidad en particular.
     *
     * @param int $id ID de la entidad
     * @param int $tipo tipo de la entidad
     *
     * @return  void
     * @throws  \CHttpException
     * @edit    2013-07-26<br />
     *          Diego Acuna <diego.acuna@usm.cl><br />
     *          Refactorizacion del controlador, eliminado componente Entidad del modulo
     *          en favor del componente Entidad del modulo usuarios.
     */
    public function actionVerActividades($id, $tipo)
    {
        $entidad = new Entidad($id, $tipo);
        $entidad->checkPermisos('integrante', user()->id);
        //se listan las actividades aprobadas de esta entidad
        $model = new Tarea('search');
        $model->unsetAttributes();
        $model->entidad_id = $entidad->getId();
        $model->tipo_entidad = strtolower($entidad->getTipoEntidad());
        $model->aprobada = 1;
        $model->publica = 1;

        if (isset($_GET['Tarea']))
            $model->attributes = $_GET['Tarea'];

        $this->render('ver_actividades', array(
            'entidad' => $entidad,
            'model' => $model,
            'resultados' => $model->generalSearch()
        ));
    }

}