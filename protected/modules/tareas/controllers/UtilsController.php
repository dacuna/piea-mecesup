<?php
/**
 * Controlador con acciones para generar las categorias a las que pertenece una actividad.
 *
 * @package   modules.tareas.controllers
 * @version   1.0
 * @since     2012-08-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class UtilsController extends Controller
{
    /**
     * Obtiene las categorias de nivel 1 (ver archivo de referencia de como se evaluan las actividades
     * PIEA).
     *
     * @return  void
     */
    public function actionObtenerCategorias1()
    {
        $nivel_1 = Yii::app()->input->post('nivel_1');
        $categorias = CategoriaActividad::model()->findAll('nivel=2 and id_padre=:pid',
            array(':pid' => $nivel_1));

        $data = CHtml::listData($categorias, 'id', 'nombre');
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    /**
     * Obtiene las categorias de nivel 2 (ver archivo de referencia de como se evaluan las actividades
     * PIEA).
     *
     * @return  void
     */
    public function actionObtenerCategorias2()
    {
        $nivel_2 = Yii::app()->input->post('nivel_2');
        $categorias = CategoriaActividad::model()->findAll('nivel=3 and id_padre=:pid',
            array(':pid' => $nivel_2));

        $data = CHtml::listData($categorias, 'id', 'nombre');
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    /**
     * Obtiene las categorias de nivel 3 (ver archivo de referencia de como se evaluan las actividades
     * PIEA).
     *
     * @return  void
     */
    public function actionObtenerCategorias3()
    {
        $nivel_3 = Yii::app()->input->post('nivel_3');
        $categorias = CategoriaActividad::model()->findAll('nivel=4 and id_padre=:pid',
            array(':pid' => $nivel_3));

        $data = CHtml::listData($categorias, 'id', 'nombre');
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

}