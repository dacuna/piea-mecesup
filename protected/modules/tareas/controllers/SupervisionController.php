<?php
/**
 * Controlador que contiene las acciones para revision y supervision de tareas..
 *
 * Permite a los participantes de una tarea como tambien a los encargados de entidades el aprobar,
 * rechazar y dar visto bueno a tareas creadas en las entidades del sistema.
 *
 * @package   modules.tareas.controllers
 * @version   1.0
 * @since     2012-08-11
 * @author    dacuna <diego.acuna@usm.cl>
 */
class SupervisionController extends Controller
{

    /**
     * Permite al encargado de una entidad el aprobar una actividad. Notar que dicho encargado puede
     * tambien delegar la aprobacion por lo que tambien se debe verificar dicho comportamiento.
     *
     * @param int $id ID de la tarea a aprobar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAprobarActividad($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //se verifica que el usuario logeado es coordinador o jefe de proyecto
        if ($tarea->tipo_entidad == 'iniciativa') {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_iniciativa}} WHERE nombre_item="coordinador_iniciativa" AND iniciativa_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            if (Yii::app()->user->id != $row['user_id'])
                throw new CHttpException(403, 'No tienes los permisos necesarios para ejecutar esta acci&oacute;n.');
        } else {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_proyecto}} WHERE nombre_item="jefe_proyecto" AND proyecto_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            //tambien debo verificar si es que fue delegada
            $delegacion = $tarea->delegacion;
            if (user()->id != $row['user_id'] && ($delegacion != null && $delegacion->iniciativa->coordinador->id != user()->id))
                throw new CHttpException(403, 'No tienes los permisos necesarios para ejecutar esta acci&oacute;n.');
        }
        //se cambia el estado de la tarea a aprobada
        $tarea->aprobada = 1;
        if ($tarea->save(false)) {
            //se debe notificar a los participantes de que la actividad ha sido aprobada
            $notify = new NotificacionSupervisionTarea;
            $notify->redactar($tarea, 'aprobar');
            $notify->enviarNotificacion();
            Yii::app()->user->setFlash('success', 'Se ha aprobado correctamente la actividad.');

            //se marca el mensaje del buzon que tiene esta aprobacion como contestado
            $mensaje = Mensaje::model()->find('id_tarea=:tid and accion="aprobar_superior" and destinatario=:uid', array(
                ':tid' => $tarea->id, ':uid' => Yii::app()->user->id));
            if ($mensaje != null) {
                $mensaje->contestado = 1;
                $mensaje->accion_realizada = 'Rechazar invitacion';
                $mensaje->save(false);
            }

            $this->redirect(array('/tareas/gestionActividades'));
        } else {
            Yii::app()->user->setFlash('error', 'Ha ocurrido un error al aprobar la actividad. Por favor, int&eacute;ntalo nuevamente.');
            $this->redirect(array('/tareas/supervision/ver', 'id' => $tarea->id));
        }
    }

    /**
     * Permite al encargado de una entidad el rechazar una actividad. Notar que dicho encargado puede
     * tambien delegar el rechazo por lo que tambien se debe verificar dicho comportamiento.
     *
     * @param int $id ID de la tarea a rechazar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRechazarActividad($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //se verifica que el usuario logeado es coordinador o jefe de proyecto
        if ($tarea->tipo_entidad == 'iniciativa') {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_iniciativa}} WHERE nombre_item="coordinador_iniciativa" AND iniciativa_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            if (Yii::app()->user->id != $row['user_id'])
                throw new CHttpException(500, 'No tienes los permisos necesarios para ejecutar esta acci&oacute;n.');
        } else {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_proyecto}} WHERE nombre_item="jefe_proyecto" AND proyecto_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            //tambien debo verificar si es que fue delegada
            $delegacion = $tarea->delegacion;
            if (user()->id != $row['user_id'] || $delegacion->iniciativa->coordinador->id != user()->id)
                throw new CHttpException(500, 'No tienes los permisos necesarios para ejecutar esta acci&oacute;n.');
        }
        if (isset($_POST['comentario_rechazo'])) {
            $rechazo = Yii::app()->input->post('comentario_rechazo');
            //se cambia el estado de la tarea a rechazada
            $tarea->aprobada = -1;
            $tarea->_revision_comment = $rechazo;
            if ($tarea->save(false)) {
                //se debe notificar a los participantes de que la actividad ha sido rechazada
                $notify = new NotificacionSupervisionTarea;
                $notify->redactar($tarea, 'rechazar');
                $notify->enviarNotificacion();
                Yii::app()->user->setFlash('success', 'Se ha rechazado correctamente la actividad.');

                //se marca el mensaje del buzon que tiene esta aprobacion como contestado
                $mensaje = Mensaje::model()->find('id_tarea=:tid and accion="aprobar_superior" and destinatario=:uid', array(
                    ':tid' => $tarea->id, ':uid' => Yii::app()->user->id));
                if ($mensaje != null) {
                    $mensaje->contestado = 1;
                    $mensaje->save(false);
                }

                $this->redirect(array('/tareas/gestionActividades'));
            } else {
                Yii::app()->user->setFlash('error', 'Ha ocurrido un error al rechazar la actividad. Por favor, int&eacute;ntalo nuevamente.');
                $this->redirect(array('/tareas/supervision/ver', 'id' => $tarea->id));
            }
        }
        $this->redirect(array('/tareas/supervision/ver', 'id' => $tarea->id));
    }

    /**
     * Permite al encargado de un proyecto el delegar la aprobacion de una tarea al coordinador de la iniciativa
     * a la que pertenece el proyecto. Esto puede darse si es que el encargado del proyecto participo de la tarea,
     * si es asi entonces no debe aprobar la tarea puesto que tiene conflictos de interes y entonces debera
     * delegar la decision (si es que lo desea).
     *
     * @param int $id ID de la tarea a delegar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionDelegarDecision($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //se verifica que el usuario logeado es coordinador o jefe de proyecto
        if ($tarea->tipo_entidad == 'proyecto') {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_proyecto}} WHERE nombre_item="jefe_proyecto" AND proyecto_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            if (Yii::app()->user->id != $row['user_id'])
                throw new CHttpException(403, 'No tienes los permisos necesarios para ejecutar esta acci&oacute;n.');
            //se debe notificar al coordinador de la iniciativa que evalue esta actividad
            $proyecto = Proyecto::model()->findByPk($tarea->entidad_id);
            $delegacion = new DelegacionTarea;
            $delegacion->proyecto = $proyecto;
            $delegacion->id_tarea = $tarea->id;
            $delegacion->id_iniciativa = $proyecto->iniciativa->id;
            if ($delegacion->save()) {
                Yii::app()->user->setFlash('success', 'Se ha delegado correctamente la aprobaci&oacute;n de la presente actividad');
                $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
            } else {
                Yii::app()->user->setFlash('error', 'Ha ocurrido un error al delegar la actividad. Por favor, int&eacute;ntalo nuevamente.');
                $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
            }
        }
        $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
    }

}