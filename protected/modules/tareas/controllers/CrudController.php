<?php
/**
 * Controlador con las acciones principales para la creacion, edicion y modificacion de tareas.
 *
 * Permite a los usuarios del sistema el crear, editar y guardar tareas dentro de las entidades a las
 * que ellos pertenezcan.
 *
 * @package   modules.tareas.controllers
 * @version   1.0
 * @since     2012-08-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class CrudController extends Controller
{
    /**
     * Permite a un usuario crear una tarea dentro de una iniciativa a la que pertenezca. Recordar que
     * para que una tarea sea aprobada entonces deben aprobarla todos sus participantes y luego el
     * encargado de la entidad donde se creo.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCrear()
    {
        $tarea = new Tarea('create');

        if (isset($_POST['Tarea'])) {
            $tarea->attributes = $_POST['Tarea'];
            if (isset($_POST['CategoriaActividadHasTarea']))
                $tarea->caracteristicas_al_crear = $_POST['CategoriaActividadHasTarea'];
            if ($tarea->validate()) {
                //si solo hay 1 integrante, entonces debo actualizar el estado de la tarea
                $notificar_superior = false;
                if (count($tarea->integrantes) == 1)
                    $notificar_superior = true;
                $tarea->save(false);

                //agrego los integrantes
                $integrantes = $tarea->integrantes;
                foreach ($integrantes as $integrante) {
                    $nuevo = new ParticipanteTarea;
                    $nuevo->tarea_id = $tarea->primaryKey;
                    $nuevo->user_id = $integrante;
                    if ($integrante == user()->id)
                        $nuevo->aprobada = 1;
                    $nuevo->save(false);
                }
                //Se envia la notificacion a todos los integrantes
                $notificacion = new NotificacionNuevaTarea;
                $notificacion->entidad_id = $tarea->entidad_id;
                $notificacion->tipo_entidad = $tarea->tipo_entidad;
                $notificacion->integrantes = $tarea->integrantes;
                $notificacion->tarea_id = $tarea->primaryKey;
                $notificacion->enviarNotificacion();
                //agrego las caracteristicas
                if (isset($_POST['CategoriaActividadHasTarea'])) {
                    $caracteristicas = $_POST['CategoriaActividadHasTarea'];
                    foreach ($caracteristicas as $caracteristica) {
                        $nueva = new CategoriaActividadHasTarea;
                        $nueva->categoria_actividad_id = Yii::app()->input->stripClean($caracteristica['categoria_actividad_id']);
                        $nueva->titulo = Yii::app()->input->stripClean($caracteristica['titulo']);
                        $nueva->tarea_id = $tarea->primaryKey;
                        $nueva->save(false);
                    }
                }
                //se procesan los ficheros
                $archivos = CUploadedFile::getInstances($tarea, 'archivos');
                if (count($archivos) > 0) {
                    foreach ($archivos as $index => $archivo) {
                        $nuevo = new TareaArchivo;
                        $nuevo->tarea_id = $tarea->id;
                        $nuevo->archivo = time() . '_' . $archivo;
                        $dir = YiiBase::getPathOfAlias('webroot') . '/tareas/archivos/' . $tarea->id;
                        if (!is_dir($dir))
                            mkdir($dir);
                        $archivo->saveAs($dir . '/' . $nuevo->archivo);
                        $nuevo->save(false);
                    }
                }

                //si la actividad la creo 1 solo usuario entonces se actualiza su estado
                if ($notificar_superior)
                    $tarea->actualizarEstado();

                $this->redirect(array('ver', 'id' => $tarea->id));
            }
        }

        $this->render('crear', array(
            'tarea' => $tarea,
            'usuario' => Persona::model()->findByPk(user()->id)
        ));
    }

    /**
     * Permite editar una tarea por cualquier usuario que sea integrante de esta misma. Cuando una
     * tarea es editada entonces todos los integrantes deben volver a aprobarla nuevamente. Notar
     * que la logica de almacenamiento de modificaciones de tareas fue implementada mediante un
     * trigger en la base de datos.
     *
     * @param int $id ID de la tarea a editar
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEditar($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        $blame = $tarea->blameUltimoUsuario;
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        if (!$tarea->usuarioEsParticipante(Yii::app()->user->id))
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');

        if (isset($_POST['Tarea'])) {
            $tarea->attributes = $_POST['Tarea'];
            if ($tarea->validate()) {
                //Se envia la notificacion a todos los integrantes
                $integrantes = array();
                foreach ($tarea->participantes as $participante) {
                    array_push($integrantes, $participante->user_id);
                    //marco como aprobado al usuario que edito la tarea
                    if ($participante->user_id == user()->id) {
                        $participante->aprobada = 1;
                        $participante->save(false);
                    }
                }

                //si solo hay 1 integrante, entonces debo actualizar el estado de la tarea
                if (count($integrantes) == 1)
                    $tarea->aceptada = 1;
                $tarea->save(false);

                $notificacion = new NotificacionEdicionTarea;
                $notificacion->entidad_id = $tarea->entidad_id;
                $notificacion->tipo_entidad = $tarea->tipo_entidad;
                $notificacion->integrantes = $integrantes;
                $notificacion->tarea_id = $tarea->primaryKey;
                $notificacion->enviarNotificacion();
                //se eliminar las caracteristicas seleccionadas para eliminacion
                if (isset($_POST['EliminarCaracteristica'])) {
                    $eliminacion = Yii::app()->input->stripClean($_POST['EliminarCaracteristica']);
                    if (count($eliminacion > 0)) {
                        foreach ($eliminacion as $eliminar) {
                            //se eliminar cada caracteristica
                            CategoriaActividadHasTarea::model()->deleteByPk($eliminar['id']);
                        }
                    }
                }
                //agrego las caracteristicas
                if (isset($_POST['CategoriaActividadHasTarea'])) {
                    $caracteristicas = $_POST['CategoriaActividadHasTarea'];
                    foreach ($caracteristicas as $caracteristica) {
                        $nueva = new CategoriaActividadHasTarea;
                        $nueva->categoria_actividad_id = Yii::app()->input->stripClean($caracteristica['categoria_actividad_id']);
                        $nueva->titulo = Yii::app()->input->stripClean($caracteristica['titulo']);
                        $nueva->tarea_id = $tarea->primaryKey;
                        $nueva->save(false);
                    }
                }
                //se eliminar las caracteristicas seleccionadas para eliminacion
                if (isset($_POST['EliminarArchivo'])) {
                    $eliminacion = Yii::app()->input->stripClean($_POST['EliminarArchivo']);
                    if (count($eliminacion > 0)) {
                        foreach ($eliminacion as $eliminar) {
                            //se eliminar cada caracteristica
                            $archivo = TareaArchivo::model()->findByPk($eliminar['id']);
                            $archivo->delete();
                        }
                    }
                }
                //se procesan los ficheros
                $archivos = CUploadedFile::getInstances($tarea, 'archivos');
                if (count($archivos) > 0) {
                    foreach ($archivos as $index => $archivo) {
                        $nuevo = new TareaArchivo;
                        $nuevo->tarea_id = $tarea->id;
                        $nuevo->archivo = time() . '_' . $archivo;
                        $dir = YiiBase::getPathOfAlias('webroot') . '/tareas/archivos/' . $tarea->id;
                        if (!is_dir($dir))
                            mkdir($dir);
                        $archivo->saveAs($dir . '/' . $nuevo->archivo);
                        $nuevo->save(false);
                    }
                }
                $this->redirect(array('ver', 'id' => $tarea->id));
            }
        }

        $this->render('editar', array(
            'tarea' => $tarea,
            'blame' => $blame
        ));
    }

    /**
     * Accion que permite a los usuarios con los permisos correspondientes el ver una tarea
     * dentro de una entidad. Si la tarea es publica entonces cualquier usuario que pueda
     * ver los datos de la entidad puede ver dicha tarea. Si la tarea es privada entonces solo
     * la pueden ver los participantes de dicha tarea.
     *
     * @param int $id ID de la tarea
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVer($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        $acceso = 'ver_tarea_iniciativa';
        $params = array('iniciativa' => $tarea->entidad_id);
        if ($tarea->tipo_entidad == 'proyecto') {
            $acceso = 'ver_tarea_proyecto';
            $proyecto = Proyecto::model()->findByPk($tarea->entidad_id);
            $params = array('iniciativa' => $proyecto->iniciativa_id, 'proyecto' => $proyecto->id);
        }
        if (!Yii::app()->user->checkAccess($acceso, $params) && !$tarea->usuarioEsParticipante(Yii::app()->user->id))
            throw new CHttpException(403, 'No tienes los permisos suficientes para ver esta tarea.');

        //verifico que el usuario sea superior en esta tarea. Asi le muestro el mensaje de aprobacion
        $mostrar_aprobacion = true;
        $es_jefe_proyecto = false;
        if ($tarea->tipo_entidad == 'iniciativa') {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_iniciativa}} WHERE nombre_item="coordinador_iniciativa" AND iniciativa_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            if (Yii::app()->user->id != $row['user_id'])
                $mostrar_aprobacion = false;
        } else {
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                'SELECT user_id FROM {{lookup_proyecto}} WHERE nombre_item="jefe_proyecto" AND proyecto_id=' . $tarea->entidad_id);
            $row = $command->queryRow();
            if (Yii::app()->user->id != $row['user_id'])
                $mostrar_aprobacion = false;
            else
                $es_jefe_proyecto = true;
        }
        //compruebo la delegacion de la tarea
        $delegacion = $tarea->delegacion;
        if ($delegacion != null && $delegacion->iniciativa->coordinador->id == user()->id)
            $mostrar_aprobacion = true;

        $blame = $tarea->blameUltimoUsuario;
        $this->render('ver', array(
            'tarea' => $tarea,
            'blame' => $blame,
            'mostrar_aprobacion' => $mostrar_aprobacion,
            'es_jefe_proyecto' => $es_jefe_proyecto,
            'acceso' => $acceso
        ));
    }

    /**
     * Permite crear temporalmente una tarea. Cuando una tarea es guardada entonces el usuario que la
     * guardo puede volver a completarla mas tarde. Notar que una tarea guardada no se considera como
     * una tarea dentro del sistema sino que simplemente es un registro temporal sin significado alguno.
     * Para que pase a ser una tarea entonces el usuario debe completarla y enviarla a revision por sus
     * participantes.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo    este metodo esta fallando en almacenar los archivos de la tarea temporal. Debe ser arreglado.
     */
    public function actionGuardar()
    {
        $created = false;
        if (isset($_GET['id'])) {
            $id = Yii::app()->input->get('id');
            $tarea = TareaTmp::model()->findByPk($id);
            if ($tarea == null)
                throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
            //verifico que el usuario logeado sea integrante de la tarea
            if ($tarea->editor != Yii::app()->user->id)
                throw new CHttpException(500, 'No tienes los permisos para ver esta tarea.');
            $created = true;
        } else
            $tarea = new TareaTmp;

        if (isset($_POST['Tarea']) || isset($_POST['TareaTmp'])) {
            if ($created)
                $tarea->attributes = $_POST['TareaTmp'];
            else
                $tarea->attributes = $_POST['Tarea'];
            $tarea->editor = Yii::app()->user->id;
            if ($tarea->validate()) {
                /*$tarea->save(false);

                //agrego los integrantes
                $integrantes=$tarea->integrantes;
                if($created)
                {
                  $participantes=$tarea->participantes;
                }
                foreach($integrantes as $integrante)
                {
                  //verifico que no se agregue a los integrantes ya ingresados en una actividad
                  //que ya estaba en edicion
                  $flag=false;
                  if($created)
                  {
                    foreach($participantes as $participante)
                    {
                      if($participante->user_id==$integrante)
                        $flag=true;
                    }
                  }
                  if(!$flag)
                  {
                    $nuevo=new ParticipanteTareaTmp;
                    $nuevo->tarea_id=$tarea->primaryKey;
                    $nuevo->user_id=$integrante;
                    $nuevo->save(false);
                  }
                }

                //agrego las caracteristicas
                if(isset($_POST['CategoriaActividadHasTarea']))
                {
                  $caracteristicas=$_POST['CategoriaActividadHasTarea'];
                  foreach($caracteristicas as $caracteristica)
                  {
                    $nueva = new CategoriaActividadHasTareaTmp;
                    $nueva->categoria_actividad_id=Yii::app()->input->stripClean($caracteristica['categoria_actividad_id']);
                    $nueva->titulo=Yii::app()->input->stripClean($caracteristica['titulo']);
                    $nueva->tarea_id=$tarea->primaryKey;
                    $nueva->save(false);
                  }
                }*/
                //se procesan los ficheros
                $archivos = CUploadedFile::getInstances($tarea, 'archivos');
                die(var_dump($_POST['Tarea']['archivos']));
                if (count($archivos) > 0) {
                    foreach ($archivos as $index => $archivo) {
                        $nuevo = new TareaArchivoTmp;
                        $nuevo->tarea_id = $tarea->id;
                        $nuevo->archivo = time() . '_' . $archivo;
                        $dir = YiiBase::getPathOfAlias('webroot') . '/tareas_tmp/archivos/' . $tarea->id;
                        if (!is_dir($dir))
                            mkdir($dir);
                        $archivo->saveAs($dir . '/' . $nuevo->archivo);
                        $nuevo->save(false);
                    }
                }
                Yii::app()->user->setFlash('success', 'Se ha guardado correctamente la actividad.
            Puedes volver a trabajar en ella en el momento que estimes conveniente.');
                $this->redirect(array('/tareas/gestionActividades/index'));
            }
            Yii::app()->user->setFlash('error', 'Ha ocurrido un error al guardar la actividad. Por favor, int&eacute;ntalo nuevamente.');
            //$this->redirect(array('crear'));
        }

        $this->render('guardar', array(
            'tarea' => $tarea
        ));
    }

    /**
     * Permite que una tarea guardada (temporal) pase a transformarse en una tarea oficial dentro
     * del sistema. Para esto el usuario debe presionar en "Crear tarea" en el formulario correspondiente.
     *
     * @param int $id ID de la tarea temporal
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCrearEditada($id)
    {
        $tarea_tmp = TareaTmp::model()->findByPk($id);
        if ($tarea_tmp == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        if ($tarea_tmp->editor != Yii::app()->user->id)
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');

        $tarea = new Tarea('create');

        if (isset($_POST['TareaTmp'])) {
            $tarea->attributes = $_POST['TareaTmp'];
            $tarea_tmp->attributes = $_POST['TareaTmp'];
            $tarea_tmp->validate();
            if ($tarea->validate()) {
                $tarea->save(false);

                //agrego los integrantes
                $integrantes = $tarea->integrantes;
                foreach ($integrantes as $integrante) {
                    $nuevo = new ParticipanteTarea;
                    $nuevo->tarea_id = $tarea->primaryKey;
                    $nuevo->user_id = $integrante;
                    $nuevo->save(false);
                }
                //Se envia la notificacion a todos los integrantes
                $notificacion = new NotificacionNuevaTarea;
                $notificacion->entidad_id = $tarea->entidad_id;
                $notificacion->tipo_entidad = $tarea->tipo_entidad;
                $notificacion->integrantes = $tarea->integrantes;
                $notificacion->tarea_id = $tarea->primaryKey;
                $notificacion->enviarNotificacion();
                //agrego las caracteristicas
                $caracteristicas = $_POST['CategoriaActividadHasTarea'];
                foreach ($caracteristicas as $caracteristica) {
                    $nueva = new CategoriaActividadHasTarea;
                    $nueva->categoria_actividad_id = Yii::app()->input->stripClean($caracteristica['categoria_actividad_id']);
                    $nueva->titulo = Yii::app()->input->stripClean($caracteristica['titulo']);
                    $nueva->tarea_id = $tarea->primaryKey;
                    $nueva->save(false);
                }

                //debo eliminar todo lo referente a la tarea temporal
                //gracias al CASCADE solo se debe eliminar la tarea temporal
                $tarea_tmp->delete();

                $this->redirect(array('ver', 'id' => $tarea->id));
            }
        }

        $this->render('guardar', array(
            'tarea' => $tarea_tmp,
            'original' => $tarea
        ));
    }

}