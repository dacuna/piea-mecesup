<?php
/**
 * Controlador que contiene las acciones para gestionar las actividades por los usuarios del sistema.
 *
 * Contiene acciones que permiten a un usuario el ver sus actividades, aprobar y rechazar su participacion
 * en actividades, entre otras acciones.
 *
 * @package   modules.tareas.controllers
 * @version   1.0
 * @since     2012-08-03
 * @author    dacuna <diego.acuna@usm.cl>
 */
class GestionActividadesController extends Controller
{
    /**
     * Lista las actividades creadas por un usuario. Se listan las actividades pendientes de aprobacion
     * y las ya aprobadas.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionIndex()
    {
        //se obtienen las tareas del usuario que estan pendientes de aprobacion
        $connection = Yii::app()->db;
        $command = $connection->createCommand(
            'SELECT t.id AS id,t.entidad_id AS entidad_id,t.tipo_entidad AS tipo_entidad,
            t.titulo AS titulo,t.fecha_inicio AS fecha_inicio,t.fecha_termino AS fecha_termino
     FROM tbl_tarea t
     INNER JOIN tbl_participante_tarea p ON p.tarea_id=t.id
     WHERE p.user_id=' . Yii::app()->user->id . '
     AND t.aceptada=0');
        $pendientes = $command->query();

        //se obtienen las tareas que ya han sido aprobadas
        $command = $connection->createCommand(
            'SELECT t.id AS id,t.entidad_id AS entidad_id,t.tipo_entidad AS tipo_entidad,
            t.titulo AS titulo,t.fecha_inicio AS fecha_inicio,t.fecha_termino AS fecha_termino
     FROM tbl_tarea t
     INNER JOIN tbl_participante_tarea p ON p.tarea_id=t.id
     WHERE p.user_id=' . Yii::app()->user->id . '
     AND t.aceptada=1');
        $aceptadas = $command->query();

        //se obtienen las pendientes de edicion
        $por_editar = TareaTmp::model()->findAll('editor=:eid', array(':eid' => Yii::app()->user->id));

        //accion de busqueda en actividades aprobadas
        $model = new Tarea('search');
        $model->unsetAttributes();
        $model->aprobada = 1;

        if (isset($_GET['Tarea']))
            $model->attributes = $_GET['Tarea'];

        $this->render('index', array(
            'pendientes' => $pendientes,
            'aceptadas' => $aceptadas,
            'por_editar' => $por_editar,
            'model' => $model,
            'resultados' => $model->search()
        ));
    }

    /**
     * Permite a un usuario el ver una actividad desde una notificacion que le haya sido
     * enviada. Esto permite marcar el mensaje como leido y contestado una vez que el usuario
     * haya presionado el enlace de ver actividad.
     *
     * @param int $id ID del mensaje
     * @param int $tarea ID de la tarea
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionVerDesdeBuzon($id, $tarea)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->destinatario != Yii::app()->user->id)
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');
        //obtengo la tarea
        $tarea = Tarea::model()->findByPk($tarea);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        if (!$tarea->usuarioEsParticipante(Yii::app()->user->id))
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');
        //actualizo el mensaje del estado
        Mensaje::model()->updateByPk($mensaje->id, array('leido' => 1, 'contestado' => 1));
        //se muestra la tarea entonces en formato de vista

        $blame = $tarea->blameUltimoUsuario;
        $this->render('ver', array(
            'tarea' => $tarea,
            'blame' => $blame
        ));
    }

    /**
     * Permite a un usuario el ver el historial de revisiones de una tarea en la cual es participante o
     * es encargado de la entidad en la cual se creo la tarea.
     *
     * @param int $id ID de la tarea.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionHistorialRevisiones($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        $acceso = 'ver_tarea_iniciativa';
        $params = array('iniciativa' => $tarea->entidad_id);
        if ($tarea->tipo_entidad == 'proyecto') {
            $acceso = 'ver_tarea_proyecto';
            $proyecto = Proyecto::model()->findByPk($tarea->entidad_id);
            $params = array('iniciativa' => $proyecto->iniciativa_id, 'proyecto' => $proyecto->id);
        }
        if (!$tarea->usuarioEsParticipante(Yii::app()->user->id) && !Yii::app()->user->checkAccess($acceso, $params))
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');

        //busco las revisiones de la tarea actual
        $connection = Yii::app()->db;
        $command = $connection->createCommand(
            'SELECT _revision,_revhistory_user_id,_revhistory_timestamp FROM _revhistory_tbl_tarea WHERE id=' . $tarea->id);
        $dataReader = $command->query();
        $revisiones = array();
        $id = 1;
        foreach ($dataReader as $row) {
            $revision = array();
            $revision['index'] = $id++;
            $revision['revision'] = $row['_revision'];
            $revision['usuario'] = Persona::model()->findByPk($row['_revhistory_user_id']);
            $revision['fecha'] = $row['_revhistory_timestamp'];
            array_push($revisiones, $revision);
        }

        $blame = $tarea->blameUltimoUsuario;

        $this->render('historial_revisiones', array(
            'tarea' => $tarea,
            'revisiones' => $revisiones,
            'blame' => $blame
        ));
    }

    /**
     * Permite a un usuario el ver una revision en particular de una tarea en la cual es participante.
     *
     * @param int $id ID de la tarea.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo al parecer se debe verificar que el usuario tenga los permisos correspondientes para ver la revision
     */
    public function actionVerRevision($id)
    {
        $revision = RevisionTarea::model()->findByPk($id);
        if ($revision == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        $this->render('ver_revision', array(
            'tarea' => $revision,
        ));
    }

    /**
     * Permite a un usuario el aprobar una actividad en la cual fue mencionado como participante de esta.
     *
     * @param int $id ID de la tarea.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAceptarActividad($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        if (!$tarea->usuarioEsParticipante(Yii::app()->user->id))
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');
        //obteno la participacion del presente usuario
        $participante = ParticipanteTarea::model()->findByPk(array(
            'tarea_id' => $tarea->id, 'user_id' => Yii::app()->user->id));
        //se actualiza su estado
        if ($participante->aceptarActividad()) {
            //debo tambien actualizar el estado de la tarea
            $tarea->actualizarEstado();
            Yii::app()->user->setFlash('success', 'Se ha aceptado correctamente la actividad.');
            $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
        } else {
            Yii::app()->user->setFlash('error', 'Ha ocurrido un error al aceptar la actividad. Por favor, int&eacute;ntalo nuevamente.');
            $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
        }
    }

    /**
     * Permite a un usuario el desvincularse de una actividad en la cual fue mencionado como participante. Una
     * vez desvinculado el usuario no puede volver a ver nada sobre dicha tarea (a menos que vuelva a ser
     * agregado como participante por un tercero).
     *
     * @param int $id ID de la tarea.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionDesvincularActividad($id)
    {
        $tarea = Tarea::model()->findByPk($id);
        if ($tarea == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario logeado sea integrante de la tarea
        if (!$tarea->usuarioEsParticipante(Yii::app()->user->id))
            throw new CHttpException(403, 'No tienes los permisos para ver esta tarea.');
        //se elimina el usuario como integrante
        $participante = ParticipanteTarea::model()->findByPk(array(
            'tarea_id' => $tarea->id, 'user_id' => Yii::app()->user->id));
        if ($participante->delete()) {
            Yii::app()->user->setFlash('success', 'Has sido correctamente desvinculado de la actividad anterior.');
            $this->redirect(array('/tareas/gestionActividades/index', 'id' => $tarea->id));
        }
        Yii::app()->user->setFlash('error', 'Ha ocurrido un error al desvincularte de la actividad. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/tareas/crud/ver', 'id' => $tarea->id));
    }

    /**
     * Permite eliminar una caracteristica de una tarea. Solo el editar de la tarea puede eliminar una
     * caracteristica de esta.
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionEliminarCaracteristica()
    {
        $id = Yii::app()->input->post('id');
        if (!is_numeric($id))
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        $caracteristica = CategoriaActividadHasTareaTmp::model()->findByPK($id);
        if ($caracteristica == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        //verifico que el usuario tiene permisos para eliminar esta caracteristica
        $tarea = $caracteristica->tarea;
        if ($tarea->editor != Yii::app()->user->id)
            throw new CHttpException(403, 'No tienes los permisos para ver eliminar esta caracteristica.');
        if ($caracteristica->delete())
            echo 'Se ha eliminado correctamente la caracter&iacute;stica seleccionada';
        else
            echo 'Ha ocurrido un error al realizar la operacion solicitada. Por favor, intentalo nuevamente.';
        Yii::app()->end();
    }

    /**
     * Permite que un usuario envie tareas para revision. De esta forma puede ser evaluado con FAE dentro de
     * la universidad.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo PIEA aun no define completamente como se realizara esta actividad. Esto es solo un bosquejo.
     */
    public function actionEnviarParaRevision()
    {
        if (isset($_POST['selectedItems']) || isset($_POST['Tareas'])) {
            $tareas = array();
            if (isset($_POST['selectedItems'])) {
                $data = app()->input->stripClean($_POST['selectedItems']);
                foreach ($data as $tarea) {
                    $tareas[] = Tarea::load($tarea);
                }
            }

            if (isset($_POST['Tareas'])) {
                $data = app()->input->stripClean($_POST['Tareas']);
                $usuario = Persona::load($data['revisor_final']);
                user()->setFlash("success", "Se han enviado correctamente las tareas para revisi&oacute;n.");
                $this->redirect(array('/tareas/gestionActividades'));
            }

            $this->render('enviar_para_revision', array(
                'tareas' => $tareas
            ));
        } else
            throw new CHttpException(404, "La p&aacute;gina solicitada no existe.");
    }

}