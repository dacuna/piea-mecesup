<?php

/**
 * Modelo para la tabla "{{categoria_actividad_has_tarea}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{categoria_actividad_has_tarea}}':
 * @property integer $categoria_actividad_id
 * @property integer $tarea_id
 */
class CategoriaActividadHasTarea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoriaActividadHasTarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{categoria_actividad_has_tarea}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('categoria_actividad_id, tarea_id', 'required'),
			array('categoria_actividad_id, tarea_id', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
      'tarea' => array(self::BELONGS_TO, 'Tarea', 'tarea_id'),
		);
	}
  
}