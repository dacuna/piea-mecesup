<?php

/**
 * Modelo para la tabla "{{categoria_actividad}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{categoria_actividad}}':
 * @property integer $id
 * @property integer $id_padre
 * @property string $nombre
 * @property integer $nivel
 *
 * Las siguientes son las relaciones disponibles:
 * @property Tarea[] $tblTareas
 */
class CategoriaActividad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoriaActividad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{categoria_actividad}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'tblTareas' => array(self::MANY_MANY, 'Tarea', '{{categoria_actividad_has_tarea}}(categoria_actividad_id, tarea_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_padre' => 'Id Padre',
			'nombre' => 'Nombre',
			'nivel' => 'Nivel',
		);
	}

}