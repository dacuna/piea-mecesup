<?php

/**
 * Modelo para la tabla "{{participante_tarea_tmp}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{participante_tarea_tmp}}':
 * @property integer $tarea_id
 * @property integer $user_id
 * @property integer $aprobada
 *
 * Las siguientes son las relaciones disponibles:
 * @property TareaTmp $tarea
 * @property User $user
 */
class ParticipanteTareaTmp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParticipanteTareaTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{participante_tarea_tmp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarea_id, user_id', 'required'),
			array('tarea_id, user_id, aprobada', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tarea_id, user_id, aprobada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarea' => array(self::BELONGS_TO, 'TareaTmp', 'tarea_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
  
  public function getUsuario()
  {
    return Persona::model()->findByPk($this->user_id);
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tarea_id' => 'Tarea',
			'user_id' => 'User',
			'aprobada' => 'Aprobada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tarea_id',$this->tarea_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('aprobada',$this->aprobada);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}