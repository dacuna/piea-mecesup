<?php

Yii::import('application.modules.iniciativas.models.IniciativaModel');
Yii::import('application.modules.iniciativas.models.LookupIniciativa');

/**
 * Modelo para la tabla "{{delegacion_tarea}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{delegacion_tarea}}':
 * @property integer $id_iniciativa
 * @property integer $id_tarea
 */
class DelegacionTarea extends CActiveRecord
{
	public $proyecto;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DelegacionTarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{delegacion_tarea}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarea' => array(self::BELONGS_TO, 'Tarea', 'id_tarea'),
			'iniciativa' => array(self::BELONGS_TO, 'IniciativaModel', 'id_iniciativa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_iniciativa' => 'Iniciativa',
			'id_tarea' => 'Tarea',
		);
	}

	public function afterSave()
	{
		parent::afterSave();
		//notificar al coordinador de la iniciativa
		$coordinador=$this->iniciativa->coordinador;
		if($coordinador!=null)
		{
			$mailman=Yii::app()->mailman;
			$mailman->crearMensaje('Solicitud de aprobaci&oacute;n de actividad en proyecto '.$this->proyecto->nombre,'temp');
			$mailman->setPublisher(Yii::app()->user->id);
			$mailman->setSuscriber($coordinador->id);
			$mailman->setRelation($this->iniciativa->id,'iniciativa');
			$id_mensaje=$mailman->enviarMensaje();
			$html = Yii::app()->controller->renderPartial('/notificaciones/_mensaje_delegacion',
      array('iniciativa'=>$this->iniciativa,'proyecto'=>$this->proyecto,'tarea_id'=>$this->id_tarea),true);
      $mailman->actualizarMensaje($id_mensaje,$html);
		}
	}

}