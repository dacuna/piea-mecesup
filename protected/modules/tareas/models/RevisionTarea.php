<?php

/**
 * Modelo para la tabla "_revision_tbl_tarea".
 *
 * Los siguientes atributos estan disponibles desde la tabla '_revision_tbl_tarea':
 * @property integer $id
 * @property integer $entidad_id
 * @property string $tipo_entidad
 * @property integer $categoria_id
 * @property string $titulo
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $duracion
 * @property integer $aceptada
 * @property string $descripcion
 * @property string $fecha_ingreso
 * @property string $fecha_actualizacion
 * @property string $_revision
 * @property string $_revision_previous
 * @property string $_revision_action
 * @property string $_revision_user_id
 * @property string $_revision_timestamp
 * @property string $_revision_comment
 */
class RevisionTarea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RevisionTarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '_revision_tbl_tarea';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entidad_id' => 'Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'categoria_id' => 'Categoria',
			'titulo' => 'Titulo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_termino' => 'Fecha Termino',
			'duracion' => 'Duracion',
			'aceptada' => 'Aceptada',
			'descripcion' => 'Descripcion',
			'fecha_ingreso' => 'Fecha Ingreso',
			'fecha_actualizacion' => 'Fecha Actualizacion',
			'_revision' => 'Revision',
			'_revision_previous' => 'Revision Previous',
			'_revision_action' => 'Revision Action',
			'_revision_user_id' => 'Revision User',
			'_revision_timestamp' => 'Revision Timestamp',
			'_revision_comment' => 'Revision Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entidad_id',$this->entidad_id);
		$criteria->compare('tipo_entidad',$this->tipo_entidad,true);
		$criteria->compare('categoria_id',$this->categoria_id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_termino',$this->fecha_termino,true);
		$criteria->compare('duracion',$this->duracion,true);
		$criteria->compare('aceptada',$this->aceptada);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
		$criteria->compare('fecha_actualizacion',$this->fecha_actualizacion,true);
		$criteria->compare('_revision',$this->_revision,true);
		$criteria->compare('_revision_previous',$this->_revision_previous,true);
		$criteria->compare('_revision_action',$this->_revision_action,true);
		$criteria->compare('_revision_user_id',$this->_revision_user_id,true);
		$criteria->compare('_revision_timestamp',$this->_revision_timestamp,true);
		$criteria->compare('_revision_comment',$this->_revision_comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  public function getCategorias()
  {
    return array(
      array('id'=>1,'nombre'=>'Categoria 1'),
      array('id'=>2,'nombre'=>'Categoria 2'),
      array('id'=>3,'nombre'=>'Categoria 3')
    );
  }
  
  public function getCategoria()
  {
    $categorias = array(
      array('id'=>1,'nombre'=>'Categoria 1'),
      array('id'=>2,'nombre'=>'Categoria 2'),
      array('id'=>3,'nombre'=>'Categoria 3')
    );
    return $categorias[$this->categoria_id-1];
  }
  
  public function getParticipantes()
  {
    return ParticipanteTarea::model()->findAll('tarea_id=:tid',array(':tid'=>$this->id));
  }
  
  public function getUsuario()
  {
    return $usuario=Persona::model()->findByPk($this->_revision_user_id);
  }
  
  public function getCaracteristicas()
  {
    $connection=Yii::app()->db; 
    $command=$connection->createCommand(
    'SELECT r.titulo AS titulo,c.nombre AS tipo FROM _revision_tbl_categoria_actividad_has_tarea r
     INNER JOIN tbl_categoria_actividad c ON r.categoria_actividad_id=c.id
     WHERE r._revision='.$this->_revision.' AND r.tarea_id='.$this->id);
    $dataReader=$command->query();
    $caracteristicas=array();
    foreach($dataReader as $row)
    {
      $caracteristica=array();
      $caracteristica['titulo']=$row['titulo'];
      $caracteristica['tipo']=$row['tipo'];
      array_push($caracteristicas,$caracteristica);
    }
    return $caracteristicas;
  }
  
  public function getFicheros()
  {
    $connection=Yii::app()->db; 
    $command=$connection->createCommand(
    'SELECT id,tarea_id,archivo,descripcion FROM _revision_tbl_tarea_archivo 
     WHERE _revision='.$this->_revision.' AND tarea_id='.$this->id);
    $dataReader=$command->query();
    $ficheros=array();
    foreach($dataReader as $row)
    {
      $tmp=new TareaArchivo;
      $tmp->id=$row['id'];
      $tmp->tarea_id=$row['tarea_id'];
      $tmp->archivo=$row['archivo'];
      $tmp->descripcion=$row['descripcion'];
      array_push($ficheros,$tmp);
    }
    return $ficheros;
  }
}