<?php

/**
 * Modelo para la tabla "{{participante_tarea}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{participante_tarea}}':
 * @property integer $tarea_id
 * @property integer $user_id
 *
 * Las siguientes son las relaciones disponibles:
 * @property Tarea $tarea
 * @property User $user
 */
class ParticipanteTarea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParticipanteTarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{participante_tarea}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarea_id, user_id', 'required'),
			array('tarea_id, user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tarea_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'tarea' => array(self::BELONGS_TO, 'Tarea', 'tarea_id'),
		);
	}

  public function getUsuario()
  {
    return Persona::model()->findByPk($this->user_id);
  }
  
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tarea_id' => 'Tarea',
			'user_id' => 'User',
		);
	}

	public function primaryKey()
  {
      return array('tarea_id', 'user_id');
  }
  
  /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tarea_id',$this->tarea_id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  public function aceptarActividad()
  {
    return ParticipanteTarea::model()->updateByPk(array('tarea_id'=>$this->tarea_id,'user_id'=>$this->user_id),array('aprobada'=>1));
  }
}