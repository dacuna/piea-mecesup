<?php

/**
 * Modelo para la tabla "{{tarea_tmp}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{tarea_tmp}}':
 * @property integer $id
 * @property integer $entidad_id
 * @property string $tipo_entidad
 * @property integer $categoria_id
 * @property string $titulo
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $duracion
 * @property integer $aceptada
 * @property string $descripcion
 * @property string $fecha_ingreso
 * @property string $fecha_actualizacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property CategoriaActividadHasTareaTmp[] $categoriaActividadHasTareaTmps
 * @property ParticipanteTareaTmp[] $participanteTareaTmps
 */
class TareaTmp extends CActiveRecord
{
  public $integrantes;
  
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TareaTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{tarea_tmp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
      array('entidad_id','numerical','integerOnly'=>true),
			array('titulo, duracion, descripcion', 'safe'),
			array('categoria_id', 'numerical', 'integerOnly'=>true),
			array('tipo_entidad','in','range'=>array('iniciativa','proyecto')),
      array('fecha_inicio', 'date', 'format'=>'d-M-yyyy'),
      array('fecha_termino', 'date', 'format'=>'d-M-yyyy'),
			array('titulo', 'length', 'max'=>45),
			array('duracion', 'length', 'max'=>4),
      array('integrantes','integrantesValidator')
		);
	}
	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'caracteristicas' => array(self::HAS_MANY, 'CategoriaActividadHasTareaTmp', 'tarea_id'),
			'participanteTareaTmps' => array(self::HAS_MANY, 'ParticipanteTareaTmp', 'tarea_id'),
		);
	}
  
  public function behaviors(){
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'createAttribute' => 'fecha_ingreso',
        'updateAttribute' => 'fecha_actualizacion',
      )
    );
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entidad_id' => 'Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'categoria_id' => 'Categoria',
			'titulo' => 'Titulo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_termino' => 'Fecha Termino',
			'duracion' => 'Duracion',
			'aceptada' => 'Aceptada',
			'descripcion' => 'Descripcion',
			'fecha_ingreso' => 'Fecha Ingreso',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}
  
  public function getCategorias()
  {
    return array(
      array('id'=>1,'nombre'=>'Categoria 1'),
      array('id'=>2,'nombre'=>'Categoria 2'),
      array('id'=>3,'nombre'=>'Categoria 3')
    );
  }
  
  public function getParticipantes()
  {
    return ParticipanteTareaTmp::model()->findAll('tarea_id=:tid',array(':tid'=>$this->id));
  }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entidad_id',$this->entidad_id);
		$criteria->compare('tipo_entidad',$this->tipo_entidad,true);
		$criteria->compare('categoria_id',$this->categoria_id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_termino',$this->fecha_termino,true);
		$criteria->compare('duracion',$this->duracion,true);
		$criteria->compare('aceptada',$this->aceptada);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
		$criteria->compare('fecha_actualizacion',$this->fecha_actualizacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  public function integrantesValidator($attribute,$params)
  {
    //verifico que al menos este el usuario logeado como integrante
    $integrantes = $this->integrantes;
    $have = false;
    
    if(count($integrantes)==0)
    {
      $this->addError('integrantes', 'Debes ingresar al menos 1 integrante para la actividad actual');
      return;
    }
    
    foreach($integrantes as $integrante)
    {
      if(Yii::app()->user->id == $integrante)
        $have=true;
    }
    
    if(!$have)
      $this->addError('integrantes', 'Debes ingresar al menos 1 integrante para la actividad actual');
  }
  
  public function beforeSave()
	{
		if(parent::beforeSave())
		{
      if(isset($this->fecha_inicio) && !empty($this->fecha_inicio))
        $this->fecha_inicio=date('Y-m-d', strtotime($this->fecha_inicio));
      if(isset($this->fecha_termino) && !empty($this->fecha_termino))
        $this->fecha_termino=date('Y-m-d', strtotime($this->fecha_termino));
			return true;
		}
		else
			return false;
	}
  
  public function afterFind()
  {
    $this->fecha_inicio =  Yii::app()->dateFormatter->format("dd-MM-yyyy",$this->fecha_inicio);
    $this->fecha_termino =  Yii::app()->dateFormatter->format("dd-MM-yyyy",$this->fecha_termino);
    return parent::afterFind();
  }
}