<?php

/**
 * Modelo para la tabla "{{categoria_actividad_has_tarea_tmp}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{categoria_actividad_has_tarea_tmp}}':
 * @property integer $id
 * @property integer $categoria_actividad_id
 * @property integer $tarea_id
 * @property string $titulo
 *
 * Las siguientes son las relaciones disponibles:
 * @property CategoriaActividad $categoriaActividad
 * @property TareaTmp $tarea
 */
class CategoriaActividadHasTareaTmp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoriaActividadHasTareaTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{categoria_actividad_has_tarea_tmp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categoria_actividad_id, tarea_id', 'required'),
			array('categoria_actividad_id, tarea_id', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categoria_actividad_id, tarea_id, titulo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoriaActividad' => array(self::BELONGS_TO, 'CategoriaActividad', 'categoria_actividad_id'),
			'tarea' => array(self::BELONGS_TO, 'TareaTmp', 'tarea_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'categoria_actividad_id' => 'Categoria Actividad',
			'tarea_id' => 'Tarea',
			'titulo' => 'Titulo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('categoria_actividad_id',$this->categoria_actividad_id);
		$criteria->compare('tarea_id',$this->tarea_id);
		$criteria->compare('titulo',$this->titulo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}