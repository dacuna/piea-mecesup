<?php

/**
 * Modelo para la tabla "{{tarea_archivo_tmp}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{tarea_archivo_tmp}}':
 * @property integer $id
 * @property integer $tarea_id
 * @property string $archivo
 * @property string $descripcion
 *
 * Las siguientes son las relaciones disponibles:
 * @property TareaTmp $tarea
 */
class TareaArchivoTmp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TareaArchivoTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{tarea_archivo_tmp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarea_id, archivo', 'required'),
			array('tarea_id', 'numerical', 'integerOnly'=>true),
			array('archivo', 'length', 'max'=>100),
			array('descripcion', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tarea_id, archivo, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarea' => array(self::BELONGS_TO, 'TareaTmp', 'tarea_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tarea_id' => 'Tarea',
			'archivo' => 'Archivo',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tarea_id',$this->tarea_id);
		$criteria->compare('archivo',$this->archivo,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}