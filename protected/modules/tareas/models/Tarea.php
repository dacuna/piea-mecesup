<?php

/**
 * Modelo para la tabla "{{tarea}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{tarea}}':
 * @property integer $id
 * @property integer $entidad_id
 * @property string $tipo_entidad
 * @property string $titulo
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $duracion
 * @property string $descripcion
 *
 * Las siguientes son las relaciones disponibles:
 * @property ParticipanteTarea[] $participanteTareas
 */
class Tarea extends CActiveRecord
{
	public $integrantes;
  public $archivos;
  public $caracteristicas_al_crear;
  
  /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{tarea}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
      array('entidad_id','required','on'=>'create'),
      array('entidad_id','numerical','integerOnly'=>true,'on'=>'create'),
	  array('titulo, categoria_id,duracion, descripcion, clasificacion,fecha_inicio,fecha_termino', 'required'),
      array('archivos','file','types'=>'pdf,jpg,gif,png,docx,xlsx','maxFiles'=>10,'maxSize'=>1024*1024*3,'allowEmpty'=>true),
	  array('categoria_id', 'numerical', 'integerOnly'=>true),
      array('tipo_entidad','in','range'=>array('iniciativa','proyecto'),'allowEmpty'=>false,'on'=>'create'),
	  array('publica','in','range'=>array('0','1'),'allowEmpty'=>false),
      array('fecha_inicio', 'date', 'format'=>'d-M-yyyy','allowEmpty'=>false),
      array('fecha_termino', 'date', 'format'=>'d-M-yyyy','allowEmpty'=>false),
	  array('titulo', 'length', 'max'=>45),
      array('duracion', 'length', 'max'=>5),
      array('integrantes','integrantesValidator','on'=>'create'),
      array('descripcion,caracteristicas_al_crear', 'safe'),
      //aqui se esta validando que el usuario que crea la tarea pertenezca a la entidad que selecciono
      array('entidad_id','pertenenciaEntidadValidator','on'=>'create'),
      //se valida que la fecha inicial sea antes que la fecha final
      array('fecha_inicio,fecha_termino','application.extensions.DatesValidator'),

			array('id, entidad_id, tipo_entidad, titulo, fecha_inicio, fecha_termino, duracion, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
      'categoria_caracteristicas' => array(self::MANY_MANY, 'CategoriaActividad', '{{categoria_actividad_has_tarea}}(tarea_id, categoria_actividad_id)'),
      'caracteristicas' => array(self::HAS_MANY, 'CategoriaActividadHasTarea', 'tarea_id'),
      'ficheros' => array(self::HAS_MANY, 'TareaArchivo', 'tarea_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entidad_id' => 'Iniciativa o Proyecto',
			'tipo_entidad' => 'Tipo Entidad',
      'categoria_id' => 'Tipo de Actividad',
			'titulo' => 'Nombre de la actividad',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_termino' => 'Fecha Termino',
			'duracion' => 'Duracion (hrs.)',
			'descripcion' => 'Descripcion',
      'publica'=>'Visibilidad'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('entidad_id',$this->entidad_id);
		$criteria->compare('tipo_entidad',$this->tipo_entidad,true);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_termino',$this->fecha_termino,true);
		$criteria->compare('duracion',$this->duracion,true);
		$criteria->compare('descripcion',$this->descripcion,true);
    
    $criteria->compare('t.aprobada',$this->aprobada);
    
    //debe tener un join con el participante actual
    $criteria->join='INNER JOIN {{participante_tarea}} p ON p.tarea_id=t.id';
    $criteria->compare('p.user_id',Yii::app()->user->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function generalSearch()
  {
    $criteria=new CDbCriteria;

    $criteria->compare('entidad_id',$this->entidad_id);
    $criteria->compare('tipo_entidad',$this->tipo_entidad,true);
    $criteria->compare('titulo',$this->titulo,true);
    $criteria->compare('fecha_inicio',$this->fecha_inicio,true);
    $criteria->compare('fecha_termino',$this->fecha_termino,true);
    $criteria->compare('duracion',$this->duracion,true);
    $criteria->compare('descripcion',$this->descripcion,true);
    
    $criteria->compare('t.aprobada',$this->aprobada);
    $criteria->compare('t.publica',$this->publica);

    return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
    ));
  }
  
  public function getCategorias()
  {
    return array(
      array('id'=>1,'nombre'=>'Actividad Administrativa'),
      array('id'=>2,'nombre'=>'Actividad T&eacute;cnica'),
    );
  }
  
  public function getCategoria()
  {
    $categorias = array(
      array('id'=>1,'nombre'=>'Actividad Administrativa'),
      array('id'=>2,'nombre'=>'Actividad T&eacute;cnica'),
    );
    return $categorias[$this->categoria_id-1];
  }
  
  public function getDuracionesDisponibles()
  {
    $duraciones=array();
    for($i=0;$i<30;$i++)
    {
      if(($i+1)*15<60)
        array_push($duraciones,array('id'=>15*($i+1),'nombre'=>15*($i+1).' min.'));
      else
        array_push($duraciones,array('id'=>15*($i+1),'nombre'=>((15*($i+1))/60).' hrs.'));
    }
    return $duraciones;
  }
  
  public function getParticipantes()
  {
    return ParticipanteTarea::model()->findAll('tarea_id=:tid',array(':tid'=>$this->id));
  }
  

  public function behaviors(){
    return array(
      'CTimestampBehavior' => array(
        'class' => 'zii.behaviors.CTimestampBehavior',
        'createAttribute' => 'fecha_ingreso',
        'updateAttribute' => 'fecha_actualizacion',
      ),
      'CAdvancedArBehavior' => array(
        'class' => 'application.extensions.CAdvancedArBehavior')
    );
  }
  
  public function beforeSave()
	{
		if(parent::beforeSave())
		{
      if(isset($this->fecha_inicio) && !empty($this->fecha_inicio))
        $this->fecha_inicio=date('Y-m-d', strtotime($this->fecha_inicio));
      if(isset($this->fecha_termino) && !empty($this->fecha_termino))
        $this->fecha_termino=date('Y-m-d', strtotime($this->fecha_termino));
      //debo settear la variable de usuario a mysql, se setea la id del usuario como el usuario logeado
      //o sea este usuario esta haciendo los cambios en la tabla de tareas
      $connection=Yii::app()->db; 
      $command=$connection->createCommand('SET @auth_uid='.Yii::app()->user->id);
      $command->execute();
      
      //cuando una tarea es creada o actualizada, todos los participantes deben tener su aprobacion en 0 (no aprobada)
      if(!$this->isNewRecord && $this->aceptada!=1)
      {
        foreach($this->participantes as $participante)
        {
          $participante->aprobada=0;
          $participante->save(false);
        }
      }
      
			return true;
		}
		else
			return false;
	}
  
  public function afterFind()
  {
    $this->fecha_inicio =  Yii::app()->dateFormatter->format("dd-MM-yyyy",$this->fecha_inicio);
    $this->fecha_termino =  Yii::app()->dateFormatter->format("dd-MM-yyyy",$this->fecha_termino);
    return parent::afterFind();
  }
  
  public function integrantesValidator($attribute,$params)
  {
    //verifico que al menos este el usuario logeado como integrante
    $integrantes = $this->integrantes;
    $have = false;
    
    if(count($integrantes)==0)
    {
      $this->addError('integrantes', 'Debes ingresar al menos 1 integrante para la actividad actual');
      return;
    }
    
    foreach($integrantes as $integrante)
    {
      if(Yii::app()->user->id == $integrante)
        $have=true;
    }
    
    if(!$have)
      $this->addError('integrantes', 'Debes ingresar al menos 1 integrante para la actividad actual');
  }

   //valida que el usuario que crea la actividad pertenezca a la entidad que selecciono para creacion
   public function pertenenciaEntidadValidator($attribute,$params)
   {
       if(!isset($this->entidad_id) || empty($this->entidad_id))
           return;
       if($this->tipo_entidad=='iniciativa')
       {
           $pertenece=IntegranteIniciativa::model()->find('iniciativa_id=:iid and user_id=:uid',array(
               ':iid'=>$this->entidad_id,
               ':uid'=>user()->id
           ));
           if($pertenece==null)
               $this->addError('entidad_id','No perteneces a la iniciativa seleccionada. Debes elegir una iniciativa de la cual seas integrante.');
       }
       elseif($this->tipo_entidad=='proyecto')
       {
           $pertenece=IntegranteProyecto::model()->find('proyecto_id=:pid and user_id=:uid',array(
               ':pid'=>$this->entidad_id,
               ':uid'=>user()->id
           ));
           if($pertenece==null)
               $this->addError('entidad_id','No perteneces al proyecto seleccionado. Debes elegir un proyecto del cual seas integrante.');
       }
       else
           $this->addError('tipo_entidad','Debes seleccionar ya sea una iniciativa o proyecto');
   }
  
  public function usuarioEsParticipante($id_usuario)
  {
    $participantes=$this->participantes;
    foreach($participantes as $participante)
      if($participante->user_id == $id_usuario)
        return true;
    return false;
  }
  
  public function getBlameUltimoUsuario()
  {
    $revision=$this->_revision;
    //busco la id del usuario que hizo la ultima revision
    $connection=Yii::app()->db; 
    $command=$connection->createCommand(
    'SELECT _revhistory_user_id,_revhistory_timestamp FROM _revhistory_tbl_tarea WHERE _revision='.$revision.' AND id='.$this->id);
    $dataReader=$command->query();
    $result=array();
    foreach($dataReader as $row)
    {
      $result['usuario']=Persona::model()->findByPk($row['_revhistory_user_id']);
      $result['fecha']=$row['_revhistory_timestamp'];
    }
    return $result;
  }
  
  //actualiza el estado de una tarea (1 si esta aprobada, 0 si no esta aprobada)
  public function actualizarEstado()
  {
    foreach($this->participantes as $participante)
    {
      if($participante->aprobada==0)
        return 0;
    }
    //en este punto todos los integrantes aceptaron la actividad. Se actualiza el estado
    $this->aceptada=1;
    $this->save(false);
    //ahora se debe notificar al encargado (jefe de proyecto o coordinador de iniciativa) que
    //debe aprobar la actividad ingresada
    $notificar=new NotificacionAceptacionTarea;
    $notificar->redactar($this->id,$this->tipo_entidad,$this->entidad_id);
    $notificar->enviarNotificacion();
    return 1;
  }
  
  //retorna el estado de aprobacion de un usuario con id $id
  public function usuarioEstadoAprobacion($id)
  {
    return ParticipanteTarea::model()->findByPk(array('tarea_id'=>$this->id,'user_id'=>$id))->aprobada;
  }
  
  //retorna el nombre de la entidad asociada a esta tarea
  public function getNombre_entidad()
  {
    if($this->tipo_entidad=='iniciativa')
      return Iniciativa::model()->findByPk($this->entidad_id)->nombre_abreviado;
    return Proyecto::model()->findByPk($this->entidad_id)->nombre;
  }

  public function getComentarioRechazo()
  {
    $revision=$this->_revision;
    //busco la id del usuario que hizo la ultima revision
    $connection=Yii::app()->db; 
    $command=$connection->createCommand(
    'SELECT _revision_comment AS comentario FROM _revision_tbl_tarea WHERE _revision='.$revision.' AND id='.$this->id);
    $dataReader=$command->queryRow();
    return $dataReader; 
  }

  public function getDelegacion()
  {
    $delegacion=DelegacionTarea::model()->find('id_tarea=:tid',array(':tid'=>$this->id));
    return $delegacion;
  }

  public function getClasificacionesTecnicas()
  {
    return array(
      array('value'=>'An&aacute;lisis de marco te&oacute;rico','label'=>'An&aacute;lisis de marco te&oacute;rico','group'=>'Trabajo te&oacute;rico'),
      array('value'=>'An&aacute;lisis de datos','label'=>'An&aacute;lisis de datos','group'=>'Trabajo te&oacute;rico'),
      array('value'=>'S&iacute;ntesis final','label'=>'S&iacute;ntesis final','group'=>'Trabajo te&oacute;rico'),
      
      array('value'=>'Dise&ntilde;o experimental','label'=>'Dise&ntilde;o experimental','group'=>'Trabajo experimental'),
      array('value'=>'Construcci&oacute;n experimental','label'=>'Construcci&oacute;n experimental','group'=>'Trabajo experimental'),
      array('value'=>'Experimentaci&oacute;n','label'=>'Experimentaci&oacute;n','group'=>'Trabajo experimental'),

      array('value'=>'Fabricaci&oacute;n','label'=>'Fabricaci&oacute;n','group'=>'Implementaci&oacute;n'),
      array('value'=>'Instalaci&oacute;n','label'=>'Instalaci&oacute;n','group'=>'Implementaci&oacute;n'),

      array('value'=>'Divulgaci&oacute;n','label'=>'Divulgaci&oacute;n','group'=>'Ense&ntilde;anza'),
      array('value'=>'Pares','label'=>'Pares','group'=>'Ense&ntilde;anza'),
      array('value'=>'Superiores','label'=>'Superiores','group'=>'Ense&ntilde;anza'),

    );
  }

  public function getClasificacionesAdministrativas()
  {
    return array(
      array('value'=>'Planificaci&oacute;n','label'=>'Planificaci&oacute;n'),
      array('value'=>'Ejecuci&oacute;n','label'=>'Ejecuci&oacute;n'),
    );
  }

    public static function load($id){
        $tarea=Tarea::model()->findByPk($id);
        if($tarea==null)
            throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
        return $tarea;
    }

}