<?php

/**
 * Modelo para la tabla "{{tarea_archivo}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{tarea_archivo}}':
 * @property integer $id
 * @property integer $tarea_id
 * @property string $archivo
 * @property string $descripcion
 *
 * Las siguientes son las relaciones disponibles:
 * @property Tarea $tarea
 */
class TareaArchivo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TareaArchivo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{tarea_archivo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarea_id, archivo', 'required'),
			array('tarea_id', 'numerical', 'integerOnly'=>true),
			array('archivo', 'length', 'max'=>100),
			array('descripcion', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tarea_id, archivo, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'tarea' => array(self::BELONGS_TO, 'Tarea', 'tarea_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tarea_id' => 'Tarea',
			'archivo' => 'Archivo',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tarea_id',$this->tarea_id);
		$criteria->compare('archivo',$this->archivo,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
  public function beforeDelete()
  {
    //se debe verificar que este archivo no sea utilizado por alguna de las revisiones,
    //si no es utilizado entonces se eliminar, en caso contrario se mantiene
    $command=$connection->createCommand(
    'SELECT id FROM _revision_tbl_tarea_archivo 
     WHERE id='.$this->id.' LIMIT 1');
    $row=$command->queryRow();
    if($row==false)
    {
      $dir=YiiBase::getPathOfAlias('webroot').'/tareas/archivos/'.$this->tarea_id;
      //se elimina el archivo correspondiente
      if(is_dir($dir) && is_file($dir.'/'.$this->archivo))
        unlink($dir.'/'.$this->archivo);
    }
    return parent::beforeDelete();
  }
  
}