<?php
/**
 * Modulo que contiene la logica para el ingreso y evaluacion de actividades en el sistema.
 *
 * Una actividad cae dentro del marco de un proyecto o iniciativa y consiste en tareas en las
 * cuales participan integrantes de entidades (proyectos o iniciativas). El interes de registrar
 * en el sistema las actividades de los usuarios es que posteriormente las actividades pueden
 * ser evaluados por personas especificas que luego pueden asignar FAE a los usuarios que recibieron
 * buenas evaluaciones en las tareas. Este modulo contiene la logica necesaria para dichas acciones.
 * Puntos a considerar: cada tarea guarda un registro de cada modificacion que se hace sobre ella, por
 * ejemplo si se crea una tarea y luego se cambia la descripcion entonces exisitiran dos registros, uno
 * con la tarea con la primera descripcion que tenia y otro con la tarea actualizada, esto permite tener
 * un historial de versiones de tareas. El aspecto tenico del historial de versiones se maneja mediante
 * triggers en la base de datos.
 *
 * Notar que la parte de la evaluacion de actividades no esta implementada dado que PIEA aun no define un
 * procedimiento algoritmico para realizar dicha evaluacion.
 *
 * @package   modules.tareas
 * @version   1.0
 * @since     2012-08-01
 * @author    dacuna <diego.acuna@usm.cl>
 */
class TareasModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
	{
		$this->setImport(array(
			'tareas.models.*',
			'tareas.components.*',
            'usuarios.components.Entidad'
		));
	}

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
