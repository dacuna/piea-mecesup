<div class="panel">
  <h5>Archivos:</h5>
  <p>Los siguientes archivos han sido adjuntados a la tarea actual.</p>
  <ul>
    <?php foreach($this->archivos as $index=>$archivo):?>
      <li><a href="<?php echo Yii::app()->baseUrl;?>/tareas/archivos/<?php echo $archivo->tarea_id;?>/<?php echo $archivo->archivo;?>">
        <?php echo $archivo->archivo;?>
      </a></li>
    <?php endforeach;?>
  </ul>
</div>    