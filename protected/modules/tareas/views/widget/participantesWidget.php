<div class="panel">
  <h5>Participantes:</h5>
  <p>Los siguientes son los participantes de esta actividad. En rojo los participantes que a&uacute;n no han
  aprobado la actividad, en verde quienes ya la aprobaron.</p>
  <?php foreach($this->participantes as $index=>$participante):?>
    <?php if($index%3==0):?>
      <div class="row">
    <?php endif;?>
    <div class="four columns">
      <?php if($participante->aprobada==1):?>
        <p style="color:green;">
      <?php else:?>
        <p style="color:red;">
      <?php endif;?>
        <img src="<?php echo $participante->usuario->avatarUrl;?>" width="52" height="52" style="float:left;margin-right:5px;">
        <?php echo $participante->usuario->nombreCompleto;?><br>
        <span title="<?php echo $participante->usuario->email;?>"><?php echo truncate($participante->usuario->email,20);?></span>
      </p>
    </div>
    <?php if($index%3==2):?>
      </div><br>
    <?php endif;?>
  <?php endforeach;?>
  <?php if($index%3==1 || $index%3==0):?>
      </div>
    <?php endif;?>
</div>    