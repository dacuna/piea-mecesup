<div class="form">
  <div class="row">
    <div class="twelve columns">
      <form class="nice" method="post" action="<?php echo Yii::app()->createUrl('/tareas/supervision/rechazarActividad',
        array('id'=>$tarea->id));?>">
      <div class="panel">
        <h5>Rechazar actividad</h5>
        <p>Puedes incluir un comentario indicando los motivos de porque has decidido rechazar la actividad
        actual. Este motivo ser&aacute; visible por los participantes de la actividad.</p>
        <textarea name="comentario_rechazo" rows="5">Comentario...</textarea>        
        <p>
          <br>
          <?php echo CHtml::submitButton('Rechazar actividad',array('class'=>'nice red radius small button')); ?>
        </p>        
      </div>   
      </form>
    </div>
  </div>
</div>