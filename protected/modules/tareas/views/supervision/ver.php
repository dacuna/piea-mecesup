<?php
$this->pageTitle = 'Ver Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('/tareas/gestionActividades/index'),
	'Ver actividad',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Ver actividad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

      <div class="row">
        <div class="eight columns">
          <p>
          <strong>Actividad registrada en:</strong> <?php echo $tarea->tipo_entidad;?>
          <?php if($tarea->tipo_entidad=='iniciativa'):?>
            <?php echo Iniciativa::model()->findByPk($tarea->entidad_id)->nombre_abreviado;?>
          <?php else:?>
            <?php echo Proyecto::model()->findByPk($tarea->entidad_id)->nombre;?>
          <?php endif;?><br>
         </p>
        </div>
        
        <div class="four columns">
          <?php if($tarea->aprobada==0):?>
            <a href="<?php echo Yii::app()->createUrl('/tareas/supervision/aprobarActividad',array('id'=>$tarea->id));?>" class="nice green small button">Aprobar</a>
            <a href="#" data-reveal-id="myModal" class="nice red small button">Rechazar</a>
          <?php endif;?>
        </div>
      </div>
      
      <div class="panel">
        <h5>Actividad: <?php echo $tarea->titulo;?></h5>
        <p><?php echo $tarea->categoria['nombre'];?></p>
        
        <div class="row">
          <div class="four columns">
            <strong>Fecha de inicio</strong>: <?php echo $tarea->fecha_inicio; ?>
          </div>
          
          <div class="four columns">
            <strong>Fecha de t&eacute;rmino</strong>: <?php echo $tarea->fecha_termino; ?>
          </div>
          
          <div class="four columns">
            <strong>Duraci&oacute;n</strong>: <?php echo $tarea->duracion;?> min.
          </div>
        </div>
        <br>
      </div>

      <?php $this->widget('ParticipantesWidget', array('participantes' => $tarea->participantes)); ?>
      
      <div class="panel">
        <h5>Caracter&iacute;sticas</h5>
        <ul id="actividades-agregadas">
            <?php foreach($tarea->caracteristicas as $caracteristica):?>
              <li><?php echo $caracteristica->titulo;?></li>
            <?php endforeach;?>
          </ul>
      </div>

      <?php $this->widget('ArchivosWidget', array('archivos' => $tarea->ficheros)); ?>
      
      <div class="panel">
        <h5>Descripci&oacute;n</h5>
        <div style="border:1px #ccc dashed;background:#fff;padding:10px;"><?php echo $tarea->descripcion;?></div>
        <br>
      </div>

  </li>
</ul>
<?php
$this->modal['id']='myModal';
$this->modal['close']=true;

$this->modal['content']=$this->renderPartial('_rechazar_actividad',array('tarea'=>$tarea),true)
?>