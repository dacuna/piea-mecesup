<?php
$this->pageTitle = 'Ver Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('index'),
	'Ver actividad',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Ver actividad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

      <div class="row">
        <div class="eight columns">
          <p>
          <strong>Actividad registrada en:</strong> <?php echo $tarea->tipo_entidad;?>
          <?php if($tarea->tipo_entidad=='iniciativa'):?>
            <?php echo Iniciativa::model()->findByPk($tarea->entidad_id)->nombre_abreviado;?>
          <?php else:?>
            <?php echo Proyecto::model()->findByPk($tarea->entidad_id)->nombre;?>
          <?php endif;?><br>
          <?php if(isset($blame['usuario']->nombreCompleto)):?>
            <strong>&Uacute;ltima revisi&oacute;n por</strong>: <?php echo $blame['usuario']->nombreCompleto;?>
          <?php else:?>
            <strong>&Uacute;ltima revisi&oacute;n por</strong>: --
          <?php endif;?><br>
          <strong>Fecha &uacute;ltima revisi&oacute;n</strong>: <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y 'a las' HH:mm:ss 'hrs.'",$blame['fecha']);?>
          </p>
        </div>
        
        <div class="four columns">
          <a href="<?php echo Yii::app()->createUrl('/tareas/gestionActividades/historialRevisiones',array('id'=>$tarea->id));?>" class="nice small button" style="float:right;">Ver historial de revisiones</a>
        </div>
      </div>
      
      <div class="panel">
        <h5>Actividad: <?php echo $tarea->titulo;?></h5>
        <p><?php echo $tarea->categoria['nombre'];?></p>
        <?php if($tarea->aprobada==-1):?>
          <p>
            Estado: <span style="color:red;">rechazada</span><br>
            <?php if(isset($tarea->comentarioRechazo['comentario'])):?>
              Motivo: <?php echo $tarea->comentarioRechazo['comentario'];?>
            <?php endif;?>
          </p>
        <?php endif;?>
        
        <div class="row">
          <div class="four columns">
            <strong>Fecha de inicio</strong>: <?php echo $tarea->fecha_inicio; ?>
          </div>
          
          <div class="four columns">
            <strong>Fecha de t&eacute;rmino</strong>: <?php echo $tarea->fecha_termino; ?>
          </div>
          
          <div class="four columns">
            <strong>Duraci&oacute;n</strong>: <?php echo $tarea->duracion;?> min.
          </div>
        </div>
        <br>
      </div>

      <?php $this->widget('ParticipantesWidget', array('participantes' => $tarea->participantes)); ?>
        
      <div class="panel">
        <h5>Caracter&iacute;sticas</h5>
        <ul id="actividades-agregadas">
            <?php foreach($tarea->caracteristicas as $caracteristica):?>
              <li><?php echo $caracteristica->titulo;?></li>
            <?php endforeach;?>
          </ul>
      </div>
      
      <div class="panel">
        <h5>Descripci&oacute;n</h5>
        <div style="border:1px #ccc dashed;background:#fff;padding:10px;"><?php echo $tarea->descripcion;?></div>
        <br>
      </div>
      
      <?php if(!$tarea->usuarioEstadoAprobacion(Yii::app()->user->id)):?>
        <?php $this->widget('AccionesActividadWidget', array('tarea'=>$tarea));?>
      <?php endif;?>

  </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.fcbkcomplete.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/style.css');?>
<script type="text/javascript">
$(document).ready(function(){                
    $("#integrantes").fcbkcomplete({
        json_url: '<?php echo Yii::app()->createUrl('');?>',
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        filter_selected:true,
        select_all_text: "seleccionar",
    });
});
</script>