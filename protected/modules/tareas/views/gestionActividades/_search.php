<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
  )); ?>
<div class="row">
    <div class="three columns">
      <label for="Tarea[tipo_entidad]">Tarea en:</label>
      <select name="Tarea[tipo_entidad]" id="Tarea_tipo_entidad" style="width:130px;height:22px;margin-top:9px;">
        <option value="">Todas</option>
        <option value="iniciativa">Iniciativa</option>
        <option value="proyecto">Proyecto</option>
      </select>
    </div>

    <div class="three columns">
      <label for="Tarea_entidad_id">Iniciativa/Proyecto:</label>
      <?php echo $form->textField($model,'entidad_id',array('style'=>'width:130px;')); ?>
    </div>

    <div class="three columns">
      <?php echo $form->label($model,'titulo'); ?>
      <?php echo $form->textField($model,'titulo',array('style'=>'width:130px;')); ?>
    </div>

    <div class="three columns">
      <?php echo $form->label($model,'duracion'); ?>
      <?php echo $form->textField($model,'duracion',array('style'=>'width:80px;')); ?>
    </div>
    
</div>
<div class="row">
    
    <div class="three columns">
      <?php echo $form->label($model,'fecha_inicio'); ?>
      <?php echo $form->textField($model,'fecha_inicio',array('style'=>'width:130px;')); ?>
    </div>
    
    <div class="three columns">
      <?php echo $form->label($model,'fecha_termino'); ?>
      <?php echo $form->textField($model,'fecha_termino',array('style'=>'width:130px;')); ?>
    </div>

    <div class="three columns">
      <?php echo CHtml::submitButton('Buscar',array('style'=>'margin-top:25px;')); ?>
    </div>

  <?php $this->endWidget(); ?>

</div>