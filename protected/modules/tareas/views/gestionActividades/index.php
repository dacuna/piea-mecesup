<?php
$this->pageTitle = 'Historial de actividades';
$this->breadcrumbs=array(
	'Actividades'
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Listado de actividades</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="listado-actividades">

    <div class="panel">
      <h5>Actividades en edici&oacute;n</h5>
      <p>
        El siguiente listado muestra las actividades que se encuentran en edici&oacute;n (a&uacute;n no han sido
        creadas). Para continuar el trabajo sobre ellas, presiona en "editar".
        
        <table>
          <thead>
            <th>T&iacute;tulo</th>
            <th>Actividad/Proyecto</th>
            <th>Fecha</th>
            <th>Editar</th>
          </thead>
          <tbody>
            <?php if(count($por_editar)>0):?>
              <?php foreach($por_editar as $tarea):?>
                <tr>
                  <td>
                    <?php if(isset($tarea->titulo)):?>
                      <?php echo $tarea->titulo;?>
                    <?php else:?>
                      --
                    <?php endif;?>
                  </td>
                  <td>
                    <?php if(isset($tarea->entidad_id)):?>
                      <?php if($tarea->tipo_entidad=='iniciativa'):?>
                        <?php echo Iniciativa::model()->findByPk($tarea->entidad_id)->nombre_abreviado;?>
                      <?php else:?>
                        <?php echo Proyecto::model()->findByPk($tarea->entidad_id)->nombre;?>
                    <?php endif;?>
                    <?php else:?>
                      --
                    <?php endif;?>
                  </td>
                  <td>
                    <?php if(isset($tarea->fecha_inicio)):?>
                      <?php echo $tarea->fecha_inicio;?>
                    <?php else:?>
                      --
                    <?php endif;?>
                  </td>
                  <td><a href="<?php echo Yii::app()->createUrl('/tareas/crud/guardar',array('id'=>$tarea->id));?>">Editar</a></td>
                </tr>
              <?php endforeach;?>
            <?php else:?>
              <tr>
                <td colspan="5" style="text-align:center;">No se han encontrado resultados.</td>
              </tr>
            <?php endif;?>
          </tbody>
        </table>
      </p>
    </div>
    
    <div class="panel">
      <h5>Actividades pendientes de aprobaci&oacute;n</h5>
      <p>
        El siguiente listado muestra las actividades que a&uacute;n est&aacute;n pendientes de aprobaci&oacute;n por
        sus participantes.
        
        <table>
          <thead>
            <th>T&iacute;tulo</th>
            <th>Actividad/Proyecto</th>
            <th>Fecha</th>
            <th>Ver</th>
          </thead>
          <tbody>
            <?php if(count($pendientes)>0):?>
              <?php foreach($pendientes as $pendiente):?>
                <tr>
                  <td><?php echo $pendiente['titulo'];?></td>
                  <td>
                    <?php if($pendiente['tipo_entidad']=='iniciativa'):?>
                      <?php echo Iniciativa::model()->findByPk($pendiente['entidad_id'])->nombre_abreviado;?>
                    <?php else:?>
                      <?php echo Proyecto::model()->findByPk($pendiente['entidad_id'])->nombre;?>
                    <?php endif;?>
                  </td>
                  <td><?php echo $pendiente['fecha_inicio'];?></td>
                  <td><a href="<?php echo Yii::app()->createUrl('/tareas/crud/ver',array('id'=>$pendiente['id']));?>">Ver</a></td>
                </tr>
              <?php endforeach;?>
            <?php else:?>
              <tr>
                <td colspan="5" style="text-align:center;">No se han encontrado resultados.</td>
              </tr>
            <?php endif;?>
          </tbody>
        </table>
      </p>
    </div>
    
    <div class="panel">
      <h5>Actividades ingresadas al sistema</h5>
      <p>
        El siguiente listado muestra las actividades que ya han sido aprobadas por lo que han sido ingresadas en su
        totalidad al sistema. Puedes buscar actividades utilizando los filtros disponibles.

          <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm',
            array('type'=>'nice','action'=>url('/tareas/gestionActividades/enviarParaRevision'))); ?>
          <?php $this->widget('zii.widgets.grid.CGridView', array(
              'dataProvider'=>$resultados,
              'itemsCssClass'=>'table-resultados',
              'columns'=>array(
              array(
                'id'=>'selectedItems',
                'class'=>'CCheckBoxColumn',
                'selectableRows'=>2
              ),
              'titulo',
              array(
                'name'=>'Actividad/Proyecto',
                'value'=>'$data->nombre_entidad',
              ),
              'fecha_inicio',
              array(
                'class'=>'CLinkColumn',
                'label'=>'Ver',
                'urlExpression'=>'Yii::app()->createUrl("/tareas/crud/ver",array("id"=>$data->id))'
              ),
              )
          )); 
          ?>

          Con las seleccionadas:
          <input type="submit" name="revision" value="Enviar para revisi&oacute;n" class="nice button">
          <br><br>
          <?php $this->endWidget(); ?>
          <?php $this->renderPartial('_search',array('model'=>$model)); ?>

      </p>
    </div>

  </li>
</ul>