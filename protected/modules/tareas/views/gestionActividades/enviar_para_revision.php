<?php
$this->pageTitle = 'Enviar tareas para revisi&oacute;n';
$this->breadcrumbs=array(
	'Enviar tareas para revisi&oacute;n'
);
?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Enviar tareas para revisi&oacute;n</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="listado-actividades">

    <div class="panel">
      <h5>Enviar tareas para revisi&oacute;n</h5>
      <p>
        Se han seleccionado las siguientes tareas para revisi&oacute;n:
        
        <table>
          <thead>
            <th>T&iacute;tulo</th>
            <th>Actividad/Proyecto</th>
            <th>Fecha</th>
            <th>Ver</th>
          </thead>
          <tbody>
            <?php if(count($tareas)>0):?>
              <?php foreach($tareas as $pendiente):?>
                <tr>
                  <td><?php echo $pendiente['titulo'];?></td>
                  <td>
                    <?php if($pendiente['tipo_entidad']=='iniciativa'):?>
                      <?php echo Iniciativa::model()->findByPk($pendiente['entidad_id'])->nombre_abreviado;?>
                    <?php else:?>
                      <?php echo Proyecto::model()->findByPk($pendiente['entidad_id'])->nombre;?>
                    <?php endif;?>
                  </td>
                  <td><?php echo $pendiente['fecha_inicio'];?></td>
                  <td><a href="<?php echo Yii::app()->createUrl('/tareas/crud/ver',array('id'=>$pendiente['id']));?>">Ver</a></td>
                </tr>
              <?php endforeach;?>
            <?php else:?>
              <tr>
                <td colspan="5" style="text-align:center;">No se han encontrado resultados.</td>
              </tr>
            <?php endif;?>
          </tbody>
        </table>

        Para continuar, debes seleccionar a alg&uacute;n usuario para que realice la revisi&oacute;n de tus
        actividades:
      </p>

        <p>
            <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm', array('id' => 'invitar-jefe-form')); ?>
            <label style="display:inline;">Ingrese nombre de usuario:</label>
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'attribute' => 'Tareas[revisor]',
                'sourceUrl' => array('/iniciativas/crudIniciativas/buscarIntegrante'),
                'name' => 'Tareas[revisor]',
                'options' => array(
                    'minLength' => '3',
                    'showAnim' => 'fold',
                    'select' => "js: function(event, ui) {
                      $('#Tareas_revisor_final').val(ui.item['id']);
                    }"
                ),
                'htmlOptions' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'style' => 'height:24px;'
                ),
            )); ?>
            <input type="hidden" name="Tareas[revisor_final]" value="0" id="Tareas_revisor_final">
            <?php echo CHtml::submitButton('Seleccionar usuario', array('class' => 'nice radius small button')); ?>

            <?php $this->endWidget(); ?>
        </p>
    </div>

  </li>
</ul>