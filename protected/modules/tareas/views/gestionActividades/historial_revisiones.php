<?php
$this->pageTitle = 'Ver Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('index'),
  $tarea->titulo=>array('crud/ver','id'=>$tarea->id),
	'Ver historial de revisiones',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Ver historial de revisiones</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

      <h4><?php echo $tarea->titulo;?></h4>
      
      <div class="row">
        <div class="twelve columns">
          <p>
          <strong>Actividad registrada en:</strong> <?php echo $tarea->tipo_entidad;?>
          <?php if($tarea->tipo_entidad=='iniciativa'):?>
            <?php echo Iniciativa::model()->findByPk($tarea->entidad_id)->nombre_abreviado;?>
          <?php else:?>
            <?php echo Proyecto::model()->findByPk($tarea->entidad_id)->nombre;?>
          <?php endif;?><br>
          <?php if(isset($blame['usuario']->nombreCompleto)):?>
            <strong>&Uacute;ltima revisi&oacute;n por</strong>: <?php echo $blame['usuario']->nombreCompleto;?><br>
          <?php else:?>
            <strong>&Uacute;ltima revisi&oacute;n por</strong>: --
          <?php endif;?><br>
          <strong>Fecha &uacute;ltima revisi&oacute;n</strong>: <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y 'a las' HH:mm:ss 'hrs.'",$blame['fecha']);?>
          </p>
        </div>
      </div>
      
      <h5>Revisiones</h5>
      <table>
        <thead>
          <tr>
            <th>#ID</th>
            <th>Usuario que edit&oacute;</th>
            <th>Fecha edici&oacute;n</th>
            <th>Ver versi&oacute;n</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($revisiones as $revision):?>
            <tr>
              <td><?php echo $revision['index'];?></td>
              <td>
                <?php if($revision['usuario']!=null):?>
                  <?php echo $revision['usuario']->nombreCompleto;?>
                <?php else:?>
                  --
                <?php endif;?>
              </td>
              <td><?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y 'a las' HH:mm:ss 'hrs.'",$revision['fecha']);?></td>
              <td><a href="<?php echo Yii::app()->createUrl('/tareas/gestionActividades/verRevision',array('id'=>$revision['revision']));?>">Ver versi&oacute;n</a></td>
            </tr>
          <?php endforeach;?>
        </tbody>
      </table>

  </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.fcbkcomplete.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/style.css');?>
<script type="text/javascript">
$(document).ready(function(){                
    $("#integrantes").fcbkcomplete({
        json_url: '<?php echo Yii::app()->createUrl('');?>',
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        filter_selected:true,
        select_all_text: "seleccionar",
    });
});
</script>