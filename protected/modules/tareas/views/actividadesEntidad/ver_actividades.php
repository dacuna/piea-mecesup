<?php
$this->pageTitle = 'Ver '.$entidad->getTipoEntidad().' '.$entidad->getNombre();
$this->breadcrumbs=array(
  $entidad->getTipoEntidad()=>array('index'),
  'Ver '.$entidad->getTipoEntidad(),
);

?>

<?php $this->renderPartial("//partials/_menu_top_".strtolower($entidad->getTipoEntidad()),array('entidad'=>$entidad,'selected'=>2));?>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-proyecto">

    <div class="panel">
      <h5>Actividades ingresadas al sistema</h5>
      <p>
        El siguiente listado muestra las actividades p&uacute;blicas que ya han sido aprobadas por lo que han sido ingresadas en su
        totalidad al sistema en/la <?php echo strtolower($entidad->getTipoEntidad()).' '.$entidad->getNombre();?>.
        Puedes buscar actividades utilizando los filtros disponibles.
        
        <?php $this->widget('zii.widgets.grid.CGridView', array(
              'dataProvider'=>$resultados,
              'itemsCssClass'=>'table-resultados',
              'columns'=>array(
              'titulo',
              array(
                'name'=>'Actividad/Proyecto',
                'value'=>'$data->nombre_entidad',
              ),
              'fecha_inicio',
              'fecha_termino',
              array(
                'class'=>'CLinkColumn',
                'label'=>'Ver',
                'urlExpression'=>'Yii::app()->createUrl("/tareas/crud/ver",array("id"=>$data->id))'
              ),
              )
          )); 
          ?>

      </p>
    </div>

  </li>
</ul>


