<p>Estimado usuario:</p>
En calidad de coordinador de la iniciativa <?php echo $iniciativa->nombre_abreviado;?>, debe aprobar una actividad
creada en el proyecto <?php echo $proyecto->nombre;?>, dado que el jefe de este proyecto as&iacute; lo ha determinado.
Para ver la actividad a aprobar puedes presionar en el bot&oacute;n "Ver actividad", luego
tendr&aacute;s la oportunidad ya sea de aprobar o rechazarla.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/tareas/crud/ver',array('id'=>$tarea_id));?>" class="nice radius small button">
  Ver actividad
</a>