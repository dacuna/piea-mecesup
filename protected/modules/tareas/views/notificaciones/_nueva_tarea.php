<p>Estimado usuario:</p>
Una nueva actividad ha sido creadad en <?php echo $nombre;?> por el usuario <?php echo Yii::app()->user->nombre;?>, 
para que esta actividad sea ingresada al sistema debe ser
validada por cada uno de los integrantes que en ella aparecen. Como integrante de esta actividad debes verificar y
validar su contenido. Para esto, puedes presionar en el bot&oacute;n "Ver Actividad", luego podr&aacute;s ya sea
editar, aceptar o rechazar la actividad. Si no deseas formar parte de esta actividad puedes presionar en "Desvincular de
actividad" y ser&aacute;as eliminado como participante de esta.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/tareas/gestionActividades/verDesdeBuzon',array('id'=>$id,'tarea'=>$tarea_id));?>" class="nice radius small button">Ver actividad</a>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/tareas/gestionActividades/desvincularDesdeBuzon',array('id'=>$id,'tarea'=>$tarea_id));?>"  class="nice radius small red button">Desvincular de actividad</a>
    