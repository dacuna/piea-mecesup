<p>Estimado usuario:</p>
En calidad de jefe/coordinador en <?php echo $tipo_entidad;?> <?php echo $nombre;?>, debe aprobar una actividad
creada en esta entidad. Para ver la actividad a aprobar puedes presionar en el bot&oacute;n "Ver actividad", luego
tendr&aacute;s la oportunidad ya sea de aprobar o rechazar la actividad.
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/tareas/crud/ver',array('id'=>$tarea_id));?>" class="nice radius small button">Ver actividad</a>