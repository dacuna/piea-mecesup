<p>Estimado usuario:</p>
El superior a cargo de su <?php echo $tipo_entidad;?> ha 
<?php if($accion=='aprobar'):echo 'aprobado'; else: echo 'rechazado'; endif;?> la actividad <?php echo $tarea->titulo;?>. Puedes ver un resumen
de la actividad mencionada presionando el bot&oacute;n "Ver Actividad".
<br><br>
Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/tareas/gestionActividades/verDesdeBuzon',array('id'=>$id,'tarea'=>$tarea->id));?>" class="nice radius small button">Ver actividad</a>