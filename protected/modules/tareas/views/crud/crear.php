<?php
$this->pageTitle = 'Ingresar Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('index'),
	'Crear actividad',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Ingresar actividad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

    <div class="form">

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
      'id'=>'actividad-form',
      'type'=>'nice',
      'htmlOptions'=>array('enctype'=>'multipart/form-data')
    )); ?>
    
      <?php echo $form->errorSummary($tarea); ?>

      <div class="row">
        <div class="six columns">
          Iniciativa:
          <ul id="iniciativas-choose">
          </ul>
        </div>

        <div class="six columns">
          Proyecto:
          <ul id="proyectos-choose">
          </ul>
        </div>
      </div>

      <p id="mensaje-entidad">
        Debes seleccionar una iniciativa o proyecto para tu actividad.
      </p>
      <?php echo $form->hiddenField($tarea,'entidad_id');?>
      <?php echo $form->hiddenField($tarea,'tipo_entidad');?>

      <div class="row">
        <div class="twelve columns">
          <?php echo $form->textFieldRow($tarea,"titulo",array('style'=>'width:100%;')); ?> 
        </div>
        
        <div class="twelve columns">
          <div class="row">
            <div class="six columns">
              <?php echo $form->dropDownListRow($tarea, 'categoria_id',
                CHtml::listData($tarea->categorias, 'id', 'nombre')
              ,array('style'=>'height: 28px;width:100%;','encode'=>false)); ?> 
            </div>
            <div class="six columns">
              <label for="Tarea[clasificacion]" class="required" style="margin-bottom:0;">Clasificaci&oacute;n *</label>
              <select style="height:28px;display:none;width:100%;margin-top:0;" name="Tarea[clasificacion]" id="clasificacion_tecnica">
                <optgroup label="Trabajo te&oacute;rico">
                  <option>An&aacute;lisis de marco te&oacute;rico</option>
                  <option>An&aacute;lisis de datos</option>
                  <option>S&iacute;ntesis final</option>
                </optgroup>
                <optgroup label="Trabajo experimental">
                  <option>Dise&ntilde;o experimental</option>
                  <option>Construcci&oacute;n experimental</option>
                  <option>Experimentaci&oacute;n</option>
                </optgroup>
                <optgroup label="Implementaci&oacute;n">
                  <option>Fabricaci&oacute;n</option>
                  <option>Instalaci&oacute;n</option>
                </optgroup>
                <optgroup label="Ense&ntilde;anza">
                  <option>Divulgaci&oacute;n</option>
                  <option>Pares</option>
                  <option>Superiores</option>
                </optgroup>
              </select>

              <select style="height:28px;display:none;width:100%;" name="Tarea[clasificacion]" id="clasificacion_administrativa">
                <option>Planificaci&oacute;n</option>
                <option>Ejecuci&oacute;n</option>
              </select>
            </div>
          </div>
          
            <p>
              Descripci&oacute;n: *<br>
              <?php $this->widget('application.extensions.redactorjs.Redactor', 
                array('lang'=>'en','toolbar'=>'mini','model'=>$tarea,'attribute'=>'descripcion',
                  'height'=>'300px','editorOptions'=>array('focus'=>false)));?>
            </p>

            <fieldset>
              <h5>Archivos</h5>
              <p>Puedes adjuntar archivos a la actividad mediante el siguiente formulario.</p>
              <div id="archivos-agregados">
                
              </div>
              <a href="#" class="nice small green button" id="agregar-archivo">Agregar archivo</a><br><br>
            </fieldset>
        </div>

      </div>
      
      <p>
        <label for="integrantes" class="required">
          Participantes: <span class="required">*</span>
        </label>
        <select id="integrantes" name="Tarea[integrantes]">
          <option value="<?php echo Yii::app()->user->id;?>" class="selected"><?php echo Yii::app()->user->nombre;?></option>
          <?php if(isset($tarea->integrantes)):?>
            <?php foreach($tarea->integrantes as $integrante):?>
                <?php if($integrante!=Yii::app()->user->id && ($persona=Persona::model()->findByPk($integrante))!=null):?>
                  <option value="<?php echo $integrante;?>" class="selected"><?php echo $persona->nombreCompleto;?></option>
                <?php endif;?>
            <?php endforeach;?>
          <?php endif;?>
        </select>
      </p>      
      
      <div class="row">
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_inicio'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_inicio',
            'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_inicio'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_termino'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_termino',
            'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_termino'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->textFieldRow($tarea,"duracion",array('style'=>'width:50%;margin-top:-9px;')); ?> 
        </div>
      </div>
      
      <fieldset id="fieldset_caracteristicas">
        <h5>Caracter&iacute;sticas</h5>
        <p>Selecciona las caracter&iacute;sticas de la actividad a ingresar e ingresa una breve descripci&oacute;n de ser necesario.</p>
        
        <p style="margin-bottom:3px;">Caracter&iacute;stica:</p>
        <div class="row">
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_1','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=1'), 'id', 'nombre'),
              array('style'=>'height:28px;width:138px;','encode'=>false)); 
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_2','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=2 and id_padre=1'), 'id', 'nombre'),
              array('style'=>'height:28px;width:138px;','encode'=>false));
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_3','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=3 and id_padre=3'), 'id', 'nombre'),
                    array(
                    'ajax' => array(
                    'type'=>'POST',
                    'url'=>CController::createUrl('utils/obtenerCategorias3'),
                    'update'=>'#nivel_4',
                    ),'style'=>'height:28px;width:138px;','encode'=>false));
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_4','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=4 and id_padre=7'), 'id', 'nombre'),
                   array('style'=>'height:28px;width:138px;','encode'=>false)
              );
            ?>
          </div>
        </div>
        
        <div class="row">
          <div class="eight columns">
            Breve descripci&oacute;n: <input type="text" name="titulo_actividad" id="titulo_actividad" class="input-text" style="width:420px;">
          </div>
          
          <div class="four columns">
            <a href="#" id="agregar-actividad-select" class="nice small radius green button" style="margin-top:19px;">Agregar</a>
          </div>
        </div>
        
        <?php if(isset($tarea->caracteristicas_al_crear)):?>
          <div id="div-agregadas" class="row">
        <?php else:?>
          <div id="div-agregadas" class="row hide">
        <?php endif;?>
          <div class="twelve columns">
            <h5>Actividades agregadas</h5>
            <ul id="actividades-agregadas">
              <?php //die(var_dump($tarea->caracteristicas_al_crear));?>
              <?php if(isset($tarea->caracteristicas_al_crear)):?>
                <?php foreach($tarea->caracteristicas_al_crear as $index=>$caracteristica):?>
                  <li><?php echo $caracteristica['tipo_nombre'];?> >> <?php echo $caracteristica['titulo'];?>
                  <span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>
                  <input type="hidden" name="CategoriaActividadHasTarea[<?php echo $index;?>][categoria_actividad_id]" value="<?php echo $caracteristica['categoria_actividad_id'];?>">
                  <input type="hidden" name="CategoriaActividadHasTarea[<?php echo $index;?>][tipo_nombre]" value="<?php echo $caracteristica['tipo_nombre'];?>">
                  <input type="hidden" name="CategoriaActividadHasTarea[<?php echo $index;?>][titulo]" value="<?php echo $caracteristica['titulo'];?>"></li>
                <?php endforeach;?>
              <?php endif;?>
            </ul>
          </div>
        </div>
      
      </fieldset>
      <br>

      <div class="panel">
        <h5>Visibilidad de la actividad</h5>
        <p>&iquest;Deseas que esta actividad sea visible entre las actividades de la iniciativa o proyecto seleccionado?</p>
        <?php echo $form->radioButtonListRow($tarea, "publica", array(
            "1" => "Si",
            "0" => "No",
        ), array('labelHtmlOptions' => array("label" => false))); ?>
      </div>

      <div class="row buttons">
        <?php echo CHtml::submitButton('Ingresar actividad',array('class'=>'nice radius medium button')); ?>
        <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius white medium button','id'=>'guardar-actividad')); ?>
      </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->
  </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.fcbkcomplete.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/style.css');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.timepicker.min.js', CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.timepicker.css');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/underscore-min.js', CClientScript::POS_HEAD);?>

<div class="hidden">
  <ul id="iniciativas-list">
    <?php foreach($usuario->iniciativas as $iniciativa):?>
      <li data-id="<?php echo $iniciativa->id;?>"><?php echo $iniciativa->nombre_abreviado;?></li>
    <?php endforeach;?>
    <?php foreach($usuario->proyectos as $proyecto):?>
        <?php if(isset($proyecto->iniciativa_id)):?>
            <li data-id="<?php echo $proyecto->iniciativa->id;?>"><?php echo $proyecto->iniciativa->nombre_abreviado;?></li>
        <?php endif;?>
    <?php endforeach;?>
  </ul>

  <ul id="proyectos-list">
  <?php foreach($usuario->proyectos as $proyecto):?>
      <?php if(isset($proyecto->iniciativa_id)):?>
        <li data-id="<?php echo $proyecto->id;?>">
          <span class="padre"><?php echo $proyecto->iniciativa->id;?></span>
          <span class="nombre"><?php echo $proyecto->nombre;?></span>
        </li>
      <?php endif;?>
  <?php endforeach;?>
  </ul>
</div>

<script type="text/javascript">
$(document).ready(function(){
  //codigo final para filtros de entidades
  var mensajes=["Debes seleccionar una iniciativa o proyecto para tu actividad","Has seleccionado el proyecto ","Has seleccionado la iniciativa "];
  var iniciativas=[]; var proyectos=[];
  $('#iniciativas-list li').each(function(){
    iniciativas.push({'id':$(this).attr('data-id'),'name':$(this).html()});
  });
  //en iniciativas_final estan las iniciativas filtradas (sin repeticion)
  iniciativas=_.groupBy(iniciativas,function(iniciativa){return iniciativa.name});
  _.each(iniciativas,function(elem,key){
    iniciativas[key]=_.uniq(elem,false,function(value){return value.name});
  });
  //para los proyectos
  $('#proyectos-list li').each(function(){
    var name=$(this).children('.nombre').first().html();
    var padre=$(this).children('.padre').first().html();
    proyectos.push({'id':$(this).attr('data-id'),'name':name,'padre':padre});
  });
  //se llenan las listas con la informacion correspondiente
  _.each(iniciativas,function(iniciativa,key){
    elem='<a href="#" class="filter-proyecto iniciativa-select" data-id="'+iniciativa[0].id+'">'+iniciativa[0].name+'</a>';
    $('#iniciativas-choose').append('<li>'+elem+'</li>');
  });
  //handler para filtrar los proyectos
  $('.filter-proyecto').click(function(e){
    e.preventDefault();
    $('#proyectos-choose').empty(); //se eliminan todos los proyectos en la lista
    var id=$(this).attr('data-id');
    _.each(proyectos,function(proyecto,key){
      if(proyecto.padre==id)
      {
        elem='<a href="#" class="proyecto-select" data-id="'+proyecto.id+'">'+proyecto.name+'</a>';
        $('#proyectos-choose').append('<li>'+elem+'</li>');
        $('.proyecto-select').bind('click',proyectoSelectClickHandler);
      }
    });
  });
  //dos handlers mas, uno para cuando se selecciona una iniciativa, y el otro para cuando se selecciona un proyecto
  function changeDataStyle(elem){
    //se remueven los estilos de seleccion de los hermanos
    $(elem).parent().siblings().removeAttr('style');
    $(elem).parent().siblings().children().removeAttr('style');
    //se agregan los estilos de seleccion
    $(elem).parent().css('background-color','#00A6FC');
    $(elem).css('color','white');
  }
  $('.iniciativa-select').click(function(e){
    e.preventDefault();
    changeDataStyle(this);
    //se debe actualizar el mensaje y los inputs correspondientes
    $('#mensaje-entidad').html(mensajes[2]+'<b>'+$(this).html()+'</b>');
    $('#Tarea_entidad_id').val($(this).attr('data-id'));
    $('#Tarea_tipo_entidad').val('iniciativa');
  });
  function proyectoSelectClickHandler(e)
  {
    e.preventDefault();
    changeDataStyle(this);
    //se debe actualizar el mensaje y los inputs correspondientes
    $('#mensaje-entidad').html(mensajes[1]+'<b>'+$(this).html()+'</b>');
    $('#Tarea_entidad_id').val($(this).attr('data-id'));
    $('#Tarea_tipo_entidad').val('proyecto');
  }
  //por ultimo, si hay errores de validacion, mantener seleccionada la entidad correspondiente
  var selected_in=$('#Tarea_entidad_id').val();
  var tipo_selected=$('#Tarea_tipo_entidad').val();
  if(tipo_selected=='iniciativa')
    $('.iniciativa-select[data-id='+selected_in+']').trigger('click');
  if(tipo_selected=='proyecto')
  {
    //se deben lanzar dos handlers, el de la iniciativa y el del proyecto
    var proyecto=_.find(proyectos,function(proyecto){ return proyecto.id==selected_in;});
    $('.iniciativa-select[data-id='+proyecto.padre+']').trigger('click');
    $('.proyecto-select[data-id='+proyecto.id+']').trigger('click');
  }

  //OTROS
    $('#Tarea_titulo').focus(); 
    $('#Tarea_duracion').timepicker({ 'timeFormat': 'H:i','step':15 });
    $("#integrantes").fcbkcomplete({
        json_url: '<?php echo Yii::app()->createUrl('/usuarios/userActions/buscarIntegrante',array('mode'=>'json'));?>',
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        filter_selected:true,
        select_all_text: "seleccionar",
    });
    
    $('.span-actividad').bind('click', function() {
        $(this).parent().remove();
        return false;
    });

    $('#agregar-actividad-select').click(function(){
      //se debe obtener el tipo de actividad
      var tipo_nombre=$('#nivel_4').find(':selected').text();
      if(tipo_nombre=="")
        tipo_nombre=$('#nivel_3').find(':selected').text();
      if(tipo_nombre=="")
        tipo_nombre=$('#nivel_2').find(':selected').text();
      var tipo_id=$('#nivel_4').find(':selected').val();
      if(tipo_id==undefined)
        tipo_id=$('#nivel_3').find(':selected').val();
      if(tipo_id==undefined)
        tipo_id=$('#nivel_2').find(':selected').val();
      var act_titulo=$('#titulo_actividad').val();
      var index=$('#actividades-agregadas li:last-child').index();
      if(index==-1)
        index=0;
      else
        index+=1;
      var elem='<li>'+tipo_nombre+' >> '+act_titulo+
        '<span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">'+
        'Eliminar'
        +'</span>'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][categoria_actividad_id]" value="'+tipo_id+'">'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][tipo_nombre]" value="'+tipo_nombre+'">'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][titulo]" value="'+act_titulo+'"></li>';
      $('#actividades-agregadas').append(elem);
      $('.span-actividad').bind('click', function() {
        $(this).parent().remove();
        return false;
      });
      $('#div-agregadas').removeClass('hide');
      return false;
    });
     
     $('#guardar-actividad').click(function(){
      $('#actividad-form').attr('action','<?php echo Yii::app()->createUrl('/tareas/crud/guardar');?>');
     });
     
     $('#agregar-archivo').click(function(){
      var total= $('#archivos-agregados').children().length;
      var elem='<p class="archivos-actividad" data-id="'+total+'">'+
            'Archivo: <input type="file" name="Tarea[archivos][]">'+
            '<span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>'+
      '</p>';
      $('#archivos-agregados').append(elem);
      return false;
     });

     var valor=$('#Tarea_categoria_id option:selected').val();
       if(valor==1)
       {
        $('#clasificacion_administrativa').show();
        $('#clasificacion_administrativa').attr('name','Tarea[clasificacion]');
        $('#fieldset_caracteristicas').show();
        $('#clasificacion_tecnica').hide();
        $('#clasificacion_tecnica').attr('name','fail');
       }
       if(valor==2)
       {
        $('#clasificacion_tecnica').show();
        $('#clasificacion_tecnica').attr('name','Tarea[clasificacion]');
        $('#clasificacion_administrativa').hide();
        $('#clasificacion_administrativa').attr('name','fail');
        $('#fieldset_caracteristicas').hide();
       }
     $('#Tarea_categoria_id').change(function(){
       var valor=$('#Tarea_categoria_id option:selected').val();
       if(valor==1)
       {
        $('#clasificacion_administrativa').show();
        $('#clasificacion_administrativa').attr('name','Tarea[clasificacion]');
        $('#clasificacion_tecnica').hide();
        $('#clasificacion_tecnica').attr('name','fail');
        $('#fieldset_caracteristicas').show();
       }
       if(valor==2)
       {
        $('#clasificacion_tecnica').show();
        $('#clasificacion_tecnica').attr('name','Tarea[clasificacion]');
        $('#clasificacion_administrativa').hide();
        $('#clasificacion_administrativa').attr('name','fail');
        $('#fieldset_caracteristicas').hide();
       }
     });
    
});
</script>

<?php Yii::app()->clientScript->registerScript('trigger_nivel3', 
  "jQuery(function($) {
  $('body').on('change','#nivel_1',function(){
     jQuery.ajax({'type':'POST','url':'".CController::createUrl('utils/obtenerCategorias1')."','cache':false,
     'data':jQuery(this).parents('form').serialize(),
     'success':function(html){
      jQuery('#nivel_2').html(html);
      //se actualiza tambien el nivel_3
      $('#nivel_2').trigger('change');
     }
     });
     return false;});
  $('body').on('change','#nivel_2',function(){
     jQuery.ajax({'type':'POST','url':'".CController::createUrl('utils/obtenerCategorias2')."','cache':false,
     'data':jQuery(this).parents('form').serialize(),
     'success':function(html){
      jQuery('#nivel_3').html(html);
      //se actualiza tambien el nivel_3
      $('#nivel_3').trigger('change');
     }
     });
     return false;});
  });", 
CClientScript::POS_END);?>