<?php
$this->pageTitle = 'Editar Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('index'),
	'Editar actividad',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Editar actividad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

    <div class="form">

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
      'id'=>'actividad-form',
      'type'=>'nice',
      'htmlOptions'=>array('enctype'=>'multipart/form-data')
    )); ?>
    
      <?php echo $form->errorSummary($tarea); ?>

      <div class="row">
        <div class="eight columns">
          <p>
          <strong>Actividad registrada en:</strong> <?php echo $tarea->tipo_entidad;?>
          <?php if($tarea->tipo_entidad=='iniciativa'):?>
            <?php echo Iniciativa::model()->findByPk($tarea->entidad_id)->nombre_abreviado;?>
          <?php else:?>
            <?php echo Proyecto::model()->findByPk($tarea->entidad_id)->nombre;?>
          <?php endif;?><br>
          <strong>&Uacute;ltima revisi&oacute;n por</strong>: <?php echo $blame['usuario']->nombreCompleto;?><br>
          <strong>Fecha &uacute;ltima revisi&oacute;n</strong>: <?php echo Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' y 'a las' HH:mm:ss 'hrs.'",$blame['fecha']);?>
          </p>
        </div>
        
        <div class="four columns">
          <a href="<?php echo Yii::app()->createUrl('/tareas/gestionActividades/historialRevisiones',array('id'=>$tarea->id));?>" class="nice small button" style="float:right;">Ver historial de revisiones</a>
        </div>
      </div>
      
      <div class="row">
        <div class="twelve columns">
          <?php echo $form->textFieldRow($tarea,"titulo",array('style'=>'width:100%;')); ?> 
        </div>
        
        <div class="twelve columns">
          <div class="row">
            <div class="six columns">
              <?php echo $form->dropDownListRow($tarea, 'categoria_id',
                CHtml::listData($tarea->categorias, 'id', 'nombre')
              ,array('style'=>'height: 28px;width:100%;','encode'=>false)); ?> 
            </div>
            <div class="six columns">
              <label for="Tarea[clasificacion]" class="required" style="margin-bottom:0;">Clasificaci&oacute;n *</label>
              <?php echo $form->dropDownList($tarea, 'clasificacion',
                CHtml::listData($tarea->clasificacionesTecnicas, 'label', 'value','group')
              ,array('style'=>'height: 28px;width:100%;display:none;margin-top:0;','id'=>'clasificacion_tecnica','encode'=>false)); ?>

              <?php echo $form->dropDownList($tarea, 'clasificacion',
                CHtml::listData($tarea->clasificacionesAdministrativas, 'label', 'value')
              ,array('style'=>'height: 28px;width:100%;display:none;','id'=>'clasificacion_administrativa','encode'=>false)); ?>
            </div>
          </div>
          
            <p>
              Descripci&oacute;n: *<br>
              <?php $this->widget('application.extensions.redactorjs.Redactor', 
                array('lang'=>'en','toolbar'=>'mini','model'=>$tarea,'attribute'=>'descripcion',
                  'height'=>'300px','editorOptions'=>array('focus'=>false)));?>
            </p>

            <fieldset>
              <h5>Archivos</h5>
              <p>Puedes adjuntar archivos a la actividad mediante el siguiente formulario.</p>
              <div id="archivos-agregados">
                
              </div>
              <a href="#" class="nice small green button" id="agregar-archivo">Agregar archivo</a><br><br>
              
              <div class="row">
                <div class="twelve columns">
                  <h5>Archivos agregados</h5>
                  <ul id="archivos-agregados">
                    <?php foreach($tarea->ficheros as $index=>$archivo):?>
                      <li><a href="<?php echo Yii::app()->baseUrl;?>/tareas/archivos/<?php echo $archivo->tarea_id;?>/<?php echo $archivo->archivo;?>">
                        <?php echo $archivo->archivo;?>
                        </a>
                        <span class="span-archivo archivo-anterior red label" data-id="<?php echo $archivo->id;?>" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>
                      </li>
                    <?php endforeach;?>
                  </ul>
                </div>
              </div>
            </fieldset>
        </div>
      </div>

      <p>
        <label for="integrantes" class="required">
          Integrantes: <span class="required">*</span>
        </label>
        <select id="integrantes" name="Tarea[integrantes]">
          <?php foreach($tarea->participantes as $participante):?>
            <option class="selected"><?php echo $participante->usuario->nombreCompleto;?></option>
          <?php endforeach;?>
        </select>
      </p>      
      
      <div class="row">
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_inicio'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_inicio',
            'options' => array('dateFormat'=>'dd-mm-yy'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_inicio'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_termino'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_termino',
            'options' => array('dateFormat'=>'dd-mm-yy'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_termino'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->textFieldRow($tarea,"duracion",array('style'=>'width:50%;margin-top:-9px;')); ?> 
        </div>
      </div>
      
      <fieldset id="fieldset_caracteristicas">
        <h5>Caracter&iacute;sticas</h5>
        <p>Selecciona las caracter&iacute;sticas de la actividad a ingresar e ingresa una breve descripci&oacute;n de ser necesario.</p>
        
        <p style="margin-bottom:3px;">Caracter&iacute;stica:</p>
        <div class="row">
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_1','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=1'), 'id', 'nombre'),
              array('style'=>'height:28px;width:138px;')); 
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_2','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=2 and id_padre=1'), 'id', 'nombre'),
              array('style'=>'height:28px;width:138px;'));
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_3','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=3 and id_padre=3'), 'id', 'nombre'),
                    array(
                    'ajax' => array(
                    'type'=>'POST',
                    'url'=>CController::createUrl('utils/obtenerCategorias3'),
                    'update'=>'#nivel_4',
                    ),'style'=>'height:28px;width:138px;'));
            ?>
          </div>
          
          <div class="three columns">
            <?php
              echo CHtml::dropDownList('nivel_4','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=4 and id_padre=7'), 'id', 'nombre'),
                   array('style'=>'height:28px;width:138px;')
              );
            ?>
          </div>
        </div>
        <div class="row">
          <div class="eight columns">
            Breve descripci&oacute;n: <input type="text" name="titulo_actividad" id="titulo_actividad" class="input-text" style="width:420px;">
          </div>
          
          <div class="four columns">
            <a href="#" id="agregar-actividad-select" class="nice small radius green button" style="margin-top:19px;">Agregar</a>
          </div>
        </div>
        
        <div id="div-agregadas" class="row">
          <div class="twelve columns">
            <h5>Caracter&iacute;sticas agregadas</h5>
            <ul id="actividades-agregadas">
              <?php foreach($tarea->caracteristicas as $caracteristica):?>
                <li>
                  <?php echo $caracteristica->titulo;?>
                  <span class="span-actividad agregada-anterior red label" data-id="<?php echo $caracteristica->id;?>" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      </fieldset>

      <div class="panel">
        <h5>Visibilidad de la actividad</h5>
        <p>&iquest;Deseas que esta actividad sea visible entre las actividades de la iniciativa o proyecto seleccionado?</p>
        <?php echo $form->radioButtonListRow($tarea, "publica", array(
            "1" => "Si",
            "0" => "No",
        ), array('labelHtmlOptions' => array("label" => false))); ?>
      </div>
      
      <div class="row buttons">
        <?php echo CHtml::submitButton($tarea->isNewRecord ? 'Crear actividad' : 'Guardar',array('class'=>'nice radius medium button')); ?>
      </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->
  </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.fcbkcomplete.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/style.css');?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.timepicker.min.js', CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.timepicker.css');?>
<script type="text/javascript">
$(document).ready(function(){  
    $('#Tarea_titulo').focus(); 
    $('#Tarea_duracion').timepicker({ 'timeFormat': 'H:i','step':15 });              
    $("#integrantes").fcbkcomplete({
        json_url: '<?php echo Yii::app()->createUrl('/usuarios/userActions/buscarIntegrante',array('mode'=>'json'));?>',
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        filter_selected:true,
        select_all_text: "seleccionar",
    });
    
    $('.span-actividad').bind('click', function() {
        $(this).parent().remove();
        return false;
    });

    $('#agregar-actividad-select').click(function(){
      //se debe obtener el tipo de actividad
      var tipo_nombre=$('#nivel_4').find(':selected').text();
      if(tipo_nombre=="")
        tipo_nombre=$('#nivel_3').find(':selected').text();
      if(tipo_nombre=="")
        tipo_nombre=$('#nivel_2').find(':selected').text();
      var tipo_id=$('#nivel_4').find(':selected').val();
      if(tipo_id==undefined)
        tipo_id=$('#nivel_3').find(':selected').val();
      if(tipo_id==undefined)
        tipo_id=$('#nivel_2').find(':selected').val();
      var act_titulo=$('#titulo_actividad').val();
      var index=$('#actividades-agregadas li:last-child').index();
      if(index==-1)
        index=0;
      else
        index+=1;
      var elem='<li>'+tipo_nombre+' >> '+act_titulo+
        '<span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">'+
        'Eliminar'
        +'</span>'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][categoria_actividad_id]" value="'+tipo_id+'">'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][tipo_nombre]" value="'+tipo_nombre+'">'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][titulo]" value="'+act_titulo+'"></li>';
      $('#actividades-agregadas').append(elem);
      $('.span-actividad').bind('click', function() {
        $(this).parent().remove();
        return false;
      });
      $('#div-agregadas').removeClass('hide');
      return false;
    });

 var valor=$('#Tarea_categoria_id option:selected').val();
       if(valor==1)
       {
        $('#clasificacion_administrativa').show();
        $('#clasificacion_administrativa').attr('name','Tarea[clasificacion]');
        $('#fieldset_caracteristicas').show();
        $('#clasificacion_tecnica').hide();
        $('#clasificacion_tecnica').attr('name','fail');
       }
       if(valor==2)
       {
        $('#clasificacion_tecnica').show();
        $('#clasificacion_tecnica').attr('name','Tarea[clasificacion]');
        $('#clasificacion_administrativa').hide();
        $('#clasificacion_administrativa').attr('name','fail');
        $('#fieldset_caracteristicas').hide();
       }
     $('#Tarea_categoria_id').change(function(){
       var valor=$('#Tarea_categoria_id option:selected').val();
       if(valor==1)
       {
        $('#clasificacion_administrativa').show();
        $('#clasificacion_administrativa').attr('name','Tarea[clasificacion]');
        $('#clasificacion_tecnica').hide();
        $('#clasificacion_tecnica').attr('name','fail');
        $('#fieldset_caracteristicas').show();
       }
       if(valor==2)
       {
        $('#clasificacion_tecnica').show();
        $('#clasificacion_tecnica').attr('name','Tarea[clasificacion]');
        $('#clasificacion_administrativa').hide();
        $('#clasificacion_administrativa').attr('name','fail');
        $('#fieldset_caracteristicas').hide();
       }
     });
    
    $('#actividad-form').submit(function(){
       var iniciativa=$('#iniciativa').find(':selected').val();
       var proyecto=$('#proyecto').find(':selected').val();
       
       if(proyecto==undefined)
       {
         $('#entidad_id').val(iniciativa);
         $('#tipo_entidad').val('iniciativa');
       }
       else
       {
         $('#entidad_id').val(proyecto);
         $('#tipo_entidad').val('proyecto');
       }
     });
     
     $('.agregada-anterior').click(function(){
      var id=$(this).attr('data-id');
      $(this).parent().append('<input type="hidden" name="EliminarCaracteristica['+id+'][id]" value="'+id+'">');
      $(this).parent().addClass('hide');
     });
     
     $('#agregar-archivo').click(function(){
      var total= $('#archivos-agregados').children().length;
      var elem='<p class="archivos-actividad" data-id="'+total+'">'+
            'Archivo: <input type="file" name="Tarea[archivos][]">'+
            '<span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>'+
      '</p>';
      $('#archivos-agregados').append(elem);
      return false;
     });
     
     $('.archivo-anterior').click(function(){
      var id=$(this).attr('data-id');
      $(this).parent().append('<input type="hidden" name="EliminarArchivo['+id+'][id]" value="'+id+'">');
      $(this).parent().addClass('hide');
     });
});
</script>

<?php Yii::app()->clientScript->registerScript('trigger_nivel3', 
  "jQuery(function($) {
  $('body').on('change','#nivel_1',function(){
     jQuery.ajax({'type':'POST','url':'".CController::createUrl('utils/obtenerCategorias1')."','cache':false,
     'data':jQuery(this).parents('form').serialize(),
     'success':function(html){
      jQuery('#nivel_2').html(html);
      //se actualiza tambien el nivel_3
      $('#nivel_2').trigger('change');
     }
     });
     return false;});
  $('body').on('change','#nivel_2',function(){
     jQuery.ajax({'type':'POST','url':'".CController::createUrl('utils/obtenerCategorias2')."','cache':false,
     'data':jQuery(this).parents('form').serialize(),
     'success':function(html){
      jQuery('#nivel_3').html(html);
      //se actualiza tambien el nivel_3
      $('#nivel_3').trigger('change');
     }
     });
     return false;});
  });", 
CClientScript::POS_END);?>