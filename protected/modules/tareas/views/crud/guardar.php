<?php
$this->pageTitle = 'Editar Actividad';
$this->breadcrumbs=array(
	'Actividades'=>array('index'),
	'Editar actividad',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Editar actividad</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-iniciativa">

    <div class="form">

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
      'id'=>'actividad-form',
      'type'=>'nice',
      'htmlOptions'=>array('enctype'=>'multipart/form-data')
    )); ?>
    
      <?php echo $form->errorSummary($tarea); ?>
      <?php echo $form->errorSummary($original); ?>

      <div class="row">
        <div class="six columns">
          Iniciativa:
          <?php echo CHtml::dropDownList('iniciativa','', 
               CHtml::listData(Persona::model()->findByPk(Yii::app()->user->id)->iniciativas, 'id', 'nombre_abreviado'),
            array('style'=>'width:90%;','size'=>'8')); 
          ?> 
        </div>
        
        <div class="six columns">
          Proyecto:
          <?php echo CHtml::dropDownList('proyecto','', 
               CHtml::listData(Persona::model()->findByPk(Yii::app()->user->id)->proyectos, 'id', 'nombre'),
            array('style'=>'width:90%;','size'=>'8')); 
          ?> 
        </div>
        
        <input type="hidden" name="TareaTmp[entidad_id]" id="entidad_id">
        <input type="hidden" name="TareaTmp[tipo_entidad]" id="tipo_entidad">
      </div>
      
      <div class="row">
        <div class="six columns">
          <?php echo $form->textFieldRow($tarea,"titulo"); ?> 
        </div>
        
        <div class="six columns">
          <?php echo $form->dropDownListRow($tarea, 'categoria_id',
                CHtml::listData($tarea->categorias, 'id', 'nombre')
            ,array('style'=>'height: 28px;')); ?> 
        </div>
      </div>
      
      <p>
        <label for="integrantes" class="required">
          Integrantes: <span class="required">*</span>
        </label>
        <select id="integrantes" name="TareaTmp[integrantes]">
          <?php foreach($tarea->participantes as $participante):?>
            <option value="<?php echo $participante->user_id;?>" class="selected"><?php echo $participante->usuario->nombreCompleto;?></option>
          <?php endforeach;?>
        </select>
      </p>      
      
      <div class="row">
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_inicio'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_inicio',
            'options' => array('dateFormat'=>'dd-mm-yy'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_inicio'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->labelEx($tarea,'fecha_termino'); ?>
          <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model'=>$tarea,
            'attribute'=>'fecha_termino',
            'options' => array('dateFormat'=>'dd-mm-yy'),
            'language'=>'es',
            'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
            ));
          ?>
          <div class="form-field error">
            <?php echo $form->error($tarea,'fecha_termino'); ?>
          </div>
        </div>
        
        <div class="four columns">
          <?php echo $form->textFieldRow($tarea,"duracion",array('style'=>'width:150px;'));?>
        </div>
      </div>
      
      <fieldset>
        <h5>Caracter&iacute;sticas</h5>
        <p>Selecciona las caracter&iacute;sticas de la actividad a ingresar e ingresa un t&iacute;tulo de ser necesario.</p>
        
        <p style="margin-bottom:3px;">Caracter&iacute;stica:</p>
        <div class="row">
          <div class="four columns">
            <?php
              echo CHtml::dropDownList('nivel_1','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=1'), 'id', 'nombre'),
              array('style'=>'height:28px;width:180px;')); 
            ?>
          </div>
          
          <div class="four columns">
            <?php
              echo CHtml::dropDownList('nivel_2','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=2 and id_padre=1'), 'id', 'nombre'),
              array(
              'ajax' => array(
              'type'=>'POST',
              'url'=>CController::createUrl('utils/obtenerCategorias2'),
              'update'=>'#nivel_3',
              ),'style'=>'height:28px;width:180px;'));
            ?>
          </div>
          
          <div class="four columns">
            <?php
              echo CHtml::dropDownList('nivel_3','', 
                   CHtml::listData(CategoriaActividad::model()->findAll('nivel=3 and id_padre=3'), 'id', 'nombre'),
                   array('style'=>'height:28px;width:180px;')
              );
            ?>
          </div>
        </div>
        
        <div class="row">
          <div class="eight columns">
            T&iacute;tulo: <input type="text" name="titulo_actividad" id="titulo_actividad" class="input-text" style="width:420px;">
          </div>
          
          <div class="four columns">
            <a href="#" id="agregar-actividad-select" class="nice small radius green button" style="margin-top:19px;">Agregar</a>
          </div>
        </div>
        
        <div id="div-agregadas" class="row">
          <div class="twelve columns">
            <h5>Caracter&iacute;sticas agregadas</h5>
            <ul id="actividades-agregadas">
              <?php foreach($tarea->caracteristicas as $caracteristica):?>
                <li>
                  <?php echo $caracteristica->titulo;?>
                  <span class="span-actividad agregada-anterior red label" data-id="<?php echo $caracteristica->id;?>" style="cursor:pointer;float:right;margin-right:20px">Eliminar</span>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      
      </fieldset>
      
      <p>
        Descripci&oacute;n: *<br>
        <?php $this->widget('application.extensions.redactorjs.Redactor', 
          array( 'lang' => 'en', 'toolbar' => 'mini', 'model' => $tarea, 'attribute' => 'descripcion' ));?>
      </p>
      <div class="row buttons">
        <?php echo CHtml::submitButton('Crear actividad',array('class'=>'nice radius medium button','id'=>'crear-actividad')); ?>
        <?php echo CHtml::submitButton('Guardar',array('class'=>'nice radius white medium button','id'=>'guardar-actividad')); ?>
      </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->
  </li>
</ul>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.fcbkcomplete.min.js', CClientScript::POS_HEAD);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/style.css');?>
<script type="text/javascript">
$(document).ready(function(){                
    $("#integrantes").fcbkcomplete({
        json_url: '<?php echo Yii::app()->createUrl('/usuarios/userActions/buscarIntegrante',array('mode'=>'json'));?>',
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        filter_selected:true,
        select_all_text: "seleccionar",
    });
    
    $('#agregar-actividad-select').click(function(){
      //se debe obtener el tipo de actividad
      var tipo_nombre=$('#nivel_3').find(':selected').text();
      var tipo_id=$('#nivel_3').find(':selected').val();
      var act_titulo=$('#titulo_actividad').val();
      var index=$('#actividades-agregadas li:last-child').index();
      if(index==-1)
        index=0;
      else
        index+=1;
      var elem='<li>'+tipo_nombre+' >> '+act_titulo+
        '<span class="span-actividad red label" style="cursor:pointer;float:right;margin-right:20px">'+
        'Eliminar'
        +'</span>'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][categoria_actividad_id]" value="'+tipo_id+'">'+
        '<input type="hidden" name="CategoriaActividadHasTarea['+index+'][titulo]" value="'+act_titulo+'"></li>';
      $('#actividades-agregadas').append(elem);
      $('.span-actividad').bind('click', function() {
        $(this).parent().remove();
        return false;
      });
      $('#div-agregadas').removeClass('hide');
      return false;
    });
    
    $('#actividad-form').submit(function(){
       var iniciativa=$('#iniciativa').find(':selected').val();
       var proyecto=$('#proyecto').find(':selected').val();
       
       if(proyecto==undefined)
       {
         $('#entidad_id').val(iniciativa);
         $('#tipo_entidad').val('iniciativa');
       }
       else
       {
         $('#entidad_id').val(proyecto);
         $('#tipo_entidad').val('proyecto');
       }
     });
     
     $('#crear-actividad').click(function(){
      $('#actividad-form').attr('action','<?php echo Yii::app()->createUrl('/tareas/crud/crearEditada',array('id'=>$tarea->id));?>');
     });
     
     $('.agregada-anterior').click(function(){
      var elem=$(this);
      var id=$(this).attr('data-id');
      jQuery.ajax({
        'type':'POST',
        'url':'<?php echo Yii::app()->createUrl('/tareas/gestionActividades/eliminarCaracteristica');?>',
        'cache':false,
        'data':{'id':id},
        'success':function(html){
          alert(html);
          $(elem).parent().remove();
       }
       });
       return false;
     });
    
});
</script>

<?php Yii::app()->clientScript->registerScript('trigger_nivel3', 
  "jQuery(function($) {
  $('body').on('change','#nivel_1',function(){
     jQuery.ajax({'type':'POST','url':'".CController::createUrl('utils/obtenerCategorias1')."','cache':false,
     'data':jQuery(this).parents('form').serialize(),
     'success':function(html){
      jQuery('#nivel_2').html(html);
      //se actualiza tambien el nivel_3
      $('#nivel_2').trigger('change');
     }
     });
     return false;});
  });", 
CClientScript::POS_END);?>