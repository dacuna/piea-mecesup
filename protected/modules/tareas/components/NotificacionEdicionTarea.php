<?php
/**
 * Notificacion que alerta sobre que una tarea en la que el receptor es participante ha sido editada por lo
 * que debe ser nuevamente aprobada.
 *
 * @package   modules.tareas.components
 * @version   1.0
 * @since     2012-08-11
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo Con la refactorizacion del MailMan ya no se necesitan este tipo de clases. Refactorizar para usar el sistema de eventos.
 */
class NotificacionEdicionTarea extends CComponent
{
  public $integrantes;
  public $tarea_id;
  public $tipo_entidad;
  public $entidad_id;
  
  public function enviarNotificacion()
  {
    $mailman=Yii::app()->mailman;
    if($this->tipo_entidad=='iniciativa')
    {
      $iniciativa=Iniciativa::model()->findByPk($this->entidad_id);
      $asunto='Edici&oacute;n de actividad en '.$iniciativa->nombre_abreviado;
    }
    else
    {
      $proyecto=Proyecto::model()->findByPk($this->entidad_id);
      $asunto='Edici&oacute;n de actividad en '.$proyecto->nombre;
    }
    $mailman->crearMensaje($asunto,'temp');
    $mailman->setPublisher(Yii::app()->user->id);
    $mailman->setSuscriber($this->integrantes);
    if($this->tipo_entidad=='iniciativa')
      $mailman->setRelation($iniciativa->id,'iniciativa');
    else
      $mailman->setRelation($proyecto->id,'proyecto');
    $id_mensajes=$mailman->enviarMensaje();
    if(is_array($id_mensajes))
    {
      foreach($id_mensajes as $id_mensaje)
      {
        $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
      }
    }
    else
      $mailman->actualizarEnviados($this->mensaje($id_mensajes));
  }
  
  public function mensaje($id)
  {
    if($this->tipo_entidad=='iniciativa')
    {
      $entidad=Iniciativa::model()->findByPk($this->entidad_id);
      $nombre=$entidad->nombre_abreviado;
    }
    else
    {
      $entidad=Proyecto::model()->findByPk($this->entidad_id);
      $nombre=$entidad->nombre;
    }
    $html = Yii::app()->controller->renderPartial('/notificaciones/_editar_tarea',
      array('nombre'=>$nombre,'id'=>$id,'tarea_id'=>$this->tarea_id),true);

    return $html;
  }

}