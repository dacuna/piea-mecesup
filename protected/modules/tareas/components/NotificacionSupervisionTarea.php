<?php
/**
 * Notificacion que alerta sobre que una tarea ha sido aceptada/rechazada.
 *
 * @package   modules.tareas.components
 * @version   1.0
 * @since     2012-08-11
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo Con la refactorizacion del MailMan ya no se necesitan este tipo de clases. Refactorizar para usar el sistema de eventos.
 */
class NotificacionSupervisionTarea extends CComponent
{
  private $tarea;
  private $accion;
  
  public function redactar($tarea,$accion)
  {
    $this->tarea=$tarea;
    $this->accion=$accion;
  }
  
  public function enviarNotificacion()
  {
    $tarea=$this->tarea; //shortcut
    $mailman=Yii::app()->mailman;
    if($tarea->tipo_entidad=='iniciativa')
    {
      $iniciativa=Iniciativa::model()->findByPk($tarea->entidad_id);
      if($this->accion=='aprobar')
        $asunto='Actividad aprobada en iniciativa '.$iniciativa->nombre_abreviado;
      else
        $asunto='Actividad rechazada en iniciativa '.$iniciativa->nombre_abreviado;
    }
    else
    {
      $proyecto=Proyecto::model()->findByPk($tarea->entidad_id);
      if($this->accion=='aprobar')
        $asunto='Actividad aprobada en proyecto '.$proyecto->nombre;
      else
        $asunto='Actividad rechazada en proyecto '.$proyecto->nombre;
    }
    $mailman->crearMensaje($asunto,'temp');
    $mailman->setPublisher(Yii::app()->user->id);
    
    //participantes
    $integrantes=array();
    foreach($tarea->participantes as $participante)
    {
      array_push($integrantes,$participante->user_id);
    }
    $mailman->setSuscriber($integrantes);
    if($tarea->tipo_entidad=='iniciativa')
      $mailman->setRelation($iniciativa->id,'iniciativa');
    else
      $mailman->setRelation($proyecto->id,'proyecto');
    $id_mensajes=$mailman->enviarMensaje();
    if(is_array($id_mensajes))
    {
      foreach($id_mensajes as $id_mensaje)
      {
        $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
      }
    }
    else
      $mailman->actualizarEnviados($this->mensaje($id_mensajes));
  }
  
  public function mensaje($id)
  {
    if($this->tarea->tipo_entidad=='iniciativa')
    {
      $entidad=Iniciativa::model()->findByPk($this->tarea->entidad_id);
      $nombre=$entidad->nombre_abreviado;
    }
    else
    {
      $entidad=Proyecto::model()->findByPk($this->tarea->entidad_id);
      $nombre=$entidad->nombre;
    }
    $html = Yii::app()->controller->renderPartial('/notificaciones/_notificacion_supervision',
      array('nombre'=>$nombre,'id'=>$id,'tarea'=>$this->tarea,'tipo_entidad'=>$this->tarea->tipo_entidad,
        'accion'=>$this->accion),true);

    return $html;
  }

}