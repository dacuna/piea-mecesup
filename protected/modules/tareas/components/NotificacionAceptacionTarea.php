<?php
/**
 * Notificacion que alerta sobre que una tarea debe ser aprobada por el usuario receptor de la
 * notificacion.
 *
 * @package   modules.tareas.components
 * @version   1.0
 * @since     2012-08-11
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo Con la refactorizacion del MailMan ya no se necesitan este tipo de clases. Refactorizar para usar el sistema de eventos.
 */
class NotificacionAceptacionTarea extends CComponent
{
  public $integrantes;
  public $tarea_id;
  public $tipo_entidad;
  public $entidad_id;
  
  public function redactar($tarea_id,$tipo_entidad,$entidad_id)
  {
    $this->tarea_id=$tarea_id;
    $this->tipo_entidad=$tipo_entidad;
    $this->entidad_id=$entidad_id;
  }
  
  public function enviarNotificacion()
  {
    $mailman=Yii::app()->mailman;
    if($this->tipo_entidad=='iniciativa')
    {
      $iniciativa=Iniciativa::model()->findByPk($this->entidad_id);
      $asunto='Solicitud aprobaci&oacute;n actividad en '.$iniciativa->nombre_abreviado;
      //obtengo el coordinador
      $connection=Yii::app()->db; 
      $command=$connection->createCommand(
      'SELECT user_id FROM {{lookup_iniciativa}} WHERE nombre_item="coordinador_iniciativa" AND iniciativa_id='.$this->entidad_id);
      $row=$command->queryRow(); 
      $this->integrantes=$row['user_id'];
    }
    else
    {
      $proyecto=Proyecto::model()->findByPk($this->entidad_id);
      $asunto='Solicitud aprobaci&oacute;n actividad en '.$proyecto->nombre;
      //obtengo el jefe de proyecto
      $connection=Yii::app()->db; 
      $command=$connection->createCommand(
      'SELECT user_id FROM {{lookup_proyecto}} WHERE nombre_item="jefe_proyecto" AND proyecto_id='.$this->entidad_id);
      $row=$command->queryRow();
      //verifico si es que hay un jefe de proyecto, sino entonces la invitacion se envia al coordinador del proyecto
        if($row==false)
      {
          $delegacion=new DelegacionTarea;
          $delegacion->proyecto=$proyecto;
          $delegacion->id_tarea=$this->tarea_id;
          $delegacion->id_iniciativa=$proyecto->iniciativa->id;
          if($delegacion->save())
              return;
      }
      else
        $this->integrantes=$row['user_id'];
    }
    $mailman->crearMensaje($asunto,'temp');
    $mailman->setPublisher(Yii::app()->user->id);
    $mailman->setSuscriber($this->integrantes);
    $mailman->setTarea($this->tarea_id);
    $mailman->setAccion('aprobar_superior');
    if($this->tipo_entidad=='iniciativa')
      $mailman->setRelation($iniciativa->id,'iniciativa');
    else
      $mailman->setRelation($proyecto->id,'proyecto');
    $id_mensajes=$mailman->enviarMensaje();
    if(is_array($id_mensajes))
    {
      foreach($id_mensajes as $id_mensaje)
      {
        $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
      }
    }
    else
      $mailman->actualizarEnviados($this->mensaje($id_mensajes));
  }
  
  public function mensaje($id)
  {
    if($this->tipo_entidad=='iniciativa')
    {
      $entidad=Iniciativa::model()->findByPk($this->entidad_id);
      $nombre=$entidad->nombre_abreviado;
    }
    else
    {
      $entidad=Proyecto::model()->findByPk($this->entidad_id);
      $nombre=$entidad->nombre;
    }
    $html = Yii::app()->controller->renderPartial('/notificaciones/_notificar_cargo_alto',
      array('nombre'=>$nombre,'id'=>$id,'tarea_id'=>$this->tarea_id,'tipo_entidad'=>$this->tipo_entidad),true);

    return $html;
  }

}