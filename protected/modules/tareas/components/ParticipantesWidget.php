<?php
/**
 * Renderiza el widget que contiene los participantes de una actividad.
 *
 * @package   modules.tareas.components
 * @example <br />
 *  //en una vista
 *  $this->widget('ParticipantesWidget',array('participantes'=>$tarea->participantes));
 * @version   1.0
 * @since     2012-08-05
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ParticipantesWidget extends CWidget
{
    /**
     * Participantes de la tarea
     * @var array
     */
    public $participantes = array();

    /**
     * Despliega el widget actual.
     */
    public function run()
    {
        $this->render('tareas.views.widget.participantesWidget', array('participantes' => $this->participantes));
    }
}