<?php
/**
 * Renderiza el widget que contiene los acciones que se pueden realizar en una actividad.
 *
 * @package   modules.tareas.components
 * @example <br />
 *  //en una vista
 *  $this->widget('AccionesActividadWidget',array('tarea'=>$tarea));
 * @version   1.0
 * @since     2012-08-05
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AccionesActividadWidget extends CWidget
{
    /**
     * Tarea desplegada
     * @var array
     */
    public $tarea;

    /**
     * Despliega el widget actual.
     */
    public function run()
    {
        $this->render('tareas.views.widget.accionesActividadWidget', array('tarea' => $this->tarea));
    }
}