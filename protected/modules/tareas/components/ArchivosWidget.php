<?php
/**
 * Renderiza el widget que contiene los archivos de una actividad.
 *
 * @package   modules.tareas.components
 * @example <br />
 *  //en una vista
 *  $this->widget('ArchivosWidget',array('archivos'=>$tarea->archivos));
 * @version   1.0
 * @since     2012-08-08
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ArchivosWidget extends CWidget
{
    /**
     * Archivos de la tarea
     * @var array
     */
    public $archivos = array();

    /**
     * Despliega el widget actual.
     */
    public function run()
    {
        $this->render('tareas.views.widget.archivosWidget', array('archivos' => $this->archivos));
    }
}