<?php

/**
 * Modelo para la tabla "{{lookup_proyecto}}". Este modelo representa un permiso que tiene un
 * usuario dentro de un proyecto. Es un add-on para manejar de manera mas sencilla el sistema
 * de permisos del framework Yii. Vease la documentacion tecnica en seccion "Autorizacion y
 * autenticacion".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{lookup_proyecto}}':
 * @property integer $nombre_item
 * @property integer $proyecto_id
 * @property integer $user_id
 * @property integer $activo
 * @property string  $cargo_que_asigno
 * @property integer $es_cargo
 * @property integer $asignacion_nula
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $proyecto
 * @property User $user
 */
class LookupProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LookupProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{lookup_proyecto}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'proyecto_id'),
			'usuario' => array(self::BELONGS_TO, 'Persona', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nombre_item' => 'Nombre Item',
			'proyecto_id' => 'Proyecto',
			'user_id' => 'Usuario',
		);
	}

    /**
     * Agregar un lookup de un permiso a un usuario en un proyecto en particular.
     *
     * @param string $nombre_item permiso a asignar
     * @param int $user_id ID del usuario
     * @param int $proyecto_id ID del proyecto
     *
     * @return  mixed LookupProyecto si fue creado con exito, null en caso contrario
     */
    public static function crearLookupProyecto($nombre_item,$user_id,$proyecto_id){
        $lookup=LookupProyecto::model()->find('nombre_item=:ni and proyecto_id=:ii and user_id=:ui',array(
            ':ni'=>$nombre_item,
            ':ii'=>$proyecto_id,
            ':ui'=>$user_id
        ));
        //ya existe, con 99% de probabilidad este cargo esta inactivo, se debe activar
        if($lookup!=null){
            $lookup->activo=1;
            if($lookup->save())
                return $lookup;
        }
        //sino, debo crearlo
        $integrante=new LookupProyecto();
        $integrante->nombre_item=$nombre_item;
        $integrante->proyecto_id=$proyecto_id;
        $integrante->user_id=$user_id;
        if($integrante->save())
            return $integrante;
        return null;
    }

    /**
     * Retorna un listado con los cargos de un usuario en todos sus proyectos.
     *
     * @param int $user_id ID del usuario
     *
     * @return  array array de LookupProyecto
     */
    public static function getCargosDeUsuario($user_id){
        return LookupProyecto::model()->findAll('user_id=:ui and activo=1 and es_cargo=1',array(
            ':ui'=>$user_id
        ));
    }

}
