<?php

/**
 * Modelo para la tabla "{{invitacion}}". Permite gestionar invitaciones a ser jefe de proyecto
 * en un proyecto en particular.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{invitacion}}':
 * @property integer $mensaje_id
 * @property integer $usuario_id
 * @property integer $usuario_invito_id
 * @property integer $entidad_id
 * @property string  $tipo_invitacion
 * @property integer $tipo_entida => 0 iniciativa, 1 proyecto
 * @property string $aceptada
 * @property string $fecha_invitacion
 * @property string $fecha_aceptada
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $usuario
 * @property Mensaje $mensaje
 */
class InvitacionProyecto extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Invitacion the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{invitacion}}';
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'usuario' => array(self::BELONGS_TO, 'User', 'usuario_id'),
            'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'entidad_id'),
            'mensaje' => array(self::BELONGS_TO, 'Mensaje', 'mensaje_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'mensaje_id' => 'Mensaje',
            'usuario_id' => 'Usuario',
            'entidad_id' => 'Entidad',
            'tipo_entidad' => 'Tipo Entidad',
            'aceptada' => 'Aceptada',
            'fecha_invitacion' => 'Fecha Invitacion',
            'fecha_aceptada' => 'Fecha Aceptada',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('mensaje_id', $this->mensaje_id);
        $criteria->compare('usuario_id', $this->usuario_id);
        $criteria->compare('entidad_id', $this->entidad_id);
        $criteria->compare('tipo_entidad', $this->tipo_entidad);
        $criteria->compare('aceptada', $this->aceptada, true);
        $criteria->compare('fecha_invitacion', $this->fecha_invitacion, true);
        $criteria->compare('fecha_aceptada', $this->fecha_aceptada, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Behaviors del modelo.
     *
     * @return  array behaviors del modelo
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_invitacion',
                'updateAttribute' => 'fecha_aceptada',
            )
        );
    }

    /**
     * Callback para el evento beforeSave. Si el registro es nuevo entonces se setea la variable de si
     * la invitacion fue aceptada o no a 0 (no se sabe aun).
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario.
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord)
                $this->aceptada = 0; //0 es neutro, -1 rechazada, 1 aceptada
            return true;
        } else
            return false;
    }

    /**
     * Permite saber si es que existe una invitacion en curso para ser coordinador o jefe de proyecto en una
     * entidad en particular. Esto debido a que solo puede haber una en curso en cada momento.
     *
     * @param int $tipo_invitacion tipo de la invitacion 0=coordinador iniciativa, 1=jefe de proyecto
     * @param int $entidad_id ID de la entidad
     * @param int $tipo_entidad tipo de la entidad, 0=iniciativa, 1=proyecto.
     *
     * @return  bool true si es que existe, false caso contrario
     */
    public static function existeInvitacionEnCurso($tipo_invitacion, $entidad_id, $tipo_entidad)
    {
        $invitaciones = InvitacionProyecto::model()->findAll('tipo_invitacion=:ti and entidad_id=:ei and tipo_entidad=:te', array(
            ':ti' => $tipo_invitacion,
            ':ei' => $entidad_id,
            ':te' => $tipo_entidad
        ));

        foreach ($invitaciones as $invitacion) {
            //si hay alguna invitacion que no esta ni rechazada ni aceptada entonces hay una invitacion pendiente
            if ($invitacion->aceptada == 0)
                return true;
        }
        return false;
    }
}