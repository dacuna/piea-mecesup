<?php
Yii::import('application.modules.iniciativas.models.LookupIniciativa');
/**
 * Clase que implementa metodos para el modelo @link{Proyecto} que solo son necesarios en
 * el modulo de proyectos.
 *
 * Se decidio extender la clase Proyecto con el objetivo de no contaminar la clase general de
 * proyectos con metodos que no son necesarios de ser compartidos en otros modulos.
 *
 * @category  Controllers
 * @package   modules.proyectos.models
 * @version   1.0
 * @since     2012-07-12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ProyectoModel extends Proyecto
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Proyecto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('nombre, sigla, descripcion, campus, fecha_inicio', 'required'),
            array('iniciativa_id', 'numerical', 'integerOnly' => true, 'on' => 'insert'),
            array('iniciativa_id', 'exist', 'className' => 'Iniciativa', 'attributeName' => 'id', 'allowEmpty' => false, 'on' => 'insert'),
            array('nombre', 'length', 'max' => 255),
            array('sigla', 'length', 'max' => 10),
            //hay dos validadores de sigla ya que las siglas son unicas tanto para iniciativas como para proyectos
            array('sigla', 'unique', 'className' => 'Iniciativa', 'attributeName' => 'sigla'),
            array('sigla', 'unique'),
            array('campus', 'length', 'max' => 40),
            array('objetivos_generales, objetivos_especificos', 'safe'),
            // The following rule is used by search().
            array('iniciativa_id,nombre,sigla,campus', 'safe', 'on' => 'search'),
        );
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('iniciativa_id', $this->iniciativa_id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('sigla', $this->sigla, true);
        $criteria->compare('campus', $this->campus, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente setea la fecha de inicio
     * del proyecto y genera un rol piea.
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (isset($this->fecha_inicio) && !empty($this->fecha_inicio))
                $this->fecha_inicio = date('Y-m-d', strtotime($this->fecha_inicio));
            if ($this->isNewRecord)
                $this->rol_piea = RolPiea::model()->generarRol();
            return true;
        } else
            return false;
    }

    /**
     * Handler para el evento afterSave del modelo. Basicamente actualiza el generador de roles PIEA.
     */
    public function afterSave()
    {
        parent::afterSave();
        if ($this->isNewRecord)
            RolPiea::model()->actualizarGenerador();
    }

    /**
     * Retorna el usuario que es jefe del proyecto actual.
     *
     * @return  mixed @link{Persona} si existe jefe de proyecto, null en caso contrario
     */
    public function getJefe_proyecto()
    {
        //se busca el jefe de proyecto de esta iniciativa en la tabla tbl_lookup_proyecto
        $lookup = LookupProyecto::model()->find('nombre_item=:n_item and proyecto_id=:iid and activo=1', array(
            ':n_item' => 'jefe_proyecto', ':iid' => $this->id
        ));

        if ($lookup != null)
            return $lookup->usuario;

        return null;
    }

    /**
     * getCargoDeUsuario: Obtiene el cargo que posee el usuario de id $user_id dentro del proyecto
     * especificado por $proyecto_id.
     *
     * @param   int  $proyecto_id ID del proyecto donde se busca el cargo del usuario
     * @param   int  $user_id     ID del usuario al que se le busca el cargo dentro de la iniciativa especificada
     *
     * @return  Mixed  String con el nombre del cargo en caso de que el usuario posea un cargo, null en caso contrario.
     *
     * @since   16-10-2012
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public static function getCargoDeUsuario($proyecto_id, $user_id)
    {
        $lookup = LookupProyecto::model()->find('proyecto_id=:ii and user_id=:ui and activo=1 and es_cargo=1',
            array(':ii' => $proyecto_id, ':ui' => $user_id));
        if ($lookup != null)
            return $lookup;
        return null;
    }

    /**
     * Retorna un array con los cargos de todos los usuarios que son integrantes del proyecto.
     *
     * @return  array array con los cargos de los usuarios
     */
    public function getCargos()
    {
        $cargos = array();
        foreach ($this->usuarios as $integrante) {
            $cargo = ProyectoModel::getCargoDeUsuario($this->id, $integrante->id);
            if ($cargo != null) {
                $item = am()->getAuthItem($cargo->nombre_item);
                array_push($cargos, array($item, $cargo));
            }
        }
        //obtengo tambien los cargos asignados a personas nulas
        $nulos = LookupProyecto::model()->findAll('proyecto_id=:ii and asignacion_nula=1', array(':ii' => $this->id));
        foreach ($nulos as $nulo) {
            $item = am()->getAuthItem($nulo->nombre_item);
            array_push($cargos, array($item, $nulo));
        }
        return $cargos;
    }

    /**
     * Carga un modelo Proyecto. Si no existe entonces se lanza una exception.
     *
     * @param int $id ID del proyecto
     *
     * @return  \Proyecto
     * @throws  \CHttpException
     */
    public static function load($id)
    {
        $proyecto = ProyectoModel::model()->findByPk($id);
        if ($proyecto == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $proyecto;
    }

    /**
     * Permite asignar a un usuario en particular como jefe del proyecto actual.
     *
     * @param int $usuario_id ID del usuario
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function asignarJefeProyecto($usuario_id)
    {
        $transaction = app()->db->beginTransaction();
        try {
            $auth = Yii::app()->authManager;
            //verifico que no sea coordinador de otro proyecto, si lo es entonces ya tiene el permiso asignado
            if ($auth->getAuthAssignment("jefe_proyecto", $usuario_id) == null) {
                $admin = $auth->assign('jefe_proyecto', $usuario_id);
            }
            //Un jefe de proyecto tambien debe ser integrante de la iniciativa correspondiente
            if ($auth->getAuthAssignment("integrante_iniciativa", $usuario_id) == null) {
                $admin = $auth->assign('integrante_iniciativa', $usuario_id);
            }

            //seteo el lookup del proyecto correspondiente
            $lookup = new LookupProyecto;
            $lookup->nombre_item = 'jefe_proyecto';
            $lookup->proyecto_id = $this->id;
            $lookup->user_id = $usuario_id;
            $lookup->save(false);

            $lok = LookupIniciativa::model()->find("nombre_item='integrante_iniciativa' and iniciativa_id=:iid and user_id=:uid", array(
                ':iid' => $this->iniciativa_id, ':uid' => $usuario_id
            ));
            if ($lok == null) {
                $lookup = new LookupIniciativa;
                $lookup->nombre_item = 'integrante_iniciativa';
                $lookup->iniciativa_id = $this->iniciativa_id;
                $lookup->user_id = $usuario_id;
                $lookup->save(false);

                $integrante_ini = new IntegranteIniciativa;
                $integrante_ini->iniciativa_id = $this->iniciativa_id;
                $integrante_ini->user_id = $usuario_id;
                $integrante_ini->save(false);
            }
            //se debe tambien ingresar el integrante nuevo
            $integrante = IntegranteProyecto::crearIntegrante($usuario_id, $this->id);
            $transaction->commit();
            return $integrante;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }
}