<?php
Yii::import('application.modules.usuarios.components.Entidad');

/**
 * InvitarCoordinadorForm class, formulario para invitar a un usuario
 * a ser jefe de un proyecto.
 *
 * Provee el formulario y metodos para invitar a un usuario a ser
 * el jefe de un proyecto. Realiza todas las validaciones
 * correspondientes sobre los modelos referenciados.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package proyectos.models
 * @since 1.0
 */
class InvitarJefeProyectoForm extends CFormModel
{
    /**
     * @var int id_usuario id del usuario a invitar.
     */
    public $id_usuario;

    /**
     * @var int id_proyecto id del proyecto al que se invita un jefe.
     */
    public $id_proyecto;

    
    /**
     * Inicializacion del modelo. Se adjunta el handler de notificaciones.
     *
     * @since   12-06-2013
     */
    public function init(){
        $this->onSendUserNotificacion=array(app()->mailman,'notificacionHandler');
        app()->mailman->onMensajeEnviado=array($this,"procesarInvitacion");
    }

    /**
     * Reglas de validacion para el modelo.
     * Las reglas corresponden a que el nombre de usuario y
     * password deben existir y ser una combinacion valida de
     * un usuario en la db.
     */
    public function rules()
    {
        return array(
            array('id_usuario,id_proyecto', 'numerical', 'allowEmpty' => false, 'integerOnly' => true, 'message' => 'Debes ingresar un {attribute} v&aacute;lido'),
            //verifico la existencia del usuario y de la proyecto enviadas
            array('id_usuario', 'exist', 'className' => 'Persona', 'attributeName' => 'id', 'allowEmpty' => false,
                'message' => $_POST['InvitarJefeProyectoForm']['nombre_usuario'] . ' no est&aacute; registrado en el sistema. Para invitar a ' . $_POST['InvitarJefeProyectoForm']['nombre_usuario'] . ' al sistema haga click en invitar persona.'),
            array('id_proyecto', 'exist', 'className' => 'Proyecto', 'attributeName' => 'id', 'allowEmpty' => false),
        );
    }

    /**
     * Labels de cada atributo del modelo.
     */
    public function attributeLabels()
    {
        return array(
            'id_usuario' => 'Usuario',
            'id_proyecto' => 'Proyecto'
        );
    }

    /**
     * Enviar la invitacion para ser jefe de proyecto (lo delega al evento correspondiente).
     */
    public function enviarInvitacion()
    {
        $proyecto = Proyecto::load($this->id_proyecto);
        $usuario = Persona::load($this->id_usuario);
        $entidad=new Entidad($this->id_proyecto, 1);
        //se envia la notificacion
        $this->onSendUserNotificacion(new CEvent($this,array('tipo'=>'invitacion_jefe_proyecto',
            'proyecto'=>$proyecto,'usuario'=>$usuario,'destinatario'=>$usuario->id,'entidad'=>$entidad)));
        //se espera la respuesta del envio del mensaje, luego se procesa la invitacion
    } 

    /**
     * Permite luego de haberse enviado la notificacion de invitar jefe de proyecto el procesar la invitacion
     * correspondiente dentro del sistema. Esta accion es utilizada como un callback luego de que se envia
     * la notificacion de invitacion.
     *
     * @param CEvent $event evento desde el que se llama esta accion
     *
     * @return  bool true si se proceso correctamente, false en caso contrario
     * @throws  \CHttpException
     */
    public function procesarInvitacion($event)
    {
        if($event->params['tipo']=='invitacion_jefe_proyecto'){
            //se crea la invitacion en la base de datos
            $invitacion = new InvitacionProyecto;
            $invitacion->mensaje_id = $event->params['mensaje']->primaryKey;
            $invitacion->usuario_id = $this->id_usuario;
            $invitacion->entidad_id = $this->id_proyecto;
            $invitacion->tipo_invitacion='jefe_proyecto';
            $invitacion->usuario_invito_id=user()->id;
            $invitacion->tipo_entidad = 1; //1 quiere decir proyecto
            Yii::log("Enviando invitacion a usuario ID {$invitacion->usuario_id} relacionada con el
                mensaje ID {$invitacion->mensaje_id}","trace","app.modules.proyectos.InvitarJefeProyectoForm");
            if(!$invitacion->save(false)){
                throw new CHttpException(500,"Error el enviar la invitacion");
                return false;
            }
            return true;
        }
    }    

    /**
     * Evento que es lanzado para enviar una notificacion a un usuario. En este modelo se utiliza
     * para enviar notificaciones de invitacion a usuarios.
     *
     * @param CEvent $event Evento a lanzar
     *
     * @return  void
     *
     * @since   12-06-2013
     */
    public function onSendUserNotificacion($event){
        $this->raiseEvent('onSendUserNotificacion',$event);
    }

}
