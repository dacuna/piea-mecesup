<?php
/**
 * CambiarJefeProyectoForm class, formulario para invitar a un usuario
 * a ser jefe de un proyecto.
 *
 * Provee el formulario y metodos para invitar a un usuario a ser
 * el jefe de un proyecto. Realiza todas las validaciones correspondientes
 * sobre los modelos referenciados.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package proyectos.models
 * @since 1.0
 */
class CambiarJefeProyectoForm extends CFormModel
{
    /**
     * @var int mensaje_id id del mensaje que contiene la notificacion que se envio al usuario
     *          que se invito a ser jefe. Permite almacenar la invitacion en la DB en
     *          respuesta al evento generado por el MailMan de mensaje enviado.
     */
    private $mensaje_id;

    /**
     * @var int id_usuario id del usuario a invitar.
     */
    public $id_usuario;

    /**
     * @var int id_proyecto id del proyecto al que se invita un jefe.
     */
    public $id_proyecto;

    /**
     * Inicializa el modelo cargando los callbacks para los eventos del MailMan.
     */
    public function init(){
        //handler para que el mailman envie la invitacion
        $this->onEnviarInvitacion = array(app()->mailman, 'notificacionHandler');
        //registro este componente para cuando mailman envie un mensaje
        app()->mailman->onMensajeEnviado=array($this,'notificacionEnviadaHandler');
    }

    /**
     * Reglas de validacion para el modelo.
     * Las reglas corresponden a que el nombre de usuario y
     * password deben existir y ser una combinacion valida de
     * un usuario en la db.
     */
    public function rules()
    {
        return array(
            array('id_usuario,id_proyecto', 'numerical', 'allowEmpty' => false, 'integerOnly' => true, 'message' => 'Debes ingresar un {attribute} v&aacute;lido'),
            //verifico la existencia del usuario y de la iniciativa enviadas
            array('id_usuario', 'exist', 'className' => 'Persona', 'attributeName' => 'id', 'allowEmpty' => false),
            array('id_proyecto', 'exist', 'className' => 'Proyecto', 'attributeName' => 'id', 'allowEmpty' => false),
        );
    }

    /**
     * Labels de cada atributo del modelo.
     */
    public function attributeLabels()
    {
        return array(
            'id_usuario' => 'Usuario',
            'id_proyecto' => 'Proyecto'
        );
    }

    /**
     * Funcion que se encarga de enviar la notificacion de invitacion y de posteriormente
     * procesar la invitacion dentro del sistema.
     */
    public function enviarInvitacion()
    {
        //lanzo el evento de que la invitacion se va a enviar
        $this->onEnviarInvitacion(new CEvent($this,array('tipo'=>'jefe_proyecto')));

        //se crea la invitacion en la base de datos
        $invitacion = new InvitacionProyecto;
        $invitacion->mensaje_id = $this->mensaje_id;
        $invitacion->usuario_id = $this->id_usuario;
        $invitacion->entidad_id = $this->id_proyecto;
        $invitacion->tipo_invitacion = 'jefe_proyecto';
        $invitacion->usuario_invito_id=user()->id;
        $invitacion->tipo_entidad = 1; //1 quiere decir proyecto
        $invitacion->save(false);

        //se suelta el handler
        app()->mailman->detachEventHandler('onMensajeEnviado','notificacionEnviadaHandler');
    }

    /**
     * Evento generado al enviar una invitacion.
     *
     * @param CEvent $event evento
     */
    public function onEnviarInvitacion($event){
        $this->raiseEvent('onEnviarInvitacion', $event);
    }

    /**
     * Evento generado al enviar una notificacion
     *
     * @param CEvent $event evento
     */
    public function notificacionEnviadaHandler($event){
        if($event->params['tipo']=='jefe_proyecto')
            $this->mensaje_id=$event->sender->mensaje->primaryKey;
    }

}
