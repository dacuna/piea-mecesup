<?php
/**
 * SolicitudMembresia: componente que maneja la logica de solicitar
 * una membresia en una iniciativa
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package iniciativas.components
 * @since 1.0
 * @todo Refactorizar y utilizar el mailman con eventos!
 */
class AvisoRechazoSolicitud extends CComponent
{
  public $rechazo;
  public $invito;
  public $proyecto;
  
  public function enviarSolicitud() 
  {
    $mailman=Yii::app()->mailman;
    $asunto='Rechazo de invitaci&oacute;n a iniciativa '.$this->proyecto->nombre;
    $mailman->crearMensaje($asunto,'temp');
    $mailman->setPublisher($this->rechazo->id);
    $mailman->setSuscriber($this->invito->id);
    $mailman->setAccion('answered_as_read');
    $mailman->setRelation($this->proyecto->id,'proyecto');
    $id_mensaje=$mailman->enviarMensaje();
    $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
  }
  
  public function mensaje($id)
  {
    $mensaje='Estimado '.$this->invito->nombrePresentacion.':<br><br>El usuario '.$this->rechazo->nombrePresentacion;
    $mensaje.=' ha rechazado la invitaci&oacute;n a ser parte del equipo de la iniciativa '.$this->proyecto->nombre.'.';
    $mensaje.='Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.';
    return $mensaje;
  }
  
}