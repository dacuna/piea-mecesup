<?php
/**
 * SolicitudMembresia: componente que maneja la logica de solicitar
 * una membresia en una iniciativa
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package iniciativas.components
 * @since 1.0
 * @todo Refactorizar y utilizar el mailman con eventos!
 */
class SolicitudMembresia extends CComponent
{
  public $usuario_id;
  public $proyecto;
  
  public function enviarSolicitud() {
    //el mensaje se le envia al usuario coordinador
    $coordinador=$this->proyecto->jefe_proyecto;
    if($coordinador!=null)
    {
      $mailman=Yii::app()->mailman;
      $asunto='Solicitud de membres&iacute;a iniciativa '.$this->proyecto->nombre;
      $mailman->crearMensaje($asunto,'temp');
      $mailman->setPublisher($this->usuario_id);
      $mailman->setSuscriber($coordinador);
      $mailman->setRelation($this->proyecto->id,'proyecto');
      $mailman->setAccion('solicitud_membresia');
      $id_mensaje=$mailman->enviarMensaje();
      $mailman->actualizarMensaje($id_mensaje,$this->mensaje($id_mensaje));
      return true;
    }
    else
      return false;
  }
  
  public function mensaje($id)
  {
    $usuario=Persona::model()->findByPk($this->usuario_id);
    $mensaje='Estimado jefe de proyecto <b>'.$this->proyecto->nombre.'</b>:<br><br>El usuario '.$usuario->nombreCompleto;
    $mensaje.=' ha solicitado ser parte del equipo de este proyecto.';
    $mensaje.='. Para aceptar la solicitud presiona en el bot&oacute;n "Aceptar solicitud"';
    $mensaje.=', para rechazarla presiona en "Rechazar solicitud".<br><br>';
    $mensaje.='Atte. Equipo Sistema de gesti&oacute;n y seguimiento I+D+i PIE&gt;A.<br><br>';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/proyectos/gestionIntegrantes/aceptarSolicitud',array('id'=>$id)).'" class="nice radius small button">Aceptar solicitud</a> ';
    $mensaje.='<a href="'.Yii::app()->createAbsoluteUrl('/proyectos/gestionIntegrantes/rechazarSolicitudMembresia',array('id'=>$id)).'" class="nice radius small red button">Rechazar solicitud</a> ';
    
    return $mensaje;
  }
  
}