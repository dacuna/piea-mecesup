<?php
$this->pageTitle = 'Ver Proyecto '.$model->nombre;
$this->breadcrumbs=array(
    'Proyectos'=>array('index'),
    'Ver Proyecto'=>array('/proyectos/crud/ver','id'=>$model->id),
    'Cambiar jefe de proyecto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="<?php echo url('/proyectos/crud/ver',array('id'=>$model->id));?>">Ver proyecto</a></dd>
    <dd><a href="<?php echo url('/tareas/actividadesEntidad/verActividades',array('id'=>$model->id,'tipo'=>1));?>">Actividades</a></dd>
    <dd><a href="<?php echo url('/proyectos/perfil/integrantes',array('id'=>$model->id));?>">Integrantes</a></dd>
    <dd><a href="<?php echo url('/proyectos/administracion/cambiarJefeProyecto',array('id'=>$model->id));?>" class="active">Cambiar jefe proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-jefe_proyecto">

        <h1><?php echo $model->nombre;?></h1>

        <?php if($model->jefe_proyecto!=null):?>
            <h3>Cambiar jefe de proyecto:</h3>
                <p>Mediante el siguiente formulario se puede enviar una invitaci&oacute;n para que otro usuario sea el
                jefe del presente proyecto. Una vez que el otro usuario acepte la invitaci&oacute;n, se har&aacute; efectivo el
                cambio.</p>
             <p>

                 <?php if($invitacion):?>
                    Ya existe una solicitud de cambio de jefe de proyecto pendiente.
                <?php else:?>

                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm');?>
                <?php echo $form->errorSummary($cambiar_jefe); ?>
                <label style="display:inline;">Ingrese nombre de usuario:</label>
                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'attribute'=>'coordinador-livesearch',
                    'sourceUrl'=>array('/iniciativas/crudIniciativas/buscarIntegrante'),
                    'name'=>'coordinador-livesearch',
                    'options'=>array(
                        'minLength'=>'3',
                        'showAnim'=>'fold',
                        'select'=>"js: function(event, ui) {
                          $('#CambiarJefeProyectoForm_id_usuario').val(ui.item['id']);
                        }"
                    ),
                    'htmlOptions'=>array(
                        'size'=>45,
                        'maxlength'=>45,
                        'style'=>'height:24px;'
                    ),
                )); ?>
                <?php echo $form->hiddenField($cambiar_jefe, "id_usuario"); ?>
                <?php echo $form->hiddenField($cambiar_jefe, "id_proyecto",array('value'=>$model->id)); ?>
                <?php echo CHtml::submitButton('Invitar',array('class'=>'nice radius small button')); ?>

                <?php $this->endWidget(); ?>

                <?php endif;?>
            </p>
        <?php endif;?>
    </li>
</ul>
