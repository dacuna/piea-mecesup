<?php
$this->pageTitle = 'Editar Proyecto '.$model->nombre;
$this->breadcrumbs=array(
  'Proyectos'=>array('index'),
  $model->nombre=>array('ver','id'=>$model->id),
  'Editar',
);

?>

<dl class="nice contained tabs">
  <dd><a href="#" class="active">Editar Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="crear-proyecto">

    <div class="form">

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
      'id'=>'proyecto-form',
      'type'=>'nice'
    )); ?>

      <p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

      <?php echo $form->textFieldRow($model, "nombre"); ?>
      <?php echo $form->dropDownListRow($model, 'iniciativa_id',
                          CHtml::listData(Iniciativa::model()->findAll(), 'id', 'nombre_abreviado')
                          ); ?>      
      <?php echo $form->textFieldRow($model, "sigla"); ?>
      <?php echo $form->dropDownListRow($model, 'campus',
        CHtml::listData($model->lista_campus, 'id', 'nombre'),array('encode'=>false,'style'=>'height: 28px;')); ?> 
      <?php echo $form->labelEx($model,'fecha_inicio'); ?>
      <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
        'model'=>$model,
        'attribute'=>'fecha_inicio',
        'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy','changeYear'=>'true','yearRange'=> '1940:2012'),
        'language'=>'es',
        'htmlOptions' => array('class'=>'input-text'),
        ));
      ?>
      <div class="form-field error">
        <?php echo $form->error($model,'fecha_inicio'); ?>
      </div>
      <?php echo $form->textAreaRow($model, "descripcion",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "objetivos_generales",array('rows'=>6)); ?> 
      <?php echo $form->textAreaRow($model, "objetivos_especificos",array('rows'=>6)); ?> 

      <div class="row buttons">
        <?php echo CHtml::submitButton('Crear Proyecto',array('class'=>'nice radius medium button')); ?>
      </div>

    <?php $this->endWidget(); ?>

    </div><!-- form -->
  </li>
</ul>