<?php
$this->pageTitle = 'Ver Proyecto '.$model->nombre;
$this->breadcrumbs=array(
  'Proyectos'=>array('index'),
  'Ver Proyecto',
);

?>

<dl class="nice contained tabs">
    <dd><a href="<?php echo url('/site/explorarPiea');?>" id="explorar-piea">Explorar PIEA</a></dd>
    <dd><a href="<?php echo url('/iniciativas/crudIniciativas/explorar',array('id'=>$model->iniciativa_id));?>" id="ver-iniciativa">Iniciativa</a></dd>
    <dd><a href="#ver-proyecto" id="ver-proyecto" class="active">Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-proyecto">

    <h1><?php echo $model->nombre;?></h1>

    <p>
    <strong>Nombre:</strong> <?php echo $model->nombre;?><br>
    <strong>Iniciativa:</strong> <?php echo $model->iniciativa->nombre_abreviado;?><br>
    <strong>Descripci&oacute;n:</strong> <?php echo $model->descripcion;?><br>
    <strong>Objetivos generales:</strong> <?php echo $model->objetivos_generales;?>
    </p>
    
    <p>
      <?php if($model->jefe_proyecto!=null):?>
        <strong>Coordinador: </strong><?php echo $model->jefe_proyecto->nombreCompleto;?>

        <?php if(IntegranteProyecto::model()->find('proyecto_id=:pid and user_id=:uid',array(':pid'=>$model->id,':uid'=>user()->id))==null):?>
            <br><br>
            <?php if(Mensaje::model()->mensajeSolicitud(array('tipo'=>'proyecto','id'=>$model->id),user()->id)==null):?>
                <a href="<?php echo Yii::app()->createUrl('/proyectos/gestionIntegrantes/solicitarMembresia',array('id'=>$model->id));?>" class="small nice button">Solicitar participaci&oacute;n</a>
             <?php else:?>
                Ya has solicitado participaci&oacute;n. Tu solicitud est&aacute; siendo procesada.
             <?php endif;?>
        <?php endif;?>
      <?php endif;?>
    </p>

    <div class="panel">
      <h5>Integrantes del proyecto</h5>
      <?php
        $this->widget('zii.widgets.grid.CGridView', array(
          'dataProvider'=>$integrantes,
          'selectableRows'=>0,
          'itemsCssClass'=>'table',
          'columns'=>array(
              'usuario.nombreCompleto',
              'usuario.email',  
          ),
      ));
      ?>
    </div>
    
  </li>
</ul>


