<?php
$this->pageTitle = 'Ver Proyecto ' . $model->nombre;
$this->breadcrumbs = array(
    'Proyectos' => array('index'),
    'Ver Proyecto',
);

?>

<?php $this->renderPartial('//partials/_menu_top_proyecto',array('entidad'=>$entidad,'selected'=>0));?>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-proyecto">

        <h1><?php echo $model->nombre;?></h1>
        <p>
            <strong>Nombre:</strong> <?php echo $model->nombre;?><br>
            <strong>Iniciativa:</strong>
            <?php echo (isset($model->iniciativa)) ? $model->iniciativa->nombre_abreviado : "No pertenece a ninguna iniciativa PIEA";?><br>
            <?php if(isset($model->campus)):?>
                <strong>Campus:</strong> <?php echo $model->campus;?><br>
            <?php endif;?>
            <?php if(isset($model->descripcion)):?>
                <strong>Descripci&oacute;n:</strong> <?php echo $model->descripcion;?><br>
            <?php endif;?>
            <?php if(isset($model->objetivos_generales)):?>
                <strong>Objetivos generales:</strong> <?php echo $model->objetivos_generales;?><br>
            <?php endif;?>
            <?php if($model->es_postulacion==1 && ($entidad->checkPermisosBoolean('integrante',user()->id)
                    || user()->checkAccess("ayudante_hitos_proyecto_{$model->id}"))):?>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/generarPdfHistoricoProyecto',array('id'=>$model->id));?>">
                    Generar PDF con postulaci&oacute;n a fondo I+D+i</a><br>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/generarZipAnexos',array('id'=>$model->id));?>">
                    Generar .ZIP con anexos del proyecto</a><br>
            <?php endif;?>
            <br><a href="<?php echo url('/site/page',array('view'=>'documentos'));?>" style="font-size: 15px;">
                    <strong>Documentos de inter&eacute;s de PIEA</strong></a>
        </p>

        <?php if($model->es_postulacion==1 && $model->estado_postulacion==0):?>
            <p>Este proyecto se encuentra en etapa de postulaci&oacute;n a los fondos I+D+i</p>
        <?php else:?>
            <p>
                <?php if ($model->jefe_proyecto != null): ?>
                <strong>Jefe de proyecto: </strong><?php echo $model->jefe_proyecto->nombreCompleto; ?>
                <?php endif;?>
            </p>

            <?php if (user()->checkAccess('jefe_proyecto', array('iniciativa' => $model->iniciativa_id, 'proyecto' => $model->id)) || Yii::app()->user->checkAccess('coordinador_iniciativa', array('iniciativa' => $model->iniciativa_id))): ?>
                <a href="<?php echo Yii::app()->createUrl('/usuarios/userActions/invitar', array('tipo' => 'proyecto', 'id' => $model->id));?>"
                   class="nice radius small button">Invitar persona</a>
                <a href="<?php echo Yii::app()->createUrl('/proyectos/crud/editar', array('id' => $model->id));?>"
                   class="nice radius small button">Editar Proyecto</a>
                <br><br>
            <?php endif;?>

        <?php if(false && $entidad->checkPermisosBoolean('integrante',user()->id)):?>
            <div class="row">
                <div class="six columns">
                    <div class="panel">
                        <h5>Cuentas y transferencias:</h5>
                        <ul>
                            <li><a href="<?php echo url('/finanzas/crud/verCuentasEntidad',array('id'=>$model->id,'tipo'=>1));?>">
                                - Ver Cuentas</a></li>
                            <li><a href="<?php echo url('/finanzas/transferencia/realizarTransferencia',array('id'=>$model->id,'tipo'=>1));?>">
                                - Realizar transferencia</a>
                            </li>
                            <li><a href="<?php echo url('/finanzas/transferencia/transferenciaInterna',array('id'=>$model->id,'tipo'=>1));?>">
                                - Transferencia interna</a>
                            </li>
                            <li><a href="<?php echo url('/finanzas/historico/verTransferenciasEntidad',array('id'=>$model->id,'tipo'=>1));?>">
                                - Ver transferencias</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="six columns">
                    <div class="panel">
                        <h5>Operaciones de finanzas:</h5>
                        <ul>
                            <?php if($entidad->checkPermisosBoolean('ingresar_presupuesto',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearPresupuesto',array('id'=>$model->id,'tipo'=>1));?>">- Ingresar presupuesto</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ingresar_recuperacion',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearRecibo',array('id'=>$model->id,'tipo'=>1,'tf'=>1));?>">- Solicitar recuperaci&oacute;n de gastos</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ver_recuperacion',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/verRecuperacionGastos',array('id'=>$model->id,'tipo'=>1));?>">- Ver recuperaci&oacute;n de gastos</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ingresar_vale',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearRecibo',array('id'=>$model->id,'tipo'=>1,'tf'=>2));?>">- Solicitar vale a rendir</a>
                            </li>
                            <li><a href="<?php echo url('/finanzas/operaciones/verRetirosFondoDisponiblePorUsuario',array('id'=>$model->id,'tipo'=>1));?>">- Gestionar vales que he solicitado</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('gestionar_vales',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/verRetirosFondoDisponible',array('id'=>$model->id,'tipo'=>1));?>">- Gestionar vales solicitados del proyecto</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ver_vales_historicos',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/verValesRendir',array('id'=>$model->id,'tipo'=>1));?>">- Ver vales a rendir hist&oacute;ricos</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ingresar_pago_proveedor',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearPagoProveedores',array('id'=>$model->id,'tipo'=>1,'tf'=>1));?>">- Crear pago a proveedores</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ver_pago_proveedor',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/verPagoProveedores',array('id'=>$model->id,'tipo'=>1,'tf'=>1));?>">- Ver pagos a proveedores</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ingresar_presupuesto',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearPresupuestoUnidadAbastecimiento',array('id'=>$model->id,'tipo'=>1));?>">- Ingresar presupuesto para unidad de abastecimiento</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ingresar_unidad_abastecimiento',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/crearPagoProveedores',array('id'=>$model->id,'tipo'=>1,'tf'=>2));?>">- Crear unidad de abastecimiento</a>
                            </li>
                            <?php endif;?>
                            <?php if($entidad->checkPermisosBoolean('ver_unidad_abastecimiento',user()->id)):?>
                            <li><a href="<?php echo url('/finanzas/operaciones/verPagoProveedores',array('id'=>$model->id,'tipo'=>1,'tf'=>2));?>">- Ver unidad de abastecimiento</a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif;?>

            <?php if($entidad->verificarAsignacionDeCargo(user()->id)):?>
                <a href="<?php echo Yii::app()->createUrl('/usuarios/cargos/crear', array('id' => $model->id, 'tipo' => 1));?>"
               class="small nice button">Asignar Cargo</a>
            <?php endif;?>

            <?php if(user()->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->iniciativa_id)) && $model->jefe_proyecto!=null):?>
                <a href="<?php echo url('/proyectos/administracion/cambiarJefeProyecto',array('id'=>$model->id));?>" class="small nice button">Cambiar Coordinador</a>
            <?php endif;?>

            <br><br>

            <?php if (user()->checkAccess('coordinador_iniciativa', array('iniciativa' => $model->iniciativa_id)) && $model->jefe_proyecto == null):?>
            <?php if (!InvitacionProyecto::model()->existeInvitacionEnCurso('jefe_proyecto', $model->id, 1)): ?>
                <h3>Invitar jefe de proyecto:</h3>
                <p>
                    <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm', array('id' => 'invitar-jefe-form')); ?>
                    <?php echo $form->errorSummary($invitar_jefe); ?>
                    <label style="display:inline;">Ingrese nombre de usuario:</label>
                    <?php
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'attribute' => 'InvitarJefeProyectoForm[nombre_usuario]',
                        'sourceUrl' => array('/iniciativas/crudIniciativas/buscarIntegrante'),
                        'name' => 'InvitarJefeProyectoForm[nombre_usuario]',
                        'options' => array(
                            'minLength' => '3',
                            'showAnim' => 'fold',
                            'select' => "js: function(event, ui) {
                          $('#InvitarJefeProyectoForm_id_usuario').val(ui.item['id']);
                        }"
                        ),
                        'htmlOptions' => array(
                            'size' => 45,
                            'maxlength' => 45,
                            'style' => 'height:24px;'
                        ),
                    )); ?>
                    <?php echo $form->hiddenField($invitar_jefe, "id_usuario"); ?>
                    <?php echo $form->hiddenField($invitar_jefe, "id_proyecto", array('value' => $model->id)); ?>
                    <?php echo CHtml::submitButton('Invitar', array('class' => 'nice radius small button')); ?>

                    <?php $this->endWidget(); ?>
                </p>
                <?php else: ?>
                    <h3>Invitar jefe de proyecto:</h3>
                    <p>Ya existe una invitaci&oacute;n de jefe de proyecto en curso.</p>
                <?php endif;?>
            <?php endif;?>

            <?php if(user()->checkAccess('integrante_proyecto',array('iniciativa'=>$model->iniciativa_id,'proyecto'=>$model->id))):?>

            <div class="row">
                <div class="twelve columns">
                    <div class="panel">
                        <h5>Cargos</h5>
                        <p>La siguiente tabla muestra los cargos existentes en el proyecto junto con sus responsables.</p>
                        <table>
                            <thead>
                            <tr>
                                <th>Cargo</th>
                                <th>Responsable</th>
                                <th>Permisos</th>
                                <th>Transferir</th>
                                <?php if(user()->checkAccess('jefe_proyecto',array('iniciativa'=>$entidad->entidad->iniciativa_id,'proyecto'=>$entidad->getId()))):?>
                                <th>Eliminar</th>
                                <?php endif;?>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($model->cargos as $cargo):?>
                            <tr>
                                <td><?php echo $cargo[0]->getDescription();?></td>
                                <td>
                                    <?php if($cargo[1]->asignacion_nula==1):?>
                                    ---
                                    <?php else:?>
                                    <?php echo $cargo[1]->usuario->nombrePresentacion;?>
                                    <?php endif;?>
                                </td>
                                <td><?php array_walk($cargo[0]->getChildren(),
                                    create_function('$item,$key,$size','echo $item->getDescription();if($key<$size-1) echo ", ";'),count($cargo[0]->getChildren()));?></td>
                                <td>
                                    <?php if($entidad->verificarJerarquiaCargo($cargo[1])):?>
                                        <a href="<?php echo url('/usuarios/cargos/transferirCargo',array(
                                            'id'=>$model->id,'tipo'=>1,'cargo'=>$cargo[1]->nombre_item));?>">Transferir</a>
                                    <?php else:?>
                                        ---
                                    <?php endif;?>
                                </td>
                                <?php if(user()->checkAccess('jefe_proyecto',array('iniciativa'=>$entidad->entidad->iniciativa_id,'proyecto'=>$entidad->getId()))):?>
                                <td>
                                    <a href="<?php echo url('/usuarios/cargos/eliminarCargo',
                                        array('id'=>$entidad->getId(),'tipo'=>$entidad->getTipoEntidadNumerico(),'cargo'=>$cargo[1]->nombre_item));?>">Eliminar</a>
                                </td>
                                <?php endif;?>
                            </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif;?>

        <?php endif;?>

    </li>
</ul>
