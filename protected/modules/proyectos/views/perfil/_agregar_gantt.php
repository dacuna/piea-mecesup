<div class="panel">
        <h5>Agregar actividad</h5>
        <p>Para agregar una actividad debes completar los siguientes campos. Todos los campos son obligatorios.</p>

    <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
        {{errorMsg}}
        <ul>
            <li ng-repeat="error in validationErrors">
                - {{error}}
            </li>
        </ul>
        <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
    </div>

    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id'=>'agregar-hito','type'=>'nice')); ?>

        <div class="row">
            <div class="twelve columns">
                <?php echo $form->textFieldRow($model,'nombre',array('style'=>'width:100%;','ng-model'=>'activity.nombre')); ?>
            </div>
        </div>

        <p>Seleccione los meses en los que desarrollar&aacute; la actividad:</p>

    <table>
        <thead>
        <tr>
            <th>May</th>
            <th>Jun</th>
            <th>Jul</th>
            <th>Ago</th>
            <th>Sep</th>
            <th>Oct</th>
            <th>Nov</th>
            <th>Dic</th>
            <th>Ene</th>
            <th>Feb</th>
            <th>Mar</th>
            <th>Abr</th>
            <th>May</th>
            <th>Jun</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td ng-repeat="month in months">
                <input type="checkbox" ng-model="month.status">
            </td>
        </tr>
        </tbody>
    </table>

        <a href="" class="nice medium green button" ng-click="saveActivity()">Agregar Actividad</a>
        <a href="" class="close-modal nice medium red button">Cancelar</a>
    </div>

  <?php $this->endWidget(); ?>

</div>