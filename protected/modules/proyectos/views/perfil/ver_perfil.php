<?php
$this->pageTitle = 'Ver Proyecto ' . $model->nombre;
$this->breadcrumbs = array(
    'Proyectos' => array('index'),
    'Perfil',
);

?>

<?php $this->renderPartial('//partials/_menu_top_proyecto',array('entidad'=>$entidad,'selected'=>1));?>

<ul class="nice tabs-content contained" ng-app="refererModule" ng-controller="RefererController">
    <li class="active" id="ver-proyecto">

        <h3>Perfil del proyecto</h3>
        <p>
            <?php if(isset($proyecto->nombre)):?> <strong>Nombre</strong>: <?php echo $proyecto->nombre;?><br> <?php endif;?>
            <?php if(isset($proyecto->iniciativa_id)):?> <strong>Iniciativa</strong>: <?php echo $proyecto->iniciativa->nombre_abreviado;?><br> <?php endif;?>
            <?php if(isset($proyecto->campus)):?> <strong>Campus</strong>: <?php echo $proyecto->campus;?><br> <?php endif;?>
            <?php if(isset($perfil->extracto)):?> <br><strong>Extracto</strong>: <?php echo $perfil->extracto;?><br> <?php endif;?>
            <?php if(isset($perfil->resumen)):?> <br><strong>Resumen</strong>: <?php echo $perfil->resumen;?><br> <?php endif;?>
            <?php if(isset($perfil->objetivos_generales)):?> <br><strong>Objetivos generales</strong>: <?php echo $perfil->objetivos_generales;?><br> <?php endif;?>
            <?php if(isset($perfil->estado_del_arte)):?> <br><strong>Estado del arte</strong>: <?php echo $perfil->estado_del_arte;?><br> <?php endif;?>
            <?php if(isset($perfil->experiencia_previa)):?> <br><strong>Experiencia previa</strong>: <?php echo $perfil->experiencia_previa;?><br> <?php endif;?>
        </p>

        <h3 ng-show="referers.length>0">Referentes del proyecto</h3>

        <table ng-show="referers.length>0">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Tel&eacute;fono</th>
                <th>Relaci&oacute;n con el proyecto</th>
                <th>Instituci&oacute;n</th>
                <th>Cargo</th>
                <th>Acci&oacute;n</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="referer in referers">
                <td>{{referer.nombre}}</td>
                <td>{{referer.email}}</td>
                <td>{{referer.telefono}}</td>
                <td>{{referer.relacion_proyecto}}</td>
                <td>{{referer.institucion}}</td>
                <td>{{referer.cargo}}</td>
                <td><a href="" ng-click="deleteReferer(referer.id)"><span style="color:red;">Eliminar</span></a></td>
            </tr>
            </tbody>
        </table>
    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var milestoneModule = angular.module('refererModule', []);
    function RefererController($scope, $http) {
        $scope.referer={};
        $scope.showAdd=0;
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.referers = [<?php foreach($referentes as $r) echo "{id:{$r->id},nombre:'{$r->nombre}',email:'{$r->email}',telefono:'{$r->telefono}',relacion_proyecto:'{$r->relacion_proyecto}',institucion:'{$r->institucion}',cargo:'{$r->cargo}'},";?>];

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.referers, function(ref) {
                count += 1;
            });
            return count;
        };

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

</script>