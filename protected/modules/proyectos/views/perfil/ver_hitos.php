<?php
$this->pageTitle = 'Ver Proyecto ' . $model->nombre;
$this->breadcrumbs = array(
    'Proyectos' => array('index'),
    'Ver Hitos',
);

?>

<?php $this->renderPartial('//partials/_menu_top_proyecto',array('entidad'=>$entidad,'selected'=>4));?>

<ul class="nice tabs-content contained" ng-app="milestoneModule" ng-controller="HitosController">
    <li class="active" id="ver-proyecto">

        <h3>Hitos del proyecto</h3>
        <p>
            A continuaci&oacute;n se listan los hitos del proyecto
        </p>

        <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
            <div class="row">
                <a href="" class="nice small button" ng-show="showAdd==0" ng-click="showAdd=1" style="float:right;">+ Agregar Hito</a><br><br>
                <a href="" class="nice red small button" ng-show="showAdd==1" ng-click="showAdd=0" style="float:right;">Cancelar</a>
            </div>

            <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
                {{errorMsg}}
                <ul>
                    <li ng-repeat="error in validationErrors">
                        - {{error}}
                    </li>
                </ul>
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="agregar-hito" ng-show="showAdd==1" style="display:none;">
                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                'id'=>'agregar-hito','type'=>'nice')); ?>

                <div class="row">
                    <div class="six columns">
                        <?php echo $form->textFieldRow($nuevo,'nombre',array('style'=>'width:100%;','ng-model'=>'milestone.nombre')); ?>
                    </div>
                    <div class="six columns">
                        <?php echo $form->labelEx($nuevo,'fecha'); ?>
                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                            'model'=>$nuevo,
                            'attribute'=>'fecha',
                            'options' => array('dateFormat'=>'dd-mm-yy','minDate'=>'new Date()'),
                            'language'=>'es',
                            'htmlOptions' => array('class'=>'input-text','style'=>'width:100%;','ng-model'=>'milestone.fecha'),
                        ));
                        ?>
                    </div>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($nuevo,'descripcion'); ?>
                    <?php echo $form->textArea($nuevo,'descripcion',array('rows'=>5,'style'=>'width:100%;','ng-model'=>'milestone.descripcion')); ?>
                    <?php echo $form->error($nuevo,'descripcion'); ?>
                </div>

                <a href="" class="nice medium green button" ng-click="saveMilestone()">Agregar Hito</a><br>
            </div>

            <?php $this->endWidget(); ?>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>
        <?php endif;?>

        <table>
            <thead>
            <tr>
                <th>Nombre</th>
                <th style="width:15%;">Fecha</th>
                <th>Descripci&oacute;n</th>
                <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
                <th>Editar</th>
                <th>Eliminar</th>
                <?php endif;?>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="milestone in milestones | orderBy:'fecha'">
                <td>{{milestone.nombre}}</td>
                <td>
                    <span ng-show="milestone.edit==0">{{milestone.fecha}}</span>
                    <div ng-show="milestone.edit==1">
                        <input type="text" ng-model="milestone.fecha" style="width:100%;">
                    </div>
                </td>
                <td>{{milestone.descripcion}}</td>
                <?php if($entidad->checkPermisosBoolean('encargado',user()->id)):?>
                <td>
                    <a ng-show="milestone.edit==0" href="" ng-click="milestone.edit=1">Editar Fecha</a>
                    <a ng-show="milestone.edit==1" href="" ng-click="editMilestone(milestone.id)">Guardar</a>
                    <a ng-show="milestone.edit==1" href="" ng-click="milestone.edit=0" style="color:red;">Cancelar</a>
                </td>
                <td><a href="" ng-click="deleteMilestone(milestone.id)">Eliminar</a></td>
                <?php endif;?>
            </tr>
            </tbody>
        </table>
    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var milestoneModule = angular.module('milestoneModule', []);
    function HitosController($scope, $http) {
        $scope.milestone={};
        $scope.showAdd=0;
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.milestones = [<?php foreach($hitos as $hito){ echo "{id:{$hito->id},nombre:'{$hito->nombre}',descripcion:'";echo trim(preg_replace('/\s/',' ',$hito->descripcion));echo "',fecha:'{$hito->fecha}',edit:0},";}?>];

        $scope.saveMilestone = function() {
            $scope.milestone.fecha=$('#HitoProyecto_fecha').val();
            $http.post("<?php echo url('/proyectos/perfil/agregarHito',array('id'=>$model->id));?>",$scope.milestone)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new milestone into the array
                    $scope.milestones.push({'id':data.id,'nombre':data.nombre,'descripcion':data.descripcion,'fecha':data.fecha});
                    $scope.milestone.nombre = '';
                    $scope.milestone.descripcion= '';
                    $scope.milestone.fecha= '';
                    $scope.showAdd=$scope.showError=0;
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        };

        $scope.editMilestone = function(id) {
            elem=-1;
            angular.forEach($scope.milestones,function(milestone) {
                if(milestone.id==id)
                    elem=milestone;
            });
            $http.post("<?php echo url('/proyectos/perfil/editarHito',array('id'=>$model->id));?>",elem)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.id!=undefined){
                        $scope.showAdd=$scope.showError=0;
                        $scope.showSuccess=1;
                        $scope.successMsg=data.mensaje;
                        elem.edit=0;
                    }else{
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.deleteMilestone = function(id){
            elem=-1;
            count=0;
            angular.forEach($scope.milestones,function(milestone) {
                if(milestone.id==id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/proyectos/perfil/eliminarHito',array('id'=>$model->id));?>",
                    {params:{'hito':id}}).success(function(data) {
                if(data.success!=undefined){
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                    $scope.milestones.remove(elem);
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                }
            });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

</script>
