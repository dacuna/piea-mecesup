<?php
$this->pageTitle = 'Ver Proyecto ' . $model->nombre;
$this->breadcrumbs = array(
    'Proyectos' => array('index'),
    'Ver Proyecto',
);

?>

<?php $this->renderPartial('//partials/_menu_top_proyecto',array('entidad'=>$entidad,'selected'=>6));?>

<ul class="nice tabs-content contained">
    <li class="active" id="ver-proyecto">

        <h3>Presupuesto</h3>
        <p>
            A continuaci&oacute;n se lista el presupuesto para el presente proyecto.
        </p>

        <table>
            <thead>
            <tr>
                <th style="font-size: 13px;">Item</th>
                <th style="font-size: 13px;">Precio</th>
                <th style="font-size: 13px;">Cant.</th>
                <th style="font-size: 13px;">Total</th>
                <th style="font-size: 13px;">I+D+i</th>
                <th style="font-size: 13px;">Propio</th>
                <th style="font-size: 13px;">Otros</th>
                <th style="font-size: 13px;">PIE>A Modificado</th>
                <th style="font-size: 13px;">Adjudicado</th>
            </tr>
            </thead>
            <tbody>
            <?php for($i=0;$i<4;$i++):?>
                <tr>
                    <td><b>
                            <?php if($i==0){?> Adquisiciones de implementaci&oacute;n
                            <?php }elseif($i==1){?> Materiales e insumos
                            <?php }elseif($i==2){?> Gastos Operativos
                            <?php }else{?> Otros items <?php }?>
                        </b>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="money"></td>
                    <td class="money"></td>
                    <td class="money"></td>
                    <td class="money"></td>
                    <td class="money"></td>
                    <td class="money"></td>
                </tr>
                <?php foreach($presupuesto['items'][$i] as $item):?>
                    <tr>
                        <td><span><?php echo $item[0]->nombre;?></span></td>
                        <td class="money"><span><?php echo $item[0]->precio;?></span></td>
                        <td class="money"><span><?php echo $item[0]->unidades;?></span></td>
                        <td class="money"><?php echo $item[0]->precio*$item[0]->unidades;?></td>
                        <td class="money"><span><?php echo $item[0]->idi;?></span></td>
                        <td class="money"><span><?php echo $item[0]->propio;?></span></td>
                        <td class="money"><span><?php echo $item[0]->otros;?></span></td>
                        <td class="money"><span>
                            <?php if($item[1]!=null):?>
                                <?php echo $item[1]->monto_idi_editado;?>
                            <?php else:?>
                                0
                            <?php endif;?></span>
                        </td>
                        <td class="money"><span>
                            <?php if($item[1]!=null):?>
                                <?php echo $item[1]->monto_idi_editado;?>
                            <?php else:?>
                                -
                            <?php endif;?></span>
                        </td>
                <?php endforeach;?>
                <tr style="border-bottom:2px #818181 solid;">
                    <td><b>TOTAL (
                            <?php if($i==0){?> Adquisiciones de implementaci&oacute;n
                            <?php }elseif($i==1){?> Materiales e insumos
                            <?php }elseif($i==2){?> Gastos Operativos
                            <?php }else{?> Otros items <?php }?>
                            )</b></td>
                    <td></td>
                    <td></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][0];?></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][1];?></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][2];?></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][3];?></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][4];?></td>
                    <td class="money"><?php echo $presupuesto['totales_categoria'][$i][5];?></td>
                </tr>
            <?php endfor;?>
            <tr style="border-bottom:2px #818181 solid;">
                <td><b>TOTAL</b></td>
                <td></td>
                <td></td>
                <td class="money"><?php echo $presupuesto['totales'][0];?></td>
                <td class="money"><?php echo $presupuesto['totales'][1];?></td>
                <td class="money"><?php echo $presupuesto['totales'][2];?></td>
                <td class="money"><?php echo $presupuesto['totales'][3];?></td>
                <td class="money"><?php echo $presupuesto['totales'][4];?></td>
                <td class="money"><?php echo $total_adjudicado;?></td>
            </tr>
            </tbody>
        </table>
    </li>
</ul>
