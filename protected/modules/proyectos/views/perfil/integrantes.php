<?php
$this->pageTitle = 'Ver Proyecto '.$model->nombre;
$this->breadcrumbs=array(
  'Proyectos'=>array('index'),
  $model->nombre=>array('ver','id'=>$model->id),
  'Integrantes'
);
?>

<?php $this->renderPartial('//partials/_menu_top_proyecto',array('entidad'=>$entidad,'selected'=>3));?>

<ul class="nice tabs-content contained">
  <li class="active" id="ver-integrantes">

    <div class="panel">
      <h5>Integrantes del proyecto</h5>

    <p>Los siguientes son los participantes de este proyecto:</p>

      <ul class="clearfix">
        <?php foreach($model->integrantes as $integrante):?>
          <li style="width:47%;float:left;">
            <?php if(!empty($integrante->usuario->perfil->avatar)):?>
              <img style="float:left;" src="<?php echo Yii::app()->request->baseUrl;?>/avatars/<?php echo $integrante->usuario->perfil->avatar;?>" width="64" height="64">
            <?php else:?>
              <img style="float:left;" src="<?php echo Yii::app()->request->baseUrl;?>/images/no-picture.jpg" width="64" height="64">
            <?php endif;?>

            <p style="float:left;padding-top:10px;margin-left:10px;">
              <?php echo $integrante->usuario->nombreCompleto;?><br>
              &lt;<?php echo $integrante->usuario->email;?>&gt;<br>
              <a href="<?php echo app()->createUrl('/usuarios/perfil/index',array('id'=>$integrante->usuario->id));?>">Ver Perfil</a>
              <?php if($model->sigla=='MI' && user()->checkAccess('administrador')):?>
                <!-- SOLO VALIDO PARA LA MESA DE INVESTIGACION --><br>
                <a href="<?php echo app()->createUrl('/formularios/administracionFondoIDI/asignarProyectosAyudanteHitos'
                    ,array('ayudante'=>$integrante->usuario->id));?>">
                    Asignar proyectos para seguimiento
                </a>
              <?php endif;?>
            </p>
          </li>
        <?php endforeach;?>
      </ul>

    </div>

  </li>
</ul>