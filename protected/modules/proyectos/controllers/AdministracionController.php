<?php
/**
 * Controlador con acciones para administrar un proyecto.
 *
 * Principalmente contiene las acciones que permiten cambiar al jefe de proyecto actual del proyecto y ademas
 * invitar a otro usuario a ser jefe de proyecto.
 *
 * @version   1.0
 * @since     10/15/12
 * @author    dacuna <dacuna@dacuna.me>
 */
class AdministracionController extends Controller
{
    /**
     * Permite cambiar al actual jefe de proyecto de un proyecto. El flujo de esta accion consiste en
     * enviar una invitacion al nuevo jefe de proyecto. Solo el coordinador de la iniciativa (o administrador)
     * a la que pertenece el proyecto puede ejecutar esta accion.
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionCambiarJefeProyecto($id)
    {
        $model = ProyectoModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'La p&aacute;gina seleccionada no existe.');
        //verifico que el acceso sea de el del coordinador de la iniciativa
        if(!user()->checkAccess('coordinador_iniciativa',array('iniciativa'=>$model->iniciativa_id)))
            throw new CHttpException(403,'No tienes los permisos necesarios para ver esta p&aacute;gina');
        if ($model->jefe_proyecto == null)
            $this->redirect(array('/proyectos/crud/ver', 'id' => $id));

        //si es que ya se ha enviado una solicitud de cambio, no se puede ejecutar esta accion, solo una solicitud
        //puede haber en el sistema por cada iniciativa
        $invitacion = InvitacionProyecto::model()->existeInvitacionEnCurso('jefe_proyecto',$id,1);

        //se debe invitar a un coordinador primero, el nuevo coordinador debe aceptar la invitacion
        $cambiar_jefe= new CambiarJefeProyectoForm;
        if (!$invitacion && isset($_POST['CambiarJefeProyectoForm'])) {
            $cambiar_jefe->attributes = $_POST['CambiarJefeProyectoForm'];
            if ($cambiar_jefe->validate()) {
                $cambiar_jefe->enviarInvitacion();
                Yii::app()->user->setFlash('success', 'Se ha enviado correctamente la invitaci&oacute;n.');
                $this->redirect(array('/proyectos/crud/ver', 'id' => $cambiar_jefe->id_proyecto));
            }
        }

        $this->render('cambiar_jefe_proyecto', array(
            'model' => $model,
            'cambiar_jefe' => $cambiar_jefe,
            'invitacion' => $invitacion
        ));
    }

    /**
     * Permite que un usuario que fue invitado a ser jefe de un proyecto acepte dicha invitacion pasando asi a
     * ser el nuevo jefe de proyecto. Esta accion solo puede ser ejecutada desde la notificacion de invitacion
     * correspondiente de invitacion a ser jefe de proyecto en algun proyecto en particular.
     *
     * @param int $e emisor de la notificacion
     * @param int $d destinatario de la notificacion
     * @param int $uid identificador unico del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionAceptarSolicitudCambioJefeProyecto($e, $d, $uid)
    {
        $this->onContestarInvitacion=array(app()->mailman,'notificacionHandler');
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        //se deben revocar los permisos de coordinacion al usuario antiguo y darle nuevos permisos al usuario destinatario del mensaje
        $proyecto = ProyectoModel::model()->findByPk($mensaje->id_relacion);
        $jefe_antiguo = $proyecto->jefe_proyecto;
        $lookup_antiguo = LookupProyecto::model()->find('nombre_item=:ni and proyecto_id=:ii and user_id=:ui', array(
            ':ni' => 'jefe_proyecto',
            ':ii' => $proyecto->id,
            ':ui' => $jefe_antiguo->id
        ));
        //a ese lookup se debe dejar como inactivo
        $lookup_antiguo->activo = 0;
        if ($lookup_antiguo->save()) {
            $auth = Yii::app()->authManager;
            //verifico que no sea coordinador de otra iniciativa, si lo es entonces ya tiene el permiso asignado
            if ($auth->getAuthAssignment("jefe_proyecto", $mensaje->destinatario) == null)
                $auth->assign('jefe_proyecto', $mensaje->destinatario);
            //seteo el lookup de la proyecto correspondiente
            LookupProyecto::crearLookupProyecto('jefe_proyecto', $mensaje->destinatario, $proyecto->id);

            //se debe tambien ingresar el integrante nuevo
            if (IntegranteProyecto::crearIntegrante($mensaje->destinatario, $proyecto->id) != null) {
                //por ultimo marco la invitacion como contestada y el mensaje como leido
                InvitacionProyecto::model()->updateByPk($mensaje->id, array('aceptada' => 1));
                $mensaje->contestado = 1;
                $mensaje->accion_realizada = 'Aceptar invitaci&oacute;n';
                $mensaje->save(false);

                //se notifica al usuario que la invitacion ha sido aceptada
                $persona=Persona::model()->findByPk($mensaje->destinatario);
                $this->onContestarInvitacion(new CEvent($this,array('tipo'=>'aceptar_cambio_jefe_proyecto','proyecto'=>$proyecto,'usuario'=>$persona,'destinatario'=>$mensaje->emisor)));

                user()->setFlash('success', 'Felicidades, ahora eres el jefe del proyecto ' . $proyecto->nombre);
                $this->redirect(array('/proyectos/crud/ver', 'id' => $proyecto->id));
            } else {
                user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
                $this->redirect(array('/'));
            }
        }
    }

    /**
     * Permite que un usuario que fue invitado a ser jefe de un proyecto rechazo dicha invitacion. Esta accion solo
     * puede ser ejecutada desde la notificacion de invitacion correspondiente de invitacion a ser jefe de proyecto
     * en algun proyecto en particular. Si la invitacion es rechazada entonces se alerta a la persona que envio la
     * invitacion de dicha accion.
     *
     * @param int $e emisor de la notificacion
     * @param int $d destinatario de la notificacion
     * @param int $uid identificador unico del mensaje
     *
     * @return  void
     * @throws  \CHttpException
     */
    public function actionRechazarSolicitudCambioJefeProyecto($e, $d, $uid)
    {
        $this->onContestarInvitacion=array(app()->mailman,'notificacionHandler');
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        if ($mensaje == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if ($mensaje->contestado == 1) {
            user()->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        //por ultimo marco la invitacion como contestada y el mensaje como leido
        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar invitaci&oacute;n';
        $mensaje->save(false);

        $proyecto = ProyectoModel::model()->findByPk($mensaje->id_relacion);
        $persona=Persona::model()->findByPk($mensaje->destinatario);
        $this->onContestarInvitacion(new CEvent($this,array('tipo'=>'rechazar_cambio_jefe_proyecto','proyecto'=>$proyecto,'usuario'=>$persona,'destinatario'=>$mensaje->emisor)));
        $invitacion = InvitacionProyecto::model()->updateByPk($mensaje->id, array('aceptada' => -1));
        if($invitacion>0)
            user()->setFlash('success','Se ha rechazado correctamente la invitacion.');
        else
            user()->setFlash('error', 'Ha ocurrido un error. Por favor, int&eacute;ntalo nuevamente.');
        $this->redirect(array('/buzon/inbox'));
    }

    /**
     * Filtros del controlador.
     *
     * @return  array array de filtros que el controlador utiliza.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Reglas de acceso del controlador.
     *
     * @return  array array de reglas de acceso que el controlador utiliza.
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('cambiarJefeProyecto','aceptarSolicitudCambioJefeProyecto', 'rechazarSolicitudCambioJefeProyecto'),
                'roles' => array('usuario'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Evento enviado al contestar la invitacion a ser jefe de proyecto.
     *
     * @param CEvent $event evento
     *
     * @return  void
     */
    public function onContestarInvitacion($event){
        $this->raiseEvent('onContestarInvitacion', $event);
    }
}
