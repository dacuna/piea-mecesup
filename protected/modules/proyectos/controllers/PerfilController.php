<?php
Yii::import('application.modules.usuarios.components.Entidad');
Yii::import('application.modules.formularios.models.ItemPresupuestoPostulacion');
Yii::import('application.modules.formularios.models.ObservacionItemPresupuesto');
Yii::import('application.modules.formularios.models.HitoProyecto');
Yii::import('application.modules.formularios.models.MesHito');
Yii::import('application.modules.formularios.models.ReferentesProyecto');
Yii::import('application.modules.formularios.models.ProyectoSeleccionado');
/**
 * Controlador que contiene las acciones necesarias para desplegar toda la informacion
 * de un proyecto en particular..
 *
 * El presente controlador posee acciones para visualizar a los integrantes de un proyecto,
 * el presupuesto de este, los hitos entre otras cosas. Cada accion puede ser ejecutada por
 * los usuarios que posean los permisos necesarios para visualizar la accion (pueden ser
 * distintos por accion). Lo importante a notar es que no cualquier usuario puede observar
 * la informacion de un proyecto.
 *
 * @category  Controller
 * @package   Proyectos
 * @version   1.0
 * @since     2012-07-07
 * @author    dacuna <diego.acuna@usm.cl>
 */
class PerfilController extends Controller
{
    /**
     * Layout utilizado por el controlador (por defecto aplicado a todas las acciones)
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Accion que despliega el perfil de un determinado proyecto (referenciado por $id). Este
     * perfil corresponde a los datos basicos del proyecto agregando tambien los referentes
     * del proyecto e informacion del proceso de postulacion I+D+i si es que corresponde (es decir
     * solo para los proyectos que postularon al fondo I+D+i).
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   08-06-2013
     */
    public function actionIndex($id){
        $proyecto=Proyecto::load($id);
        $entidad=new Entidad($proyecto->id, 1);
        if($entidad->checkPermisosBoolean('integrante',user()->id) ||
            user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}")){
            $this->render('ver_perfil',array(
                'entidad'=>$entidad,
                'model'=>$proyecto,
                'perfil'=>$proyecto->perfilPostulacion,
                'referentes'=>$proyecto->referentes,
                'proyecto' => $proyecto
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ver
            esta p&aacute;gina.");
    }

    /**
     * Accion que permite visualizar los integrantes de un proyecto. Si quien visualiza esta pagina
     * no es miembro del proyecto entonces solo podra ver a los integrantes que explicitamente
     * hayan marcado la opcion de aparecer en los listados publicos del proyecto. El permiso
     * minimo para ver el listado completo de integrantes es ser un integrante del proyecto.
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   2012-07-07
     */
    public function actionIntegrantes($id)
    {
        $model = ProyectoModel::load($id);
        if (!user()->checkAccess('integrante_proyecto', array('iniciativa' => $model->iniciativa_id,
            'proyecto' => $model->id)) && !user()->checkAccess("ayudante_hitos_proyecto_{$model->id}"))
            throw new CHttpException(403, 'No tienes los permisos necesarios para ver esta p&aacute;gina.');
        $entidad=new Entidad($model->id, 1);

        $this->render('integrantes', array(
            'model' => $model,
            'entidad' => $entidad
        ));
    }

    /**
     * Accion que despliega el presupuesto de un proyecto (si es que este posee). Notar que por
     * defecto los proyectos que nacen desde una postulacion I+D+i poseen un presupuesto asociado
     * que sirve como referencia para la direccion de PIEA. Los proyectos creados directamente
     * desde una iniciativa no poseen un presupuesto asociado. Ademas, los usuarios con permisos
     * de administrador y ayudante de investigacion pueden ver los presupuestos modificados por
     * cada revisor que reviso al presente proyecto.
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   04-06-2013
     */
    public function actionVerPresupuesto($id){
        $proyecto=Proyecto::load($id);
        $entidad=new Entidad($proyecto->id, 1);
        if($entidad->checkPermisosBoolean('integrante',user()->id) ||
            user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}")){
            $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
            $total=array(0,0,0,0,0);
            $items_cat_total=array(array(0,0,0,0,0,0),array(0,0,0,0,0,0),array(0,0,0,0,0,0),array(0,0,0,0,0,0));
            $items_cat=array(array(),array(),array(),array());
            foreach ($items_presupuesto as $item) {
                //se debe cargar tambien los montos aprobados por PIEA, recordar que para esto se obtiene la observacion
                //que hizo olga.godoy en cada proyecto
                $obs=ObservacionItemPresupuesto::load(1,$item->id,false);
                $total[0]+=$item->precio*$item->unidades;
                $total[1]+=$item->idi;
                $total[2]+=$item->propio;
                $total[3]+=$item->otros;
                $total[4]+=($obs!=null) ? $obs->monto_idi_editado : 0;
                $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
                $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
                $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
                $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
                $items_cat_total[$item->id_categoria-1][4]+=($obs!=null) ? $obs->monto_idi_editado : 0;
                //se calcula lo real adjudicado por categoria
                $items_cat_total[$item->id_categoria-1][5]+=($obs!=null) ? $obs->monto_idi_editado : $item->idi;
                $items_cat[$item->id_categoria-1][]=array($item,$obs);
            }
            $presupuesto=array('items'=>$items_cat,'totales_categoria'=>$items_cat_total,'totales'=>$total);
            //se agrega el total asignado obtenido desde la seleccion del proyecto
            $seleccion=ProyectoSeleccionado::model()->findByPk($proyecto->id);
            $total_adjudicado=($seleccion!=null) ? $seleccion->monto_idi : 0;

            $this->render('ver_presupuesto', array(
                'model' => $proyecto,
                'entidad' => $entidad,
                'presupuesto' => $presupuesto,
                'total_adjudicado' => $total_adjudicado
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ver
            esta p&aacute;gina.");
    }

    /**
     * Accion que muestra los hitos de un proyecto en particular. Notar que por
     * defecto los proyectos que nacen desde una postulacion I+D+i poseen hitos asociados
     * que sirve como referencia para la direccion de PIEA. Los proyectos creados directamente
     * desde una iniciativa no poseen hitos asociados (pero si los pueden crear).
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   08-06-2013
     */
    public function actionVerHitos($id){
        $proyecto=Proyecto::load($id);
        $entidad=new Entidad($proyecto->id, 1);
        if($entidad->checkPermisosBoolean('integrante',user()->id) ||
            user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}")){
            $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1',array(':ip'=>$proyecto->id));
            $nuevo=new HitoProyecto;
            $this->render('ver_hitos',array(
                'hitos'=>$hitos,
                'entidad'=>$entidad,
                'model'=>$proyecto,
                'nuevo'=>$nuevo
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ver
            esta p&aacute;gina.");
    }

    /**
     * Accion que muestra la carta gantt de un proyecto en particular. Notar que por
     * defecto los proyectos que nacen desde una postulacion I+D+i poseen una carta gantt asociada
     * que sirve como referencia para la direccion de PIEA. Los proyectos creados directamente
     * desde una iniciativa no poseen carta gantt asociada (pero si pueden crear una).
     *
     * @param int $id ID del proyecto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   08-06-2013
     */
    public function actionVerCartaGantt($id){
        $proyecto=Proyecto::load($id);
        $entidad=new Entidad($proyecto->id, 1);
        if($entidad->checkPermisosBoolean('integrante',user()->id) ||
            user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}")){
            $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC,es_hito DESC',array(':ip'=>$proyecto->id));
            $model=new HitoProyecto;
            $this->render('ver_carta_gantt',array(
                'hitos'=>$hitos,
                'entidad'=>$entidad,
                'proyecto'=>$proyecto,
                'model'=>$model
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos suficientes para ver
            esta p&aacute;gina.");
    }

    /**
     * Accion para agregar hitos a un proyecto. El metodo preferido para agregar hitos
     * es mediante una peticion ajax, es por esto que la vista es construida teniendo dicho requerimiento
     * en mente.
     *
     * @param int $id ID del proyecto al cual se le desea agregar un hito
     *
     * @return  string JSON con el mensaje final de la funcion.
     * @throws  \CHttpException
     *
     * @since   11-06-2013
     */
    public function actionAgregarHito($id){
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::load($id,true);
            $entidad=new Entidad($proyecto->id, 1);
            $entidad->checkPermisos('encargado',user()->id);
            $model=new HitoProyecto;
            $model->setScenario('agregarHito');
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=array('mensaje'=>'Se ha agregado correctamente el hito al proyecto.');
                $return['nombre']=$model->nombre;
                $return['descripcion']=$model->descripcion;
                $return['id']=$model->primaryKey;
                $return['fecha']=$model->fecha;
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al agregar el hito:');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion para editar la fecha de un hito en un proyecto. El metodo preferido para editar hitos
     * es mediante una peticion ajax, es por esto que la vista es construida teniendo dicho requerimiento
     * en mente.
     *
     * @param int $id ID del proyecto al cual se le desea editar un hito
     *
     * @return  string JSON con el mensaje final de la funcion.
     * @throws  \CHttpException
     *
     * @since   24-06-2013
     */
    public function actionEditarHito($id){
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::load($id,true);
            $entidad=new Entidad($proyecto->id, 1);
            $entidad->checkPermisos('encargado',user()->id);
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model=HitoProyecto::load($objData['id'],$id);
            $model->fecha=$objData['fecha'];
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=array('mensaje'=>'Se ha editar correctamente el hito al proyecto.');
                $return['nombre']=$model->nombre;
                $return['descripcion']=$model->descripcion;
                $return['id']=$model->primaryKey;
                $return['fecha']=$model->fecha;
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al editar el hito:');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite eliminar un hito de un proyecto mediante una peticion
     * GET, la respuesta es un objeto JSON con el mensaje ya sea de exito o de
     * fracaso. Para ejecutar la accion, se deben tener los permisos necesarios.
     *
     * @param int $id ID del proyecto donde se desea eliminar un hito
     * @param int $hito ID del hito a eliminar.
     *
     * @return  string respuesta JSON con mensaje correspondiente al status de la
     *                 peticion
     * @throws  \CHttpException
     * @since   11-06-2013
     */
    public function actionEliminarHito($id,$hito){
        $proyecto=Proyecto::load($id);
        $entidad=new Entidad($proyecto->id, 1);
        $entidad->checkPermisos('encargado',user()->id);
        $model=HitoProyecto::load($hito,$id);
        if($model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente el hito del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el hito. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para agregar una actividad en la carta gantt a un proyecto.
     * La respuesta es un string JSON con el objeto ingresado o con mensaje de error segun
     * sea el caso.
     *
     * @param int $id ID del proyecto
     *
     * @return  string JSON con la respuesta de la operacion
     * @throws  \CHttpException
     *
     * @since   11-06-2013
     */
    public function actionAgregarActividad($id){
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::load($id,true);
            $entidad=new Entidad($proyecto->id, 1);
            $entidad->checkPermisos('encargado',user()->id);
            $model=new HitoProyecto;
            $model->setScenario('agregarActividad');
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            $model->es_hito=0;
            if(count($objData['meses'])!=0 && $model->save()){
                //se agregan los meses
                foreach ($objData['meses'] as $mes) {
                    if(isset($mes['status'],$mes['id']) && $mes['status']==true && is_numeric($mes['id']) &&  (0<=$mes['id']) && ($mes['id']<=13)){
                        $newMonth=new MesHito;
                        $newMonth->id_hito=$model->primaryKey;
                        $newMonth->mes=$mes['id'];
                        $newMonth->save(false);
                    }
                }
                //se envia nuevo modelo junto con mensaje de exito
                $return=array('mensaje'=>'Se ha agregado correctamente el hito al proyecto.');
                $return['nombre']=$model->nombre;
                $return['id']=$model->primaryKey;
                $return['fecha']=$model->fecha;
                $return['es_hito']=$model->es_hito;
                $return['meses']=$model->getMeses();
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al agregar el hito (verifica que has seleccionado algun mes):');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion para editar una actividad en la carta gantt a un proyecto.
     * La respuesta es un string JSON con el objeto ingresado o con mensaje de error segun
     * sea el caso.
     *
     * @param int $id ID del proyecto
     *
     * @return  string JSON con la respuesta de la operacion
     * @throws  \CHttpException
     *
     * @since   11-06-2013
     */
    public function actionEditarActividad($id){
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::load($id,true);
            $entidad=new Entidad($proyecto->id, 1);
            $entidad->checkPermisos('encargado',user()->id);
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model=HitoProyecto::load($objData['id'],$id);
            if(count($objData['meses'])==14){
                $transaction=app()->db->beginTransaction();
                try {
                    //se deben eliminar los meses actuales
                    foreach ($model->mesesAR as $mes)
                        $mes->delete();
                    //ahora se insertan los meses nuevos
                    foreach ($objData['meses'] as $key=>$mes) {
                        if($mes==true){
                            $newMonth=new MesHito;
                            $newMonth->id_hito=$model->id;
                            $newMonth->mes=$key;
                            $newMonth->save(false);
                        }
                    }
                    $transaction->commit();
                    //se envia nuevo modelo junto con mensaje de exito
                    $return=array('mensaje'=>'Se ha editado correctamente la actividad del proyecto.');
                    $return['nombre']=$model->nombre;
                    $return['id']=$model->id;
                    $return['fecha']=$model->fecha;
                    $return['es_hito']=$model->es_hito;
                    $return['meses']=$model->getMeses();
                    $this->renderJSON($return);
                } catch(CException $e) {
                    $transaction->rollback();
                    //crear un array con errores
                    $return=array('mensaje'=>'Ha ocurrido un error al actualizar los meses. Por favor intentalo nuevamente.');
                    $this->renderJSON($return);
                    Yii::log("'Ha ocurrido un error: al actualizar la actividad. PROYECTO ID: {$id}, ERROR:
                    {$e->getMessage()}","error","app.modules.proyecos.PerfilController.editarActividad");
                }
            }else{
                $return=array('mensaje'=>'Debes seleccionar los meses nuevos para la actividad a editar.');
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion para eliminar una actividad de la carta gantt de un proyecto. Solo se permite
     * eliminar actividades (que no sean hitos). Se retorna un string JSON con un mensaje
     * de exito/error segun sea necesario. Para ejecutar la accion se deben poseer los permisos
     * correspondientes.
     *
     * @param int $id ID del proyecto
     * @param int $act ID de la actividad a eliminar
     *
     * @return  string JSON con mensaje de exito/error segun sea necesario
     * @throws  \CHttpException
     *
     * @since   11-06-2013
     */
    public function actionEliminarActividad($id,$act){
        $proyecto=Proyecto::load($id,true);
        $entidad=new Entidad($proyecto->id, 1);
        $entidad->checkPermisos('encargado',user()->id);
        $model=HitoProyecto::load($act,$id);
        if($model->es_hito!=1 && $model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente la actividad del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el hito. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

}