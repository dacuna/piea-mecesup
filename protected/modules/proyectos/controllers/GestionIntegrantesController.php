<?php
Yii::import('application.modules.iniciativas.models.LookupIniciativa');
/**
 * Controlador que contiene la logica para invitar y aceptar/rechazar invitaciones de
 * membresia dentro de un proyecto.
 *
 * @package   modules.proyectos.controllers
 * @version   1.0
 * @since     2012-07-22
 * @author    dacuna <diego.acuna@usm.cl>
 * @todo al 29 de julio de 2013 este controlador esta sujeto a grandes refactorizaciones
 */
class GestionIntegrantesController extends Controller
{
    /**
     * Layout por defecto del controladr.
     * @var string
     */
    public $layout = '//layouts/column2';

    /**
     * Accion que permite que un usuario externo a un proyecto solicite
     * una membresia dentro de este. Para esto se envia una notificacion
     * al jefe de proyecto para que acepte o rechaze la membresia.
     *
     * @param int id id del proyecto al que se solicita acceso
     *
     * @return  void
     * @since   01-06-2012
     */
    public function actionSolicitarMembresia($id)
    {
        $proyecto = ProyectoModel::load($id);
        $solicitud = new SolicitudMembresia;
        $solicitud->usuario_id = user()->id;
        $solicitud->proyecto = $proyecto;
        if ($solicitud->enviarSolicitud())
            Yii::app()->user->setFlash('success', 'Tu solicitud ha sido enviada con &eacute;xito. El jefe de proyecto te
                responder&aacute; a la brevedad.');
        else
            Yii::app()->user->setFlash('error', 'Ha ocurrido un error al crear la solicitud. Por favor, int&eacute;ntalo
                nuevamente.');
        $this->redirect(array('/proyectos/crud/explorar', 'id' => $proyecto->id));
    }

    /**
     * Accion que permite a un jefe de proyecto aceptar una solicitud
     * de un usuario para formar parte del equipo de un proyecto
     *
     * @param int id id del mensaje que contiene la solicitud
     *
     * @return  void
     * @throws  \CHTTPException
     *
     * @since   01-06-2012
     *
     * @edit    06-01-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Refactorizacion en metodos de obtener mensajes y proyectos, se agrega funcion
     *          IntegranteProyecto::agregarIntegrante
     *          #edit989f9ab3e912f03b90347bd3ba68d36500f38343
     */
    public function actionAceptarSolicitud($id)
    {
        $mensaje = Mensaje::obtenerMensajeDeUsuario($id);
        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        $proyecto = ProyectoModel::load($mensaje->id_relacion);
        //verifico que esta accion solo la pueda ejecutar el coordinador de la iniciativa
        if (!user()->checkAccess('jefe_proyecto', array('iniciativa' => $proyecto->iniciativa_id, 'proyecto' => $proyecto->id)))
            throw new CHttpException(500, 'No tienes los permisos suficientes para acceder a esta p&aacute;gina.');

        $usuario = Persona::load($mensaje->emisor);
        //se crea entonces al nuevo integrante
        $result = IntegranteProyecto::agregarIntegrante($usuario->id, $proyecto->id);
        if (!$result)
            user()->setFlash('error', 'Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente.');
        else {
            //tambien debo marcar el mensaje como contestado y leido
            $mensaje->contestarMensaje('Aceptar solicitud');
            user()->setFlash('success', "Se ha aceptado correctamente al usuario {$usuario->nombreCompleto} como
                integrante en el proyecto {$proyecto->nombre}");
        }
        $this->redirect(array('/buzon/inbox/index'));
    }


    /**
     * Accion que ejecuta la logica cuando un usuario acepta la invitacion
     * a coordinar una iniciativa.
     *
     * @param int $e emisor del mensaje de invitacion
     * @param int $d destinatario del mensaje de invitacion
     * @param int $uid identificador unico del mensaje de invitacion
     *
     * @return  void
     * @throws  \CHTTPException
     *
     * @since   01-06-2012
     * @edit    06-01-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Agregado metodo en ProyectoModel para asignar jefe de proyecto.
     * @edit    10-01-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Refactorizado sistema de recepcion de la notificacion.
     */
    public function actionAceptarInvitacion($e,$d,$uid)
    {
        //se obtiene el mensaje correspondiente
        $mensaje = Mensaje::obtenerMensaje($e,$d,$uid);
        $usuario_id = Yii::app()->user->id;
        if($mensaje->destinatario!=user()->id) throw new CHttpException(403,"No tienes los permisos necesarios
            para leer este mensaje.");
        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }
        //verifico que efectivamente corresponde a una invitacion a coordinar iniciativa, lo contrario
        //nunca deberia pasar pero siempre hay que estar paranoic_mode ON
        if ($mensaje->tipo_relacion != 'proyecto')
            throw new CHttpException(403, 'Solicitud incorrecta. Por favor, no vuelva a realizar la misma petici&oacute;n.');
        $proyecto = ProyectoModel::load($mensaje->id_relacion);
        $proyecto->asignarJefeProyecto($usuario_id);
        //se debe obtener la invitacion correspondiente para marcarla como aceptada
        InvitacionProyecto::model()->updateByPk($mensaje->id, array('aceptada' => 1));
        //tambien debo marcar el mensaje como contestado y leido
        $mensaje->contestarMensaje('Aceptar invitacion');

        user()->setFlash('success', "Felicidades, ahora eres el nuevo jefe del proyecto {$proyecto->nombre}");
        $this->redirect(array('/proyectos/crud/ver','id' => $proyecto->id));
    }

    /**
     * Accion que ejecuta la logica cuando un usuario decide rechazar ya sea una
     * invitacion o solicitud. En este caso se marca el mensaje como contestado y
     * si es una invitacion entonces se marca como rechazada.
     *
     * @param Integer id la id de la notificacion de invitacion a coordinacion de iniciativa
     */
    public function actionRechazarSolicitud($id)
    {
        $mensaje = Mensaje::obtenerMensajeDeUsuario($id);
        $usuario_id = Yii::app()->user->id;

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        //veo si es que es una invitacion
        $invitacion = InvitacionProyecto::model()->findByPk($mensaje->id);
        if ($invitacion != null) {
            $invitacion->aceptada = -1;
            $invitacion->save(false);
        }
        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar solicitud';
        $mensaje->save(false);

        Yii::app()->user->setFlash('success', 'Se ha rechazado correctamente la notificaci&oacute;n.');
        $this->redirect(array('/buzon/inbox/index'));
    }

    /**
     * Permite a un usuario rechazar la solicitud de membresia a un proyecto en particular (invitacion a ser
     * parte como integrante de un proyecto).
     *
     * @param int $id la id de la notificacion de solicitud de membresia
     * @throws CHttpException
     */
    public function actionRechazarSolicitudMembresia($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        $usuario_id = Yii::app()->user->id;

        if ($mensaje == null || $mensaje->destinatario != $usuario_id)
            throw new CHttpException(403, 'No tienes los permisos para rechazar esta solicitud. Tu acci&oacute;n ser&aacute; notificada al administrador');

        //se verifica que el mensaje no este contestado, si esta contestado se envia un mensaje de error
        if ($mensaje->contestado == 1) {
            Yii::app()->user->setFlash('error', 'Esta notificaci&oacute;n ya ha sido contestada. No puede volver a contestarla.');
            $this->redirect(array('/buzon/inbox/verMensaje', 'id' => $mensaje->id));
        }

        $mensaje->contestado = 1;
        $mensaje->accion_realizada = 'Rechazar invitacion';
        $mensaje->save(false);

        //notifico al emisor que la actividad ha sido rechazada
        $notificacion = new AvisoRechazoSolicitud;
        $notificacion->rechazo = Persona::model()->findByPk($usuario_id);
        $notificacion->invito = Persona::model()->findByPk($mensaje->emisor);
        $notificacion->proyecto = Proyecto::model()->findByPk($mensaje->id_relacion);
        $notificacion->enviarSolicitud();

        Yii::app()->user->setFlash('success', 'Se ha rechazado correctamente la solicitud de membres&iacute;a');
        $this->redirect(array('/buzon/inbox/index'));
    }

}