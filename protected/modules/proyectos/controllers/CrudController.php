<?php
Yii::import('application.modules.usuarios.components.Entidad');

/**
 * Controlador que contiene las acciones principales para manejar el modelo de Proyectos del sistema.
 * Dichas acciones son aquellas conocidas por su denominacion CRUD.
 *
 * Las acciones son accesibles desde la url /proyectos/crud/nombreMetodo. Se debe destacar la accion
 * ver la cual diferencia entre proyectos creados directamente desde una iniciativa y proyectos que
 * fueron creados como una postulacion a los fondos I+D+i, los cuales poseen visualizaciones distintas.
 *
 * @category  Controller
 * @package   Proyectos
 * @version   1.0
 * @since     2012-05-07
 * @author    dacuna <diego.acuna@usm.cl>
 */
class CrudController extends Controller
{
    /**
     * Accion que lista para los usuarios administradores la totalidad de proyectos segmentados por
     * iniciativa y para los usuarios comunes lista sus proyectos.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo    Implementar esta funcion
     *
     * @since   -
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Accion que permite al usuario con los permisos correspondientes (crear_proyecto) el crear
     * un proyecto en el sistema y asociado a la entidad enviada por formulario.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   2012-05-07
     */
    public function actionCrear()
    {
        if (!Yii::app()->user->checkAccess('crear_proyecto'))
            throw new CHttpException(403, 'No tienes los permisos necesarios para ingresar a esta p&aacute;gina.');

        $model = new ProyectoModel;

        if (isset($_POST['ProyectoModel'])) {
            $model->attributes = $_POST['ProyectoModel'];
            if ($model->save())
                $this->redirect(array('ver', 'id' => $model->id));
        }

        $this->render('crear', array(
            'model' => $model,
        ));
    }

    /**
     * Accion que permite ver el detalle de un proyecto perteneciente a una iniciativa en particular. Se
     * agrega la funcionalidad para cargar una visualizacion minima del proyecto mediante ajax y ademas
     * la funcionalidad para invitar a un usuario a ser jefe de proyecto (solo lo puede hacer un usuario
     * autorizado).
     *
     * @param int $id ID del proyecto a visualizar.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo    Mover funcionalidad de InvitarJefeProyecto a una accion por separada.
     *
     * @since   2012-05-30
     */
    public function actionVer($id)
    {
        $model = ProyectoModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'El proyecto buscado no existe en nuestros registros.');

        $invitar_jefe = new InvitarJefeProyectoForm;
        //formulario para invitar a un jefe de proyecto
        if (isset($_POST['InvitarJefeProyectoForm'])) {
            $invitar_jefe->attributes = $_POST['InvitarJefeProyectoForm'];
            if ($invitar_jefe->validate()) {
                //si es que el usuario logeado es coordinador de la iniciativa a la que pertenece el proyecto, entonces
                //automaticamente es seteado como el jefe de proyecto (si es que se eligio a si mismo)
                if ($invitar_jefe->id_usuario == user()->id) {
                    $auth = Yii::app()->authManager;
                    //verifico que no sea coordinador de otro proyecto, si lo es entonces ya tiene el permiso asignado
                    if ($auth->getAuthAssignment("jefe_proyecto", user()->id) == null) {
                        $admin = $auth->assign('jefe_proyecto', user()->id);
                    }
                    //seteo el lookup de la iniciativa correspondiente
                    $lookup = new LookupProyecto;
                    $lookup->nombre_item = 'jefe_proyecto';
                    $lookup->proyecto_id = $invitar_jefe->id_proyecto;
                    $lookup->user_id = user()->id;
                    $lookup->save(false);

                    //se debe tambien ingresar el integrante nuevo
                    $integrante = new IntegranteProyecto;
                    $integrante->proyecto_id = $invitar_jefe->id_proyecto;
                    $integrante->user_id = user()->id;
                    $integrante->save(false);
                    user()->setFlash('success', 'Felicidades, ahora eres el nuevo jefe del proyecto ' . $model->nombre);
                } else {
                    try{
                        $invitar_jefe->enviarInvitacion();
                        user()->setFlash('success', 'Se ha enviado correctamente la invitaci&oacute;n.');
                    }catch(CHttpException $e){
                        user()->setFlash('error', 'Ha ocurrido un error al enviar la invitaci&oacute;n. por
                            favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al
                            administrador.');
                    }
                }
                $this->refresh();
            }
        }

        if (isset($_GET['ajax']))
            $this->RenderPartial('_ver', array('model' => $model), false, false);
        else
            $this->render('ver', array(
                'model' => $model,
                'invitar_jefe' => $invitar_jefe,
                'entidad' => new Entidad($model->id, 1)
            ));
    }

    /**
     * Accion disenada para el menu "Explorar PIEA". Basicamente se muestra una pantalla con informacion
     * reducida del proyecto y que permite a usuarios que no pertenecen al proyecto conocer sobre que
     * trata dicho proyecto.
     *
     * @param int $id ID del proyecto a visualizar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   2012-05-30
     */
    public function actionExplorar($id)
    {
        $model = ProyectoModel::model()->findByPk($id);
        if ($model == null)
            throw new CHttpException(404, 'El proyecto buscado no existe en nuestros registros.');

        $integrantes = new CActiveDataProvider('IntegranteProyecto', array(
            'criteria' => array(
                'condition' => 'proyecto_id=' . $model->id . ' and visibilidad=1',
                'order' => 'id ASC'
            )
        ));

        $this->render('explorar', array(
            'model' => $model,
            'integrantes' => $integrantes
        ));
    }

    /**
     * Accion que permite crear un proyecto en una determinada iniciativa (conocida mediante el parametro
     * $id). La diferencia principal con la accion "Crear" radica en que en esta funcion se crea el proyecto
     * directamente mediante una iniciativa.
     *
     * @param int $id ID de la iniciativa donde se va a crear el proyecto.
     *
     * @return  void
     * @throws  \CHttpException
     * @todo    A futuro se puede unir esta funcion con la funcion principal de crear proyecto.
     *
     * @since   2012-05-30
     */
    public function actionCrearProyecto($id)
    {
        $iniciativa = Iniciativa::model()->findByPk($id);
        if ($iniciativa == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');

        if (!Yii::app()->user->checkAccess('coordinador_iniciativa', array('iniciativa' => $iniciativa->id)))
            throw new CHttpException(500, 'No tienes los permisos necesarios para ingresar a esta p&aacute;gina.');

        $model = new ProyectoModel;

        if (isset($_POST['ProyectoModel'])) {
            $model->attributes = $_POST['ProyectoModel'];
            $model->iniciativa_id = $iniciativa->id;
            if ($model->save())
                $this->redirect(array('ver', 'id' => $model->id));
        }

        $this->render('crearProyecto', array(
            'model' => $model,
            'iniciativa' => $iniciativa
        ));
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el editar la informacion general
     * de un proyecto.
     *
     * @param int $id ID del proyecto a editar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   2012-05-30
     */
    public function actionEditar($id)
    {
        $model = Proyecto::load($id);
        if (!user()->checkAccess('jefe_proyecto', array('iniciativa' => $model->iniciativa_id, 'proyecto' => $model->id)))
            throw new CHttpException(403, 'El sistema ha detectado que no posees los permisos necesarios para ver esta
        p&aacute;gina.');

        if (isset($_POST['Proyecto'])) {
            $model->attributes = $_POST['Proyecto'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Se ha editado correctamente el proyecto.');
                $this->redirect(array('ver', 'id' => $model->id));
            }
        }

        $this->render('editar', array(
            'model' => $model
        ));
    }

    /**
     * Accion que permite obtener los proyectos donde un usuario es integrante mediante una peticion ajax. La
     * respuesta es un mensaje JSON con el listado de proyectos con el id, nombre y iniciativa a la que pertenece
     * el proyecto.
     *
     * @return  string Representacion JSON de los proyectos del usuario
     * @throws  \CHttpException
     *
     * @since   2012-05-30
     */
    public function actionProyectosUsuario()
    {
        $preini = user()->proyectos();
        $ret = array();
        foreach ($preini as $proyecto) {
            array_push($ret, array('id' => $proyecto->id, 'nombre' => $proyecto->nombre, 'padre' => $proyecto->iniciativa_id));
        }
        echo CJSON::encode($ret);
        app()->end();
    }

}