<?php

/**
 * Modelo para la tabla "{{revision_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{revision_proyecto}}':
 * @property integer $id_proyecto
 * @property integer $id_revisor
 * @property integer $extracto_ortografia
 * @property integer $extracto_capacidad_sintesis
 * @property integer $extracto_redaccion
 * @property integer $resumen_ortografia
 * @property integer $resumen_capacidad_sintesis
 * @property integer $resumen_redaccion
 * @property integer $objetivos_ortografia
 * @property integer $objetivos_capacidad_sintesis
 * @property integer $objetivos_redaccion
 * @property integer $objetivos_factibilidad
 * @property integer $objetivos_alcanzabilidad
 * @property integer $estado_del_arte_pertinencia
 * @property integer $estado_del_arte_acceso
 * @property integer $estado_del_arte_ortografia
 * @property integer $estado_del_arte_capacidad_sintesis
 * @property integer $estado_del_arte_redaccion
 * @property integer $experiencia_previa_ortografia
 * @property integer $experiencia_previa_capacidad_sintesis
 * @property integer $experiencia_previa_redaccion
 * @property integer $investigacion_presencia
 * @property integer $investigacion_ortografia
 * @property integer $investigacion_capacidad_sintesis
 * @property integer $investigacion_redaccion
 * @property integer $desarrollo_presencia
 * @property integer $desarrollo_ortografia
 * @property integer $desarrollo_capacidad_sintesis
 * @property integer $desarrollo_redaccion
 * @property integer $innovacion_presencia
 * @property integer $innovacion_ortografia
 * @property integer $innovacion_capacidad_sintesis
 * @property integer $innovacion_redaccion
 * @property integer $impacto_academico_nota
 * @property integer $impacto_academico_ortografia
 * @property integer $impacto_academico_capacidad_sintesis
 * @property integer $impacto_academico_redaccion
 * @property integer $impacto_universitario_social_nota
 * @property integer $impacto_universitario_social_ortografia
 * @property integer $impacto_universitario_social_capacidad_sintesis
 * @property integer $impacto_universitario_social_redaccion
 * @property integer $proyecciones_nota
 * @property integer $proyecciones_ortografia
 * @property integer $proyecciones_capacidad_sintesis
 * @property integer $proyecciones_redaccion
 * @property integer $equipo_trabajo_presencia_especialidades
 * @property integer $equipo_trabajo_asignacion_tareas
 * @property integer $referentes_nota
 * @property integer $hitos_alcanzables
 * @property integer $hitos_realizables
 * @property integer $carta_gantt_tareas_apropiadas
 * @property integer $carta_gantt_tiempos_apropiados
 * @property integer $anexos_pertinencia
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 * @property string $extracto_comentario
 * @property string $resumen_comentario
 * @property string $objetivos_comentario
 * @property string $estado_del_arte_comentario
 * @property string $experiencia_previa_comentario
 * @property string $investigacion_comentario
 * @property string $desarrollo_comentario
 * @property string $innovacion_comentario
 * @property string $impacto_academico_comentario
 * @property string $impacto_universitario_social_comentario
 * @property string $proyecciones_comentario
 * @property string $equipo_trabajo_comentario
 * @property string $referentes_comentario
 * @property string $hitos_comentario
 * @property string $carta_gantt_comentario
 * @property string $presupuesto_comentario
 * @property string $anexos_comentario
 */
class RevisionProyecto extends CActiveRecord
{
    public $errores_revision=array();
    private $total_revisiones_items_presupuesto=0;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RevisionProyecto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{revision_proyecto}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('extracto_ortografia, extracto_capacidad_sintesis, extracto_redaccion, resumen_ortografia, resumen_capacidad_sintesis, resumen_redaccion, objetivos_ortografia, objetivos_capacidad_sintesis, objetivos_redaccion, objetivos_factibilidad, objetivos_alcanzabilidad, estado_del_arte_pertinencia, estado_del_arte_acceso, estado_del_arte_ortografia, estado_del_arte_capacidad_sintesis, estado_del_arte_redaccion, experiencia_previa_ortografia, experiencia_previa_capacidad_sintesis, experiencia_previa_redaccion, investigacion_presencia, investigacion_ortografia, investigacion_capacidad_sintesis, investigacion_redaccion, desarrollo_presencia, desarrollo_ortografia, desarrollo_capacidad_sintesis, desarrollo_redaccion, innovacion_presencia, innovacion_ortografia, innovacion_capacidad_sintesis, innovacion_redaccion, impacto_academico_nota, impacto_academico_ortografia, impacto_academico_capacidad_sintesis, impacto_academico_redaccion, impacto_universitario_social_nota, impacto_universitario_social_ortografia, impacto_universitario_social_capacidad_sintesis, impacto_universitario_social_redaccion, proyecciones_nota, proyecciones_ortografia, proyecciones_capacidad_sintesis, proyecciones_redaccion, equipo_trabajo_presencia_especialidades, equipo_trabajo_asignacion_tareas, referentes_nota, hitos_alcanzables, hitos_realizables,hitos_ortografia,hitos_capacidad_sintesis,hitos_redaccion, carta_gantt_tareas_apropiadas, carta_gantt_tiempos_apropiados,carta_gantt_ortografia,carta_gantt_capacidad_sintesis,carta_gantt_redaccion,anexos_pertinencia', 'numerical', 'integerOnly' => true),
            array('extracto_comentario, resumen_comentario, objetivos_comentario, estado_del_arte_comentario, experiencia_previa_comentario, investigacion_comentario, desarrollo_comentario, innovacion_comentario, impacto_academico_comentario, impacto_universitario_social_comentario, proyecciones_comentario, equipo_trabajo_comentario, referentes_comentario, hitos_comentario, carta_gantt_comentario, presupuesto_comentario, anexos_comentario', 'safe'),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
            'revisor' => array(self::BELONGS_TO, 'Persona', 'id_revisor'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_proyecto' => 'Id Proyecto',
            'id_revisor' => 'Id Revisor',
            'extracto_ortografia' => 'Extracto Ortografia',
            'extracto_capacidad_sintesis' => 'Extracto Capacidad Sintesis',
            'extracto_redaccion' => 'Extracto Redaccion',
            'resumen_ortografia' => 'Resumen Ortografia',
            'resumen_capacidad_sintesis' => 'Resumen Capacidad Sintesis',
            'resumen_redaccion' => 'Resumen Redaccion',
            'objetivos_ortografia' => 'Objetivos Ortografia',
            'objetivos_capacidad_sintesis' => 'Objetivos Capacidad Sintesis',
            'objetivos_redaccion' => 'Objetivos Redaccion',
            'objetivos_factibilidad' => 'Objetivos Factibilidad',
            'objetivos_alcanzabilidad' => 'Objetivos Alcanzabilidad',
            'estado_del_arte_pertinencia' => 'Estado Del Arte Pertinencia',
            'estado_del_arte_acceso' => 'Estado Del Arte Acceso',
            'estado_del_arte_ortografia' => 'Estado Del Arte Ortografia',
            'estado_del_arte_capacidad_sintesis' => 'Estado Del Arte Capacidad Sintesis',
            'estado_del_arte_redaccion' => 'Estado Del Arte Redaccion',
            'experiencia_previa_ortografia' => 'Experiencia Previa Ortografia',
            'experiencia_previa_capacidad_sintesis' => 'Experiencia Previa Capacidad Sintesis',
            'experiencia_previa_redaccion' => 'Experiencia Previa Redaccion',
            'investigacion_presencia' => 'Investigacion Presencia',
            'investigacion_ortografia' => 'Investigacion Ortografia',
            'investigacion_capacidad_sintesis' => 'Investigacion Capacidad Sintesis',
            'investigacion_redaccion' => 'Investigacion Redaccion',
            'desarrollo_presencia' => 'Desarrollo Presencia',
            'desarrollo_ortografia' => 'Desarrollo Ortografia',
            'desarrollo_capacidad_sintesis' => 'Desarrollo Capacidad Sintesis',
            'desarrollo_redaccion' => 'Desarrollo Redaccion',
            'innovacion_presencia' => 'Innovacion Presencia',
            'innovacion_ortografia' => 'Innovacion Ortografia',
            'innovacion_capacidad_sintesis' => 'Innovacion Capacidad Sintesis',
            'innovacion_redaccion' => 'Innovacion Redaccion',
            'impacto_academico_nota' => 'Impacto Academico Nota',
            'impacto_academico_ortografia' => 'Impacto Academico Ortografia',
            'impacto_academico_capacidad_sintesis' => 'Impacto Academico Capacidad Sintesis',
            'impacto_academico_redaccion' => 'Impacto Academico Redaccion',
            'impacto_universitario_social_nota' => 'Impacto Universitario Social Nota',
            'impacto_universitario_social_ortografia' => 'Impacto Universitario Social Ortografia',
            'impacto_universitario_social_capacidad_sintesis' => 'Impacto Universitario Social Capacidad Sintesis',
            'impacto_universitario_social_redaccion' => 'Impacto Universitario Social Redaccion',
            'proyecciones_nota' => 'Proyecciones Nota',
            'proyecciones_ortografia' => 'Proyecciones Ortografia',
            'proyecciones_capacidad_sintesis' => 'Proyecciones Capacidad Sintesis',
            'proyecciones_redaccion' => 'Proyecciones Redaccion',
            'equipo_trabajo_presencia_especialidades' => 'Equipo Trabajo Presencia Especialidades',
            'equipo_trabajo_asignacion_tareas' => 'Equipo Trabajo Asignacion Tareas',
            'referentes_nota' => 'Referentes Nota',
            'hitos_alcanzables' => 'Hitos Alcanzables',
            'hitos_realizables' => 'Hitos Realizables',
            'carta_gantt_tareas_apropiadas' => 'Carta Gantt Tareas Apropiadas',
            'carta_gantt_tiempos_apropiados' => 'Carta Gantt Tiempos Apropiados',
            'anexos_pertinencia' => 'Anexos Pertinencia',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_actualizacion' => 'Fecha Actualizacion',
            'extracto_comentario' => 'Extracto Comentario',
            'resumen_comentario' => 'Resumen Comentario',
            'objetivos_comentario' => 'Objetivos Comentario',
            'estado_del_arte_comentario' => 'Estado Del Arte Comentario',
            'experiencia_previa_comentario' => 'Experiencia Previa Comentario',
            'investigacion_comentario' => 'Investigacion Comentario',
            'desarrollo_comentario' => 'Desarrollo Comentario',
            'innovacion_comentario' => 'Innovacion Comentario',
            'impacto_academico_comentario' => 'Impacto Academico Comentario',
            'impacto_universitario_social_comentario' => 'Impacto Universitario Social Comentario',
            'proyecciones_comentario' => 'Proyecciones Comentario',
            'equipo_trabajo_comentario' => 'Equipo Trabajo Comentario',
            'referentes_comentario' => 'Referentes Comentario',
            'hitos_comentario' => 'Hitos Comentario',
            'carta_gantt_comentario' => 'Carta Gantt Comentario',
            'presupuesto_comentario' => 'Presupuesto Comentario',
            'anexos_comentario' => 'Anexos Comentario',
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_creacion',
                'updateAttribute' => 'fecha_actualizacion',
            )
        );
    }

    public static function cargarRevision($id, $uid)
    {
        $rev = RevisionProyecto::model()->find('id_proyecto=:ip and id_revisor=:ir', array(
            ':ip' => $id,
            ':ir' => $uid
        ));
        if ($rev == null) {
            $revision = new RevisionProyecto;
            $revision->id_proyecto = $id;
            $revision->id_revisor = $uid;
            return $revision;
        }
        return $rev;
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $proceso=FormularioPiea::procesoFormularioFondosIDI();
                //se cambia el estado de la revision para que figure como que al menos se comenzo a revisar el proyecto
                $revision=RevisorProyecto::model()->find('id_proyecto=:ip and id_usuario=:iu and id_proceso_formulario=:ipf',
                array(':ip'=>$this->id_proyecto,':iu'=>$this->id_revisor,':ipf'=>$proceso->id));
                $revision->estado_revision=1;
                $revision->save(false);
            }
            return true;
        } else
            return false;
    }

    public function afterSave(){
        parent::afterSave();
        //se actualiza la nota de la revision
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        //se cambia el estado de la revision para que figure como que al menos se comenzo a revisar el proyecto
        $revision=RevisorProyecto::model()->find('id_proyecto=:ip and id_usuario=:iu and id_proceso_formulario=:ipf',
            array(':ip'=>$this->id_proyecto,':iu'=>$this->id_revisor,':ipf'=>$proceso->id));
        $revision->nota=$this->calcularNotaRevision();
        //tambien se valida el estado de la revision
        $this->errores_revision=$this->validarRevision();
        if(count($this->errores_revision)==0)
            $revision->estado_revision=2;
        else
            $revision->estado_revision=1;
        $revision->save(false);
    }

    public function calcularNotaRevision(){
        $forma=$this->calcularNotaForma();
        $fondo=$this->calcularNotaFondo();
        $planificacion=$this->calcularNotaPlanificacion();
        $impacto=$this->calcularNotaImpacto();
        $a=array($forma,$fondo,$planificacion,$impacto);
        $mul=$forma; //para evitar error
        foreach($a as $i=>$n) $mul = $i == 0 ? $n : $mul*$n;
        return pow($mul,1/count($a));
    }

    public function calcularNotaCartaGantt(){
        $carta_gantt_tareas_apropiadas=(isset($this->carta_gantt_tareas_apropiadas)) ? $this->carta_gantt_tareas_apropiadas: 0;
        $carta_gantt_tiempos_apropiados=(isset($this->carta_gantt_tiempos_apropiados)) ? $this->carta_gantt_tiempos_apropiados: 0;
        return (($carta_gantt_tareas_apropiadas+$carta_gantt_tiempos_apropiados)/2);
    }

    public function calcularNotaGastos(){
        $items=$this->proyecto->itemsPresupuestosPostulacion;
        $nota_costo_apropiado=0;
        $nota_monto_adecuado=0;
        $this->total_revisiones_items_presupuesto=0;
        foreach ($items as $item) {
            $item_revisado=$item->getItemRevisado($this->id_revisor);
            if($item_revisado!=null){
                $this->total_revisiones_items_presupuesto++;
                $nota_costo_apropiado=(isset($item_revisado->item_de_costo_apropiado_nota))?$nota_costo_apropiado+
                    $item_revisado->item_de_costo_apropiado_nota:$nota_costo_apropiado;
                $nota_monto_adecuado=(isset($item_revisado->montos_adecuados_a_la_realizacion_item))?$nota_monto_adecuado+
                    $item_revisado->montos_adecuados_a_la_realizacion_item:$nota_monto_adecuado;
            }
        }
        $nota_costo_apropiado/=count($items);
        $nota_monto_adecuado/=count($items);
        return (($nota_costo_apropiado+$nota_monto_adecuado)/2);
    }
    
    public function calcularNotaHito(){
        return (isset($this->hitos_realizables)) ? $this->hitos_realizables: 0;
    }

    public function calcularNotaPlanificacion(){
        //$nota_hito_alcanzable=(isset($this->hitos_alcanzables)) ? $this->hitos_alcanzables: 0;
        $nota_hito_realizable=(isset($this->hitos_realizables)) ? $this->hitos_realizables: 0;
        $carta_gantt_tareas_apropiadas=(isset($this->carta_gantt_tareas_apropiadas)) ? $this->carta_gantt_tareas_apropiadas: 0;
        $carta_gantt_tiempos_apropiados=(isset($this->carta_gantt_tiempos_apropiados)) ? $this->carta_gantt_tiempos_apropiados: 0;

        //para el presupuesto, se obtienen todos los items de presupuesto de proyecto y se verifica si es que estan
        //revisados por el revisor de la presente revision, cada item cuenta por lo que si no se ha revisado el item
        //entonces se sumara un 0
        $items=$this->proyecto->itemsPresupuestosPostulacion;
        $nota_costo_apropiado=0;
        $nota_monto_adecuado=0;
        $this->total_revisiones_items_presupuesto=0;
        foreach ($items as $item) {
            $item_revisado=$item->getItemRevisado($this->id_revisor);
            if($item_revisado!=null){
                $this->total_revisiones_items_presupuesto++;
                $nota_costo_apropiado=(isset($item_revisado->item_de_costo_apropiado_nota))?$nota_costo_apropiado+
                    $item_revisado->item_de_costo_apropiado_nota:$nota_costo_apropiado;
                $nota_monto_adecuado=(isset($item_revisado->montos_adecuados_a_la_realizacion_item))?$nota_monto_adecuado+
                    $item_revisado->montos_adecuados_a_la_realizacion_item:$nota_monto_adecuado;
            }
        }
        $nota_costo_apropiado/=count($items);
        $nota_monto_adecuado/=count($items);

        //se pondera el total
        return 0.30*(($nota_hito_realizable))+0.40*(($carta_gantt_tareas_apropiadas+
            $carta_gantt_tiempos_apropiados)/2)+0.30*(($nota_costo_apropiado+$nota_monto_adecuado)/2);
    }

    public function calcularNotaProyecciones(){
        $nota_proyecciones=0;
        $nota_proyecciones=(isset($this->proyecciones_nota)) ? $nota_proyecciones+$this->proyecciones_nota: $nota_proyecciones;
        return $nota_proyecciones;
    }
    
    public function calcularNotaUniversitarioSocial(){
        $nota_us=0;
        $nota_us=(isset($this->impacto_universitario_social_nota)) ? $nota_us+$this->impacto_universitario_social_nota: $nota_us;
        return $nota_us;
    }
    
    public function calcularNotaAcademico(){
        $nota_academico=0;
        $nota_academico=(isset($this->impacto_academico_nota)) ? $nota_academico+$this->impacto_academico_nota: $nota_academico;
        return $nota_academico;
    }
    
    public function calcularNotaImpacto(){
        $nota_academico=0;
        $nota_academico=(isset($this->impacto_academico_nota)) ? $nota_academico+$this->impacto_academico_nota: $nota_academico;
        $nota_us=0;
        $nota_us=(isset($this->impacto_universitario_social_nota)) ? $nota_us+$this->impacto_universitario_social_nota: $nota_us;
        $nota_proyecciones=0;
        $nota_proyecciones=(isset($this->proyecciones_nota)) ? $nota_proyecciones+$this->proyecciones_nota: $nota_proyecciones;

        return 0.40*$nota_academico+0.30*$nota_us+0.30*$nota_proyecciones;
    }

    public function calcularNotaAntecedentes(){
        $nota_antecedentes=0;
        $nota_antecedentes=(isset($this->estado_del_arte_pertinencia)) ? $nota_antecedentes+$this->estado_del_arte_pertinencia: $nota_antecedentes;
        return $nota_antecedentes;
    }
    
    public function calcularNotaIDI(){
        $nota_idi=0;
        $nota_idi=(isset($this->investigacion_presencia)) ? $nota_idi+$this->investigacion_presencia: $nota_idi;
        $nota_idi=(isset($this->innovacion_presencia)) ? $nota_idi+$this->innovacion_presencia: $nota_idi;
        $nota_idi=(isset($this->desarrollo_presencia)) ? $nota_idi+$this->desarrollo_presencia: $nota_idi;
        $nota_idi/=3;
        return $nota_idi;
    }

    public function calcularNotaObjetivos(){
        $nota_objetivos=0;
        $nota_objetivos=(isset($this->objetivos_factibilidad)) ? $nota_objetivos+$this->objetivos_factibilidad: $nota_objetivos;
        $nota_objetivos=(isset($this->objetivos_alcanzabilidad)) ? $nota_objetivos+$this->objetivos_alcanzabilidad: $nota_objetivos;
        $nota_objetivos/=2;
        return $nota_objetivos;
    }

    public function calcularNotaEquipoTrabajo(){
        $nota_equipo_trabajo=0;
        $nota_equipo_trabajo=(isset($this->equipo_trabajo_presencia_especialidades)) ? $nota_equipo_trabajo+$this->equipo_trabajo_presencia_especialidades: $nota_equipo_trabajo;
        $nota_equipo_trabajo=(isset($this->equipo_trabajo_asignacion_tareas)) ? $nota_equipo_trabajo+$this->equipo_trabajo_asignacion_tareas: $nota_equipo_trabajo;
        $nota_equipo_trabajo=(isset($this->referentes_nota)) ? $nota_equipo_trabajo+$this->referentes_nota : $nota_equipo_trabajo;
        $nota_equipo_trabajo/=3;
        return $nota_equipo_trabajo;
    }

    public function calcularNotaFondo(){
        $nota_equipo_trabajo=0;
        $nota_equipo_trabajo=(isset($this->equipo_trabajo_presencia_especialidades)) ? $nota_equipo_trabajo+$this->equipo_trabajo_presencia_especialidades: $nota_equipo_trabajo;
        $nota_equipo_trabajo=(isset($this->equipo_trabajo_asignacion_tareas)) ? $nota_equipo_trabajo+$this->equipo_trabajo_asignacion_tareas: $nota_equipo_trabajo;
        $nota_equipo_trabajo=(isset($this->referentes_nota)) ? $nota_equipo_trabajo+$this->referentes_nota : $nota_equipo_trabajo;
        $nota_equipo_trabajo/=3;
        $nota_objetivos=0;
        $nota_objetivos=(isset($this->objetivos_factibilidad)) ? $nota_objetivos+$this->objetivos_factibilidad: $nota_objetivos;
        $nota_objetivos=(isset($this->objetivos_alcanzabilidad)) ? $nota_objetivos+$this->objetivos_alcanzabilidad: $nota_objetivos;
        $nota_objetivos/=2;
        $nota_idi=0;
        $nota_idi=(isset($this->investigacion_presencia)) ? $nota_idi+$this->investigacion_presencia: $nota_idi;
        $nota_idi=(isset($this->innovacion_presencia)) ? $nota_idi+$this->innovacion_presencia: $nota_idi;
        $nota_idi=(isset($this->desarrollo_presencia)) ? $nota_idi+$this->desarrollo_presencia: $nota_idi;
        $nota_idi/=3;
        $nota_antecedentes=0;
        $nota_antecedentes=(isset($this->estado_del_arte_pertinencia)) ? $nota_antecedentes+$this->estado_del_arte_pertinencia: $nota_antecedentes;

        return 0.25*$nota_equipo_trabajo+0.25*$nota_objetivos+0.40*$nota_idi+0.10*$nota_antecedentes;
    }

    public function calcularNotaOrtografia(){
        //se obtienen todos los items de ortografia:
        $nota_ortografia=0;
        $nota_ortografia=(isset($this->extracto_ortografia)) ? $nota_ortografia+$this->extracto_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->resumen_ortografia)) ? $nota_ortografia+$this->resumen_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->objetivos_ortografia)) ? $nota_ortografia+$this->objetivos_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->estado_del_arte_ortografia)) ? $nota_ortografia+$this->estado_del_arte_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->experiencia_previa_ortografia)) ? $nota_ortografia+$this->experiencia_previa_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->investigacion_ortografia)) ? $nota_ortografia+$this->investigacion_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->desarrollo_ortografia)) ? $nota_ortografia+$this->desarrollo_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->innovacion_ortografia)) ? $nota_ortografia+$this->innovacion_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->impacto_academico_ortografia)) ? $nota_ortografia+$this->impacto_academico_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->impacto_universitario_social_ortografia)) ? $nota_ortografia+$this->impacto_universitario_social_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->proyecciones_ortografia)) ? $nota_ortografia+$this->proyecciones_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->hitos_ortografia)) ? $nota_ortografia+$this->hitos_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->carta_gantt_ortografia)) ? $nota_ortografia+$this->carta_gantt_ortografia: $nota_ortografia;
        $nota_ortografia=$nota_ortografia/13;
        return $nota_ortografia;
    }

    public function calcularNotaCapacidadSintesis(){
        //se obtienen las ntoas de los items de capacidad de sintesis
        $nota_capacidad_sintesis=0;
        $nota_capacidad_sintesis=(isset($this->extracto_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->extracto_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->resumen_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->resumen_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->objetivos_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->objetivos_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->estado_del_arte_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->estado_del_arte_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->experiencia_previa_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->experiencia_previa_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->investigacion_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->investigacion_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->desarrollo_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->desarrollo_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->innovacion_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->innovacion_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->impacto_academico_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->impacto_academico_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->impacto_universitario_social_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->impacto_universitario_social_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->proyecciones_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->proyecciones_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->hitos_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->hitos_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->carta_gantt_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->carta_gantt_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=$nota_capacidad_sintesis/13;
        return $nota_capacidad_sintesis;
    }

    public function calcularNotaRedaccion(){
        //se obtienen las notas de la redaccion
        $nota_redaccion=0;
        $nota_redaccion=(isset($this->extracto_redaccion)) ? $nota_redaccion+$this->extracto_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->resumen_redaccion)) ? $nota_redaccion+$this->resumen_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->objetivos_redaccion)) ? $nota_redaccion+$this->objetivos_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->estado_del_arte_redaccion)) ? $nota_redaccion+$this->estado_del_arte_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->experiencia_previa_redaccion)) ? $nota_redaccion+$this->experiencia_previa_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->investigacion_redaccion)) ? $nota_redaccion+$this->investigacion_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->desarrollo_redaccion)) ? $nota_redaccion+$this->desarrollo_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->innovacion_redaccion)) ? $nota_redaccion+$this->innovacion_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->impacto_academico_redaccion)) ? $nota_redaccion+$this->impacto_academico_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->impacto_universitario_social_redaccion)) ? $nota_redaccion+$this->impacto_universitario_social_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->proyecciones_redaccion)) ? $nota_redaccion+$this->proyecciones_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->hitos_redaccion)) ? $nota_redaccion+$this->hitos_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->carta_gantt_redaccion)) ? $nota_redaccion+$this->carta_gantt_redaccion: $nota_redaccion;
        $nota_redaccion=$nota_redaccion/13;
        return $nota_redaccion;
    }

    public function calcularNotaPertinenciaAnexos(){
        //pertinencia de anexos
        $nota_anexos=0;
        $nota_anexos=(isset($this->anexos_pertinencia)) ? $nota_anexos+$this->anexos_pertinencia : $nota_anexos;
        return $nota_anexos;
    }

    public function calcularNotaForma(){
        //se obtienen todos los items de ortografia:
        $nota_ortografia=0;
        $nota_ortografia=(isset($this->extracto_ortografia)) ? $nota_ortografia+$this->extracto_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->resumen_ortografia)) ? $nota_ortografia+$this->resumen_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->objetivos_ortografia)) ? $nota_ortografia+$this->objetivos_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->estado_del_arte_ortografia)) ? $nota_ortografia+$this->estado_del_arte_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->experiencia_previa_ortografia)) ? $nota_ortografia+$this->experiencia_previa_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->investigacion_ortografia)) ? $nota_ortografia+$this->investigacion_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->desarrollo_ortografia)) ? $nota_ortografia+$this->desarrollo_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->innovacion_ortografia)) ? $nota_ortografia+$this->innovacion_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->impacto_academico_ortografia)) ? $nota_ortografia+$this->impacto_academico_ortografia : $nota_ortografia;
        $nota_ortografia=(isset($this->impacto_universitario_social_ortografia)) ? $nota_ortografia+$this->impacto_universitario_social_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->proyecciones_ortografia)) ? $nota_ortografia+$this->proyecciones_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->hitos_ortografia)) ? $nota_ortografia+$this->hitos_ortografia: $nota_ortografia;
        $nota_ortografia=(isset($this->carta_gantt_ortografia)) ? $nota_ortografia+$this->carta_gantt_ortografia: $nota_ortografia;
        $nota_ortografia=$nota_ortografia/13;

        //se obtienen las ntoas de los items de capacidad de sintesis
        $nota_capacidad_sintesis=0;
        $nota_capacidad_sintesis=(isset($this->extracto_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->extracto_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->resumen_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->resumen_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->objetivos_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->objetivos_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->estado_del_arte_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->estado_del_arte_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->experiencia_previa_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->experiencia_previa_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->investigacion_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->investigacion_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->desarrollo_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->desarrollo_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->innovacion_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->innovacion_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->impacto_academico_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->impacto_academico_capacidad_sintesis : $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->impacto_universitario_social_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->impacto_universitario_social_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->proyecciones_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->proyecciones_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->hitos_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->hitos_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=(isset($this->carta_gantt_capacidad_sintesis)) ? $nota_capacidad_sintesis+$this->carta_gantt_capacidad_sintesis: $nota_capacidad_sintesis;
        $nota_capacidad_sintesis=$nota_capacidad_sintesis/13;
        //se obtienen las notas de la redaccion
        $nota_redaccion=0;
        $nota_redaccion=(isset($this->extracto_redaccion)) ? $nota_redaccion+$this->extracto_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->resumen_redaccion)) ? $nota_redaccion+$this->resumen_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->objetivos_redaccion)) ? $nota_redaccion+$this->objetivos_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->estado_del_arte_redaccion)) ? $nota_redaccion+$this->estado_del_arte_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->experiencia_previa_redaccion)) ? $nota_redaccion+$this->experiencia_previa_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->investigacion_redaccion)) ? $nota_redaccion+$this->investigacion_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->desarrollo_redaccion)) ? $nota_redaccion+$this->desarrollo_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->innovacion_redaccion)) ? $nota_redaccion+$this->innovacion_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->impacto_academico_redaccion)) ? $nota_redaccion+$this->impacto_academico_redaccion : $nota_redaccion;
        $nota_redaccion=(isset($this->impacto_universitario_social_redaccion)) ? $nota_redaccion+$this->impacto_universitario_social_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->proyecciones_redaccion)) ? $nota_redaccion+$this->proyecciones_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->hitos_redaccion)) ? $nota_redaccion+$this->hitos_redaccion: $nota_redaccion;
        $nota_redaccion=(isset($this->carta_gantt_redaccion)) ? $nota_redaccion+$this->carta_gantt_redaccion: $nota_redaccion;
        $nota_redaccion=$nota_redaccion/13;
        //pertinencia de anexos
        $nota_anexos=0;
        $nota_anexos=(isset($this->anexos_pertinencia)) ? $nota_anexos+$this->anexos_pertinencia : $nota_anexos;

        return 0.15*$nota_ortografia+0.35*$nota_capacidad_sintesis+0.30*$nota_redaccion+0.20*$nota_anexos;
    }

    public function validarRevision(){
        $errores=array();
        if(!isset($this->extracto_ortografia)) array_push($errores,"No has evaluado la ortografia del extracto.");
        if(!isset($this->resumen_ortografia)) array_push($errores,"No has evaluado la ortografia del resumen.");
        if(!isset($this->objetivos_ortografia)) array_push($errores,"No has evaluado la ortografia de los objetivos.");
        if(!isset($this->estado_del_arte_ortografia)) array_push($errores,"No has evaluado la ortografia del estado del arte.");
        if(!isset($this->experiencia_previa_ortografia)) array_push($errores,"No has evaluado la ortografia de la experiencia previa.");
        if(!isset($this->investigacion_ortografia)) array_push($errores,"No has evaluado la ortografia de la investigacion.");
        if(!isset($this->desarrollo_ortografia)) array_push($errores,"No has evaluado la ortografia del desarrollo.");
        if(!isset($this->innovacion_ortografia)) array_push($errores,"No has evaluado la ortografia de la innovacion.");
        if(!isset($this->impacto_academico_ortografia)) array_push($errores,"No has evaluado la ortografia del impacto academico.");
        if(!isset($this->impacto_universitario_social_ortografia)) array_push($errores,"No has evaluado la ortografia del impacto universitario/social.");
        if(!isset($this->proyecciones_ortografia)) array_push($errores,"No has evaluado la ortografia de las proyecciones.");
        if(!isset($this->hitos_ortografia)) array_push($errores,"No has evaluado la ortografia de los hitos.");
        if(!isset($this->carta_gantt_ortografia)) array_push($errores,"No has evaluado la ortografia de la carta gantt.");
        if(!isset($this->extracto_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del extracto.");
        if(!isset($this->resumen_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del resumen.");
        if(!isset($this->objetivos_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de los objetivos.");
        if(!isset($this->estado_del_arte_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del estado del arte.");
        if(!isset($this->experiencia_previa_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de la experiencia previa.");
        if(!isset($this->investigacion_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de la investigacion.");
        if(!isset($this->desarrollo_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del desarrollo.");
        if(!isset($this->innovacion_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de la innovacion.");
        if(!isset($this->impacto_academico_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del impacto academico.");
        if(!isset($this->impacto_universitario_social_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis del impacto universitario/social.");
        if(!isset($this->proyecciones_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de las proyecciones.");
        if(!isset($this->hitos_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de los hitos.");
        if(!isset($this->carta_gantt_capacidad_sintesis)) array_push($errores,"No has evaluado la capacidad de sintesis de la carta gantt.");
        if(!isset($this->extracto_redaccion)) array_push($errores,"No has evaluado la redaccion del extracto.");
        if(!isset($this->resumen_redaccion)) array_push($errores,"No has evaluado la redaccion del resumen.");
        if(!isset($this->objetivos_redaccion)) array_push($errores,"No has evaluado la redaccion de los objetivos.");
        if(!isset($this->estado_del_arte_redaccion)) array_push($errores,"No has evaluado la redaccion del estado del arte.");
        if(!isset($this->experiencia_previa_redaccion)) array_push($errores,"No has evaluado la redaccion de la experiencia previa.");
        if(!isset($this->investigacion_redaccion)) array_push($errores,"No has evaluado la redaccion de la investigacion.");
        if(!isset($this->desarrollo_redaccion)) array_push($errores,"No has evaluado la redaccion del desarrollo.");
        if(!isset($this->innovacion_redaccion)) array_push($errores,"No has evaluado la redaccion de la innovacion.");
        if(!isset($this->impacto_academico_redaccion)) array_push($errores,"No has evaluado la redaccion del impacto academico.");
        if(!isset($this->impacto_universitario_social_redaccion)) array_push($errores,"No has evaluado la redaccion del impacto universitario/social.");
        if(!isset($this->proyecciones_redaccion)) array_push($errores,"No has evaluado la redaccion de las proyecciones.");
        if(!isset($this->hitos_redaccion)) array_push($errores,"No has evaluado la redaccion de los hitos.");
        if(!isset($this->carta_gantt_redaccion)) array_push($errores,"No has evaluado la redaccion de la carta gantt.");
        if(!isset($this->anexos_pertinencia)) array_push($errores,"No has evaluado la pertinencia de los anexos.");
        
        if(!isset($this->equipo_trabajo_presencia_especialidades)) array_push($errores,"No has evaluado la presencia de especialidades en el equipo de trabajo.");
        if(!isset($this->equipo_trabajo_asignacion_tareas)) array_push($errores,"No has evaluado la asignacion de tareas en el equipo de trabajo.");
        if(!isset($this->referentes_nota)) array_push($errores,"No has evaluado los referentes.");
        if(!isset($this->objetivos_factibilidad)) array_push($errores,"No has evaluado la factibilidad de los objetivos.");
        if(!isset($this->objetivos_alcanzabilidad)) array_push($errores,"No has evaluado la alcanzabilidad de los objetivos.");
        if(!isset($this->investigacion_presencia)) array_push($errores,"No has evaluado la presencia de investigacion.");
        if(!isset($this->innovacion_presencia)) array_push($errores,"No has evaluado la presencia de innovacion.");
        if(!isset($this->desarrollo_presencia)) array_push($errores,"No has evaluado la presencia de desarrollo.");
        if(!isset($this->estado_del_arte_pertinencia)) array_push($errores,"No has evaluado la pertinencia del estado del arte.");
        //if(!isset($this->estado_del_arte_acceso)) array_push($errores,"No has evaluado el acceso al estado del arte.");

        if(!isset($this->impacto_academico_nota)) array_push($errores,"No has evaluado el impacto academico.");
        if(!isset($this->impacto_universitario_social_nota)) array_push($errores,"No has el impacto universitario/social.");
        if(!isset($this->proyecciones_nota)) array_push($errores,"No has evaluado las proyecciones.");

        //if(!isset($this->hitos_alcanzables)) array_push($errores,"No has evaluado si los hitos son alcanzables .");
        if(!isset($this->hitos_realizables)) array_push($errores,"No has evaluado si los hitos son realizables.");
        if(!isset($this->carta_gantt_tareas_apropiadas)) array_push($errores,"No has evaluado si la tareas son apropiadas en la carta gantt.");
        if(!isset($this->carta_gantt_tiempos_apropiados)) array_push($errores,"No has evaluado si los tiempos son apropiados en la carta gantt.");

        $items=$this->proyecto->itemsPresupuestosPostulacion;
        if(count($items)!=$this->total_revisiones_items_presupuesto)
            array_push($errores,"No has evaluado todos los items del presupuesto del proyecto.");
        return $errores;
    }

}