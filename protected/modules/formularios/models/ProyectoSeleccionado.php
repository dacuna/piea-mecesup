<?php

/**
 * Modelo para la tabla "{{proyecto_seleccionado}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{proyecto_seleccionado}}':
 * @property integer $id_proyecto
 * @property integer $id_proceso_formulario
 * @property integer $monto_idi
 * @property string $fecha_seleccion
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 * @property ProcesoFormulario $idProcesoFormulario
 */
class ProyectoSeleccionado extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProyectoSeleccionado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{proyecto_seleccionado}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_proyecto,monto_idi', 'required'),
			array('id_proyecto,monto_idi', 'numerical', 'integerOnly'=>true),
			array('id_proyecto, id_proceso_formulario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
			'procesoFormulario' => array(self::BELONGS_TO, 'ProcesoFormulario', 'id_proceso_formulario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proyecto' => 'Proyecto',
			'id_proceso_formulario' => 'Proceso Formulario',
			'monto_idi' => 'Monto I+D+i',
			'fecha_seleccion' => 'Fecha Selecci&oacute;n',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_proyecto',$this->id_proyecto);
		$criteria->compare('id_proceso_formulario',$this->id_proceso_formulario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}