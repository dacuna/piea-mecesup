<?php

/**
 * Modelo para la tabla "{{objetivos_especificos_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{objetivos_especificos_proyecto}}':
 * @property integer $id_proyecto
 * @property string $descripcion
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class ObjetivosEspecificosProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObjetivosEspecificosProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{objetivos_especificos_proyecto}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proyecto' => 'Id Proyecto',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_proyecto',$this->id_proyecto);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function create($id_proyecto,$descripcion){
        $objetivo=new ObjetivosEspecificosProyecto;
        $objetivo->id_proyecto=$id_proyecto;
        $objetivo->descripcion=$descripcion;
        return $objetivo->save();
    }
}