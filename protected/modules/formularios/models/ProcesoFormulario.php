<?php

/**
 * Modelo para la tabla "{{proceso_formulario}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{proceso_formulario}}':
 * @property integer $id
 * @property integer $id_formulario
 * @property string $fecha_inicio
 * @property string $fecha_cierre
 *
 * Las siguientes son las relaciones disponibles:
 * @property FormularioPiea $idFormulario
 */
class ProcesoFormulario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProcesoFormulario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{proceso_formulario}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('fecha_inicio, fecha_cierre', 'required'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'formulario' => array(self::BELONGS_TO, 'FormularioPiea', 'id_formulario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_formulario' => 'Id Formulario',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_cierre' => 'Fecha Cierre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_formulario',$this->id_formulario);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_cierre',$this->fecha_cierre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if(isset($this->fecha_inicio) && !empty($this->fecha_inicio))
                $this->fecha_inicio=date('Y-m-d', strtotime($this->fecha_inicio));
            if(isset($this->fecha_cierre) && !empty($this->fecha_cierre))
                $this->fecha_cierre=date('Y-m-d', strtotime($this->fecha_cierre));
            return true;
        }
        else
            return false;
    }
}