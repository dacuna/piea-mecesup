<?php

/**
 * Modelo para la tabla "{{formulario_piea}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{formulario_piea}}':
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $estado
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 */
class FormularioPiea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FormularioPiea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{formulario_piea}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('nombre, descripcion', 'required'),
            array('estado','numerical','integerOnly'=>true,'min'=>1,'max'=>5,'on'=>'update'),
			array('nombre', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
            'procesos' => array(self::HAS_MANY,'ProcesoFormulario','id_formulario')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'estado' => 'Estado',
			'fecha_creacion' => 'Fecha Creacion',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('fecha_creacion',$this->fecha_creacion,true);
		$criteria->compare('fecha_actualizacion',$this->fecha_actualizacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave(){
        if(parent::beforeSave()){
            if($this->isNewRecord){
                $this->estado=0;
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Behaviors del modelo. Se cuenta con los siguientes behaviors:
     *   - CTimeStampBehavior: agrega y actualiza automaticamente los campos de
     *                         fecha_creacion y fecha_actualizacion respectivamente.
     * @return array Listado de behaviors del modelo.
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_creacion',
                'updateAttribute' => 'fecha_actualizacion',
            ),
        );
    }

    public function getEstadoFormulario(){
        if($this->estado==0)
            return "No hay proceso en curso";
        if($this->estado==1)
            return "Formulario abierto a postulaciones";
        if($this->estado==2)
            return "Postulaciones cerradas";
        if($this->estado==3)
            return "Postulaciones en revisi&oacute;n";
        if($this->estado==4)
            return "Periodo de revisi&oacute;n cerrado";
        if($this->estado==5)
            return "Resultados de postulaciones publicados";
        else
            return "Sin informaci&oacute;n";
    }

    /**
     * Retorna el proceso actual que se esta llevando a cabo para el presente
     * formulario.
     *
     * @return  \ProcesoFormulario
     *
     * @since   01-02-2013
     */
    public function getProcesoActual(){
        $proceso=ProcesoFormulario::model()->find('id_formulario=:ifo and activo=1',array(':ifo'=>$this->id));
        return $proceso;
    }

    /**
     * Retorna el proceso actual para el formulario de fondos concursables I+D+i.
     *
     * @return  \ProcesoFormulario
     *
     * @since   02-02-2013
     */
    public static function procesoFormularioFondosIDI(){
        $formulario=FormularioPiea::model()->findByPk(1); //se obtiene el formulario I+D+i
        return $formulario->procesoActual;
    }

    public static function formularioIDI(){
        $idi=FormularioPiea::model()->find('nombre=:no',array(':no'=>'fondo_idi' ));
        return $idi;
    }

    public static function formularioFAE(){
        $fae=FormularioPiea::model()->find('nombre=:no',array(':no'=>'fae' ));
        return $fae;
    }

    public function procesoFAE(){
        $fae=FormularioPiea::model()->find('nombre=:no',array(':no'=>'fae' ));
        $proceso=$fae->procesoActual;
        return $proceso;
    }

    public function getEstadoFAE(){
        if($this->estado==0)
            return "No hay proceso FAE en curso";
        if($this->estado==1)
            return "Primera fase FAE: selecci&oacute;n de alumnos a evaluar por los evaluadores";
        if($this->estado==2)
            return "Segunda fase FAE: evaluaci&oacute;n a alumnos por los evaluadores";
        if($this->estado==3)
            return "Proceso FAE cerrado";
        else
            return "Sin informaci&oacute;n";
    }

}