<?php

/**
 * Modelo para la tabla "{{hito_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{hito_proyecto}}':
 * @property integer $id
 * @property integer $id_proyecto
 * @property string $nombre
 * @property string $descripcion
 * @property string $fecha
 * @property string $es_hito
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class HitoProyecto extends CActiveRecord
{
    private $meses=array();

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HitoProyecto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{hito_proyecto}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('nombre, fecha', 'required'),
            array('descripcion','required','on'=>'agregarHito'),
            array('nombre', 'length', 'max' => 50),
            array('descripcion', 'length', 'max' => 500),
            array('fecha', 'date', 'format' => 'd-M-yyyy','on'=>'agregarHito'),
            array('fecha', 'date', 'format' => 'yyyy-M-d','on'=>'agregarActividad'),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
            'mesesAR' => array(self::HAS_MANY, 'MesHito', 'id_hito'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_proyecto' => 'Id Proyecto',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fecha' => 'Fecha',
        );
    }

    public function beforeSave()
    {
        if(parent::beforeSave()) {
            if(isset($this->fecha) && !empty($this->fecha)){
                $this->fecha= date('Y-m-d', strtotime($this->fecha));
            }
            return true;
        } else
            return false;
    }

    public function afterSave(){
        parent::afterSave();
        if($this->es_hito==1){
            $mes=new MesHito;
            $mes->id_hito=$this->primaryKey;
            $numero=date("n",strtotime($this->fecha));
            $anio=date("Y",strtotime($this->fecha));
            if($numero>=5 && $numero<=12 && $anio==2013)
                $mes->mes=date("n",strtotime($this->fecha))-5;
            else
                $mes->mes=date("n",strtotime($this->fecha))+7;
            $mes->save(false);
        }
    }

    public static function load($id,$proyecto)
    {
        $hito = HitoProyecto::model()->findByPk($id);
        if ($hito== null || $hito->id_proyecto!=$proyecto)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $hito;
    }

    public function getMeses(){
        if(empty($this->meses)){
            $meses=array();
            foreach ($this->mesesAR as $mes) {
                $meses[]=$mes->mes;
            }

            $finMes=array();
            for($i=0;$i<14;$i++){
                if(in_array($i,$meses))
                    $finMes[$i]=true;
                else
                    $finMes[$i]=false;
            }
                $this->meses=$finMes;
        }
        return $this->meses;
    }

    public function mesesAsJSON(){
        return CJSON::encode($this->getMeses());
    }
}
