<?php

/**
 * Modelo para la tabla "{{observacion_item_presupuesto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{observacion_item_presupuesto}}':
 * @property integer $id_item_presupuesto_postulacion
 * @property integer $id_observador
 * @property integer $monto_idi_editado
 */
class ObservacionItemPresupuesto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObservacionItemPresupuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{observacion_item_presupuesto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('monto_idi_editado', 'required'),
			array('monto_idi_editado', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
            'item' => array(self::BELONGS_TO, 'ItemPresupuestoPostulacion', 'id_item_presupuesto_postulacion'),
            'revisor' => array(self::BELONGS_TO, 'Persona', 'id_observador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_presupuesto_postulacion' => 'Id Item Presupuesto Postulacion',
			'id_observador' => 'Id Observador',
			'monto_idi_editado' => 'Monto Idi Editado',
		);
	}

    public static function load($id_observador,$id_item,$show_error=true)
    {
        $item = ObservacionItemPresupuesto::model()->find('id_observador=:ir and id_item_presupuesto_postulacion=:ipp',array(
            ':ir'=>$id_observador,':ipp'=>$id_item
        ));
        if ($item== null && $show_error)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $item;
    }

    public function convertToArray(){
        $return=array();
        $return['id']=$this->item->id;
        $return['id_categoria']=$this->item->id_categoria;
        $return['nombre']=$this->item->nombre;
        $return['precio']=$this->item->precio;
        $return['unidades']=$this->item->unidades;
        $return['idi']=$this->item->idi;
        $return['propio']=$this->item->propio;
        $return['otros']=$this->item->otros;
        $return['idi_sugerido']=$this->monto_idi_editado;
        return $return;
    }
}