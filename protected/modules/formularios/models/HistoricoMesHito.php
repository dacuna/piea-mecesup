<?php

/**
 * Modelo para la tabla "{{mes_hito}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{mes_hito}}':
 * @property integer $id
 * @property integer $id_hito
 * @property integer $mes
 *
 * Las siguientes son las relaciones disponibles:
 * @property HitoProyecto $idHito
 */
class HistoricoMesHito extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MesHito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{historico_mes_hito}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_hito, mes', 'required'),
			array('id_hito, mes', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'hito' => array(self::BELONGS_TO, 'HistoricoHitoProyecto', 'id_hito'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_hito' => 'Id Hito',
			'mes' => 'Mes',
		);
	}

}