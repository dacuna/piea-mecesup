<?php

/**
 * Modelo para la tabla "{{estadistica_idi}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{estadistica_idi}}':
 * @property integer $id_formulario
 * @property integer $id_proceso_formulario
 * @property string $nota_promedio
 * @property string $nota_sd
 * @property string $nota_minima
 * @property string $nota_maxima
 * @property string $forma_promedio
 * @property string $forma_sd
 * @property string $fondo_promedio
 * @property string $fondo_sd
 * @property string $planificacion_promedio
 * @property string $planificacion_sd
 * @property string $impacto_promedio
 * @property string $impacto_sd
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 */
class EstadisticaIdi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EstadisticaIdi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{estadistica_idi}}';
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_formulario' => 'Id Formulario',
			'id_proceso_formulario' => 'Id Proceso Formulario',
			'nota_promedio' => 'Nota Promedio',
			'nota_sd' => 'Nota Sd',
			'nota_minima' => 'Nota Minima',
			'nota_maxima' => 'Nota Maxima',
			'forma_promedio' => 'Forma Promedio',
			'forma_sd' => 'Forma Sd',
			'fondo_promedio' => 'Fondo Promedio',
			'fondo_sd' => 'Fondo Sd',
			'planificacion_promedio' => 'Planificacion Promedio',
			'planificacion_sd' => 'Planificacion Sd',
			'impacto_promedio' => 'Impacto Promedio',
			'impacto_sd' => 'Impacto Sd',
			'fecha_creacion' => 'Fecha Creacion',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}

    /**
     * Genera todas las estadistocas del proceso actual de formulario I+D+i con la posibilidad tambien
     * de actualizar las estadisticas ya generadas. Estas estadisticas se almacenan en la base de datos
     * para que luego sea mas sencillo obtener la informacion relacionada y evitar congestion en el
     * servidor.
     *
     * @param int $act 1 si es que se desea actualizar las estadisticas, por defecto no se actualizan
     *
     * @return  void
     *
     * @since   14-05-2013
     */
    public function generarEstadisticas($act=0){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        $estadisticas=EstadisticaIdi::model()->find('id_formulario=:if and id_proceso_formulario=:ip',array(
           ':if'=> $proceso->id_formulario,':ip'=>$proceso->id
        ));
        if($estadisticas==null || $act==1){
            //se generan o actualizan las estadisticas
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $proyectos=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and  id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');
            $nota_final=array();
            $forma=array();
            $fondo=array();
            $planificacion=array();
            $impacto=array();
            foreach ($proyectos as $proyecto) {
                $revisiones=RevisionProyecto::model()->findAll('id_proyecto=:ip',array(
                    ':ip'=>$proyecto->id));
                foreach ($revisiones as $revision) {
                    $nota_final[]=$revision->calcularNotaRevision();
                    $forma[]=$revision->calcularNotaForma();
                    $fondo[]=$revision->calcularNotaFondo();
                    $planificacion[]=$revision->calcularNotaPlanificacion();
                    $impacto[]=$revision->calcularNotaImpacto();
                }
            }
            $estadistica=new EstadisticaIdi;
            $estadistica->id_formulario=$proceso->id_formulario;
            $estadistica->id_proceso_formulario=$proceso->id;
            $estadistica->nota_promedio=array_sum($nota_final)/count($nota_final);
            $estadistica->nota_minima=min($nota_final);
            $estadistica->nota_maxima=max($nota_final);
            $estadistica->nota_sd=$this->sd($nota_final);
            $estadistica->fondo_promedio=array_sum($fondo)/count($fondo);
            $estadistica->forma_promedio=array_sum($forma)/count($forma);
            $estadistica->planificacion_promedio=array_sum($planificacion)/count($planificacion);
            $estadistica->impacto_promedio=array_sum($impacto)/count($impacto);
            $estadistica->fondo_sd=$this->sd($fondo);
            $estadistica->forma_sd=$this->sd($forma);
            $estadistica->planificacion_sd=$this->sd($planificacion);
            $estadistica->impacto_sd=$this->sd($impacto);
            $estadistica->save();
            return $estadistica;
        }
        return $estadisticas;
    }

    // Function to calculate standard deviation (uses sd_square)
    function sd($array) {
        // square root of sum of squares devided by N-1
        return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
    }

}