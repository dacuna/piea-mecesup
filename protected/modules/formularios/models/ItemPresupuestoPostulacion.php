<?php

/**
 * Modelo para la tabla "{{item_presupuesto_postulacion}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{item_presupuesto_postulacion}}':
 * @property integer $id
 * @property integer $id_proyecto
 * @property integer $id_categoria
 * @property string $nombre
 * @property integer $precio
 * @property integer $unidades
 * @property integer $idi
 * @property integer $propio
 * @property integer $otros
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class ItemPresupuestoPostulacion extends CActiveRecord
{
	public $revision_item=null;

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPresupuestoPostulacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{item_presupuesto_postulacion}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_categoria, nombre, precio, unidades,idi,propio,otros', 'required'),
            array('id_categoria','in','range'=>array(1,2,3,4)),
            //array('precio','totalValidator'),
			array('id_categoria, precio, unidades, idi, propio, otros', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_proyecto' => 'Id Proyecto',
			'id_categoria' => 'Id Categoria',
			'nombre' => 'Nombre',
			'precio' => 'Precio',
			'unidades' => 'Unidades',
			'idi' => 'Idi',
			'propio' => 'Propio',
			'otros' => 'Otros',
		);
	}

    public function convertToArray(){
        $return=array();
        $return['id']=$this->id;
        $return['id_categoria']=$this->id_categoria;
        $return['nombre']=$this->nombre;
        $return['precio']=$this->precio;
        $return['unidades']=$this->unidades;
        $return['idi']=$this->idi;
        $return['propio']=$this->propio;
        $return['otros']=$this->otros;
        $return['color']=$this->color;
        return $return;
    }

    public function totalValidator($attribute,$params)
    {
        $total = $this->precio*$this->unidades;
        $suma = $this->idi + $this->propio + $this->otros;
        if($total != $suma)
            $this->addError('','La suma de I+D+i, propios y otros debe ser igual al precio total (precio*unidades) del item');
    }

    public static function load($id,$proyecto)
    {
        $item = ItemPresupuestoPostulacion::model()->findByPk($id);
        if ($item== null || $item->id_proyecto!=$proyecto)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $item;
    }

    public function getColor(){
        if(($this->precio*$this->unidades)!=($this->idi+$this->propio+$this->otros))
            return "true";
        return "false";
    }

    public function getItemRevisado($id_revisor){
        if($this->revision_item==null)
            $this->revision_item=RevisionItemPresupuesto::load($id_revisor,$this->id,false);
        return $this->revision_item;
    }

    public function getItemObservado($id_observador){
        if($this->revision_item==null)
            $this->revision_item=ObservacionItemPresupuesto::load($id_observador,$this->id,false);
        return $this->revision_item;
    }

    public function getEstaRevisado($id_revisor){
        $this->getItemRevisado($id_revisor);
        if($this->revision_item!=null) return 1;
        return 0;
    }

}
