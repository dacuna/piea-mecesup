<?php

/**
 * Modelo para la tabla "{{referentes_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{referentes_proyecto}}':
 * @property integer $id
 * @property integer $id_proyecto
 * @property string $nombre
 * @property string $email
 * @property string $telefono
 * @property string $relacion_proyecto
 * @property string $institucion
 * @property string $cargo
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class ReferentesProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReferentesProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{referentes_proyecto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('nombre, email, telefono, relacion_proyecto, institucion, cargo', 'required'),
			array('nombre, email, institucion, cargo', 'length', 'max'=>45),
            array('email','email'),
			array('telefono', 'length', 'max'=>12),
			array('relacion_proyecto', 'length', 'max'=>100),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_proyecto' => 'Id Proyecto',
			'nombre' => 'Nombre',
			'email' => 'Email',
			'telefono' => 'Telefono',
			'relacion_proyecto' => 'Relaci&oacute;n con el Proyecto',
			'institucion' => 'Institucion',
			'cargo' => 'Cargo',
		);
	}

    public function convertToArray(){
        $return=array();
        $return['id']=$this->id;
        $return['id_proyecto']=$this->id_proyecto;
        $return['nombre']=$this->nombre;
        $return['email']=$this->email;
        $return['telefono']=$this->telefono;
        $return['relacion_proyecto']=$this->relacion_proyecto;
        $return['institucion']=$this->institucion;
        $return['cargo']=$this->cargo;
        return $return;
    }

    public static function load($id,$proyecto)
    {
        $ref = ReferentesProyecto::model()->findByPk($id);
        if ($ref== null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        if($ref->id_proyecto!=$proyecto)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $ref;
    }

    public function imprimirEmail(){
        $var=explode("@",$this->email);
        return $var[0]."@<br>".$var[1];
    }
}