<?php

/**
 * Modelo para la tabla "{{perfil_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{perfil_proyecto}}':
 * @property integer $id_proyecto
 * @property integer $id_proceso_formulario
 * @property integer $id_postulante
 * @property string $extracto
 * @property string $campus_sede
 * @property string $linea_postulacion
 * @property string $resumen
 * @property string $objetivos_generales
 * @property string $estado_del_arte
 * @property string $experiencia_previa
 * @property string $investigacion
 * @property string $desarrollo
 * @property string $innovacion
 * @property integer $lugar_investigacion
 * @property integer $lugar_desarrollo
 * @property integer $lugar_innovacion
 * @property string $impacto_academico
 * @property string $impacto_universitario_social
 * @property string $proyecciones
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 * @property User $idPostulante
 * @property ProcesoFormulario $idProcesoFormulario
 */
class PerfilProyecto extends CActiveRecord
{
	public $objetivos_especificos=array();

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerfilProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{perfil_proyecto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('lugar_investigacion, lugar_desarrollo, lugar_innovacion', 'numerical', 'integerOnly'=>true,'max'=>3,
                'min'=>1),
			array('extracto', 'length', 'max'=>310),
            array('linea_postulacion','in','range'=>array(0,1)),
			array('resumen, objetivos_generales, estado_del_arte, experiencia_previa, investigacion, desarrollo, innovacion, impacto_academico, impacto_universitario_social, proyecciones', 'length', 'max'=>510),
            array('objetivos_especificos','safe')
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
			'postulante' => array(self::BELONGS_TO, 'Persona', 'id_postulante'),
			'proceso' => array(self::BELONGS_TO, 'ProcesoFormulario', 'id_proceso_formulario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proyecto' => 'Id Proyecto',
			'id_proceso_formulario' => 'Id Proceso Formulario',
			'id_postulante' => 'Id Postulante',
			'extracto' => 'Extracto',
			'campus_sede' => 'Campus Sede',
			'linea_postulacion' => 'L&iacute;nea de Postulaci&oacute;n',
			'resumen' => 'Resumen',
			'objetivos_generales' => 'Objetivo General',
            'objetivos_especificos' => 'Objetivos Espec&iacute;ficos',
			'estado_del_arte' => 'Estado Del Arte',
			'experiencia_previa' => 'Experiencia Previa',
			'investigacion' => 'Investigacion',
			'desarrollo' => 'Desarrollo',
			'innovacion' => 'Innovacion',
			'lugar_investigacion' => 'Lugar Investigacion',
			'lugar_desarrollo' => 'Lugar Desarrollo',
			'lugar_innovacion' => 'Lugar Innovacion',
			'impacto_academico' => 'Acad&eacute;mico',
			'impacto_universitario_social' => 'Universitario/Social',
			'proyecciones' => 'Proyecciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_proyecto',$this->id_proyecto);
		$criteria->compare('id_proceso_formulario',$this->id_proceso_formulario);
		$criteria->compare('id_postulante',$this->id_postulante);
		$criteria->compare('extracto',$this->extracto,true);
		$criteria->compare('campus_sede',$this->campus_sede,true);
		$criteria->compare('linea_postulacion',$this->linea_postulacion,true);
		$criteria->compare('resumen',$this->resumen,true);
		$criteria->compare('objetivos_generales',$this->objetivos_generales,true);
		$criteria->compare('estado_del_arte',$this->estado_del_arte,true);
		$criteria->compare('experiencia_previa',$this->experiencia_previa,true);
		$criteria->compare('investigacion',$this->investigacion,true);
		$criteria->compare('desarrollo',$this->desarrollo,true);
		$criteria->compare('innovacion',$this->innovacion,true);
		$criteria->compare('lugar_investigacion',$this->lugar_investigacion);
		$criteria->compare('lugar_desarrollo',$this->lugar_desarrollo);
		$criteria->compare('lugar_innovacion',$this->lugar_innovacion);
		$criteria->compare('impacto_academico',$this->impacto_academico,true);
		$criteria->compare('impacto_universitario_social',$this->impacto_universitario_social,true);
		$criteria->compare('proyecciones',$this->proyecciones,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave(){
        if(parent::beforeSave()){
            if($this->isNewRecord){
                $this->id_proceso_formulario=FormularioPiea::procesoFormularioFondosIDI()->id;
                $this->id_postulante=user()->id;
                //ademas asigno al usuario postulante el permiso para revisar y seguir postulando
                $administrador = am()->getAuthItem('administrador');
                am()->createRole("postulante_proyecto_{$this->id_proyecto}","Usuario postulante a fondos
                    IDI con proyecto ID {$this->id_proyecto}");
                //el administrador es el superior de esta postulacion
                $administrador->addChild("postulante_proyecto_{$this->id_proyecto}");
                am()->assign("postulante_proyecto_{$this->id_proyecto}",user()->id);
            }
            return true;
        }else{
            return false;
        }
    }

    public function getLineaPostulacion($encode=false){
        if($this->linea_postulacion==0){
            if($encode)
                return "Consolidacion";
            else
                return "Consolidaci&oacute;n";
        }
        if($encode)
            return "Iniciacion";
        else
            return "Iniciaci&oacute;n";
    }

}