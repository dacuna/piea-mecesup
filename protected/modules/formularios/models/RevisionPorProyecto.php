<?php
/**
 * Brief description of class
 *
 * Long description of class
 *
 * @example   ../index.php
 * @example <br />
 * 	$oUser = new MyLibrary\User(new Mappers\UserMapper());<br />
 *  $oUser->setUsername('swader');<br />
 *  $aAllEmails = $oUser->getEmails();<br />
 *  $oUser->addEmail('test@test.com');<br />
 * @version   0.01
 * @since     4/9/13
 * @author    dacuna <dacuna@dacuna.me>
 */
class RevisionPorProyecto extends CComponent {

    public $data=array();

    public function __construct(){
        //se obtienen todos los proyectos con sus revisiones
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        $proyectos=Proyecto::model()->with(array(
            'perfilPostulacion'=>array('condition'=>'estado_postulacion=1 and id_proceso_formulario='.$proceso->id)
        ))->findAll('es_postulacion=1');

        $array_data=array();

        foreach ($proyectos as $proyecto) {
            $data=array('nombreProyecto'=>$proyecto->nombre,'nombreRevisor'=>null,'mailRevisor'=>null);
            $array_data[]=$data;
            $proyecto_rev=Proyecto::loadPostulacion($proyecto->id);
            $revisiones=RevisorProyecto::model()->findAll('id_proyecto=:ip and id_proceso_formulario=:ipf',
                array(':ip'=>$proyecto_rev->id,':ipf'=>$proceso->id));
            foreach ($revisiones as $revision) {
                $data=array('nombreProyecto'=>null,'nombreRevisor'=>$revision->revisor->nombreCompleto,'mailRevisor'=>
                    $revision->revisor->email);
                $array_data[]=$data;
            }
        }

        $this->data=$array_data;
    }

}
