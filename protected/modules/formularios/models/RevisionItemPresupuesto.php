<?php

/**
 * Modelo para la tabla "{{revision_item_presupuesto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{revision_item_presupuesto}}':
 * @property integer $id_item_presupuesto_postulacion
 * @property integer $id_revisor
 * @property integer $fue_editado
 * @property integer $item_de_costo_apropiado_nota
 * @property integer $montos_adecuados_a_la_realizacion_item
 * @property integer $monto_idi_editado
 * @property string $motivo_edicion
 */
class RevisionItemPresupuesto extends CActiveRecord
{
	public $idi;

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RevisionItemPresupuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{revision_item_presupuesto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('item_de_costo_apropiado_nota, montos_adecuados_a_la_realizacion_item, monto_idi_editado', 'numerical', 'integerOnly'=>true),
            array('item_de_costo_apropiado_nota, montos_adecuados_a_la_realizacion_item','numerical','integerOnly'=>true,'max'=>100,'min'=>0),
            array('monto_idi_editado', 'validateIdiSugerido'),
            array('motivo_edicion', 'length', 'max'=>255),
            array('idi', 'numerical', 'integerOnly'=>true,'allowEmpty'=>false),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
        return array(
            'item' => array(self::BELONGS_TO, 'ItemPresupuestoPostulacion', 'id_item_presupuesto_postulacion'),
            'revisor' => array(self::BELONGS_TO, 'Persona', 'id_revisor'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_presupuesto_postulacion' => 'Id Item Presupuesto Postulacion',
			'id_revisor' => 'Id Revisor',
			'fue_editado' => 'Fue Editado',
			'item_de_costo_apropiado_nota' => 'Item De Costo Apropiado Nota',
			'montos_adecuados_a_la_realizacion_item' => 'Montos Adecuados A La Realizacion Item',
			'monto_idi_editado' => 'Monto Idi Editado',
			'motivo_edicion' => 'Motivo Edicion',
		);
	}

    public static function load($id_revisor,$id_item,$show_error=true)
    {
        $item = RevisionItemPresupuesto::model()->find('id_revisor=:ir and id_item_presupuesto_postulacion=:ipp',array(
            ':ir'=>$id_revisor,':ipp'=>$id_item
        ));
        if ($item== null && $show_error)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $item;
    }

    public function convertToArray(){
        $return=array();
        $return['id']=$this->item->id;
        $return['id_categoria']=$this->item->id_categoria;
        $return['nombre']=$this->item->nombre;
        $return['precio']=$this->item->precio;
        $return['unidades']=$this->item->unidades;
        $return['idi']=$this->item->idi;
        $return['propio']=$this->item->propio;
        $return['otros']=$this->item->otros;
        $return['idi_sugerido']=$this->monto_idi_editado;
        $return['item_apropiado']=$this->item_de_costo_apropiado_nota;
        $return['monto_adecuado']=$this->montos_adecuados_a_la_realizacion_item;
        $return['comentario']=$this->motivo_edicion;
        return $return;
    }

    public function validateIdiSugerido($attribute,$params)
    {
        if(!isset($this->$attribute) || empty($this->$attribute))
            return;

        if($this->$attribute!=$this->idi && empty($this->motivo_edicion))
            $this->addError($this->motivo_edicion,'Debes ingresar un motivo de porque has editado el monto I+D+i.');
        return;
    }

}