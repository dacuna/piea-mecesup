<?php

/**
 * Modelo para la tabla "{{observacion_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{observacion_proyecto}}':
 * @property integer $id_proyecto
 * @property integer $id_revisor
 * @property string $extracto_comentario
 * @property string $resumen_comentario
 * @property string $objetivos_comentario
 * @property string $estado_del_arte_comentario
 * @property string $experiencia_previa_comentario
 * @property string $investigacion_comentario
 * @property string $desarrollo_comentario
 * @property string $innovacion_comentario
 * @property string $impacto_academico_comentario
 * @property string $impacto_universitario_social_comentario
 * @property string $proyecciones_comentario
 * @property string $equipo_trabajo_comentario
 * @property string $referentes_comentario
 * @property string $hitos_comentario
 * @property string $carta_gantt_comentario
 * @property string $presupuesto_comentario
 * @property string $anexos_comentario
 * @property string $fecha_creacion
 * @property string $fecha_actualizacion
 */
class ObservacionProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObservacionProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{observacion_proyecto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('extracto_comentario, resumen_comentario, objetivos_comentario, estado_del_arte_comentario, experiencia_previa_comentario, investigacion_comentario, desarrollo_comentario, innovacion_comentario, impacto_academico_comentario, impacto_universitario_social_comentario, proyecciones_comentario, equipo_trabajo_comentario, referentes_comentario, hitos_comentario, carta_gantt_comentario, presupuesto_comentario, anexos_comentario', 'safe'),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
        return array(
            'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
            'revisor' => array(self::BELONGS_TO, 'Persona', 'id_revisor'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proyecto' => 'Id Proyecto',
			'id_revisor' => 'Id Revisor',
			'extracto_comentario' => 'Extracto Comentario',
			'resumen_comentario' => 'Resumen Comentario',
			'objetivos_comentario' => 'Objetivos Comentario',
			'estado_del_arte_comentario' => 'Estado Del Arte Comentario',
			'experiencia_previa_comentario' => 'Experiencia Previa Comentario',
			'investigacion_comentario' => 'Investigacion Comentario',
			'desarrollo_comentario' => 'Desarrollo Comentario',
			'innovacion_comentario' => 'Innovacion Comentario',
			'impacto_academico_comentario' => 'Impacto Academico Comentario',
			'impacto_universitario_social_comentario' => 'Impacto Universitario Social Comentario',
			'proyecciones_comentario' => 'Proyecciones Comentario',
			'equipo_trabajo_comentario' => 'Equipo Trabajo Comentario',
			'referentes_comentario' => 'Referentes Comentario',
			'hitos_comentario' => 'Hitos Comentario',
			'carta_gantt_comentario' => 'Carta Gantt Comentario',
			'presupuesto_comentario' => 'Presupuesto Comentario',
			'anexos_comentario' => 'Anexos Comentario',
			'fecha_creacion' => 'Fecha Creacion',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_creacion',
                'updateAttribute' => 'fecha_actualizacion',
            )
        );
    }

    public static function cargarRevision($id, $uid)
    {
        $rev = ObservacionProyecto::model()->find('id_proyecto=:ip and id_revisor=:ir', array(
            ':ip' => $id,
            ':ir' => $uid
        ));
        if ($rev == null) {
            $revision = new ObservacionProyecto;
            $revision->id_proyecto = $id;
            $revision->id_revisor = $uid;
            return $revision;
        }
        return $rev;
    }
}