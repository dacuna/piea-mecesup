
<?php

/**
 * Modelo para la tabla "{{revisor_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{revisor_proyecto}}':
 * @property integer $id_usuario
 * @property integer $id_proyecto
 * @property integer $id_proceso_formulario
 * @property integer $estado_revision
 * @property integer $nota
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class RevisorProyecto extends CActiveRecord
{
    /**
     * Indica si se desea eliminar el revisor representado por el modelo. Cuando es false entonces
     * se desea hacer una insercion.
     * @var bool
     */
    public $delete=false;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id_usuario', 'exist','allowEmpty'=>false,'className'=>'Persona','attributeName'=>'id'),
            array('id_proyecto', 'exist','allowEmpty'=>false,'className'=>'Proyecto','attributeName'=>'id'),
            array('delete', 'boolean'),
            array('id_usuario', 'validateUsuarioEsRevisor'),
            array('id_proyecto', 'validateProyectoEsPostulacion'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RevisorProyecto the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{revisor_proyecto}}';
    }

    /**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
			'revisor' => array(self::BELONGS_TO, 'Persona', 'id_usuario'),
		);
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_usuario' => 'Id Usuario',
            'id_proyecto' => 'Id Proyecto',
            'id_proceso_formulario' => 'Id Proceso Formulario',
        );
    }

    public function validateUsuarioEsRevisor($attribute,$params)
    {
        $usuario=Persona::model()->findByPk($this->$attribute);
        if($usuario==null){
            $this->addError($this->$attribute,'El usuario seleccionado no es revisor.');
            return;
        }
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if(!am()->checkAccess("revisor_proceso_{$proceso->id}",$usuario->id))
            $this->addError($this->$attribute,'El usuario seleccionado no es revisor.');
        return;
    }

    public function validateProyectoEsPostulacion($attribute,$params)
    {
        $proyecto=Proyecto::model()->findByPk($this->$attribute);
        if($proyecto==null){
            $this->addError($this->$attribute,'El proyecto seleccionado no tiene una postulacion vigente.');
            return;
        }
        if($proyecto->es_postulacion!=1)
            $this->addError($this->$attribute,'El proyecto seleccionado no tiene una postulacion vigente.');
        return;
    }

    public function getEstado(){
        if($this->estado_revision==0)
            return "No se ha iniciado la revisi&oacute;n";
        if($this->estado_revision==1)
            return "No se ha completado la revisi&oacute;n";
        if($this->estado_revision==2)
            return "Revisi&oacute;n completada";
    }

}