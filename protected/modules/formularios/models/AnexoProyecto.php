<?php

/**
 * Modelo para la tabla "{{anexo_proyecto}}".
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{anexo_proyecto}}':
 * @property integer $id
 * @property integer $id_proyecto
 * @property string $nombre
 * @property string $filename
 * @property string $original_name
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $idProyecto
 */
class AnexoProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnexoProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{anexo_proyecto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('nombre', 'required'),
			array('nombre', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array Relaciones del modeo.
	 */
	public function relations()
	{
		return array(
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
		);
	}

    protected function afterDelete()
    {
        parent::afterDelete();
        //se elimina el archivo
        $path = app()->getBasePath()."/../../piea-files/postulaciones/{$this->id_proyecto}/".$this->filename;
        unlink($path);
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_proyecto' => 'Id Proyecto',
			'nombre' => 'Descripci&oacute;n',
			'filename' => 'Filename',
		);
	}

    public static function load($id,$proyecto)
    {
        $anexo=AnexoProyecto::model()->findByPk($id);
        if ($anexo== null || $anexo->id_proyecto!=$proyecto)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $anexo;
    }

}