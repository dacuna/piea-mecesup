<?php
/**
 * El modulo de formularios provee de las funcionalidades que permiten a los usuarios del sistema
 * postular, entre otras acciones,a los distintos formularios que PIEA ofrece a la comunidad.
 *
 * En este modulo se implementan las funcionalidades para que los usuarios puedan:
 *
 *  - Fondo I+D+i: postular al fondo, revisores revisar las postulaciones, en cada instante de la
 *                 postulacion se puede ver un resumen de los proyectos y revisiones, y, que los
 *                 usuarios puedan conocer los resultados del fondo.
 *  - FAE: que coordinadores y usuarios externos (con los permisos suficientes) puedan evaluar a
 *         usuarios de iniciativas y proyectos asignandoles FAE.
 *
 * @package   modules.formularios
 * @version   1.0
 * @since     2013-02-01
 * @author    dacuna <diego.acuna@usm.cl>
 */
class FormulariosModule extends CWebModule
{
    /**
     * Inicializa el modulo. Aqui se puede colocar cualquier fragmento de codigo que se requiera
     * que este disponible en cualquier clase del modulo.
     */
    public function init()
	{
		$this->setImport(array(
			'formularios.models.*',
			'formularios.components.*',
            'usuarios.components.Entidad'
		));
	}

    /**
     * Metodo a ejecutarse previo a la ejecucion de una accion de un controlador existentes dentro
     * del modulo.
     *
     * @param CController $controller el controlador
     * @param CAction $action la accion
     *
     * @return  bool si es que la accion debe ser ejecutada o no.
     */
    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
