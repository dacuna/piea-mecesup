<?php
/**
 * Controlador que contiene los metodos y acciones que permiten realizar revisiones de los proyectos
 * postulantes a fondos I+D+i.
 *
 * Este controlador posee las funcionalidades necesarias para realizar la revision de los proyectos que
 * enviaron su postulacion a los fondos I+D+i. Ademas, se agregaron acciones para que los revisores puedan
 * ver los proyectos a revisar y para que la administracion pueda asignar revisores a distintos proyectos.
 * Cada accion realizada tiene un permiso especial que permite ejecutar dicha accion,
 *
 * @version   0.01
 * @since     3/17/13
 * @author    dacuna <diego.acuna@usm.cl>
 */
class RevisionFondosIDIController extends Controller{

    /**
     * Accion que despliega toda la informacion de un proyecto junto el formulario de revision para
     * cada usuario que tiene permisos de revision sobre el proyecto.
     *
     * @param int $id ID del proyecto en revision
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   17-03-2013
     * @edit    31-03-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Se agrega verificacion de permisos del usuario que intenta ingresar su revision
     */
    public function actionRevisionProyecto($id){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if(user()->checkAccess("revisor_proceso_{$proceso->id}")){
            $revision_proyecto=RevisorProyecto::model()->find('id_usuario=:iu and id_proyecto=:ip',array(
               ':iu'=>user()->id,':ip'=>$id));
            if($revision_proyecto==null)
                throw new CHttpException(500,"No tienes los permisos necesarios para ver esta p&aacute;gina");
            $this->layout='//layouts/column1'; //solo a 1 columna para aprovechar el espacio
            $this->verificarEstadoProceso();
            $proyecto=Proyecto::loadPostulacion($id);
            $revision=RevisionProyecto::cargarRevision($id,user()->id);
            $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
            $actividades=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC',array(':ip'=>$proyecto->id));
            $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
            $total=array(0,0,0,0);
            $items_cat_total=array(array(0,0,0,0),array(0,0,0,0),array(0,0,0,0),array(0,0,0,0));
            $items_cat=array(array(),array(),array(),array());
            foreach ($items_presupuesto as $item) {
                $total[0]+=$item->precio*$item->unidades;
                $total[1]+=$item->idi;
                $total[2]+=$item->propio;
                $total[3]+=$item->otros;
                $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
                $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
                $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
                $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
                $items_cat[$item->id_categoria-1][]=$item;
            }
            $presupuesto=array('items'=>$items_cat,'totales_categoria'=>$items_cat_total,'totales'=>$total);

            if(isset($_POST['RevisionProyecto'])){
                $revision->attributes=$_POST['RevisionProyecto'];
                if($revision->save()){
                    user()->setFlash("success","Se ha guardado correctamente la revisi&oacute;n del proyecto.");
                    $this->refresh();
                }
            }

            $this->render('revision_proyecto',array(
                'proyecto'=>$proyecto,
                'hitos'=>$hitos,
                'actividades'=>$actividades,
                'presupuesto'=>$presupuesto,
                'revision'=>$revision
            ));
        }else
            throw new CHttpException(500,"No tienes los permisos suficientes para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite a un revisor evaluar items al presupuesto de un proyecto en postulacion. El metodo
     * preferido es mediante una peticion AJAX que retorna un string JSON ya sea con el objeto ingresado
     * o editado (exito) o con los mensajes de error en caso de falla. Para ejecutar la accion se necesitan
     * los permisos correspondientes sobre el proyecto. Para solicitar la edicion de una revision, se debe agregar
     * como parametro en el post JSON el id del item a editar. Si no se agrega, se considera que es una insercion.
     *
     * @param int $id ID del proyecto al cual se le desea evaluar un item
     *
     * @return  string JSON con el mensaje que corresponda (ver descripcion del metodo)
     * @throws  \CHttpException
     * @todo    Verificar que el usuario que ejecuta la accion tiene los permisos correspondientes
     *
     * @since   24-03-2013
     */
    public function actionRevisarItemPresupuesto($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model=(!$objData['esta_revisado']) ? new RevisionItemPresupuesto : RevisionItemPresupuesto::load(user()->id,
                $objData['id']);
            $model->attributes=$objData;
            //se valida que el item que se esta revisando pertenece al proyecto en revision
            $item=ItemPresupuestoPostulacion::load($objData['id'],$id);
            $model->id_item_presupuesto_postulacion=$item->id;
            $model->id_revisor=user()->id;
            if($model->idi!=$model->monto_idi_editado) $model->fue_editado=1;
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=$model->convertToArray();
                $return['mensaje']="Se ha guardado correctamente la revision del item.";
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>"Ha ocurrido un error al guardar la revision del item del presupuesto:");
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite a un observador evaluar items al presupuesto de un proyecto en postulacion. El metodo
     * preferido es mediante una peticion AJAX que retorna un string JSON ya sea con el objeto ingresado
     * o editado (exito) o con los mensajes de error en caso de falla. Para ejecutar la accion se necesitan
     * los permisos correspondientes sobre el proyecto. Para solicitar la edicion de una observacion, se debe agregar
     * como parametro en el post JSON el id del item a editar. Si no se agrega, se considera que es una insercion.
     *
     * @param int $id ID del proyecto al cual se le desea observar un item
     *
     * @return  string JSON con el mensaje que corresponda (ver descripcion del metodo)
     * @throws  \CHttpException
     * @todo    Verificar que el usuario que ejecuta la accion tiene los permisos correspondientes
     *
     * @since   04-06-2013
     */
    public function actionObservarItemPresupuesto($id){
        if(Yii::app()->request->isPostRequest)
        {
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if(!user()->checkAccess("observador_proceso_{$proceso->id}") && !user()->checkAccess('administrador'))
                throw new CHttpException(403,"No tienes los permisos para ver esta p&aacute;gina.");
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            //$model=(!$objData['esta_revisado']) ? new ObservacionProyecto : ObservacionProyecto::load(user()->id,$objData['id']);
            $model=new ObservacionItemPresupuesto;
            $model->attributes=$objData;
            //se valida que el item que se esta revisando pertenece al proyecto en revision
            $item=ItemPresupuestoPostulacion::load($objData['id'],$id);
            $model->id_item_presupuesto_postulacion=$item->id;
            $model->id_observador=user()->id;
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=$model->convertToArray();
                $return['mensaje']="Se ha guardado correctamente la observaci&oacute;n del item.";
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>"Ha ocurrido un error al guardar la revision del item del presupuesto:");
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(500,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite a un encargado (con los permisos correspondientes) asignar revisores de un proceso del
     * formulario de fondos I+D+i, proyectos especificos mediante una matriz de revisores vs proyectos.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   24-03-2013
     *
     * @edit    26-03-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Se agrega funcionalidad para obtener revisores y se finaliza la vista.
     */
    public function actionAsignacionRevisoresProyectos(){
        if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')){
            $this->layout='//layouts/column1'; //solo a 1 columna para aprovechar el espacio
            $proceso=FormularioPiea::procesoFormularioFondosIDI();

            $proyectos=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');

            $revisiones_asignadas=RevisorProyecto::model()->findAll('id_proceso_formulario=:ip',array(':ip'=>$proceso->id));
            $rev_final=array();
            foreach ($revisiones_asignadas as $revision) {
                $rev_final[md5($revision->id_proyecto.$revision->id_usuario)]=$revision;
            }

            $revisores=new AuthAssignment;
            $revisores->itemname="revisor_proceso_{$proceso->id}";
            $revisores=$revisores->search();

            $this->render('asignacion_revisores',array(
                'proyectos'=>$proyectos,
                'revisores'=>$revisores->getData(),
                'totalRevisores'=>$revisores->totalItemCount,
                'asignados'=>$rev_final
            ));
        }else
            $this->redirect(array('/'));
    }

    /**
     * Accion que permite entregar el permiso de revisor a un usuario en particular para el proceso actual del
     * formulario I+D+i y para un proyecto en particular
     *
     * @param int $id id del usuario que se quiere agregar como revisor, si no se especifica entonces se despliega
     *                el formulario de busqueda de usuarios.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   24-03-2013
     */
    public function actionDarPermisoDeRevisor($id=null){
        if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')){
            $model=new Persona('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Persona']))
                $model->attributes=$_GET['Persona'];

            if(isset($id)){
                $persona=Persona::load($id);
                $proceso=FormularioPiea::procesoFormularioFondosIDI();
                //se verifica que esta persona no tenga el permiso de revisor para el proceso actual
                if(!am()->checkAccess("revisor_proceso_{$proceso->id}",$persona->id)){
                    $transaction=app()->db->beginTransaction();
                    try {
                        //si no existe el item entonces se crea y se asigna al administrador como padre
                        if(am()->getAuthItem("revisor_proceso_{$proceso->id}")==null){
                            am()->createRole("revisor_proceso_{$proceso->id}");
                            am()->getAuthItem("administrador")->addChild("revisor_proceso_{$proceso->id}");
                            am()->getAuthItem("revisor_proceso_{$proceso->id}")->addChild("ver_menu_idi");
                        }
                        am()->assign("revisor_proceso_{$proceso->id}",$persona->id);
                        $transaction->commit();
                        user()->setFlash('success',"Se ha asignado correctamente como revisor al usuario {$persona->nombrePresentacion}");
                    }catch (CException $e){
                        $transaction->rollback();
                        user()->setFlash('error', 'Ha ocurrido un error al asignar al revisor. Por favor int&eacute;ntalo nuevamente.');
                        Yii::log("Error al asignar revisor. ID USUARIO {$persona->id}; ERROR: {$e->getTraceAsString()}",
                            CLogger::LEVEL_ERROR,"application.modules.formularios.revisionFondosIDI.darPermisoDeRevisor");
                    }
                }else
                    user()->setFlash("error","El usuario {$persona->nombrePresentacion} ya ha sido seleccionado como
                    revisor del proceso actual del formulario de fondos I+D+i.");
            }

            $this->render('dar_permiso_revisor',array(
                'model'=>$model
            ));
        }else
            $this->redirect(array('/'));
    }

    /**
     * Accion para asignar a un usuario el permiso para revisar un proyecto en particular. El metodo preferido
     * es mediante una peticion ajax, es por esto que la vista es construida teniendo dicho requerimiento
     * en mente. Para ejecutar la accion se deben tener los permisos correspondientes
     *
     * @return  string JSON con el mensaje final de la funcion.
     * @throws  \CHttpException
     * @todo    Verificar que el usuario que ejecuta la accion tiene los permisos correspondientes
     *
     * @since   26-03-2013
     */
    public function actionAsignarUsuarioProyecto(){
        if(Yii::app()->request->isPostRequest)
        {
            $model=new RevisorProyecto;
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $model->id_proceso_formulario=$proceso->id;
            //se verifica si es que ya existe el revisor, si ya existe entonces se debe eliminar
            if($model->validate()){
                $transaction=app()->db->beginTransaction();
                try {
                    $check=RevisorProyecto::model()->find('id_usuario=:iu and id_proyecto=:ip',array(
                        ':iu'=>$model->id_usuario,':ip'=>$model->id_proyecto
                    ));
                    if($check==null) $model->save(false);
                    else{
                        //se debe eliminar tambien la revision si es que existe
                        $rev=RevisionProyecto::model()->find('id_proyecto=:ip and id_revisor=:ir',array(
                            ':ip'=>$model->id_proyecto,':ir'=>$model->id_usuario
                        ));
                        if($rev!=null) $rev->delete();
                        $check->delete();
                    }
                    $transaction->commit();
                    //se envia nuevo modelo junto con mensaje de exito
                    $return=array('mensaje'=>'correcto');
                }catch (CException $e){
                    $transaction->rollback();
                    $return=array('mensaje'=>'error');
                    Yii::log("Error al asignar revisor. ID USUARIO {$model->id_usuario}; ERROR: {$e->getTraceAsString()}",
                        CLogger::LEVEL_ERROR,"application.modules.formularios.revisionFondosIDI.asignarUsuarioProyecto");
                }
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al asignar al usuario como revisor:');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite a un revisor listar que proyectos debe revisar entregando un acceso facil al formulario
     * de revision de los proyectos y al estado de la revision a dicho proyecto.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   30-03-2013
     */
    public function actionVerProyectosARevisar(){
        //se chequea si el usuario efectivamente es revisor del proceso
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if(user()->checkAccess("revisor_proceso_{$proceso->id}")){
            $data=new CActiveDataProvider('RevisorProyecto',array(
                'criteria'=>array(
                    'condition'=>'id_usuario=:iu and id_proceso_formulario=:ipf',
                    'params'=>array(':iu'=>user()->id,':ipf'=>$proceso->id),
                    'order'=>'id_proyecto',
                ),
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));
            $this->render('proyectos_a_revisar',array(
                'revisiones'=>$data
            ));
        }else
            throw new CHttpException(500,"No tienes los permisos necesarios para ver esta p&aacute;gina.");
    }
    
    /**
     * Accion que lista los revisores de el proceso actual del formulario de fondos I+D+i. Solo el administrador
     * puede listar a los revisores.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   07-04-2013
     */
    public function actionListarRevisores(){
        if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $revisores=AuthAssignment::model()->findAll('itemname=:in',array(':in'=>"revisor_proceso_{$proceso->id}"));
            $data=new CArrayDataProvider($revisores, array(
                'id'=>'revisores-provider',
                'keyField'=>false
            ));
            $this->render('listar_revisores',array(
               'revisores'=>$revisores,
               'data'=>$data
            ));
        }else
            throw new CHttpException(500,"No tienes los permisos necesarios para ver esta p&aacute;gina");
    }

    /**
     * Accion que permite ver al administrador del sistema el estado de las revisiones asociadas con un proyecto
     * en particular. En la vista de la accion se permite seleccionar que proyecto se desea ver.
     *
     * @param int $id ID del proyecto, por defecto no se especifica ninguno
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-03-2013
     */
    public function actionVerEstadoRevisionPorProyecto($id=null){
        if(user()->checkAccess('idi_estado_proyectos')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $proyectos=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');
            $proyecto=null;
            $revisiones=null;
            if($id!=null){
                $proyecto=Proyecto::loadPostulacion($id);
                $revisiones=new CActiveDataProvider('RevisorProyecto',array(
                    'criteria'=>array(
                        'condition'=>'id_proyecto=:ip and id_proceso_formulario=:ipf',
                        'params'=>array(':ip'=>$proyecto->id,':ipf'=>$proceso->id),
                        'order'=>'id_proyecto',
                    ),
                    'pagination'=>array(
                        'pageSize'=>20,
                    ),
                ));
            }
            $this->render('ver_estado_revision_por_proyecto',array(
                'proyectos'=>$proyectos,
                'proyecto'=>$proyecto,
                'revisiones'=>$revisiones
            ));
        }else
            throw new CHttpException(500,"No tienes los permisos suficientes para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite ver una revision hecha por un revisor a un determinado proyecto. Solamente el revisor
     * que hizo la revision junto con el administrador y la persona que postulo con el proyecto pueden ver dicha
     * revision hecha.
     *
     * @param int $user ID del usuario revisor
     * @param int $proyecto ID del proyecto revisado
     * @param int $e Si e=1 entonces se el user esta encriptado, es una cadena random pero el ultimo digito es el id del
     *               usuario
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   09-04-2013
     */
    public function actionVerRevisionHecha($user,$proyecto,$e=null){
        $this->layout='//layouts/column1'; //solo a 1 columna para aprovechar el espacio
        //se carga la revision
        if($e!=null){
            $user=explode(".",$user);
            $user=$user[1]-12;
        }
        $revision=RevisionProyecto::cargarRevision($proyecto,$user);
        //se chequean los permisos
        if(user()->id!=$revision->id_revisor && user()->id!=$revision->proyecto->perfilPostulacion->id_postulante
            && !user()->checkAccess('idi_estado_proyectos') && !user()->checkAccess('administrador'))
            throw new CHttpException(500,"No tienes los permisos suficientes para ingresar a esta p&aacute;gina");
        //todo ok, se muestra la revision

        $proyecto=Proyecto::loadPostulacion($revision->id_proyecto);
        $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
        $actividades=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC',array(':ip'=>$proyecto->id));
        $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
        $total=array(0,0,0,0);
        $items_cat_total=array(array(0,0,0,0),array(0,0,0,0),array(0,0,0,0),array(0,0,0,0));
        $items_cat=array(array(),array(),array(),array());
        foreach ($items_presupuesto as $item) {
            $total[0]+=$item->precio*$item->unidades;
            $total[1]+=$item->idi;
            $total[2]+=$item->propio;
            $total[3]+=$item->otros;
            $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
            $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
            $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
            $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
            $items_cat[$item->id_categoria-1][]=$item;
        }
        $presupuesto=array('items'=>$items_cat,'totales_categoria'=>$items_cat_total,'totales'=>$total);

        $this->render('ver_revision',array(
           'revision'=>$revision,
           'proyecto'=>$revision->proyecto,
           'hitos'=>$hitos,
           'actividades'=>$actividades,
           'presupuesto'=>$presupuesto,
        ));
    }

    /**
     * Accion para ver el estado final de las revisiones del fondo I+D+i. Se muestran los proyectos segmentados
     * por linea de postulacion y con nota de mayor a menor.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   23-04-2013
     */
    public function actionVerEstadoFinalRevisiones(){
        $this->layout="//layouts/column1";
        if(user()->checkAccess('administrador')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $proyectos_a=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and linea_postulacion=0 and  id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');

            $proyectos_final_a=array();
            foreach ($proyectos_a as $proyecto) {
                $proyectos_final_a[]=array(
                $proyecto->id,
                $proyecto->nombre,
                $proyecto->perfilPostulacion->postulante->nombreCompleto,
                round($proyecto->calcularEvaluacionPromedio(),2),
                $proyecto->calcularIDISolicitado(),
                round($proyecto->calcularIDIAsignado(),0),
                $proyecto->fueSeleccionado(),
                $proyecto->perfilPostulacion->postulante->email,
                $proyecto->campus
                );
            }

            usort($proyectos_final_a, "compareGrade");

            $proyectos_b=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and linea_postulacion=1 and id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');

            $proyectos_final_b=array();
            foreach ($proyectos_b as $proyecto) {
                $proyectos_final_b[]=array(
                    $proyecto->id,
                    $proyecto->nombre,
                    $proyecto->perfilPostulacion->postulante->nombreCompleto,
                    round($proyecto->calcularEvaluacionPromedio(),2),
                    $proyecto->calcularIDISolicitado(),
                    round($proyecto->calcularIDIAsignado(),0),
                    $proyecto->fueSeleccionado(),
                    $proyecto->perfilPostulacion->postulante->email,
                    $proyecto->campus
                    );
            }

            usort($proyectos_final_b, "compareGrade");

            $this->render('estado_final_revisiones',array(
                'linea_a'=>$proyectos_final_a,
                'linea_b'=>$proyectos_final_b,
                'seleccion'=>new ProyectoSeleccionado
            ));
        }else
            throw new CHttpException(403,"No tienes los permisos necesarios para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite al administrador del sistema seleccionar un proyecto en base a su nota y en base
     * a la linea de postulacion a la cual el proyecto esta postulando. El administrador tiene la posibilidad
     * de cambiar el monto idi asignado al proyecto segun el propio postulante a los fondos o segun lo
     * asignado por los revisores.
     *
     * @param int $id ID del proyecto que se desea seleccionar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   30-04-2013
     */
    public function actionSeleccionarProyecto($id){
        if(user()->checkAccess('administrador')){
            $seleccionado=new ProyectoSeleccionado;
            if(isset($_GET['monto'])){
                $proyecto=Proyecto::loadPostulacion($id);
                $seleccionado->monto_idi=$_GET['monto'];
                $seleccionado->id_proyecto=$proyecto->id;
                if($seleccionado->validate()){
                    $proceso=FormularioPiea::procesoFormularioFondosIDI();
                    $seleccionado->id_proceso_formulario=$proceso->id;
                    $seleccionado->save(false);
                    user()->setFlash("success","El proyecto se ha seleccionado correctamente. Ahora, debes ingresar las modificaciones
                    en el presupuesto de &eacute;ste. Esto se realiza en la observaci&oacute;n realizada para el presente proyecto.");
                    $this->redirect(array('/formularios/revisionFondosIDI/observarProyecto','id'=>$proyecto->id));
                }else{
                    user()->setFlash("error","Debes ingresar un monto I+D+i para el proyecto seleccionado");
                    $this->redirect(array('/formularios/revisionFondosIDI/verEstadoFinalRevisiones'));
                }
            }
            $this->redirect(array('/formularios/revisionFondosIDI/verEstadoFinalRevisiones'));
        }else
            throw new CHttpException(403,"No tienes los permisos necesarios para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite a un observador de los fondos I+D+i, listar todos los proyectos a observar (que en
     * realidad son TODOS los proyectos postulantes) mostrando un enlace que permite crear una observacion
     * para el proyecto en particular. Solo los usuarios con el permiso de observador pueden ejectuar la
     * presente accion.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   28-04-2013
     */
    public function actionObservarProyectos(){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if(user()->checkAccess("observador_proceso_{$proceso->id}")){
            $proyectos=new CActiveDataProvider('Proyecto',array(
                'criteria'=>array(
                    'with'=>'perfilPostulacion',
                    'condition'=>'perfilPostulacion.estado_postulacion=1 and perfilPostulacion.id_proceso_formulario=:ipf',
                    'params'=>array(':ipf'=>$proceso->id),
                    'order'=>'id',
                ),
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $this->render('observar_proyectos',array(
                'proyectos'=>$proyectos,
                'proceso'=>$proceso
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos para ingresar a la
            p&aacute;gina que has solicitado.");
    }

    /**
     * Accion que permite a un observador de los fondos I+D+i el realizar comentarios y observaciones sobre un
     * proyecto determinado por $id postulante a los fondos concursables I+D+i del proceso actual de dicho
     * formulario. Las observaciones se almacenan de la misma manera que las revisiones con la diferencia que no
     * se consideran dentro del promedio final del proyecto ya que no llevan una nota asociada.
     *
     * @param int $id ID del proyecto a observar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   28-04-2013
     */
    public function actionObservarProyecto($id){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if(user()->checkAccess("observador_proceso_{$proceso->id}") || user()->checkAccess('administrador')){
            $this->layout='//layouts/column1'; //solo a 1 columna para aprovechar el espacio
            $proyecto=Proyecto::loadPostulacion($id);
            $revision=ObservacionProyecto::cargarRevision($id,user()->id);
            $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
            $actividades=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC',array(':ip'=>$proyecto->id));
            $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
            $total=array(0,0,0,0);
            $items_cat_total=array(array(0,0,0,0),array(0,0,0,0),array(0,0,0,0),array(0,0,0,0));
            $items_cat=array(array(),array(),array(),array());
            foreach ($items_presupuesto as $item) {
                $total[0]+=$item->precio*$item->unidades;
                $total[1]+=$item->idi;
                $total[2]+=$item->propio;
                $total[3]+=$item->otros;
                $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
                $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
                $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
                $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
                $items_cat[$item->id_categoria-1][]=$item;
            }
            $presupuesto=array('items'=>$items_cat,'totales_categoria'=>$items_cat_total,'totales'=>$total);

            if(isset($_POST['ObservacionProyecto'])){
                $revision->attributes=$_POST['ObservacionProyecto'];
                if($revision->save()){
                    user()->setFlash("success","Se ha guardado correctamente la observaci&oacute;n del proyecto.");
                    if($proceso->formulario->estado>3){
                        //se hizo una observacion pero fuera de la revision, esto es en el proceso de seleccion de proyectos
                        user()->setFlash("success","Se ha seleccionado y guardado la observacion del proyecto exitosamente.");
                        $this->redirect(array('/formularios/revisionFondosIDI/verEstadoFinalRevisiones'));
                    }else{
                        user()->setFlash("success","Se ha guardado correctamente la observaci&oacute;n del proyecto.");
                        $this->refresh();
                    }
                }
            }

            $this->render('observar_proyecto',array(
                'proyecto'=>$proyecto,
                'hitos'=>$hitos,
                'actividades'=>$actividades,
                'presupuesto'=>$presupuesto,
                'revision'=>$revision,
                'verificacion'=>$this->verificarEstadoProceso(false)
            ));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no tienes los permisos para ingresar a la
            p&aacute;gina que has solicitado.");
    }

    /**
     * Accion que permite a usuarios con los permisos necesarios del sistema, ver estadisticas del proceso de seleccion
     * llevado a cabo. Las estadisticas basicamente corresponden a los promedios de notas obtenidos por cada item de
     * evaluacion que realizo cada revisor.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   30-04-2013
     */
    public function actionEstadisticasProceso(){
        $e=new EstadisticaIdi;
        $estadisticas=$e->generarEstadisticas();
        $this->render('estadisticas_proceso',array(
           'estadistica'=>$estadisticas
        ));
    }

    /**
     * Permite al administrador generar un excel con todas las revisiones hechas a la fecha por los revisores. El
     * excel muestra el listado de todas las revisiones y todos los proyectos en revision. Solo el administrador
     * puede generar el excel.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   22-05-2013
     */
    public function actionGenerarExcelConRevisiones(){
        if(user()->checkAccess('administrador')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            $proyectos=Proyecto::model()->with(array(
                'perfilPostulacion'=>array('condition'=>'perfilPostulacion.estado_postulacion=1 and id_proceso_formulario='.$proceso->id)
            ))->findAll('es_postulacion=1');
            $rows=array(); //contiene cada fila del excel
            foreach ($proyectos as $proyecto) {
                //se obtienen las revisiones
                $revisiones=RevisionProyecto::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
                foreach ($revisiones as $revision) {
                    $row=array('id'=>$proyecto->id,'nombre'=>$proyecto->nombre,'id_revisor'=>$revision->id_revisor,'revisor'=>$revision->revisor->nombreCompleto,
                    'email_revisor'=>$revision->revisor->email,'linea_postulacion'=>$proyecto->perfilPostulacion->getLineaPostulacion(true),
                    'forma'=>$revision->calcularNotaForma(),'ortografia'=>$revision->calcularNotaOrtografia(),'sintesis'=>$revision->calcularNotaCapacidadSintesis(),'redaccion'=>$revision->calcularNotaRedaccion(),'anexos'=>$revision->calcularNotaPertinenciaAnexos(),
                    'fondo'=>$revision->calcularNotaFondo(),'equipo_trabajo'=>$revision->calcularNotaEquipoTrabajo(),'objetivos'=>$revision->calcularNotaObjetivos(),'idi'=>$revision->calcularNotaIDI(),'antecedentes'=>$revision->calcularNotaAntecedentes()
                    ,'planificacion'=>$revision->calcularNotaPlanificacion(),'hito'=>$revision->calcularNotaHito(),'carta_gantt'=>$revision->calcularNotaCartaGantt(),'gastos'=>$revision->calcularNotaGastos()
                    ,'impacto'=>$revision->calcularNotaImpacto(),'academico'=>$revision->calcularNotaAcademico(),'us'=>$revision->calcularNotaUniversitarioSocial(),'proyecciones'=>$revision->calcularNotaProyecciones()
                    ,'nota'=>$revision->calcularNotaRevision());
                    $rows[]=$row;
                }
            }
            //ahora se genera el excel
            $data=new CArrayDataProvider($rows, array(
                'id'=>'revisiones-provider',
                'keyField'=>false,
                'pagination'=>false
            ));

            // Export it (note the way we define columns, the same as in CGridView, thanks to EExcelView)
            $this->toExcel($data,
                array(
                    'id::ID proyecto',
                    'nombre::Nombre proyecto',
                    'id_revisor::ID revisor',
                    'revisor::Nombre revisor',
                    'email_revisor::Email revisor',
                    'linea_postulacion::Linea de postulacion',
                    'forma::Nota forma',
                    'ortografia::Nota ortografia',
                    'sintesis::Nota capacidad sintesis',
                    'redaccion::Nota redaccion',
                    'anexos::Nota pertinencia anexos',
                    'fondo::Nota fondo',
                    'equipo_trabajo::Nota equipo de trabajo',
                    'objetivos::Nota objetivos',
                    'idi::Nota I+D+i',
                    'antecedentes::Nota antecedentes',
                    'planificacion::Nota planificacion',
                    'hito::Nota hito',
                    'carta_gantt::Nota carta gantt',
                    'gastos::Nota gastos',
                    'impacto::Nota impacto',
                    'academico::Nota academico',
                    'us::Nota universitario/social',
                    'proyecciones::Nota proyecciones',
                    'nota::Nota final',
                ),
                'Test File',
                array(
                    'creator' => 'Zen',
                ),
                'Excel2007' // This is the default value, so you can omit it. You can export to CSV, PDF or HTML too
            );
        }else
            throw new CHttpException(403, "No tienes los permisos necesarios para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite al usuario administrador del sistema generar un excel con los datos de todos los revisores
     * que tienen permisos de revision en el proceso actual de formulario de postulacion a fondos I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-05-2013
     */
    public function actionGenerarExcelRevisores(){
        if(user()->checkAccess('administrador')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                $revisores=AuthAssignment::model()->findAll('itemname=:in',array(':in'=>"revisor_proceso_{$proceso->id}"));
                $data=new CArrayDataProvider($revisores, array(
                    'id'=>'revisores-provider',
                    'keyField'=>false,
                    'pagination'=>false
                ));

                $this->toExcel($revisores,
                    array(
                        'usuario.id::ID usuario',
                        'usuario.nombres::Nombres',
                        'usuario.apellido_paterno::Apellido Paterno',
                        'usuario.apellido_materno::Apellido Materno',
                        'usuario.email',
                    ),
                    'Usuarios revisores IDI',
                    array(
                        'creator' => 'Zen',
                    ),
                    'Excel2007' // This is the default value, so you can omit it. You can export to CSV, PDF or HTML too
                );
            }
        }else
            throw new CHttpException(403, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
    }

    /**
     * Permite verificar si es que el proceso actual del formulario de fondos I+D+i aun permite que se revisen
     * proyectos en postulacion.
     *
     * @param bool true si es que se desea lanzar una exepcion si es que el formulario esta cerrado, false caso contrario.
     *
     * @return  bool true si es que aun se permiten
     * @throws  \CHttpException
     *
     * @since   17-03-2013
     */
    private function verificarEstadoProceso($throw_ex=true){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if($proceso->formulario->estado!=3){
            if($throw_ex) throw new CHttpException(403,"Estimado usuario, el formulario de fondos I+D+i no se encuentra
                en proceso de selecci&oacute;n de revisores.");
            else return false;
        }
        return true;
    }

    /**
     * Behaviors del componente controlador.
     *
     * @return  array array de behaviors
     *
     * @since   09-04-2013
     */
    public function behaviors()
    {
        return array(
            'eexcelview'=>array(
                'class'=>'ext.eexcelview.EExcelBehavior',
            ),
        );
    }

}
