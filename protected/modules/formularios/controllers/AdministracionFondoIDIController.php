<?php
/**
 * Controlador para funciones de administracion del fondo I+D+i.
 *
 * Este controlador posee las funciones para que un usuario con los permisos correspondientes
 * pueda administrar las caracteristicas principales del formulario de fondos I+D+i.
 *
 * @category  Controller
 * @package   formularios
 * @version   1.0
 * @since     2013-03-15
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AdministracionFondoIDIController extends Controller
{
    /**
     * Accion para ver los proyectos que estan en postulacion en el proceso
     * actual del formulario I+D+i. Lista <b>todos</b> los proyectos que estan
     * en postulacion, independiente de aquellos que no hayan aun sido enviados.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   15-03-2013
     */
    public function actionVerProyectosEnPostulacion(){
        if(user()->checkAccess('idi_ver_proyectos_postulantes')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                //se cargan los proyectos
                $perfiles=new CActiveDataProvider('PerfilProyecto',array(
                    'criteria'=>array(
                        'condition'=>'id_proceso_formulario='.$proceso->id,
                        'order'=>'id_proyecto',
                    ),
                    'pagination'=>array(
                        'pageSize'=>20,
                    ),
                ));
                /*$perfiles=PerfilProyecto::model()->findAll('id_proceso_formulario==:pid',
                    array(':pid'=>$proceso->id));*/
                $this->render('ver_proyectos_en_postulacion',array(
                    'perfiles'=>$perfiles
                ));
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion para ver los proyectos que enviaron su postulacion en el proceso
     * actual del formulario I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   15-03-2013
     */
    public function actionVerProyectosPostulantes(){
        if(user()->checkAccess('idi_ver_proyectos_postulantes')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                //se cargan los proyectos
                $perfiles=new CActiveDataProvider('PerfilProyecto',array(
                    'criteria'=>array(
                        'condition'=>'estado_postulacion=1 and id_proceso_formulario='.$proceso->id,
                        'order'=>'id_proyecto',
                    ),
                    'pagination'=>array(
                        'pageSize'=>20,
                    ),
                ));

                $this->render('ver_proyectos_postulantes',array(
                    'perfiles'=>$perfiles
                ));
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el abrir  el periodo de revision
     * del formulario de fondos I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   02-04-2013
     */
    public function actionAbrirPeriodoRevision(){
        if(user()->checkAccess('idi_abrir_revision')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                $formulario=$proceso->formulario;
                $formulario->estado=3;
                $transaction=app()->db->beginTransaction();
                try {
                    $formulario->save(false);
                    $transaction->commit();
                    user()->setFlash("success","Se ha abierto el per&iacute;odo de revisiones correctamente.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                } catch(CException $e) {
                    $transaction->rollback();
                    Yii::log("'Ha ocurrido un error: al abrir el periodo de revisiones del formulario IDI. ERROR:
                    {$e->getMessage()}","error","app.modules.formularios.administracionFondoIDI.abrirPeriodoRevision");
                    user()->setFlash('error',"Ha ocurrido un error inesperado. Por favor, int&eacute;ntalo nuevamente.
                    Si los problemas persisten, comunicate con el administrador del sistema.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                }
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el cerrar el periodo de postulaciones
     * del formulario de fondos I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   02-04-2013
     */
    public function actionCerrarPeriodoPostulacion(){
        if(user()->checkAccess('idi_cerrar_postulacion')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                $formulario=$proceso->formulario;
                $formulario->estado=2;
                $transaction=app()->db->beginTransaction();
                try {
                    $formulario->save(false);
                    $transaction->commit();
                    user()->setFlash("success","Se ha cerrado el per&iacute;odo de postulaciones correctamente.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                } catch(CException $e) {
                    $transaction->rollback();
                    Yii::log("'Ha ocurrido un error: al cerrar el periodo de postulaciones del formulario IDI. ERROR:
                    {$e->getMessage()}","error","app.modules.formularios.administracionFondoIDI.cerrarPeriodoPostulacion");
                    user()->setFlash('error',"Ha ocurrido un error inesperado. Por favor, int&eacute;ntalo nuevamente.
                    Si los problemas persisten, comunicate con el administrador del sistema.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                }
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el cerrar el periodo de revision
     * del formulario de fondos I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   02-04-2013
     */
    public function actionCerrarPeriodoRevision(){
        if(user()->checkAccess('idi_cerrar_revision')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                $formulario=$proceso->formulario;
                $formulario->estado=4;
                $transaction=app()->db->beginTransaction();
                try {
                    $formulario->save(false);
                    $transaction->commit();
                    user()->setFlash("success","Se ha cerrado el per&iacute;odo de revisiones correctamente.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                } catch(CException $e) {
                    $transaction->rollback();
                    Yii::log("'Ha ocurrido un error: al cerrar el periodo de revisiones del formulario IDI. ERROR:
                    {$e->getMessage()}","error","app.modules.formularios.administracionFondoIDI.cerrarPeriodoRevision");
                    user()->setFlash('error',"Ha ocurrido un error inesperado. Por favor, int&eacute;ntalo nuevamente.
                    Si los problemas persisten, comunicate con el administrador del sistema.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                }
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion que permite a un usuario con los permisos correspondientes el abrir el periodo de muestra de los resultados
     * del formulario de fondos I+D+i.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   02-04-2013
     */
    public function actionAbrirPeriodoResultados(){
        if(user()->checkAccess('idi_abrir_resultados')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                $formulario=$proceso->formulario;
                $formulario->estado=5;
                $transaction=app()->db->beginTransaction();
                try {
                    $formulario->save(false);
                    $transaction->commit();
                    user()->setFlash("success","Se ha abierto el per&iacute;odo de exposici&oacute;n de resultados correctamente.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                } catch(CException $e) {
                    $transaction->rollback();
                    Yii::log("'Ha ocurrido un error: al abrir el periodo de exposicion de resultados del formulario IDI. ERROR:
                    {$e->getMessage()}","error","app.modules.formularios.administracionFondoIDI.cerrarPeriodoPostulacion");
                    user()->setFlash('error',"Ha ocurrido un error inesperado. Por favor, int&eacute;ntalo nuevamente.
                    Si los problemas persisten, comunicate con el administrador del sistema.");
                    $this->redirect(array('/formularios/administracionFondos/verFormulario','id'=>$formulario->id));
                }
            }
        }else{
            throw new CHttpException(500, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
        }
    }

    /**
     * Accion que permite al administrador del sistema generar un excel con todos los usuarios postulantes al
     * proceso actual de postulacion de fondos I+D+i. Se adjunta tambien cual es el proyecto con el que estan
     * postulando.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   31-05-2013
     */
    public function actionGenerarExcelPostulantes(){
        if(user()->checkAccess('administrador')){
            $proceso=FormularioPiea::procesoFormularioFondosIDI();
            if($proceso!=null){
                //se cargan los proyectos
                $perfiles=new CActiveDataProvider('PerfilProyecto',array(
                    'criteria'=>array(
                        'condition'=>'estado_postulacion=1 and id_proceso_formulario='.$proceso->id,
                        'order'=>'id_proyecto',
                    ),
                    'pagination'=>false,
                ));

                $this->toExcel($perfiles,
                    array(
                        'postulante.id::ID usuario',
                        'postulante.nombres::Nombres',
                        'postulante.apellido_paterno::Apellido Paterno',
                        'postulante.apellido_materno::Apellido Materno',
                        'postulante.email',
                        'id_proyecto::Proyecto ID',
                        'proyecto.nombre::Proyecto Nombre',
                    ),
                    'Usuarios postulantes IDI',
                    array(
                        'creator' => 'Zen',
                    ),
                    'Excel2007' // This is the default value, so you can omit it. You can export to CSV, PDF or HTML too
                );
            }
        }else
            throw new CHttpException(403, "No tienes los permisos suficientes para ver esta p&aacute;gina.");
    }

    /**
     * Accion que permite a los usuarios con los permisos correspondientes el asignar proyectos a los
     * ayudantes de hitos I+D+i. Los proyectos que se pueden asignar son todos los proyectos del proceso
     * actual del formulario I+D+i.
     *
     * @param int $ayudante ID del ayudante administrativo
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   08-06-2013
     */
    public function actionAsignarProyectosAyudanteHitos($ayudante){
        if(user()->checkAccess('administrador')){
            $usuario=Persona::load($ayudante);
            //debe ser integrante de la mesa de investigacion
            $proyecto=Proyecto::model()->find('sigla=:s',array(':s'=>'MI'));
            $integrante=IntegranteProyecto::loadByPersonaId($usuario->id,$proyecto->id);
            if($integrante!=null){
                //se cargan los proyectos que quedaron seleccionados
                $proceso=FormularioPiea::procesoFormularioFondosIDI();
                $proyectos=ProyectoSeleccionado::model()->findAll('id_proceso_formulario=:ipf',array(
                    ':ipf'=>$proceso->id));
                //ver que proyectos ya tiene asignado
                $asignados=array();
                foreach ($proyectos as $proyecto) {
                    $permiso="ayudante_hitos_proyecto_{$proyecto->id_proyecto}";
                    if(am()->checkAccess($permiso,$usuario->id))
                        $asignados[]=$proyecto->id_proyecto;
                }
                $this->render('asignar_proyectos_ayudante',array(
                   'proyectos'=>$proyectos,
                    'usuario'=>$usuario,
                    'asignados'=>$asignados
                ));
            }else
                $this->redirect(array('/'));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no posees los permisos necesarios para ver
            esta p&aacute;gina.");
    }

    /**
     * Accion que ejecuta la asignacion de proyectos realizada en la funcion de asignarProyectosAyudantes Hitos.
     * Permite tanto asignar como revocar un permiso.
     *
     * @param int $ayudante ID del ayudante administrativo
     * @param int $proyecto ID del proyecto a asignar/revocar
     * @param int $accion 1 asignar, 2 revocar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   09-06-2013
     */
    public function actionAccionProyectosAyudanteHitos($ayudante,$proyecto,$accion=1){
        if(user()->checkAccess('administrador')){
            $usuario=Persona::load($ayudante);
            //debe ser integrante de la mesa de investigacion
            $proyecto=Proyecto::loadPostulacion($proyecto);
            //se esta asignando sino revocando
            $permiso="ayudante_hitos_proyecto_{$proyecto->id}";
            if($accion==1){
                if(am()->getAuthItem($permiso)==null){
                    am()->createRole($permiso);
                    am()->getAuthItem("administrador")->addChild($permiso);
                }
                am()->assign($permiso,$usuario->id);
                user()->setFlash("success","Se ha asignado correctamente el proyecto");
            }else{
                if(am()->getAuthItem($permiso)!=null){
                    am()->revoke($permiso,$usuario->id);
                }
                user()->setFlash("success","Se ha revocado correctamente el proyecto");
            }
            $this->redirect(array('/formularios/administracionFondoIDI/asignarProyectosAyudanteHitos','ayudante'=>
                $usuario->id));
        }else
            throw new CHttpException(403,"El sistema ha detectado que no posees los permisos necesarios para ver
            esta p&aacute;gina.");
    }
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Behaviors del componente controlador.
     *
     * @return  array array de behaviors
     *
     * @since   31-05-2013
     */
    public function behaviors()
    {
        return array(
            'eexcelview'=>array(
                'class'=>'ext.eexcelview.EExcelBehavior',
            ),
        );
    }

}
