<?php
Yii::import('xupload.models.XUploadForm');

/**
 * Controlador para las acciones principales a realizar para una postulacion para los fondos
 * concursables I+D+i.
 *
 * Contiene las acciones principales que realizan los usuarios que postulan a fondos
 * concursables I+D+i como por ejemplo crear un proyecto, agregar hitos, enviar postulacion
 * etc.
 *
 * @version   0.01
 * @since     01/02/2013
 * @author    dacuna <diego.acuna@usm.cl>
 *
 * @todo En aceptar/rechazar membresia se debe comprobar (aparte de que el proyecto no haya sido
 * enviado a revision) que el proceso actual del formulario de postulacion sea el mismo que el del
 * proyecto donde se esta intentando aceptar/rechazar una membresia. Notar que la funcion
 * Proyecto::loadPostulacion($id,true) ya verifica dicha situacion.
 */
class PostulacionFondosIDIController extends Controller
{
    /**
     * Inicializacion del controlador. Se adjunta el handler de notificaciones.
     *
     * @since   10-02-2013
     */
    public function init(){
        $this->onSendUserNotificacion=array(app()->mailman,'notificacionHandler');
        //me conecto con el evento de envio de mensajes del mailman
        app()->mailman->onMensajeEnviado=array($this,"updateIntegranteUID");
        Yii::log("'Attached handler for onMensajeEnviado: callback updateIntegranteUID","info",
            "app.modules.formularios.PostulacionFondosIDIController");
    }

    /**
     * Accion que permite ver el estado actual de un proyecto en postulacion. Si el proyecto
     * no ha sido enviado entonces se presenta un resumen general de este, mostrando que cosas
     * le faltan por completar para poder enviarlo. Si el proyecto ya fue enviado entonces se
     * permite generar un archivo PDF con los datos de este ademas de ver en que estado de la
     * postulacion se encuentra.
     *
     * @param int $id ID del proyecto a ver
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   13-02-2013
     */
    public function actionVerProyecto($id){
        $proyecto=Proyecto::loadPostulacion($id);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
        $actividades=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=0 order by fecha ASC',array(':ip'=>$proyecto->id));

        $estadoPerfil=$proyecto->estadoPerfilPostulacion();
        $estadoFinanciero=$proyecto->estadoFinancieroPostulacion();
        $estadoIntegrantes=$proyecto->estadoIntegrantes();

        $show_send=$proyecto->estadoPostulacion();

        //este codigo permite mostrar la vista de revisiones si es que el proyecto fue enviado y si es que el proceso de
        //revisiones a terminado
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        $revisiones=null;
        if($proyecto->perfilPostulacion->estado_postulacion==1 && $proceso->formulario->estado>=4){
            //se deben mostrar las revisiones de este proyecto
            $revisiones=new CActiveDataProvider('RevisorProyecto',array(
                'criteria'=>array(
                    'condition'=>'id_proyecto=:ip and id_proceso_formulario=:ipf and nota is not null',
                    'params'=>array(':ip'=>$proyecto->id,':ipf'=>$proceso->id),
                    'order'=>'id_proyecto',
                ),
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));
        }

        $this->render('ver_proyecto',array(
           'proyecto'=>$proyecto,
           'hitos'=>$hitos,
           'actividades'=>$actividades,
           'estadoPerfil'=>$estadoPerfil,
           'estadoFinanciero'=>$estadoFinanciero,
           'estadoIntegrantes'=>$estadoIntegrantes,
           'showSend'=>$show_send,
           'estadoProceso'=>$this->verificarEstadoProceso(false),
           'revisiones'=>$revisiones
        ));
    }

    /**
     * Accion que permite generar el pdf resumen del proyecto en postulacion. Esta funcion tiene
     * sentido solo para proyectos que ya han sido enviados a revision, es decir, que tienen un
     * perfil de postulacion completo. Para ejecutar la accion se deben tener los permisos
     * correspondientes.
     *
     * @param int $id ID del proyecto en postulacion
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   25-02-2013
     */
    public function actionGenerarPdfProyecto($id){
        $proyecto=Proyecto::loadPostulacion($id);
        if(!$proyecto->checkPermisoPostulacion(user()->id) && !user()->checkAccess('idi_ver_proyectos_enviados')
            && !user()->checkAccess('idi_ver_proyectos_postulantes'))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
        $actividades=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC',array(':ip'=>$proyecto->id));
        $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
        $total=array(0,0,0,0);
        $items_cat_total=array(array(0,0,0,0),array(0,0,0,0),array(0,0,0,0),array(0,0,0,0));
        $items_cat=array(array(),array(),array(),array());
        foreach ($items_presupuesto as $item) {
            $total[0]+=$item->precio*$item->unidades;
            $total[1]+=$item->idi;
            $total[2]+=$item->propio;
            $total[3]+=$item->otros;
            $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
            $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
            $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
            $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
            $items_cat[$item->id_categoria-1][]=$item;
        }

        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('_pdf_proyecto',
            array('proyecto'=>$proyecto,'perfil'=>$proyecto->perfilPostulacion,'hitos'=>$hitos,'actividades'=>$actividades,
            'items_cat'=>$items_cat,'items_cat_total'=>$items_cat_total,'total'=>$total), true));
        $html2pdf->Output();
    }

    /**
     * Accion que permite generar el pdf resumen del proyecto de un proyecto que historicamente postulo a los
     * fondos I+D+i. La unica diferencia es que los hitos y la carta gantt generada es la historica y no contiente
     * las modificaciones futuras que pudieron haber afectado a estas.
     *
     * @param int $id ID del proyecto que postulo a los fondos I+D+i
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   10-06-2013
     */
    public function actionGenerarPdfHistoricoProyecto($id){
        $proyecto=Proyecto::loadPostulacion($id);
        if(!$proyecto->checkPermisoPostulacion(user()->id) && !user()->checkAccess('idi_ver_proyectos_enviados')
            && !user()->checkAccess('idi_ver_proyectos_postulantes') && !user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}"))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $hitos=HistoricoHitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC',array(':ip'=>$proyecto->id));
        $actividades=HistoricoHitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC',array(':ip'=>$proyecto->id));
        $items_presupuesto=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip',array(':ip'=>$proyecto->id));
        $total=array(0,0,0,0);
        $items_cat_total=array(array(0,0,0,0),array(0,0,0,0),array(0,0,0,0),array(0,0,0,0));
        $items_cat=array(array(),array(),array(),array());
        foreach ($items_presupuesto as $item) {
            $total[0]+=$item->precio*$item->unidades;
            $total[1]+=$item->idi;
            $total[2]+=$item->propio;
            $total[3]+=$item->otros;
            $items_cat_total[$item->id_categoria-1][0]+=$item->precio*$item->unidades;
            $items_cat_total[$item->id_categoria-1][1]+=$item->idi;
            $items_cat_total[$item->id_categoria-1][2]+=$item->propio;
            $items_cat_total[$item->id_categoria-1][3]+=$item->otros;
            $items_cat[$item->id_categoria-1][]=$item;
        }

        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('_pdf_proyecto',
            array('proyecto'=>$proyecto,'perfil'=>$proyecto->perfilPostulacion,'hitos'=>$hitos,'actividades'=>$actividades,
                'items_cat'=>$items_cat,'items_cat_total'=>$items_cat_total,'total'=>$total), true));
        $html2pdf->Output();
    }

    /**
     * Accion para generar un archivo .zip con los anexos de un proyecto en postulacion, Para ejecutar la accion
     * se deben tener los permisos correspondientes.
     *
     * @param int $id ID del proyecto donde se quiere obtener los anexos
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   12-03-2013
     */
    public function actionGenerarZipAnexos($id){
        $proyecto=Proyecto::loadPostulacion($id);
        if(!$proyecto->checkPermisoPostulacion(user()->id) && !user()->checkAccess('idi_ver_proyectos_enviados')
            && !user()->checkAccess('idi_ver_proyectos_postulantes') && !user()->checkAccess("ayudante_hitos_proyecto_{$proyecto->id}"))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        //se debe haber enviado el proyecto para generar el .zip
        if($proyecto->perfilPostulacion->estado_postulacion!=1)
            throw new CHttpException(404,"La p&aacute;gina solicitada no existe.");
        $zip_dir=app()->getBasePath()."/../../piea-files/postulaciones/{$proyecto->id}/{$proyecto->id}_anexos.zip";
        $anexos=array();
        foreach ($proyecto->anexos as $anexo) {
            $anexos[]=array(app()->getBasePath()."/../../piea-files/postulaciones/{$proyecto->id}/".$anexo->filename,
                $anexo->original_name);
        }
        create_zip($anexos,$zip_dir);
        //se envia el archivo
        if(file_exists($zip_dir)){
            $myfile = Yii::app()->file->set($zip_dir);
            $fakeName="Proyecto_{$proyecto->id}_anexos.zip";
            $myfile->download($fakeName);
        }else{
            user()->setFlash("error","Ha ocurrido un error al generar el .zip con los anexos. Por favor, int&eacute;ntelo
            nuevamente. Si los errores persisten contactar al administrador del sistema.");
            $this->redirect(array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id));
        }

    }
    
    /**
     * Accion para crear/editar un proyecto a postulacion. Un usuario del sistema solo
     * puede postular con 1 proyecto por proceso del fondo. Para entrar en modo edicion
     * se debe pasar como parametro la id del proyecto a editar.
     *
     * @param int $id si es que es edicion, la id del proyecto a editar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   01-02-2013
     */
    public function actionCrearProyecto($id=null){
        $this->verificarEstadoProceso();
        $usuario=Persona::model()->findByPk(user()->getId());
        if($id==null && $usuario->esPostulanteFondoIDI){
            user()->setFlash("error","Ya est&aacute;s participando en el presente proceso de postulaci&oacute;n a fondos
            I+D+i. Solo puedes registrar una postulaci&oacute;n por proceso");
            $this->redirect(array('/'));
        }

        if($id==null){
            $proyecto=new Proyecto;
            $proyecto->estado_postulacion=0;
            $perfil=new PerfilProyecto;
            $title="Crear";
        }else{
            $proyecto=Proyecto::loadPostulacion($id,true);
            $perfil=$proyecto->perfilPostulacion;
            if($perfil==null) throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
            if(!$proyecto->checkPermisoPostulacion(user()->id)) throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
            $title="Editar";
        }
        $iniciativas=Iniciativa::model()->findAll();
        array_push($iniciativas,array('id'=>-1,'nombre_abreviado'=>"Proyecto sin relaci&oacute;n con iniciativas PIE&gt;A"));
        $iniciativas=array_reverse($iniciativas);

        if(isset($_POST['Proyecto'])){
            $proyecto->attributes=$_POST['Proyecto'];
            $valid=$proyecto->validate();
            if(isset($_POST['PerfilProyecto'])){
                $perfil->attributes=$_POST['PerfilProyecto'];
                $valid=$perfil->validate() && $valid;
            }

            if($valid){
                //se guarda la postulacion
                $transaction=app()->db->beginTransaction();
                try {
                    $proyecto->es_postulacion=1;
                    $proyecto->save(false);
                    $perfil->id_proyecto=$proyecto->primaryKey;
                    $perfil->save(false);
                    //se deben almacenar los objetivos especificos si es que hay alguno
                    foreach ($perfil->objetivos_especificos as $objetivo) {
                        ObjetivosEspecificosProyecto::create($proyecto->primaryKey,$objetivo);
                    }
                    if($id==null){
                        //se crea un nuevo integrante
                        $integrante=new IntegranteProyecto;
                        $integrante->proyecto_id=$proyecto->primaryKey;
                        $integrante->user_id=user()->id;
                        $integrante->estado=1;
                        $integrante->save(false);
                    }
                    $transaction->commit();
                    user()->setFlash("success","Se ha guardado correctamente el proyecto para postulaci&oacute;n. Para
                    continuar, debes completar la informaci&oacute;n restante del formulario dirigi&eacute;ndote al
                    men&uacute; \"Postulaci&oacute;n I+D+i\" ubicado en la parte izquierda del sitio.");
                    $this->redirect(array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->primaryKey));
                } catch(CException $e) {
                    $transaction->rollback();
                    Yii::log("'Ha ocurrido un error: al guardar proyecto para postulacion. USER ID: {$usuario->id}, ERROR:
                    {$e->getMessage()}","error","app.modules.formularios.PostulacionFondosIDIController.actionCrearProyecto");
                    user()->setFlash('error',"Ha ocurrido un error inesperado. Por favor, int&eacute;ntalo nuevamente.
                    Si los problemas persisten, comunicate con el administrador del sistema.");
                }
            }
        }

        $this->render('crear_proyecto',array(
           'proyecto'=>$proyecto,
           'perfil'=>$perfil,
           'iniciativas'=>$iniciativas,
           'title'=>$title
        ));
    }

    /**
     * Permite a un usuario con los permisos suficientes el eliminar un objetivo especifico
     * de su proyecto en postulacion.
     *
     * @param int $id ID del proyecto
     * @param int $obj ID del objetivo especifico
     *
     * @throws  \CHttpException
     */
    public function actionEliminarObjetivoEspecifico($id,$obj){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(403,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=ObjetivosEspecificosProyecto::model()->findByPk($obj);
        if($model!=null && $model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente el objetivo del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el objetivo. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para ver e los hitos de un proyecto en postulacion. Desde esta misma vista se
     * puede utilizar la accion para insertar hitos mediante ajax (esta es la forma
     * predeterminada de hacerlo).
     *
     * @param int $id ID del proyecto al cual se le agregan hitos
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   03-02-2013
     */
    public function actionHitosProyecto($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1',array(':ip'=>$proyecto->id));
        //para las inserciones ajax
        $model=new HitoProyecto;
        $this->render('ver_hitos',array(
           'hitos'=>$hitos,
           'model'=>$model,
           'proyecto'=>$proyecto
        ));
    }
    
    /**
     * Accion para agregar hitos a un proyecto en postulacion. El metodo preferido para agregar hitos
     * es mediante una peticion ajax, es por esto que la vista es construida teniendo dicho requerimiento
     * en mente.
     *
     * @param int $id ID del proyecto al cual se le desea agregar un hito
     *
     * @return  string JSON con el mensaje final de la funcion.
     * @throws  \CHttpException
     *
     * @since   03-02-2013
     */
    public function actionAgregarHito($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id,true);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $model=new HitoProyecto;
            $model->setScenario('agregarHito');
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=array('mensaje'=>'Se ha agregado correctamente el hito al proyecto.');
                $return['nombre']=$model->nombre;
                $return['descripcion']=$model->descripcion;
                $return['id']=$model->primaryKey;
                $return['fecha']=$model->fecha;
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al agregar el hito:');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite eliminar un hito de un proyecto mediante una peticion
     * GET, la respuesta es un objeto JSON con el mensaje ya sea de exito o de
     * fracaso. Para ejecutar la accion, se deben tener los permisos necesarios.
     *
     * @param int $id ID del proyecto donde se desea eliminar un hito
     * @param int $hito ID del hito a eliminar.
     *
     * @return  string respuesta JSON con mensaje correspondiente al status de la
     *                 peticion
     * @throws  \CHttpException
     * @since   04-02-2013
     */
    public function actionEliminarHito($id,$hito){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=HitoProyecto::load($hito,$id);
        if($model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente el hito del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el hito. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion que despliega la vista donde se exhibe el presupuesto de un proyecto. Ademas
     * en dicha vista se permite ingresar y eliminar items del presupuesto del proyecto
     * en postulacion.
     *
     * @param int $id ID del proyecto en postulacion al que se le desea ver el presupuesto
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   04-02-2013
     */
    public function actionPresupuestoProyecto($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $items1=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip and id_categoria=1',array(':ip'=>$proyecto->id));
        $items2=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip and id_categoria=2',array(':ip'=>$proyecto->id));
        $items3=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip and id_categoria=3',array(':ip'=>$proyecto->id));
        $items4=ItemPresupuestoPostulacion::model()->findAll('id_proyecto=:ip and id_categoria=4',array(':ip'=>$proyecto->id));
        //para las inserciones ajax
        $model=new ItemPresupuestoPostulacion;
        $this->render('ver_presupuesto',array(
            'item1'=>$items1,
            'item2'=>$items2,
            'item3'=>$items3,
            'item4'=>$items4,
            'model'=>$model,
            'proyecto'=>$proyecto
        ));
    }

    /**
     * Accion que permite agregar y editar items al presupuesto de un proyecto en postulacion. El metodo
     * preferido es mediante una peticion AJAX que retorna un string JSON ya sea con el objeto ingresado
     * o editado (exito) o con los mensajes de error en caso de falla. Para ejecutar la accion se necesitan
     * los permisos correspondientes sobre el proyecto. Para solicitar la edicion de un item, se debe agregar
     * como parametro en el post JSON el id del item a editar. Si no se agrega, se considera que es una insercion.
     *
     * @param int $id ID del proyecto al cual se le desea agregar/editar un item
     *
     * @return  string JSON con el mensaje que corresponda (ver descripcion del metodo)
     * @throws  \CHttpException
     *
     * @since   04-02-2013
     * @edit    17-02-2013<br />
     *          dacuna <diego.acuna@usm.cl><br />
     *          Agregado soporte para edicion de items de presupuesto.
     *          #edit1
     */
    public function actionAgregarItemPresupuesto($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id,true);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model=(!isset($objData['id'])) ? new ItemPresupuestoPostulacion : ItemPresupuestoPostulacion::load($objData['id'],$id);
            $text=(!isset($objData['id'])) ? array("agregar","agregado") : array("editar","editado");
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            if($model->save()){
                //se envia nuevo modelo junto con mensaje de exito
                $return=$model->convertToArray();
                $return['mensaje']="Se ha {$text[1]} correctamente el item al presupuesto del proyecto.";
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>"Ha ocurrido un error al {$text[0]} el item del presupuesto:");
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite eliminar un item del presupuesto de un proyecto mediante
     * una peticion GET, la respuesta es un objeto JSON con el mensaje ya sea de
     * exito o de fracaso. Para ejecutar la accion, se deben tener los permisos necesarios.
     *
     * @param int $id ID del proyecto donde se desea eliminar un item del presupuesto
     * @param int $item ID del item del presupuesto a eliminar.
     *
     * @return  string respuesta JSON con mensaje correspondiente al status de la
     *                 peticion
     * @throws  \CHttpException
     * @since   04-02-2013
     */
    public function actionEliminarItemPresupuesto($id,$item){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=ItemPresupuestoPostulacion::load($item,$proyecto->id);
        $categoria=$model->id_categoria;
        if($model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente el item del presupuesto.','success'=>true,'categoria'=>$categoria);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el item del presupuesto. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para agregar, ver y eliminar integrantes del equipo de trabajo del proyecto
     * en postulacion. Se deben tener los permisos necesarios para ver y ejecutar acciones
     * en esta accion vista.
     *
     * @param int $id ID del proyecto donde se ve el equipo de trabajo
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   05-02-2013
     */
    public function actionEquipoTrabajoProyecto($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=new IntegranteProyecto;
        $this->render('equipo_trabajo',array(
            'proyecto'=>$proyecto,
            'integrantes'=>$proyecto->integrantes,
            'estadoIntegrantes'=>$proyecto->estadoIntegrantes(),
            'model'=>$model
        ));
    }

    /**
     * Accion que permite buscar usuarios mediante una consulta AJAX. Retorna un
     * string JSON con la informacion del usuario encontrada. Para ejecutar esta
     * accion se debe estar logeado en el sistema.
     *
     * @param int $id ID del proyecto donde el usuario logeado esta postulando, sirve para
     *                filtrar a los integrantes de proyecto en la busqueda
     * @param string $q query a buscar
     * @param int $t tipo de busqueda. Puede ser por nombre de usuario o por email
     *
     * @return  string JSON con los usuarios encontrados
     * @throws  \CHttpException
     *
     * @since   05-02-2013
     */
    public function actionBuscarUsuarios($id,$q,$t){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $q=app()->input->stripClean($q);
        $t=app()->input->stripClean($t);
        if(!is_numeric($t)){
            Yii::log("Usuario ID ".user()->id." realizo peticion incorrecta (param t={$t}).",CLogger::LEVEL_WARNING,
                "app.modules.formularios.postulacionFondosIDI.actionBuscarUsuarios");
            throw new CHttpException(500,"Solicitud err&oacute;nea. Por favor no vuelvas a realizar esta petici&oacute;n");
        }
        $data=new Persona;
        if($t==0) //busqueda por mail
            $finded=$data->searchByMail($q);
        else if($t==1)
            $finded=$data->searchByName($q);
        else{
            Yii::log("Usuario ID ".user()->id." realizo peticion incorrecta (param t={$t}).",CLogger::LEVEL_WARNING,
                "app.modules.formularios.postulacionFondosIDI.actionBuscarUsuarios");
            throw new CHttpException(500,"Solicitud err&oacute;nea. Por favor no vuelvas a realizar esta petici&oacute;n");
        }
        $result=array();
        $result['results']=array();
        //filtro
        $integrantes=$proyecto->integrantesAsArray();
        foreach($finded->data as $entity){
            if(!in_array($entity->id,$integrantes))
                $result['results'][]=array('id'=>$entity->id,'nombre'=>$entity->nombreCompleto,'email'=>$entity->email);
        }
        $total=count($result['results']);
        $result['total']="Se han encontrado {$total} resultados que coinciden con '{$q}'";
        $this->renderJSON($result);
    }

    /**
     * Accion para agregar o editar un posible integrante al equipo de trabajo de un proyecto. La accion se
     * ejecuta mediante una llamada ajax al controlador que envia un string json. Para ejecutar la
     * accion se requieren los permisos necesarios. Cuando se agrega al usuario como integrante, este
     * debe ya sea aceptar o rechazar su participacion en el proyecto. Para editar un integrante se debe
     * incluir en la llamada json el campo editable=true.
     *
     * @param int $id ID del proyecto donde se desea agregar o editar el integrante
     *
     * @return  string JSON con el objeto insertado (success) o con los mensajes de error (error)
     * @throws  \CHttpException
     *
     * @since   08-02-2013
     */
    public function actionAgregarIntegrante($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id,true);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $model=new IntegranteProyecto;
            $model->setScenario("Postulacion");
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            if($objData['editable']==true){
                $model=IntegranteProyecto::load($objData['id'],$id);
                $model->lider=$objData['lider'];
                $model->finanzas=$objData['finanzas'];
                $model->responsabilidades=$objData['responsabilidades'];
                $model->estado=($model->user_id==user()->id) ? 1 : 0;
                $model->id_mensaje=null;
                $text=array("editar","editado");
            }else{
                $model->proyecto_id=$proyecto->id;
                $model->user_id=$objData['id'];
                $model->visibilidad=1;
                $model->estado=($model->user_id==user()->id) ? 1 : 0;
                $model->attributes=$objData;
                $text=array("agregar","agregado");
            }
            if($model->save()){
                $return=$model->convertToArray();
                $return['mensaje']="Se ha {$text[1]} correctamente al integrante al equipo de trabajo del proyecto.";
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>"Ha ocurrido un error al {$text[0]} el integrante al equipo:");
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion que permite enviar una notificacion a un integrante para que acepte su participacion en
     * un proyecto en postulacion. Si mientras la invitacion ya ha sido enviada se vuelve a editar
     * a un integrante, entonces se debe volver a enviar la invitacion. El id del integrante se pasa por
     * json hacia la accion.
     *
     * @param int $id ID del proyecto donde es integrante el usuario
     *
     * @return  string JSON con mensaje de exito/error segun sea el caso
     * @throws  \CHttpException
     *
     * @since   19-02-2013
     */
    public function actionEnviarNotificacionIntegrante($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $data = file_get_contents("php://input");
        $objData = json_decode($data,true);
        if(!isset($objData) && !is_numeric($objData)) throw new CHttpException(500,"Petici&oacute;n incorrecta.");
        $model=IntegranteProyecto::load($objData,$id);
        if($model->estado==1 || $model->estado==-1) throw new CHttpException(500,"Petici&oacute;n incorrecta.");
        //se envia notificacion a usuario de que se le ha agregado a un proyecto
        $model->estado=2;
        if($model->save(false)){
            $entidad=new Entidad($proyecto->id,1);
            $this->onSendUserNotificacion(new CEvent($this,array('tipo'=>'solicitud_equipo_trabajo',
                'proyecto'=>$proyecto,'integrante'=>$model,'destinatario'=>$model->user_id,'entidad'=>$entidad)));
            $this->renderJSON(array('mensaje'=>"Se ha enviado correctamente la solicitud al integrante."));
        }
        $this->renderJSON(array('mensajeError'=>"Ha ocurrido un error inesperado. Por favor, intentalo nuevamente."));
    }

    /**
     * Callback para settear el id del mensaje para la notificacion de solicitud de aceptacion
     * de integrante a un proyecto en postulacion.
     *
     * @param CEvent $event evento de envio del mensaje
     *
     * @return  bool true si se guarda el id del mensaje, false caso contrario.
     * @since   20-02-2013
     */
    public function updateIntegranteUID($event){
        if($event->params['tipo']=='solicitud_equipo_trabajo'){
            $id_proyecto=$event->params['mensaje']->id_relacion;
            $id_usuario=$event->params['mensaje']->destinatario;
            $integrate=IntegranteProyecto::model()->find('user_id=:ui and proyecto_id=:pi',array(':ui'=>$id_usuario,
                ':pi'=>$id_proyecto));
            if($integrate!=null){
                $integrate->id_mensaje=$event->params['mensaje']->primaryKey;
                return $integrate->save(false);
            }
        }
        return false;
    }

    /**
     * Accion que permite a integrantes en estado de pendiente, aceptar su membresia dentro
     * de un proyecto en postulacion. Para esto, se obtiene el mensaje enviado con la solicitud
     * de membresia y se procesa dicha solicitud.
     *
     * @param int $e ID del emisor del mensaje
     * @param int $d ID del receptor del mensaje
     * @param int $uid ID del mensaje
     * @param int $i ID del integrante del proyecto (en estado pendiente)
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   10-02-2013
     */
    public function actionAceptarMembresia($e, $d, $uid, $i){
        $this->verificarEstadoProceso();
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        $integrante=IntegranteProyecto::load($i,$mensaje->id_relacion);
        if($integrante->user_id!=user()->id || $integrante->estado!=2
            || $integrante->proyecto->perfilPostulacion->estado_postulacion!=0){
            $mensaje->contestado=1;$mensaje->save(false);
            throw new CHttpException(500,"Este mensaje ya no es v&aacute;lido (dadao que tus datos en el proyecto han sido
            actualizados o ya has previamente aceptado ser parte del proyecto).");
        }
        if($integrante->id_mensaje!=$mensaje->id){
            user()->setFlash("error","Este mensaje ya no es v&aacute;lido (dado que tus datos en el proyecto han sido
            actualizados y deber&aacute;s revisar esos nuevos cambios). Debes aceptar tu membres&iacute;a con el
            &uacute;ltimo mensaje que se te ha enviado relativo a tu membres&iacute;a en el proyecto.");
            $this->redirect(array('/buzon/inbox/index'));
        }
        $integrante->estado=1;
        //IntegranteProyecto::agregarIntegrante($integrante->user_id,$mensaje->id_relacion,false);
        $integrante->save(false);
        $mensaje->contestado=1;
        $mensaje->save(false);
        user()->setFlash("success","Se ha aceptado correctamente la membres&iacute;a al proyecto en postulaci&oacute;n");
        $this->redirect(array('/buzon/inbox/index'));
    }

    /**
     * Accion que permite a integrantes en estado de pendiente, rechazar su membresia dentro
     * de un proyecto en postulacion. Para esto, se obtiene el mensaje enviado con la solicitud
     * de membresia y se procesa dicha solicitud.
     *
     * @param int $e ID del emisor del mensaje
     * @param int $d ID del receptor del mensaje
     * @param int $uid ID del mensaje
     * @param int $i ID del integrante del proyecto (en estado pendiente)
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   10-02-2013
     */
    public function actionRechazarMembresia($e, $d, $uid, $i){
        $this->verificarEstadoProceso();
        $mensaje = Mensaje::obtenerMensaje($e, $d, $uid);
        $integrante=IntegranteProyecto::load($i,$mensaje->id_relacion);
        if($integrante->user_id!=user()->id || $integrante->estado!=2
            || $integrante->proyecto->perfilPostulacion->estado_postulacion!=0){
            $mensaje->contestado=1;$mensaje->save(false);
            throw new CHttpException(500,"Este mensaje ya no es v&aacute;lido (dado que tus datos en el proyecto han sido
            actualizados o ya has aceptado o rechazado tu participaci&oacute;n en el proyecto previamente.");
        }
        if($integrante->id_mensaje!=$mensaje->id){
            user()->setFlash("error","Este mensaje ya no es v&aacute;lido (dado que tus datos en el proyecto han sido
            actualizados y deber&aacute;s revisar esos nuevos cambios). Debes aceptar tu membres&iacute;a con el
            &uacute;ltimo mensaje que se te ha enviado relativo a tu membres&iacute;a en el proyecto.");
            $this->redirect(array('/buzon/inbox/index'));
        }
        $integrante->estado=-1;
        $transaction = app()->db->beginTransaction();
        try {
            $integrante->save(false);
            $mensaje->contestado=1;
            $mensaje->save(false);
            $transaction->commit();
            user()->setFlash("success","Se ha rechazado correctamente la membres&iacute;a al proyecto en postulaci&oacute;n");
        }catch (Exception $e) {
            Yii::log("ERROR AL RECHAZAR SOLICIUD, USER ID {$integrante->user_id}, PROYECTO ID: {$integrante->proyecto_id}
             ERROR: {$e->getMessage()}");
            $transaction->rollback();
            user()->setFlash("error","Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los
            problemas persisten por favor contactar con el administrador.");
        }
        $this->redirect(array('/buzon/inbox/index'));
    }

    /**
     * Accion para eliminar un integrante del proyecto. Se debe tener en cuenta que cuando
     * el estado del integrante es 1 entonces tambien se debe eliminar el LookupProyecto
     * de dicho usuario y revocarle el permiso del AuthManager.
     *
     * @param int $id ID del proyecto
     * @param int $integrante ID del integrante a eliminar
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   11-02-2013
     */
    public function actionEliminarIntegrante($id,$integrante){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=IntegranteProyecto::load($integrante,$proyecto->id);
        $transaction = app()->db->beginTransaction();
        try {
            if($model->estado==1){
                $keyArray['nombre_item'] = 'integrante_proyecto';
                $keyArray['proyecto_id'] = $id;
                $keyArray['user_id'] = $model->user_id;
                $lok = LookupProyecto::model()->findByPk($keyArray);
                if($lok!=null) $lok->delete();
            }
            $model->delete();
            $transaction->commit();
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente el integrante del proyecto.','success'=>true);
            $this->renderJSON($return);
        }catch (Exception $e) {
            $transaction->rollback();
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el integrante del proyecto. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para ver/agregar y eliminar referentes de un proyecto. Para ejecutar la accion
     * se deben poseer los permisos necesarios.
     *
     * @param int $id ID del proyecto donde se desea hacer crud de referentes
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   08-02-2013
     */
    public function actionReferentesProyecto($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=new ReferentesProyecto;
        $this->render('referentes_proyecto',array(
            'proyecto'=>$proyecto,
            'referentes'=>$proyecto->referentes,
            'model'=>$model
        ));
    }

    /**
     * Accion que permite agregar a un referente a un proyecto en postulacion. Para ejecutar la accion
     * se necesita contar con los permisos correspondientes. La data del referente se envia como un
     * string JSON hacia la accion.
     *
     * @param int $id ID del proyecto al que se desea agregar un referente
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   09-02-2013
     */
    public function actionAgregarReferente($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id,true);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $model=new ReferentesProyecto;
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            if($model->save()){
                $return=$model->convertToArray();
                $return['mensaje']='Se ha agregado correctamente al referente al proyecto.';
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al agregar el referente al proyecto:');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion para eliminar un referente de un proyecto en particular. Para ejecutar la accion se deben
     * tener los permisos necesarios. Se retorna un string json con los mensajes ya sea de exito o error
     * segun corresponda.
     *
     * @param int $id ID del proyecto donde se desea eliminar el referente
     * @param int $ref ID del referente a eliminar
     *
     * @return  string JSON con mensajes de exito/error segun corresponda
     * @throws  \CHttpException
     *
     * @since   09-02-2013
     */
    public function actionEliminarReferente($id,$ref){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=ReferentesProyecto::load($ref,$id);
        if($model->delete()){
            $return=array('mensaje'=>'Se ha eliminado correctamente al referente del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar al referente del proyecto. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para desplegar la carta gantt de un proyecto en postulacion. Ademas, permite
     * agregar y eliminar actividades (no hitos) de la carta gantt. Para ejecutar la acccion
     * se deben tener los permisos correspondientes.
     *
     * @param int $id ID del proyecto en postulacion
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   13-02-2013
     */
    public function actionCartaGantt($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $hitos=HitoProyecto::model()->findAll('id_proyecto=:ip order by fecha ASC,es_hito DESC',array(':ip'=>$proyecto->id));
        $model=new HitoProyecto;
            $this->render('carta_gantt',array(
           'hitos'=>$hitos,
           'proyecto'=>$proyecto,
           'model'=>$model
        ));
    }

    /**
     * Accion para agregar una actividad en la carta gantt a un proyecto en postulacion.
     * La respuesta es un string JSON con el objeto ingresado o con mensaje de error segun
     * sea el caso.
     *
     * @param int $id ID del proyecto en postulacion
     *
     * @return  string JSON con la respuesta de la operacion
     * @throws  \CHttpException
     *
     * @since   13-02-2013
     */
    public function actionAgregarActividad($id){
        $this->verificarEstadoProceso();
        if(Yii::app()->request->isPostRequest)
        {
            $proyecto=Proyecto::loadPostulacion($id,true);
            if(!$proyecto->checkPermisoPostulacion(user()->id))
                throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
            $model=new HitoProyecto;
            $model->setScenario('agregarActividad');
            $data = file_get_contents("php://input");
            $objData = json_decode($data,true);
            $model->attributes=$objData;
            $model->id_proyecto=$proyecto->id;
            $model->es_hito=0;
            if(count($objData['meses'])!=0 && $model->save()){
                //se agregan los meses
                foreach ($objData['meses'] as $mes) {
                    if(isset($mes['status'],$mes['id']) && $mes['status']==true && is_numeric($mes['id']) &&  (0<=$mes['id']) && ($mes['id']<=13)){
                        $newMonth=new MesHito;
                        $newMonth->id_hito=$model->primaryKey;
                        $newMonth->mes=$mes['id'];
                        $newMonth->save(false);
                    }
                }
                //se envia nuevo modelo junto con mensaje de exito
                $return=array('mensaje'=>'Se ha agregado correctamente el hito al proyecto.');
                $return['nombre']=$model->nombre;
                $return['id']=$model->primaryKey;
                $return['fecha']=$model->fecha;
                $return['es_hito']=$model->es_hito;
                $return['meses']=$model->getMeses();
                $this->renderJSON($return);
            }else{
                //crear un array con errores
                $return=array('mensaje'=>'Ha ocurrido un error al agregar el hito (verifica que has seleccionado algun mes):');
                foreach($model->errors as $error)
                    $return['errores'][]=$error[0];
                $this->renderJSON($return);
            }
        }
        else
            throw new CHttpException(400,'Petici&oacute;n inv&aacute;lida. La acci&oacute;n ser&aacute; notificada
            al administrador.');
    }

    /**
     * Accion para eliminar una actividad de la carta gantt de un proyecto postulacion. Solo se permite
     * eliminar actividades (que no sean hitos). Se retorna un string JSON con un mensaje
     * de exito/error segun sea necesario. Para ejecutar la accion se deben poseer los permisos
     * correspondientes.
     *
     * @param int $id ID del proyecto en postulacion
     * @param int $act ID de la actividad a eliminar
     *
     * @return  string JSON con mensaje de exito/error segun sea necesario
     * @throws  \CHttpException
     *
     * @since   13-02-2013
     */
    public function actionEliminarActividad($id,$act){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=HitoProyecto::load($act,$id);
        if($model->es_hito!=1 && $model->delete()){
            //se envia nuevo modelo junto con mensaje de exito
            $return=array('mensaje'=>'Se ha eliminado correctamente la actividad del proyecto.','success'=>true);
            $this->renderJSON($return);
        }else{
            //crear un array con errores
            $return=array('mensaje'=>'Ha ocurrido un error al eliminar el hito. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
            $this->renderJSON($return);
        }
    }

    /**
     * Accion para listar y agregar anexos a un proyecto en postulacion. Para ejecutar la accion
     * se deben tener los permisos correspondientes.
     *
     * @param int $id ID del proyecto en postulacion
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   20-02-2013
     */
    public function actionAnexosProyecto($id){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $anexo=new XUploadForm;
        $model=new AnexoProyecto;

        if(isset($_POST['AnexoProyecto'])){
            $model->attributes=$_POST['AnexoProyecto'];
            if($model->validate()){
                $transaction=app()->db->beginTransaction();
                try {
                    $data=$this->addFiles($proyecto->id);
                    $model->filename=$data[0];
                    $model->original_name=$data[1];
                    $model->id_proyecto=$proyecto->id;
                    $model->save(false);
                    $transaction->commit();
                    user()->setFlash("success","Se ha agregado correctamente el anexo.");
                    $this->refresh();
                }catch(CException $e) {
                    $transaction->rollback();
                    user()->setFlash('error','Ha ocurrido un error: '.$e->getMessage());
                }
            }else{
                user()->setFlash("error","Verifica que has ingresado un nombre para tu anexo y que se ha subido el
                archivo correctamente.");
            }
        }

        $this->render('anexos',array(
           'anexoFile'=>$anexo,
           'model'=>$model,
           'proyecto'=>$proyecto
        ));
    }

    /**
     * Accion para eliminar un anexo de un proyecto en particular. Para ejecutar la accion se deben
     * tener los permisos necesarios. Se retorna un string json con los mensajes ya sea de exito o error
     * segun corresponda.
     *
     * @param int $id ID del proyecto donde se desea eliminar el anexo
     * @param int $ref ID del anexo a eliminar
     *
     * @return  string JSON con mensajes de exito/error segun corresponda
     * @throws  \CHttpException
     *
     * @since   01-03-2013
     */
    public function actionEliminarAnexo($id,$ref){
        $this->verificarEstadoProceso();
        $proyecto=Proyecto::loadPostulacion($id,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $model=AnexoProyecto::load($ref,$id);
        if($model->delete())
            user()->setFlash("success",'Se ha eliminado correctamente el anexo del proyecto.');
        else
            user()->setFlash("error",'Ha ocurrido un error al eliminar el anexo del proyecto. Por favor intentalo nuevamente.
            Si los problemas persiste, contacta al administrador.');
        $this->redirect(array('/formularios/postulacionFondosIDI/anexosProyecto','id'=>$id));
    }

    /**
     * Accion para subir archivos para los presupuestos. Es el mismos sistema de subida que
     * para el sistema existente en OperacionesController. Hace uso del XUpload widget.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   20-02-2013
     */
    public function actionUpload(){
        $path = app( )->getBasePath( )."/../../piea-files/postulaciones/tmp/";
        header( 'Vary: Accept' );
        if( isset( $_SERVER['HTTP_ACCEPT'] )
            && (strpos( $_SERVER['HTTP_ACCEPT'], 'application/json' ) !== false) ) {
            header( 'Content-type: application/json' );
        } else {
            header( 'Content-type: text/plain' );
        }

        //Here we check if we are deleting and uploaded file
        if( isset( $_GET["_method"] ) ) {
            if( $_GET["_method"] == "delete" ) {
                if( $_GET["file"][0] !== '.' ) {
                    $file = $path.$_GET["file"];
                    if( is_file( $file ) ) {
                        unlink( $file );
                    }
                }
                echo json_encode( true );
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance( $model, 'file' );
            if( $model->file !== null ) {
                $model->mime_type = $model->file->getType( );
                $model->size = $model->file->getSize( );
                $model->name = $model->file->getName( );
                $filename = md5(microtime( ).$model->name);
                $filename .= ".".$model->file->getExtensionName( );
                if( $model->validate( ) ) {
                    $model->file->saveAs( $path.$filename );
                    chmod( $path.$filename, 0777 );
                    //Now we need to save this path to the user's session
                    if( Yii::app()->user->hasState('anexo') ) {
                        $userImages = Yii::app( )->user->getState('anexo');
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $path.$filename,
                        "filename" => $filename,
                        "originalName"=>$model->name,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('anexo', $userImages );
                    // json structure: https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode( array( array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "delete_url" => $this->createUrl( "upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        ) ),
                        "delete_type" => "POST"
                    ) ) );
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(array("error" => $model->getErrors('file'))));
                    Yii::log( "XUploadAction: ".CVarDumper::dumpAsString($model->getErrors()),
                        CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction");
                }
            } else {
                throw new CHttpException(500,"No se ha podido subir el archivo. Por favor, intentelo nuevamente.");
            }
        }
    }

    /**
     * Accion que permite obtener un anexo de un proyecto (archivo fisico).
     *
     * @param int $p ID del proyecto
     * @param int $id ID del anexo
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   21-02-2013
     */
    public function actionSendAnexo($p,$id){
        $anexo=AnexoProyecto::load($id,$p);
        $proyecto=Proyecto::load($p,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id))
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $path = app()->getBasePath()."/../../piea-files/postulaciones/{$p}/".$anexo->filename;
        //se obtiene la extension original del archivo
        $extension=explode(".",$anexo->filename);
        $extension=$extension[count($extension)-1];
        //le quito la extension al nombre original del anexo
        $antiguo=explode(".",$anexo->original_name);
        array_pop($antiguo);
        $antiguo=implode(".",$antiguo);
        $myfile = Yii::app()->file->set($path);
        $fakeName="Proyecto_{$p}_anexo_{$id}_{$antiguo}.{$extension}";
        $myfile->download($fakeName);
    }

    /**
     * Accion que permite enviar un proyecto en postulacion a revision. Una vez que el
     * proyecto es enviado entonces ya no es factible de edicion en ninguna de sus
     * secciones. Para enviar un proyecto se deben tener los permisos necesarios. Para
     * evitar ataques, entonces se obtiene el proyecto en postulacion del usuario que
     * ejecuta la accion de manera automatica, no es necesario pasar una ID a la accion.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   03-03-2013
     */
    public function actionEnviarPostulacion(){
        $this->verificarEstadoProceso();
        $perfil=user()->proyectoPostulacion();
        if($perfil==null)
            throw new CHttpException(500,"No tienes proyectos en postulaci&oacute;n para enviar.");
        $proyecto=Proyecto::loadPostulacion($perfil->id_proyecto,true);
        if(!$proyecto->checkPermisoPostulacion(user()->id)) //paranoic mode
            throw new CHttpException(500,"No tienes los permisos para ver esta p&aacute;gina.");
        $proyecto->estado_postulacion=1;
        $perfil->estado_postulacion=1; //estado enviado
        $transaction=app()->db->beginTransaction();
        try {
            $proyecto->save(false);
            $perfil->save(false);
            //se debe crear para los usuarios de este proyecto los permisos necesarios para que puedan ver las
            //partes de los proyectos:
            foreach ($proyecto->integrantes as $integrante)
                IntegranteProyecto::agregarIntegrante($integrante->user_id,$proyecto->id,false);
            //se envia la notificacion al usuario que envio el proyecto de que este ha sido enviado para revision
            $entidad=new Entidad($proyecto->id,1);
            $this->onSendUserNotificacion(new CEvent($this,array('tipo'=>'envio_proyecto_postulacion',
                'proyecto'=>$proyecto,'perfil'=>$perfil,'destinatario'=>user()->id,'entidad'=>$entidad)));
            $transaction->commit();
            user()->setFlash("success", "Se ha enviado correctamente el proyecto a revisi&oacute;n. Ser&aacute;s
            notificado de cualquier decisi&oacute;n sobre tu proyecto.");
        }catch (CException $e){
            $transaction->rollback();
            user()->setFlash('error', 'Ha ocurrido un error al enviar el proyecto a postulaci&oacute;n. Por favor
            int&eacute;ntalo nuevamente. Si los errores persisten, contacta al administrador.');
            Yii::log("Error al enviar proyecto ha postulacion. ID Proyecto {$proyecto->id}; ERROR: {$e->getTraceAsString()}",
                CLogger::LEVEL_ERROR,"application.modules.formularios.postulacionFondosIDI.enviarPostulacion");
        }
        $this->redirect(array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id));
    }

    /**
     * Funcion para mover los archivos de anexo subidos a su ubicacion final. Corresponde al
     * mismo sistema que en OperacionesController.
     *
     * @param int $proyecto_id ID del proyecto al cual se le esta guardando el anexo
     *
     * @return  string Nombre del archivo agregado
     * @throws  \CException
     *
     * @since   21-02-2013
     */
    private function addFiles($proyecto_id) {
        if(user()->hasState('anexo') ) {
            $anexos = user()->getState('anexo');
            $path = app( )->getBasePath( )."/../../piea-files/postulaciones/".$proyecto_id."/";
            if( !is_dir( $path ) ) {
                mkdir($path,0777,true);
            }
            foreach($anexos as $anexo ) {
                if( is_file( $anexo["path"] ) ) {
                    if( rename( $anexo["path"], $path.$anexo["filename"] ) ) {
                        Yii::log( $anexo["path"]." agregado correctamente", CLogger::LEVEL_INFO );
                    }
                } else {
                    Yii::log($anexo["path"]." is not a file", CLogger::LEVEL_WARNING );
                    user()->setState('anexo',null);
                    throw new CException("El archivo no pudo ser subido. Por favor, int&eacute;ntelo nuevamente");
                }
            }
            //Clear the user's session
            user()->setState('anexo',null);
            return array($anexo['filename'],$anexo['originalName']);
        }else
            throw new CException("Se ha intentando agregar anexo sin archivo adjunto.");
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // access control filter
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',  // denegar a todos los usuarios
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Evento que es lanzado para enviar una notificacion al usuario que se esta
     * agregando al equipo de trabajo de un proyecto.
     *
     * @param CEvent $event Evento a lanzar
     *
     * @return  void
     *
     * @since   10-03-2013
     */
    public function onSendUserNotificacion($event){
        $this->raiseEvent('onSendUserNotificacion',$event);
    }

    /**
     * Permite verificar si es que el proceso actual del formulario de fondos I+D+i aun permite que s
     * se inscriban o editen postulaciones.
     *
     * @param bool true si es que se desea lanzar una exepcion si es que el formulario esta cerrado, false caso contrario.
     *
     * @return  bool true si es que aun se permiten
     * @throws  \CHttpException
     *
     * @since   17-03-2013
     */
    private function verificarEstadoProceso($throw_ex=true){
        $proceso=FormularioPiea::procesoFormularioFondosIDI();
        if($proceso->formulario->estado!=1){
            if($throw_ex) throw new CHttpException(500,"El proceso de postulaciones ha finalizado.");
            else return false;
        }
        return true;
    }
}
