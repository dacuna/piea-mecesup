<?php

/**
 * Controlador para manejas los formularios del sistema, contiene las acciones generales
 * para manejar los formularios.
 *
 * El proceso para cada formulario es demasiado especifico para ser manejado por una unica
 * funcion, por lo que se han creado funciones especiales para cada tipo de formulario. Hasta
 * el momento los formularios a implementar son:
 *  - Formulario de fondos I+D+i: implementado
 *  - FAE: implementado
 *  - Fondos de viaje: por implementar
 * Cada formulario tiene asociado un proceso de formulario (/ProcesoFormulario) el cual mantiene
 * informacion sobre el estado del formulario.
 *
 * @category  Controller
 * @package   app.modules.formularios.controllers
 * @version   1.0
 * @since     2013-05-13
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AdministracionFondosController extends Controller
{

	/**
	 * Accion para ver el estado y informacion adicional del formulario de fondos I+D+i. El nombre de la
     * funcion es legacy, deberia ser cambiado en una proxima refactorizacion.
     *
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionVerFormulario($id)
	{
        $model=$this->loadModel($id);
        $nuevoProceso=new ProcesoFormulario;
        if(isset($_POST['ProcesoFormulario'])){
            $nuevoProceso->attributes=$_POST['ProcesoFormulario'];
            $nuevoProceso->id_formulario=$id;
            if($nuevoProceso->save()){
                $model->estado=1;
                $model->save(false);
                user()->setFlash("success","Se ha iniciado un nuevo proceso correctamente.");
                $this->refresh();
            }
        }

		$this->render('verFormulario',array(
			'model'=>$model,
            'procesos'=>$model->procesos,
            'nuevoProceso'=>$nuevoProceso,
            'procesoActual'=>$model->procesoActual
		));
	}

    /**
     * Accion para ver el estado y modificar el proceso del formulario FAE. Por ahora solo el administrador
     * puede realizar acciones en este formulario.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   13-05-2013
     */
    public function actionVerFAE(){
        $formulario=FormularioPiea::formularioFAE();
        $proceso=$formulario->procesoFAE();

        $nuevoProceso=new ProcesoFormulario;
        if($proceso==null && isset($_POST['ProcesoFormulario'])){
            $nuevoProceso->attributes=$_POST['ProcesoFormulario'];
            $nuevoProceso->id_formulario=$formulario->id;
            if($nuevoProceso->save()){
                $formulario->estado=1;
                $formulario->save(false);
                user()->setFlash("success","Se ha iniciado un nuevo proceso correctamente.");
                $this->refresh();
            }
        }

        $this->render('ver_fae',array(
            'formulario'=>$formulario,
            'procesos'=>$formulario->procesos,
            'nuevoProceso'=>$nuevoProceso,
            'procesoActual'=>$proceso
        ));
    }

	/**
	 * Crear un nuevo formularo de postulacion a fondos
	 * Si la creacion es exitosa, el usuario sera redirigido a la vista ver formulario del
     * formulario que acaba de crear.
	 */
	public function actionCrearFormulario()
	{
		$model=new FormularioPiea;

		$this->performAjaxValidation($model);

		if(isset($_POST['FormularioPiea']))
		{
			$model->attributes=$_POST['FormularioPiea'];
			if($model->save())
				$this->redirect(array('verFormulario','id'=>$model->id));
		}

		$this->render('crearFormulario',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['FormularioPiea']))
		{
			$model->attributes=$_POST['FormularioPiea'];
			if($model->save()){
                if($model->nombre=='fae')
				    $this->redirect(array('verFAE'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FormularioPiea');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FormularioPiea('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FormularioPiea']))
			$model->attributes=$_GET['FormularioPiea'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
     *
	 * @param integer the ID of the model to be loaded
     *
     * @return FormularioPiea modelo encontrado.
	 * @throws \CHttpException
	 */
	public function loadModel($id)
	{
		$model=FormularioPiea::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La p&aacute;gina solicitada no existe.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='formulario-piea-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('crearFormulario','update','admin','delete'),
                'roles'=>array('administrador'),
            ),
            array('allow',
                'actions'=>array('verFormulario','verFAE','index'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
}
