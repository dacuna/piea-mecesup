<?php
$this->breadcrumbs=array(
    'Administraci&oacute;n Fondos I+D+i'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Administraci&oacute;n Fondos I+D+i</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <h5>Proyectos Postulantes</h5>
        <div class="panel">
            <p>Los proyectos listados a continuaci&oacute;n corresponden a los proyectos que han postulado en el proceso
            actual del formulario de fondos I+D+i.</p>
        </div>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'formulario-piea-grid',
            'dataProvider'=>$perfiles,
            'columns'=>array(
                'proyecto.id',
                'proyecto.nombre',
                'postulante.nombrePresentacion',
                'postulante.email',
                array(
                    'class'=>'CLinkColumn',
                    'label'=>'Ver PDF',
                    'header'=>'Acci&oacute;n',
                    'urlExpression'=>'array("/formularios/postulacionFondosIDI/generarPdfProyecto", "id"=>$data->id_proyecto)',
                ),
                array(
                    'class'=>'CLinkColumn',
                    'label'=>'Descargar Anexos',
                    'header'=>'Acci&oacute;n',
                    'urlExpression'=>'array("/formularios/postulacionFondosIDI/generarZipAnexos", "id"=>$data->id_proyecto)',
                ),
            ),
        )); ?>

    </li>
</ul>
