<?php
$this->breadcrumbs=array(
    'Asignar proyectos a ayudantes de hitos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Asignar proyectos a ayudantes de hitos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">
        <h5>Asignar proyectos a ayudantes de hitos</h5>
        <div class="panel">
            <p>Estas asignando permisos al ayudante:
                <br><br>
                <strong><?php echo $usuario->nombreCompleto;?></strong>
            </p>
        </div>

        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Proyecto</th>
                    <th>Campus</th>
                    <th>Iniciativa</th>
                    <th>Estado</th>
                    <th>Acci&oacute;n</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($proyectos as $proyecto):?>
                <tr>
                    <td><?php echo $proyecto->id_proyecto;?></td>
                    <td><?php echo $proyecto->proyecto->nombre;?></td>
                    <td><?php echo $proyecto->proyecto->campus;?></td>
                    <td><?php echo $proyecto->proyecto->iniciativa->nombre_abreviado;?></td>
                    <td>
                        <?php if(in_array($proyecto->id_proyecto,$asignados)):?>
                            Asignado
                        <?php else:?>
                            No asignado
                        <?php endif;?>
                    </td>
                    <td>
                        <?php if(in_array($proyecto->id_proyecto,$asignados)):?>
                            <a href="<?php echo url('/formularios/administracionFondoIDI/accionProyectosAyudanteHitos',
                            array('ayudante'=>$usuario->id,'proyecto'=>$proyecto->id_proyecto,'accion'=>2));?>">
                                Remover asignaci&oacute;n</a>
                        <?php else:?>
                            <a href="<?php echo url('/formularios/administracionFondoIDI/accionProyectosAyudanteHitos',
                                array('ayudante'=>$usuario->id,'proyecto'=>$proyecto->id_proyecto));?>">Asignar</a>
                        <?php endif;?>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </li>
</ul>
