<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Observar proyectos'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Observar proyectos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>Los siguientes son los proyectos que puedes observar para el formulario de fondos I+D+i. Este listado
                corresponde a todos los proyectos que enviaron su postulaci&oacute;n a los fondos I+D+i.
            </p>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'proyecto-grid',
                'dataProvider'=>$proyectos,
                'columns'=>array(
                    'id',
                    'nombre',
                    'perfilPostulacion.postulante.nombreCompleto',
                    array(
                        'class'=>'CLinkColumn',
                        'label'=>'Ir a observaci&oacute;n',
                        'header'=>'Acci&oacute;n',
                        'urlExpression'=>'array("/formularios/revisionFondosIDI/observarProyecto", "id"=>$data->id)',
                    ),
                ),
            )); ?>

        </div>



    </li>

</ul>