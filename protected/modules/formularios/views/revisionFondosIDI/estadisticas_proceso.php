<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Estad&iacute;sticas del proceso'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Estad&iacute;sticas del proceso</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>A continuaci&oacute;n se presenta una tabla con las estad&iacute;sticas m&aacute;s relevantes del proceso
                de postulaci&ioacute;n al formulario de fondos I+D+i llevado a cabo recientemente.
            </p>

            <table>
                <thead>
                    <tr>
                        <th>Estad&iacute;stico</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Nota promedio</td>
                        <td><?php echo $estadistica->nota_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota</td>
                        <td><?php echo $estadistica->nota_sd;?></td>
                    </tr>
                    <tr>
                        <td>Nota m&iacute;nima</td>
                        <td><?php echo $estadistica->nota_minima;?></td>
                    </tr>
                    <tr>
                        <td>Nota m&aacute;xima</td>
                        <td><?php echo $estadistica->nota_maxima;?></td>
                    </tr>
                    <tr>
                        <td>Nota forma promedio</td>
                        <td><?php echo $estadistica->forma_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota forma</td>
                        <td><?php echo $estadistica->forma_sd;?></td>
                    </tr>
                    <tr>
                        <td>Nota fondo promedio</td>
                        <td><?php echo $estadistica->fondo_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota fondo</td>
                        <td><?php echo $estadistica->fondo_sd;?></td>
                    </tr>
                    <tr>
                        <td>Nota planificaci&oacute;n promedio</td>
                        <td><?php echo $estadistica->planificacion_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota planificaci&oacute;n</td>
                        <td><?php echo $estadistica->planificacion_sd;?></td>
                    </tr>
                    <tr>
                        <td>Nota forma promedio</td>
                        <td><?php echo $estadistica->forma_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota forma</td>
                        <td><?php echo $estadistica->forma_sd;?></td>
                    </tr>
                    <tr>
                        <td>Nota forma impacto</td>
                        <td><?php echo $estadistica->impacto_promedio;?></td>
                    </tr>
                    <tr>
                        <td>Desviaci&oacute;n est&aacute;ndar nota impacto</td>
                        <td><?php echo $estadistica->impacto_sd;?></td>
                    </tr>
                </tbody>
            </table>

        </div>

    </li>

</ul>