<?php
$this->breadcrumbs=array(
    'Revisi&oacute;n Fondos I+D+i'=>array('/formularios/revisionFondosIDI/verProyectosARevisar'),
    $proyecto->nombre
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Revisi&oacute;n Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app="budgetModule" ng-controller="BudgetController">
    <li class="active" id="crear-cuenta" style="padding-left:10px;">

        <p>Estas viendo la revisi&oacute;n del proyecto <strong><?php echo $proyecto->nombre;?></strong></p>

        <div class="panel">
            <h5>Perfil Proyecto</h5>

            <p>
                <strong>Nombre del proyecto: </strong><?php echo $proyecto->nombre;?><br>
                <strong>Iniciativa a la que pertenece: </strong><?php echo $proyecto->iniciativaPostulacion;?><br>
                <strong>Campus/Sede: </strong><?php echo $proyecto->campus;?>
            </p>


            <div style="border: 1px #ccc dashed;">
                <b>Extracto:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->extracto;?>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $revision->extracto_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->extracto_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->extracto_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->extracto_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Resumen:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->resumen;?>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $revision->resumen_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->resumen_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->resumen_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->resumen_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Objetivos:</b>
                <div class="row">

                    <div class="five columns">
                        <strong>Objetivo general:</strong> <?php echo $proyecto->perfilPostulacion->objetivos_generales;?>
                        <br>
                        <strong>Objetivos espec&iacute;ficos:</strong>
                        <ul>
                            <?php foreach($proyecto->objetivosEspecificos as $objetivo):?>
                                <li>- <?php echo $objetivo->descripcion;?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $revision->objetivos_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->objetivos_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->objetivos_redaccion;?>
                        <br><br>
                        Factibilidad (nota 0-100): <?php echo $revision->objetivos_factibilidad;?><br>
                        Alcanzabilidad (nota 0-100): <?php echo $revision->objetivos_alcanzabilidad;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->objetivos_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Estado del arte:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->estado_del_arte;?>
                    </div>

                    <div class="seven columns">
                        Pertinencia del estado del arte (nota 0-100): <?php echo $revision->estado_del_arte_pertinencia;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->estado_del_arte_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->estado_del_arte_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->estado_del_arte_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->estado_del_arte_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Experiencia previa:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->experiencia_previa;?>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $revision->experiencia_previa_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->experiencia_previa_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->experiencia_previa_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->experiencia_previa_comentario;?>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel">
            <h5>I+D+i</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Investigaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->investigacion;?>
                    </div>

                    <div class="seven columns">
                        Presencia de investigaci&oacute;n (nota 0-100): <?php echo $revision->investigacion_presencia;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->investigacion_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->investigacion_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->investigacion_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->investigacion_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Desarrollo:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->desarrollo;?>
                    </div>

                    <div class="seven columns">
                        Presencia de desarrollo (nota 0-100): <?php echo $revision->desarrollo_presencia;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->desarrollo_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->desarrollo_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->desarrollo_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->desarrollo_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Innovaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->innovacion;?>
                    </div>

                    <div class="seven columns">
                        Presencia de innovaci&oacute;n (nota 0-100): <?php echo $revision->innovacion_presencia;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->innovacion_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->innovacion_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->innovacion_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->innovacion_comentario;?>
                    </div>
                </div>
            </div>

        </div>

        <div class="panel">
            <h5>Impacto</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto Acad&eacute;mico:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_academico;?>
                    </div>

                    <div class="seven columns">
                        Nota impacto acad&eacute;mico (nota 0-100): <?php echo $revision->impacto_academico_nota;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->impacto_academico_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->impacto_academico_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->impacto_academico_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->impacto_academico_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto universitario/social:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_universitario_social;?>
                    </div>

                    <div class="seven columns">
                        Nota impacto universitario/social (nota 0-100): <?php echo $revision->impacto_universitario_social_nota;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->impacto_universitario_social_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->impacto_universitario_social_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->impacto_universitario_social_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->impacto_universitario_social_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Proyecciones:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->proyecciones;?>
                    </div>

                    <div class="seven columns">
                        Nota proyecciones (nota 0-100): <?php echo $revision->proyecciones_nota;?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $revision->proyecciones_ortografia;?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $revision->proyecciones_capacidad_sintesis;?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $revision->proyecciones_redaccion;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->proyecciones_comentario;?>
                    </div>
                </div>
            </div>

        </div>

        <div class="panel">
            <h5>Equipo de trabajo</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Integrantes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Nombre</th>
                                    <th style="text-align:center;">L&iacute;der</th>
                                    <th style="text-align:center;">Finanzas</th>
                                    <th style="text-align:center;">Responsabilidades</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->integrantes as $integrante):?>
                                <tr>
                                    <td>
                                        <?php echo $integrante->usuario->nombreCompleto;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esLider=="true"):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esFinanzas=="true"):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php echo $integrante->responsabilidades;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        Presencia de especialidades (nota 0-100): <?php echo $revision->equipo_trabajo_presencia_especialidades;?>
                        <br><br>
                        Asignaci&oacute;n de tareas (nota 0-100): <?php echo $revision->equipo_trabajo_asignacion_tareas;?>
                        <br><br>
                        Comentario:
                        <?php echo $revision->equipo_trabajo_comentario;?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Referentes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                            <tr>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Email</th>
                                <th style="text-align:center;">Tel&eacute;fono</th>
                                <th style="text-align:center;">Relaci&oacute;n</th>
                                <th style="text-align:center;">Instituci&oacute;n</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->referentes as $referente):?>
                                <tr>
                                    <td>
                                        <?php echo $referente->nombre;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->email;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->telefono;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->relacion_proyecto;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->institucion;?> / Cargo: <?php echo $referente->cargo;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        Nota referentes (nota 0-100): <?php echo $revision->referentes_nota;?>
                        <br><br>
                        Comentario:
                        <?php echo
                            $revision->referentes_comentario;?>
                    </div>
                </div>
            </div>

        </div>

        <div class="panel">
            <h5>Hitos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                            <th style="text-align:center;" width="70">Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($hitos as $hito):?>
                            <tr>
                                <td>
                                    <?php echo $hito->nombre;?>
                                </td>
                                <td>
                                    <?php echo $hito->descripcion;?>
                                </td>
                                <td>
                                    <?php echo $hito->fecha;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    Hitos realizables (nota 0-100): <?php echo $revision->hitos_realizables;?>
                    <br><br>
                    Ortograf&iacute;a:
                    <?php echo $revision->hitos_ortografia;?>
                    <br><br>
                    Capacidad de s&iacute;ntesis:
                    <br><?php echo $revision->hitos_capacidad_sintesis;?>
                    <br><br>
                    Redacci&oacute;n:
                    <?php echo $revision->hitos_redaccion;?>
                    <br><br>
                    Comentario:
                    <?php echo $revision->hitos_comentario;?>
                </div>
            </div>

        </div>

        <div class="panel">
            <h5>Carta Gantt</h5>
            <p>(Los items a evaluar se encuentran bajo la carta gantt)</p>
            <div class="row">

                <div class="twelve columns">
                    <table>
                        <thead>
                        <tr>
                            <th width="200" height="18" >Nombre</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                            <th width="30" height="18" >Jul</th>
                            <th width="30" height="18" >Ago</th>
                            <th width="30" height="18" >Sep</th>
                            <th width="30" height="18" >Oct</th>
                            <th width="30" height="18" >Nov</th>
                            <th width="30" height="18" >Dic</th>
                            <th width="30" height="18" >Ene</th>
                            <th width="30" height="18" >Feb</th>
                            <th width="30" height="18" >Mar</th>
                            <th width="30" height="18" >Abr</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($actividades as $actividad):?>
                            <tr>
                                <td width="200" height="18" >
                                    <?php echo $actividad->nombre;?>
                                </td>
                                <?php foreach ($actividad->meses as $mes):?>
                                    <td width="30" height="18" >
                                        <?php if($mes==1) echo "X";?>
                                    </td>
                                <?php endforeach;?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>

            <p>
                Tareas apropiadas (nota 0-100): <?php echo $revision->carta_gantt_tareas_apropiadas;?>
                <br><br>
                Tiempos apropiado (nota 0-100): <?php echo $revision->carta_gantt_tiempos_apropiados;?>
                <br><br>
                Ortograf&iacute;a:
                <?php echo $revision->carta_gantt_ortografia;?>
                <br><br>
                Capacidad de s&iacute;ntesis:
                <?php echo $revision->carta_gantt_capacidad_sintesis;?>
                <br><br>
                Redacci&oacute;n:
                <?php echo $revision->carta_gantt_redaccion;?>
                <br><br>
                Comentario:
                <?php echo $revision->carta_gantt_comentario;?>
            </p>

        </div>

        <div class="panel">
            <h5>Anexos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">ID</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($proyecto->anexos as $key=>$anexo):?>
                            <tr>
                                <td><?php echo $key+1;?></td>
                                <td>
                                    <a href="<?php echo url('/formularios/postulacionFondosIDI/sendAnexo',array('p'=>$proyecto->id,
                                        'id'=>$anexo->id));?>">
                                        <?php echo $anexo->nombre;?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    Pertinencia de los anexos:<br>
                    <?php echo $revision->anexos_pertinencia;?>
                    <br><br>
                    Comentario:
                    <?php echo $revision->anexos_comentario;?>
                </div>
            </div>

        </div>

            <h5>Presupuesto del proyecto</h5>

            <div class="row">

                <div class="twelve columns">
                    <table>
                    <thead>
                    <tr>
                        <th style="font-size: 13px;">Item</th>
                        <th style="font-size: 13px;">Precio</th>
                        <th style="font-size: 13px;">Cant.</th>
                        <th style="font-size: 13px;">Total</th>
                        <th style="font-size: 13px;">I+D+i</th>
                        <th style="font-size: 12px;text-align:center;">I+D+i Sugerido</th>
                        <th style="font-size: 13px;">Propio</th>
                        <th style="font-size: 13px;">Otros</th>
                        <th style="font-size: 12px;text-align:center;">Item apropiado</th>
                        <th style="font-size: 12px;text-align:center;">Monto adecuado</th>
                        <th style="font-size: 13px;text-align:center;">Comentario</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><b>Adquisiciones de implementaci&oacute;n</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money" ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsAdquisicion">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL (Adquisiciones de implementaci&oacute;n)</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(1))}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalIDI)}}</td>
                        <td class="money"></td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalOtros)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Materiales e insumos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsMateriales">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Materiales e insumos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(2))}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalIDI)}}</td>
                        <td class="money"></td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalOtros)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Gastos Operativos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsGastos">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Gastos operativos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(3))}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalIDI)}}</td>
                        <td class="money"></td>
                        <td class="money">{{formatCurrency(itemsGastos.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalOtros)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Otros items</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsOtros">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Otros items]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(4))}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalIDI)}}</td>
                        <td class="money"></td>
                        <td class="money">{{formatCurrency(itemsOtros.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalOtros)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td><b>TOTAL</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalBudget())}}</td>
                        <td class="money">{{formatCurrency(totalFinalIDI)}}</td>
                        <td class="money"></td>
                        <td class="money">{{formatCurrency(totalFinalPropios)}}</td>
                        <td class="money">{{formatCurrency(totalFinalOtros)}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                    </table>

                    Comentario:
                    <?php echo $revision->presupuesto_comentario;?>

                </div>

            </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">

    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var budgeteModule = angular.module('budgetModule', []);
    function BudgetController($scope, $http) {
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.itemsAdquisicion = [<?php foreach($presupuesto['items'][0] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->monto_idi_editado))?$item->getItemRevisado($revision->id_revisor)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota))?$item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->motivo_edicion))?"'".$item->getItemRevisado($revision->id_revisor)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado($revision->id_revisor)}},";}?>];
        $scope.itemsMateriales = [<?php foreach($presupuesto['items'][1] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->monto_idi_editado))?$item->getItemRevisado($revision->id_revisor)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota))?$item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->motivo_edicion))?"'".$item->getItemRevisado($revision->id_revisor)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado($revision->id_revisor)}},";}?>];
        $scope.itemsGastos = [<?php foreach($presupuesto['items'][2] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->monto_idi_editado))?$item->getItemRevisado($revision->id_revisor)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota))?$item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->motivo_edicion))?"'".$item->getItemRevisado($revision->id_revisor)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado($revision->id_revisor)}},";}?>];
        $scope.itemsOtros = [<?php foreach($presupuesto['items'][3] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->monto_idi_editado))?$item->getItemRevisado($revision->id_revisor)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota))?$item->getItemRevisado($revision->id_revisor)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado($revision->id_revisor)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado($revision->id_revisor) && isset($item->getItemRevisado($revision->id_revisor)->motivo_edicion))?"'".$item->getItemRevisado($revision->id_revisor)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado($revision->id_revisor)}},";}?>];
        $scope.totalFinalIDI=0;
        $scope.totalFinalOtros=0;
        $scope.totalFinalPropios=0;

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.formatCurrency=function(data){
            return accounting.formatMoney(data, "$", 0, ".", ",");
        }

        $scope.totalItem=function(id){
            if(id==1) {userArray=$scope.itemsAdquisicion;}
            else if(id==2) {userArray=$scope.itemsMateriales;}
            else if(id==3) {userArray=$scope.itemsGastos;}
            else if(id==4) {userArray=$scope.itemsOtros;}
            var total=0;
            var totalIDI=0;
            var totalPropios=0;
            var totalOtros=0;
            console.log(id);
            angular.forEach(userArray, function(data) {
                total += parseInt(data.precio)*parseInt(data.unidades);
                totalIDI += parseInt(data.idi);
                totalOtros+=parseInt(data.otros);
                totalPropios+=parseInt(data.propio);
            });
            userArray.totalFinal=total;
            userArray.totalIDI=totalIDI;
            userArray.totalOtros=totalOtros;
            userArray.totalPropios=totalPropios;
            return total;
        }

        $scope.totalBudget=function(){
            $scope.totalFinalIDI=$scope.itemsAdquisicion.totalIDI+$scope.itemsMateriales.totalIDI+$scope.itemsGastos.totalIDI+$scope.itemsOtros.totalIDI;
            $scope.totalFinalOtros=$scope.itemsAdquisicion.totalOtros+$scope.itemsMateriales.totalOtros+$scope.itemsGastos.totalOtros+$scope.itemsOtros.totalOtros;
            $scope.totalFinalPropios=$scope.itemsAdquisicion.totalPropios+$scope.itemsMateriales.totalPropios+$scope.itemsGastos.totalPropios+$scope.itemsOtros.totalPropios;
            return $scope.itemsAdquisicion.totalFinal+$scope.itemsMateriales.totalFinal+$scope.itemsGastos.totalFinal+$scope.itemsOtros.totalFinal;
        }

    }

</script>