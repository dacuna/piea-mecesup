<?php
$this->breadcrumbs=array(
    'Revisi&oacute;n Fondos I+D+i'=>array('/formularios/revisionFondosIDI/verProyectosARevisar'),
    $proyecto->nombre
);
?>
<style>
        /********************************************************************************************************************************************
     *
     * Portamento-related styles below
     *
     ********************************************************************************************************************************************/

    #leyenda {
        width:200px;
        padding:1em;
        background: #d8e8d8;
        position:absolute;
        right:0;
        top:0;
        z-index: 10000;
        font-size:10px;
    }

        /* Portamento styles */
    #portamento_container {
        position:absolute;
        right:-160px;
        top:0;
    }

    #portamento_container #leyenda{
        position:absolute;
    }

    #portamento_container #leyenda.fixed {
        position:fixed;
        right:auto;
        top:auto;
    }
</style>
<dl class="nice contained tabs">
    <dd><a href="#" class="active">Evaluar Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app="budgetModule" ng-controller="BudgetController">
    <li class="active" id="crear-cuenta" style="padding-left:10px;">

        <p>Estas evaluando el proyecto <strong><?php echo $proyecto->nombre;?></strong></p>


            <div id="leyenda" style="top: 0px;">
                <p>
                    <strong>LEYENDA:</strong><br>
                    <ul>
                        <li>
                            <strong>Ortograf&iacute;a</strong>: <br>
                            &nbsp;&nbsp;- Bueno: No presenta faltas de ortograf&iacute;a<br>
                            &nbsp;&nbsp;- Regular: Las faltas de ortograf&iacute;a son menores<br>
                            &nbsp;&nbsp;- Malo: Presenta faltas ortogr&aacute;ficas evidentes y reiteradas
                        </li>
                    <li>
                        <strong>Capacidad de s&iacute;ntesis</strong>: <br>
                        &nbsp;&nbsp;- Bueno: Expone de manera concreta y clara las ideas presentadas<br>
                        &nbsp;&nbsp;- Regular: La capacidad de s&iacute;nteis es aceptable<br>
                        &nbsp;&nbsp;- Malo: No presenta capacidad de s&iacute;ntesis
                    </li>
                    <li>
                        <strong>Redacci&oacute;n</strong>: <br>
                        &nbsp;&nbsp;- Bueno: Buen manejo del lenguaje y correcta exposici&oacute;n de ideas<br>
                        &nbsp;&nbsp;- Regular: Uso aceptable de puntuaci&oacute;n y exposici&oacute;n de ideas<br>
                        &nbsp;&nbsp;- Malo: No hay capacidad de redacci&oacute;n
                    </li>
                    </ul>
                </p>
            </div>

        <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
            'id'=>'formulario-piea-form','type'=>'nice')); ?>

        <?php echo $form->errorSummary(array($revision)); ?>

        <div class="panel">
            <h5>Perfil Proyecto</h5>

            <p>
                <strong>Nombre del proyecto: </strong><?php echo $proyecto->nombre;?><br>
                <strong>Iniciativa a la que pertenece: </strong><?php echo $proyecto->iniciativaPostulacion;?><br>
                <strong>Campus/Sede: </strong><?php echo $proyecto->campus;?>
            </p>


            <div style="border: 1px #ccc dashed;">
                <b>Extracto:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->extracto;?>
                    </div>

                    <div class="seven columns">
                                Ortograf&iacute;a:<br>
                                <?php echo $form->radioButtonList($revision,'extracto_ortografia',
                                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                                <br><br>
                                Capacidad de s&iacute;ntesis:<br>
                                <?php echo $form->radioButtonList($revision,'extracto_capacidad_sintesis',
                                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                                <br><br>
                                Redacci&oacute;n:<br>
                                <?php echo $form->radioButtonList($revision,'extracto_redaccion',
                                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'extracto_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Resumen:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->resumen;?>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'resumen_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'resumen_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'resumen_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'resumen_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Objetivos:</b>
                <div class="row">

                    <div class="five columns">
                        <strong>Objetivo general:</strong> <?php echo $proyecto->perfilPostulacion->objetivos_generales;?>
                        <br>
                        <strong>Objetivos espec&iacute;ficos:</strong>
                        <ul>
                            <?php foreach($proyecto->objetivosEspecificos as $objetivo):?>
                                <li>- <?php echo $objetivo->descripcion;?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'objetivos_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'objetivos_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'objetivos_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>

                        <p>
                            <strong>Factibilidad:</strong><br>
                            <strong>0 - 24</strong>: No presenta objetivos<br>
                            <strong>25 - 54</strong>: Presenta objetivos, pero son imposibles de realizar<br>
                            <strong>55 - 74</strong>: Presenta objetivos, pero dif&iacute;cilmente realizables<br>
                            <strong>75 - 100</strong>: Presenta objetivos realizables
                        </p>
                        Factibilidad (nota 0-100): <?php echo $form->textField($revision,'objetivos_factibilidad',array('size'=>3));?><br>

                        <p>
                            <strong>Alcanzabilidad:</strong><br>
                            <strong>0 &ndash; 24</strong>: Objetivos imposibles de alcanzar dentro de los plazos<br>
                            <strong>25 -54</strong>: Objetivos dif&iacute;ciles de alcanzar dentro de los plazos<br>
                            <strong>55 &ndash; 74</strong>: Objetivos con leve dificultad de ser alcanzados en los
                            plazos<br>
                            <strong>75 &ndash; 100</strong>: Objetivos f&aacute;cilmente alcanzables dentro de los plazos
                        </p>
                        Alcanzabilidad (nota 0-100): <?php echo $form->textField($revision,'objetivos_alcanzabilidad',array('size'=>3));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'objetivos_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>


            <br><br><input type="button" name="guardar1" value="Guardar" class="nice button enviar"><br><br>
        </div>

        <div class="panel">
            <h5>Antecedentes</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Estado del arte:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->estado_del_arte;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Pertinencia del estado del arte:</strong><br>
                            <strong>0 &ndash; 24</strong>: No presenta antecedentes acordes al proyecto
                            presentado<br>
                            <strong>25 -54</strong>: Presenta antecedentes pero son deficientes para justificar
                            el desarrollo del proyecto<br>
                            <strong>55 &ndash; 74</strong>: Presenta antecedentes que justifican de buena manera
                            pero no en su totalidad el desarrollo del proyecto<br>
                            <strong>75 &ndash; 100</strong>: Presenta antecedentes que justifican apropiadamente la
                            ejecuci&oacute;n del proyecto
                        </p>
                        Pertinencia del estado del arte (nota 0-100): <?php echo $form->textField($revision,'estado_del_arte_pertinencia',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'estado_del_arte_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'estado_del_arte_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'estado_del_arte_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'estado_del_arte_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Experiencia previa:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->experiencia_previa;?>
                    </div>

                    <div class="seven columns">
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'experiencia_previa_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'experiencia_previa_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'experiencia_previa_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'experiencia_previa_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar10" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>I+D+i</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Investigaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->investigacion;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Presencia de investigaci&oacute;n</strong>:<br>
                            <strong>0 &ndash; 24</strong>: No cuenta presencia de investigaci&oacute;n<br>
                            <strong>25 -54</strong>: Presencia de investigaci&oacute;n leve<br>
                            <strong>55 &ndash; 74</strong>: Presencia de investigaci&oacute;n moderada<br>
                            <strong>75 &ndash; 100</strong>: Presencia de investigaci&oacute;n fuerte
                        </p>
                        Presencia de investigaci&oacute;n (nota 0-100): <?php echo $form->textField($revision,'investigacion_presencia',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'investigacion_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'investigacion_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'investigacion_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'investigacion_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Desarrollo:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->desarrollo;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Presencia de desarrollo</strong>:<br>
                            <strong>0 &ndash; 24</strong>: No cuenta presencia de desarrollo<br>
                            <strong>25 -54</strong>: Presencia de desarrollo leve<br>
                            <strong>55 &ndash; 74</strong>: Presencia de desarrollo moderada<br>
                            <strong>75 &ndash; 100</strong>: Presencia de desarrollo fuerte
                        </p>
                        Presencia de desarrollo (nota 0-100): <?php echo $form->textField($revision,'desarrollo_presencia',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'desarrollo_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'desarrollo_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'desarrollo_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'desarrollo_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Innovaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->innovacion;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Presencia de innovaci&oacute;n</strong>:<br>
                            <strong>0 &ndash; 24</strong>: No cuenta presencia de innovaci&oacute;n<br>
                            <strong>25 -54</strong>: Presencia de innovaci&oacute;n leve<br>
                            <strong>55 &ndash; 74</strong>: Presencia de innovaci&oacute;n moderada<br>
                            <strong>75 &ndash; 100</strong>: Presencia de innovaci&oacute;n fuerte
                        </p>
                        Presencia de innovaci&oacute;n (nota 0-100): <?php echo $form->textField($revision,'innovacion_presencia',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'innovacion_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'innovacion_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'innovacion_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'innovacion_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar2" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>Impacto</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto Acad&eacute;mico:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_academico;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Impacto acad&eacute;mico</strong>:<br>
                            <strong>0 - 24</strong>: M&iacute;nimo impacto<br>
                            <strong>25 - 54</strong>: Bajo impacto<br>
                            <strong>55 - 74</strong>: Mediano impacto<br>
                            <strong>75 - 100</strong>: Alto impacto
                        </p>
                        Nota impacto acad&eacute;mico (nota 0-100): <?php echo $form->textField($revision,'impacto_academico_nota',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'impacto_academico_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'impacto_academico_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'impacto_academico_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'impacto_academico_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto universitario/social:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_universitario_social;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Impacto universitario/social</strong>:<br>
                            <strong>0 - 24</strong>: M&iacute;nimo impacto<br>
                            <strong>25 - 54</strong>: Bajo impacto<br>
                            <strong>55 - 74</strong>: Mediano impacto<br>
                            <strong>75 - 100</strong>: Alto impacto
                        </p>
                        Nota impacto universitario/social (nota 0-100): <?php echo $form->textField($revision,'impacto_universitario_social_nota',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'impacto_universitario_social_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'impacto_universitario_social_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'impacto_universitario_social_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'impacto_universitario_social_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Proyecciones:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->proyecciones;?>
                    </div>

                    <div class="seven columns">
                        <p>
                            <strong>Impacto universitario/social</strong>:<br>
                            <strong>0 - 24</strong>: Nula proyecci&oacute;n<br>
                            <strong>25 - 54</strong>: Baja proyecci&oacute;n<br>
                            <strong>55 - 74</strong>: Mediano proyecci&oacute;n<br>
                            <strong>75 - 100</strong>: Alta proyecci&oacute;n
                        </p>
                        Nota proyecciones (nota 0-100): <?php echo $form->textField($revision,'proyecciones_nota',array('size'=>3));?>
                        <br><br>
                        Ortograf&iacute;a:
                        <?php echo $form->radioButtonList($revision,'proyecciones_ortografia',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Capacidad de s&iacute;ntesis:
                        <?php echo $form->radioButtonList($revision,'proyecciones_capacidad_sintesis',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Redacci&oacute;n:
                        <?php echo $form->radioButtonList($revision,'proyecciones_redaccion',
                            array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                            array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'proyecciones_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar3" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel" id="wrapper">
            <h5>Hitos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                            <th style="text-align:center;" width="70">Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($hitos as $hito):?>
                            <tr>
                                <td>
                                    <?php echo $hito->nombre;?>
                                </td>
                                <td>
                                    <?php echo $hito->descripcion;?>
                                </td>
                                <td>
                                    <?php echo $hito->fecha;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    <p>
                        <strong>Hitos realizables</strong>:<br>
                        <strong>0 &ndash; 24</strong>: No presenta hitos realizables<br>
                        <strong>25 -54</strong>: Presenta hitos dif&iacute;cilmente realizables<br>
                        <strong>55 - 74</strong>: Presenta hitos ligeramente dif&iacute;ciles de realizar.<br>
                        <strong>75 &ndash;</strong>: 100 Presenta hitos que podr&aacute;n ser realizados acorde a los
                        tiempos de ejecuci&oacute;n del proyecto
                    </p>
                    Hitos realizables (nota 0-100): <?php echo $form->textField($revision,'hitos_realizables',array('size'=>3));?>
                    <br><br>
                    Ortograf&iacute;a:
                    <?php echo $form->radioButtonList($revision,'hitos_ortografia',
                        array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                        array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                    <br><br>
                    Capacidad de s&iacute;ntesis:
                    <br><?php echo $form->radioButtonList($revision,'hitos_capacidad_sintesis',
                        array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                        array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                    <br><br>
                    Redacci&oacute;n:
                    <?php echo $form->radioButtonList($revision,'hitos_redaccion',
                        array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                        array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                    <br><br>
                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'hitos_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                </div>
            </div>

            <br><br><input type="button" name="guardar5" value="Guardar" class="nice button enviar"><br><br>

        </div>


        <div class="panel">
            <h5>Equipo de trabajo</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Integrantes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Nombre</th>
                                    <th style="text-align:center;">L&iacute;der</th>
                                    <th style="text-align:center;">Finanzas</th>
                                    <th style="text-align:center;">Responsabilidades</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->integrantes as $integrante):?>
                                <tr>
                                    <td>
                                        <?php echo $integrante->usuario->nombreCompleto;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esLider=="true"):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esFinanzas=="true"):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php echo $integrante->responsabilidades;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        <p>
                            <strong>Pertinencia de especialidades</strong>:<br>
                            <strong>0 &ndash; 24</strong>: Las especialidades de los integrantes del equipo de
                            trabajo no son acordes a las necesidades del proyecto<br>
                                <strong>25 - 54</strong>: El equipo o la distribuci&oacute;n de responsabilidades no es
                            acorde a las necesidades del proyecto<br>
                                    <strong>55 - 74</strong>: El equipo y la distribuci&oacute;n de responsabilidades son
                            acordes pero no suficientes a las necesidades del proyecto<br>
                                        <strong>75 &ndash; 100</strong>: El equipo y la distribuci&oacute;n de responsabilidades facilitan
                            la correcta ejecuci&oacute;n del proyecto.
                        </p>
                        Presencia de especialidades (nota 0-100): <?php echo $form->textField($revision,'equipo_trabajo_presencia_especialidades',array('size'=>3));?>
                        <br><br>
                        <p>
                            <strong>Asignaci&oacute;n de tareas</strong>:<br>
                            <strong>0 &ndash; 24</strong>: Las tareas asignadas no tienen relaci&oacute;n con el logro de
                            objetivos del proyecto<br>
                                <strong>25 -54</strong>: Las tareas asignadas tienen alg&uacute;n grado de relaci&oacute;n pero
                            no son suficientes para el logro de objetivos del proyecto<br>
                                    <strong>55 &ndash; 74</strong>: Las tareas asignadas tienen relaci&oacute;n pero no son
                            suficientes para el logro de objetivos del proyecto<br>
                                        <strong>75 &ndash; 100</strong>: Las tareas asignadas contribuyen de manera apropiada
                            con el logro de objetivos del proyecto
                        </p>

                        Asignaci&oacute;n de tareas (nota 0-100): <?php echo $form->textField($revision,'equipo_trabajo_asignacion_tareas',array('size'=>3));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'equipo_trabajo_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Referentes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                            <tr>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Email</th>
                                <th style="text-align:center;">Tel&eacute;fono</th>
                                <th style="text-align:center;">Relaci&oacute;n</th>
                                <th style="text-align:center;">Instituci&oacute;n</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->referentes as $referente):?>
                                <tr>
                                    <td>
                                        <?php echo $referente->nombre;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->imprimirEmail();?>
                                    </td>
                                    <td>
                                        <?php echo $referente->telefono;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->relacion_proyecto;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->institucion;?> / Cargo: <?php echo $referente->cargo;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        <p>
                            <strong>Referentes:</strong><br>
                            <strong>0 &ndash; 24</strong>: No presenta referentes<br>
                            <strong>25 -54</strong>: Presenta referentes, pero no son en la l&iacute;nea del proyecto<br>
                            <strong>55 - 74</strong>: Presenta al menos 1 referente en la l&iacute;nea del proyecto<br>
                            <strong>75 &ndash; 100</strong>: Presenta m&aacute;s de 1 referente en la l&iacute;nea del proyecto
                        </p>
                        Nota referentes (nota 0-100): <?php echo $form->textField($revision,'referentes_nota',array('size'=>3));?>
                        <br><br>
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'referentes_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar4" value="Guardar" class="nice button enviar"><br><br>

        </div>


        <div class="panel">
            <h5>Anexos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">ID</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($proyecto->anexos as $key=>$anexo):?>
                            <tr>
                                <td><?php echo $key+1;?></td>
                                <td>
                                    <a href="<?php echo url('/formularios/postulacionFondosIDI/sendAnexo',array('p'=>$proyecto->id,
                                        'id'=>$anexo->id));?>">
                                        <?php echo $anexo->nombre;?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    <p><strong>Pertinencia de los anexos</strong>: <br>
                        &nbsp;&nbsp;- <b>Bueno</b>: Anexos apropiados y permiten entender a cabalidad la propuesta<br>
                        &nbsp;&nbsp;- <b>Regular</b>: Anexos &uacute;tiles pero no suficientes para justificar y entender la propuesta<br>
                        &nbsp;&nbsp;- <b>Malo</b>: Anexos confunden y no justifican apropiadamente la propuesta
                    </p>
                    Pertinencia de los anexos:<br>
                    <?php echo $form->radioButtonList($revision,'anexos_pertinencia',
                        array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                        array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                    <br><br>
                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'anexos_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                </div>
            </div>

            <br><br><input type="button" name="guardar8" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>Carta Gantt</h5>
            <p>(Los items a evaluar se encuentran bajo la carta gantt)</p>
            <div class="row">

                <div class="twelve columns">
                    <table>
                        <thead>
                        <tr>
                            <th width="200" height="18" >Nombre</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                            <th width="30" height="18" >Jul</th>
                            <th width="30" height="18" >Ago</th>
                            <th width="30" height="18" >Sep</th>
                            <th width="30" height="18" >Oct</th>
                            <th width="30" height="18" >Nov</th>
                            <th width="30" height="18" >Dic</th>
                            <th width="30" height="18" >Ene</th>
                            <th width="30" height="18" >Feb</th>
                            <th width="30" height="18" >Mar</th>
                            <th width="30" height="18" >Abr</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($actividades as $actividad):?>
                            <tr>
                                <td width="200" height="18" >
                                    <?php echo $actividad->nombre;?>
                                </td>
                                <?php foreach ($actividad->meses as $mes):?>
                                    <td width="30" height="18" >
                                        <?php if($mes==1) echo "X";?>
                                    </td>
                                <?php endforeach;?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>

            <p>
                <strong>Tareas apropiadas</strong>:<br>
                <strong>0 &ndash; 24</strong>: Las tareas no son acordes a los objetivos<br>
                <strong>25 -54</strong>: Las tareas son ligeramente acorde a los objetivos<br>
                <strong>55 - 74</strong>: Las tareas son medianamente acorde a los objetivos<br>
                <strong>75 &ndash; 100</strong>: Las tareas son altamente acorde a los objetivos<br>
                Tareas apropiadas (nota 0-100): <?php echo $form->textField($revision,'carta_gantt_tareas_apropiadas',array('size'=>3));?>
                <br><br>
                <strong>Tiempos apropiado</strong>:<br>
                <strong>0 &ndash; 24</strong>: Tareas imposibles de realizar<br>
                <strong>25 -54</strong>: Tareas dif&iacute;cilmente realizables<br>
                <strong>55 - 74</strong>: Tareas medianamente dif&iacute;cil de realizar<br>
                <strong>75 &ndash; 100</strong>: Tareas realizables dentro de los plazos estipulados por el
                equipo<br>
                Tiempos apropiado (nota 0-100): <?php echo $form->textField($revision,'carta_gantt_tiempos_apropiados',array('size'=>3));?>
                <br><br>
                Ortograf&iacute;a:
                <?php echo $form->radioButtonList($revision,'carta_gantt_ortografia',
                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                <br><br>
                Capacidad de s&iacute;ntesis:
                <?php echo $form->radioButtonList($revision,'carta_gantt_capacidad_sintesis',
                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                <br><br>
                Redacci&oacute;n:
                <?php echo $form->radioButtonList($revision,'carta_gantt_redaccion',
                    array('0'=>'Malo','55'=>'Regular','100'=>'Bueno'),
                    array('separator'=>' ','labelOptions'=>array('style'=>'display:inline;margin-right:10px;')));?>
                <br><br>
                Comentario:
                <?php $this->widget('application.extensions.redactorjs.Redactor',
                    array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'carta_gantt_comentario',
                        'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
            </p>

            <br><br><input type="button" name="guardar6" value="Guardar" class="nice button enviar"><br><br>

        </div>

            <h5>Presupuesto del proyecto</h5>
<p style="font-weight:bold;font-size:14px;">
INSTRUCCIONES: para cada item del presupuesto se deben completar 3 campos:
<ul style="font-weight:bold;font-size:14px;">
<li>- I+D+i sugerido: Para cada item que haga uso de un monto I+D+i (columna I+D+i) Ud. podr&aacute; sugerir un nuevo monto (siempre reduciendo el monto actual)  si es que lo desea. Recordar que el monto I+D+i es lo que PIEA financia de cada item.</li>
<li>- Item apropiado: corresponde a la nota de item de costos apropiados a las tareas (ver leyenda a continuaci&oacute;n)</li>
<li>- Item adecuado: corresponde a la nota de montos adecuados para la realizaci&oacute;n del item (ver leyenda a continuaci&oacute;n)</li>
</ul>
</p>
            <p>
                Los items a evaluar corresponden a:<br><br>
                <strong>&Iacute;tem de costos apropiado a las tareas (item apropiado)</strong>:<br>
                Se evaluara que los &iacute;tems de costos especificados en el presupuesto
                permitan desarrollar las tareas planteadas.<br>
                <strong>0 &ndash; 24</strong>: &Iacute;tem no presenta relaci&oacute;n con el proyecto<br>
                <strong>25 -54</strong>: &Iacute;tem no es fundamental para el proyecto<br>
                <strong>55 - 74</strong>: &Iacute;tem es de menor importancia para el proyecto<br>
                <strong>75 &ndash; 100</strong>: &Iacute;tem de alta importancia para el proyecto<br><br>

                <strong>Montos adecuados a la realizaci&oacute;n del &iacute;tem (monto adecuado)</strong>:<br>
                Se evaluara que los montos solicitados en el presupuesto sean adecuados
                con los &iacute;tems planteados y debidamente justificados.<br>
                <strong>0 &ndash; 24</strong>: Monto no se condice con el &iacute;tem<br>
                <strong>25 -54</strong>: Monto altamente desproporcionado del &iacute;tem<br>
                <strong>55 - 74</strong>: Monto ligeramente desproporcionado del &iacute;tem<br>
                <strong>75 &ndash; 100</strong>: Monto acorde al &iacute;tem
            </p>

<p style="font-size:14px;"><strong>Al ingresar la evaluaci&oacute;n para cada item, Ud. debe presionar en el enlace "Guardar" ubicado junto al item que se acaba de evaluar, as&iacute; la evaluaci&oacute;n para ese item
se registrar&aacute; autom&aacute;ticamente.</strong></p>

            <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
                {{errorMsg}}
                <ul>
                    <li ng-repeat="error in validationErrors">
                        - {{error}}
                    </li>
                </ul>
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="row">

                <div class="twelve columns">
                    <table>
                    <thead>
                    <tr>
                        <th style="font-size: 13px;">Item</th>
                        <th style="font-size: 13px;">Precio</th>
                        <th style="font-size: 13px;">Cant.</th>
                        <th style="font-size: 13px;">Total</th>
                        <th style="font-size: 13px;">I+D+i</th>
                        <th style="font-size: 13px;">Propio</th>
                        <th style="font-size: 13px;">Otros</th>
                        <th style="background-color:rgb(147, 235, 147);font-size: 12px;text-align:center;">I+D+i Sugerido</th>
                        <th style="background-color:rgb(147, 235, 147);font-size: 12px;text-align:center;">Item apropiado</th>
                        <th style="background-color:rgb(147, 235, 147);font-size: 12px;text-align:center;">Monto adecuado</th>
                        <th style="background-color:rgb(147, 235, 147);font-size: 13px;text-align:center;">Comentario</th>
                        <th style="font-size: 13px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><b>Adquisiciones de implementaci&oacute;n</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsAdquisicion">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                        <td style="text-align:center;">
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL (Adquisiciones de implementaci&oacute;n)</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(1))}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Materiales e insumos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsMateriales">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                        <td>
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Materiales e insumos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(2))}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Gastos Operativos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsGastos">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                        <td>
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Gastos operativos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(3))}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Otros items</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsOtros">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.item_de_costo_apropiado_nota" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <input type="text" ng-model="item.montos_adecuados_a_la_realizacion_item" style="width:25px;">
                        </td>
                        <td style="background-color:rgb(147, 235, 147);text-align:center;">
                            <textarea rows="2" cols="3" style="width: 100px;" ng-model="item.motivo_edicion"></textarea>
                        </td>
                        <td>
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Otros items]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(4))}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td><b>TOTAL</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalBudget())}}</td>
                        <td class="money">{{formatCurrency(totalFinalIDI)}}</td>
                        <td class="money">{{formatCurrency(totalFinalPropios)}}</td>
                        <td class="money">{{formatCurrency(totalFinalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    </tbody>
                    </table>

                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'presupuesto_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>

                    <br><br><input type="button" name="guardar9" value="Guardar" class="nice button enviar"><br><br>

                </div>

            </div>

        <?php $this->endWidget(); ?>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/portamento-min.js', CClientScript::POS_END); ?>
<script type="text/javascript">

    $(function(){
       $('.enviar').click(function(){
           $('#formulario-piea-form').submit();
       });
        $('#leyenda').portamento({wrapper: $('#wrapper')});
    });
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var budgeteModule = angular.module('budgetModule', []);
    function BudgetController($scope, $http) {
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.itemsAdquisicion = [<?php foreach($presupuesto['items'][0] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->monto_idi_editado))?$item->getItemRevisado(user()->id)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota))?$item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->motivo_edicion))?"'".$item->getItemRevisado(user()->id)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado(user()->id)}},";}?>];
        $scope.itemsMateriales = [<?php foreach($presupuesto['items'][1] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->monto_idi_editado))?$item->getItemRevisado(user()->id)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota))?$item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->motivo_edicion))?"'".$item->getItemRevisado(user()->id)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado(user()->id)}},";}?>];
        $scope.itemsGastos = [<?php foreach($presupuesto['items'][2] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->monto_idi_editado))?$item->getItemRevisado(user()->id)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota))?$item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->motivo_edicion))?"'".$item->getItemRevisado(user()->id)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado(user()->id)}},";}?>];
        $scope.itemsOtros = [<?php foreach($presupuesto['items'][3] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->monto_idi_editado))?$item->getItemRevisado(user()->id)->monto_idi_editado:'null';echo",item_de_costo_apropiado_nota:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota))?$item->getItemRevisado(user()->id)->item_de_costo_apropiado_nota:'null';echo",montos_adecuados_a_la_realizacion_item:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item))?$item->getItemRevisado(user()->id)->montos_adecuados_a_la_realizacion_item:'null';echo",motivo_edicion:";echo ($item->getEstaRevisado(user()->id) && isset($item->getItemRevisado(user()->id)->motivo_edicion))?"'".$item->getItemRevisado(user()->id)->motivo_edicion."'":'null';echo",esta_revisado:{$item->getEstaRevisado(user()->id)}},";}?>];
        $scope.totalFinalIDI=0;
        $scope.totalFinalOtros=0;
        $scope.totalFinalPropios=0;


        $scope.saveItem = function(item) {
            $http.post("<?php echo url('/formularios/revisionFondosIDI/revisarItemPresupuesto',array('id'=>$proyecto->id));?>",item)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.id!=undefined){
                        $scope.showError=0;$scope.showSuccess=1;
                        $scope.successMsg=data.mensaje;
                    }else{
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }

        $scope.formatCurrency=function(data){
            return accounting.formatMoney(data, "$", 0, ".", ",");
        }

        $scope.totalItem=function(id){
            if(id==1) {userArray=$scope.itemsAdquisicion;}
            else if(id==2) {userArray=$scope.itemsMateriales;}
            else if(id==3) {userArray=$scope.itemsGastos;}
            else if(id==4) {userArray=$scope.itemsOtros;}
            var total=0;
            var totalIDI=0;
            var totalPropios=0;
            var totalOtros=0;
            console.log(id);
            angular.forEach(userArray, function(data) {
                total += parseInt(data.precio)*parseInt(data.unidades);
                totalIDI += parseInt(data.idi);
                totalOtros+=parseInt(data.otros);
                totalPropios+=parseInt(data.propio);
            });
            userArray.totalFinal=total;
            userArray.totalIDI=totalIDI;
            userArray.totalOtros=totalOtros;
            userArray.totalPropios=totalPropios;
            return total;
        }

        $scope.totalBudget=function(){
            $scope.totalFinalIDI=$scope.itemsAdquisicion.totalIDI+$scope.itemsMateriales.totalIDI+$scope.itemsGastos.totalIDI+$scope.itemsOtros.totalIDI;
            $scope.totalFinalOtros=$scope.itemsAdquisicion.totalOtros+$scope.itemsMateriales.totalOtros+$scope.itemsGastos.totalOtros+$scope.itemsOtros.totalOtros;
            $scope.totalFinalPropios=$scope.itemsAdquisicion.totalPropios+$scope.itemsMateriales.totalPropios+$scope.itemsGastos.totalPropios+$scope.itemsOtros.totalPropios;
            return $scope.itemsAdquisicion.totalFinal+$scope.itemsMateriales.totalFinal+$scope.itemsGastos.totalFinal+$scope.itemsOtros.totalFinal;
        }

    }

</script>
