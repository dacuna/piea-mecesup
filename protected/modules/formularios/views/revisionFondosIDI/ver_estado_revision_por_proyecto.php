<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Ver estado de revisiones por proyecto'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Ver estado de revisiones por proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>A continuaci&oacute;n, se listan las revisiones realizadas por cada proyecto a revisar. Puedes cambiar
                de proyecto mediante el siguiente combo desplegable:
                <br><br>
                Proyectos postulantes:
                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                    'id'=>'formulario-piea-form','type'=>'nice','method'=>'get')); ?>
                    <div class="row">
                        <div class="ten columns">
                            <select name="id" style="width:100%;">
                                <?php foreach($proyectos as $p):?>
                                    <option value="<?php echo $p->id;?>"><?php echo $p->nombre;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="two columns">
                            <input type="submit" name="enviar" value="Ver" class="nice small button">
                        </div>
                    </div>
                <?php $this->endWidget();?>
            </p>

            <?php if(isset($proyecto)):?>

                <p>Se est&aacute; observando el proyecto <?php echo $proyecto->nombre;?></p>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'persona-grid',
                    'dataProvider'=>$revisiones,
                    'columns'=>array(
                        array(
                            'name'=>'ID',
                            'value'=>'$data->revisor->id'
                        ),
                        array(
                            'name'=>'Revisor',
                            'value'=>'$data->revisor->nombreCompleto'
                        ),
                        array(
                            'name'=>'estado',
                            'type'=>'raw'
                        ),
                        'nota',
                        array(
                            'class'=>'CLinkColumn',
                            'label'=>'Ir a Revisi&oacute;n',
                            'header'=>'Acci&oacute;n',
                            'urlExpression'=>'array("/formularios/revisionFondosIDI/verRevisionHecha",
                                "user"=>$data->id_usuario,"proyecto"=>$data->id_proyecto)',
                        ),
                    ),
                )); ?>

            <?php endif;?>

        </div>



    </li>

</ul>