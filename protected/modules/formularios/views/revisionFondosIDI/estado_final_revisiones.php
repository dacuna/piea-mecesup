<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Estado final de revisiones'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Estado final de revisiones</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>A continuaci&oacute;n se lista el estado final de todos los proyectos junto con su nota final de
                revisi&oacute;n.
            </p>

            <h5>L&iacute;nea de postulaci&oacute;n consolidaci&oacute;n</h5>

            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Campus</th>
                    <th>Email</th>
                    <th>Postulante</th>
                    <th>Nota Final</th>
                    <th>I+D+i solicitado</th>
                    <th>I+D+i asignado</th>
                    <th>I+D+i modificado</th>
                    <th>Acci&oacute;n</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($linea_a as $proyecto_a):?>
                        <tr>
                            <td><?php echo $proyecto_a[0];?></td>
                            <td><?php echo $proyecto_a[1];?></td>
                            <td><?php echo $proyecto_a[8];?></td>
                            <td><?php echo $proyecto_a[2];?></td>
                            <td><?php echo $proyecto_a[7];?></td>
                            <td><?php echo $proyecto_a[3];?></td>
                            <td><?php echo $proyecto_a[4];?></td>
                            <td><?php echo $proyecto_a[5];?></td>
                            <td>
                                <?php if($proyecto_a[6]!=null):?>
                                    <?php echo app()->NumberFormatter->formatDecimal($proyecto_a[6]->monto_idi);?>
                                <?php else:?>
                                    <input type="text" name="modificado" size="5" class="monto_idi">
                                <?php endif;?>
                            </td>
                            <td>
                                <?php if($proyecto_a[6]!=null):?>
                                    Proyecto seleccionado
                                <?php else:?>
                                    <a href="<?php echo url('/formularios/revisionFondosIDI/seleccionarProyecto',
                                        array('id'=>$proyecto_a[0]));?>" class="seleccionar">Seleccionar</a>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>

            <h5>L&iacute;nea de postulaci&oacute;n iniciaci&oacute;n</h5>

            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Campus</th>
                    <th>Postulante</th>
                    <th>Email</th>
                    <th>Nota Final</th>
                    <th>I+D+i solicitado</th>
                    <th>I+D+i asignado</th>
                    <th>I+D+i modificado</th>
                    <th>Acci&oacute;n</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($linea_b as $proyecto_a):?>
                    <tr>
                        <td><?php echo $proyecto_a[0];?></td>
                        <td><?php echo $proyecto_a[1];?></td>
                        <td><?php echo $proyecto_a[8];?></td>
                        <td><?php echo $proyecto_a[2];?></td>
                        <td><?php echo $proyecto_a[7];?></td>
                        <td><?php echo $proyecto_a[3];?></td>
                        <td><?php echo $proyecto_a[4];?></td>
                        <td><?php echo $proyecto_a[5];?></td>
                        <td>
                            <?php if($proyecto_a[6]!=null):?>
                                <?php echo app()->NumberFormatter->formatDecimal($proyecto_a[6]->monto_idi);?>
                            <?php else:?>
                                <input type="text" name="modificado" size="5" class="monto_idi">
                            <?php endif;?>
                        </td>
                        <td>
                            <?php if($proyecto_a[6]!=null):?>
                                Proyecto seleccionado
                            <?php else:?>
                                <a href="<?php echo url('/formularios/revisionFondosIDI/seleccionarProyecto',
                                    array('id'=>$proyecto_a[0]));?>" class="seleccionar">Seleccionar</a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

        </div>



    </li>

</ul>
<script>
    $(function(){
        var time=0;
        $('.monto_idi').keyup(function(){
           var url=$(this).parent().next().children('.seleccionar').attr('href');
            console.log(url);
            var valmonto=$(this).val();
            if(time==0){
                url=url+'&monto='+valmonto;
                time+=1;
            }else{
                url=url.replace(new RegExp("&monto=.*"),'&monto='+valmonto);
            }
            $(this).parent().next().children('.seleccionar').attr('href',url);
        });
    });
</script>
