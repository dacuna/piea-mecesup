<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Dar permisos de revisi&oacute;n'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Dar permisos de revisi&oacute;n</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>Mediante el siguiente formulario puedes asignar permisos de revisi&oacute;n de proyectos del
                formulario de fondos I+D+i a un usuario en particular. <strong>OJO: </strong>
                <ul>
                    <li>- Aqui solo
                    entregas un rol de <i>revisor</i> a un usuario. Posteriormente deber&aacute;s registrar a que
                    proyectos deseas asignar a cada revisor de la postulaci&oacute;n.</li>
                    <li>- Los revisores s&oacute;lo son v&aacute;lidos para el proceso de postulaci&oacute;n actual. Por ejemplo
                    si se asigna a un usuario como revisor para el formulario del 2013, entonces dicho usuario no ser&aacute;
                    considerado como revisor para el formulario del 2014.</li>
                </ul>
            </p>

            <h5>Seleccionar usuario</h5>

            <!--<?php $form=$this->beginWidget('CActiveForm', array(
                'action'=>Yii::app()->createUrl($this->route),
                'method'=>'get',
            )); ?>

                <div class="row">
                    <?php echo $form->label($model,'email'); ?>
                    <?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>45)); ?>
                </div>

                <div class="row">
                    <?php echo $form->label($model,'nombres'); ?>
                    <?php echo $form->textField($model,'nombres',array('size'=>45,'maxlength'=>45)); ?>
                </div>

                <div class="row">
                    <?php echo $form->label($model,'apellido_paterno'); ?>
                    <?php echo $form->textField($model,'apellido_paterno',array('size'=>20,'maxlength'=>20)); ?>
                </div>

                <div class="row">
                    <?php echo $form->label($model,'apellido_materno'); ?>
                    <?php echo $form->textField($model,'apellido_materno',array('size'=>20,'maxlength'=>20)); ?>
                </div>

                <div class="row buttons">
                    <?php echo CHtml::submitButton('Buscar'); ?>
                </div>
            <?php $this->endWidget(); ?>-->

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'persona-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                    'id',
                    'email',
                    'nombres',
                    'apellido_paterno',
                    'apellido_materno',
                    array(
                        'class'=>'CLinkColumn',
                        'label'=>'Seleccionar',
                        'header'=>'Acci&oacute;n',
                        'urlExpression'=>'array("/formularios/revisionFondosIDI/darPermisoDeRevisor", "id"=>$data->id)',
                    ),
                ),
            )); ?>

        </div>



    </li>

</ul>