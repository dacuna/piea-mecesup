<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Proyectos en revisi&oacute;n'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Proyectos en revisi&oacute;n</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>Los siguientes son los proyectos que debes revisar para el formulario de fondos I+D+i. Se lista adem&aacute;s
                el estado de cada revisi&oacute;n. La nota que se indica junto a cada revisi&oacute;n ser&aacute; actualizada
                cada vez que revises el proyecto.
            </p>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'persona-grid',
                'dataProvider'=>$revisiones,
                'columns'=>array(
                    'proyecto.id',
                    'proyecto.nombre',
                    array(
                        'name'=>'estado',
                        'type'=>'raw'
                    ),
                    'nota',
                    array(
                        'class'=>'CLinkColumn',
                        'label'=>'Ir a Revisi&oacute;n',
                        'header'=>'Acci&oacute;n',
                        'urlExpression'=>'array("/formularios/revisionFondosIDI/revisionProyecto", "id"=>$data->id_proyecto)',
                    ),
                ),
            )); ?>

        </div>



    </li>

</ul>