<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Listar revisores'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Listar revisores</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">

            <p>Los siguientes son los revisores asignados al proceso actual de postulaci&oacute;n del formulario
                de fondos I+D+i.
            </p>

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'persona-grid',
                'dataProvider'=>$data,
                'columns'=>array(
                    array(
                        'name'=>'ID',
                        'value'=>'$data->usuario->id'
                    ),
                    array(
                        'name'=>'Nombre de revisor',
                        'value'=>'$data->usuario->nombreCompleto'
                    ),
                    array(
                        'name'=>'Email',
                        'value'=>'$data->usuario->email'
                    ),
                ),
            )); ?>

        </div>



    </li>

</ul>