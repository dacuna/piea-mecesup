<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/administracionFondos/verFormulario','id'=>1),
    'Asignaci&oacute;n de revisores a proyectos'
);
?>

<style>
    .wrapper1, .wrapper2 {overflow-x: scroll; overflow-y:hidden;}
    .wrapper1 {height: 20px; }
    .wrapper2 {height: 1000px; }
    .div1 {width:1500px; height: 20px; }
    .div2 {width:1500px; height: 1000px; overflow: auto;}
</style>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Asignaci&oacute;n de revisores a proyectos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta" ng-app="revisorModule" ng-controller="RevisorController">

        <div class="panel">

            <p>Mediante el siguiente formulario puedes asignar permisos de revisi&oacute;n a proyectos en particular
                para revisores en particular.<br><br>
                <strong>Instrucciones:</strong>
                <ul>
                    <li>- Para asignar a un usuario la posibilidad de revisar un proyecto debes hacer click sobre la
                    casilla correspondiente en la matriz.</li>
                    <li>- Por un breve lapso de tiempo, se bloquear&aacute;n todas las casillas de la matriz mientras el
                        sistema agrega al usuario como revisor de dicho proyecto.</li>
                    <li>- Si todo funcion&oacute; correctamente, las casillas se desbloquear&aacute;n de manera
                        autom&aacute;tica y podr&aacute;s designar nuevos usuarios como revisores de otros proyectos.</li>
                    <li>- Si es que por alg&uacute;n motivo ocurriese un error, entonces el sistema informar&aacute; de
                        la situaci&oacute;n.</li>
                </ul>
            </p>

        </div>

        <h5>Asignar revisores</h5>

        <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
            {{errorMsg}}
            <ul>
                <li ng-repeat="error in validationErrors">
                    - {{error}}
                </li>
            </ul>
            <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
        </div>

        <div class="wrapper1">
            <div class="div1">
            </div>
        </div>
        <div class="wrapper2">
            <div class="div2">
        <table>
            <thead>
                <tr>
                    <th style="width:15%;"></th>
                    <?php foreach($revisores as $revisor):?>
                        <th style="text-align: center;">
                            <p class="verticalText" style="font-size:12px;margin-bottom:0;"><?php echo $revisor->usuario->getNombrePresentacion(true);?></p>
                        </th>
                    <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($proyectos as $proyecto):?>
                    <tr>
                        <td>
                            <p title="<?php echo $proyecto->nombre;?>">
                                <?php echo $proyecto->id;?>.- <?php echo truncate($proyecto->nombre,40);?>
                            </p>
                        </td>
                            <?php foreach($revisores as $revisor):?>
                            <td style="text-align:center;">
                                <input type="checkbox" ng-model="asignacion['<?php echo md5($proyecto->id.$revisor->usuario->id);?>']"
                                    ng-click="saveRevision(<?php echo $proyecto->id.",".$revisor->usuario->id;?>)">
                            </td>
                        <?php endforeach;?>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
            </div>
        </div>


    </li>

</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script>
    $(function(){
        //$.each( $(".verticalText"), function () { $(this).html($(this).text().replace(/(.)/g, "$1<br />")) } );
    });
    $(function(){
        $(".wrapper1").scroll(function(){
            $(".wrapper2")
                .scrollLeft($(".wrapper1").scrollLeft());
        });
        $(".wrapper2").scroll(function(){
            $(".wrapper1")
                .scrollLeft($(".wrapper2").scrollLeft());
        });
    });


    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var revisorModule = angular.module('revisorModule', []);
    function RevisorController($scope, $http) {
        $scope.asignacion=[];
        <?php foreach($proyectos as $proyecto):?>
        <?php foreach($revisores as $revisor):?>
        $scope.asignacion["<?php echo md5($proyecto->id.$revisor->usuario->id);?>"]=<?php if(isset($asignados[md5($proyecto->id.$revisor->usuario->id)])) echo "true"; else echo "false";?>;
        <?php endforeach;?>
        <?php endforeach;?>

        $scope.saveRevision=function(p,u){
            $('input[type=checkbox]').attr("disabled", true);
            var send={id_usuario:u,id_proyecto:p};
            $http.post("<?php echo url('/formularios/revisionFondosIDI/asignarUsuarioProyecto');?>",send)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.mensaje=="correcto"){
                        $('input[type=checkbox]').removeAttr("disabled");
                    }else{
                        $('input[type=checkbox]').removeAttr("disabled");
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

</script>