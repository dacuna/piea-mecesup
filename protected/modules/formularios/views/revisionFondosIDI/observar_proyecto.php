<?php
$this->breadcrumbs=array(
    'Revisi&oacute;n Fondos I+D+i'=>array('/formularios/revisionFondosIDI/verProyectosARevisar'),
    $proyecto->nombre
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Observar Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained" ng-app="budgetModule" ng-controller="BudgetController">
    <li class="active" id="crear-cuenta" style="padding-left:10px;">

        <p>Estas observando el proyecto <strong><?php echo $proyecto->nombre;?></strong></p>

        <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
            'id'=>'formulario-piea-form','type'=>'nice')); ?>

        <?php echo $form->errorSummary(array($revision)); ?>

        <div class="panel">
            <h5>Perfil Proyecto</h5>

            <p>
                <strong>Nombre del proyecto: </strong><?php echo $proyecto->nombre;?><br>
                <strong>Iniciativa a la que pertenece: </strong><?php echo $proyecto->iniciativaPostulacion;?><br>
                <strong>Campus/Sede: </strong><?php echo $proyecto->campus;?>
            </p>


            <div style="border: 1px #ccc dashed;">
                <b>Extracto:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->extracto;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'extracto_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Resumen:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->resumen;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'resumen_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Objetivos:</b>
                <div class="row">

                    <div class="five columns">
                        <strong>Objetivo general:</strong> <?php echo $proyecto->perfilPostulacion->objetivos_generales;?>
                        <br>
                        <strong>Objetivos espec&iacute;ficos:</strong>
                        <ul>
                            <?php foreach($proyecto->objetivosEspecificos as $objetivo):?>
                                <li>- <?php echo $objetivo->descripcion;?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'objetivos_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>


            <br><br><input type="button" name="guardar1" value="Guardar" class="nice button enviar"><br><br>
        </div>

        <div class="panel">
            <h5>Antecedentes</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Estado del arte:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->estado_del_arte;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'estado_del_arte_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Experiencia previa:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->experiencia_previa;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'experiencia_previa_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar10" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>I+D+i</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Investigaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->investigacion;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'investigacion_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Desarrollo:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->desarrollo;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'desarrollo_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Innovaci&oacute;n:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->innovacion;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'innovacion_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar2" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>Impacto</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto Acad&eacute;mico:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_academico;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'impacto_academico_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Impacto universitario/social:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->impacto_universitario_social;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'impacto_universitario_social_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Proyecciones:</b>
                <div class="row">

                    <div class="five columns">
                        <?php echo $proyecto->perfilPostulacion->proyecciones;?>
                    </div>

                    <div class="seven columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'proyecciones_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar3" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel" id="wrapper">
            <h5>Hitos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                            <th style="text-align:center;" width="70">Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($hitos as $hito):?>
                            <tr>
                                <td>
                                    <?php echo $hito->nombre;?>
                                </td>
                                <td>
                                    <?php echo $hito->descripcion;?>
                                </td>
                                <td>
                                    <?php echo $hito->fecha;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'hitos_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                </div>
            </div>

            <br><br><input type="button" name="guardar5" value="Guardar" class="nice button enviar"><br><br>

        </div>


        <div class="panel">
            <h5>Equipo de trabajo</h5>

            <div style="border: 1px #ccc dashed;">
                <b>Integrantes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Nombre</th>
                                    <th style="text-align:center;">L&iacute;der</th>
                                    <th style="text-align:center;">Finanzas</th>
                                    <th style="text-align:center;">Responsabilidades</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->integrantes as $integrante):?>
                                <tr>
                                    <td>
                                        <?php echo $integrante->usuario->nombreCompleto;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esLider):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if($integrante->esFinanzas):?>
                                            S&iacute;
                                        <?php else:?>
                                            No
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php echo $integrante->responsabilidades;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'equipo_trabajo_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <div style="border: 1px #ccc dashed;">
                <b>Referentes:</b>
                <div class="row">

                    <div class="seven columns">
                        <table>
                            <thead>
                            <tr>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Email</th>
                                <th style="text-align:center;">Tel&eacute;fono</th>
                                <th style="text-align:center;">Relaci&oacute;n</th>
                                <th style="text-align:center;">Instituci&oacute;n</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($proyecto->referentes as $referente):?>
                                <tr>
                                    <td>
                                        <?php echo $referente->nombre;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->imprimirEmail();?>
                                    </td>
                                    <td>
                                        <?php echo $referente->telefono;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->relacion_proyecto;?>
                                    </td>
                                    <td>
                                        <?php echo $referente->institucion;?> / Cargo: <?php echo $referente->cargo;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>

                    <div class="five columns">
                        Comentario:
                        <?php $this->widget('application.extensions.redactorjs.Redactor',
                            array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'referentes_comentario',
                                'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                    </div>
                </div>
            </div>

            <br><br><input type="button" name="guardar4" value="Guardar" class="nice button enviar"><br><br>

        </div>


        <div class="panel">
            <h5>Anexos del proyecto</h5>

            <div class="row">

                <div class="seven columns">
                    <table>
                        <thead>
                        <tr>
                            <th style="text-align:center;">ID</th>
                            <th style="text-align:center;">Descripci&oacute;n</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($proyecto->anexos as $key=>$anexo):?>
                            <tr>
                                <td><?php echo $key+1;?></td>
                                <td>
                                    <a href="<?php echo url('/formularios/postulacionFondosIDI/sendAnexo',array('p'=>$proyecto->id,
                                        'id'=>$anexo->id));?>">
                                        <?php echo $anexo->nombre;?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

                <div class="five columns">
                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'anexos_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
                </div>
            </div>

            <br><br><input type="button" name="guardar8" value="Guardar" class="nice button enviar"><br><br>

        </div>

        <div class="panel">
            <h5>Carta Gantt</h5>
            <p>(Los items a evaluar se encuentran bajo la carta gantt)</p>
            <div class="row">

                <div class="twelve columns">
                    <table>
                        <thead>
                        <tr>
                            <th width="200" height="18" >Nombre</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                            <th width="30" height="18" >Jul</th>
                            <th width="30" height="18" >Ago</th>
                            <th width="30" height="18" >Sep</th>
                            <th width="30" height="18" >Oct</th>
                            <th width="30" height="18" >Nov</th>
                            <th width="30" height="18" >Dic</th>
                            <th width="30" height="18" >Ene</th>
                            <th width="30" height="18" >Feb</th>
                            <th width="30" height="18" >Mar</th>
                            <th width="30" height="18" >Abr</th>
                            <th width="30" height="18" >May</th>
                            <th width="30" height="18" >Jun</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($actividades as $actividad):?>
                            <tr>
                                <td width="200" height="18" >
                                    <?php echo $actividad->nombre;?>
                                </td>
                                <?php foreach ($actividad->meses as $mes):?>
                                    <td width="30" height="18" >
                                        <?php if($mes==1) echo "X";?>
                                    </td>
                                <?php endforeach;?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>

            <p>
                Comentario:
                <?php $this->widget('application.extensions.redactorjs.Redactor',
                    array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'carta_gantt_comentario',
                        'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>
            </p>

            <br><br><input type="button" name="guardar6" value="Guardar" class="nice button enviar"><br><br>

        </div>

            <h5>Presupuesto del proyecto</h5>

            <div class="row">

                <div class="twelve columns">
                    <table>
                    <thead>
                    <tr>
                        <th style="font-size: 13px;">Item</th>
                        <th style="font-size: 13px;">Precio</th>
                        <th style="font-size: 13px;">Cant.</th>
                        <th style="font-size: 13px;">Total</th>
                        <th style="font-size: 13px;">I+D+i</th>
                        <th style="font-size: 13px;">Propio</th>
                        <th style="font-size: 13px;">Otros</th>
                        <th style="background-color:rgb(147, 235, 147);font-size: 12px;text-align:center;">I+D+i Sugerido</th>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><b>Adquisiciones de implementaci&oacute;n</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsAdquisicion">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="text-align:center;">
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL (Adquisiciones de implementaci&oacute;n)</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(1))}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsAdquisicion.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Materiales e insumos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsMateriales">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="text-align:center;">
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Materiales e insumos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(2))}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsMateriales.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Gastos Operativos</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsGastos">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="text-align:center;">
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Gastos operativos]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(3))}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsGastos.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Otros items</b></td>
                        <td></td>
                        <td></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td class="money"></td>
                        <td></td>
                    </tr>
                    <tr ng-repeat="item in itemsOtros">
                        <td><span>{{item.nombre}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.precio)}}</span></td>
                        <td class="money"><span>{{item.unidades}}</span></td>
                        <td class="money">{{formatCurrency(item.precio*item.unidades)}}</td>
                        <td class="money"><span>{{formatCurrency(item.idi)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.propio)}}</span></td>
                        <td class="money"><span>{{formatCurrency(item.otros)}}</span></td>
                        <td class="money" style="background-color:rgb(147, 235, 147);">
                            <input type="text" ng-model="item.monto_idi_editado" style="width:50px;">
                        </td>
                        <td style="text-align:center;">
                            <a href="" ng-click="saveItem(item)"><b>Guardar</b></a>
                        </td>
                    </tr>
                    <tr style="border-bottom:2px #818181 solid;">
                        <td><b>TOTAL [Otros items]</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalItem(4))}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalIDI)}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalPropios)}}</td>
                        <td class="money">{{formatCurrency(itemsOtros.totalOtros)}}</td>
                        <td class="money" style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td><b>TOTAL</b></td>
                        <td></td>
                        <td></td>
                        <td class="money">{{formatCurrency(totalBudget())}}</td>
                        <td class="money">{{formatCurrency(totalFinalIDI)}}</td>
                        <td class="money">{{formatCurrency(totalFinalPropios)}}</td>
                        <td class="money">{{formatCurrency(totalFinalOtros)}}</td>
                        <td style="background-color:rgb(147, 235, 147);"></td>
                        <td></td>
                    </tr>
                    </tbody>
                    </table>

                    Comentario:
                    <?php $this->widget('application.extensions.redactorjs.Redactor',
                        array('lang'=>'en','toolbar'=>'mini','model'=>$revision,'attribute'=>'presupuesto_comentario',
                            'htmlOptions'=>array('style'=>'height:100px;z-index:0;'),'editorOptions'=>array('focus'=>false)));?>

                    <br><br><input type="button" name="guardar9" value="Guardar" class="nice button enviar"><br><br>

                </div>

            </div>

        <?php $this->endWidget(); ?>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/portamento-min.js', CClientScript::POS_END); ?>
<script type="text/javascript">

    $(function(){
       $('.enviar').click(function(){
           $('#formulario-piea-form').submit();
       });
    });
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var budgeteModule = angular.module('budgetModule', []);
    function BudgetController($scope, $http) {
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.itemsAdquisicion = [<?php foreach($presupuesto['items'][0] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo (isset($item->getItemObservado(user()->id)->monto_idi_editado))?$item->getItemObservado(user()->id)->monto_idi_editado:'null';echo"},";}?>];
        $scope.itemsMateriales = [<?php foreach($presupuesto['items'][1] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo (isset($item->getItemObservado(user()->id)->monto_idi_editado))?$item->getItemObservado(user()->id)->monto_idi_editado:'null';echo"},";}?>];
        $scope.itemsGastos = [<?php foreach($presupuesto['items'][2] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo (isset($item->getItemObservado(user()->id)->monto_idi_editado))?$item->getItemObservado(user()->id)->monto_idi_editado:'null';echo"},";}?>];
        $scope.itemsOtros = [<?php foreach($presupuesto['items'][3] as $item){ echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},monto_idi_editado:";echo (isset($item->getItemObservado(user()->id)->monto_idi_editado))?$item->getItemObservado(user()->id)->monto_idi_editado:'null';echo"},";}?>];
        $scope.totalFinalIDI=0;
        $scope.totalFinalOtros=0;
        $scope.totalFinalPropios=0;


        $scope.saveItem = function(item) {
            $http.post("<?php echo url('/formularios/revisionFondosIDI/observarItemPresupuesto',array('id'=>$proyecto->id));?>",item)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.id!=undefined){
                        $scope.showError=0;$scope.showSuccess=1;
                        $scope.successMsg=data.mensaje;
                    }else{
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }

        $scope.formatCurrency=function(data){
            return accounting.formatMoney(data, "$", 0, ".", ",");
        }

        $scope.totalItem=function(id){
            if(id==1) {userArray=$scope.itemsAdquisicion;}
            else if(id==2) {userArray=$scope.itemsMateriales;}
            else if(id==3) {userArray=$scope.itemsGastos;}
            else if(id==4) {userArray=$scope.itemsOtros;}
            var total=0;
            var totalIDI=0;
            var totalPropios=0;
            var totalOtros=0;
            console.log(id);
            angular.forEach(userArray, function(data) {
                total += parseInt(data.precio)*parseInt(data.unidades);
                totalIDI += parseInt(data.idi);
                totalOtros+=parseInt(data.otros);
                totalPropios+=parseInt(data.propio);
            });
            userArray.totalFinal=total;
            userArray.totalIDI=totalIDI;
            userArray.totalOtros=totalOtros;
            userArray.totalPropios=totalPropios;
            return total;
        }

        $scope.totalBudget=function(){
            $scope.totalFinalIDI=$scope.itemsAdquisicion.totalIDI+$scope.itemsMateriales.totalIDI+$scope.itemsGastos.totalIDI+$scope.itemsOtros.totalIDI;
            $scope.totalFinalOtros=$scope.itemsAdquisicion.totalOtros+$scope.itemsMateriales.totalOtros+$scope.itemsGastos.totalOtros+$scope.itemsOtros.totalOtros;
            $scope.totalFinalPropios=$scope.itemsAdquisicion.totalPropios+$scope.itemsMateriales.totalPropios+$scope.itemsGastos.totalPropios+$scope.itemsOtros.totalPropios;
            return $scope.itemsAdquisicion.totalFinal+$scope.itemsMateriales.totalFinal+$scope.itemsGastos.totalFinal+$scope.itemsOtros.totalFinal;
        }

    }

</script>