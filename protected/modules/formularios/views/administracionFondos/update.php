<?php
/* @var $this AdministracionFondosController */
/* @var $model FormularioPiea */

$this->breadcrumbs=array(
	'Formulario Pieas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FormularioPiea', 'url'=>array('index')),
	array('label'=>'Create FormularioPiea', 'url'=>array('create')),
	array('label'=>'View FormularioPiea', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FormularioPiea', 'url'=>array('admin')),
);
?>

<h1>Update FormularioPiea <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>