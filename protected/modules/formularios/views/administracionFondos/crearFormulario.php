<?php
$this->breadcrumbs=array(
	'Formularios PIEA'=>array('index'),
	'Crear',
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Crear formulario PIEA</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <h5>Crear formulario</h5>
            <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
        </div>

    </li>
</ul>
