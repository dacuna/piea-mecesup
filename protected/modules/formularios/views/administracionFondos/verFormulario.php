<?php
$this->breadcrumbs=array(
	'Formularios PIEA'=>array('index'),
	$model->nombre,
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ver formulario PIEA</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <h5><?php echo $model->nombre;?></h5>

            <p>
                <b>Descripci&oacute;n: </b><?php echo $model->descripcion;?><br>
                <b>Estado: </b><?php echo $model->estadoFormulario;?><br>
            </p>

            <?php if($model->estado==1):?>
                <b>El proceso de postulaci&oacute;n actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('administrador')):?>
                    <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosEnPostulacion');?>">Ver proyectos que crearon una postulaci&oacute;n</a></li>
                    <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosPostulantes');?>">Ver proyectos que han enviado su postulaci&oacute;n</a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <a href="<?php echo url('/formularios/administracionFondoIDI/cerrarPeriodoPostulacion');?>"
                       class="nice button confirm-dialog">Cerrar per&iacute;odo de postulaciones</a>
                <?php endif;?>
            <?php endif;?>

            <?php if($model->estado==2):?>
                <b>El proceso de postulaci&oacute;n actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><b>El proceso de postulaci&oacute;n actual termin&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_cierre);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('idi_ver_proyectos_postulantes')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosEnPostulacion');?>">Ver proyectos que crearon una postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('idi_ver_proyectos_enviados')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosPostulantes');?>">Ver proyectos que han enviado su postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/darPermisoDeRevisor');?>">Asignar usuarios como revisores</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/listarRevisores');?>">Listar revisores asignados</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                    <li><a href="<?php echo url('/formularios/revisionFondosIDI/asignacionRevisoresProyectos');?>">Asignar revisores a proyectos</a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <a href="<?php echo url('/formularios/administracionFondoIDI/abrirPeriodoRevision');?>"
                       class="nice button">Abrir per&iacute;odo de revisiones</a>
                <?php endif;?>
            <?php endif;?>

            <?php if($model->estado==3):?>
                <b>El proceso de postulaci&oacute;n actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><b>El proceso de postulaci&oacute;n actual termin&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_cierre);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('idi_ver_proyectos_postulantes')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosEnPostulacion');?>">Ver proyectos que crearon una postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('idi_ver_proyectos_enviados')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosPostulantes');?>">Ver proyectos que han enviado su postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/darPermisoDeRevisor');?>">Asignar usuarios como revisores</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/listarRevisores');?>">Listar revisores asignados</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/asignacionRevisoresProyectos');?>">Asignar revisores a proyectos</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('idi_estado_proyectos')):?>
                    <li><a href="<?php echo url('/formularios/revisionFondosIDI/verEstadoRevisionPorProyecto');?>">Ver estado de revisiones por proyecto</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess("observador_proceso_{$procesoActual->id}")):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/observarProyectos');?>">Observar proyectos</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/generarExcelConRevisiones');?>">Generar excel con revisiones</a></li>
                    <?php endif;?>
                    <li><a href="<?php echo url('/formularios/revisionFondosIDI/verProyectosARevisar');?>">Ver proyectos a revisar</a></li>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <a href="<?php echo url('/formularios/administracionFondoIDI/cerrarPeriodoRevision');?>"
                       class="nice button">Cerrar periodo de revisiones de proyectos</a>
                <?php endif;?>
            <?php endif;?>

            <?php if($model->estado==4 || $model->estado==5):?>
                <b>El proceso de postulaci&oacute;n actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><b>El proceso de postulaci&oacute;n actual termin&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_cierre);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('idi_ver_proyectos_postulantes')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosEnPostulacion');?>">Ver proyectos que crearon una postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('idi_ver_proyectos_enviados')):?>
                        <li><a href="<?php echo url('/formularios/administracionFondoIDI/verProyectosPostulantes');?>">Ver proyectos que han enviado su postulaci&oacute;n</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('dar_permiso_de_revisor_fondos_idi')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/listarRevisores');?>">Listar revisores asignados</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('idi_estado_proyectos')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/verEstadoRevisionPorProyecto');?>">Ver estado de revisiones por proyecto</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/verEstadoFinalRevisiones');?>">Seleccionar proyectos</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/estadisticasProceso');?>">Ver estad&iacute;sticas del proceso</a></li>
                    <?php endif;?>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/formularios/revisionFondosIDI/generarExcelConRevisiones');?>">Generar excel con revisiones</a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <a href="<?php echo url('/formularios/administracionFondoIDI/cerrarPeriodoRevision');?>"
                       class="nice button">Cerrar periodo de revisiones de proyectos</a>
                <?php endif;?>
            <?php endif;?>

            <?php if($model->estado==0):?>
                <?php if(user()->checkAccess('administrador')):?>
                    <b>Iniciar un nuevo proceso de postulaci&oacute;n</b>

                    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('id'=>'proceso-piea-form','type'=>'nice')); ?>

                    <?php echo $form->labelEx($nuevoProceso,'fecha_inicio'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model'=>$nuevoProceso,
                    'attribute'=>'fecha_inicio',
                    'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
                    'language'=>'es',
                    'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
                ));
                    ?>
                    <div class="form-field error">
                        <?php echo $form->error($nuevoProceso,'fecha_inicio'); ?>
                    </div>

                    <?php echo $form->labelEx($nuevoProceso,'fecha_termino'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model'=>$nuevoProceso,
                    'attribute'=>'fecha_cierre',
                    'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
                    'language'=>'es',
                    'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
                     ));
                    ?>
                    <div class="form-field error">
                        <?php echo $form->error($nuevoProceso,'fecha_termino'); ?>
                    </div>

                    <?php echo CHtml::submitButton('Crear nuevo Proceso',array('class'=>'nice radius medium button')); ?>

                    <?php $this->endWidget(); ?>
                <?php endif;?>
            <?php endif;?>
            <br><br>
        </div>

    </li>
</ul>

<script>
    $(function(){
       $('.confirm-dialog').click(function(){
           if(confirm('¿Desea cerrar el proceso de postulacion actual?')){
               return true;
           }
           return false;
       });
    });
</script>