<?php
/* @var $this AdministracionFondosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Formulario Pieas',
);

$this->menu=array(
	array('label'=>'Create FormularioPiea', 'url'=>array('create')),
	array('label'=>'Manage FormularioPiea', 'url'=>array('admin')),
);
?>

<h1>Formulario Pieas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
