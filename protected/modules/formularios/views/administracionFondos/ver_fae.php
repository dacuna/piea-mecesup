<?php
$this->breadcrumbs=array(
	'Formularios PIEA'=>array('index'),
	'FAE',
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">FAE</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <h5>FAE</h5>

            <p>
                <b>Estado: </b><?php echo $formulario->estadoFAE;?><br>
            </p>

            <?php if($formulario->estado==1):?>
                <b>El proceso FAE actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('administrador')):?>
                    <li><a href="<?php echo url('/usuarios/fae/generarExcelEvaluacion');?>">
                            Generar excel FAE
                        </a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <?php $this->beginWidget('foundation.widgets.FounActiveForm',
                        array('id'=>'proceso-piea-form','type'=>'nice',
                              'action'=>array('/formularios/administracionFondos/update','id'=>$formulario->id))); ?>
                    <input type="hidden" name="FormularioPiea[estado]" value="2">
                    <input type="submit" value="Cerrar primera fase FAE" class="nice button confirm-dialog">
                    <?php $this->endWidget(); ?>
                <?php endif;?>
            <?php endif;?>

            <?php if($formulario->estado==2):?>
                <b>El proceso FAE actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/usuarios/fae/generarExcelEvaluacion');?>">
                                Generar excel FAE
                            </a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <?php $this->beginWidget('foundation.widgets.FounActiveForm',
                        array('id'=>'proceso-piea-form','type'=>'nice',
                            'action'=>array('/formularios/administracionFondos/update','id'=>$formulario->id))); ?>
                    <input type="hidden" name="FormularioPiea[estado]" value="3">
                    <input type="submit" value="Cerrar segunda fase FAE" class="nice button confirm-dialog">
                    <?php $this->endWidget(); ?>
                <?php endif;?>
            <?php endif;?>

            <?php if($formulario->estado==3):?>
                <b>El proceso FAE actual comenz&oacute; el: <?php echo app()->dateFormatter->format("dd' de 'MMMM' de 'y",$procesoActual->fecha_inicio);?></b>
                <br><br>
                <h6>Enlaces r&aacute;pidos:</h6>
                <ul>
                    <?php if(user()->checkAccess('administrador')):?>
                        <li><a href="<?php echo url('/usuarios/fae/generarExcelEvaluacion');?>">
                                Generar excel FAE
                            </a></li>
                    <?php endif;?>
                </ul>

                <?php if(user()->checkAccess('administrador')):?>
                    <a href="<?php echo url('/formularios/administracionFondoIDI/cerrarPeriodoRevision');?>"
                       class="nice button">Cerrar proceso FAE</a>
                <?php endif;?>
            <?php endif;?>

            <?php if($formulario->estado==0):?>
                <?php if(user()->checkAccess('administrador')):?>
                    <b>Iniciar un nuevo proceso FAE</b>

                    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array('id'=>'proceso-piea-form','type'=>'nice')); ?>

                    <?php echo $form->labelEx($nuevoProceso,'fecha_inicio'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model'=>$nuevoProceso,
                    'attribute'=>'fecha_inicio',
                    'options' => array('dateFormat'=>'dd-mm-yy','maxDate'=>'new Date()'),
                    'language'=>'es',
                    'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
                ));
                    ?>
                    <div class="form-field error">
                        <?php echo $form->error($nuevoProceso,'fecha_inicio'); ?>
                    </div>

                    <?php echo $form->labelEx($nuevoProceso,'fecha_termino'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model'=>$nuevoProceso,
                    'attribute'=>'fecha_cierre',
                    'options' => array('dateFormat'=>'dd-mm-yy','minDate'=>'new Date()'),
                    'language'=>'es',
                    'htmlOptions' => array('class'=>'input-text','style'=>'width:150px;'),
                     ));
                    ?>
                    <div class="form-field error">
                        <?php echo $form->error($nuevoProceso,'fecha_termino'); ?>
                    </div>

                    <?php echo CHtml::submitButton('Crear nuevo Proceso',array('class'=>'nice radius medium button')); ?>

                    <?php $this->endWidget(); ?>
                <?php endif;?>
            <?php endif;?>
            <br><br>
        </div>

    </li>
</ul>

<script>
    $(function(){
       $('.confirm-dialog').click(function(){
           if(confirm('¿Desea cerrar el proceso FAE actual?')){
               return true;
           }
           return false;
       });
    });
</script>