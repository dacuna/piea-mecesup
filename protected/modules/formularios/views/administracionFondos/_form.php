<?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
	'id'=>'formulario-piea-form',
	'enableAjaxValidation'=>false,
    'type'=>'nice'
)); ?>

	<p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'nombre'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

    <?php echo CHtml::submitButton('Crear',array('class'=>'nice radius medium button','encode'=>false)); ?>

<?php $this->endWidget(); ?>