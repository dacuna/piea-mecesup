<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id)
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Perfil Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <!--<h5>Ver Proyecto</h5>-->

            <p>A continuaci&oacute;n se presenta un resumen general del proyecto que
                preparas para la postulaci&oacute;n indicando el estado de cada parte del
                formulario, de este modo tienes una referencia de los campos que falta
                por completar.
                Recuerda que tu proyecto no ser&aacute; enviado hasta que presiones el bot&oacute;n
                "Enviar Postulaci&oacute;n" el que se habilitar&aacute; &uacute;nicamente cuando el
                formulario se encuentre debidamente completado con los antecedentes
                m&iacute;nimos para postular.
            </p>

            <p>
                <strong>Nombre del proyecto: </strong><?php echo $proyecto->nombre;?><br>
                <strong>Iniciativa a la que pertenece: </strong><?php echo $proyecto->iniciativaPostulacion;?><br>
                <strong>Campus/Sede: </strong><?php echo $proyecto->campus;?>
            </p>

            <?php if($estadoProceso && $proyecto->perfilPostulacion->estado_postulacion!=1):?>
                <?php if(!$showSend):?>
                    <form action="<?php echo url('/');?>">
                    <input type="submit" class="nice green button" value="Enviar Postulaci&oacute;n" disabled="disabled">
                    </form>
                <?php else:?>
                    <a href="<?php echo url('/formularios/postulacionFondosIDI/enviarPostulacion');?>" class="nice green button">
                        Enviar Postulaci&oacute;n</a><br><br>
                <?php endif;?>
            <?php else:?>
                <?php if($proyecto->perfilPostulacion->estado_postulacion==1):?>
                    <p style="color:green;font-weight:bold;">El proyecto ya ha sido enviado para revisi&oacute;n.</p>
                <?php endif;?>
                <a href="<?php echo url('/formularios/postulacionFondosIDI/generarPdfProyecto',array('id'=>$proyecto->id));?>"
                        class="nice medium button">
                    Generar PDF resumen del proyecto</a>
                <?php if(count($proyecto->anexos)>0):?>
                    <a href="<?php echo url('/formularios/postulacionFondosIDI/generarZipAnexos',array('id'=>$proyecto->id));?>"
                    class="nice medium button">
                    Generar .ZIP con anexos del proyecto</a>
                <?php endif;?>
                <br><br>
            <?php endif;?>

        </div>


        <?php if($revisiones!=null):?>

            <div class="panel">
                <h5>Revisiones del proyecto</h5>
                <p>
                    A continuaci&oacute;n se listan las revisiones de tu proyecto.
                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'persona-grid',
                        'dataProvider'=>$revisiones,
                        'columns'=>array(
                            'nota',
                            array(
                                'class'=>'CLinkColumn',
                                'label'=>'Ir a Revisi&oacute;n',
                                'header'=>'Acci&oacute;n',
                                'urlExpression'=>'array("/formularios/revisionFondosIDI/verRevisionHecha",
                                "user"=>md5($data->id_usuario+1234).".".($data->id_usuario+12),"proyecto"=>$data->id_proyecto,"e"=>1)',
                            ),
                        ),
                    )); ?>
                </p>
            </div>

        <?php endif;?>

<?php if($estadoProceso && $proyecto->perfilPostulacion->estado_postulacion!=1):?>
        <div class="row">
            <div class="six columns">
                <div class="panel">
                    <h5>Perfil del proyecto</h5>
                    <p>
                        <?php if(count($estadoPerfil)==0):?>
                            <span style="color:green;">El perfil de tu proyecto est&aacute; completo</span>
                        <?php else:?>
                            Existen aspectos pendientes en el perfil de proyecto que deben ser revisados:
                            <ul>
                                <?php foreach($estadoPerfil as $error):?>
                                    <li style="color:red;"> - <?php echo $error;?></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>
                    </p>
                </div>
            </div>

            <div class="six columns">
                <div class="panel">
                    <h5>Presupuesto del proyecto</h5>
                    <p>
                        Resumen del presupuesto del proyecto en postulaci&oacute;n:
                    <ul>
                        <li>- <strong>Total gastos</strong>: $<?php echo app()->NumberFormatter->formatDecimal($estadoFinanciero['total']);?></li>
                        <li>- <strong>Total gastos I+D+i</strong>: $<?php echo app()->NumberFormatter->formatDecimal($estadoFinanciero['totalIDI']);?></li>
                        <li>- <strong>Total gastos propios</strong>: $<?php echo app()->NumberFormatter->formatDecimal($estadoFinanciero['totalPropio']);?></li>
                        <li>- <strong>Total gastos otros</strong>: $<?php echo app()->NumberFormatter->formatDecimal($estadoFinanciero['totalOtros']);?></li>
                    </ul>

                    <?php if($estadoFinanciero['total']!=($estadoFinanciero['totalIDI']+$estadoFinanciero['totalPropio']+$estadoFinanciero['totalOtros'])):?>
                    <span style="color:red;">Existen items del presupuesto incorrectos. Por favor, revisar el presupuesto
                            antes de enviar el proyecto a revisi&oacute;n.</span>
                    <?php endif;?><br>
                    <?php if($estadoFinanciero['linea']==false):?>
                    <span style="color:red;">- Tu presupuesto I+D+i excede lo m&aacute;ximo otorgado en la l&iacute;ne de
                        postulaci&oacute;n seleccionada (l&iacute;nea
                    <?php echo ($proyecto->perfilPostulacion->linea_postulacion==0)?'A - m&aacute;ximo: $850.000':'B - m&aacute;ximo: $350.000';?>).</span>
                    <?php endif;?><br>
                    <a href="<?php echo url('/formularios/postulacionFondosIDI/presupuestoProyecto',array('id'=>$proyecto->id));?>">
                        Ir a la p&aacute;gina de presupuesto del proyecto</a>
                    </p>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="six columns">
                <div class="panel">
                    <h5>Integrantes del proyecto</h5>
                    <p>
                        Existen <strong><?php echo count($proyecto->integrantes);?></strong> usuarios
                        registrados en tu proyecto.
                    <ul style="color:red;">
                        <?php if(count($proyecto->integrantes)<2):?>
                            <li>- Deben registrarse a lo menos 2 integrantes en el proyecto.</li>
                        <?php endif;?>
                        <?php if($estadoIntegrantes[0]==false):?>
                            <li>- Existe m&aacute;s de un integrante asignado como l&iacute;der. Solo puede haber uno.</li>
                        <?php endif;?>
                        <?php if($estadoIntegrantes[1]==false):?>
                        <li>- Existe m&aacute;s de un integrante asignado como encargado de finanzas. Solo puede haber uno.</li>
                        <?php endif;?>
                        <?php if($estadoIntegrantes[2]==false):?>
                        <li>- Existen usuarios que a&uacute;n no han aceptado o rechazado su participaci&oacute;n en el proyecto.
                        Todos los usuarios deben aceptar o rechazar su participaci&oacute;n para poder enviar la postulaci&oacute;n</li>
                        <?php endif;?>
                        <?php if($estadoIntegrantes[3]==false):?>
                            <li>- A lo menos el 50% de los integrantes debe pertenecer a la UTFSM</li>
                        <?php endif;?>
                    </ul>

                    <a href="<?php echo url('/formularios/postulacionFondosIDI/equipoTrabajoProyecto',array('id'=>$proyecto->id));?>">
                        Ir a la p&aacute;gina de integrantes del proyecto</a>
                    </p>
                </div>
            </div>

            <div class="six columns">
                <div class="panel">
                    <h5>Hitos del proyecto</h5>
                    <p>
                        Existen <strong><?php echo count($hitos);?></strong> registrados en tu proyecto.
                        <?php if(count($hitos)<5):?>
                        <br><br><span style="color:red;">- El m&iacute;nimo de hitos a agregar es de 5, a&uacute;n te faltan
                            <?php echo (5-count($hitos));?> por agregar.
                        </span>
                        <?php endif;?>
                        <br><br>
                        <a href="<?php echo url('/formularios/postulacionFondosIDI/hitosProyecto',array('id'=>$proyecto->id));?>">
                            Ir a la p&aacute;gina de hitos del proyecto</a>
                    </p>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="six columns">
                <div class="panel">
                    <h5>Carta Gantt del proyecto</h5>
                    <p>
                        Existen <strong><?php echo count($actividades);?></strong> actividades registradas en la carta
                        gantt de tu proyecto.<br><br>
                    <a href="<?php echo url('/formularios/postulacionFondosIDI/cartaGantt',array('id'=>$proyecto->id));?>">
                        Ver la Carta Gantt del proyecto</a>
                    </p>
                </div>
            </div>

            <div class="six columns">
                <div class="panel">
                    <h5>Referentes del proyecto</h5>
                    <p>
                        Existen <strong><?php echo count($proyecto->referentes);?></strong> referentes registrados
                        en tu proyecto.
                    <br><br>
                        <a href="<?php echo url('/formularios/postulacionFondosIDI/referentesProyecto',array('id'=>$proyecto->id));?>">
                        Ir a la p&aacute;gina de referentes del proyecto</a>
                    </p>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="six columns">
                <div class="panel">
                    <h5>Anexos del proyecto</h5>
                    <p>
                        Existen <strong><?php echo count($proyecto->anexos);?></strong> anexos registrados
                        en tu proyecto.
                    <br><br>
                        <a href="<?php echo url('/formularios/postulacionFondosIDI/anexosProyecto',array('id'=>$proyecto->id));?>">
                        Ir a la p&aacute;gina de anexos del proyecto</a>
                    </p>
                </div>
            </div>

            <div class="six columns">

            </div>

        </div>
<?php endif;?>

    </li>
</ul>
