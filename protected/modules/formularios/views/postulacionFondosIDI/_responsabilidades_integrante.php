<div class="panel">
  <div class="row">
    <div class="twelve columns">

        <h5>Responsabilidades</h5>
        <p>Puedes elegir las responsabilidades que estimes necesarias para asignar al integrante
        {{person.nombre}}</p>
      </div>
    </div>

  <div class="row">
      <div class="six columns">
          <input type="checkbox" ng-model="resp[0].status" ng-change="computeResp()">
          Direcci&oacute;n estrat&eacute;gica
      </div>
      <div class="six columns">
          <input type="checkbox" ng-model="resp[1].status" ng-change="computeResp()">
          Planificaci&oacute;n
      </div>
  </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[2].status" ng-change="computeResp()">
            Adquisiciones
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[3].status" ng-change="computeResp()">
            Administraci&oacute;n de recursos
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[4].status" ng-change="computeResp()">
            Comunicaci&oacute;n con entidades externas
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[5].status" ng-change="computeResp()">
            B&uacute;squeda de financiamiento
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[6].status" ng-change="computeResp()">
            Redacci&oacute;n de documentos
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[7].status" ng-change="computeResp()">
            Actualizaci&oacute;n con el estado del arte
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[8].status" ng-change="computeResp()">
            Mantenci&oacute;n de documentaci&oacute;n
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[9].status" ng-change="computeResp()">
            Dise&ntilde;o de la arquitectura
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[10].status" ng-change="computeResp()">
            Dise&ntilde;o de funciones
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[11].status" ng-change="computeResp()">
            Implementaci&oacute;n t&eacute;cnica
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <input type="checkbox" ng-model="resp[12].status" ng-change="computeResp()">
            Instalaci&oacute;n de sistemas
        </div>
        <div class="six columns">
            <input type="checkbox" ng-model="resp[13].status" ng-change="computeResp()">
            Pruebas y casos de uso
        </div>
    </div>

    <br><br>

    <a href="" class="close-modal nice button">Ok</a>
</div>