<?php
$this->breadcrumbs=array(
    'Perfil del Proyecto',
);
?>

<style>
.row .counter{
    font-weight:bold;
}
.row .warning{color:#600;}
.row .exceeded{color:#e00;}
</style>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Perfil del Proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel" ng-app ng-controller="ProyectoController">
            <!--<h5><?php echo $title;?> Proyecto Fondos I+D+i</h5>-->

            <?php if($title=='Editar'):?>
                <p>Este formulario permite guardar perfiles de
                    proyecto incompletos, de modo que puedes editarlo posteriormente</p>
            <?php else:?>
                <p>Puedes editar posteriormente todos los datos asociados a tu proyecto.</p>
            <?php endif;?>

            <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                'id'=>'formulario-piea-form','type'=>'nice')); ?>

            <p class="note">Los campos marcados con <span class="required">*</span> son obligatorios.</p>

            <?php echo $form->errorSummary(array($proyecto,$perfil)); ?>

            <div class="row">
                <?php echo $form->labelEx($proyecto,'nombre'); ?>
                <p class="postulacion-note">Indicar un nombre explicativo del proyecto</p>
                <?php echo $form->textField($proyecto,'nombre',array('class'=>'input-text','style'=>'width:100%;')); ?>
            </div>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($proyecto,'iniciativa_id'); ?>
                    <p class="postulacion-note">Indicar la principal iniciativa responsable del proyecto</p>
                    <?php echo $form->dropDownList($proyecto, 'iniciativa_id',CHtml::listData($iniciativas, 'id',
                        'nombre_abreviado'),array('encode'=>false,'style'=>'height: 28px;width:100%;'));?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($proyecto,'campus'); ?>
                    <p class="postulacion-note">Indicar el campus donde se desarrollar&aacute; el proyecto</p>
                    <?php echo $form->dropDownList($proyecto, 'campus',CHtml::listData($proyecto->lista_campus, 'id',
                        'nombre'),array('encode'=>false,'style'=>'height: 28px;width:100%;')); ?>
                </div>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'extracto'); ?>
                <p class="postulacion-note">Resumen simple de la tem&aacute;tica del proyecto, este se utilizar&aacute;
                    para la difusi&oacute;n del proyecto por parte del PIE>A</p>
                <?php echo $form->textArea($perfil,'extracto',array('rows'=>5,'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'extracto'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'linea_postulacion'); ?>
                <p class="postulacion-note">Indicar a que l&iacute;nea desea postular el proyecto</p>
                <?php echo $form->radioButtonList($perfil, 'linea_postulacion', array('Consolidaci&oacute;n','Iniciaci&oacute;n'),array('labelOptions'=>array('style'=>'display:inline;margin-right:20px;'))); ?>
                <p style="margin-bottom: 5px;"></p>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'resumen'); ?>
                <?php echo $form->textArea($perfil,'resumen',array('rows'=>5,'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'resumen'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'objetivos_generales'); ?>
                <p class="postulacion-note">Descripci&oacute;n del objetivo principal perseguido con la ejecuci&oacute;n del
                proyecto</p>
                <?php echo $form->textField($perfil,'objetivos_generales',array('class'=>'input-text','style'=>'width:100%;')); ?>
            </div>

            <label>Objetivos Espec&iacute;ficos</label>
            <p class="postulacion-note">Descripci&oacute;n de objetivos intermedios que se buscan cumplir, con la
            finalidad de lograr el objetivo general.</p>
            <div class="row">
                <div class="ten columns">
                    <input type="text" class="input-text" ng-model="objectiveText"
                           placeholder="Escribe un objetivo espec&iacute;fico y luego haz click en el bot&oacute;n agregar" style="width:100%;">
                </div>
                <div class="two columns">
                    <input type="button" class="small nice button" value="Agregar" ng-click="addObjective()">
                 </div>
            </div>
            <p><strong>Llevas {{total()}} objetivos espec&iacute;ficos. Debes agregar a lo menos 3.</strong></p>
            <ul>
                <li ng-repeat="objective in objectives">
                   <div class="row">
                       <div class="ten columns">
                            - <span class="done-{{todo.done}}">{{objective.text}}</span>
                       </div>
                       <div class="two columns">
                            <a href="" ng-click="deleteObjective(objective)"><span style="color:red;">Eliminar</span></a>
                       </div>
                    <input type="hidden" name="PerfilProyecto[objetivos_especificos][]" value="{{objective.text}}">
                   </div>
                </li>
                <li ng-repeat="objective in addedObjectives">
                    <div class="row">
                        <div class="ten columns">
                            - <span class="done-{{todo.done}}">{{objective.text}}</span>
                        </div>
                        <div class="two columns">
                            <a href="" ng-click="deleteObjective(objective)"><span style="color:red;">Eliminar</span></a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="row">
                <?php echo $form->labelEx($perfil,'estado_del_arte'); ?>
                <p class="postulacion-note">Resumen de la documentaci&oacute;n encontrada en relaci&oacute;n con la
                    tem&aacute;tica sobre la cual se desea trabajar, publicaciones, tecnolog&iacute;a asociada y
                    cualquier otro resultado de la investigaci&oacute;n previa que sirva como punto de partida
                    para el proyecto a realizar. (Indicar referencias bibliográficas en anexos)</p>
                <?php echo $form->textArea($perfil,'estado_del_arte',array('rows'=>5, 'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'estado_del_arte'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'experiencia_previa'); ?>
                <p class="postulacion-note">Descripci&oacute;n de la experiencia que tiene el grupo que postula en
                    proyectos similares o experiencia de los integrantes en la ejecuci&oacute;n de tareas afines.</p>
                <?php echo $form->textArea($perfil,'experiencia_previa',array('rows'=>5, 'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'experiencia_previa'); ?>
            </div>

            <p style="margin-bottom:10px;">Selecciona la
                prioridad de Investigaci&oacute;n, Desarrollo e Innovaci&oacute;n que presenta tu
                proyecto (1 m&aacute;s alta, 3 m&aacute;s baja</p>

            <div class="row">
                <div class="four columns">

                </div>
                <div class="one columns">
                    &nbsp;&nbsp;1
                </div>
                <div class="one columns">
                    &nbsp;&nbsp;2
                </div>
                <div class="six columns">
                    &nbsp;&nbsp;3
                </div>
            </div>

            <div class="row">
                <div class="four columns">
                    - Lugar investigaci&oacute;n:
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_investigacion_0" value="1" <?php if($perfil->lugar_investigacion==1) echo 'checked="checked"';?> type="radio" class="lugar_1" data-id="investigacion" name="PerfilProyecto[lugar_investigacion]">
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_investigacion_1" value="2" <?php if($perfil->lugar_investigacion==2) echo 'checked="checked"';?> type="radio" class="lugar_2" data-id="investigacion"  name="PerfilProyecto[lugar_investigacion]">
                </div>
                <div class="six columns">
                    <input id="PerfilProyecto_lugar_investigacion_2" value="3" <?php if($perfil->lugar_investigacion==3) echo 'checked="checked"';?> type="radio" class="lugar_3" data-id="investigacion"  name="PerfilProyecto[lugar_investigacion]">
                </div>
            </div>

            <div class="row">
                <div class="four columns">
                    - Lugar desarrollo:
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_desarrollo_0" value="1" <?php if($perfil->lugar_desarrollo==1) echo 'checked="checked"';?> type="radio" class="lugar_1" data-id="desarrollo" name="PerfilProyecto[lugar_desarrollo]">
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_desarrollo_1" value="2" <?php if($perfil->lugar_desarrollo==2) echo 'checked="checked"';?> type="radio" class="lugar_2" data-id="desarrollo"  name="PerfilProyecto[lugar_desarrollo]">
                </div>
                <div class="six columns">
                    <input id="PerfilProyecto_lugar_desarrollo_2" value="3" <?php if($perfil->lugar_desarrollo==3) echo 'checked="checked"';?> type="radio" class="lugar_3" data-id="desarrollo"  name="PerfilProyecto[lugar_desarrollo]">
                </div>
            </div>

            <div class="row">
                <div class="four columns">
                    - Lugar innovaci&oacute;n:
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_innovacion_0" value="1" <?php if($perfil->lugar_innovacion==1) echo 'checked="checked"';?> type="radio" class="lugar_1" data-id="innovacion" name="PerfilProyecto[lugar_innovacion]">
                </div>
                <div class="one columns">
                    <input id="PerfilProyecto_lugar_innovacion_1" value="2" <?php if($perfil->lugar_innovacion==2) echo 'checked="checked"';?> type="radio" class="lugar_2" data-id="innovacion"  name="PerfilProyecto[lugar_innovacion]">
                </div>
                <div class="six columns">
                    <input id="PerfilProyecto_lugar_innovacion_2" value="3" <?php if($perfil->lugar_innovacion==3) echo 'checked="checked"';?> type="radio" class="lugar_3" data-id="innovacion"  name="PerfilProyecto[lugar_innovacion]">
                </div>
            </div>
            <br>

            <div class="row">
                <?php echo $form->labelEx($perfil,'investigacion'); ?>
                <?php echo $form->textArea($perfil,'investigacion',array('rows'=>5, 'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'investigacion'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'desarrollo'); ?>
                <?php echo $form->textArea($perfil,'desarrollo',array('rows'=>5, 'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'desarrollo'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'innovacion'); ?>
                <?php echo $form->textArea($perfil,'innovacion',array('rows'=>5, 'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'innovacion'); ?>
            </div>

            <p style="font-weight:bold;margin-bottom:0;">Impacto:</p>
            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($perfil,'impacto_academico'); ?>
                    <?php echo $form->textArea($perfil,'impacto_academico',array('rows'=>5,'style'=>'width:100%;')); ?>
                    <?php echo $form->error($perfil,'impacto_academico'); ?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($perfil,'impacto_universitario_social'); ?>
                    <?php echo $form->textArea($perfil,'impacto_universitario_social',array('rows'=>5,'style'=>'width:100%;')); ?>
                    <?php echo $form->error($perfil,'impacto_universitario_social'); ?>
                </div>
            </div>

            <div class="row">
                <?php echo $form->labelEx($perfil,'proyecciones'); ?>
                <?php echo $form->textArea($perfil,'proyecciones',array('rows'=>5,'style'=>'width:100%;')); ?>
                <?php echo $form->error($perfil,'proyecciones'); ?>
            </div>

            <?php echo CHtml::submitButton('Continuar postulaci&oacute;n',array(
                'class'=>'nice radius medium button','encode'=>false)); ?>

            <?php $this->endWidget(); ?>

        </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/underscore-min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.CharCount.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">

    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    function ProyectoController($scope, $http) {
        $scope.objectives = [<?php foreach($perfil->objetivos_especificos as $key=>$objetivo) echo "{id:{$key},text:'{$objetivo}',type:1},";?>];
        $scope.addedObjectives = [<?php foreach($proyecto->objetivosEspecificos as $objetivo) echo "{id:{$objetivo->id},text:'{$objetivo->descripcion}',type:2},";?>];
        $scope.idCounter=0;

        $scope.addObjective = function() {
            $scope.idCounter=$scope.total()+1;
            $scope.objectives.push({text:$scope.objectiveText, id:$scope.idCounter,type:1});
            $scope.objectiveText = '';
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.objectives, function(objective) {
                count += 1;
            });
            angular.forEach($scope.addedObjectives, function(objective) {
                count += 1;
            });
            return count;
        };

        $scope.deleteObjective = function(objective){
            id=objective.id;
            elem=-1;
            if(objective.type==1){
                count=0;
                angular.forEach($scope.objectives, function(objective) {
                    if(objective.id==id)
                        elem=count;
                    count+=1;
                });
                console.log(elem);
                $scope.objectives.remove(elem);
            }
            else{
                count=0;
                angular.forEach($scope.addedObjectives, function(objective) {
                    if(objective.id==id)
                        elem=count;
                    count+=1;
                });
                if(elem!=-1){
                    $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarObjetivoEspecifico',array('id'=>$proyecto->id));?>",
                        {params:{'obj':id}}).success(function(data) {
                            if(data.success!=undefined){
                                $scope.showSuccess=1;
                                $scope.successMsg=data.mensaje;
                                $scope.addedObjectives.remove(elem);
                            }else{
                                $scope.showError=1;$scope.showSuccess=0;
                                $scope.errorMsg=data.mensaje;
                            }
                        });
                }
            }
        }
    }

    $(function(){
       $("#PerfilProyecto_extracto").charCount({allowed:300,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_resumen").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_estado_del_arte").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_experiencia_previa").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_impacto_academico").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_impacto_universitario_social").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_proyecciones").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_investigacion").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_innovacion").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});
       $("#PerfilProyecto_desarrollo").charCount({allowed:500,warning:20,counterText: 'Caracteres restantes: '});


        $('.lugar_1').change(function(){
           var data=$(this).attr('data-id');
           $('.lugar_1').each(function(){
                if($(this).attr('data-id')!=data)
                    $(this).prop('checked', false);
           });
       });
        $('.lugar_2').change(function(){
            var data=$(this).attr('data-id');
            $('.lugar_2').each(function(){
                if($(this).attr('data-id')!=data)
                    $(this).prop('checked', false);
            });
        });
        $('.lugar_3').change(function(){
            var data=$(this).attr('data-id');
            $('.lugar_3').each(function(){
                if($(this).attr('data-id')!=data)
                    $(this).prop('checked', false);
            });
        });
    });
</script>
