<div class="panel">
        <h5>Agregar anexo</h5>
        <p>Para agregar una actividad debes completar los siguientes campos. Todos los campos son obligatorios.</p>

    <?php $form = $this->beginWidget('foundation.widgets.FounActiveForm',
    array('type' => 'nice','id' => 'somemodel-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),)); ?>

    <?php echo $form->textFieldRow($modelo,'nombre',array('style'=>'width:100%;')); ?>

    Archivo (solo se admiten archivos pdf o de Microsoft Excel.
    <?php
    $this->widget('xupload.XUpload', array(
            'url' => Yii::app()->createUrl("/formularios/postulacionFondosIDI/upload"),
            'model' => $anexoFile,
            //We set this for the widget to be able to target our own form
            'htmlOptions' => array('id'=>'somemodel-form'),
            'attribute' => 'file',
            'autoUpload'=>true,
            'options'=>array('acceptFileTypes' => "js:/(\.|\/)(xls|xlsx|pdf)$/i",
            'maxFileSize'=>'5242880')
        )
    );
    ?>

    <?php echo CHtml::submitButton('Agregar Anexo', array('name'=>'enviar','class' => 'nice radius medium button', 'encode' => false)); ?>

    <?php $this->endWidget(); ?>

</div>