<?php
$this->breadcrumbs=array(
    'Postulaci&oacute;n Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Anexos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Anexos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <!--<h5>Anexos</h5>-->

            <p>A continuaci&oacute;n se listan los anexos que se han agregado al proyecto. Para agregar un nuevo
            anexo presiona en el bot&oacute;n "Agregar Anexo".</p>

            <p style="text-align: right;">
                <a href="#" data-reveal-id="myModal" class="nice button">Agregar Anexo</a>
            </p>

            <?php if(count($proyecto->anexos)>0):?>

                <table>
                    <thead>
                        <tr>
                            <th>Descripci&oacute;n</th>
                            <th>Archivo</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($proyecto->anexos as $key=>$anexo):?>
                            <tr>
                                <td><?php echo $anexo->nombre;?></td>
                                <td>
                                    <a href="<?php echo url('/formularios/postulacionFondosIDI/sendAnexo',array('p'=>$proyecto->id,
                                    'id'=>$anexo->id));?>">
                                        <?php echo "Proyecto_{$proyecto->id}_anexo_".($key+1)."_{$anexo->original_name}";?>
                                    </a>
                                </td>
                                <td><a href="<?php echo url('/formularios/postulacionFondosIDI/eliminarAnexo',array(
                                    'id'=>$proyecto->id,'ref'=>$anexo->id
                                ));?>" style="color:red;">Eliminar</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>

            <?php else:?>
                No se han agregado anexos al proyecto. <br><br>
            <?php endif;?>

        </div>


        <?php
        $this->modal['id']='myModal';
        $this->modal['close']=true;
        $this->modal['size']="xlarge";
        $this->modal['content']=$this->renderPartial('_agregar_anexo',array('modelo'=>$model,'anexoFile'=>$anexoFile),true)
        ?>

    </li>
</ul>
