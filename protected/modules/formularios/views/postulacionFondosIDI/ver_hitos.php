<?php
$this->breadcrumbs=array(
    'Postulaci&oacute;n Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Ver Hitos'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Ver Hitos</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel" ng-app="milestoneModule" ng-controller="HitosController">
            <!--<h5>Ver Hitos</h5>-->

            <p>A continuaci&oacute;n se listan los hitos que has agregado a la postulaci&oacute;n de tu proyecto. Para
            agregar un nuevo hito presiona el bot&oacute;n "Agregar Hito". Puedes agregar los hitos que estimes
            necesario.</p>

            <div class="row">
                <a href="" class="nice small button" ng-show="showAdd==0" ng-click="showAdd=1" style="float:right;">+ Agregar Hito</a>
                <a href="" class="nice red small button" ng-show="showAdd==1" ng-click="showAdd=0" style="float:right;">Cancelar</a>
            </div>

            <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
                {{errorMsg}}
                <ul>
                    <li ng-repeat="error in validationErrors">
                        - {{error}}
                    </li>
                </ul>
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="agregar-hito" ng-show="showAdd==1" style="display:none;">
                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                'id'=>'agregar-hito','type'=>'nice')); ?>

                <div class="row">
                    <div class="six columns">
                        <?php echo $form->textFieldRow($model,'nombre',array('style'=>'width:100%;','ng-model'=>'milestone.nombre')); ?>
                    </div>
                    <div class="six columns">
                        <?php echo $form->labelEx($model,'fecha'); ?>
                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                            'model'=>$model,
                            'attribute'=>'fecha',
                            'options' => array('dateFormat'=>'dd-mm-yy','minDate'=>'new Date()'),
                            'language'=>'es',
                            'htmlOptions' => array('class'=>'input-text','style'=>'width:100%;','ng-model'=>'milestone.fecha'),
                        ));
                        ?>
                    </div>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'descripcion'); ?>
                    <?php echo $form->textArea($model,'descripcion',array('rows'=>5,'style'=>'width:100%;','ng-model'=>'milestone.descripcion')); ?>
                    <?php echo $form->error($model,'descripcion'); ?>
                </div>

                <a href="" class="nice medium green button" ng-click="saveMilestone()">Agregar Hito</a><br>
            </div>

            <?php $this->endWidget(); ?>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <p><strong>Has ingresado un total de {{total()}} hitos. <span ng-show="total()<5">Debes agregar a lo menos 5.</span></strong></p>

            <table>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Descripci&oacute;n</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="milestone in milestones | orderBy:'fecha'">
                        <td>{{milestone.nombre}}</td>
                        <td>{{milestone.fecha}}</td>
                        <td>{{milestone.descripcion}}</td>
                        <td><a href="" ng-click="deleteMilestone(milestone.id)"><span style="color:red;">Eliminar</span></a></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var milestoneModule = angular.module('milestoneModule', []);
    function HitosController($scope, $http) {
        $scope.milestone={};
        $scope.showAdd=0;
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.milestones = [<?php foreach($hitos as $hito){ echo "{id:{$hito->id},nombre:'{$hito->nombre}',descripcion:'";echo trim(preg_replace('/\s/',' ',$hito->descripcion));echo "',fecha:'{$hito->fecha}'},";}?>];

        $scope.saveMilestone = function() {
            $scope.milestone.fecha=$('#HitoProyecto_fecha').val();
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarHito',array('id'=>$proyecto->id));?>",$scope.milestone)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new milestone into the array
                    $scope.milestones.push({'id':data.id,'nombre':data.nombre,'descripcion':data.descripcion,'fecha':data.fecha});
                    $scope.milestone.nombre = '';
                    $scope.milestone.descripcion= '';
                    $scope.milestone.fecha= '';
                    $scope.showAdd=$scope.showError=0;
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.deleteMilestone = function(id){
            elem=-1;
            count=0;
            angular.forEach($scope.milestones,function(milestone) {
                if(milestone.id==id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarHito',array('id'=>$proyecto->id));?>",
                    {params:{'hito':id}}).success(function(data) {
                if(data.success!=undefined){
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                    $scope.milestones.remove(elem);
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                }
            });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

</script>
