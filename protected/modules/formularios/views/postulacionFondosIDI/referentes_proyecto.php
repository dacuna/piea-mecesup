<?php
$this->breadcrumbs=array(
    'Postulaci&oacute;n Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Referentes del proyecto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Referentes del proyecto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta" ng-app="refererModule" ng-controller="RefererController">

        <div class="panel">
            <!--<h5>Referentes del proyecto</h5>-->

            <p>A continuaci&oacute;n se listan los referentes que has agregado a tu proyecto. Para agregar un nuevo
            referente, debes presionar sobre el bot&oacute;n "Agregar referente".</p>

            <div class="row">
                <a href="" class="nice small button" ng-show="showAdd==0" ng-click="showAdd=1" style="float:right;">+ Agregar referente</a>
                <a href="" class="nice red small button" ng-show="showAdd==1" ng-click="showAdd=0" style="float:right;">Cancelar</a>
            </div>

            <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
                {{errorMsg}}
                <ul>
                    <li ng-repeat="error in validationErrors">
                        - {{error}}
                    </li>
                </ul>
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="agregar-referente" ng-show="showAdd==1" style="display:none;">
                <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                'id'=>'agregar-referente','type'=>'nice')); ?>

                <p>Los campos marcados con * son obligatorios.</p>

                <div class="row">
                    <div class="four columns">
                        <?php echo $form->textFieldRow($model,'nombre',array('style'=>'width:100%;','ng-model'=>'referer.nombre')); ?>
                    </div>
                    <div class="four columns">
                        <?php echo $form->textFieldRow($model,'email',array('style'=>'width:100%;','ng-model'=>'referer.email')); ?>
                    </div>
                    <div class="four columns">
                        <?php echo $form->textFieldRow($model,'telefono',array('style'=>'width:100%;','ng-model'=>'referer.telefono')); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="twelve columns">
                        <?php echo $form->textFieldRow($model,'relacion_proyecto',array('style'=>'width:100%;','ng-model'=>'referer.relacion_proyecto')); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="six columns">
                        <?php echo $form->textFieldRow($model,'institucion',array('style'=>'width:100%;','ng-model'=>'referer.institucion')); ?>
                    </div>
                    <div class="six columns">
                        <?php echo $form->textFieldRow($model,'cargo',array('style'=>'width:100%;','ng-model'=>'referer.cargo')); ?>
                    </div>
                </div>

                <a href="" class="nice medium green button" ng-click="saveReferer()">Agregar Referente</a><br>
            </div>

            <?php $this->endWidget(); ?>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <p><strong>Has ingresado un total de {{total()}} referentes.</strong></p>

        </div>

        <table ng-show="referers.length>0">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Tel&eacute;fono</th>
                <th>Relaci&oacute;n con el proyecto</th>
                <th>Instituci&oacute;n</th>
                <th>Cargo</th>
                <th>Acci&oacute;n</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="referer in referers">
                <td>{{referer.nombre}}</td>
                <td>{{referer.email}}</td>
                <td>{{referer.telefono}}</td>
                <td>{{referer.relacion_proyecto}}</td>
                <td>{{referer.institucion}}</td>
                <td>{{referer.cargo}}</td>
                <td><a href="" ng-click="deleteReferer(referer.id)"><span style="color:red;">Eliminar</span></a></td>
            </tr>
            </tbody>
        </table>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var milestoneModule = angular.module('refererModule', []);
    function RefererController($scope, $http) {
        $scope.referer={};
        $scope.showAdd=0;
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.referers = [<?php foreach($referentes as $r) echo "{id:{$r->id},nombre:'{$r->nombre}',email:'{$r->email}',telefono:'{$r->telefono}',relacion_proyecto:'{$r->relacion_proyecto}',institucion:'{$r->institucion}',cargo:'{$r->cargo}'},";?>];

        $scope.saveReferer = function() {
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarReferente',array('id'=>$proyecto->id));?>",$scope.referer)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new milestone into the array
                    $scope.referers.push($scope.referer);
                    $scope.referer={};
                    $scope.showAdd=$scope.showError=0;
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.referers, function(ref) {
                count += 1;
            });
            return count;
        };

        $scope.deleteReferer= function(id){
            elem=-1;
            count=0;
            angular.forEach($scope.referers,function(ref) {
                if(ref.id==id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarReferente',array('id'=>$proyecto->id));?>",
                    {params:{'ref':id}}).success(function(data) {
                if(data.success!=undefined){
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                    $scope.referers.remove(elem);
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                }
            });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

</script>