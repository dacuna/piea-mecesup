<style xmlns="http://www.w3.org/1999/html">
    .row:before, .row:after, .clearfix:before, .clearfix:after { content:""; display:table; }
    .row:after, .clearfix:after { clear: both; }
    .row, .clearfix { zoom: 1; }
    .p{margin-top:5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;}
    .top{
        border:1px #000000 solid;width:100%;height: 180px;
    }
    .money{
        text-align: right;
    }
</style>
    <h1><?php echo $proyecto->id;?>.- <?php echo $proyecto->nombre;?></h1>

    <p>
        Iniciativa PIEA: <?php echo $proyecto->iniciativaPostulacion;?><br>
        Campus/Sede: <?php echo $proyecto->campus;?><br>
        L&iacute;nea de postulaci&oacute;n: <?php echo $perfil->lineaPostulacion;?><br>
        Responsable del proyecto: <?php echo $proyecto->perfilPostulacion->postulante->nombreCompleto;?>
    </p>

<h2>1. Perfil del proyecto</h2>
     <h3>1.1 Extracto</h3>
     <p>
         <?php echo $perfil->extracto;?>
     </p>

<h3>1.2 Resumen</h3>
<p>
    <?php echo $perfil->resumen;?>
</p>


<h3>1.3 Objetivos</h3>

    <h4>1.3.1 Objetivos generales</h4>
    <p>
        <?php echo $perfil->objetivos_generales;?>
    </p>

    <h4>1.3.2 Objetivos espec&iacute;ficos</h4>
    <ul>
        <?php foreach($proyecto->objetivosEspecificos as $objetivo):?>
            <li><?php echo $objetivo->descripcion;?></li>
        <?php endforeach;?>
    </ul>

<h3>1.4 Estado del arte</h3>
<p>
    <?php echo $perfil->estado_del_arte;?>
</p>


<h3>1.5 Experiencia previa</h3>
<p>
    <?php echo $perfil->experiencia_previa;?>
</p>

<h3>1.6 Impacto Acad&eacute;mico</h3>
<p>
    <?php echo $perfil->impacto_academico;?>
</p>

<h3>1.7 Impacto Universitario/Social</h3>
<p>
    <?php echo $perfil->impacto_universitario_social;?>
</p>

<h3>1.8 Investigaci&oacute;n</h3>
<p>
    <?php echo $perfil->investigacion;?>
</p>

<h3>1.9 Desarrollo</h3>
<p>
    <?php echo $perfil->desarrollo;?>
</p>


<h3>1.10 Innovaci&oacute;n</h3>
<p>
    <?php echo $perfil->innovacion;?>
</p>

<h3>1.11 Proyecciones</h3>
<p>
    <?php echo $perfil->proyecciones;?>
</p>

<h2>2. Equipo de trabajo</h2>

<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18" >Nombre</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="60" height="18" >L&iacute;der</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="70" height="18" >Finanzas</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;" width="300" height="18" >Responsabilidades</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($proyecto->integrantes as $integrante):?>
    <tr ng-repeat="user in team">
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18">
            <?php echo $integrante->usuario->nombreCompleto;?>
        </td>
        <td style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="60" height="18">
            <?php if($integrante->esLider=="true"):?>
                S&iacute;
            <?php else:?>
                No
            <?php endif;?>
        </td>
        <td style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="70" height="18">
            <?php if($integrante->esFinanzas=="true"):?>
            S&iacute;
            <?php else:?>
            No
            <?php endif;?>
        </td>
        <td style="border-bottom: 1px #000000 solid;" width="300" height="18">
            <?php echo $integrante->responsabilidades;?>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>

<h2>3. Hitos del proyecto</h2>

<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18" >Nombre</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="120" height="18" >Fecha</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;" width="300" height="18" >Descripci&oacute;n</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($hitos as $hito):?>
    <tr ng-repeat="user in team">
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18">
            <?php echo $hito->nombre;?>
        </td>
        <td style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="120" height="18">
            <?php echo $hito->fecha;?>
        </td>
        <td style="border-bottom: 1px #000000 solid;" width="300" height="18">
            <?php echo $hito->descripcion;?>
        </td>
    </tr>
        <?php endforeach;?>
    </tbody>
</table>

<h2>4. Carta Gantt</h2>

<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18" >Nombre</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >May</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Jun</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Jul</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Ago</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Sep</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Oct</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Nov</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Dic</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Ene</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Feb</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Mar</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Abr</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >May</th>
        <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >Jun</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($actividades as $actividad):?>
    <tr>
        <td style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="200" height="18" >
            <?php echo $actividad->nombre;?>
        </td>
        <?php foreach ($actividad->meses as $mes):?>
            <td style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="30" height="18" >
                <?php if($mes==1) echo "X"; else echo "-";?>
            </td>
        <?php endforeach;?>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>

<h2>5. Presupuesto</h2>

<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
<thead>
<tr>
    <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="260">Nombre Item</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="75">Precio</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="75">Unidades</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="75">Total</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="75">I+D+i</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" width="75">Propio</th>
    <th style="text-align:center;border-bottom: 1px #000000 solid;" width="70">Otros</th>
</tr>
</thead>
<tbody>
<tr>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ><b>Adquisiciones de implementaci&oacute;n</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"></td>
</tr>
<?php foreach($items_cat[0] as $item):?>
    <tr>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span><?php echo $item->nombre;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->precio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo $item->unidades;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <?php echo app()->NumberFormatter->formatDecimal($item->precio*$item->unidades);?>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->idi);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->propio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->otros);?></span>
        </td>
    </tr>
<?php endforeach;?>
<tr style="border-bottom:2px #818181 solid;">
    <td style="text-align: right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><b>TOTAL (Adquisiciones de implementaci&oacute;n)</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][0]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][1]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][2]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][3]);?></td>
</tr>
<tr>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ><b>Materiales e insumos</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"></td>
</tr>
<?php foreach($items_cat[1] as $item):?>
    <tr>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span><?php echo $item->nombre;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->precio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo $item->unidades;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <?php echo app()->NumberFormatter->formatDecimal($item->precio*$item->unidades);?>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->idi);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->propio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->otros);?></span>
        </td>
    </tr>
<?php endforeach;?>
<tr style="border-bottom:2px #818181 solid;">
    <td style="text-align: right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><b>TOTAL (Materiales e insumos)</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][0]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][1]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][2]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][3]);?></td>
</tr>
<tr>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ><b>Gastos operativos</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"></td>
</tr>
<?php foreach($items_cat[2] as $item):?>
    <tr>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span><?php echo $item->nombre;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->precio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo $item->unidades;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <?php echo app()->NumberFormatter->formatDecimal($item->precio*$item->unidades);?>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->idi);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->propio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->otros);?></span>
        </td>
    </tr>
<?php endforeach;?>
<tr style="border-bottom:2px #818181 solid;">
    <td style="text-align: right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><b>TOTAL (Gastos operativos)</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][0]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][1]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][2]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[2][3]);?></td>
</tr>
<tr>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ><b>Otros items</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;" ></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"></td>
</tr>
<?php foreach($items_cat[3] as $item):?>
    <tr>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span><?php echo $item->nombre;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->precio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo $item->unidades;?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <?php echo app()->NumberFormatter->formatDecimal($item->precio*$item->unidades);?>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->idi);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->propio);?></span>
        </td>
        <td class="money" style="border-bottom: 1px #000000 solid;">
            <span ng-show="item.editable==false"><?php echo app()->NumberFormatter->formatDecimal($item->otros);?></span>
        </td>
    </tr>
<?php endforeach;?>
<tr style="border-bottom:2px #818181 solid;">
    <td style="text-align: right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><b>TOTAL (Otros items)</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][0]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][1]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][2]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($items_cat_total[3][3]);?></td>
</tr>
<tr style="font-weight:bold;">
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><b>TOTAL</b></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($total[0]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($total[1]);?></td>
    <td style="text-align:right;border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo app()->NumberFormatter->formatDecimal($total[2]);?></td>
    <td style="border-bottom: 1px #000000 solid;text-align:right;"><?php echo app()->NumberFormatter->formatDecimal($total[3]);?></td>
</tr>x
</tbody>
</table>


<h2>6. Referentes</h2>

<table style="border:1px #000000 solid;border-bottom: 0px;" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;text-align:center;" width="150">Nombre</th>
        <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;text-align:center;" width="100">Email</th>
        <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;text-align:center;" width="60">Tel&eacute;fono</th>
        <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;text-align:center;" width="150">Relaci&oacute;n con el proyecto</th>
        <th style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;text-align:center;" width="80">Instituci&oacute;n</th>
        <th style="border-bottom: 1px #000000 solid;">Cargo</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($proyecto->referentes as $referente):?>
    <tr>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo $referente->nombre;?></td>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo $referente->imprimirEmail();?></td>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo $referente->telefono;?></td>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo $referente->relacion_proyecto;?></td>
        <td style="border-bottom: 1px #000000 solid;border-right: 1px #000000 solid;"><?php echo $referente->institucion;?></td>
        <td style="border-bottom: 1px #000000 solid;"><?php echo $referente->cargo;?></td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>

<h2>7. Anexos</h2>

<ul>
    <?php foreach($proyecto->anexos as $anexo):?>
        <li><?php echo $anexo->original_name;?></li>
    <?php endforeach;?>
</ul>
