<?php
$this->breadcrumbs=array(
    'Postulaci&oacute;n Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Carta Gantt'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Carta Gantt</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
            <!--<h5>Carta Gantt</h5>-->

            <p>En la Carta Gantt debes detallar las
                actividades que desarrollaras para cumplir con los objetivos del proyecto, a continuaci&oacute;n se listan
                los hitos que has agregado a la postulaci&oacute;n. Para agregar actividades presiona el s&iacute;mbolo "+" que
                se encuentra al final de cada hito en la tabla.</p>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>
        </div>

        <?php if(count($hitos)==0):?>
            <p style="font-weight:bold;">Para agregar una actividad debes primero ingresar hitos en la pesta&ntilde;a de
                <a href="<?php echo url('/formularios/postulacionFondosIDI',array('id'=>$proyecto->id));?>">"Hitos del proyecto"</a></p>
        <?php else:?>
            <table>
                <thead>
                    <tr>
                        <th>Hito</th>
                        <th style="font-size: 11px;">May</th>
                        <th style="font-size: 11px;">Jun</th>
                        <th style="font-size: 11px;">Jul</th>
                        <th style="font-size: 11px;">Ago</th>
                        <th style="font-size: 11px;">Sep</th>
                        <th style="font-size: 11px;">Oct</th>
                        <th style="font-size: 11px;">Nov</th>
                        <th style="font-size: 11px;">Dic</th>
                        <th style="font-size: 11px;">Ene</th>
                        <th style="font-size: 11px;">Feb</th>
                        <th style="font-size: 11px;">Mar</th>
                        <th style="font-size: 11px;">Abr</th>
                        <th style="font-size: 11px;">May</th>
                        <th style="font-size: 11px;">Jun</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="milestone in milestones">
                        <td>
                            <span ng-show="milestone.es_hito==1"><b>{{milestone.nombre}}</b></span>
                            <p ng-show="milestone.es_hito==0" style="font-size:12px;margin-bottom:0;">{{milestone.nombre}}</p>
                        </td>
                        <td ng-repeat="mes in milestone.meses" ng-class="{nopad: milestone.es_hito==0 && mes==1}" style="text-align: center;">
                            <span ng-show="mes==1 && milestone.es_hito==1"><b>X</b></span>
                        </td>
                        <td>
                            <a href="" ng-show="milestone.es_hito==1" style="color:green;font-size:18px;"
                               data-reveal-id="myModal" ng-click="setMilestone(milestone)">+</a>
                            <a href="" ng-show="milestone.es_hito==0" style="color:red;font-size:16px;" ng-click="deleteActivity(milestone.id)">-</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php endif;?>

        <?php
        $this->angular=' ng-app="milestoneModule" ng-controller="HitosController"';
        $this->modal['id']='myModal';
        $this->modal['close']=true;
        $this->modal['size']="xlarge";
        $this->modal['content']=$this->renderPartial('_agregar_gantt',array('model'=>$model),true)
        ?>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var milestoneModule = angular.module('milestoneModule', []);
    function HitosController($scope, $http) {
        $scope.milestones=[<?php foreach($hitos as $h){
            echo "{id:{$h->id},nombre:\"{$h->nombre}\",meses:{$h->mesesAsJSON()},fecha:'{$h->fecha}',es_hito:{$h->es_hito}},";
        };?>];
        $scope.activity={};
        $scope.months=[<?php for($i=0;$i<14;$i++){echo "{id:{$i},status:false},";}?>];

        $scope.setMilestone=function(milestone){
            $scope.activity.milestone=milestone;
        }

        $scope.saveActivity=function(){
            $scope.activity.fecha=$scope.activity.milestone.fecha;//set activity date
            $scope.activity.meses=$scope.months;
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarActividad',array('id'=>$proyecto->id));?>",$scope.activity)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new milestone into the array, we need to insert the new activity in the right place
                    var newM=[];
                    angular.forEach($scope.milestones,function(m) {
                        newM.push(m);
                        if(m.id==$scope.activity.milestone.id)
                            newM.push({id:data.id,nombre:data.nombre,meses:data.meses,fecha:data.fecha,es_hito:data.es_hito});
                    });
                    $scope.milestones=newM;
                    $scope.activity={};
                    angular.forEach($scope.months,function(m) {
                        m.status=false;
                    });
                    $scope.showError=0;
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                    $('#myModal').trigger('reveal:close');
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        }

        $scope.deleteActivity = function(id){
            elem=-1;
            count=0;
            angular.forEach($scope.milestones,function(milestone) {
                if(milestone.id==id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarActividad',array('id'=>$proyecto->id));?>",
                    {params:{'act':id}}).success(function(data) {
                        if(data.success!=undefined){
                            $scope.showSuccess=1;
                            $scope.successMsg=data.mensaje;
                            $scope.milestones.remove(elem);
                        }else{
                            $scope.showError=1;$scope.showSuccess=0;
                            $scope.errorMsg=data.mensaje;
                        }
                    });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }
    }

    $(function(){
        $('.close-modal').click(function(){
            $('#myModal').trigger('reveal:close');
        });
    });

</script>