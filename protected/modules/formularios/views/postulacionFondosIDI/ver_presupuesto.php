<?php
$this->breadcrumbs=array(
    'Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Presupuesto'
);
?>

<dl class="nice contained tabs">
    <dd><a href="#" class="active">Presupuesto</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta" ng-app="budgetModule" ng-controller="BudgetController">

        <div class="panel">
        <!--<h5>Ver Presupuesto</h5>-->

        <p>A continuaci&oacute;n debes agregar el presupuesto completo necesario para el
            desarrollo de tu proyecto, ingresando cada &iacute;tem en la categor&iacute;a que le
            corresponde. Para agregar un nuevo item, debes llenar las casillas
            sobre la categor&iacute;a pertinente y luego presionar "agregar".</p>
            <p style="color: grey;font-size: 12px;">*Los items marcados con rojo corresponden a items que no est&aacute;n correctos, por el motivo de que
            el total del item es distinto que la suma de I+D+i + Propio + Otros.</p>
        </div>

        <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
            {{errorMsg}}
            <ul>
                <li ng-repeat="error in validationErrors">
                    - {{error}}
                </li>
            </ul>
            <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
        </div>

        <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
            {{successMsg}}
            <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
        </div>

        <table>
            <thead>
            <tr>
                <th>Nombre Item</th>
                <th>Precio</th>
                <th>Unidades</th>
                <th>Total</th>
                <th>I+D+i</th>
                <th>Propio</th>
                <th>Otros</th>
                <th>Acci&oacute;n</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>Adquisiciones de implementaci&oacute;n</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" class="input-text" name="nombre_1" ng-model="adquisicion.nombre" style="width:120px;"></td>
                <td><input type="text" class="input-text" name="precio_1" ng-model="adquisicion.precio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="unidades_1" ng-model="adquisicion.unidades" style="width:25px;"></td>
                <td style="text-align:right;">{{formatCurrency(adquisicion.precio*adquisicion.unidades)}}</td>
                <td><input type="text" class="input-text" name="idi_1" ng-model="adquisicion.idi" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="propio_1" ng-model="adquisicion.propio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="otros_1" ng-model="adquisicion.otros" style="width:50px;"></td>
                <td><a href="" ng-click="saveItem(1)">Agregar</a></td>
            </tr>
            <tr ng-repeat="item in itemsAdquisicion">
                <td ng-class="{'row-error-first': item.color==true}">
                    <span ng-show="item.editable==false">{{item.nombre}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.nombre" style="width:120px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.precio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.precio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{item.unidades}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.unidades" style="width:25px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    {{formatCurrency(item.precio*item.unidades)}}
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.idi)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.idi" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.propio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.propio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.otros)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.otros" style="width:50px;">
                </td>
                <td ng-class="{'row-error-last': item.color==true}" style="text-align:center;">
                    <a href="" ng-click="item.editable=true" ng-show="item.editable==false">Editar</a>
                    <a href="" ng-show="item.editable==true" ng-click="editItem(item)" style="color:green;">Guardar</a>
                    <a href="" ng-click="deleteItem(item)" style="color:red;">Eliminar</a>
                </td>
            </tr>
            <tr style="border-bottom:2px #818181 solid;">
                <td style="text-align: right;"><b>TOTAL (Adquisiciones de implementaci&oacute;n)</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;">{{formatCurrency(totalItem(1))}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsAdquisicion.totalIDI)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsAdquisicion.totalPropios)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsAdquisicion.totalOtros)}}</td>
                <td></td>
            </tr>
            <tr>
                <td><b>Materiales e insumos</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" class="input-text" name="nombre_2" ng-model="materiales.nombre" style="width:120px;"></td>
                <td><input type="text" class="input-text" name="precio_2" ng-model="materiales.precio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="unidades_2" ng-model="materiales.unidades" style="width:25px;"></td>
                <td style="text-align:right;">{{formatCurrency(materiales.precio*materiales.unidades)}}</td>
                <td><input type="text" class="input-text" name="idi_2" ng-model="materiales.idi" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="propio_2" ng-model="materiales.propio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="otros_2" ng-model="materiales.otros" style="width:50px;"></td>
                <td><a href="" ng-click="saveItem(2)">Agregar</a></td>
            </tr>
            <tr ng-repeat="item in itemsMateriales">
                <td ng-class="{'row-error-first': item.color==true}">
                    <span ng-show="item.editable==false">{{item.nombre}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.nombre" style="width:120px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.precio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.precio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{item.unidades}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.unidades" style="width:25px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    {{formatCurrency(item.precio*item.unidades)}}
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.idi)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.idi" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.propio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.propio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.otros)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.otros" style="width:50px;">
                </td>
                <td ng-class="{'row-error-last': item.color==true}" style="text-align:center;">
                    <a href="" ng-click="item.editable=true" ng-show="item.editable==false">Editar</a>
                    <a href="" ng-show="item.editable==true" ng-click="editItem(item)" style="color:green;">Guardar</a>
                    <a href="" ng-click="deleteItem(item)" style="color:red;">Eliminar</a>
                </td>
            </tr>
            <tr style="border-bottom:2px #818181 solid;">
                <td><b>TOTAL [Materiales e insumos]</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;">{{formatCurrency(totalItem(2))}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsMateriales.totalIDI)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsMateriales.totalPropios)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsMateriales.totalOtros)}}</td>
                <td></td>
            </tr>
            <tr>
                <td><b>Gastos Operativos</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" class="input-text" name="nombre_3" ng-model="gastos.nombre" style="width:120px;"></td>
                <td><input type="text" class="input-text" name="precio_3" ng-model="gastos.precio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="unidades_3" ng-model="gastos.unidades" style="width:25px;"></td>
                <td style="text-align:right;">{{formatCurrency(gastos.precio*gastos.unidades)}}</td>
                <td><input type="text" class="input-text" name="idi_3" ng-model="gastos.idi" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="propio_3" ng-model="gastos.propio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="otros_3" ng-model="gastos.otros" style="width:50px;"></td>
                <td><a href="" ng-click="saveItem(3)">Agregar</a></td>
            </tr>
            <tr ng-repeat="item in itemsGastos">
                <td ng-class="{'row-error-first': item.color==true}">
                    <span ng-show="item.editable==false">{{item.nombre}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.nombre" style="width:120px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.precio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.precio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{item.unidades}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.unidades" style="width:25px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    {{formatCurrency(item.precio*item.unidades)}}
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.idi)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.idi" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.propio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.propio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.otros)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.otros" style="width:50px;">
                </td>
                <td ng-class="{'row-error-last': item.color==true}" style="text-align:center;">
                    <a href="" ng-click="item.editable=true" ng-show="item.editable==false">Editar</a>
                    <a href="" ng-show="item.editable==true" ng-click="editItem(item)" style="color:green;">Guardar</a>
                    <a href="" ng-click="deleteItem(item)" style="color:red;">Eliminar</a>
                </td>
            </tr>
            <tr style="border-bottom:2px #818181 solid;">
                <td><b>TOTAL [Gastos operativos]</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;">{{formatCurrency(totalItem(3))}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsGastos.totalIDI)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsGastos.totalPropios)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsGastos.totalOtros)}}</td>
                <td></td>
            </tr>
            <tr>
                <td><b>Otros items</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td style="text-align:right;"></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" class="input-text" name="nombre_4" ng-model="otros.nombre" style="width:120px;"></td>
                <td><input type="text" class="input-text" name="precio_4" ng-model="otros.precio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="unidades_4" ng-model="otros.unidades" style="width:25px;"></td>
                <td style="text-align:right;">{{formatCurrency(otros.precio*otros.unidades)}}</td>
                <td><input type="text" class="input-text" name="idi_4" ng-model="otros.idi" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="propio_4" ng-model="otros.propio" style="width:50px;"></td>
                <td><input type="text" class="input-text" name="otros_4" ng-model="otros.otros" style="width:50px;"></td>
                <td><a href="" ng-click="saveItem(4)">Agregar</a></td>
            </tr>
            <tr ng-repeat="item in itemsOtros">
                <td ng-class="{'row-error-first': item.color==1}">
                    <span ng-show="item.editable==false">{{item.nombre}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.nombre" style="width:120px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.precio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.precio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{item.unidades}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.unidades" style="width:25px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    {{formatCurrency(item.precio*item.unidades)}}
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.idi)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.idi" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.propio)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.propio" style="width:50px;">
                </td>
                <td class="money" ng-class="{'row-error': item.color==true}">
                    <span ng-show="item.editable==false">{{formatCurrency(item.otros)}}</span>
                    <input type="text" class="input-text" ng-show="item.editable==true" ng-model="item.otros" style="width:50px;">
                </td>
                <td ng-class="{'row-error-last': item.color==true}" style="text-align:center;">
                    <a href="" ng-click="item.editable=true" ng-show="item.editable==false">Editar</a>
                    <a href="" ng-show="item.editable==true" ng-click="editItem(item)" style="color:green;">Guardar</a>
                    <a href="" ng-click="deleteItem(item)" style="color:red;">Eliminar</a>
                </td>
            </tr>
            <tr style="border-bottom:2px #818181 solid;">
                <td><b>TOTAL [Otros items]</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;">{{formatCurrency(totalItem(4))}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsOtros.totalIDI)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsOtros.totalPropios)}}</td>
                <td style="text-align:right;">{{formatCurrency(itemsOtros.totalOtros)}}</td>
                <td></td>
            </tr>
            <tr style="font-weight:bold;">
                <td><b>TOTAL</b></td>
                <td></td>
                <td></td>
                <td style="text-align:right;">{{formatCurrency(totalBudget())}}</td>
                <td style="text-align:right;">{{formatCurrency(totalFinalIDI)}}</td>
                <td style="text-align:right;">{{formatCurrency(totalFinalPropios)}}</td>
                <td style="text-align:right;">{{formatCurrency(totalFinalOtros)}}</td>
                <td></td>
            </tr>
            </tbody>
        </table>

        <div style="border: 2px #ccc dashed;padding:15px;background-color: #fcefa1;">
            <h6>Leyenda:</h6>
            <ul>
                <li>- <b>Precio</b>: precio <strong>unitario</strong> del item a agregar al presupuesto.</li>
                <li>- <b>Tota</b>l: El valor monetario del precio del item multiplicado por la cantidad de unidades de &eacute;ste.</li>
                <li>- <b>I+D+i</b>: El total monetario que se desea cubrir con lo obtenido en los fondos I+D+i del item agregado
                al presupuesto.</li>
                <li>- <b>Propio</b>: El total monetario cubierto con fondos propios del item agregado al presupuesto.</li>
                <li>- <b>Otros</b>: El total monetario cubierto con otros fondos del item agregado al presupuesto.</li>
            </ul>
        </div>

    </li>
</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var budgeteModule = angular.module('budgetModule', []);
    function BudgetController($scope, $http) {
        $scope.adquisicion={'precio':0,'unidades':0,color:false};
        $scope.materiales={'precio':0,'unidades':0,color:false};
        $scope.gastos={'precio':0,'unidades':0,color:false};
        $scope.otros={'precio':0,'unidades':0,color:false};
        $scope.showSuccess=0;
        $scope.showError=0;
        $scope.itemsAdquisicion = [<?php foreach($item1 as $item) echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},editable:false,color:{$item->color}},";?>];
        $scope.itemsMateriales = [<?php foreach($item2 as $item) echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},editable:false,color:{$item->color}},";?>];
        $scope.itemsGastos = [<?php foreach($item3 as $item) echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},editable:false,color:{$item->color}},";?>];
        $scope.itemsOtros = [<?php foreach($item4 as $item) echo "{id:{$item->id},categoria:'{$item->id_categoria}',nombre:'{$item->nombre}',precio:{$item->precio},unidades:{$item->unidades},idi:{$item->idi},propio:{$item->propio},otros:{$item->otros},editable:false,color:{$item->color}},";?>];
        $scope.totalFinalIDI=0;
        $scope.totalFinalOtros=0;
        $scope.totalFinalPropios=0;

        $scope.saveItem = function(category) {
            var useData,userArray;
            if(category==1) {$scope.adquisicion.id_categoria=1;useData=$scope.adquisicion;userArray=$scope.itemsAdquisicion;}
            if(category==2) {$scope.materiales.id_categoria=2;useData=$scope.materiales;userArray=$scope.itemsMateriales;}
            if(category==3) {$scope.gastos.id_categoria=3;useData=$scope.gastos;userArray=$scope.itemsGastos;}
            if(category==4) {$scope.otros.id_categoria=4;useData=$scope.otros;userArray=$scope.itemsOtros;}
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarItemPresupuesto',array('id'=>$proyecto->id));?>",useData)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new milestone into the array
                    var colorf=false;
                    if(data.precio*data.unidades!=data.idi+data.propio+data.otros)
                        colorf=true;
                    userArray.push({'id':data.id,'nombre':data.nombre,'precio':data.precio,'unidades':data.unidades,'idi':data.idi,'propio':data.propio,'otros':data.otros,editable:false,color:colorf});
                    useData.nombre = '';useData.precio=0;useData.unidades=0;
                    useData.idi=0;useData.propio=0;useData.otros=0;
                    $scope.showError=0;$scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;

                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        };

        $scope.editItem = function(item) {
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarItemPresupuesto',array('id'=>$proyecto->id));?>",item)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.id!=undefined){
                        $scope.showError=0;$scope.showSuccess=1;
                        $scope.successMsg=data.mensaje;
                        item.editable=false;
                        $scope.setColor(item);
                    }else{
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        };

        $scope.total = function() {
            var count = 0;
            angular.forEach($scope.milestones, function(milestone) {
                count += 1;
            });
            return count;
        };

        $scope.deleteItem = function(item){
            elem=-1;
            count=0;
            var userArray;
            if(item.categoria==1) {userArray=$scope.itemsAdquisicion;}
            if(item.categoria==2) {userArray=$scope.itemsMateriales;}
            if(item.categoria==3) {userArray=$scope.itemsGastos;}
            if(item.categoria==4) {userArray=$scope.itemsOtros;}
            angular.forEach(userArray,function(milestone) {
                if(milestone.id==item.id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarItemPresupuesto',array('id'=>$proyecto->id));?>",
                    {params:{'item':item.id}}).success(function(data) {
                if(data.success!=undefined){
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                    userArray.remove(elem);
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                }
            });
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }

        $scope.formatCurrency=function(data){
            return accounting.formatMoney(data, "$", 0, ".", ",");
        }

        $scope.totalItem=function(id){
            if(id==1) {userArray=$scope.itemsAdquisicion;}
            else if(id==2) {userArray=$scope.itemsMateriales;}
            else if(id==3) {userArray=$scope.itemsGastos;}
            else if(id==4) {userArray=$scope.itemsOtros;}
            var total=0;
            var totalIDI=0;
            var totalPropios=0;
            var totalOtros=0;
            console.log(id);
            angular.forEach(userArray, function(data) {
                total += parseInt(data.precio)*parseInt(data.unidades);
                totalIDI += parseInt(data.idi);
                totalOtros+=parseInt(data.otros);
                totalPropios+=parseInt(data.propio);
            });
            userArray.totalFinal=total;
            userArray.totalIDI=totalIDI;
            userArray.totalOtros=totalOtros;
            userArray.totalPropios=totalPropios;
            return total;
        }

        $scope.totalBudget=function(){
            $scope.totalFinalIDI=$scope.itemsAdquisicion.totalIDI+$scope.itemsMateriales.totalIDI+$scope.itemsGastos.totalIDI+$scope.itemsOtros.totalIDI;
            $scope.totalFinalOtros=$scope.itemsAdquisicion.totalOtros+$scope.itemsMateriales.totalOtros+$scope.itemsGastos.totalOtros+$scope.itemsOtros.totalOtros;
            $scope.totalFinalPropios=$scope.itemsAdquisicion.totalPropios+$scope.itemsMateriales.totalPropios+$scope.itemsGastos.totalPropios+$scope.itemsOtros.totalPropios;
            return $scope.itemsAdquisicion.totalFinal+$scope.itemsMateriales.totalFinal+$scope.itemsGastos.totalFinal+$scope.itemsOtros.totalFinal;
        }

        $scope.setColor = function (item) {
            if((parseInt(item.precio)*parseInt(item.unidades))!=(parseInt(item.idi)+parseInt(item.propio)+parseInt(item.otros)))
                item.color=true;
            else
                item.color=false;
            console.log(item.color);
        };
    }

</script>