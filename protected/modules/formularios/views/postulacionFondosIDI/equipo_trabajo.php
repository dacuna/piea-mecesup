<?php
$this->breadcrumbs=array(
    'Postulaci&oacute;n Fondos I+D+i'=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    $proyecto->nombre=>array('/formularios/postulacionFondosIDI/verProyecto','id'=>$proyecto->id),
    'Equipo de trabajo'
);
?>

<dl class="nice contained tabs" xmlns="http://www.w3.org/1999/html">
    <dd><a href="#" class="active">Equipo de trabajo</a></dd>
</dl>

<ul class="nice tabs-content contained">
    <li class="active" id="crear-cuenta">

        <div class="panel">
        <!--<h5>Equipo de trabajo</h5>-->

        <p>Para registrar tu equipo de trabajo debes buscar a los participantes
            de tu equipo en el sistema, luego indicar sus responsabilidades y
            finalmente agregarlo. S&oacute;lo puedes agregar personas que se encuentren
            registradas en el sistema, por lo que si no encuentras a alguien de tu
            equipo solic&iacute;tale que se registre de la misma forma que lo hiciste tu.
            Recuerda que cada integrante de tu equipo recibir&aacute; una notificaci&oacute;n
            en el sistema para que acepte ser parte de tu proyecto.
        </p>

        <div class="panel">
            <h5>Agregar Integrante</h5>
            <div class="row">
                <div class="seven columns">
                    Buscar usuario: <input type="text" class="input-text" name="buscar" ng-model="searchPattern" style="width:100%;">
                </div>
                <div class="three columns">
                    <label for="searchType">Por:</label><br>
                    <select ng-model="searchType" name="searchType" id="searchType" style="width:100%;height:28px;">
                        <option value="1" selected="selected">Por nombre</option>
                        <option value="0">Por email</option>
                    </select>
                </div>
                <div class="two columns">
                    <a href="" class="nice small button" ng-click="search()" style="margin-top:15px;">Buscar</a>
                </div>
            </div>

            <div class="alert-box error" ng-show="showError==1" style="display:none;margin-top:10px;">
                {{errorMsg}}
                <ul>
                    <li ng-repeat="error in validationErrors">
                        - {{error}}
                    </li>
                </ul>
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div class="alert-box success" ng-show="showSuccess==1" style="display:none;margin-top:10px;">
                {{successMsg}}
                <a href="" ng-click="resetMsg()" style="color: #000;position: absolute;right: 4px;top: 0;font-size: 18px;opacity: 0.2;padding: 4px;">×</a>
            </div>

            <div ng-show="searchResults.length>0">
                <table>
                    <thead>
                        <tr><th>Nombre del usuario</th><th>Email</th><th>Seleccionar</th></tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="user in searchResults">
                            <td>{{user.nombre}}</td>
                            <td>{{user.email}}</td>
                            <td><a href="" ng-click="addUser(user)">Seleccionar</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

            <div ng-show="person.valid==1">
                <div class="panel">
                    <h5>
                        <!--<span ng-show="person.editable==false">Agregar integrante</span>-->
                        <span ng-show="person.editable==true">Editar integrante</span>
                    </h5>
                    <p ng-show="person.editable==false">
                        Para agregar al integrante, debes seleccionar si es que &eacute;ste tiene un cargo
                        como l&iacute;der, cargo en finanzas y adem&aacute;s completar las responsabilidades
                        que tendr&aacute; en el proyecto. Luego, el usuario deber&aacute; aceptar su membres&iacute;a
                        en el proyecto. <span style="color:red;font-weight:bold;">Debe haber un
                        &uacute;nico usuario seleccionado como l&iacute;der y un &uacute;nico usuario seleccionado
                        como encargado de finanzas.</span>.
                    </p>
                    <p ng-show="person.editable==true">
                        Para editar al integrante, debes realizar los cambios correspondientes y luego presionar en
                        el bot&oacute;n <span style="color:green;font-weight:bold;">"Guardar cambios"</span>. Para cancelar la edici&oacute;n, presiona en "Cancelar".
                    </p>
                    <div class="row">
                        <div class="four columns">
                            <b>Integrante</b>
                        </div>
                        <div class="two columns" style="text-align:center;">
                            <b>Lider</b>
                        </div>
                        <div class="two columns" style="text-align:center;">
                            <b>Finanzas</b>
                        </div>
                        <div class="four columns">
                            <b>Responsabilidades</b>
                        </div>
                    </div>

                    <?php $form=$this->beginWidget('foundation.widgets.FounActiveForm', array(
                    'id'=>'agregar-integrante','type'=>'nice')); ?>

                    <div class="row">
                        <div class="four columns">
                            {{person.nombre}}
                        </div>

                        <div class="two columns" style="text-align:center;">
                            <input type="checkbox" name="lider" ng-model="person.lider" style="margin-top:10px;" value="1">
                        </div>
                        <div class="two columns" style="text-align:center;">
                            <input type="checkbox" name="finanzas" ng-model="person.finanzas" style="margin-top:10px;" value="1">
                        </div>
                        <div class="four columns">
                            <input type="text" class="input-text" name="responsabilidades" ng-model="person.responsabilidades" style="width:90%;float:left;">
                            <a href="#" data-reveal-id="myModal" style="font-size:28px;">+</a>
                        </div>
                    </div>
                    <a href="" class="nice small button" ng-click="addToTeam()" ng-show="person.editable==false">Agregar usuario a equipo de trabajo</a>
                    <a href="" class="nice small button" ng-click="editMember()" ng-show="person.editable==true">Guardar cambios</a>
                    <a href="" class="nice red small button" ng-click="person.valid=0;person.editable=false" ng-show="person.editable==true">Cancelar</a>
                    <?php $this->endWidget(); ?>
                </div>

            </div>


            <div class="panel">
            <h5>Integrantes del proyecto</h5>
            <p>A continuaci&oacute;n se listan los usuarios que han sido agregados como
                integrantes al proyecto. Recuerda que todos los integrantes deben
                aceptar la notificaci&oacute;n para formar parte de tu proyecto para
                que puedan ser considerados.
            </p>
            <?php if($estadoIntegrantes[0]==false || $estadoIntegrantes[1]==false):?>
                <p style="margin-bottom:5px;">
                Atenci&oacute;n:
                <ul style="color:red;">
                    <?php if($estadoIntegrantes[0]==false):?>
                    <li>- Existe m&aacute;s de un integrante asignado como l&iacute;der. Solo puede haber uno.</li>
                    <?php endif;?>
                    <?php if($estadoIntegrantes[1]==false):?>
                    <li>- Existe m&aacute;s de un integrante asignado como encargado de finanzas. Solo puede haber uno.</li>
                    <?php endif;?>
                </ul>
            </p>
            <?php endif;?>
            <table>
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>L&iacute;der</th>
                    <th>Finanzas</th>
                    <th>Responsabilidades</th>
                    <th>Estado</th>
                    <th>Acci&oacute;n</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="user in team">
                    <td>
                        {{user.nombre}}
                    </td>
                    <td>
                        <span ng-show="user.lider==true" style="color:green;">S&iacute;</span><span ng-show="user.lider==false" style="color:red;">No</span>
                    </td>
                    <td>
                        <span ng-show="user.finanzas==true" style="color:green;">S&iacute;</span><span ng-show="user.finanzas==false" style="color:red;">No</span>
                    </td>
                    <td>
                        {{user.responsabilidades}}
                    </td>
                    <td>
                        {{user.estado}}
                        <a href="" ng-click="sendInvitation(user)" ng-show="user.estadoNum==0"
                                >(Enviar solicitud para que el integrante acepte su participaci&oacute;n)</a>
                    </td>
                    <td style="color:red;">
                        <a href="" ng-click="preEdit(user)" ng-show="user.editable==false">Editar</a>
                        <a href="" ng-click="deleteMember(user.id)">Eliminar</a>
                    </td>
                </tr>
                </tbody>
            </table>
            </div>

        </div>

        <?php
        $this->angular=' ng-app="teamModule" ng-controller="teamController"';
        $this->modal['id']='myModal';
        $this->modal['close']=true;
        $this->modal['content']=$this->renderPartial('_responsabilidades_integrante',null,true)
        ?>

    </li>

</ul>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular.min.js', CClientScript::POS_HEAD); ?>

<script type="text/javascript">
    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    var teamModule = angular.module('teamModule', []);
    function teamController($scope, $http) {
        $scope.searchResults=[];
        $scope.searchType=1;
        $scope.team=[<?php foreach($integrantes as $integrante){echo "{id:'{$integrante->id}',nombre:'{$integrante->usuario->nombreCompleto}',lider:{$integrante->esLider},finanzas:{$integrante->esFinanzas},responsabilidades:'{$integrante->responsabilidades}',estado:'{$integrante->estadoAceptacion}',estadoNum:{$integrante->estado},editable:false},";}?>];
        $scope.person={valid:0,editable:false};
        $scope.resp=[{name:'Dirección estratégica',status:false},{name:'Planificación',status:false},
         {name:'Adquisiciones',status:false},{name:'Administración de recursos',status:false},
         {name:'Comunicación con entidades externas',status:false},{name:'Búsqueda de financiamiento',status:false},
         {name:'Redacción de documentos',status:false},{name:'Actualización con el estado del arte',status:false},
         {name:'Mantención de documentación',status:false},{name:'Diseño de la arquitectura',status:false},
         {name:'Diseño de funciones',status:false},{name:'Implementación técnica',status:false},
         {name:'Instalación de sistemas',status:false},{name:'Pruebas y casos de uso',status:false}];

        $scope.search=function(){
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/buscarUsuarios',array('id'=>$proyecto->id));?>",
                {params:{'q':$scope.searchPattern,'t':$scope.searchType}}).success(function(data) {
                    $scope.showSuccess=1;
                    $scope.successMsg=data.total;
                    $scope.searchResults=data.results;
                });
        }

        $scope.addToTeam=function(){
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarIntegrante',array('id'=>$proyecto->id));?>",$scope.person)
            .success(function(data, status, headers, config) {
                $scope.data = data;
                if(data.id!=undefined){
                    //add new member into the array of participants
                    $scope.person.estado=data.estado;
                    $scope.person.estadoNum=data.estadoNum;
                    $scope.person.id=data.id;
                    $scope.team.push($scope.person);
                    $scope.person={valid:0,editable:false};
                    $scope.searchResults=[];
                    angular.forEach($scope.resp,function(r) {
                        r.status=false;
                    });
                    $scope.showAdd=$scope.showError=0;
                    $scope.showSuccess=1;
                    $scope.successMsg=data.mensaje;
                }else{
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg=data.mensaje;
                    $scope.validationErrors=data.errores;
                }
            }).error(function(data, status, headers, config) {
                $scope.showError=1;$scope.showSuccess=0;
                $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
            });
        }

        $scope.sendInvitation=function(user){
            if(confirm('¿Enviar notificación a '+user.nombre+'?')){
                $http.post("<?php echo url('/formularios/postulacionFondosIDI/enviarNotificacionIntegrante',array('id'=>$proyecto->id));?>",user.id)
                    .success(function(data, status, headers, config) {
                        $scope.data = data;
                        if(data.mensaje!=undefined){
                            $scope.showAdd=$scope.showError=0;
                            $scope.showSuccess=1;
                            $scope.successMsg=data.mensaje;
                            user.estadoNum=2;
                        }
                    }).error(function(data, status, headers, config) {
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                    });
            }
        }

        $scope.resetMsg=function(){
            $scope.showSuccess=0;
            $scope.showError=0;
        }

        $scope.addUser=function(user){
            $scope.person=user;
            $scope.person.finanzas=false;
            $scope.person.lider=false;
            $scope.person.valid=1;$scope.person.editable=false;
            //enable editable in users
            angular.forEach($scope.team,function(r) {
                r.editable=false;
            });
        }

        $scope.editMember=function(){
            $http.post("<?php echo url('/formularios/postulacionFondosIDI/agregarIntegrante',array('id'=>$proyecto->id));?>",$scope.person)
                .success(function(data, status, headers, config) {
                    $scope.data = data;
                    if(data.id!=undefined){
                        $scope.person.estado=data.estado;
                        $scope.person.estadoNum=data.estadoNum;
                        $scope.person.editable=false;
                        $scope.person={valid:0,editable:false};
                        $scope.searchResults=[];
                        angular.forEach($scope.resp,function(r) {
                            r.status=false;
                        });
                        $scope.showAdd=$scope.showError=0;
                        $scope.showSuccess=1;
                        $scope.successMsg=data.mensaje;
                    }else{
                        $scope.showError=1;$scope.showSuccess=0;
                        $scope.errorMsg=data.mensaje;
                        $scope.validationErrors=data.errores;
                    }
                }).error(function(data, status, headers, config) {
                    $scope.showError=1;$scope.showSuccess=0;
                    $scope.errorMsg="Ha ocurrido un error inesperado. Por favor, int&eacute;ntelo nuevamente. Si los problemas persisten, contactar al administrador.";
                });
        }

        $scope.deleteMember = function(id){
            elem=-1;
            count=0;
            angular.forEach($scope.team,function(milestone) {
                if(milestone.id==id)
                    elem=count;
                count+=1;
            });
            $http.get("<?php echo url('/formularios/postulacionFondosIDI/eliminarIntegrante',array('id'=>$proyecto->id));?>",
                    {params:{'integrante':id}}).success(function(data) {
                        if(data.success!=undefined){
                            $scope.showSuccess=1;
                            $scope.successMsg=data.mensaje;
                            $scope.team.remove(elem);
                        }else{
                            $scope.showError=1;$scope.showSuccess=0;
                            $scope.errorMsg=data.mensaje;
                        }
                    });
        }

        $scope.computeResp=function(){
            var sresp="";
            angular.forEach($scope.resp,function(r) {
                if(r.status)
                    sresp+= r.name+', ';
            });
            $scope.person.responsabilidades=sresp;
        }

        $scope.preEdit = function (user) {
            $scope.person=user;
            $scope.person.valid=1
            $scope.person.editable=true;
        };
    }

    $(function(){
        $('.close-modal').click(function(){
            $('#myModal').trigger('reveal:close');
        });
    });

</script>