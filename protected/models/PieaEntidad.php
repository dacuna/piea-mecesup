<?php
/**
 * Representa la entidad PIEA dentro del sistema.
 *
 * En un esfuerzo por normalizar el codigo para las iniciativas y proyectos se creo la clase
 * @link{Entidad} la cual encapsula el codigo para poder referenciarse tanto a un proyecto como
 * a una iniciativa mediante la misma clase. En ciertos casos, tambien se necesita representar
 * a PIEA como una entidad del sistema. PIEA no corresponde ni a una iniciativa ni a un proyecto
 * es por esto que se crea esta clase la cual es compatible con @link{Entidad}.
 *
 * @version   1.0
 * @since     02/11/12
 * @author    dacuna <diego.acuna@usm.cl>
 */
class PieaEntidad extends CComponent
{
    /**
     * El nombre de la entidad. Por defecto es PIEA
     * @var string
     */
    private $nombre = "PIEA";

    /**
     * Setter para el nombre de la entidad.
     *
     * @param string $nombre nombre a settear
     *
     * @return  void
     *
     * @since   2012-11-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Getter para el nombre de la entidad.
     *
     * @return  string nombre de la entidad
     *
     * @since   2012-11-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function getNombre()
    {
        return $this->nombre;
    }

}
