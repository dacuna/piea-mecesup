<?php

/**
 * Modelo para la tabla "{{rol_piea}}".
 *
 * El rol piea es un identificador unico que tienen todos los usuarios y entidades del sistema.
 * En la direccion de PIEA se solicito tener este campo pero por lo visto no se sabe que uso
 * darle, de todas maneras como es un requerimiento tuvo que ser implementado.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{rol_piea}}':
 * @property integer $ano_actual
 * @property string $siguiente_id
 *
 * @package   models
 * @version   1.0
 * @since     09-07-2012
 * @author    dacuna <diego.acuna@usm.cl>
 */
class RolPiea extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RolPiea the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{rol_piea}}';
    }

    /**
     * Genera el rol piea y lo retorna como string.
     *
     * @return  mixed string si existe rol piea para asignar, null caso contrario (no deberia ocurrir)
     *
     * @since   2012-07-09
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function generarRol()
    {
        //obtengo el anio actual
        $year = date("Y");
        $data = $this->model()->findByPk($year);
        if ($data != null) {
            $rol = $data->ano_actual . str_pad($data->siguiente_id, 4, '0', STR_PAD_LEFT);
            return $rol . '-' . $this->generarDV($rol);
        }
        return null;
    }

    /**
     * Actualiza el generador de roles piea cuando ocurre un cambio de anio en el sistema.
     *
     * @since   2012-07-09
     * @author  dacuna <diego.acuna@usm.cl>
     */
    public function actualizarGenerador()
    {
        $year = date("Y");
        $data = $this->model()->findByPk($year);
        if ($data == null) //es un anio nuevo, debo hacer un insert
        {
            $generador = new RolPiea;
            $generador->ano_actual = $year;
            $generador->siguiente_id = 1;
            $generador->save();
        } else {
            //actualizo el generador de ids.
            $data->siguiente_id++;
            $data->save(false);
        }
    }

    /**
     * Genera el digito verificador segun el rol generado utilizando calculo modulo 11.
     *
     * @param string $rol rol generado
     *
     * @return  string digito verificador
     *
     * @since   2012-07-09
     * @author  dacuna <diego.acuna@usm.cl>
     */
    private function generarDV($rol)
    {
        $tur = strrev($rol);
        $mult = 2;
        $suma = 0;
        for ($i = 0; $i <= strlen($tur); $i++) {
            if ($mult > 7) $mult = 2;
            $suma = $mult * substr($tur, $i, 1) + $suma;
            $mult = $mult + 1;
        }

        $valor = 11 - ($suma % 11);

        if ($valor == 11)
            $codigo_veri = "0";
        elseif ($valor == 10)
            $codigo_veri = "k"; else
            $codigo_veri = $valor;
        return $codigo_veri;
    }

}