<?php

Yii::import('application.modules.usuarios.models.PerfilUsuario');
Yii::import('application.modules.formularios.models.FormularioPiea');
Yii::import('application.modules.formularios.models.PerfilProyecto');
Yii::import('application.modules.formularios.models.ProcesoFormulario');

/**
 * Modelo para la tabla "{{user}}".
 *
 * Este modelo representa a un usuario en la plataforma. Cada usuario ademas posee
 * un perfil de usuario (vease @link{PerfilUsuario}) que le permite almacenar
 * informacion de corte mas especifico.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{user}}':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nombres
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $fecha_registro
 * @property string $rol_piea
 * @property string $fecha_actualizacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property IntegranteIniciativa[] $integranteIniciativas
 * @property IntegranteProyecto[] $integranteProyectos
 * @property PerfilUsuario $perfil
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Persona extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Persona the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array reglas de validacion del modelo
     */
    public function rules()
    {
        return array(
            array('email, nombres, apellido_paterno, apellido_materno', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'perfil' => array(self::HAS_ONE, 'PerfilUsuario', 'user_id'),
            'integranteIniciativas' => array(self::HAS_MANY, 'IntegranteIniciativa', 'user_id'),
            'integranteProyectos' => array(self::HAS_MANY, 'IntegranteProyecto', 'user_id'),
            'iniciativas' => array(self::MANY_MANY, 'Iniciativa', 'tbl_integrante_iniciativa(user_id,iniciativa_id)'),
            'proyectos' => array(self::MANY_MANY, 'Proyecto', 'tbl_integrante_proyecto(user_id,proyecto_id)',
                'condition' => 'proyectos.estado_postulacion=1'),
            'postulacion' => array(self::HAS_MANY, 'PefilProyecto', 'id_postulante'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email' => 'Email',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'fecha_registro' => 'Fecha Registro',
            'rol_piea' => 'Rol Piea',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('email', $this->email, true);
        $criteria->compare('nombres', $this->nombres, true);
        $criteria->compare('apellido_paterno', $this->apellido_paterno, true);
        $criteria->compare('apellido_materno', $this->apellido_materno, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Metodo que permite encontrar a un usuario mediante su nombre. El metodo intenta
     * hacer una busqueda sea cual sea el formato del nombre (recordar que en la base
     * de datos el nombre se guarda como $nombres,$ap_paterno,$ap_materno).
     *
     * @param string $name Nombre del usuario
     *
     * @return  CActiveDataProvider resultados de la busqueda
     */
    public function searchByName($name)
    {
        $criteria = new CDbCriteria;
        $params = explode(" ", $name);
        if (count($params) == 2) {
            //probablemente sea nombre+apellido
            $criteria->compare('nombres', $params[0], true, 'OR');
            $criteria->compare('apellido_paterno', $params[1], true, 'OR');
        } else if (count($params) == 4) {
            //probablemente sea nombre1 nombre2 apellido paterno apellido materno
            $criteria->compare('nombres', $params[0] . ' ' . $params[1], true, 'OR');
            $criteria->compare('apellido_paterno', $params[2], true, 'OR');
            $criteria->compare('apellido_materno', $params[3], true, 'OR');
        } else {
            //se busca por todos los parametros (para el caso count()==1 funciona bien
            //para el resto de los casos, se intenta hacer algun match en alguna parte
            $criteria->compare('nombres', $name, true, 'OR');
            $criteria->compare('apellido_paterno', $name, true, 'OR');
            $criteria->compare('apellido_materno', $name, true, 'OR');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Permite buscar a un usuario por su email.
     *
     * @param string $mail Email del usuario
     *
     * @return  CActiveDataProvider resultados de la busqueda
     */
    public function searchByMail($mail)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('email', $mail, true);

        return new CActiveDataProvider('Persona', array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retorna el nombre completo del usuario (concatenacion de nombres y ambos apellidos).
     *
     * @return  string nombre completo del usuario
     */
    public function getNombreCompleto()
    {
        return $this->nombres . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
    }

    /**
     * Retorna el nombre abreviado de un usuario (primer nombre + apellido paterno)
     *
     * @param bool $split si es true entonces se separa el nombre del apellido mediante un <br>
     *
     * @return  string nombre abreviado del usuario
     */
    public function getNombrePresentacion($split = false)
    {
        $nombre = explode(" ", $this->nombres);
        if ($split == false)
            return $nombre[0] . ' ' . $this->apellido_paterno;
        else
            return $nombre[0] . '<br>' . $this->apellido_paterno;
    }

    /**
     * Retorna la url completa de la imagen de avatar de un usuario.
     *
     * @return  string url del avatar del usuario.
     */
    public function getAvatarUrl()
    {
        if (!empty($this->perfil->avatar))
            return Yii::app()->request->baseUrl . '/avatars/' . $this->perfil->avatar;
        else
            return Yii::app()->request->baseUrl . '/images/no-picture.jpg';
    }

    /**
     * Carga el modelo de un usuario mediante su id. Si el usuario no existe entonces
     * se lanza una exception.
     *
     * @param int $id ID del usuario
     *
     * @return  User usuario encontrado
     * @throws  \CHttpException
     */
    public static function load($id)
    {
        $usuario = Persona::model()->findByPk($id);
        if ($usuario == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $usuario;
    }

    /**
     * Retorna un boolean que indica si es que el usuario es postulante del proceso actual
     * al formulario de fondos I+D+i.
     *
     * @return  bool true si es postulante, false en caso contrario
     */
    public function getEsPostulanteFondoIDI()
    {
        $formulario = FormularioPiea::model()->findByPk(1); //se obtiene el formulario I+D+i
        $proceso = $formulario->procesoActual;
        if ($proceso != null) {
            $proyecto = PerfilProyecto::model()->find('id_proceso_formulario=:ip and id_postulante=:iu', array(
                ':ip' => $proceso->id, ':iu' => $this->id
            ));
            if ($proyecto != null)
                return true;
        }
        return false;
    }

    /**
     * Si el usuario es postulante a los fondos I+D+i, entonces este metodo retorna el proyecto con el
     * que el usuario esta postulando.
     *
     * @return  mixed Proyecto si es que el usuario esta postulando, null en caso contrario.
     */
    public function getProyectoEnPostulacion()
    {
        $formulario = FormularioPiea::model()->findByPk(1); //se obtiene el formulario I+D+i
        $proceso = $formulario->procesoActual;
        if ($proceso != null) {
            $proyecto = PerfilProyecto::model()->find('id_proceso_formulario=:ip and id_postulante=:iu', array(
                ':ip' => $proceso->id, ':iu' => $this->id
            ));
            if ($proyecto != null)
                return $proyecto;
        }
        return null;
    }

    /**
     * Indica si es que el usuario pertenece a la usm ya sea como alumno o profesor.
     *
     * @return  bool true si es que pertenece, false en caso contrario.
     */
    public function perteneceAUTFSM()
    {
        $sql = "select es_interno from {{usuario_has_status}} where user_id={$this->id}";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $val = $command->queryRow();
        if (isset($val) && $val == 1)
            return true;
        return false;
    }

}