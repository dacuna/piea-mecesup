<?php
/**
 * Modelo para la tabla "{{evaluacion_fae}}".
 *
 * Este modelo representa una evaluacion FAE realizada por un evaluador sobre un evaluado. Los evaluadores
 * normalmente seran los coordinadores de iniciativas y otros usuarios con permisos especiales. Los siguientes
 * atributos estan disponibles desde la tabla '{{evaluacion_fae}}':
 *
 * @property integer $id
 * @property integer $id_evaluado
 * @property integer $id_evaluador
 * @property integer $id_entidad
 * @property integer $tipo_entidad
 * @property integer $competencia_uno
 * @property integer $competencia_dos
 * @property integer $estado
 * @property string $fecha_evaluacion
 * @property string $fecha_actualizacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property User $evaluado
 * @property User $evaluador
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class EvaluacionFae extends CActiveRecord
{
    /**
     * Entidad asociada a la evaluacion FAE actual
     * @var \Entidad
     */
    private $entidad=null;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluacionFae the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{evaluacion_fae}}';
	}

	/**
	 * @return array reglas de validacion del modelo
	 */
	public function rules()
	{
		return array(
			array('competencia_uno, competencia_dos', 'required'),
			array('competencia_uno, competencia_dos', 'numerical', 'integerOnly'=>true),
			array('id_evaluado, id_evaluador', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array Relaciones del modeo
	 */
	public function relations()
	{
		return array(
			'evaluado' => array(self::BELONGS_TO, 'Persona', 'id_evaluado'),
			'evaluador' => array(self::BELONGS_TO, 'Persona', 'id_evaluador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_evaluado' => 'Id Evaluado',
			'id_evaluador' => 'Id Evaluador',
			'id_entidad' => 'Id Entidad',
			'tipo_entidad' => 'Tipo Entidad',
			'competencia_uno' => 'Competencia Uno',
			'competencia_dos' => 'Competencia Dos',
			'fecha_evaluacion' => 'Fecha Evaluacion',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_evaluado',$this->id_evaluado);
		$criteria->compare('id_evaluador',$this->id_evaluador);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Behaviors del modelo.
     *
     * @return  array behaviors del modelo
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_evaluacion',
                'updateAttribute' => 'fecha_actualizacion',
            )
        );
    }

    /**
     * Retorna un listado de competencias FAE con las que se puede evaluar a un alumno.
     *
     * @return  array competencias FAE
     */
    public static function getCompetencias(){
        return array(
            array('id'=>1,'nombre' => 'Dise&ntilde;ar, proponer y gestionar soluciones innovadoras a las necesidades de la sociedad, organizaciones y personas, en el &aacute;mbito de la ciencia y la tecnolog&iacute;a.'),
            array('id'=>2,'nombre' => 'Capacidad para formular y gestionar proyectos.'),
            array('id'=>3,'nombre' => 'Capacidad de aplicar los conocimientos en la pr&aacute;ctica.'),
            array('id'=>4,'nombre' => 'Capacidad para actuar en nuevas situaciones.'),
            array('id'=>5,'nombre' => 'Capacidad creativa.'),
            array('id'=>6,'nombre' => 'La toma de decisiones con responsabilidad &eacute;tica.'),
            array('id'=>7,'nombre' => 'Capacidad para tomar decisiones'),
            array('id'=>8,'nombre' => 'Compromiso &eacute;tico.'),
            array('id'=>9,'nombre' => 'Ser efectivo en la comunicaci&oacute;n oral y escrita.'),
            array('id'=>10,'nombre' => 'Enfrentar las exigencias y responsabilidades del liderazgo.'),
            array('id'=>11,'nombre' => 'Capacidad de motivar y conducir hacia metas comunes.'),
            array('id'=>12,'nombre' => 'Habilidades interpersonales.'),
            array('id'=>13,'nombre' => 'Capacidad de trabajo en equipo.'),
            array('id'=>14,'nombre' => 'El dominio del idioma ingl&eacute;s.'),
            array('id'=>15,'nombre' => 'Capacidad de comunicaci&oacute;n en un segundo idioma.'),
            array('id'=>16,'nombre' => 'Habilidad para trabajar en contextos internacionales.'),
            array('id'=>17,'nombre' => 'Emprendimiento e innovaci&oacute;n con conciencia clara de las necesidades de la regi&oacute;n y de su pa&iacute;s.'),
            array('id'=>18,'nombre' => 'Compromiso con su medio social-cultural'),
            array('id'=>19,'nombre' => 'Liderazgo con reconocimiento y respeto del valor inherente y de los derechos innatos de la persona, naturaleza y de la sociedad.'),
            array('id'=>20,'nombre' => 'Responsabilidad social y compromiso ciudadano.'),
            array('id'=>21,'nombre' => 'Cooperaci&oacute;n, responsabilidad, respetuosidad y honestidad en el actuar profesional'),
            array('id'=>22,'nombre' => 'Aprecio por la cultura y compromiso con el cuidado de su salud y entorno'),
            array('id'=>23,'nombre' => 'Compromiso con la preservaci&oacute;n del medio ambiente'),
            array('id'=>24,'nombre' => 'Valoraci&oacute;n y respeto por la diversidad y multiculturalidad.'),
        );
    }

    /**
     * Retorna una version textual de la competencia 1 con que se evaluo a un usuario.
     *
     * @return  string competencia.
     */
    public function competenciaUnoTexto(){
        $competencias=EvaluacionFae::getCompetencias();
        return $competencias[$this->competencia_uno]['nombre'];
    }

    /**
     * Retorna una version textual de la competencia 2 con que se evaluo a un usuario.
     *
     * @return  string competencia.
     */
    public function competenciaDosTexto(){
        $competencias=EvaluacionFae::getCompetencias();
        return $competencias[$this->competencia_dos]['nombre'];
    }

    /**
     * Retorna la entidad asociada a la evaluacion FAE realizada.
     *
     * @return  Entidad
     */
    public function getEntidad()
    {
        if($this->entidad==null)
            $this->entidad=new Entidad($this->id_entidad,$this->tipo_entidad);
        return $this->entidad;
    }

    /**
     * Permite settear la entidad asociada al modelo.
     *
     * @param Entidad $entidad entidad asociada
     */
    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    /**
     * Carga un modelo de EvaluacionFAE mediante id. Si este no existe entonces se lanza
     * una exception.
     *
     * @param int $id ID del modelo a cargar-.
     *
     * @return  EvaluacionFae modelo encontrado
     * @throws  \CHttpException
     */
    public static function load($id)
    {
        $eval = EvaluacionFae::model()->findByPk($id);
        if ($eval== null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $eval;
    }

}