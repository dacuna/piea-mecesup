<?php

/**
 * Modelo para la tabla "{{iniciativa}}".
 *
 * Representa una iniciativa PIE>A en el mundo real. Las iniciativas son instituciones estudiantiles y
 * academicas compuestas por proyectos dentro del mismo contexto. Los siguientes son los atributos que se
 * pueden encontrar en el modelo '{{iniciativa}}':
 *
 * @property integer $id
 * @property string $rol_piea
 * @property string $nombre_completo
 * @property string $nombre_abreviado
 * @property string $sigla
 * @property string $estatutos
 * @property string $email
 * @property string $direccion
 * @property string $descripcion
 * @property string $objetivos_generales
 * @property string $objetivos_especificos
 * @property string $mision
 * @property string $vision
 * @property string $campus
 * @property string $telefono
 * @property string $logo
 * @property string $foto_equipo
 * @property string $fecha_creacion
 * @property string $fecha_ingreso
 * @property string $fecha_actualizacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property IntegranteIniciativa[] $integranteIniciativas
 * @property Proyecto[] $proyectos
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Iniciativa extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Iniciativa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{iniciativa}}';
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'integrantes' => array(self::HAS_MANY, 'IntegranteIniciativa', 'iniciativa_id'),
            //'proyectos' => array(self::HAS_MANY, 'Proyecto', 'iniciativa_id')
        );
    }

    /**
     * Overload de la relacion HAS_MANY proyectos. La necesidad de esta funcion nace cuando se implemento el modulo
     * de formularios de postulacion a los fondos I+D+i. Dado que un proyecto en postulacion tiene las mismas
     * caracteristicas que un proyecto del sistema, la entidad a utilizar es la misma. Esto genera problemas por
     * ejemplo para los proyectos que nunca fueron enviados, estos se deben mantener para fines historicos pero no
     * deben aparecer en los registros de las iniciativas a las que pertenecen. Por lo tanto se debe realizar un
     * filtro en torno a lo comentado.
     *
     * @return  void
     * @throws  \CHttpException
     *
     * @since   03-06-2013
     */
    public function getProyectos()
    {
        $proyectos = Proyecto::model()->findAll('estado_postulacion!=0 and iniciativa_id=:ii', array(
            ':ii' => $this->id
        ));
        return $proyectos;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'rol_piea' => 'Rol Piea',
            'nombre_completo' => 'Nombre Completo',
            'nombre_abreviado' => 'Nombre Abreviado',
            'sigla' => 'Sigla',
            'estatutos' => 'Estatutos',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'descripcion' => 'Descripcion',
            'objetivos_generales' => 'Objetivos Generales',
            'objetivos_especificos' => 'Objetivos Especificos',
            'mision' => 'Mision',
            'vision' => 'Vision',
            'campus' => 'Campus/Sede',
            'telefono' => 'Telefono',
            'logo' => 'Logo',
            'foto_equipo' => 'Foto Equipo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_ingreso' => 'Fecha Ingreso',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        );
    }

    /**
     * Behaviors del modelo.
     *
     * @return  array behaviors del modelo
     */
    public function behaviors()
    {
        return array(
            'dataProvider' => array(
                'class' => 'application.components.Behaviors.DynamicDataProviderBehavior',
            ),
        );
    }

    /**
     * Retorna un listado de los proyectos a los que pertenece un usuario dentro de esta iniciativa.
     * OBS: Solo se cuentan los proyectos que pertenecen a la iniciativa indicada por el modelo.
     *
     * @param int $user_id ID del usuario
     *
     * @return  array array de modelos de @link{Proyecto}
     */
    public function getProyectosDeUsuario($user_id)
    {
        return Proyecto::model()->with('integrantes')->
            findAll('t.iniciativa_id=:iid and integrantes.user_id=:uid',
                array(':iid' => $this->id,
                    ':uid' => $user_id
                ));
    }

    /**
     * Lista de campus disponibles a los que puede pertenecer una iniciativa.
     *
     * @return  array listado de campus
     */
    public function getLista_campus()
    {
        return array(
            array('id' => 'Casa Central', 'nombre' => 'Casa Central'),
            array('id' => 'Campus San Joaqu&iacute;n', 'nombre' => 'Campus San Joaqu&iacute;n'),
            array('id' => 'Campus Vitacura', 'nombre' => 'Campus Vitacura'),
            array('id' => 'Sede Vi&ntilde;a del Mar', 'nombre' => 'Sede Vi&ntilde;a del Mar'),
            array('id' => 'Sede Concepci&oacute;n', 'nombre' => 'Sede Concepci&oacute;n')
        );
    }

    /**
     * Retorna un listado de los integrantes de la iniciativa pero como modelo de @link{Persona}.
     *
     * @return  array array de @link{Persona}
     */
    public function getUsuarios()
    {
        $usuarios = array();
        foreach ($this->integrantes as $integrante) {
            array_push($usuarios, $integrante->usuario);
        }
        return $usuarios;
    }

    /**
     * Retorna los campus que pueden ser utilizados en la evaluacion FAE. Cuando se evalua un usuario
     * para FAE, este usuario no puede pertenecer a cualquier campus, solo algunos se admite. Este
     * metodo realiza una normalizacion de dicha informacion.
     *
     * @return  string Campus al que pertenece esta iniciativa segun FAE
     */
    public function getCampusSegunFae()
    {
        if ($this->campus == 'Casa Central' || $this->campus == 'Sede Jose Miguel Carrera')
            return "Valparaiso";
        if ($this->campus == 'Campus Santiago San Joaqu&iacute;n')
            return 'San Joaquin';
        if ($this->campus == 'Campus Santiago Vitacura')
            return 'Vitacura';
        else
            return "Valparaiso";
    }

    /**
     * Retorna los campus (como codigo) que pueden ser utilizados en la evaluacion FAE. Cuando se evalua un usuario
     * para FAE, este usuario no puede pertenecer a cualquier campus, solo algunos se admite. Este
     * metodo realiza una normalizacion de dicha informacion.
     *
     * @return  int Campus (codigo) al que pertenece esta iniciativa segun FAE
     */
    public function getCampusCodigoSegunFae()
    {
        if ($this->campus == 'Casa Central' || $this->campus == 'Sede Jose Miguel Carrera')
            return 1;
        if ($this->campus == 'Campus Santiago San Joaqu&iacute;n')
            return 2;
        if ($this->campus == 'Campus Santiago Vitacura')
            return 3;
        else
            return 1;
    }

}