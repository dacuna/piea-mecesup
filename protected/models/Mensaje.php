<?php

/**
 * Modelo para la tabla "{{mensaje}}".
 *
 * Este modelo representa una notificacion que se envia a un usuario del sistema. Las
 * notificaciones se envian utilizando el mensajero @link{Mailman}. Los mensajes se
 * identifican de la siguiente manera: mediante su emisor (id), receptor (id) y un
 * identificador unico generado automaticamente (uid). Para obtener un mensaje se utilizan
 * los tres campos mencionados anteriormente. Ademas, cada mensaje cuenta con su clave
 * primaria (id) por lo que si se dispone de dicha informacion tambien se puede obtener
 * un mensaje mediante ese atributo. Sin embargo, el metodo de emisor, receptor y uid
 * es el recomendado pues permite en el @link{Mailman} no tener que insertar en la base
 * de datos un mensaje y luego tener que actualizar su contenido para que el mensaje
 * pueda referenciarse a si mismo (mas informacion en @link{Mailman}).
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{mensaje}}':
 * @property integer $id
 * @property string $uid
 * @property string $asunto
 * @property string $contenido
 * @property integer $emisor
 * @property integer $destinatario
 * @property integer $leido
 * @property integer $contestado
 * @property string $fecha_envio
 * @property integer $id_relacion
 * @property string $tipo_relacion
 * @property integer $id_tarea
 * @property string $accion
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Mensaje extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Mensaje the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{mensaje}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('asunto, contenido, emisor, destinatario', 'required'),
            array('emisor, destinatario', 'numerical', 'integerOnly' => true),
            array('asunto', 'length', 'max' => 100),
            array('asunto, contenido, emisor, destinatario, leido, contestado, fecha_envio', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'emisor_persona' => array(self::HAS_ONE, 'Persona', array('id' => 'emisor')),
            'proyecto' => array(self::HAS_ONE, 'Proyecto', array('id' => 'id_relacion')),
            'iniciativa' => array(self::HAS_ONE, 'Iniciativa', array('id' => 'id_relacion')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'asunto' => 'Asunto',
            'contenido' => 'Contenido',
            'emisor' => 'Emisor',
            'destinatario' => 'Destinatario',
            'leido' => 'Leido',
            'contestado' => 'Contestado',
            'fecha_envio' => 'Fecha Envio',
            'id_relacion' => 'Id Relacion',
            'tipo_relacion' => 'Tipo Relacion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('asunto', $this->asunto, true);
        $criteria->compare('contenido', $this->contenido, true);
        $criteria->compare('emisor', $this->emisor);
        $criteria->compare('destinatario', $this->destinatario);
        $criteria->compare('leido', $this->leido);
        $criteria->compare('contestado', $this->contestado);
        $criteria->compare('fecha_envio', $this->fecha_envio, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente lo que realiza es setear
     * la fecha de envio automaticamente de mensaje.
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->fecha_envio = date('Y-m-d H:i:s');
                //$this->uid=uniqid('',true); //se genera un identificador unico para el mensaje, este codigo debe ir en el mailman
            }
            return true;
        } else
            return false;
    }

    /**
     * Retorna el total de notificaciones que el usuario logeado en el sistema posee
     * y que aun no lee o contesta.
     *
     * @return  int numero que representa el total de notificaciones pendientes de contestar
     */
    public function getNotificaciones()
    {
        return Mensaje::model()->count('destinatario=:dest and contestado=0', array(':dest' => Yii::app()->user->id));
    }

    /**
     * Retorna un mensaje asociado a una solicitud de membresia en una entidad en particular.
     * La entidad se especifica mediante un array que contiene el tipo (0=iniciativa, 1=proyecto)
     * y el id.
     *
     * @param array $entidad Entidad asociada a la solicitud
     * @param int $usuario_id ID del usuario al que se le envio la solicitud
     *
     * @return  Mensaje Solicitud enviada o null si es que no existe solicitud
     */
    public function mensajeSolicitud($entidad, $usuario_id)
    {
        return $this->find('tipo_relacion=:tid and id_relacion=:eid and emisor=:uid and accion=:aac and contestado=0', array(
            ':tid' => $entidad['tipo'],
            ':eid' => $entidad['id'],
            ':uid' => $usuario_id,
            ':aac' => 'solicitud_membresia'
        ));
    }

    /**
     * Shortcut para marcar un mensaje como contestado. Ademas permite setearle una accion realizada
     * al mensaje, es decir, almacenar cual fue la accion que se tomo al contestar el mensaje.
     *
     * @param string $accion_realizada Accion que se realizo
     *
     * @return  bool true si se contesto exitosamente el mensaje, false en caso contrario.
     */
    public function contestarMensaje($accion_realizada = null)
    {
        $this->contestado = 1;
        if ($accion_realizada != null)
            $this->accion_realizada = $accion_realizada;
        return $this->save(false);
    }

    /**
     * Permite obtener un mensaje mediante su emisor, receptor y el uid (identificador unico) del
     * mensaje. Si el mensaje no existe entonces se lanza una exception.
     *
     * @param int $emisor definition of param
     * @param int $receptor definition of param
     * @param string $uid definition of param
     *
     * @return  Mensaje mensaje obtenido.
     * @throws  \CHttpException
     */
    public static function obtenerMensaje($emisor, $receptor, $uid)
    {
        $mensaje = Mensaje::model()->find('uid=:uid and emisor=:e and destinatario=:d', array(
            ':uid' => $uid,
            ':e' => $emisor,
            ':d' => $receptor
        ));
        if ($mensaje == null)
            throw new CHttpException(404, 'El mensaje solicitado no existe.');
        return $mensaje;
    }

    /**
     * Permite marcar como contestado un mensaje especifico para las tareas de presupuestos
     * del modulo de finanzas.
     *
     * @param int $presupuesto_id ID del presupuesto
     * @param string $tipo Tipo del presupuesto
     *
     * @return  bool true si se contesta el mensaje, false en caso contrario
     * @throws  \CHttpException
     */
    public static function contestarMensajePresupuesto($presupuesto_id, $tipo = "presupuesto")
    {
        $mensaje = Mensaje::model()->find("id_tarea=:it and accion='{$tipo}' order by fecha_envio DESC", array(
            ':it' => $presupuesto_id,
        ));
        if ($mensaje == null)
            throw new CHttpException(404, 'El mensaje solicitado no existe.');
        $mensaje->contestado = 1;
        $mensaje->save(false);
        return true;
    }

    /**
     * Permite obtener un mensaje especificamente del usuario logeado. El mensaje se
     * obtiene mediante su primaryKey => ID. Si el mensaje no existe o el destinatario
     * del mensaje no es el usuario logeado entonces se lanza una exception.
     *
     * @param int $id ID del mensaje
     *
     * @return  Mensaje mensaje obtenido
     * @throws  \CHttpException
     */
    public static function obtenerMensajeDeUsuario($id)
    {
        $mensaje = Mensaje::model()->findByPk($id);
        if ($mensaje == null || $mensaje->destinatario != Yii::app()->user->id)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $mensaje;
    }

}