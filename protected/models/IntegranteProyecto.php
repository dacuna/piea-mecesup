<?php
Yii::import('application.modules.proyectos.models.LookupProyecto');

/**
 * Modelo para la tabla "{{integrante_proyecto}}".
 *
 * Este modelo representa a una @link{Persona} integrante de un proyecto.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{integrante_proyecto}}':
 * @property integer $id
 * @property integer $proyecto_id
 * @property integer $user_id
 * @property integer $visibilidad
 *
 * Las siguientes son las relaciones disponibles:
 * @property Proyecto $proyecto
 * @property User $user
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class IntegranteProyecto extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return IntegranteProyecto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{integrante_proyecto}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('visibilidad', 'required'),
            array('visibilidad', 'numerical', 'integerOnly' => true),
            array('user_id', 'exist', 'className' => 'Persona', 'attributeName' => 'id', 'allowEmpty' => false, 'on' => 'Postulacion'),
            array('responsabilidades', 'required', 'on' => 'Postulacion'),
            array('lider,finanzas', 'in', 'range' => array(0, 1), 'on' => 'Postulacion'),
            array('responsabilidades', 'type', 'type' => 'string', 'on' => 'Postulacion')
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'proyecto_id'),
            'usuario' => array(self::BELONGS_TO, 'Persona', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'proyecto_id' => 'Proyecto',
            'user_id' => 'User',
            'cargo' => 'Cargo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('proyecto_id', $this->proyecto_id);
        $criteria->compare('user_id', $this->user_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente setea la visibilidad del
     * usuario a que por defecto este sea visible (es decir pueda ser visto en los listados
     * publicos de integrantes).
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord)
                $this->visibilidad = 1;
            else
                if ($this->visibilidad != 0)
                    $this->visibilidad = 1;
            return true;
        } else
            return false;
    }

    /**
     * Shortcut para agregar un integrante en un proyecto. Realiza la verificacion de si es que
     * el integrante ya existe o no. Si es que existe entonces retorna el modelo necesario, sino
     * existe crea el modelo y lo retorna.
     *
     * @param int $user_id ID del usuario que se desea agregar como integrante
     * @param int $proyecto_id ID del proyecto.
     *
     * @return  IntegranteProyecto
     */
    public static function crearIntegrante($user_id, $proyecto_id)
    {
        $integrante = new IntegranteProyecto;
        $integrante->proyecto_id = $proyecto_id;
        $integrante->user_id = $user_id;
        $search = $integrante->search();
        if ($search->totalItemCount > 0)
            return $search->getData();
        //en este punto no hay ningun usuario creado, se agrega uno
        if ($integrante->save(false))
            return $integrante;
        return null;
    }

    /**
     * Agrega un integrante a un proyecto. Esta funcion crea los registros necesarios en todas las tablas
     * para que el usuario sea reconocido como integrante en un proyecto, es decir, crea los registros
     * en IntegranteProyecto, en LookupProyecto y el AuthItem necesario.
     *
     * @param int $user_id ID del usuario a agregar como integrante del proyecto
     * @param int $proyecto_id ID del proyecto donde se desea agregar al usuario
     * @param bool $create_integrante_model Si es que se desea crear el modelo IntegranteProyecto
     *
     * @return  mixed retorna el integrante si no existieron errores, false en caso contrario
     *
     * @since   06-01-2013
     */
    public static function agregarIntegrante($user_id, $proyecto_id, $create_integrante_model = true)
    {
        $transaction = app()->db->beginTransaction();
        try {
            $auth = Yii::app()->authManager;
            //si no tiene el authItem entonces se le agrega
            if ($auth->getAuthAssignment("integrante_proyecto", $user_id) == null) {
                $auth->assign('integrante_proyecto', $user_id);
            }
            $keyArray['nombre_item'] = 'integrante_proyecto';
            $keyArray['proyecto_id'] = $proyecto_id;
            $keyArray['user_id'] = $user_id;
            $lok = LookupProyecto::model()->findByPk($keyArray);
            if ($lok == null) {
                $lookup = new LookupProyecto;
                $lookup->nombre_item = 'integrante_proyecto';
                $lookup->proyecto_id = $proyecto_id;
                $lookup->user_id = $user_id;
                $lookup->save(false);
            }
            //se debe tambien ingresar el integrante nuevo
            $integrante = new IntegranteProyecto;
            $integrante->proyecto_id = $proyecto_id;
            $integrante->user_id = $user_id;
            $search = $integrante->search();
            if ($search->totalItemCount > 0)
                $int_ret = $search->getData();
            //en este punto no hay ningun usuario creado, se agrega uno
            if ($create_integrante_model) {
                if ($integrante->save(false))
                    $int_ret = $integrante;
            }
            $transaction->commit();
            return $int_ret;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    /**
     * Retorna una version de los parametros del modelo como array. Util para ser enviada
     * a traves de cadenas JSON.
     *
     * @return  array array con los atributos publicos del modelo.
     */
    public function convertToArray()
    {
        $return = array();
        $return['id'] = $this->id;
        $return['user_id'] = $this->user_id;
        $return['estado'] = $this->getEstadoAceptacion();
        $return['estadoNum'] = $this->estado;
        $return['lider'] = $this->lider;
        $return['finanzas'] = $this->finanzas;
        $return['responsabilidades'] = $this->responsabilidades;
        $return['nombre'] = $this->usuario->nombreCompleto;
        $return['email'] = $this->usuario->email;
        return $return;
    }

    /**
     * Retorna una version textual del estado de aceptacion de la invitacion a ser parte del
     * proyecto por este integrante (esto es valido solo para los fondos I+D+i).
     *
     * @return  string Texto que indica el estado de aceptacion del integrante.
     */
    public function getEstadoAceptacion()
    {
        if ($this->estado == 0)
            return "Pendiente de aceptacion";
        if ($this->estado == 1)
            return "Es parte del proyecto";
        if ($this->estado == -1)
            return "No ha aceptado ser parte del proyecto";
        if ($this->estado == 2)
            return "En espera de respuesta de solicitud";
    }

    /**
     * Carga un modelo IntegranteProyecto. Si no existe entonces se lanza una exception.
     *
     * @param int $id ID del integrante
     * @param int $proyecto_id ID del proyecto
     *
     * @return  \IntegranteProyecto
     * @throws  \CHttpException
     */
    public static function load($id, $proyecto_id)
    {
        $integrante = IntegranteProyecto::model()->findByPk($id);
        if ($integrante == null || $integrante->proyecto_id != $proyecto_id)
            throw new CHttpException(404, "La p&aacute;gina solicitada no existe.");
        return $integrante;
    }

    /**
     * Carga un modelo IntegranteProyecto. Si no existe entonces se lanza una exception.
     *
     * @param int $id ID del usuario que es integrante.
     * @param int $proyecto_id ID del proyecto
     *
     * @return  \IntegranteProyecto
     * @throws  \CHttpException
     */
    public static function loadByPersonaId($id, $proyecto_id)
    {
        $integrante = IntegranteProyecto::model()->find('proyecto_id=:pi and user_id=:ui', array(
            ':pi' => $proyecto_id, ':ui' => $id
        ));
        if ($integrante == null)
            throw new CHttpException(500, "El integrante solicitado no existe en el proyecto indicado.");
        return $integrante;
    }

    /**
     * Retorna si es que el integrante ha sido marcado como lider en el formulario de postulacion
     * I+D+i.
     *
     * @return  string true o false respectivamente.
     */
    public function getEsLider()
    {
        if ($this->lider) return "true";
        return "false";
    }

    /**
     * Retorna si es que el integrante ha sido marcado como responsable de finanzas en el formulario
     * de postulacion I+D+i.
     *
     * @return  string true o false respectivamente.
     */
    public function getEsFinanzas()
    {
        if ($this->finanzas) return "true";
        return "false";
    }

}