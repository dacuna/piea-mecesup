<?php
/**
 * LoginForm class, representa el modelo de ingreso al sistema.
 *
 * Provee el formulario y metodos para autenticar a los
 * usuarios en la plataforma. Es utilizado por la accion
 * login del controlador AutenticateController.
 *
 * @author Diego Acuna R. <diego.acuna@usm.cl>
 * @package models
 * @since 1.0
 */
class LoginForm extends CFormModel
{
    /**
     * Nombre de usuario del usuario de la plataforma. Debe ser unico.
     * @var string
     */
    public $username;

    /**
     * Password de acceso del usuario.
     * @var string
     */
    public $password;

    /**
     * Recordar ingreso del usuario en futuras sesiones.
     * @var bool
     */
    public $rememberMe;

    /**
     * Labels de cada atributo del modelo.
     *
     * @return array labels del modelo
     */
    public function attributeLabels()
    {
        return array(
            'username' => 'Email',
            'password' => 'Contrase&ntilde;a',
            'rememberMe' => 'Recordarme',
        );
    }

}
