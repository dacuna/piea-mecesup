<?php
Yii::import('application.modules.formularios.models.PerfilProyecto');
Yii::import('application.modules.formularios.models.ObjetivosEspecificosProyecto');
/**
 * Modelo para la tabla "{{proyecto}}".
 *
 * Representa un proyecto en PIEA. Los proyectos son agrupaciones estudiantiles que pueden emerger
 * tanto ya sea desde dentro de una iniciativa o mediante una postulacion a los formularios de
 * postulacion de fondos I+D+i.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{proyecto}}':
 * @property integer $id
 * @property integer $iniciativa_id
 * @property string $nombre
 * @property string $rol_piea
 * @property string $sigla
 * @property string $descripcion
 * @property string $campus
 * @property string $objetivos_generales
 * @property string $objetivos_especificos
 * @property string $logo
 * @property string $foto_equipo
 * @property string $fecha_inicio
 * @property string $fecha_ingreso
 * @property string $fecha_actualizacion
 * @property integer $es_postulacion
 * @property integer $estado_postulacion
 *
 * Las siguientes son las relaciones disponibles:
 * @property IntegranteProyecto[] $integranteProyectos
 * @property Iniciativa $iniciativa
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class Proyecto extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Proyecto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{proyecto}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('nombre,iniciativa_id', 'required'),
            array('iniciativa_id', 'validarIniciativa'),
            array('nombre', 'length', 'max' => 255),
            array('sigla', 'length', 'max' => 10),
            array('campus', 'length', 'max' => 40),
            array('objetivos_generales, objetivos_especificos', 'safe'),
            // The following rule is used by search().
            array('iniciativa_id,nombre,sigla,campus', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Validador para comprobar que un proyecto pertenece a una iniciativa existente. Para
     * mas informacion sobre como escribir un validador en yii ver @link{CValidator}.
     *
     * @param array $attribute atributos del validador
     * @param array $params parametros del validador
     *
     * @return  void
     */
    public function validarIniciativa($attribute, $params)
    {
        if (isset($this->iniciativa_id) && !empty($this->iniciativa_id)) {
            if ($this->iniciativa_id == -1) return;
            $iniciativa = Iniciativa::model()->findByPk($this->iniciativa_id);
            if ($iniciativa == null)
                $this->addError($attribute, 'Debes seleccionar una iniciativa v&aacute;lida.');
        }
    }

    /**
     * @return array Relaciones del modelo.
     */
    public function relations()
    {
        return array(
            'integrantes' => array(self::HAS_MANY, 'IntegranteProyecto', 'proyecto_id'),
            'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'iniciativa_id'),
            'perfilPostulacion' => array(self::HAS_ONE, 'PerfilProyecto', 'id_proyecto'),
            'objetivosEspecificos' => array(self::HAS_MANY, 'ObjetivosEspecificosProyecto', 'id_proyecto'),
            'itemsPresupuestosPostulacion' => array(self::HAS_MANY, 'ItemPresupuestoPostulacion', 'id_proyecto'),
            'referentes' => array(self::HAS_MANY, 'ReferentesProyecto', 'id_proyecto'),
            'anexos' => array(self::HAS_MANY, 'AnexoProyecto', 'id_proyecto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'iniciativa_id' => 'Iniciativa',
            'nombre' => 'T&iacute;tulo del proyecto',
            'sigla' => 'Sigla',
            'descripcion' => 'Descripcion',
            'campus' => 'Campus/Sede',
            'objetivos_generales' => 'Objetivos Generales',
            'objetivos_especificos' => 'Objetivos Especificos',
            'logo' => 'Logo',
            'foto_equipo' => 'Foto Equipo',
            'fecha_ingreso' => 'Fecha Ingreso',
            'fecha_inicio' => 'Fecha Inicio',
        );
    }

    /**
     * Lista de campus disponibles a los que puede pertenecer un proyecto.
     *
     * @return  array listado de campus
     */
    public function getLista_campus()
    {
        return array(
            array('id' => 'Casa Central', 'nombre' => 'Casa Central'),
            array('id' => 'Campus Santiago San Joaqu&iacute;n', 'nombre' => 'Campus Santiago San Joaqu&iacute;n'),
            array('id' => 'Campus Santiago Vitacura', 'nombre' => 'Campus Santiago Vitacura'),
            array('id' => 'Sede Jose Miguel Carrera', 'nombre' => 'Sede Jose Miguel Carrera'),
            array('id' => 'Sede Rey Balduino de B&eacute;lgica', 'nombre' => 'Sede Rey Balduino de B&eacute;lgica'),
            array('id' => 'Campus Rancagua', 'nombre' => 'Campus Rancagua')
        );
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente setea la fecha de inicio
     * del proyecto y verifica la iniciativa a la que este pertenece.
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (isset($this->fecha_inicio) && !empty($this->fecha_inicio))
                $this->fecha_inicio = date('Y-m-d', strtotime($this->fecha_inicio));
            if ($this->iniciativa_id == -1)
                $this->iniciativa_id = null;
            return true;
        } else
            return false;
    }

    /**
     * Retorna un array con los integrantes pero como modelos @link{Persona} pertenecientes
     * al proyecto.
     *
     * @return  array array de usuarios del proyecto.
     */
    public function getUsuarios()
    {
        $usuarios = array();
        foreach ($this->integrantes as $integrante) {
            array_push($usuarios, $integrante->usuario);
        }
        return $usuarios;
    }

    /**
     * Carga un modelo Proyecto. Si no existe entonces se lanza una exception.
     *
     * @param int $id ID del proyecto
     *
     * @return  \Proyecto
     * @throws  \CHttpException
     */
    public static function load($id)
    {
        $proyecto = Proyecto::model()->findByPk($id);
        if ($proyecto == null)
            throw new CHttpException(404, 'La p&aacute;gina solicitada no existe.');
        return $proyecto;
    }

    /**
     * Carga un modelo Proyecto que este en postulacion. Si no existe entonces se lanza una exception.
     * Si el parametro $verificar_enviado es true entonces se verifica si es que el postulante
     * ya envio el proyecto para revision, si es asi entonces se lanza un exception.
     *
     * @param int $id ID del proyecto
     * @param bool $verificar_enviado si se desea verificar que el proyecto fue enviado o no
     *
     * @return  \Proyecto
     * @throws  \CHttpException
     */
    public static function loadPostulacion($id, $verificar_enviado = false)
    {
        $proyecto = Proyecto::load($id);
        $perfil = $proyecto->perfilPostulacion;
        if ($perfil == null)
            throw new CHttpException(404, "La p&aacute;gina solicitada no existe.");
        if ($verificar_enviado && $perfil->estado_postulacion != 0)
            throw new CHttpException(500, "Este proyecto no est&aacute; habilitado para edici&oacute;n dado que ya
            ha sido enviado para postulaci&oacute;n.");
        return $proyecto;
    }

    /**
     * Behaviors del modelo.
     *
     * @return  array behaviors del modelo
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fecha_ingreso',
                'updateAttribute' => 'fecha_actualizacion',
            )
        );
    }

    /**
     * Chequea si es que un usuario tiene permisos sobre el proyecto si es que
     * el proyecto es una postulacion a los fondos I+D+i. Los usuarios que pueden
     * tener permisos son: postulantes, revisores y el administrador del sistema.
     *
     * @param int $user_id ID del usuario que desea chequear permisos
     *
     * @return  bool true si es que tiene permisos, false en caso contrario.
     */
    public function checkPermisoPostulacion($user_id)
    {
        if ($this->es_postulacion == 1) {
            //se verifica el permiso de usuario
            if (am()->checkAccess("postulante_proyecto_{$this->id}", $user_id))
                return true;
            $proceso = FormularioPiea::procesoFormularioFondosIDI();
            if (am()->checkAccess("revisor_proceso_{$proceso->id}", $user_id)) {
                $revision_proyecto = RevisorProyecto::model()->find('id_usuario=:iu and id_proyecto=:ip', array(
                    ':iu' => $user_id, ':ip' => $this->id));
                if ($revision_proyecto != null)
                    return true;
            }
        }
        return false;
    }

    /**
     * Retorna una version de las id de los integrantes del proyecto como un array.
     *
     * @return  array array con las id de los integrantes del proyecto (del usuario).
     */
    public function integrantesAsArray()
    {
        $data = array();
        foreach ($this->integrantes as $integrante) {
            $data[] = $integrante->user_id;
        }
        return $data;
    }

    /**
     * Retorna los errores en el llenado del perfil de un proyecto para la postulacion a los fondos
     * I+D+i. Basicamente chequea los campos que no se han completado.
     *
     * @return  array errores encontrados.
     */
    public function estadoPerfilPostulacion()
    {
        $errores = array();
        $perfil = $this->perfilPostulacion;
        if (empty($perfil->extracto)) $errores[] = "No has completado el extracto";
        if ($perfil->linea_postulacion == null || ($perfil->linea_postulacion != 1 && $perfil->linea_postulacion != 0)) $errores[] = "No has seleccionado la l&iacute;nea de postulaci&oacute;n de tu proyecto";
        if (empty($perfil->resumen)) $errores[] = "No has completado el resumen";
        if (empty($perfil->objetivos_generales)) $errores[] = "No has completado los objetivos generales";
        if (count($this->objetivosEspecificos) < 3) $errores[] = "Debes agregar a lo menos 3 objetivos espec&iacute;ficos";
        if (empty($perfil->estado_del_arte)) $errores[] = "No has completado el estado del arte del proyecto";
        if (empty($perfil->experiencia_previa)) $errores[] = "No has completado el campo 'experiencia previa'";
        if (empty($perfil->investigacion)) $errores[] = "No has completado el campo 'investigaci&oacute;n'";
        if (empty($perfil->desarrollo)) $errores[] = "No has completado el campo 'desarrollo'";
        if (empty($perfil->innovacion)) $errores[] = "No has completado el campo 'innovaci&oacute;n'";
        if (empty($perfil->impacto_academico)) $errores[] = "No has completado el impacto acad&eacute;mico de tu proyecto";
        if (empty($perfil->impacto_universitario_social)) $errores[] = "No has completado el impacto universitario/social de tu proyecto";
        if (empty($perfil->proyecciones)) $errores[] = "No has completado las proyecciones de tu proyecto";
        return $errores;
    }

    /**
     * Retorna la iniciativa a la que pertenece un proyecto en postulacion. Si no pertenece a ninguna
     * iniciativa entonces se retorna "No pertenece a ninguna iniciativa PIEA".
     *
     * @return  string nombre de la iniciativa
     */
    public function getIniciativaPostulacion()
    {
        if ($this->iniciativa != null)
            return $this->iniciativa->nombre_completo;
        return "No pertenece a ninguna iniciativa PIEA";
    }

    /**
     * Verifica el estado de un proyecto en postulacion con respecto al presupuesto de este. Para esto se siguen
     * las siguientes reglas de negocio:
     *  <ul>
     *      <li>La linea 0 tiene como maximo un totalIDI de 850.000</li>
     *      <li>La linea 1 tiene como maximo un totalIDI de 350.000</li>
     * </ul>
     * Si se cumplen todos los requerimientos entonces el estado es correcto. Si no, entonces el proyecto no es
     * viable para enviar a postulacion.
     *
     * @return  array cada estado de cada requerimiento (como boolean)
     *
     * @since   13-03-2013
     */
    public function estadoFinancieroPostulacion()
    {
        $items = $this->itemsPresupuestosPostulacion;
        $total = $totalIDI = $totalPropio = $totalOtros = 0;
        foreach ($items as $item) {
            $total += $item->precio * $item->unidades;
            $totalIDI += $item->idi;
            $totalPropio += $item->propio;
            $totalOtros += $item->otros;
        }
        $linea = $this->perfilPostulacion->linea_postulacion;
        $linea_post = true;
        if (isset($linea) && $linea == 0 && $totalIDI > 850000)
            $linea_post = false;
        if (isset($linea) && $linea == 1 && $totalIDI > 350000)
            $linea_post = false;
        return array('total' => $total, 'totalIDI' => $totalIDI, 'totalPropio' => $totalPropio, 'totalOtros' => $totalOtros, 'linea' => $linea_post);
    }

    /**
     * Verifica el estado de un proyecto en postulacion con respecto a los integrantes de este. Para esto se siguen
     * las siguientes reglas de negocio:
     *  <ul>
     *      <li>Solo puede haber 1 miembro lider y 1 miembro encargado de finanzas</li>
     *      <li>Todos los usuarios agregados deben haber aceptado su participacion en el proyecto</li>
     *      <li>Deben haber como minimo 2 integrantes</li>
     *      <li>El 50% (hacia arriba) de los integrantes deben ser de la UTFSM</li>
     * </ul>
     * Si se cumplen todos los requerimientos entonces el estado es correcto. Si no, entonces el proyecto no es
     * viable para enviar a postulacion. Ademas, se adjuntan los totales solicitados en el presupuesto.
     *
     * @return  array cada estado de cada requerimiento (como boolean) y los totales del presupuesto.
     *
     * @since   13-03-2013
     */
    public function estadoIntegrantes()
    {
        $lider = $finanzas = 0;
        $aprobados = true;
        $cantidad_total = count($this->integrantes);
        $cantidad_utfsm = 0;
        foreach ($this->integrantes as $integrante) {
            if ($integrante->lider == 1) $lider++;
            if ($integrante->finanzas == 1) $finanzas++;
            if ($integrante->estado == 0) $aprobados = false;
            if ($integrante->usuario->perteneceAUTFSM()) $cantidad_utfsm += 1;
        }
        $bool_cantidad = ($cantidad_total != 0) ? (($cantidad_utfsm * 100) / $cantidad_total) >= 50 : false;
        return array($lider <= 1, $finanzas <= 1, $aprobados, count($this->integrantes) > 1, $bool_cantidad);
    }

    /**
     * Indica el estado actual de la postulacion de un proyecto postulante a los fondos I+D+i. Es
     * decir, si es que al proyecto en postulacion le falta rellenar alguno de los campos obligatorios
     * o si es que ya completo la postulacion.
     *
     * @return  bool true si ya completo la postulacion, false en caso contrario.
     *
     * @since   13-03-2013
     */
    public function estadoPostulacion()
    {
        $estadoFinanciero = $this->estadoFinancieroPostulacion();
        $estadoIntegrantes = $this->estadoIntegrantes();
        $hitos = HitoProyecto::model()->findAll('id_proyecto=:ip and es_hito=1 order by fecha ASC', array(':ip' => $this->id));
        if (count($this->estadoPerfilPostulacion()) > 0 || ($estadoFinanciero['total'] != ($estadoFinanciero['totalIDI'] +
                    $estadoFinanciero['totalPropio'] + $estadoFinanciero['totalOtros'])) || count($hitos) < 5 || $estadoIntegrantes[0] == false
            || $estadoIntegrantes[1] == false || $estadoIntegrantes[2] == false || $estadoIntegrantes[3] == false
            || $estadoFinanciero['linea'] == false
        ) {
            return false;
        }
        return true;
    }

    /**
     * Permite calcular la evaluacion promedio de un proyecto en postulacion a los fondos I+D+i. Esto, considerando
     * el promedio de todas las evaluaciones realizadas al presente proyecto y presentando dicho promedio como
     * nota final.
     *
     * @return  int nota promedio de evaluacion del proyecto
     * @throws  \CHttpException
     *
     * @since   23-04-2013
     */
    public function calcularEvaluacionPromedio()
    {
        $revisores = RevisorProyecto::model()->findAll('id_proyecto=:ip', array(':ip' => $this->id));
        $nota = 0;
        foreach ($revisores as $revisor) {
            $nota += $revisor->nota;
        }
        $nota = $nota / count($revisores);
        return $nota;
    }

    /**
     * Permite calcular el total de fondos solicitado a los fondos I+D+i por el proyecto instanciado. Esto
     * se calcula mediante el presupuesto del proyecto, sumando solamente la columna de montos idi.
     *
     * @return  int El total de monto I+D+i solicitado
     *
     * @since   29-04-2013
     */
    public function calcularIDISolicitado()
    {
        $items = $this->itemsPresupuestosPostulacion;
        $total = 0;
        foreach ($items as $item) {
            $total += $item->idi;
        }
        return $total;
    }

    /**
     * Permite calcular el total de fondos asignado por los revisores para el proyecto instanciado. Esto
     * se calcula mediante el presupuesto del proyecto, sumando solamente la columna de montos idi revisado
     * de cada revision. Si un revisor no modifico el monto idi, entonces para esa revision se contabiliza
     * el monto original. Luego, se promedian todos los totales.
     *
     * @return  int El total de monto I+D+i asignado por los revisores
     *
     * @since   29-04-2013
     */
    public function calcularIDIAsignado()
    {
        //se obtienen todos los revisores para este proyecto
        $proceso = FormularioPiea::procesoFormularioFondosIDI();
        $revisiones = RevisorProyecto::model()->findAll('id_proyecto=:ip and id_proceso_formulario=:ipf', array(
            ':ip' => $this->id, ':ipf' => $proceso->id));
        $items = $this->itemsPresupuestosPostulacion;
        $totales = array();
        foreach ($revisiones as $revisor) {
            $total = 0; //guarda el total de el $revisor
            foreach ($items as $item) {
                $item_revisado = $item->getItemRevisado($revisor->id_usuario);
                if (isset($item_revisado->monto_idi_editado))
                    $total += $item_revisado->monto_idi_editado;
                else //se suma el valor original
                $total += $item->idi;
            }
            $totales[] = $total;
        }
        //ahora se promedian los totales
        return array_sum($totales) / count($totales);
    }

    /**
     * Indica si es que un proyecto que es postulante a los fondos I+D+i fue seleccionado como
     * uno de los proyectos ganadores del fondo.
     *
     * @return  mixed ProyectoSeleccionado si es que fue seleccionado, false en caso contrario.
     */
    public function fueSeleccionado()
    {
        $seleccion = ProyectoSeleccionado::model()->find('id_proyecto=:ip', array(':ip' => $this->id));
        if ($seleccion != null)
            return $seleccion;
        return false;
    }

    /**
     * Retorna los campus que pueden ser utilizados en la evaluacion FAE. Cuando se evalua un usuario
     * para FAE, este usuario no puede pertenecer a cualquier campus, solo algunos se admite. Este
     * metodo realiza una normalizacion de dicha informacion.
     *
     * @return  string Campus al que pertenece este proyecto segun FAE
     */
    public function getCampusSegunFae()
    {
        if ($this->campus == 'Casa Central' || $this->campus == 'Sede Jose Miguel Carrera')
            return "Valparaiso";
        if ($this->campus == 'Campus Santiago San Joaqu&iacute;n')
            return 'San Joaquin';
        if ($this->campus == 'Campus Santiago Vitacura')
            return 'Vitacura';
        else
            return "Valparaiso";
    }

    /**
     * Retorna los campus (como codigo) que pueden ser utilizados en la evaluacion FAE. Cuando se evalua un usuario
     * para FAE, este usuario no puede pertenecer a cualquier campus, solo algunos se admite. Este
     * metodo realiza una normalizacion de dicha informacion.
     *
     * @return  int Campus (codigo) al que pertenece este proyecto segun FAE
     */
    public function getCampusCodigoSegunFae()
    {
        if ($this->campus == 'Casa Central' || $this->campus == 'Sede Jose Miguel Carrera')
            return 1;
        if ($this->campus == 'Campus Santiago San Joaqu&iacute;n')
            return 2;
        if ($this->campus == 'Campus Santiago Vitacura')
            return 3;
        else
            return 1;
    }

    /**
     * Retorna la evaluacion FAE de un integrante de la entidad.
     *
     * @param int $integrante ID del integrante
     *
     * @return  \EvaluacionFae
     *
     * @since   05-05-2013
     */
    public function obtenerEvaluacionFaeIntegrante($integrante)
    {
        $evaluacion = EvaluacionFae::model()->find('id_evaluado=:ie and id_entidad=:in and tipo_entidad=:te', array(
            ':ie' => $integrante, ':in' => $this->id, ':te' => 1
        ));
        return $evaluacion;
    }

}