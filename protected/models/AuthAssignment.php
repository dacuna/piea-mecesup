<?php
/**
 * Modelo para la tabla "{{AuthAssignment}}".
 *
 * Representa la asignacion de un permiso a un usuario. En un ambiente normal en yii no se deberia crear
 * un modelo para esta tabla pero para los fines de esta aplicacion se necesita por ejemplo obtener todos
 * los usuarios que tienen asignado un rol en particular lo que es imposible utilizando la API actual de
 * Yii. Esta clase es util para tareas de busqueda de permisos que no se pueden realizar con el AuthManager
 * de Yii. Para tareas de asignacion, eliminacion y chequeo de permisos entonces AuthManager es la
 * herramienta a utilizar, por defecto mediante el atajo1: app()->am. Los siguientes atributos estan disponibles
 * desde la tabla '{{AuthAssignment}}':
 *
 * @property string $itemname
 * @property string $userid
 * @property string $bizrule
 * @property string $data
 *
 * Las siguientes son las relaciones disponibles:
 * @property AuthItem $permiso
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class AuthAssignment extends CActiveRecord
{
	/**
	 * Retorna una referencia estatica a la clase actual.
	 * @param string $className active record class name.
	 * @return AuthAssignment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string la tabla asociada en la db
	 */
	public function tableName()
	{
		return '{{AuthAssignment}}';
	}

	/**
	 * @return array relaciones del modelo
	 */
	public function relations()
	{
		return array(
			'permiso' => array(self::BELONGS_TO, 'AuthItem', 'itemname'),
			'usuario' => array(self::BELONGS_TO, 'Persona', 'userid'),
		);
	}

	/**
	 * @return array labels de los atributos del modelo
	 */
	public function attributeLabels()
	{
		return array(
			'itemname' => 'Itemname',
			'userid' => 'Userid',
			'bizrule' => 'Bizrule',
			'data' => 'Data',
		);
	}

	/**
	 * Retorna una lista de entidades del presente modelo segun los filtros de busqueda indicados.
	 * @return CActiveDataProvider el dataProvider que retorna los resultados de la busqueda
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('itemname',$this->itemname,true);
        $criteria->compare('itemname','administrador',true,"OR");
		$criteria->compare('userid',$this->userid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>false
		));
	}
}