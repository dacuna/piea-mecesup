<?php
/**
 * Modelo del formulario de contacto del sitio.
 *
 * Contiene ademas los metodos que implementan la logica para enviar el correo de contacto.
 *
 * @package   models
 * @version   1.0
 * @since     2012-07-02
 * @author    dacuna <diego.acuna@usm.cl>
 */
class ContactForm extends CFormModel
{
    /**
     * Nombre de la persona que se desea contactar
     * @var string
     */
	public $name;

    /**
     * Email de la persona que se desea contactar
     * @var string
     */
	public $email;

    /**
     * Asunto del mensaje de contacto
     * @var string
     */
	public $subject;

    /**
     * Cuerpo del mensaje de contacto
     * @var string
     */
	public $body;

    /**
     * Codigo Captcha de validacion del formulario de contacto
     * @var string
     */
	public $verifyCode;

	/**
	 * Reglas de validacion del modelo.
	 *
	 * @return  array array con reglas de validacion
	 *
	 * @since   2012-07-02
	 * @author  dacuna <diego.acuna@usm.cl>
	 */
	public function rules()
	{
		return array(
			array('name, email, subject, body', 'required'),
			array('email', 'email'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

    /**
     * Labels de cada atributo del modelo.
     *
     * @return  array array los labels de los atributos
     *
     * @since   2012-07-02
     * @author  dacuna <diego.acuna@usm.cl>
     */
	public function attributeLabels()
	{
		return array(
            'name'=>'Nombre',
            'email'=>'Email',
            'subject'=>'Asunto',
            'body'=>'Mensaje',
			'verifyCode'=>'Verification Code',
		);
	}
}