<?php

/**
 * Modelo para la tabla "{{integrante_iniciativa}}".
 *
 * Este modelo representa a una @link{Persona} integrante de una iniciativa.
 *
 * Los siguientes atributos estan disponibles desde la tabla '{{integrante_iniciativa}}':
 * @property integer $id
 * @property integer $iniciativa_id
 * @property integer $user_id
 * @property string $cargo
 *
 * Las siguientes son las relaciones disponibles:
 * @property Iniciativa $iniciativa
 * @property User $user
 *
 * @package   models
 * @version   1.0
 * @author    dacuna <diego.acuna@usm.cl>
 */
class IntegranteIniciativa extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return IntegranteIniciativa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string la tabla asociada en la db
     */
    public function tableName()
    {
        return '{{integrante_iniciativa}}';
    }

    /**
     * @return array reglas de validacion del modelo
     */
    public function rules()
    {
        return array(
            array('visibilidad', 'required'),
            array('visibilidad', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array Relaciones del modeo.
     */
    public function relations()
    {
        return array(
            'iniciativa' => array(self::BELONGS_TO, 'Iniciativa', 'iniciativa_id'),
            'usuario' => array(self::BELONGS_TO, 'Persona', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'iniciativa_id' => 'Iniciativa',
            'user_id' => 'User',
            'cargo' => 'Cargo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('iniciativa_id', $this->iniciativa_id);
        $criteria->compare('user_id', $this->user_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Handler para el evento beforeSave del modelo. Basicamente setea la visibilidad del
     * usuario a que por defecto este sea visible (es decir pueda ser visto en los listados
     * publicos de integrantes).
     *
     * @return  bool true si se ejecuto con exito, false en caso contrario
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord)
                $this->visibilidad = 1;
            else
                if ($this->visibilidad != 0)
                    $this->visibilidad = 1;
            return true;
        } else
            return false;
    }

    /**
     * Shortcut para agregar un integrante en una iniciativa. Realiza la verificacion de si es que
     * el integrante ya existe o no. Si es que existe entonces retorna el modelo necesario, sino
     * existe crea el modelo y lo retorna.
     *
     * @param int $user_id ID del usuario que se desea agregar como integrante
     * @param int $iniciativa_id ID de la iniciativa.
     *
     * @return  IntegranteIniciativa
     */
    public static function crearIntegrante($user_id, $iniciativa_id)
    {
        $integrante = new IntegranteIniciativa;
        $integrante->iniciativa_id = $iniciativa_id;
        $integrante->user_id = $user_id;
        $search = $integrante->search();
        if ($search->totalItemCount > 0)
            return $search->getData();
        //en este punto no hay ningun usuario creado, se agrega uno
        if ($integrante->save(false))
            return $integrante;
        return null;
    }
}