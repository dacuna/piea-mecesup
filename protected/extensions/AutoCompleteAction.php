<?php
class AutoCompleteAction extends CAction
{
    public $model;
    public $attribute;
    private $results = array();
 
    public function run()
    {
      if(isset($this->model) && isset($this->attribute)) {
        $criteria = new CDbCriteria();
        $criteria->compare('nombres', $_GET['term'], true, 'OR');
        $criteria->compare('apellido_paterno', $_GET['term'], true, 'OR');
        $criteria->compare('apellido_materno', $_GET['term'], true, 'OR');
        $model = new $this->model;
        foreach($model->findAll($criteria) as $m)
        {
            if(isset($_GET['mode']) && $_GET['mode']=='json')
            {
              $this->results[] = array(
                'key'=>$m->id,
                'value'=>$m->nombreCompleto,
              );
            }
            else
            {
              $this->results[] = array(
                'label'=>$m->nombreCompleto,
                'value'=>$m->nombreCompleto,
                'id'=>$m->id
              );
            }
        }
      }
      echo CJSON::encode($this->results);
    }
}
