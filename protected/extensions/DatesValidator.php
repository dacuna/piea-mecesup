<?php

class DatesValidator extends CValidator
{
    public $date_first;
    public $date_second;

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;

        if($attribute == 'fecha_inicio')
            $this->date_first = $value;

        if($attribute == 'fecha_termino')
            $this->date_second = $value;

        if($this->date_first !== NULL && $this->date_second !== NULL )
        {
            if(strtotime($this->date_first) > strtotime($this->date_second))
                $model->addError($attribute, 'La fecha de t&eacute;mino debe ser posterior a la fecha de inicio');
            //tambien se aprovecha de validar que las fechas no sean mayores que la fecha actual
            if(strtotime($model->$attribute)>strtotime("now"))
                $model->addError($attribute,$model->getAttributeLabel($attribute).' no puede ser superior a la fecha actual');
        }
    }
}